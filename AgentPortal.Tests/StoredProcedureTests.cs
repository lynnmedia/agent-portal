﻿using AgentPortal.Repositories.StoredProcedures;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;

namespace AgentPortal.Tests
{
    /// <summary>
    /// Tests for the migrated Db stored procedures.
    /// </summary>
    [Ignore("Waiting for mock")]
    [TestClass]
    public class StoredProcedureTests
    {
        // ReSharper disable once UnusedMember.Local
        private static readonly JsonSerializerSettings DebugJsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new Helpers.OrderedContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };

        private static IStoredProcedureComponents _storedProcedureComponentsImplementation;

        private static StoredProcedures _storedProcedures;

        private string[] _relativities = new string[]
        {
            "Total Discount Factor",
            "Total Surcharge Factor",
            "Without Fee",
            "Annualized Vehicle Premium",
            "Vehicle Premium",
            "Annualized Policy",
            "Policy Premium"
        };

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        /// <summary>
        /// The MyClassCleanup.
        /// </summary>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary>
        /// The MyClassInitialize.
        /// </summary>
        /// <param name="testContext">The testContext<see cref="TestContext"/>.</param>
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            _storedProcedureComponentsImplementation = new StoredProcedureComponentsSql();
            _storedProcedures = new StoredProcedures(_storedProcedureComponentsImplementation);
        }

        
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
		
		
        /// <summary>
        /// The TestAllowAutoDraft.
        /// </summary>
        [TestMethod]
        public void TestAllowAutoDraft_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210009";
            const int expected = 0;

            // Act
            var result = _storedProcedures.AllowAutoDraft(policyNo);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Tests AuditSaveDifference method
        /// 
        /// Method being tested was migrated from db stored proc spUW_AuditSaveDifference.
        /// </summary>
        [TestMethod]
        public void TestAuditSaveDifference_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210016";
            const string table1 = "PolicyDriversViolations";
            const string table2 = "PolicyBindersDriversViolations";
            int? historyId = 110515;
            const string userName = "user";
            bool? ignoreSpecialFields = false;
            const string guid = "b8234893-1875-4c5d-9b61-446fc3a4e77f";

            // Act
            _storedProcedures.AuditSaveDifference(policyNo, table1, table2, historyId, userName, ignoreSpecialFields,
                guid);

            // Assert
            Assert.Fail("Not yet implemented");
        }

        /// <summary>
        /// Tests AutomaticActionsStep1 method
        /// 
        /// Method being tested was migrated from db stored proc spUW_AutomaticActionsStep1.
        /// </summary>
        [TestMethod]
        [SuppressMessage("ReSharper", "ExpressionIsAlwaysNull")]
        public void TestAutomaticActionsStep1_Migrated()
        {
            // Arrange
            DateTime? requestedReportDate = null;

            // Act
            _storedProcedures.AutomaticActionsStep1(requestedReportDate);

            // Assert
            Assert.Fail("Not yet implemented");
        }

        /// <summary>
        /// The TestAutomaticActionsStep3_Migrated.
        /// </summary>
        [TestMethod]
        public void TestAutomaticActionsStep3_Migrated()
        {
            // Arrange
            DateTime? requestReportDate = new DateTime(2021, 12, 12);

            // Act
            _storedProcedures.AutomaticActionsStep3(requestReportDate);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests ClearPolicyEndorseQuote method
        /// 
        /// Method being tested was migrated from db stored proc spUW_ClearPolicyEndorseQuote.
        /// </summary>
        [TestMethod]
        public void TestClearPolicyEndorseQuote_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210489";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.ClearPolicyEndorseQuote(policyNo, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests ClearPolicyRenQuoteTemp method
        /// 
        /// Method being tested was migrated from db stored proc spUW_ClearPolicyRenQuoteTemp.
        /// </summary>
        [TestMethod]
        public void TestClearPolicyRenQuoteTemp_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0218363-01";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.ClearPolicyRenQuoteTemp(policyNo, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests CopyPolicyEndorseQuoteToPolicyTables method
        /// 
        /// Method being tested was migrated from db stored proc spUW_CopyPolicyEndorseQuoteToPolicyTables.
        /// </summary>
        [TestMethod]
        public void TestCopyPolicyEndorseQuoteToPolicyTables_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210014";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";
            const bool fromWeb = true;

            // Act
            _storedProcedures.CopyPolicyEndorseQuoteToPolicyTables(policyNo, userName, guid, fromWeb);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Test for CopyPolicyToAuditTables
        ///
        ///.
        /// </summary>
        [TestMethod]
        public void TestCopyPolicyToAuditTables_Migrated()
        {
            const string policyNo = "AAFL0210005";
            const int historyId = 644;
            const string userName = "xyz";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.CopyPolicyToAuditTables(policyNo, historyId, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests CopyPolicyToPolicyEndorseQuoteTables method
        /// 
        /// Method being tested was migrated from db stored proc spUW_CopyPolicyToPolicyEndorseQuoteTables.
        /// </summary>
        [TestMethod]
        public void TestCopyPolicyToPolicyEndorseQuoteTables_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210014";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.CopyPolicyToPolicyEndorseQuoteTables(policyNo, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests CreateInstallmentsPolicy method
        /// 
        /// Method being tested was migrated from db stored proc spUW_CreateInstallmentsPolicy.
        /// </summary>
        [TestMethod]
        public void TestCreateInstallmentsPolicy_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0219489";
            decimal? policyWritten = null;
            int? installCount = null;
            decimal? installNo = null;
            DateTime? dueDate = null;
            decimal? installmentPymt = null;
            bool? createDownPayment = true;
            bool? forceDepositToAmountPaid = true;
            int? historyIndexId = null;
            double? amountForForceDeposit = null;
            const string userName = "user";
            const string guid = "8C3E28C6-64AB-44B6-B73C-313E6D2F2AED";

            // Act
            _storedProcedures.CreateInstallmentsPolicy(policyNo, policyWritten, installCount,
                installNo, dueDate, installmentPymt, createDownPayment, forceDepositToAmountPaid, userName, guid,
                historyIndexId, amountForForceDeposit);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests CreateInstallmentsRenQuote method
        /// 
        /// Method being tested was migrated from db stored proc spUW_CreateInstallmentsRenQuote.
        /// </summary>
        [TestMethod]
        public void TestCreateInstallmentsRenQuote_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0218259-01";
            decimal? policyWritten = null;
            int? installCount = null;
            decimal? installNo = null;
            DateTime? dueDate = null;
            decimal? installmentPymt = null;
            bool? createDownPayment = null;
            bool? forceDepositToAmountPaid = null;
            int? historyIndexId = null;
            double? amountForForceDeposit = null;
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.CreateInstallmentsRenQuote(policyNo, policyWritten, installCount,
                installNo, dueDate, installmentPymt, createDownPayment, forceDepositToAmountPaid, userName, guid,
                historyIndexId, amountForForceDeposit);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Test when nulls are sent to LogError forcing default substitutions
        /// 
        /// This test uses separate variables to hold each parameter to LogError
        /// then compares each using separate Asserts. Some would feel using multiple
        /// Asserts is a code smell, but when used for testing a result object containing
        /// multiple fields, it does not violate the spirit of the unit test.
        /// </summary>
        [TestMethod]
        public void TestErrorLogger_Defaults()
        {
            // Arrange
            const string testGuid = null;
            const string testPolicyNo = null;
            const string testUserName = null;
            DateTime? testDateTimeError = null;
            const string testErrorProcedure = null;
            const string testErrorCode = null;
            const string testErrorDescr = null;
            // MyErrorLog logEntry;

            // Act
            // ReSharper disable once ExpressionIsAlwaysNull
            _storedProcedures.LogError(testGuid, testPolicyNo, testUserName, testDateTimeError, testErrorProcedure,
                testErrorCode, testErrorDescr);

            // Get last entry to compare to expected
            // ... code to get last entry ...
            //logEntry = CopyErrorLogProperties(recs.First());

            // Assert

            //Assert.AreEqual(String.Empty, logEntry.Guid);
            //Assert.AreEqual(String.Empty, logEntry.PolicyNo);
            //Assert.AreEqual(Environment.UserName, logEntry.UserName);
            //// need to mock DateTime.Now
            //Assert.AreEqual("TestErrorLogger_Defaults", logEntry.ErrorProcedure);
            //Assert.AreEqual(String.Empty, logEntry.ErrorCode);
            //Assert.AreEqual(String.Empty, logEntry.ErrorDescr);
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Test a fully populated call to LogError
        /// 
        /// Chose to use a locally defined container to hold
        /// just the parameters that will be sent to LogError.
        /// This was needed to avoid side effects that could possibly
        /// arise from using the ErrorLog class itself to hold the data,
        /// and serializing ErrorLog, with its own annotations and purposes,
        /// does not work.  So params are put into a local object, and the
        /// object retrieved is copied into a local object.
        /// 
        /// This test method uses the JSON serialization approach.  It works
        /// fairly well for object comparisons, and does provide some good info
        /// when discrepancies are reported on failures.
        /// </summary>
        [TestMethod]
        public void TestErrorLogger_Populated()
        {
            // Arrange
            var inputs = new MyErrorLog()
            {
                Guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6",
                PolicyNo = "A20211030",
                UserName = "john.Jpones",
                DateTimeError = DateTime.Now,
                ErrorProcedure = "theErrorProc",
                ErrorCode = "17",
                ErrorDescr = "This is a test of a test."
            };
            //MyErrorLog logEntry;

            // Act
            _storedProcedures.LogError(inputs.Guid, inputs.PolicyNo, inputs.UserName, inputs.DateTimeError,
                inputs.ErrorProcedure, inputs.ErrorCode, inputs.ErrorDescr);

            // Get last entry to compare to expected
            // ... code to get last entry ...
            //logEntry = CopyErrorLogProperties(recs.First());

            //var jsonTest = JsonConvert.SerializeObject(inputs, Formatting.Indented, DebugJsonSettings);
            // var jsonResult = JsonConvert.SerializeObject(logEntry, Formatting.Indented, DebugJsonSettings);

            // Assert

            // Assert.AreEqual(jsonTest, jsonResult);
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Test when one of the values to be saved is too long for the database field
        /// 
        /// While the original stored proc would just fail in this case, the migrated
        /// procedure truncates values to fit into their respective fields.
        /// </summary>
        [TestMethod]
        public void TestErrorLogger_TooLong()
        {
            // Arrange
            const string testGuid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";
            const string testPolicyNo = "A20211030";
            const string testUserName = "adam.smith";
            var testDateTimeError = DateTime.Now;
            const string testErrorProcedure = "theErrorProcedure";
            const string testErrorCode = "1200-ABCD-9999";
            const string testErrorDescr = "emergency  system.";
            //MyErrorLog logEntry;

            // Act
            _storedProcedures.LogError(testGuid, testPolicyNo, testUserName, testDateTimeError, testErrorProcedure,
                testErrorCode, testErrorDescr);

            // Get last entry to compare to expected
            // ... code to get last entry ...
            //logEntry = CopyErrorLogProperties(recs.First());

            // Assert

            //Assert.AreEqual(testGuid, logEntry.Guid);
            //Assert.AreEqual(testPolicyNo, logEntry.PolicyNo);
            //Assert.AreEqual(testUserName, logEntry.UserName);
            //Assert.AreEqual(testDateTimeError.ToString(CultureInfo.InvariantCulture), logEntry.DateTimeError.ToString(CultureInfo.InvariantCulture));
            //Assert.AreEqual(testErrorProcedure, logEntry.ErrorProcedure);
            //Assert.AreEqual("1200-ABCD-", logEntry.ErrorCode);
            //Assert.AreEqual(testErrorDescr, logEntry.ErrorDescr);
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests GetListProjectedInstallments method
        /// 
        /// Method being tested was migrated from db stored proc spUW_GetListProjectedInstallments.
        /// </summary>
        [TestMethod]
        public void TestGetListProjectedInstallments_Migrated()
        {
            // Arrange
            const string policyNo = "Q-FL0210019";
            const int ratingId = 37;
            const int payPlan = 0;
            Guid guid = Guid.NewGuid();

            // Act
            _storedProcedures.GetListProjectedInstallments(policyNo, ratingId, payPlan, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// The TestGetRenewalDraftAmount.
        /// </summary>
        [TestMethod]
        public void TestGetRenewalDraftAmount()
        {
            // Arrange
            const string policyNo = "AAFL0210018-01";
            const string userName = "Smith";
            const string guid = "A245C80B-D046-4941-BF89-18874E8586D6";
            const decimal expected = 425.91M;

            // Act
            var result = _storedProcedures.GetRenewalDraftAmount(policyNo, userName, guid);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Tests GetTotalDueOnPolicy method
        /// 
        /// Method being tested was migrated from db stored proc spUW_GetTotalDueOnPolicy.
        /// </summary>
        [TestMethod]
        public void TestGetTotalDueOnPolicy_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0219140";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";
            const decimal expected = -143.29M;

            // Act
            var result = _storedProcedures.GetTotalDueOnPolicy(policyNo, userName, guid);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Tests InstallmentPaymentApply method
        /// 
        /// Method being tested was migrated from db stored proc spUW_InstallmentPaymentApply.
        /// </summary>
        [TestMethod]
        public void TestInstallmentPaymentApply_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210005";
            DateTime postmarkDate = DateTime.Now;
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.InstallmentPaymentApply(policyNo, postmarkDate, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// The TestInstallNoCurrent_Migrated.
        /// </summary>
        [TestMethod]
        public void TestInstallNoCurrent_Migrated()
        {
            // Arrange
            var policyNo = "AAFL0210009";
            var mailDate = new DateTime(2020, 11, 17);
            var expected = 4;

            // Act
            var result = _storedProcedures.InstallNoCurrent(policyNo, mailDate);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Tests IssueCancellation method
        /// 
        /// Method being tested was migrated from db stored proc spUW_IssueCancellation.
        /// </summary>
        [TestMethod]
        public void TestIssueCancellation_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0214739";
            DateTime? cancellationDate = new DateTime(2020, 9, 2);
            DateTime? acctDate = DateTime.Now;
            const bool flatCancel = false;
            const bool shortRate = false;
            const string userName = "user";
            const string guidErrorLog = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";
            const string guidHistoryIndexId = "6BD84C11-170C-42F3-8862-88A3276ED66E";

            // Act
            _storedProcedures.IssueCancellation(policyNo, cancellationDate, flatCancel, shortRate, acctDate, userName,
                guidErrorLog, guidHistoryIndexId);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests IssueNotice method
        /// 
        /// Method being tested was migrated from db stored proc spUW_IssueNotice.
        /// </summary>
        [TestMethod]
        public void TestIssueNotice_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210005";
            DateTime? mailDate = new DateTime(2019, 10, 2);
            DateTime noticeDate = DateTime.Now;
            const string cancelType = "Underwriting Request";
            const string reason1 = "reason 1";
            const string reason2 = "reason 2";
            const string reason3 = "reason 3";
            const string reason4 = "";
            bool isNSFFeeCharged = false;
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.IssueNotice(policyNo, noticeDate, cancelType, reason1, reason2, reason3,
                reason4, userName, mailDate, isNSFFeeCharged, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests LateFeePolicyApply method
        /// 
        /// Method being tested was migrated from db stored proc spUW_LateFeePolicyApply.
        /// </summary>
        [TestMethod]
        public void TestLateFeePolicyApply_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210005";
            DateTime mailDate = DateTime.Now;
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.LateFeePolicyApply(policyNo, mailDate, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests MinimumEndorsementPaymentDue method
        /// 
        /// Method being tested was migrated from db stored proc spUW_MinimumEndorsementPaymentDue.
        /// </summary>
        [TestMethod]
        public void TestMinimumEndorsementPaymentDue_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0219140";
            const string userName = "user";
            DateTime endorsementDate = DateTime.Parse("11/28/2021 7:01:58 PM");
            const decimal fullAmountOfEndorsement = 1000M;
            const decimal expected = 337.66M;

            // Act
            var result =
                _storedProcedures.MinimumEndorsementPaymentDue(policyNo, endorsementDate, fullAmountOfEndorsement,
                    userName);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Tests MoveMoney method
        /// 
        /// Method being tested was migrated from db stored proc spUW_MoveMoney.
        /// </summary>
        [TestMethod]
        public void TestMoveMoney_Migrated()
        {
            // Arrange
            const string moveFromPolicyNo = "AAFL0210007";
            const string moveToPolicyNo = "AAFL0210007";
            const decimal amount = 0M;
            const string userName = "user";
            DateTime displayDate = DateTime.Parse("11/28/2021 7:01:58 PM");
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";
            const bool ignoreToPolicyExists = false;


            // Act
            _storedProcedures.MoveMoney(moveFromPolicyNo, moveToPolicyNo, amount, userName, guid,
                // ReSharper disable once RedundantArgumentDefaultValue
                displayDate, ignoreToPolicyExists);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// The TestNextBusinessDay_Migrated.
        /// </summary>
        /// <param name="year">The year<see cref="int"/>.</param>
        /// <param name="month">The month<see cref="int"/>.</param>
        /// <param name="day">The day<see cref="int"/>.</param>
        /// <param name="expectedYear">The expectedYear<see cref="int"/>.</param>
        /// <param name="expectedMonth">The expectedMonth<see cref="int"/>.</param>
        /// <param name="expectedDay">The expectedDay<see cref="int"/>.</param>
        [DataRow(2021, 12, 23, 2021, 12, 24)]
        [DataRow(2021, 12, 24, 2021, 12, 27)]
        [DataRow(2021, 12, 25, 2021, 12, 27)]
        [DataRow(2022, 1, 6, 2022, 1, 7)]
        [DataRow(2022, 1, 7, 2022, 1, 10)]
        [DataRow(2022, 1, 17, 2022, 1, 18)]
        [DataTestMethod]
        public void TestNextBusinessDay_Migrated(int year, int month, int day, int expectedYear, int expectedMonth,
            int expectedDay)
        {
            // Arrange
            DateTime src = new DateTime(year, month, day);
            DateTime expected = new DateTime(expectedYear, expectedMonth, expectedDay);

            // Act
            var result = _storedProcedures.NextBusinessDay(src);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Test migration of fnPolicyBalance
        ///     to Stored Procedures/Functions: PolicyBalance().
        /// </summary>
        /// <param name="policyNo">The policyNo<see cref="string"/>.</param>
        /// <param name="expectedResultStr">The expectedResultStr<see cref="string"/>.</param>
        [DataRow("AAFL0219140", "0")]
        [DataRow("A123", "0")]
        [DataTestMethod]
        public void TestPolicyBalance_Migrated(string policyNo, string expectedResultStr)
        {
            // Arrange
            decimal expectedResult = Decimal.Parse(expectedResultStr);

            // Act
            var result = _storedProcedures.PolicyBalance(policyNo);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        /// <summary>
        /// Tests PolicyFutureActionsAddRecord method
        /// 
        /// Method being tested was migrated from db stored proc spUW_PolicyFutureActions_AddRecord.
        /// </summary>
        [TestMethod]
        public void TestPolicyFutureActionsAddRecord_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210007";
            const string action = "";
            DateTime dueDate = DateTime.Parse("11/28/2021 7:01:58 PM");
            DateTime mailDate = DateTime.Parse("11/28/2021 7:01:58 PM");
            DateTime processDate = DateTime.Parse("11/28/2021 7:01:58 PM");
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";
            const int historyIndexId = 0;


            // Act
            _storedProcedures.PolicyFutureActionsAddRecord(policyNo, action, dueDate, mailDate, processDate, userName,
                guid, historyIndexId);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests PolicyUpdateFieldFormats method
        /// 
        /// Method being tested was migrated from db stored proc spUW_PolicyUpdateFieldFormats.
        /// </summary>
        [TestMethod]
        public void TestPolicyUpdateFieldFormats_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210112";

            // Act
            _storedProcedures.PolicyUpdateFieldFormats(policyNo);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests RatePolicyAlert method
        /// 
        /// Method being tested was migrated from db stored proc spUW_RatePolicyAlert.
        /// </summary>
        /// <param name="currIdStr">The currIdStr<see cref="string"/>.</param>
        /// <param name="quoteId">The quoteId<see cref="string"/>.</param>
        /// <param name="isRenewal">The isRenewal<see cref="bool?"/>.</param>
        [DataRow("406185", "", true)]
        [DataRow("", "Q-FL0210017", false)]
        [DataRow("406185", null, false)]
        [DataTestMethod]
        public void TestRatePolicyAlert_Migrated(string currIdStr, string quoteId, bool? isRenewal)
        {
            // Arrange
            // const string currIdStr = "406185";
            const string guid = "c6c71571-30a2-4997-85bf-926f5a6c65ba";
            const string userName = null;
            //const string quoteId = "";
            const bool rtrProcessing = false;
            //bool? isRenewal = true;

            // Act
            _storedProcedures.RatePolicyAlert(currIdStr, guid, userName, quoteId, rtrProcessing, isRenewal);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests RateSpartan_CVData_AA20210412 method
        /// 
        /// Method being tested was migrated from db stored proc spUW_RateSpartan_CVData_AA20210412.
        /// </summary>
        [TestMethod]
        [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
        public void TestRateSpartan_CVData_AA20210412_Migrated()
        {
            // Arrange
            const int currId = 406185;
            const string guid = "c6c71571-30a2-4997-85bf-926f5a6c65ba";
            const string userName = null;
            const string quoteId = "Q-FL0210038";
            const bool rtrProcessing = false;
            bool? isRenewal = true;

            // Act
            _storedProcedures.RateSpartan_CVData_AA20210412(currId, guid, userName, quoteId, rtrProcessing, isRenewal);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests RateSpartan_CVData method
        /// 
        /// Method being tested was migrated from db stored proc spUW_RateSpartan_CVData.
        /// </summary>
        [TestMethod]
        [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
        public void TestRateSpartan_CVData_Migrated()
        {
            // Arrange
            const int currId = 406185;
            const string guid = "c6c71571-30a2-4997-85bf-926f5a6c65ba";
            const string userName = null;
            const string quoteId = "Q-FL0210038";
            const bool rtrProcessing = false;
            bool? isRenewal = true;

            // Act
            _storedProcedures.RateSpartan_CVData(currId, guid, userName, quoteId, rtrProcessing, isRenewal);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests TotalRelativities_AA20210412 method
        /// 
        /// Method being tested was migrated from db stored proc spUW_TotalRelativities_AA20210412.
        /// </summary>
        [TestMethod]
        public void TestTotalRelativities_AA20210412_Migrated()
        {
            // Arrange
            var mainIndexId = 49;
            var vehIndexId = 50;
            var ratedOprIndexId = 50;
            var relativity = _relativities[6];

            _storedProcedures.TotalRelativities_AA20210412(mainIndexId, vehIndexId, ratedOprIndexId, relativity);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests TotalRelativities method
        /// 
        /// Method being tested was migrated from db stored proc spUW_TotalRelativities.
        /// </summary>
        [TestMethod]
        public void TestTotalRelativities_Migrated()
        {
            // Arrange
            var mainIndexId = 50;
            var vehIndexId = 51;
            var ratedOprIndexId = 51;
            var relativity = _relativities[6];

            // Act
            _storedProcedures.TotalRelativities(mainIndexId, vehIndexId, ratedOprIndexId, relativity);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests TransferPolicyToPolicyRenQuote method
        /// 
        /// Method being tested was migrated from db stored proc spUW_TransferPolicyToPolicyRenQuote.
        /// </summary>
        [TestMethod]
        public void TestTransferPolicyToPolicyRenQuote_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210019";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.TransferPolicyToPolicyRenQuote(policyNo, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests TransferRenQuoteToPolicyBinders method
        /// 
        /// Method being tested was migrated from db stored proc spUW_TranferRenQuoteToPolicyBinders.
        /// </summary>
        [TestMethod]
        public void TestTransferRenQuoteToPolicyBinders_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210294-01";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.TransferRenQuoteToPolicyBinders(policyNo, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// Tests TransferRenQuoteToRenQuoteTemp method
        /// 
        /// Method being tested was migrated from db stored proc spUW_TransferRenQuoteToRenQuoteTemp.
        /// </summary>
        [TestMethod]
        public void TestTransferRenQuoteToRenQuoteTemp_Migrated()
        {
            // Arrange
            const string policyNo = "AAFL0210294-01";
            const string userName = "user";
            const string guid = "9A96F058-AEFA-40EF-A2EE-E647A7E9F2B6";

            // Act
            _storedProcedures.TransferRenQuoteToRenQuoteTemp(policyNo, userName, guid);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// The TestUpdateInstallmentAudit_Migrated.
        /// </summary>
        [TestMethod]
        public void TestUpdateInstallmentAudit_Migrated()
        {
            // Arrange
            const string userName = "Jones";

            // Act
            _storedProcedures.UpdateInstallmentAudit(userName);

            // Assert
            Assert.Fail("Not Yet Implemented");
        }

        /// <summary>
        /// The TestValidBusinessDay_Migrated.
        /// </summary>
        /// <param name="year">The year<see cref="int"/>.</param>
        /// <param name="month">The month<see cref="int"/>.</param>
        /// <param name="day">The day<see cref="int"/>.</param>
        /// <param name="expected">The expected<see cref="bool"/>.</param>
        [DataRow(2020, 12, 25, false)]
        [DataRow(2020, 12, 23, true)]
        [DataRow(2022, 1, 8, false)]
        [DataTestMethod]
        public void TestValidBusinessDay_Migrated(int year, int month, int day, bool expected)
        {
            // Arrange
            DateTime src = new DateTime(year, month, day);

            // Act
            var result = _storedProcedures.ValidBusinessDay(src);

            // Assert
            Assert.AreEqual(expected, result);
        }
    }

    /// <summary>
    /// Local class to hold fields of log entry, and the parameters.
    /// </summary>
    public class MyErrorLog
    {
        [JsonIgnore] // Tells DatTime to be ignored - We don't want this format since we are converting to date string comparisons
        public DateTime DateTimeError { get; set; }

        [JsonProperty(PropertyName = "DateTimeError")]
        public string DateTimeErrorString => DateTimeError.ToString(CultureInfo.InvariantCulture);

        public string ErrorCode { get; set; }

        public string ErrorDescr { get; set; }

        public string ErrorProcedure { get; set; }

        public string Guid { get; set; }

        public string PolicyNo { get; set; }

        public string UserName { get; set; }
    }
}
