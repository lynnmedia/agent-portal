﻿using System;
using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace AgentPortal.Tests.Fakes
{
    public class StubPolicyCars
    {
        private static readonly Dictionary<string, StubPolicyCars> TestCars =
            new Dictionary<string, StubPolicyCars>()
            {
                {"test1",new StubPolicyCars()
                {
                    ABS = 0,
                    CarIndex = 2,
                    CollDed = "500",
                    CompDed = "1000",
                    EffDate = DateTime.Parse("2021-02-01"),
                    HasColl = true,
                    HasComp = true,
                    HasPhyDam = false,
                    isValid = true,
                    PolicyNo = "AAFL0210003",
                    RateCycle = "AA20201007",
                    VehCylinders = "4",
                    VehDoors = "4",
                    VehDriveType = null,
                    VehMake = "Chrysler",
                    VehModel1 = "PT Cruiser",
                    VehModel2 = "WAG 4D",
                    VehYear = 2006,
                    VIN = "3A4FY48B56T247505",
                    VINIndex = -1,
                }},
                {"test2",new StubPolicyCars()
                {
                    ABS = 0,
                    CarIndex = 2,
                    CollDed = null,
                    CompDed = "1000",
                    EffDate = DateTime.Parse("2020-12-01"),
                    HasColl = false,
                    HasComp = true,
                    HasPhyDam = true,
                    isValid = true,
                    PolicyNo = "AAFL0210006-02",
                    RateCycle = null,
                    VehCylinders = "4",
                    VehDoors = "4",
                    VehDriveType = null,
                    VehMake = "Mercedes-Benz",
                    VehModel1 = "C-Class",
                    VehModel2 = "SEDAN 4D",
                    VehYear = 2003,
                    VIN = "WDBRF40J43F433102",
                    VINIndex = -1,
                }},
                {"test3",new StubPolicyCars()
                {
                    ABS = 0,
                    CarIndex = 1,
                    CollDed = "500",
                    CompDed = null,
                    EffDate = DateTime.Parse("2021-04-15"),
                    HasColl = true,
                    HasComp = false,
                    HasPhyDam = false,
                    isValid = true,
                    PolicyNo = null,
                    RateCycle = null,
                    VehCylinders = "4",
                    VehDoors = "2",
                    VehDriveType = null,
                    VehMake = "Pontiac",
                    VehModel1 = "Sunfire",
                    VehModel2 = "COUPE 2D",
                    VehYear = 2004,
                    VIN = "1G2JB12F047226515",
                    VINIndex = -1,
                }},
                {"test4",new StubPolicyCars()
                {
                    ABS = 0,
                    CarIndex = 4,
                    CollDed = null,
                    CompDed = null,
                    EffDate = DateTime.Parse("2021-10-11"),
                    HasColl = false,
                    HasComp = false,
                    HasPhyDam = false,
                    isValid = false,
                    PolicyNo = "AAFL0210826-02",
                    RateCycle = null,
                    VehCylinders = "4",
                    VehDoors = "4",
                    VehDriveType = null,
                    VehMake = "Ford",
                    VehModel1 = "Fusion",
                    VehModel2 = "SEDAN 4D",
                    VehYear = 2006,
                    VIN = "3FAFP08Z66R143414",
                    VINIndex = -1,
                }}
            };

        #region Properties currently in stub

        public int ABS { get; set; }
        public int CarIndex { get; set; }
        public string CollDed { get; set; }
        public string CompDed { get; set; }
        public DateTime EffDate { get; set; }
        public bool HasColl { get; set; }
        public bool HasComp { get; set; }
        public bool HasPhyDam { get; set; }
        public bool isValid { get; set; }
        public string PolicyNo { get; set; }
        public string RateCycle { get; set; }
        public string VehCylinders { get; set; }
        public string VehDoors { get; set; }
        public string VehDriveType { get; set; }
        public string VehMake { get; set; }
        public string VehModel1 { get; set; }
        public string VehModel2 { get; set; }
        public int VehYear { get; set; }
        public string VIN { get; set; }
        public int VINIndex { get; set; }

        #endregion

        #region Other Available Properties

        public int ABSDiscAmt { get; set; }
        public double AmountCustom { get; set; }
        public int APCDiscAmt { get; set; }
        public int ARB { get; set; }
        public int ARBDiscAmt { get; set; }
        public bool Artisan { get; set; }
        public string ATD { get; set; }
        public int ATDDiscAmt { get; set; }
        public int BIAnnlPrem { get; set; }
        public string BISymbol { get; set; }
        public int BIWritten { get; set; }
        public bool BusinessUse { get; set; }
        public int CollAnnlPrem { get; set; }
        public string COLLSymbol { get; set; }
        public int CollWritten { get; set; }
        public int CompAnnlPrem { get; set; }
        public string COMPSymbol { get; set; }
        public int CompWritten { get; set; }
        public bool ConvTT { get; set; }
        public double CostNew { get; set; }
        public string Damage { get; set; }
        public bool Deleted { get; set; }
        public int DirectRepairDiscAmt { get; set; }
        public DateTime ExpDate { get; set; }
        public bool FarmUse { get; set; }
        public int HomeDiscAmt { get; set; }
        public int InexpSurgAmt { get; set; }
        public int InternationalSurgAmt { get; set; }
        public bool Is4wd { get; set; }
        public bool IsDRL { get; set; }
        public bool IsForceSymbols { get; set; }
        public bool IsHybrid { get; set; }
        public bool IsIneligible { get; set; }
        public string ISOPhyDamSymbol { get; set; }
        public string ISOSymbol { get; set; }
        public int MCDiscAmt { get; set; }
        public int MilesToWork { get; set; }
        public int MPAnnlPrem { get; set; }
        public string MPSymbol { get; set; }
        public int MPWritten { get; set; }
        public bool MultiCar { get; set; }
        public int NoHitSurgAmt { get; set; }
        public int OID { get; set; }
        public int OOSLicSurgAmt { get; set; }
        public int PDAnnlPrem { get; set; }
        public string PDSymbol { get; set; }
        public int PDWritten { get; set; }
        public int PIPAnnlPrem { get; set; }
        public string PIPSymbol { get; set; }
        public int PIPWritten { get; set; }
        public int PointSurgAmt { get; set; }
        public int PrincOpr { get; set; }
        public int PriorBalanceSurgAmt { get; set; }
        public bool PurchNew { get; set; }
        public int RatedOpr { get; set; }
        public string RatingClass { get; set; }
        public int RenDiscAmt { get; set; }
        public int SDDiscAmt { get; set; }
        public int SSDDiscAmt { get; set; }
        public double StatedAmtAnnlPrem { get; set; }
        public double StatedAmtCost { get; set; }
        public int TotalPoints { get; set; }
        public int TransferDiscAmt { get; set; }
        public int UMAnnlPrem { get; set; }
        public string UMSymbol { get; set; }
        public int UMWritten { get; set; }
        public int UnacceptSurgAmt { get; set; }
        public string VSRCollSymbol { get; set; }
        public int WLDiscAmt { get; set; }

        #endregion

        public static StubPolicyCars GetStubPolicyCar(string testId)
        {
            return TestCars.TryGetValue(testId, out var car) ? (StubPolicyCars)car.MemberwiseClone() : new StubPolicyCars();
        }

        public Support.DataObjects.PolicyCars ToPolicyCars()
        {
            var policyCar = new Support.DataObjects.PolicyCars()
            {
                ABS = this.ABS,
                CarIndex = this.CarIndex,
                CollDed = this.CollDed,
                CompDed = this.CompDed,
                EffDate = this.EffDate,
                HasColl = this.HasColl,
                HasComp = this.HasComp,
                HasPhyDam = this.HasPhyDam,
                isValid = this.isValid,
                PolicyNo = this.PolicyNo,
                RateCycle = this.RateCycle,
                VehCylinders = this.VehCylinders,
                VehDoors = this.VehDoors,
                VehDriveType = this.VehDriveType,
                VehMake = this.VehMake,
                VehModel1 = this.VehModel1,
                VehModel2 = this.VehModel2,
                VehYear = this.VehYear,
                VIN = this.VIN,
                VINIndex = this.VINIndex
        };
            return policyCar;
        }

    }
}