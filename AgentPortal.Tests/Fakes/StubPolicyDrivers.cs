﻿using System;
using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace AgentPortal.Tests.Fakes
{
    public class StubPolicyDrivers
    {
        private static readonly Dictionary<string, StubPolicyDrivers> TestDrivers =
            new Dictionary<string, StubPolicyDrivers>()
            {
                {"test1", new StubPolicyDrivers()
                {
                    Class = "7",
                    Company = "Bowser International",
                    CompanyAddress = "4954 Stadium Drive",
                    CompanyCity = "Two Egg",
                    CompanyState = "FL",
                    CompanyZip = "32443",
                    DateLicensed = DateTime.Parse("1985-04-04"),
                    Deleted = false,
                    DOB = DateTime.Parse("1965-01-02"),
                    DriverAge = 56,
                    DriverIndex = default,
                    Exclude = false,
                    FirstName = "Bryan",
                    Gender = "MALE",
                    InceptionDate = DateTime.Parse("2020-02-02"),
                    Inexp = false,
                    InternationalLic = false,
                    JobTitle = "Nurse",
                    LastName = "Rust",
                    LicenseNo = "M624-172-78-793-0",
                    LicenseSt = "FL",
                    Married = true,
                    MI = "J",
                    Occupation = "RETIRED",
                    OID = 1000,
                    Points = 1,
                    PolicyNo = "AAFL0210369",
                    PrimaryCar = 0,
                    PTSDriverIndex = 0,
                    RelationToInsured = "INSURED",
                    SafeDriver = false,
                    SeniorDefDrvDate = DateTime.MinValue,
                    SeniorDefDrvDisc = false,
                    SR22 = false,
                    SSN = null,
                    Suffix = null,
                    SuspLic = false,
                    Unverifiable = false,
                    Violations = 0,
                    WorkPhone = null,
                    Youthful = false
                }},
                {"test2", new StubPolicyDrivers()
                {
                    Class = "5",
                    Company = "Yardbirds Home Center",
                    CompanyAddress = "3163 Pointe Lane",
                    CompanyCity = "Hollywood",
                    CompanyState = "FL",
                    CompanyZip = "33023",
                    DateLicensed = DateTime.Parse("2008-04-12"),
                    Deleted = false,
                    DOB = DateTime.Parse("1991-09-30"),
                    DriverAge = 30,
                    DriverIndex = default,
                    Exclude = false,
                    FirstName = "Thomas",
                    Gender = "MALE",
                    InceptionDate = DateTime.Parse("2020-10-02"),
                    Inexp = false,
                    InternationalLic = true,
                    JobTitle = "Conservationist",
                    LastName = "Grable",
                    LicenseNo = "M520-525-74-840-0",
                    LicenseSt = "FL",
                    Married = false,
                    MI = "J",
                    Occupation = "Other",
                    OID = 4000,
                    Points = 4,
                    PolicyNo = "AAFL0110369-03",
                    PrimaryCar = 0,
                    PTSDriverIndex = 0,
                    RelationToInsured = "BROTHER",
                    SafeDriver = false,
                    SeniorDefDrvDate = DateTime.MinValue,
                    SeniorDefDrvDisc = false,
                    SR22 = false,
                    SSN = null,
                    Suffix = null,
                    SuspLic = false,
                    Unverifiable = false,
                    Violations = 1,
                    WorkPhone = null,
                    Youthful = false
                }}
            };

        public string Class { get; set; }
        public string Company { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyZip { get; set; }
        public DateTime DateLicensed { get; set; }
        public bool Deleted { get; set; }
        public DateTime DOB { get; set; }
        public int DriverAge { get; set; }
        public int DriverIndex { get; set; }
        public DateTime EffDate { get; set; }
        public bool Exclude { get; set; }
        public DateTime ExpDate { get; set; }
        public string FirstName { get; set; }
        public string Gender { get; set; }
        public DateTime InceptionDate { get; set; }
        public bool Inexp { get; set; }
        public bool InternationalLic { get; set; }
        public string JobTitle { get; set; }
        public string LastName { get; set; }
        public string LicenseNo { get; set; }
        public string LicenseSt { get; set; }
        public bool Married { get; set; }
        public string MI { get; set; }
        public string Occupation { get; set; }
        public int OID { get; set; }
        public int Points { get; set; }
        public string PolicyNo { get; set; }
        public int PrimaryCar { get; set; }
        public int PTSDriverIndex { get; set; }
        public string RelationToInsured { get; set; }
        public bool SafeDriver { get; set; }
        public DateTime SeniorDefDrvDate { get; set; }
        public bool SeniorDefDrvDisc { get; set; }
        public bool SR22 { get; set; }
        public string SR22CaseNo { get; set; }
        public DateTime SR22PrintDate { get; set; }
        public DateTime SR26PrintDate { get; set; }
        public string SSN { get; set; }
        public string Suffix { get; set; }
        public bool SuspLic { get; set; }
        public bool Unverifiable { get; set; }
        public int Violations { get; set; }
        public string WorkPhone { get; set; }
        public bool Youthful { get; set; }

        public static StubPolicyDrivers GetStubPolicyDriver(string testId)
        {
            return TestDrivers.TryGetValue(testId, out var driver)
                ? (StubPolicyDrivers)driver.MemberwiseClone()
                : new StubPolicyDrivers();
        }

        public Support.DataObjects.PolicyDrivers ToPolicyDrivers()
        {
            var policyDriver = new Support.DataObjects.PolicyDrivers()
            {
                Class = this.Class,
                Company = this.Company,
                CompanyAddress = this.CompanyAddress,
                CompanyCity = this.CompanyCity,
                CompanyState = this.CompanyState,
                CompanyZip = this.CompanyZip,
                DateLicensed = this.DateLicensed,
                Deleted = this.Deleted,
                DOB = this.DOB,
                DriverAge = this.DriverAge,
                DriverIndex = this.DriverIndex,
                Exclude = this.Exclude,
                ExpDate = this.ExpDate,
                FirstName = this.FirstName,
                Gender = this.Gender,
                InceptionDate = this.InceptionDate,
                Inexp = this.Inexp,
                InternationalLic = this.InternationalLic,
                JobTitle = this.JobTitle,
                LastName = this.LastName,
                LicenseNo = this.LicenseNo,
                LicenseSt = this.LicenseSt,
                Married = this.Married,
                MI = this.MI,
                Occupation = this.Occupation,
                OID = this.OID,
                Points = this.Points,
                PolicyNo = this.PolicyNo,
                PrimaryCar = this.PrimaryCar,
                PTSDriverIndex = this.PTSDriverIndex,
                RelationToInsured = this.RelationToInsured,
                SafeDriver = this.SafeDriver,
                SeniorDefDrvDate = this.SeniorDefDrvDate,
                SeniorDefDrvDisc = this.SeniorDefDrvDisc,
                SR22 = this.SR22,
                SR22CaseNo = this.SR22CaseNo,
                SR22PrintDate = this.SR22PrintDate,
                SR26PrintDate = this.SR26PrintDate,
                SSN = this.SSN,
                Suffix = this.Suffix,
                SuspLic = this.SuspLic,
                Unverifiable = this.Unverifiable,
                Violations = this.Violations,
                WorkPhone = this.WorkPhone,
                Youthful = this.Youthful
        };
            return policyDriver;
        }
    }
}