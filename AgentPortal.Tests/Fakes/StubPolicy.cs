﻿using System;
using System.Collections.Generic;

namespace AgentPortal.Tests.Fakes
{
    /// <summary>
    /// Stub version of the Policy<see cref="AgentPortal.Support.DataObjects.Policy"/>
    /// class for use in testing.
    ///
    /// This stub version contains those properties that are so far
    /// determined to be useful in the current stage of testing.  As
    /// more are needed, they can be added.
    ///
    /// There are two choices for getting a test policy object:
    ///    1. Call GetStubPolicy(id) with an identifier that matches
    ///       one of the test policies in the TestPolicies dictionary.
    ///       The returned object is a memberwise clone of the dictionary
    ///       object.
    ///    2. Call GenStubPolicy(options) with an options objects where
    ///       options are specified for how to set each property.
    ///
    /// The result in either case is an object of the type StubPolicy.
    /// If an actual Policy object is desired, call ToPolicy() on the
    /// object and a Policy object will be created with the properties
    /// corresponding to those in the StubPolicy object overridden by
    /// the stub properties.
    ///
    /// When calling ToPolicy(), one can specify whether to use defaults
    /// for the other properties or use what has been found in the data
    /// to always have been the value saved to the database.
    /// </summary>
    public class StubPolicy
    {
        /// <summary>
        /// A set of policy objects for use in testing.
        ///
        /// Each object contains various different permutations of good, bad,
        /// edge, or anomalous property values.
        ///
        /// ADD AS MANY NEW CASES HERE AS DESIRED
        /// </summary>
        private static readonly Dictionary<string, StubPolicy> TestPolicies = new Dictionary<string, StubPolicy>()
        {

            { "Good", new StubPolicy() {
                Cars= 1,
                Drivers= 1,
                EffDate= DateTime.Parse("2021-10-15"),
                EFT= true,
                ExpDate= DateTime.Parse("2022-02-12"),
                PaidInFullDisc= true,
                PayPlan= 0,
                PolicyCars= 2,
                PolicyDrivers= 4,
                PolicyNo= "AAFL0210020",
                PolicyScore= 30,
                PolicyStatus= "ACTIVE",
                QuoteNo= "Q-FL0319519"
            } },
            { "NoFL", new StubPolicy() {
                Cars= 1,
                Drivers= 10,
                EffDate= DateTime.Parse("2021-10-15"),
                EFT= false,
                ExpDate= DateTime.Parse("2021-10-15"),
                PaidInFullDisc= true,
                PayPlan= 0,
                PolicyCars= 2,
                PolicyDrivers= 4,
                PolicyNo= "AATX0210020",
                PolicyScore= 30,
                PolicyStatus= "ACTIVE",
                QuoteNo= "AAFL0210020"
            } },
            { "Suffix", new StubPolicy() {
                Cars= 1,
                Drivers= 1,
                EffDate= DateTime.Parse("2021-10-15"),
                EFT= true,
                ExpDate= DateTime.Parse("2022-02-12"),
                PaidInFullDisc= true,
                PayPlan= 0,
                PolicyCars= 2,
                PolicyDrivers= 4,
                PolicyNo= "AAFL0210020-01",
                PolicyScore= 30,
                PolicyStatus= "ACTIVE",
                QuoteNo= "R-FL0319519" } },
            { "AllDefaults", new StubPolicy(){
                Cars= 0,
                Drivers= 0,
                EffDate= DateTime.MinValue,
                EFT= false,
                ExpDate=DateTime.MinValue,
                PaidInFullDisc= false,
                PayPlan= 0,
                PolicyCars= 0,
                PolicyDrivers= 0,
                PolicyNo = null,
                PolicyScore= 0,
                PolicyStatus = null,
                QuoteNo = null
            }}
        };

        public int Cars { get; set; }

        public int Drivers { get; set; }

        public DateTime EffDate { get; set; }

        // ReSharper disable once InconsistentNaming
        public bool EFT { get; set; }

        public DateTime ExpDate { get; set; }

        public bool PaidInFullDisc { get; set; }

        public int PayPlan { get; set; }

        public int PolicyCars { get; set; }

        public int PolicyDrivers { get; set; }

        public string PolicyNo { get; set; }

        public int PolicyScore { get; set; }

        public string PolicyStatus { get; set; }

        public string QuoteNo { get; set; }

        #region Methods

        /// <summary>
        /// Generates a test policy based on the options specified
        /// </summary>
        /// <param name="options">The options<see cref="PolicyTestOptions"/>.</param>
        /// <returns>The <see cref="StubPolicy"/>.</returns>
        public static StubPolicy GenStubPolicy(PolicyTestOptions options)
        {
            return new StubPolicy
            {
                PolicyNo = GenTestPolicyNo(options.OptPolicyNo),
                EffDate = GenTestEffDate(options.OptEffDate),
                EFT = GenTestEft(options.OptEft),
                ExpDate = GenTestExpDate(options.OptExpDate),
                PaidInFullDisc = GenTestPaidInFullDisc(options.OptPaidInFullDesc),
                PayPlan = GenTestPayPlan(options.OptPayPlan),
                PolicyStatus = GenTestPolicyStatus(options.OptPolicyStatus),
                PolicyScore = GenTestPolicyScore(options.OptPolicyScore),
                QuoteNo = GenTestQuoteNo(options.OptQuoteNo),
                Cars = GenTestCars(options.OptCars),
                PolicyCars = GenTestPolicyCars(options.OptPolicyCars),
                Drivers = GenTestDrivers(options.OptDrivers),
                PolicyDrivers = GenTestPolicyDrivers(options.OptPolicyDrivers)
            };
        }

        /// <summary>
        /// Returns the test policy with the given test ID
        ///
        /// Preset policies are kept in TestPolicies<see cref="TestPolicies"/>
        /// with each associated with a unique identifier for easy retrieval.
        ///
        /// If a policy with the given ID is not found, a default new Policy
        /// is returned.
        /// </summary>
        /// <param name="testId">uniquely identifies a test policy.</param>
        /// <returns>.</returns>
        public static StubPolicy GetStubPolicy(string testId)
        {
            return TestPolicies.TryGetValue(testId, out var policy) ? (StubPolicy)policy.MemberwiseClone() : new StubPolicy();
        }

        /// <summary>
        /// The GenTestCars.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyCarsOptions"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        private static int GenTestCars(PolicyCarsOptions option)
        {
            var cars = default(int);
            switch (option)
            {
                case PolicyCarsOptions.Zero:
                    cars = 0;
                    break;
                case PolicyCarsOptions.Neg:
                    cars = -2;
                    break;
                case PolicyCarsOptions.One:
                    cars = 1;
                    break;
                case PolicyCarsOptions.Low:
                    cars = 2;
                    break;
                case PolicyCarsOptions.Mid:
                    cars = 4;
                    break;
                case PolicyCarsOptions.High:
                    cars = 10;
                    break;
                case PolicyCarsOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return cars;
        }

        /// <summary>
        /// The GenTestDrivers.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyDriversOptions"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        private static int GenTestDrivers(PolicyDriversOptions option)
        {
            var drivers = default(int);
            switch (option)
            {
                case PolicyDriversOptions.Zero:
                    drivers = 0;
                    break;
                case PolicyDriversOptions.Neg:
                    drivers = -2;
                    break;
                case PolicyDriversOptions.One:
                    drivers = 1;
                    break;
                case PolicyDriversOptions.Low:
                    drivers = 2;
                    break;
                case PolicyDriversOptions.Med:
                    drivers = 4;
                    break;
                case PolicyDriversOptions.High:
                    drivers = 10;
                    break;
                case PolicyDriversOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return drivers;
        }

        /// <summary>
        /// The GenTestEffDate.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyDateOptions"/>.</param>
        /// <returns>The <see cref="DateTime"/>.</returns>
        private static DateTime GenTestEffDate(PolicyDateOptions option)
        {
            var effDate = default(DateTime);
            switch (option)
            {
                case PolicyDateOptions.InPast:
                    effDate = DateTime.Now.Subtract(new TimeSpan(60, 0, 0, 0));
                    break;
                case PolicyDateOptions.InFuture:
                    effDate = DateTime.Now.Add(new TimeSpan(60, 0, 0, 0));
                    break;
                case PolicyDateOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return effDate;
        }

        /// <summary>
        /// The GenTestEft.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyBoolOptions"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        private static bool GenTestEft(PolicyBoolOptions option)
        {
            var eft = default(bool);
            switch (option)
            {
                case PolicyBoolOptions.True:
                    eft = true;
                    break;
                case PolicyBoolOptions.False:
                    // ReSharper disable once RedundantAssignment
                    eft = false;
                    break;
                case PolicyBoolOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return eft;
        }

        /// <summary>
        /// The GenTestExpDate.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyDateOptions"/>.</param>
        /// <returns>The <see cref="DateTime"/>.</returns>
        private static DateTime GenTestExpDate(PolicyDateOptions option)
        {
            var expDate = default(DateTime);
            switch (option)
            {
                case PolicyDateOptions.InPast:
                    expDate = DateTime.Now.Subtract(new TimeSpan(60, 0, 0, 0));
                    break;
                case PolicyDateOptions.InFuture:
                    expDate = DateTime.Now.Add(new TimeSpan(60, 0, 0, 0));
                    break;
                case PolicyDateOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return expDate;
        }

        /// <summary>
        /// The GenTestPaidInFullDisc.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyBoolOptions"/>.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        private static bool GenTestPaidInFullDisc(PolicyBoolOptions option)
        {
            var paidInFullDisc = default(bool);

            switch (option)
            {
                case PolicyBoolOptions.True:
                    paidInFullDisc = true;
                    break;
                case PolicyBoolOptions.False:
                    // ReSharper disable once RedundantAssignment
                    paidInFullDisc = false;
                    break;
                case PolicyBoolOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return paidInFullDisc;
        }

        /// <summary>
        /// The GenTestPayPlan.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyPayPlanOptions"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        private static int GenTestPayPlan(PolicyPayPlanOptions option)
        {
            var payPlan = default(int);

            switch (option)
            {
                case PolicyPayPlanOptions.Zero:
                    payPlan = 0;
                    break;
                case PolicyPayPlanOptions.Neg:
                    payPlan = -51;
                    break;
                case PolicyPayPlanOptions.One:
                    payPlan = 1;
                    break;
                // TODO: Find out what these values should be
                case PolicyPayPlanOptions.Low:
                    break;
                case PolicyPayPlanOptions.Med:
                    break;
                case PolicyPayPlanOptions.High:
                    break;
                case PolicyPayPlanOptions.Min:
                    break;
                case PolicyPayPlanOptions.Max:
                    break;
                case PolicyPayPlanOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return payPlan;
        }

        /// <summary>
        /// The GenTestPolicyCars.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyCarsOptions"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        private static int GenTestPolicyCars(PolicyCarsOptions option)
        {
            var policyCars = default(int);
            switch (option)
            {
                case PolicyCarsOptions.Zero:
                    policyCars = 0;
                    break;
                case PolicyCarsOptions.Neg:
                    policyCars = -2;
                    break;
                case PolicyCarsOptions.One:
                    policyCars = 1;
                    break;
                case PolicyCarsOptions.Low:
                    policyCars = 2;
                    break;
                case PolicyCarsOptions.Mid:
                    policyCars = 4;
                    break;
                case PolicyCarsOptions.High:
                    policyCars = 10;
                    break;
                case PolicyCarsOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return policyCars;
        }

        /// <summary>
        /// The GenTestPolicyDrivers.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyDriversOptions"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        private static int GenTestPolicyDrivers(PolicyDriversOptions option)
        {
            var policyDrivers = default(int);
            switch (option)
            {
                case PolicyDriversOptions.Zero:
                    policyDrivers = 0;
                    break;
                case PolicyDriversOptions.Neg:
                    policyDrivers = -4;
                    break;
                case PolicyDriversOptions.One:
                    policyDrivers = 1;
                    break;
                case PolicyDriversOptions.Low:
                    policyDrivers = 2;
                    break;
                case PolicyDriversOptions.Med:
                    policyDrivers = 4;
                    break;
                case PolicyDriversOptions.High:
                    policyDrivers = 10;
                    break;
                case PolicyDriversOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return policyDrivers;
        }

        /// <summary>
        /// The GenTestPolicyNo.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyPolicyNoOptions"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        private static string GenTestPolicyNo(PolicyPolicyNoOptions option)
        {
            var policyNo = default(string);
            switch (option)
            {
                case PolicyPolicyNoOptions.Good:
                    policyNo = "AAFL0210020";
                    break;
                case PolicyPolicyNoOptions.NoFlorida:
                    policyNo = "AATX0210020";
                    break;
                case PolicyPolicyNoOptions.Suffixed:
                    policyNo = "AAFL0210021-02";
                    break;
                case PolicyPolicyNoOptions.Empty:
                    policyNo = string.Empty;
                    break;
                case PolicyPolicyNoOptions.Null:
                    // ReSharper disable once RedundantAssignment
                    policyNo = null;
                    break;
                case PolicyPolicyNoOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return policyNo;
        }

        /// <summary>
        /// The GenTestPolicyScore.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyPolicyScoreOptions"/>.</param>
        /// <returns>The <see cref="int"/>.</returns>
        private static int GenTestPolicyScore(PolicyPolicyScoreOptions option)
        {
            var policyScore = default(int);
            switch (option)
            {
                // TODO: Find out range of policy scores
                case PolicyPolicyScoreOptions.Min:
                    policyScore = 0;
                    break;
                case PolicyPolicyScoreOptions.Max:
                    policyScore = 100;
                    break;
                case PolicyPolicyScoreOptions.Low:
                    policyScore = 10;
                    break;
                case PolicyPolicyScoreOptions.Med:
                    policyScore = 30;
                    break;
                case PolicyPolicyScoreOptions.High:
                    policyScore = 90;
                    break;
                case PolicyPolicyScoreOptions.Zero:
                    policyScore = 0;
                    break;
                case PolicyPolicyScoreOptions.Neg:
                    policyScore = -5;
                    break;
                case PolicyPolicyScoreOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return policyScore;
        }

        /// <summary>
        /// The GenTestPolicyStatus.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyPolicyStatusOptions"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        private static string GenTestPolicyStatus(PolicyPolicyStatusOptions option)
        {
            var policyStatus = default(string);

            switch (option)
            {
                case PolicyPolicyStatusOptions.Active:
                    policyStatus = "ACTIVE";
                    break;
                case PolicyPolicyStatusOptions.Cancel:
                    policyStatus = "CANCEL";
                    break;
                case PolicyPolicyStatusOptions.Notice:
                    policyStatus = "NOTICE";
                    break;
                case PolicyPolicyStatusOptions.Voidpend:
                    policyStatus = "VOIDPEND";
                    break;
                case PolicyPolicyStatusOptions.Other:
                    policyStatus = "A8DK49FK50";
                    break;
                case PolicyPolicyStatusOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return policyStatus;
        }

        /// <summary>
        /// The GenTestQuoteNo.
        /// </summary>
        /// <param name="option">The option<see cref="PolicyQuoteNoOptions"/>.</param>
        /// <returns>The <see cref="string"/>.</returns>
        private static string GenTestQuoteNo(PolicyQuoteNoOptions option)
        {
            var quoteNo = default(string);
            switch (option)
            {
                case PolicyQuoteNoOptions.Good:
                    quoteNo = "Q-FL0319519";
                    break;
                case PolicyQuoteNoOptions.NoFlorida:
                    quoteNo = "Q-NM0319519";
                    break;
                case PolicyQuoteNoOptions.Suffixed:
                    quoteNo = "Q-FL0319519-01";
                    break;
                case PolicyQuoteNoOptions.Policy:
                    quoteNo = "AAFL0210020";
                    break;
                case PolicyQuoteNoOptions.PolicySuffixed:
                    quoteNo = "AAFL0210020-02";
                    break;
                case PolicyQuoteNoOptions.NoQ:
                    quoteNo = "R-FL0319519";
                    break;
                case PolicyQuoteNoOptions.Empty:
                    quoteNo = string.Empty;
                    break;
                case PolicyQuoteNoOptions.Null:
                    // ReSharper disable once RedundantAssignment
                    quoteNo = null;
                    break;
                case PolicyQuoteNoOptions.Ignore:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(option), option, null);
            }

            return quoteNo;
        }

        public Support.DataObjects.Policy ToPolicy(bool useDefaults = true)
        {
            var policy = new Support.DataObjects.Policy
            {
                Cars = this.Cars,
                Drivers = this.Drivers,
                EffDate = this.EffDate,
                EFT = this.EFT,
                ExpDate = this.ExpDate,
                PaidInFullDisc = this.PaidInFullDisc,
                PayPlan = this.PayPlan,
                policyCars = this.PolicyCars,
                policyDrivers = this.PolicyDrivers,
                PolicyNo = this.PolicyNo,
                PolicyStatus = this.PolicyStatus,
                QuoteNo = this.QuoteNo,
            };
            if (!useDefaults)
            {
                policy.FromPortal = true;
                policy.PDLimit = "10000";
                policy.SixMonth = true;
            }

            return policy;
        }

        #endregion

    }

    /// <summary>
    /// Object specifying desired options when generating
    /// a test policy.
    /// </summary>
    public class PolicyTestOptions
    {
        public PolicyCarsOptions OptCars { get; set; }

        public PolicyDriversOptions OptDrivers { get; set; }

        public PolicyDateOptions OptEffDate { get; set; }

        public PolicyBoolOptions OptEft { get; set; }

        public PolicyDateOptions OptExpDate { get; set; }

        public PolicyBoolOptions OptPaidInFullDesc { get; set; }

        public PolicyPayPlanOptions OptPayPlan { get; set; }

        public PolicyCarsOptions OptPolicyCars { get; set; }

        public PolicyDriversOptions OptPolicyDrivers { get; set; }

        public PolicyPolicyNoOptions OptPolicyNo { get; set; }

        public PolicyPolicyScoreOptions OptPolicyScore { get; set; }

        public PolicyPolicyStatusOptions OptPolicyStatus { get; set; }

        public PolicyQuoteNoOptions OptQuoteNo { get; set; }
    }

    public enum PolicyPayPlanOptions
    {
        Ignore,
        Zero,
        Neg,
        One,
        Low,
        Med,
        High,
        Min,
        Max
    }

    public enum PolicyCarsOptions
    {
        Ignore,
        Zero,
        Neg,
        One,
        Low,
        Mid,
        High
    }

    public enum PolicyDriversOptions
    {
        Ignore,
        Zero,
        Neg,
        One,
        Low,
        Med,
        High
    }

    public enum PolicyBoolOptions
    {
        Ignore,
        True,
        False
    }

    public enum PolicyDateOptions
    {
        Ignore,
        InPast,
        InFuture
    }

    public enum PolicyPolicyNoOptions
    {
        Ignore,
        Good,
        NoFlorida,
        Suffixed,
        Empty,
        Null
    }

    public enum PolicyPolicyScoreOptions
    {
        Ignore,
        Min,
        Max,
        Q1,
        Q2,
        Q3,
        Q4,
        Q5,
        Zero,
        Neg,
        Low,
        Med,
        High
    }

    public enum PolicyPolicyStatusOptions
    {
        Ignore,
        Active,
        Cancel,
        Notice,
        Voidpend,
        Other
    }

    public enum PolicyQuoteNoOptions
    {
        Ignore,
        Good,
        NoFlorida,
        Suffixed,
        Policy,
        Empty,
        Null,
        PolicySuffixed,
        NoQ
    }
}
