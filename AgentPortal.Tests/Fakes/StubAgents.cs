﻿using System;
using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace AgentPortal.Tests.Fakes
{
    /// <summary>
    /// Stub version of the Agent<see cref="AgentPortal.Support.DataObjects.Agents"/>
    /// class for use in testing.
    ///
    /// This stub version contains those properties that are so far
    /// determined to be useful in the current stage of testing.  As
    /// more are needed, they can be added.
    /// 
    /// Call GetStubAgents(id) with an identifier that matches
    /// one of the test agents in the TestAgents dictionary.
    /// The returned object is a memberwise clone of the dictionary
    /// object. The result is an object of the type StubAgents.
    ///
    /// </summary>
    public class StubAgents
    {
        /// <summary>
        /// A set of agent objects for use in testing.
        ///
        /// Each object contains various different permutations of good, bad,
        /// edge, or anomalous property values.
        ///
        /// ADD AS MANY NEW CASES HERE AS DESIRED
        /// </summary>
        private static readonly Dictionary<string, StubAgents> TestAgents = new Dictionary<string, StubAgents>()
        {
            {"Good1",new StubAgents
                {
                    AgencyName = "Rogers Peet",
                    AgentCode = "AA0000298",
                    AgentGuid = null,
                    CommissionRate = 0.15,
                    County = "Broward",
                    DBA = "Florida Insurance",
                    EmailAddress = "RayMSzeto@teleworm.us",
                    IsOwnerOperator = false,
                    IsPaperless = false,
                    MailAddress = "2737 Stratford Park",
                    MailCity = "Evansville",
                    MailState = "FL",
                    MailZIP = "33572",
                    MarketingRep = null,
                    OID = 2222,
                    Phone = "812-763-8314",
                    PhysicalAddress = "2295 Cottrill Lane",
                    PhysicalCity = "Hollywood",
                    PhysicalState = "FL",
                    PhysicalZIP = "33004",
                    PrimaryFirstName = "Ray",
                    PrimaryLastName = "Szeto",
                    Status = "OPEN",
                    StatusDate = default,
                    Website = "focusdevelop.com"
                }
            },
            {"Good2",new StubAgents
                {
                    AgencyName = "Asian Junction",
                    AgentCode = "AA0000298-02",
                    AgentGuid = "6B0E8336-5B91-4604-A69C-C6C26AA82ACA",
                    AllowLowDp = false,
                    CommissionRate = 0,
                    County = "Bay",
                    DBA = "Mass imports",
                    EmailAddress = "gregosa@safetymail.info",
                    IsOwnerOperator = false,
                    IsPaperless = true,
                    MailAddress = "1149 Hagenes Run Suite 045",
                    MailCity = "Ossian",
                    MailGarageSame = false,
                    MailState = "FL",
                    MailZIP = "32503",
                    MarketingRep = "Sam Jones",
                    OID = 5555,
                    Phone = "2606229604",
                    PhysicalAddress = "1149 Hagenes Run Suite 045",
                    PhysicalCity = "Ossian",
                    PhysicalState = "FL",
                    PhysicalZIP = "32503",
                    PrimaryFirstName = "Michael",
                    PrimaryLastName = "Wallace",
                    Status = "SUSPENDED",
                    StatusDate = default,
                    Website = "appleview.com",
                }
            }
        };

        // TODO: seems to be a hash -- find out what
        public string ABA { get; set; }

        public string Account { get; set; }

        // TODO: seems to be a hash -- find out what
        public string AccountNumber { get; set; }

        // TODO: seems to be a hash -- find out what
        public string AccountType { get; set; }

        public string AgencyName { get; set; }

        public string AgentCode { get; set; }

        public string AgentGuid { get; set; }

        public bool AllowLowDp { get; set; }

        public string AltEmailAddress { get; set; }

        public string AltPassword { get; set; }

        public string BankAccountName { get; set; }

        public string Carrier { get; set; }

        public double CommissionRate { get; set; }

        public string ComparativeRater { get; set; }

        public string County { get; set; }

        public DateTime DateLicensed { get; set; }

        public DateTime DateOpened { get; set; }

        public string DBA { get; set; }

        public string EmailAddress { get; set; }

        public string EOCity { get; set; }

        public DateTime EOEffDate { get; set; }

        public DateTime EOExpDate { get; set; }

        public string EOMailingAddress { get; set; }

        public string EONotes { get; set; }

        public string EOPolicyNo { get; set; }

        public string EOState { get; set; }

        public string EOZip { get; set; }

        public string Fax { get; set; }

        public string FEIN { get; set; }

        public string Group { get; set; }

        public string InitialContact { get; set; }

        public bool IsAllowedHigherBI { get; set; }

        public bool IsOwnerOperator { get; set; }

        public bool IsPaperless { get; set; }

        public bool IsProbation { get; set; }

        public bool IsRequireCall { get; set; }

        public string LastReview { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string MailAddress { get; set; }

        public string MailCity { get; set; }

        public bool MailGarageSame { get; set; }

        public string MailState { get; set; }

        public string MailZIP { get; set; }

        public string MarketingRep { get; set; }

        public string MarketingRepUserID { get; set; }

        public int OID { get; set; }

        public string Password { get; set; }

        public string Phone { get; set; }

        public string PhysicalAddress { get; set; }

        public string PhysicalCity { get; set; }

        public string PhysicalState { get; set; }

        public string PhysicalZIP { get; set; }

        public string PrimaryFirstName { get; set; }

        public string PrimaryLastName { get; set; }

        public string Status { get; set; }

        public DateTime StatusDate { get; set; }

        public string TerminationCause { get; set; }

        public string Website { get; set; }

        /// <summary>
        /// Returns the test agent with the given test ID
        ///
        /// Preset agents are kept in TestAgents<see cref="TestAgents"/>
        /// with each associated with a unique identifier for easy retrieval.
        ///
        /// If an agent with the given ID is not found, a default new StubAgents
        /// is returned.
        /// </summary>
        /// <param name="testId"></param>
        /// <returns></returns>
        public static StubAgents GetStubAgent(string testId)
        {
            return TestAgents.TryGetValue(testId, out var agent)
                ? (StubAgents)agent.MemberwiseClone()
                : new StubAgents();
        }

        /// <summary>
        /// Converts StubAgents object to Agents using defaults for other properties.
        /// </summary>
        /// <returns></returns>
        public Support.DataObjects.Agents ToAgents()
        {
            return new Support.DataObjects.Agents
            {
                AgencyName = this.AgencyName,
                AgentCode = this.AgentCode,
                AgentGuid = this.AgentGuid,
                CommissionRate = this.CommissionRate,
                County = this.County,
                DBA = this.DBA,
                EmailAddress = this.EmailAddress,
                IsOwnerOperator = this.IsOwnerOperator,
                IsPaperless = this.IsPaperless,
                MailAddress = this.MailAddress,
                MailCity = this.MailCity,
                MailState = this.MailState,
                MailZIP = this.MailZIP,
                MarketingRep = this.MarketingRep,
                OID = this.OID,
                Phone = this.Phone,
                PhysicalAddress = this.PhysicalAddress,
                PhysicalCity = this.PhysicalCity,
                PhysicalState = this.PhysicalState,
                PhysicalZIP = this.PhysicalZIP,
                PrimaryFirstName = this.PrimaryFirstName,
                PrimaryLastName = this.PrimaryLastName,
                Status = this.Status,
                StatusDate = this.StatusDate,
                Website = this.Website,
            };
        }
    }
}
