﻿using System.Collections.Generic;

namespace AgentPortal.Tests.Fakes
{
    public class Database
    {
      public List<StubAgents> Agents { get; set; }

        public List<StubPolicyDrivers> PolicyDrivers { get; set; }

        public List<StubAARatePayPlans> AARatePayPlans { get; set; }

        public List<StubPolicy> Policies { get; set; }

        public List<StubPolicyCars> PolicyCars { get; set; }

        public List<StubPolicyQuote> PolicyQuote { get; set; }

       // public List<VehicleModel> VehicleModels { get; set; }

    }
}
