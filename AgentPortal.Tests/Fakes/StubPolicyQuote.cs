﻿using System;
using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace AgentPortal.Tests.Fakes
{
    /// <summary>
    /// Stub version of PolicyQuote class for use in testing
    /// </summary>
    public class StubPolicyQuote
    {
        /// <summary>
        /// Test cases addressable using identifier
        /// </summary>
        private static readonly Dictionary<string, StubPolicyQuote> TestQuotes =
            new Dictionary<string, StubPolicyQuote>()
            {
                {
                    "test1", new StubPolicyQuote()
                    {
                        AdvancedQuote =true,
                        AgencyRep = null,
                        AgentCode = null,
                        AgentESignEmailAddress = "percival.hartmann@cartwright.com",
                        AppQuestionAnswers = default,
                        AppQuestionExplainOne = default,
                        AppQuestionExplainTwo = default,
                        Bridged = false,
                        Cars = 1,
                        CommAtIssue = 0.15D,
                        CommPrem = 111,
                        CreditScore = 0,
                        CvOrdered = false,
                        DateBound = DateTime.Parse("2020-01-01"),
                        DateIssued = default,
                        Drivers = 1,
                        EffDate = DateTime.Parse("2020-02-07"),
                        EFT = true,
                        ExpDate = DateTime.Parse("2021-12-31"),
                        FromRater = "ITC",
                        GarageCity = "Daytona Beach",
                        GarageCounty = "Volusia",
                        GarageState = "FL",
                        GarageStreet = "1634 Franklin Avenue",
                        GarageTerritory = default,
                        GarageZip = "32114",
                        HasADND = true,
                        HasBI = false,
                        HasLapse110 = false,
                        HasLapse1131 = false,
                        HasLapseNone = false,
                        HasMP = false,
                        HasPD = true,
                        HasPPO = false,
                        HasPriorCoverage = false,
                        HasUM = false,
                        Homeowner = false,
                        HomePhone = "386-252-9366",
                        Ins1First = "Casey",
                        Ins1FullName = "Casey H. Paul",
                        Ins1Last = "Paul",
                        Ins1MI = "H",
                        Ins1Suffix = null,
                        Ins2First = null,
                        Ins2FullName = null,
                        Ins2Last = null,
                        Ins2MI = null,
                        Ins2Suffix = null,
                        IsAllIntegrationCompleted = true,
                        IsCurrentCustomer = false,
                        IsEditedPalms = true,
                        IsNoREI = false,
                        IsOnHold = false,
                        IsReturnedMail = false,
                        IsShow = false,
                        MailCity = "Ocala",
                        MailGarageSame = false,
                        MailState = "FL",
                        MailStreet = "3787 Rhapsody Street",
                        MailZip = "34471",
                        MainEmail = "ClaytonJHall@teleworm.us",
                        MaintenanceFee = 10D,
                        MVRFee = 24,
                        NIO = false,
                        NIRR = true,
                        OID = default,
                        PaidInFullDisc = false,
                        Paperless = true,
                        PayPlan = 30,
                        PDLimit = "10000",
                        PIPDed = "1000",
                        PIPLimit = null,
                        PIPPDOnlyFee = 0D,
                        PolicyAnnualized = 0D,
                        PolicyFee = 25,
                        PolicyNo = "Q-FL0400034",
                        PolicyScore = 0,
                        PolicyWritten = 0D,
                        PreviousBI = false,
                        PreviousCompany = "Geico",
                        PreviousExpDate = DateTime.Parse("2020-04-04"),
                        PriorBILimit = null,
                        PriorPolicyNo = null,
                        PriorPolicyTerm = null,
                        QuotedAmount = 0D,
                        QuotedDate = DateTime.Parse("2021-09-10"),
                        QuoteNo = "Q-FL0375099",
                        RateCycle = "AA20200115",
                        RatingID = 0,
                        RatingReturnCode = "1",
                        RCPOSOrdered = false,
                        RenCount = 0,
                        RenDisc = false,
                        RenewalStatus = null,
                        RewriteFee = 0D,
                        ReWrittenFromPolicyNo = null,
                        SixMonth = false,
                        SR22Fee = 0,
                        State = "FL",
                        Territory = "6B",
                        TransDisc = 0,
                        WorkLoss = 1,
                        WorkPhone = "305-429-9222"
                    }
                },
                 {
                    "test2", new StubPolicyQuote()
                    {
                        AdvancedQuote =false,
                        AgencyRep = "Howard M. Mills",
                        AgentCode = "AA0000004",
                        AgentESignEmailAddress = "HowardMMills@armyspy.com",
                        AppQuestionAnswers = default,
                        AppQuestionExplainOne = default,
                        AppQuestionExplainTwo = default,
                        Bridged = true,
                        Cars = 2,
                        CommAtIssue = 0D,
                        CommPrem = 444,
                        CreditScore = 0,
                        CvOrdered = true,
                        DateBound = DateTime.Parse("2021-01-01"),
                        DateIssued = default,
                        Drivers = 2,
                        EffDate = DateTime.Parse("2021-02-07"),
                        EFT = false,
                        ExpDate = DateTime.Parse("2022-12-31"),
                        FromRater = "Vertafore",
                        GarageCity = "Boca Raton",
                        GarageCounty = "Palm Beach",
                        GarageState = "FL",
                        GarageStreet = "4948 Marigold Lane",
                        GarageTerritory = default,
                        GarageZip = "33487",
                        HasADND = false,
                        HasBI = true,
                        HasLapse110 = true,
                        HasLapse1131 = true,
                        HasLapseNone = true,
                        HasMP = true,
                        HasPD = false,
                        HasPPO = true,
                        HasPriorCoverage = true,
                        HasUM = true,
                        Homeowner = true,
                        HomePhone = "334-252-9309",
                        Ins1First = "Frank",
                        Ins1FullName = "Frank T. Thomson",
                        Ins1Last = "Thomson",
                        Ins1MI = "T",
                        Ins1Suffix = null,
                        Ins2First = null,
                        Ins2FullName = null,
                        Ins2Last = null,
                        Ins2MI = null,
                        Ins2Suffix = null,
                        IsAllIntegrationCompleted = true,
                        IsCurrentCustomer = false,
                        IsEditedPalms = true,
                        IsNoREI = false,
                        IsOnHold = false,
                        IsReturnedMail = false,
                        IsShow = false,
                        MailCity = "Cocoa",
                        MailGarageSame = true,
                        MailState = "FL",
                        MailStreet = "2528 Stoneybrook Road",
                        MailZip = "32922",
                        MainEmail = "FrankTThomson@teleworm.us",
                        MaintenanceFee = 0D,
                        MVRFee = 0,
                        NIO = true,
                        NIRR = false,
                        OID = default,
                        PaidInFullDisc = true,
                        Paperless = false,
                        PayPlan = 20,
                        PDLimit = "10000",
                        PIPDed = "250",
                        PIPLimit = "10000",
                        PIPPDOnlyFee = 10D,
                        PolicyAnnualized = 1500D,
                        PolicyFee = 25,
                        PolicyNo = "Q-FL0770034",
                        PolicyScore = 15,
                        PolicyWritten = 655D,
                        PreviousBI = true,
                        PreviousCompany = "Allstate",
                        PreviousExpDate = DateTime.Parse("2021-04-04"),
                        PriorBILimit = "30000",
                        PriorPolicyNo = null,
                        PriorPolicyTerm = "6",
                        QuotedAmount = 0D,
                        QuotedDate = DateTime.Parse("2022-11-10"),
                        QuoteNo = "Q-FL0000099",
                        RateCycle = "AA20200117",
                        RatingID = 21100,
                        RatingReturnCode = "INVALID AGENT",
                        RCPOSOrdered = true,
                        RenCount = 1,
                        RenDisc = true,
                        RenewalStatus = "",
                        RewriteFee = 15D,
                        ReWrittenFromPolicyNo = "AAFL0200094-01",
                        SixMonth = true,
                        SR22Fee = 30,
                        State = "FL",
                        Territory = "36B",
                        TransDisc = 3,
                        WorkLoss = 0,
                        WorkPhone = "904-534-5849"
                    }
                }
            };

        /// <summary>
        /// Returns a Stub object filled with the test data associated with the passed identifier
        ///
        /// If the caller would like other properties to be set to what has been observed in the
        /// existing data to be the only values contained in those fields, pass TRUE for setObvservedvalues
        /// </summary>
        /// <param name="testId"></param>
        /// <param name="setObservedValues"></param>
        /// <returns></returns>
        public static StubPolicyQuote GetStubPolicyQuote(string testId, bool setObservedValues)
        {
            var policyQuote = TestQuotes.TryGetValue(testId, out var quote) ? (StubPolicyQuote)quote.MemberwiseClone() : new StubPolicyQuote();

            if (setObservedValues)
            {
                policyQuote.ADNDAnnlPrem = 0;
                policyQuote.ADNDCommAtIssue = 0;
                policyQuote.ADNDCost = 0;
                policyQuote.ADNDLimit = 0;
                policyQuote.AgentGross = false;
                policyQuote.ByPassESignature = false;
                policyQuote.DBSetupFee = 0;
                policyQuote.DirectRepairDisc = false;
                policyQuote.EstimatedCreditScore = 0;
                policyQuote.FHCFFee = 0;
                policyQuote.HasLOU = false;
                policyQuote.HasPriorBalance = false;
                policyQuote.HasTOW = false;
                policyQuote.HoldRtn = false;
                policyQuote.HousholdIndex = 0;
                policyQuote.isAnnual = false;
                policyQuote.IsClaimMisRep = false;
                policyQuote.IsNoREI = false;
                policyQuote.IsOnHold = false;
                policyQuote.IsReturnedMail = false;
                policyQuote.IsSuspense = false;
                policyQuote.LOUAnnlPrem = 0;
                policyQuote.LOUCommAtIssue = 0;
                policyQuote.LOUCost = 0;
                policyQuote.MedPayLimit = "0";
                policyQuote.NonOwners = false;
                policyQuote.OOSEnd = false;
                policyQuote.policyCars = 0;
                policyQuote.PolicyQuoteDrivers = 0;
                policyQuote.PreviousBalance = false;
                policyQuote.QuotedAmount = 0;
                policyQuote.RentalAnnlPrem = 0;
                policyQuote.RentalCost = 0;
                policyQuote.UMStacked = false;
                policyQuote.Unacceptable = false;

                policyQuote.AgentAccount = null;
                policyQuote.AgentPassword = null;
                policyQuote.AltEmail = null;
                policyQuote.BILimit = null;
                policyQuote.CancelType = null;
                policyQuote.Carrier = null;
                policyQuote.ChangeGUID = null;
                policyQuote.CreditGuid = null;
                policyQuote.CreditMsg = null;
                policyQuote.LOB = null;
                policyQuote.NextPolicyNo = null;
                policyQuote.PolicyStatus = null;
                policyQuote.UMLimit = null;
                policyQuote.UndTier = null;
            }

            return policyQuote;

        }

        /// <summary>
        /// Returns a PolicyQuote object by memberwise cloning the stub.
        /// </summary>
        /// <returns></returns>
        public Support.DataObjects.PolicyQuote ToPolicyQuote()
        {
            var policyQuote = new Support.DataObjects.PolicyQuote()
            {
                ADNDAnnlPrem = this.ADNDAnnlPrem,
                ADNDCommAtIssue = this.ADNDCommAtIssue,
                ADNDCost = this.ADNDCost,
                ADNDLimit = this.ADNDLimit,
                AdvancedQuote = this.AdvancedQuote,
                AgencyRep = this.AgencyRep,
                AgentAccount = this.AgentAccount,
                AgentCode = this.AgentCode,
                AgentESignEmailAddress = this.AgentESignEmailAddress,
                AgentGross = this.AgentGross,
                AgentPassword = this.AgentPassword,
                AltEmail = this.AltEmail,
                AppQuestionAnswers = this.AppQuestionAnswers,
                AppQuestionExplainOne = this.AppQuestionExplainOne,
                AppQuestionExplainTwo = this.AppQuestionExplainTwo,
                BILimit = this.BILimit,
                Bridged = this.Bridged,
                ByPassESignature = this.ByPassESignature,
                CancelDate = this.CancelDate,
                CancelType = this.CancelType,
                Carrier = this.Carrier,
                Cars = this.Cars,
                ChangeGUID = this.ChangeGUID,
                CommAtIssue = this.CommAtIssue,
                CommPrem = this.CommPrem,
                CreditGuid = this.CreditGuid,
                CreditMsg = this.CreditMsg,
                CreditScore = this.CreditScore,
                CvOrdered = this.CvOrdered,
                DateBound = this.DateBound,
                DateIssued = this.DateIssued,
                DBSetupFee = this.DBSetupFee,
                DirectRepairDisc = this.DirectRepairDisc,
                Drivers = this.Drivers,
                EffDate = this.EffDate,
                EFT = this.EFT,
                EstimatedCreditScore = this.EstimatedCreditScore,
                ExpDate = this.ExpDate,
                FHCFFee = this.FHCFFee,
                FromRater = this.FromRater,
                GarageCity = this.GarageCity,
                GarageCounty = this.GarageCounty,
                GarageState = this.GarageState,
                GarageStreet = this.GarageStreet,
                GarageTerritory = this.GarageTerritory,
                GarageZip = this.GarageZip,
                HasADND = this.HasADND,
                HasBI = this.HasBI,
                HasLapse110 = this.HasLapse110,
                HasLapse1131 = this.HasLapse1131,
                HasLapseNone = this.HasLapseNone,
                HasLOU = this.HasLOU,
                HasMP = this.HasMP,
                HasPD = this.HasPD,
                HasPPO = this.HasPPO,
                HasPriorBalance = this.HasPriorBalance,
                HasPriorCoverage = this.HasPriorCoverage,
                HasTOW = this.HasTOW,
                HasUM = this.HasUM,
                HoldRtn = this.HoldRtn,
                Homeowner = this.Homeowner,
                HomePhone = this.HomePhone,
                HousholdIndex = this.HousholdIndex,
                Ins1First = this.Ins1First,
                Ins1FullName = this.Ins1FullName,
                Ins1Last = this.Ins1Last,
                Ins1MI = this.Ins1MI,
                Ins1Suffix = this.Ins1Suffix,
                Ins2First = this.Ins2First,
                Ins2FullName = this.Ins2FullName,
                Ins2Last = this.Ins2Last,
                Ins2MI = this.Ins2MI,
                Ins2Suffix = this.Ins2Suffix,
                IsAllIntegrationCompleted = this.IsAllIntegrationCompleted,
                isAnnual = this.isAnnual,
                IsClaimMisRep = this.IsClaimMisRep,
                IsCurrentCustomer = this.IsCurrentCustomer,
                IsEditedPalms = this.IsEditedPalms,
                IsNoREI = this.IsNoREI,
                IsOnHold = this.IsOnHold,
                IsReturnedMail = this.IsReturnedMail,
                IsShow = this.IsShow,
                IsSuspense = this.IsSuspense,
                LOB = this.LOB,
                LOUAnnlPrem = this.LOUAnnlPrem,
                LOUCommAtIssue = this.LOUCommAtIssue,
                LOUCost = this.LOUCost,
                MailCity = this.MailCity,
                MailGarageSame = this.MailGarageSame,
                MailState = this.MailState,
                MailStreet = this.MailStreet,
                MailZip = this.MailZip,
                MainEmail = this.MainEmail,
                MaintenanceFee = this.MaintenanceFee,
                MedPayLimit = this.MedPayLimit,
                MinDuePayDate = this.MinDuePayDate,
                MVRFee = this.MVRFee,
                NextPolicyNo = this.NextPolicyNo,
                NIO = this.NIO,
                NIRR = this.NIRR,
                NonOwners = this.NonOwners,
                OID = this.OID,
                OOSEnd = this.OOSEnd,
                PaidInFullDisc = this.PaidInFullDisc,
                Paperless = this.Paperless,
                PayPlan = this.PayPlan,
                PDLimit = this.PDLimit,
                PIPDed = this.PIPDed,
                PIPLimit = this.PIPLimit,
                PIPPDOnlyFee = this.PIPPDOnlyFee,
                PolicyAnnualized = this.PolicyAnnualized,
                policyCars = this.policyCars,
                PolicyFee = this.PolicyFee,
                PolicyNo = this.PolicyNo,
                PolicyQuoteDrivers = this.PolicyQuoteDrivers,
                PolicyScore = this.PolicyScore,
                PolicyStatus = this.PolicyStatus,
                PolicyWritten = this.PolicyWritten,
                PreviousBalance = this.PreviousBalance,
                PreviousBI = this.PreviousBI,
                PreviousCompany = this.PreviousCompany,
                PreviousExpDate = this.PreviousExpDate,
                PriorBILimit = this.PriorBILimit,
                PriorPolicyNo = this.PriorPolicyNo,
                PriorPolicyTerm = this.PriorPolicyTerm,
                QuotedAmount = this.QuotedAmount,
                QuotedDate = this.QuotedDate,
                QuoteNo = this.QuoteNo,
                RateCycle = this.RateCycle,
                RatingID = this.RatingID,
                RatingReturnCode = this.RatingReturnCode,
                RCPOSOrdered = this.RCPOSOrdered,
                RenCount = this.RenCount,
                RenDisc = this.RenDisc,
                RenewalStatus = this.RenewalStatus,
                RentalAnnlPrem = this.RentalAnnlPrem,
                RentalCost = this.RentalCost,
                RewriteFee = this.RewriteFee,
                ReWrittenFromPolicyNo = this.ReWrittenFromPolicyNo,
                SixMonth = this.SixMonth,
                SR22Fee = this.SR22Fee,
                State = this.State,
                Territory = this.Territory,
                TransDisc = this.TransDisc,
                UMLimit = this.UMLimit,
                UMStacked = this.UMStacked,
                Unacceptable = this.Unacceptable,
                UndTier = this.UndTier,
                WorkLoss = this.WorkLoss,
                WorkPhone = this.WorkPhone
            };
            return policyQuote;
        }

        public int ADNDAnnlPrem { get; set; }
        public double ADNDCommAtIssue { get; set; }
        public int ADNDCost { get; set; }
        public int ADNDLimit { get; set; }
        public bool AdvancedQuote { get; set; }
        public string AgencyRep { get; set; }
        public string AgentAccount { get; set; }
        public string AgentCode { get; set; }
        public string AgentESignEmailAddress { get; set; }
        public bool AgentGross { get; set; }
        public string AgentPassword { get; set; }
        public string AltEmail { get; set; }
        public string AppQuestionAnswers { get; set; }
        public string AppQuestionExplainOne { get; set; }
        public string AppQuestionExplainTwo { get; set; }
        public string BILimit { get; set; }
        public bool Bridged { get; set; }
        public bool ByPassESignature { get; set; }
        public DateTime CancelDate { get; set; }
        public string CancelType { get; set; }
        public string Carrier { get; set; }
        public int Cars { get; set; }
        public string ChangeGUID { get; set; }
        public double CommAtIssue { get; set; }
        public int CommPrem { get; set; }
        public string CreditGuid { get; set; }
        public string CreditMsg { get; set; }
        public int CreditScore { get; set; }
        public bool CvOrdered { get; set; }
        public DateTime DateBound { get; set; }
        public DateTime DateIssued { get; set; }
        public int DBSetupFee { get; set; }
        public bool DirectRepairDisc { get; set; }
        public int Drivers { get; set; }
        public DateTime EffDate { get; set; }
        public bool EFT { get; set; }
        public int EstimatedCreditScore { get; set; }
        public DateTime ExpDate { get; set; }
        public double FHCFFee { get; set; }
        public string FromRater { get; set; }
        public string GarageCity { get; set; }
        public string GarageCounty { get; set; }
        public string GarageState { get; set; }
        public string GarageStreet { get; set; }
        public string GarageTerritory { get; set; }
        public string GarageZip { get; set; }
        public bool HasADND { get; set; }
        public bool HasBI { get; set; }
        public bool HasLapse110 { get; set; }
        public bool HasLapse1131 { get; set; }
        public bool HasLapseNone { get; set; }
        public bool HasLOU { get; set; }
        public bool HasMP { get; set; }
        public bool HasPD { get; set; }
        public bool HasPPO { get; set; }
        public bool HasPriorBalance { get; set; }
        public bool HasPriorCoverage { get; set; }
        public bool HasTOW { get; set; }
        public bool HasUM { get; set; }
        public bool HoldRtn { get; set; }
        public bool Homeowner { get; set; }
        public string HomePhone { get; set; }
        public int HousholdIndex { get; set; }
        public string Ins1First { get; set; }
        public string Ins1FullName { get; set; }
        public string Ins1Last { get; set; }
        public string Ins1MI { get; set; }
        public string Ins1Suffix { get; set; }
        public string Ins2First { get; set; }
        public string Ins2FullName { get; set; }
        public string Ins2Last { get; set; }
        public string Ins2MI { get; set; }
        public string Ins2Suffix { get; set; }
        public bool IsAllIntegrationCompleted { get; set; }
        public bool isAnnual { get; set; }
        public bool IsClaimMisRep { get; set; }
        public bool IsCurrentCustomer { get; set; }
        public bool IsEditedPalms { get; set; }
        public bool IsNoREI { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsReturnedMail { get; set; }
        public bool IsShow { get; set; }
        public bool IsSuspense { get; set; }
        public string LOB { get; set; }
        public int LOUAnnlPrem { get; set; }
        public double LOUCommAtIssue { get; set; }
        public int LOUCost { get; set; }
        public string MailCity { get; set; }
        public bool MailGarageSame { get; set; }
        public string MailState { get; set; }
        public string MailStreet { get; set; }
        public string MailZip { get; set; }
        public string MainEmail { get; set; }
        public double MaintenanceFee { get; set; }
        public string MedPayLimit { get; set; }
        public DateTime MinDuePayDate { get; set; }
        public int MVRFee { get; set; }
        public string NextPolicyNo { get; set; }
        public bool NIO { get; set; }
        public bool NIRR { get; set; }
        public bool NonOwners { get; set; }
        public int OID { get; set; }
        public bool OOSEnd { get; set; }
        public bool PaidInFullDisc { get; set; }
        public bool Paperless { get; set; }
        public int PayPlan { get; set; }
        public string PDLimit { get; set; }
        public string PIPDed { get; set; }
        public string PIPLimit { get; set; }
        public double PIPPDOnlyFee { get; set; }
        public double PolicyAnnualized { get; set; }
        public int policyCars { get; set; }
        public int PolicyFee { get; set; }
        public string PolicyNo { get; set; }
        public int PolicyQuoteDrivers { get; set; }
        public int PolicyScore { get; set; }
        public string PolicyStatus { get; set; }
        public double PolicyWritten { get; set; }
        public bool PreviousBalance { get; set; }
        public bool PreviousBI { get; set; }
        public string PreviousCompany { get; set; }
        public DateTime PreviousExpDate { get; set; }
        public string PriorBILimit { get; set; }
        public string PriorPolicyNo { get; set; }
        public string PriorPolicyTerm { get; set; }
        public double QuotedAmount { get; set; }
        public DateTime QuotedDate { get; set; }
        public string QuoteNo { get; set; }
        public string RateCycle { get; set; }
        public int RatingID { get; set; }
        public string RatingReturnCode { get; set; }
        public bool RCPOSOrdered { get; set; }
        public int RenCount { get; set; }
        public bool RenDisc { get; set; }
        public string RenewalStatus { get; set; }
        public double RentalAnnlPrem { get; set; }
        public double RentalCost { get; set; }
        public double RewriteFee { get; set; }
        public string ReWrittenFromPolicyNo { get; set; }
        public bool SixMonth { get; set; }
        public int SR22Fee { get; set; }
        public string State { get; set; }
        public string Territory { get; set; }
        public int TransDisc { get; set; }
        public string UMLimit { get; set; }
        public bool UMStacked { get; set; }
        public bool Unacceptable { get; set; }
        public string UndTier { get; set; }
        public int WorkLoss { get; set; }
        public string WorkPhone { get; set; }
    }
}