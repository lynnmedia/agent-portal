﻿using System;
using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace AgentPortal.Tests.Fakes
{
    public class StubAARatePayPlans
    {
        private static readonly Dictionary<string, StubAARatePayPlans> TestPaymentPlans =
            new Dictionary<string, StubAARatePayPlans>()
            {
                {"test1",new StubAARatePayPlans()
                {
                    BI = 0D,
                    BillCycle = 30D,
                    Code = "6-Pay EFT",
                    DateCreated = DateTime.Parse("2021-04-11"),
                    DP_AsPerc = 0.26D,
                    DP_FromPDF = "26%",
                    EFT = 1D,
                    FirstDue = 20D,
                    IndexID = 10,
                    Installments = 5D,
                    NewOrRen = "N",
                    RateCycle = "AA20201007",
                    RenQuote = "19",
                    RequiresTerritoryApproval = false,
                    State = "FL",
                    Term = 6D,
                }},
                {"test2",new StubAARatePayPlans()
                {
                    BI = 1D,
                    BillCycle = 0D,
                    Code = "FULL",
                    DateCreated = DateTime.Parse("2019-05-10"),
                    DP_AsPerc = 1D,
                    DP_FromPDF = "100%",
                    EFT = 0D,
                    FirstDue = 0D,
                    IndexID = 1,
                    Installments = 0D,
                    NewOrRen = "N",
                    RateCycle = "AA20205007",
                    RenQuote = "5",
                    RequiresTerritoryApproval = false,
                    State = "FL",
                    Term = 6D,
                }},
                {"test3",new StubAARatePayPlans()
                {
                    BI = 0D,
                    BillCycle = 30D,
                    Code = "5-Pay",
                    DateCreated = DateTime.Parse("2021-04-11"),
                    DP_AsPerc = 0.3D,
                    DP_FromPDF = "30%",
                    EFT = 0D,
                    FirstDue = 30D,
                    IndexID = 110,
                    Installments = 5D,
                    NewOrRen = "N",
                    RateCycle = "AA20201007-01",
                    RenQuote = "15",
                    RequiresTerritoryApproval = true,
                    State = "FL",
                    Term = 6D,
                }}
            };

        public double BI { get; set; }
        public double BillCycle { get; set; }
        public string Code { get; set; }
        public DateTime DateCreated { get; set; }
        public double DP_AsPerc { get; set; }
        public string DP_FromPDF { get; set; }
        public double EFT { get; set; }
        public string ExtraDesc { get; set; }
        public double FirstDue { get; set; }
        public int IndexID { get; set; }
        public double Installments { get; set; }
        public string NewOrRen { get; set; }
        public double PageNo { get; set; }
        public string PDFFileName { get; set; }
        public string RateCycle { get; set; }
        public string RenQuote { get; set; }
        public bool RequiresTerritoryApproval { get; set; }
        public string State { get; set; }
        public double Term { get; set; }

        public static StubAARatePayPlans GetStubAARatePayPlan(string testId)
        {
            return TestPaymentPlans.TryGetValue(testId, out var car) ? (StubAARatePayPlans)car.MemberwiseClone() : new StubAARatePayPlans();
        }

    }
}