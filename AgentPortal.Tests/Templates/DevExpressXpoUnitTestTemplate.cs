﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac.Extras.Moq;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.DB.Helpers;
using AgentPortal.Repositories;
using AgentPortal.Services;
using AlertAuto.Integrations.Contracts.CardConnect;
using Moq;

namespace AgentPortal.Tests.Templates
{
    /// <summary>
    /// DevExpress Template for XPO unit tests
    /// </summary>
    [Ignore("Template")]
    [TestClass]
    public class DevExpressXpoUnitTestTemplate
    {
        protected IDisposable[] disposablesOnDisconect;
        protected IDataLayer dataLayer;
        protected virtual IDataStore CreateProvider()
        {
            return new InMemoryDataStore(AutoCreateOption.DatabaseAndSchema);
        }
        [TestInitialize()]
        public virtual void SetUp()
        {
            IDataStore ds = CreateProvider();
            ((IDataStoreForTests)ds).ClearDatabase();
            dataLayer = new SimpleDataLayer(ds);
        }
        [TestCleanup()]
        public virtual void TearDown()
        {
            dataLayer.Dispose();
            if (disposablesOnDisconect != null)
            {
                foreach (IDisposable disp in disposablesOnDisconect)
                {
                    disp.Dispose();
                }
            }
        }

        [TestMethod]
        public void Template_ReturnsTrue()
        { // TUORIAL : https://autofac.readthedocs.io/en/latest/integration/moq.html
            using (UnitOfWork uow = new UnitOfWork(dataLayer))
            {
                using (var mock = AutoMock.GetLoose())
                {
                    // Arrange - configure the mock
                    mock.Mock<ITestService>().Setup(x => x.TestMethod(It.IsAny<string>())).Returns("success");
                    var sut = mock.Create<TestWrapper>();

                    // Act
                    var actual = sut.Test("string");

                    // Assert - assert on the mock
                    mock.Mock<ITestService>().Verify(x => x.TestMethod("string"));
                    Assert.AreEqual(actual, "success");
                }
            }
        }
    }

    public class MSSqlTest : DevExpressXpoUnitTestTemplate
    {
        protected override DevExpress.Xpo.DB.IDataStore CreateProvider()
        {
            string cs = MSSqlConnectionProvider.GetConnectionString("localhost", "XPOTests");
            return XpoDefault.GetConnectionProvider(cs, AutoCreateOption.DatabaseAndSchema, out disposablesOnDisconect);
        }
    }
}
