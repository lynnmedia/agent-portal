﻿using System;
using System.IO;
using AgentPortal.Tests.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace AgentPortal.Tests.Templates
{
    [Ignore("Template")]
    [TestClass]
    public class StubExerciserTemplate
    {
        private const string LogPath = @"C:/Users/deveb/temp/logfile_testcases.txt";

        private static readonly JsonSerializerSettings DebugJsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new Helpers.OrderedContractResolver(),
            NullValueHandling = NullValueHandling.Ignore
        };

        [TestMethod]
        public void TestMethod1()
        {
            const string testCase = "test1";
            const bool setObservedValues = false;

            var stubObject = StubPolicyQuote.GetStubPolicyQuote(testCase, setObservedValues);
            var actualObject = stubObject.ToPolicyQuote(); // won't serialize, but can view in debugger

            var jsonstr = JsonConvert.SerializeObject(stubObject, Formatting.Indented, DebugJsonSettings);
            MLog(jsonstr);
        }

        private static void MLog(string msg)
        {
            File.AppendAllText(LogPath, $"{msg}{Environment.NewLine}");
        }
    }
}