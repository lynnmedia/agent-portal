﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using AgentPortal.Repositories;
using AgentPortal.Repositories.StoredProcedures;

namespace AgentPortal.Tests
{
    /// <summary>
    /// Tests for the migrated versions of user-defined functions
    /// in the database which do not require database access
    /// </summary>
    [TestClass]
    public class NonDbFunctionTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestDateValueNoTimestamp()
        {
            // Arrange
            DateTime src = new DateTime(2015, 12, 31, 5, 10, 20);
            DateTime expected = new DateTime(2015, 12, 31);

            // Act
            var result = NonDbFunctions.DateValueNoTimestamp(src);

            // Assert
            Assert.AreEqual(expected,result);

        }

        /// <summary>
        /// Tests the function GetMaxBillingFee -- migrated from fnGetMaxBillingFee
        /// 
        /// A preliminary test to check if the migrated function's results
        /// match the database's given the same inputs.
        /// 
        /// A set of eight runs are defined with emphasis on the 
        /// treatment of numInstallments when null, 0, 1, other
        /// </summary>
        /// <param name="policyTotalStr"></param>
        /// <param name="downPaymentStr"></param>
        /// <param name="numInstallments"></param>
        /// <param name="policyTerm"></param>
        /// <param name="expectedStr"></param>
        [DataRow("100000", "2000",null,24,"8820")]
        [DataRow("100000", "2000", 0, 24, "8820")]
        [DataRow("100000", "2000", 1, 24, "8820")]
        [DataRow("100000", "2000", 2, 24, "7350")]
        [DataRow("100000", "2000", null, 12, "17640")]
        [DataRow("100000", "2000", 0, 12, "17640")]
        [DataRow("100000", "2000", 1, 12, "17640")]
        [DataRow("100000", "2000", 2, 12, "14700")]
        [DataTestMethod]
        public void TestGetMaxBillingFee_Migrated(string policyTotalStr, string downPaymentStr, int? numInstallments, int policyTerm, string expectedStr)
        {
            // Arrange
            decimal policyTotal = Decimal.Parse(policyTotalStr);
            decimal downPayment = Decimal.Parse(downPaymentStr);
            decimal expected = Decimal.Parse(expectedStr);

            // Act
            var result = NonDbFunctions.GetMaxBillingFee( policyTotal,  downPayment,   numInstallments,  policyTerm);

            // Assert
            Assert.AreEqual(expected, result);
        }

        /// <summary>
        /// Test migration of user-defined db function edit_distance()
        ///     to Stored Procedures/Functions: EditDistance()
        /// </summary>
        /// <param name="a">string to compare</param>
        /// <param name="b">string to compare</param>
        /// <param name="expected"></param>
        [DataRow("ABC", "ABC", 0)]
        [DataRow("ABC", "ABC ", 0)]
        [DataRow("ABC", "ADCE ", 2)]
        [DataRow("John Smith", "John Smythe", 2)]
        [DataRow("John Smith", "Jon Smith", 1)]
        [DataRow("John Smith", "Bill Jones", 9)]
        [DataTestMethod]
        public void TestEditDistance_Migrated(string a, string b, int expected)
        {
            // Arrange


            // Act
            var result = NonDbFunctions.EditDistance(a, b);

            // Assert
            Assert.AreEqual(expected, result);
        }

        [DataRow("1", "","1", "1", "1")]
        [DataRow("12", "", "12", "12", "12")]
        [DataRow("123", "", "123", "123", "123")]
        [DataRow("1234", "", "1234", "1234", "1234")]
        [DataRow("12345", "", "12345", "1-2345", "12345")]
        [DataRow("123456", "", "123456", "12-3456", "123456")]
        [DataRow("1234567", "", "1234567", "123-4567", "1234567")]
        [DataRow("12345678", "1", "2345678", "1-234-5678", "12345678")]
        [DataRow("123456789", "12", "3456789", "12-345-6789", "123456789")]
        [DataRow("1234567890", "123", "4567890", "123-456-7890", "1234567890")]
        [DataRow("12345678909", "234", "5678909", "234-567-8909", "2345678909")]
        [DataRow("123456789098", "345", "6789098", "345-678-9098", "3456789098")]
        [DataRow("1234567890987", "456", "7890987", "456-789-0987", "4567890987")]
        [DataRow("12345678909876", "456", "78909876", "456-7890-9876", "12345678909876")]
        [DataRow("123456789098765", "4567", "89098765", "4567-8909-8765", "123456789098765")]
        [DataRow("1234567890987654", "5678", "90987654", "5678-9098-7654", "1234567890987654")]
        [DataRow("123-456-7890", "123", "4567890", "123-456-7890", "1234567890")]
        [DataRow("1-234-567-8909", "234", "5678909", "234-567-8909", "2345678909")]
        [DataRow("12-345-678-9098", "345", "6789098", "345-678-9098", "3456789098")]
        [DataRow("123-456-789-0987", "456", "7890987", "456-789-0987", "4567890987")]
        [DataRow("123-456-7890-9876", "456", "78909876", "456-7890-9876", "12345678909876")]
        [DataRow("123-4567-8909-8765", "4567", "89098765", "4567-8909-8765", "123456789098765")]
        [DataRow("1234-5678-9098-7654", "5678", "90987654", "5678-9098-7654", "1234567890987654")]
        [DataTestMethod]
        public void TestPhonePart_Migrated(string phoneNumber, string expectedArea, string expectedLocal, string expectedFormatted,string expectedNumbersOnly)
        {
            // Act
            var area = NonDbFunctions.PhonePart("area", phoneNumber);
            var local = NonDbFunctions.PhonePart("local", phoneNumber);
            var formatted = NonDbFunctions.PhonePart("formatted", phoneNumber);
            var numbersonly = NonDbFunctions.PhonePart("numbersonly", phoneNumber);

            // Assert
            Assert.AreEqual(expectedArea, area,"Area");
            Assert.AreEqual(expectedLocal, local,"Local");
            Assert.AreEqual(expectedFormatted, formatted,"Formatted");
            Assert.AreEqual(expectedNumbersOnly, numbersonly,"NumbersOnly");
        }

        [DataRow("123-456-7890x321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890x 321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 x321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 x 321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890ext321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890ext 321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 ext321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 ext 321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 X 321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 EXT 321", "123", "4567890", "123-456-7890 ext: 321", "1234567890", "321")]
        [DataRow("123-456-7890 x3", "123", "4567890", "123-456-7890 ext: 3", "1234567890", "3")]
        [DataRow("123-456-7890 x", "123", "4567890", "123-456-7890", "1234567890", "")]
        [DataTestMethod]
        public void TestPhonePart_Ext(string phoneNumber, string expectedArea, string expectedLocal, string expectedFormatted, string expectedNumbersOnly, string expectedExt)
        {
            // Act
            var area = NonDbFunctions.PhonePart("area", phoneNumber);
            var local = NonDbFunctions.PhonePart("local", phoneNumber);
            var formatted = NonDbFunctions.PhonePart("formatted", phoneNumber);
            var numbersonly = NonDbFunctions.PhonePart("numbersonly", phoneNumber);
            var ext = NonDbFunctions.PhonePart("ext", phoneNumber);

            // Assert
            Assert.AreEqual(expectedArea, area, "Area");
            Assert.AreEqual(expectedLocal, local, "Local");
            Assert.AreEqual(expectedFormatted, formatted, "Formatted");
            Assert.AreEqual(expectedNumbersOnly, numbersonly, "NumbersOnly");
            Assert.AreEqual(expectedExt, ext, "Ext");
        }

    }

}
