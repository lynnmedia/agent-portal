﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using AgentPortal.Models;
using AgentPortal.Repositories;
using AgentPortal.Services;
using AgentPortal.Support.DataObjects;
using AgentPortal.Tests.Templates;
using AlertAuto.Integrations.Contracts.CardConnect;
using Autofac.Extras.Moq;
using DevExpress.Xpo;
using Moq;

namespace AgentPortal.Tests
{
    /// <summary>
    /// Summary description for PaymentTests
    /// </summary>
    [TestClass]
    public class PaymentTests : DevExpressXpoUnitTestTemplate
    {
        [TestMethod]
        public void ProcessPayment_OnApproved_ReturnsSuccess()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "1234",
                    Cvv = "123",
                    ExpirationDate = "01/2021",
                    CreditCardFee = 0,
                    Amount = Convert.ToDecimal(1),
                    PolicyNo = "policyNo"
                };

                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false)).Returns(new CardConnectResponse
                {
                    respstat = ResponseCodes.APPROVED
                });
                var paymentService = mock.Create<PaymentService>();

                // Act
                var actual = paymentService.ProcessPayment(stub, false);

                // Assert
                mock.Mock<IIntegrationsClient>().Verify(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false));
                Assert.AreEqual(true, actual.Success);
            }
        }

        [TestMethod]
        public void ProcessPayment_OnAccountNumberNonNumeric_ThrowsException()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "account",
                    Cvv = "123",
                    ExpirationDate = "01/2021",
                    CreditCardFee = 0,
                    Amount = Convert.ToDecimal(0),
                    PolicyNo = "policyNo"
                };

                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false)).Returns(new CardConnectResponse
                {
                    respstat = ResponseCodes.APPROVED
                });
                var paymentService = mock.Create<PaymentService>();

                // Assert
                var ex = Assert.ThrowsException<InvalidOperationException>(() => paymentService.ProcessPayment(stub,false));
            }
        }

        
        [TestMethod]
        public void ProcessPayment_OnCVVNonNumeric_ThrowsException()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "1234",
                    Cvv = "cvv",
                    ExpirationDate = "01/2021",
                    CreditCardFee = 0,
                    Amount = Convert.ToDecimal(0),
                    PolicyNo = "policyNo"
                };
                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false))
                    .Returns(new CardConnectResponse { respstat = ResponseCodes.APPROVED });
                var paymentService = mock.Create<PaymentService>();

                // Assert
                var ex = Assert.ThrowsException<InvalidOperationException>(() => paymentService.ProcessPayment(stub,false));
            }
        }

        [TestMethod]
        public void ProcessPayment_OnAmountEqualOrLessThanZero_ThrowsException()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "1234",
                    Cvv = "123",
                    ExpirationDate = "01/22",
                    CreditCardFee = 0,
                    Amount = Convert.ToDecimal(0),
                    PolicyNo = "policyNo"
                };
                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false))
                    .Returns(new CardConnectResponse { respstat = ResponseCodes.APPROVED });
                var paymentService = mock.Create<PaymentService>();

                // Assert
                var ex = Assert.ThrowsException<InvalidOperationException>(() => paymentService.ProcessPayment(stub,false));
            }
        }

        [TestMethod]
        public void ProcessPayment_OnExpirationNonDate_ThrowsException()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "1234",
                    Cvv = "123",
                    ExpirationDate = "expiration",
                    CreditCardFee = 0,
                    Amount = Convert.ToDecimal(0),
                    PolicyNo = "policyNo"
                };
                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false))
                    .Returns(new CardConnectResponse { respstat = ResponseCodes.APPROVED });
                var paymentService = mock.Create<PaymentService>();

                // Assert
                var ex = Assert.ThrowsException<InvalidOperationException>(() => paymentService.ProcessPayment(stub,false));
            }
        }

        [TestMethod]
        [DataRow(50.00,5.00)]
        public void ProcessPayment_CreditCard_FeeIsApplied(double amount, double fee)
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "1234",
                    Cvv = "123",
                    ExpirationDate = "12/2021",
                    CreditCardFee = Convert.ToDecimal(fee),
                    Amount = Convert.ToDecimal(amount),
                    PolicyNo = "policyNo"
                };

                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false)).Returns(new CardConnectResponse
                {
                    respstat = ResponseCodes.APPROVED
                });
                var paymentService = mock.Create<PaymentService>();

                // Act
                var actual = paymentService.ProcessPayment(stub,false);

                // Assert
                mock.Mock<IIntegrationsClient>().Verify(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false));
                Assert.AreEqual((amount + fee).ToString(), actual.Amount);
            }
        }

        [TestMethod]
        public void ProcessPayment_OnDecline_ReturnsFailureAndReason()
        {
            using (var mock = AutoMock.GetLoose())
            {
                // Arrange
                var stub = new CardConnectPaymentModel
                {
                    IsCreditCard = true,
                    IsInitialDepostPayment = true,
                    Account = "1234",
                    Cvv = "123",
                    ExpirationDate = "01/2021",
                    CreditCardFee = Convert.ToDecimal(0),
                    Amount = Convert.ToDecimal(1),
                    PolicyNo = "policyNo"
                };
                var mockFailureReason = "failure reason";

                mock.Mock<IIntegrationsClient>().Setup(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false)).Returns(new CardConnectResponse
                {
                    respstat = ResponseCodes.DECLIND,
                    resptext = mockFailureReason
                });
                var paymentService = mock.Create<PaymentService>();

                // Act
                var actual = paymentService.ProcessPayment(stub,false);

                // Assert
                mock.Mock<IIntegrationsClient>().Verify(x => x.AuthorizeWithCardConnect(stub.PolicyNo, It.IsAny<CardConnectRequest>(), false));
                Assert.AreEqual(false, actual.Success);
                Assert.AreEqual(mockFailureReason, actual.FailureReason);
            }
        }
    }
}
