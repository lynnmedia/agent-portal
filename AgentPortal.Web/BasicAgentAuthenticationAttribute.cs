﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal
{
    public class BasicAgentAuthenticationAttribute : ActionFilterAttribute
    {
        public Blowfish Blowfish = new Blowfish(Constants.blowfishKey);

        public BasicAgentAuthenticationAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                var req = filterContext.HttpContext.Request;
                var auth = req.Headers["Authorization"];
                if (!String.IsNullOrEmpty(auth))
                {
                    var credentialBytes = Convert.FromBase64String(auth.Substring(6));
                    var credentials = Encoding.UTF8.GetString(credentialBytes).Split(':');
                    var username = credentials[0];
                    var password = credentials[1];
                    AgentLogins agentLogin =
                        uow.FindObject<AgentLogins>(CriteriaOperator.Parse("Username = ?", username));
                    if (agentLogin != null)
                    {
                        string hashedpass = agentLogin.Password;
                        string decryptedPass = Blowfish.Decrypt_CBC(hashedpass);
                        if (decryptedPass.Equals(password))
                        {
                            Sessions.Instance.AgentCode = agentLogin.AgentCode;
                            Sessions.Instance.AgencyName = GetAgencyName(agentLogin.AgentCode);
                            Sessions.Instance.AgentName = GetAgentName(agentLogin.AgentCode);
                            Sessions.Instance.Username = AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode);
                            Sessions.Instance.IsAuth = true;
                            Sessions.Instance.IsAgent = true;
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("Username {0} has logged in", Sessions.Instance.Username), "INFO");
                            return;
                        }
                    }
                }

                //UnAuthorized
                filterContext.HttpContext.Response.AddHeader("WWW-Authenticate",
                    String.Format("Basic realm=\"{0}\"", "PalmInsure"));
                Sessions.Instance.IsAuth = false;
                filterContext.Result = new RedirectResult("~/Agent/Login");

            }
        }

        private string GetAgencyName(string code)
        {
            string agencyname = null;

            try
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    agencyname = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", code))
                        .FirstOrDefault().AgencyName;
                }
            }
            catch
            {
                agencyname = "";
            }

            return agencyname;
        }

        private string GetAgentName(string code)
        {
            string agentname = null;
            try
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    var agentDetails = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", code))
                        .FirstOrDefault();
                    if (agentDetails != null)
                    {
                        agentname = $"{agentDetails.PrimaryFirstName} {agentDetails.PrimaryLastName}";
                    }
                }
            }
            catch
            {
                agentname = "";
            }

            return agentname;
        }
    }
}