﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class DocumentsModel
    {
        public bool IsTest { get; set; }

        public string PolicyNo { get; set; }

        public bool ShowEftProfileMissingMsg { get; set; }

        public bool IsEftPolicy { get; set; }
    }
}