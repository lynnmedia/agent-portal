﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class PolicyEndorseQuoteViolationModel
    {
        public string Name { get; set; }

        public string ViolationDate { get; set; }


        public int ViolationPoints { get; set; }

    }
}