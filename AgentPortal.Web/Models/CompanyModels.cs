﻿using System;
using System.ComponentModel.DataAnnotations;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using DevExpress.Xpo;
using PalmInsure.Palms.Xpo;

namespace AgentPortal.Models
{
    public class IISLogSearchModel
    {
        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; }

    }//END IISLogSearchModel
    //////////////////////////////////////////////////////////////////////////////////////////

    public class IISLogDataModel
    {
        public string IPAddress { get; set; }
        public DateTime Date { get; set; }
        public int NumOfHits { get; set; }
    }//END IISLogDataModel
    //////////////////////////////////////////////////////////////////////////////////////////

    public class VinLookupModel
    {
        public string VinNo { get; set; }
        public string Increment { get; set; }
        public int VinID { get; set; }
        public string xxYear { get; set; }
        public string ChangeIndicator { get; set; }
        public string Effective { get; set; }
        public string StateException { get; set; }
        public string Manufacturer { get; set; }
        public string FullName { get; set; }
        public string AbbrModelName { get; set; }
        public string BodyStyle { get; set; }
        public string EngineSize { get; set; }
        public string NoCylinders { get; set; }
        public bool FourWheelDrive { get; set; }
        public bool RestraintInfo { get; set; }
        public string Transmission { get; set; }
        public bool AntiLockBrake { get; set; }
        public string DRL { get; set; }
        public string EngineType { get; set; }
        public string ISOSymbol { get; set; }
        public string NonISOSymbol { get; set; }
        public string MiscBodyInfo { get; set; }
        public string MiscEngineInfo { get; set; }
        public string MiscRestraintInfo { get; set; }
        public string FullModelName { get; set; }
        public string AntiTheft { get; set; }
        public string Eligible { get; set; }
        public int VSRSymbol { get; set; }
        public int PriceNewSymbol { get; set; }
        public string BISymbol { get; set; }
        public string PDSymbol { get; set; }
        public string COMPSymbol { get; set; }
        public string COLLSymbol { get; set; }
        public string UMSymbol { get; set; }
        public string MPSymbol { get; set; }
        public int IDMIVehNo { get; set; }
        public int IndexID { get; set; }

        public string SymbolVehYear { get; set; }

    }
    //////////////////////////////////////////////////////////////////////////////////////////
    
    public class MarketingMapModel
    {
        public UnitOfWork uow = XpoHelper.GetNewUnitOfWork();

        public XPCollection<AgentGoogleSource> SeminoleAgents { get; set; }
        public XPCollection<AgentGoogleSource> IbGreenAgents { get; set; }
        public XPCollection<AgentGoogleSource> AccuAutoAgents { get; set; }
        public XPCollection<AgentGoogleSource> FraudClinics { get; set; }
        
        public XPCollection<XpoAgents> Agents { get; set; }
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    
}//END Namespace