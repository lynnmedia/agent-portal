﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class EftAccountUpdateModel
    {
        [Required]
        public string Account { get; set; }

        public string ExpirationDate { get; set; }

        public bool IsInitialDepostPayment { get; set; }

        public bool IsCreditCard { get; set; }

        public bool IsAch { get; set; }

        public string AchAcocuntType { get; set; }

        public bool IsAgentAccountSweep { get; set; }

        public string FutureEFTPayType { get; set; }

        [Range(0, 9999999999999999.99)]
        public decimal Amount { get; set; }

        public decimal CreditCardFee { get; set; }

        public string Cvv { get; set; }

        public bool IsEft { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }

        public string PolicyNo { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Postal { get; set; }
        public string ExtractLast4OfAccount()
        {
            if (string.IsNullOrEmpty(Account))
            {
                return string.Empty;
            }

            return Account.Substring(Account.Length - 4, 4);
        }

        [NotMapped]
        public string IFramePort { get; set; }
    }
}