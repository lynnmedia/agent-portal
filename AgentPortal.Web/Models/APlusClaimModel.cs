﻿using System;

namespace AgentPortal.Models
{
    public class APlusClaimModel
    {
        public string DriverName { get; set; }

        public string DriverIndex { get; set; }

        public DateTime DateLicensed { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string ClaimDescription { get; set; }

        public string ClaimDateOfLoss { get; set; }

        public bool InsuredAtFault { get; set; }

        public int AAPoints { get; set; }

        public string APlusCarrierClaimNumber { get; set; }
    }
}