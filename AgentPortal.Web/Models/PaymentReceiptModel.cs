﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class PaymentReceiptModel
    {
        public string PolicyNo { get; set; }

        public string Link { get; set; }

        public bool IsClientReceipt { get; set; }
    }
   
    public class ReinstatementModel
    {
        public string PolicyNo { get; set; }

        public string PayLink { get; set; }

        public string ReinstatementLink { get; set; }

        public string ConcatPdfFileLink { get; set; }

        public bool IsAgentSweep { get; set; }
    }

}