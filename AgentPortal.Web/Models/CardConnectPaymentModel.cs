﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AgentPortal.Services;

namespace AgentPortal.Models
{
    public class CardConnectPaymentModel
    {
        public string PolicyNo { get; set; }

        [Required]
        public string Account { get; set; }

        public string ExpirationDate { get; set; }

        public bool IsInitialDepostPayment { get; set; }

        public bool IsEndorsementPayment { get; set; }

        public bool IsTitanEndorsementPayment { get; set; }
        
        public bool IsRenewalPayment { get; set; }

        public bool IsClientPayment { get; set; }

        public DateTime? EndorsementEffectiveDate { get; set; }

        public bool IsCreditCard { get; set; }

        public bool IsAch { get; set; }

        public string AchAcocuntType { get; set; }

        public bool IsAgentAccountSweep { get; set; }

        public string FutureEFTPayType { get; set; }

        [Range(0, 9999999999999999.99)]
        public decimal Amount { get; set; }

        public decimal CreditCardFee { get; set; }

        public string Cvv { get; set; }

        public bool IsEft { get; set; }

        public bool IsPayForReinstatement { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Postal { get; set; }
        public string ExtractLast4OfAccount()
        {
            if (string.IsNullOrEmpty(Account))
            {
                return string.Empty;
            }

            return Account.Substring(Account.Length - 4, 4);
        }

        [NotMapped]
        public string IFramePort { get; set; }

        public PaymentTypes PaymentType
        {
            get
            {
                if (IsEndorsementPayment) return PaymentTypes.EndorsementPayment;
                if (IsTitanEndorsementPayment) return PaymentTypes.TitanEndorsementPayment;
                if (IsInitialDepostPayment) return PaymentTypes.InitialDepositPayment;
                if (IsPayForReinstatement) return PaymentTypes.ReinstatementPayment;
                if (IsRenewalPayment) return PaymentTypes.RenewalPayment;
                return PaymentTypes.InstallmentPayment;
            }
        }
    }
}