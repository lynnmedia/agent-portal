﻿using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace AgentPortal.Models
{
    public class QuoteIntegrationsViewModel
    {
        public string QuoteNo { get; set; }

        public bool AllIntegrationsOrdered()
        {
            return CoverageVerifierOrdered() && APlusOrdered() && MVROrdered() && APlusClaimModels.Any() && MvrDriverViolationsModels.Any() && CoverageVerifierReportModels.Any();
        }

        public bool CoverageVerifierOrdered()
        {
            return (CoverageVerifierReportModels.Any() &&  IsCVProcessed());
        }

        public bool APlusOrdered()
        {
            return APlusClaimModels.Any();
        }

        public bool MVROrdered()
        {
            return MvrDriverViolationsModels.Any();
        }

        public bool PolicyScoreValidForBinding { get; set; }

        public bool RiskCheckOrdered { get; set; }

        public IList<RiskCheckViolationsModel> RiskCheckDriverViolationsModels { get; set; } = new List<RiskCheckViolationsModel>();

        public IList<BrandedTitleVehiclesViewModel> BrandedTitleVehicles { get; set; } = new List<BrandedTitleVehiclesViewModel>();

        public IList<string> APlusCarrierClaimNumbers { get; set; } = new List<string>();

        public IList<MvrViolationsModel> MvrDriverViolationsModels { get; set; } = new List<MvrViolationsModel>();

        public IList<APlusClaimModel> APlusClaimModels { get; set; } = new List<APlusClaimModel>();

        public List<PolicyQuoteCars> UnOwnedVehicles { get; set; } = new List<PolicyQuoteCars>();

        public int TotalNumberOfClaims { get; set; }

        public bool APlusIsNoHit { get; set; }

        public bool CVIsNoHit { get; set; }

        public bool SingleDriverHasMoreThan3ClaimsPastThreeYearsRegardlessOfFault { get; set; }

        public bool SingleDriverHas2OrMoreClaimsPastThreeYearsAtFault { get; set; }

        public int TotalPipClaimsPastThreeYearsForAllDrivers { get; set; }

        public IList<CoverageVerifierReportModel> CoverageVerifierReportModels { get; set; } =
            new List<CoverageVerifierReportModel>();

        public bool SingleDriversTotalPointsAreInValid()
        {
            bool singleDriverHasInvalidTotalPoints = false;
            if (MvrDriverViolationsModels != null && MvrDriverViolationsModels.Any()) // if mvr violations exist count mvr's and any aplus 
            {
                foreach (List<MvrViolationsModel> mvrViolationList in MvrDriverViolationsModels
                    .Where(dv => dv.DriverIndex != null).GroupBy(dv => dv.DriverIndex).Select(dv => dv.ToList()))
                {
                    int totalPointsForAllViolationTypesForDriver = 0;
                    DateTime driverDateLicenced = mvrViolationList.First().DateLicensed;
                    if (driverDateLicenced.Equals(DateTime.MinValue) ||
                        driverDateLicenced.Equals(DateTime.MaxValue))
                    {
                        driverDateLicenced = mvrViolationList.First().DateOfBirth.AddYears(16);
                    }

                    bool isInExperienced = driverDateLicenced < DateTime.Now &&
                                           driverDateLicenced > DateTime.Now.AddYears(-3);
                    foreach (MvrViolationsModel mvrViolation in mvrViolationList)
                    {
                        totalPointsForAllViolationTypesForDriver +=
                            mvrViolation.DriversViolations.Sum(dv => dv.AAPoints);
                    }

                    // get aplus for this driver index
                    string driverIndexForCurrentDriver = mvrViolationList.First().DriverIndex?.ToUpper();
                    if (APlusClaimModels != null && APlusClaimModels.Any())
                    {
                        foreach (APlusClaimModel aplusViolation in APlusClaimModels.Where(dv =>
                            dv.DriverIndex != null && dv.DriverIndex.Equals(driverIndexForCurrentDriver)))
                        {
                            totalPointsForAllViolationTypesForDriver += aplusViolation.AAPoints;
                        }
                    }

                    if (isInExperienced && totalPointsForAllViolationTypesForDriver > 5)
                    {
                        singleDriverHasInvalidTotalPoints = true;
                    }

                    if (!isInExperienced && totalPointsForAllViolationTypesForDriver > 7)
                    {
                        singleDriverHasInvalidTotalPoints = true;
                    }
                }
            }
            else if (APlusClaimModels != null && APlusClaimModels.Any())
            {
                foreach (List<APlusClaimModel> aplusViolationList in APlusClaimModels
                    .Where(dv => dv.DriverIndex != null)
                    .GroupBy(dv => dv.DriverIndex).Select(dv => dv.ToList()))
                {
                    int totalPointsForAllViolationTypesForDriver = 0;
                    DateTime driverDateLicenced = aplusViolationList.First().DateLicensed;
                    if (driverDateLicenced.Equals(DateTime.MinValue) ||
                        driverDateLicenced.Equals(DateTime.MaxValue))
                    {
                        driverDateLicenced = aplusViolationList.First().DateOfBirth.AddYears(16);
                    }

                    bool isInExperienced = driverDateLicenced < DateTime.Now &&
                                           driverDateLicenced > DateTime.Now.AddYears(-3);
                    if (APlusClaimModels != null && APlusClaimModels.Any())
                    {
                        foreach (APlusClaimModel aplusViolation in aplusViolationList) 
                        {
                            totalPointsForAllViolationTypesForDriver += aplusViolation.AAPoints;
                        }
                    }

                    if (isInExperienced && totalPointsForAllViolationTypesForDriver > 5)
                    {
                        singleDriverHasInvalidTotalPoints = true;
                    }

                    if (!isInExperienced && totalPointsForAllViolationTypesForDriver > 7)
                    {
                        singleDriverHasInvalidTotalPoints = true;
                    }
                }
            }

            return singleDriverHasInvalidTotalPoints;
        }

        public bool IsValidToProceeed()
        {
            return TotalPipClaimsPastThreeYears() < 2 &&
                   !SingleDriverHas2OrMoreClaimsPastThreeYearsAtFault &&
                   !SingleDriverHasMoreThan3ClaimsPastThreeYearsRegardlessOfFault &&
                   !SingleDriversTotalPointsAreInValid() &&
                   !UnOwnedVehicles.Any();
        }
        public bool IsRiskCheckProcessed()
        {
            return this.RiskCheckOrdered && !BrandedTitleVehicles.Any() && !RiskCheckDriverViolationsModels.Any();
        }
            public int TotalPipClaimsPastThreeYears()
        {
            if (APlusClaimModels.Count == 0)
            {
                return 0;
            }

            int totalPipClaimsPastThreeYears = 0;
            DateTime currentDate = DateTime.Today;
            DateTime threeYearsAgo = currentDate.AddMonths(-36);
            foreach (var claim in this.APlusClaimModels)
            {
                if (claim.ClaimDateOfLoss != null)
                {
                    DateTime lossDate = DateTime.ParseExact(claim.ClaimDateOfLoss,
                        "yyyyMMdd", CultureInfo.InvariantCulture);
                    if (claim.ClaimDescription.ToUpper().Contains("PIP"))
                    {
                        if (lossDate <= currentDate && lossDate >= threeYearsAgo)
                        {
                            TotalPipClaimsPastThreeYearsForAllDrivers++;
                        }
                    }
                }
            }

            return totalPipClaimsPastThreeYears;
        }

        public bool IsCVProcessed()
        {
            bool isCVprocessed = true;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {

                XPCollection<XpoPolicyQuote> cvOrdered = null;
                cvOrdered = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ? AND CvOrdered = 1", QuoteNo));

                if (cvOrdered == null || !cvOrdered.Any())
                    isCVprocessed = false;

                if (cvOrdered != null && cvOrdered.Any())
                    isCVprocessed = true;
            }

            return isCVprocessed;
        }
    }

    public class BrandedTitleVehiclesViewModel
    {
        public BrandedTitleVehiclesViewModel()
        {
        }
        public BrandedTitleVehiclesViewModel(XpoBrandedTitleVehicles brandedTitleVehicle)
        {
            IndexID = brandedTitleVehicle.IndexID;
            PolicyNo = brandedTitleVehicle.PolicyNo;
            VehicleNbr = brandedTitleVehicle.VehicleNbr;
            Make = brandedTitleVehicle.Make;
            Model = brandedTitleVehicle.Model;
            VehYear = brandedTitleVehicle.VehYear;
            VIN = brandedTitleVehicle.VIN;
            LicensePlateNumber = brandedTitleVehicle.LicensePlateNumber;
            BrandedTitle1Code = brandedTitleVehicle.BrandedTitle1Code;
            BrandedTitle1Description = brandedTitleVehicle.BrandedTitle1Description;
            DateCreated = brandedTitleVehicle.DateCreated;
        }
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public int VehicleNbr { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VehYear { get; set; }
        public string VIN { get; set; }
        public string LicensePlateNumber { get; set; }
        public string BrandedTitle1Code { get; set; }
        public string BrandedTitle1Description { get; set; }
        public DateTime DateCreated { get; set; }
    }
}