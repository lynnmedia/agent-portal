﻿using System.ComponentModel.DataAnnotations;

namespace AgentPortal.Models
{
    public enum TypeOfPymnt
    {
        AgentSweep,
        CreditCard,
        Checking
    }

    public interface IPaymentMethod
    {
        TypeOfPymnt PaymentType { get; set; }
        string PolicyNo { get; set; }
        double Amount { get; set; }
    }

    public class AgentSweepModel
    {
        public string PolicyType { get; set; }

        [Required]
        public string PolicyNumber { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public double Amount { get; set; }
    }//END AgentSweepModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class MakePaymentModel
    {
        [DataType(DataType.Currency)]
        [Required]
        public double MinimumAmount { get; set; }
        [DataType(DataType.Currency)]
        [Display(Name = "Payment Amount")]
        public double PaymentAmt { get; set; }
        [DataType(DataType.Text)]
        public string PolicyNo { get; set; }
        public string PolicyType { get; set; }
    }//END MakePaymentModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class PaymentResponseModel
    {
        public string csbillTo_city { get; set; }
        public string csbillTo_country { get; set; }
        public string csbillTo_firstName { get; set; }
        public string csbillTo_lastName { get; set; }
        public string csbillTo_phoneNumber { get; set; }
        public string csbillTo_postalCode { get; set; }
        public string csbillTo_state { get; set; }
        public string csbillTo_street1 { get; set; }
        public string cscard_accountNumber { get; set; }
        public string cscard_cardType { get; set; }
        public string cscard_expirationMonth { get; set; }
        public string cscard_expirationYear { get; set; }
        public string csccAuthReply_amount { get; set; }
        public string csccAuthReply_authorizationCode { get; set; }
        public string csccAuthReply_authorizedDateTime { get; set; }
        public string csccAuthReply_avsCode { get; set; }
        public string csccAuthReply_avsCodeRaw { get; set; }
        public string csccAuthReply_cvCode { get; set; }
        public string csccAuthReply_cvCodeRaw { get; set; }
        public string csccAuthReply_processorResponse { get; set; }
        public string csccAuthReply_reasonCode { get; set; }
        public string csdecision { get; set; }
        public string csdecision_publicSignature { get; set; }
        public string csmerchantID { get; set; }
        public string csorderAmount { get; set; }
        public string csorderAmount_publicSignature { get; set; }
        public string csorderCurrency { get; set; }
        public string csorderCurrency_publicSignature { get; set; }
        public string csorderNumber { get; set; }
        public string csorderNumber_publicSignature { get; set; }
        public string csorderPage_requestToken { get; set; }
        public string csorderPage_serialNumber { get; set; }
        public string csorderPage_transactionType { get; set; }
        public string cspaymentOption { get; set; }
        public string csreasonCode { get; set; }
        public string csreconciliationID { get; set; }
        public string csrequestID { get; set; }
        public string cssignedDataPublicSignature { get; set; }
        public string cssignedFields { get; set; }
        public string cstransactionSignature { get; set; }
        public string pymntPolicyNumber { get; set; }
        public string now { get; set; }
        public string decision { get; set; }
        public string portalController { get; set; }
        public string policyType { get; set; }
        /// <summary>
        /// Response message from SystemCsResponseCodes Table
        /// </summary>
        public string DisplayResponseMsg { get; set; }
        public bool IsSuccessful { get; set; }
        /// <summary>
        /// Used to show error/success div styling
        /// </summary>
        public string DisplayCssClass { get; set; }
        /// <summary>
        /// Custom built reponse message based on certain criteria from cyber source
        /// </summary>
        public string BuiltResponseMsg { get; set; }
        public string GeneratedPolicyNo { get; set; }
        //--------------------------
        // Checks Only Fields
        public string ecDebitAmount { get; set; }
        public string orderAmount { get; set; }
        public string paymentOption { get; set; }

        public string RenPolicyNo { get; set; }
    }//END SubmitPaymentModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}//END Namepsace