﻿using AgentPortal.Support.DataObjects;
using System;
using System.Collections.Generic;

namespace AgentPortal.Models
{
    public class RiskCheckViolationsModel
    {
        public int DriverIndex { get; set; }
        public string DriverName { get; set; }
        public DateTime DOB { get; set; }
        public int NotAssociatedDriverIndex { get; set; }

        public IList<RiskCheckDriverViolationsModel> DriversViolations { get; set; } = new List<RiskCheckDriverViolationsModel>();
    }

    public class RiskCheckDriverViolationsModel
    {
        public RiskCheckDriverViolationsModel()
        {
        }
        public RiskCheckDriverViolationsModel(string violationDesc)
        {
            ViolationDesc = violationDesc;
        }
        public RiskCheckDriverViolationsModel(PolicyQuoteDriversViolations driversViolations)
        {
            ViolationDesc = driversViolations.ViolationDesc;
            ViolationDate = driversViolations.ViolationDate;
        }
        public string ViolationDesc { get; set; }
        public DateTime ViolationDate { get; set; }

    }
}