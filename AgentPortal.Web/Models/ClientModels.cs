﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AgentPortal.Models
{

    #region Registration and Login

    public class ClientLoginModel
    {
        [Required]
        [Display(Name="Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }//END ClientLoginModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class ClientSetPasswordModel
    {
        [Required]
        [Display(Name="Validation Code")]
        public string ValidationCode { get; set; }

        [Display(Name = "Email Address")]
        [Required]
        public string EmailAddress { get; set; }

        [Required]
        [Display(Name = "Your desired password")]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,100}$",
           ErrorMessage ="Your password must be between 8 to 100 characters long. Must contains one uppercase and one lowercase alphabet and one number.")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "Re-type password.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

        public bool IsResetPassword { get; set; }
    }//END ClientSetPasswordModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class ClientRegisterModel : ClientEmailAddress
    {
        [Required]
        [Display(Name = "Policy Number")]
        public string PolicyNumber { get; set; }

        [Display(Name="Drivers License Number")]
        public string DriversLicense { get; set; }

        [Required]
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string Firstname { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string Lastname { get; set; }

        [Required]
        [Display(Name="Security Question")]
        public string SecurityQuestion { get; set; }
        
        [Required]
        [Display(Name="Security Question Answer")]
        public string SecurityAnswer { get; set; }

    }//END ClientRegisterModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class ClientEmailAddress
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }
    }
    public class PasswordResetModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Old Password")]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Confirm Password")]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,100}$",
           ErrorMessage ="Your password must be between 8 to 100 characters long. Must contains one uppercase and one lowercase alphabet and one number.")]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        public bool IsForgotPassword { get; set; }
        public bool IsResetPassword { get; set; }
        public bool IsPasswordEmailSent { get; set; }
    }//END ClientEmailAddress
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    #endregion

    public class ClientPaymentModel
    {
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name="Amount to Pay")]
        public double Amount { get; set; }
    }//END ClientPaymentModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}//END Namespace