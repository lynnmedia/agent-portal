﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class CoverageVerifierReportModel
    {
        public string DriverName { get; set; }

        public string CarrierName { get; set; }

        public string PolicyExpiration { get; set; }
    }
}