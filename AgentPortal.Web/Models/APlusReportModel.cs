﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgentPortal.Support.DataObjects;
using PalmInsure.Palms.Xpo;

namespace AgentPortal.Models
{
    public class APlusReportModel
    {
        public IList<PolicyQuoteDrivers> Drivers { get; set; } = new List<PolicyQuoteDrivers>();

        public IList<APlusAddressModel> Addresses { get; set; } = new List<APlusAddressModel>();

        public IList<string> Vins { get; set; } = new List<string>();

        public IList<string> PhoneNumbers { get; set; } = new List<string>();
    }
}