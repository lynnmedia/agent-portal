﻿using System.Collections.Generic;
using System.Linq;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;

namespace AgentPortal.Models
{
    public class ESignatureForQuoteModel
    {
        public ESignatureForQuoteModel(IEnumerable<PolicySignature> policySignatures, string agentEmailAddress, string insuredFirstName, string insuredLastName, string insuredEmailAddress)
        {
            if (policySignatures.Any(sig => sig.EmailAddress.ToLower().Equals(agentEmailAddress.ToLower())))
            {
                var agentSignature =
                    policySignatures.FirstOrDefault(sig =>
                        sig.EmailAddress.ToLower().Equals(agentEmailAddress.ToLower()));
                AgentSigned = agentSignature.Status.Equals(AlertAuto.Integrations.Contracts.HelloSign.SignatureStatus.Signed);
            }
            if (policySignatures.Any() && !string.IsNullOrEmpty(insuredFirstName) &&
                !string.IsNullOrEmpty(insuredLastName) &&
                !string.IsNullOrEmpty(insuredEmailAddress))
            {
                var insuredSignature =
                    policySignatures.FirstOrDefault(sig =>
                        sig.EmailAddress.ToLower().Equals(insuredEmailAddress.ToLower()));
                if (insuredSignature != null)
                {
                    InsuredSigned = insuredSignature.Status.Equals(AlertAuto.Integrations.Contracts.HelloSign.SignatureStatus.Signed);
                }
            }
        }

        public bool? AgentSigned { get; set; }
        
        public bool? InsuredSigned { get; set; }

        public bool AllSigned => (AgentSigned.GetValueOrDefault() && InsuredSigned.GetValueOrDefault());

        public string ESignatureDocumentDownloadUrl { get; set; }
    }
}