﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace AgentPortal.Models
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


        public SelectListItem ListItems { get; set; }
    }//END ChangePasswordModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }//END RegisterModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }//END LoginModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class AgentLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string ReturnCode { get; set; }

        public string RedirectUrl { get; set; }
    }//END AgentLoginModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class CompanyLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }//END CompanyLoginModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}//END Namespace Dec.
