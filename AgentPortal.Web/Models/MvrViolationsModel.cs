﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgentPortal.Support.DataObjects;
using PalmInsure.Palms.Xpo;

namespace AgentPortal.Models
{
    public class MvrViolationsModel
    {
        public string DriverName { get; set; }

        public string DriverIndex { get; set; }

        public DateTime DateLicensed { get; set; }

        public DateTime DateOfBirth { get; set; }

        public IList<PolicyQuoteDriversViolations> DriversViolations { get; set; } = new List<PolicyQuoteDriversViolations>();
    }
} 