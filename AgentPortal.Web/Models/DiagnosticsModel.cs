﻿
namespace AgentPortal.Models
{
    public class DiagnosticsModel
    {
        public string IsDevEnvironment { get; set; }
        public string XpoWcfServerLink { get; set; }
        public string WcfLink { get; set; }
        public string Servername { get; set; }
        public string Username { get; set; }
        public string Version { get; set; }
        public string UserIP { get; set; }
    }//END Class
    ///////////////////////////////////////////////////////////////
}//END Namespace