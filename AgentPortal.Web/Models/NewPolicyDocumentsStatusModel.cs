﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class NewPolicyDocumentsStatusModel
    {
        public string PolicyDeclarationsFile { get; set; }

        public string IDCardsFile { get; set; }

        public string EFTFile { get; set; }

        public string ApplicationCoverFile { get; set; }

        public string ApplicationForInsuranceFile { get; set; }

        public string PaymentScheduleFile { get; set; }

        public string PaymentReceiptFile { get; set; }

        public string NewPolicyDocumentsFile { get; set; }
        
        public bool IsAllDocumentsGenerated { get; set; }

        public bool IsEftPolicy { get; set; }

        public bool AllDocumentsGenerated(bool isEftPolicy)
        {
            if (isEftPolicy)
            {
                return !string.IsNullOrEmpty(NewPolicyDocumentsFile) && !string.IsNullOrEmpty(IDCardsFile) &&
                       !string.IsNullOrEmpty(EFTFile) && !string.IsNullOrEmpty(ApplicationCoverFile) &&
                       !string.IsNullOrEmpty(ApplicationForInsuranceFile) &&
                       !string.IsNullOrEmpty(PaymentScheduleFile) &&
                       !string.IsNullOrEmpty(PaymentReceiptFile) && !string.IsNullOrEmpty(NewPolicyDocumentsFile);
            }
            else
            {
                return !string.IsNullOrEmpty(NewPolicyDocumentsFile) && !string.IsNullOrEmpty(IDCardsFile) &&
                       !string.IsNullOrEmpty(ApplicationCoverFile) &&
                       !string.IsNullOrEmpty(ApplicationForInsuranceFile) &&
                       !string.IsNullOrEmpty(PaymentScheduleFile) &&
                       !string.IsNullOrEmpty(PaymentReceiptFile) && !string.IsNullOrEmpty(NewPolicyDocumentsFile);
            }
        }
    }
}