﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlertAuto.Integrations.Contracts.MVR;

namespace AgentPortal.Models
{
    public class OrderMvrsViewModel
    {
        public IEnumerable<DriverModel> Drivers { get; set; } = new List<DriverModel>();

        public IEnumerable<VeriskMvrSubmitRequestBody> MvrReportRequests { get; set; } = new List<VeriskMvrSubmitRequestBody>();

        public IEnumerable<VeriskRetreiveMvrReportBody> MvrReports { get; set; } = new List<VeriskRetreiveMvrReportBody>();
    }
}