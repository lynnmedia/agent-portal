﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Models
{
    public class APlusAddressModel
    {
        public APlusAddressModel()
        {
            AddressType = "Current";
        }

        public string AddressType { get; set; }

        public string Street1 { get; set; }

        public string Street2 { get; set; }

        public string City { get; set; }

        public string StateCode { get; set; }

        public string Zip { get; set; }
    }
}