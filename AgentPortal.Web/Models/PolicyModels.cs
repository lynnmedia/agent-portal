﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using AgentPortal.Support;
using AgentPortal.Support.Attributes;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Xpo;
using PolicyCarLienHolders = AgentPortal.Support.DataObjects.PolicyCarLienHolders;

namespace AgentPortal.Models
{
    public class SearchPolicyModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Insured Name")]
        public string InsuredName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Policy Number")]
        public string PolicyNo { get; set; }

        public int NumDaysSearch { get; set; }

        public bool HideCancelledPolicies { get; set; }

    }//END Class SearchPolicyModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public class AgentSearchPolicyModel
    {
        public IList<FoundPolicyModel> FoundPolicys { get; set; } = new List<FoundPolicyModel>();
        public IList<FoundPolicyModel> FoundQuotes { get; set; } = new List<FoundPolicyModel>();
        public IList<FoundPolicyModel> Last10Quotes { get; set; } = new List<FoundPolicyModel>();
        public IList<FoundPolicyModel> Last10Policies { get; set; } = new List<FoundPolicyModel>();
        public string SearchMessage { get; set; } = string.Empty;
        public bool? HideCancelledPolicies { get; set; } = false;
        public bool? TenDaysSelected { get; set; } = false;
        public bool? ThirtyDaysSelected {get; set; } = true;
        public bool? NinetyDaysSelected { get; set; } = false;
        public bool? AllSelected { get; set; } = false;

    }

    public class FoundPolicyModel
    {
        public string PolicyNo { get; set; }
        public DateTime Effdate { get; set; }
        public string Insuredname { get; set; }
        public string StreetAddress { get; set; }
        public string Status { get; set; }
    }//END FoundPolicyModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class ViewPolicyModel
    {
        public string PolicyNo { get; set; }
        //------------------------------------------
        public string PriorPolicyNo { get; set; }
        public string NextPolicyNo { get; set; }
        public int RenCount { get; set; }
        public string State { get; set; }
        public string LOB { get; set; }
        public bool IsClient { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public string PolicyStatus { get; set; }
        public string RenewalStatus { get; set; }
        public DateTime DateBound { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime CancelDate { get; set; }
        public string CancelType { get; set; }
        public string Ins1First { get; set; }
        public string Ins1Last { get; set; }
        public string Ins1MI { get; set; }
        public string Ins1Suffix { get; set; }
        public string Ins1FullName { get; set; }
        public string Ins2First { get; set; }
        public string Ins2Last { get; set; }
        public string Ins2MI { get; set; }
        public string Ins2Suffix { get; set; }
        public string Ins2FullName { get; set; }
        public string Territory { get; set; }
        public double PolicyWritten { get; set; }
        public double PolicyAnnualized { get; set; }
        public int CommPrem { get; set; }
        public int DBSetupFee { get; set; }
        public int PolicyFee { get; set; }
        public int SR22Fee { get; set; }
        public double FHCFFee { get; set; }
        public string RateCycle { get; set; }
        public bool SixMonth { get; set; }
        public int PayPlan { get; set; }
        public bool PaidInFullDisc { get; set; }
        public bool EFT { get; set; }
        public string MailStreet { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailZip { get; set; }
        public bool MailGarageSame { get; set; }
        public string GarageStreet { get; set; }
        public string GarageCity { get; set; }
        public string GarageState { get; set; }
        public string GarageZip { get; set; }
        public string GarageTerritory { get; set; }
        public string GarageCounty { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MainEmail { get; set; }
        public string AltEmail { get; set; }
        public bool Paperless { get; set; }
        public string AgentCode { get; set; }
        public double CommAtIssue { get; set; }
        public double LOUCommAtIssue { get; set; }
        public double ADNDCommAtIssue { get; set; }
        public bool AgentGross { get; set; }
        public string PIPDed { get; set; }
        public bool NIO { get; set; }
        public bool NIRR { get; set; }
        public bool UMStacked { get; set; }
        public bool HasBI { get; set; }
        public bool HasMP { get; set; }
        public bool HasUM { get; set; }
        public bool HasLOU { get; set; }
        public string BILimit { get; set; }
        public string PDLimit { get; set; }
        public string UMLimit { get; set; }
        public string MedPayLimit { get; set; }
        public bool Homeowner { get; set; }
        public bool RenDisc { get; set; }
        public TransferDiscountOptions TransDisc { get; set; }
        public string PreviousCompany { get; set; }
        public DateTime PreviousExpDate { get; set; }
        public bool PreviousBI { get; set; }
        public bool PreviousBalance { get; set; }
        public bool DirectRepairDisc { get; set; }
        public string UndTier { get; set; }
        public bool OOSEnd { get; set; }
        public int LOUCost { get; set; }
        public int LOUAnnlPrem { get; set; }
        public int ADNDLimit { get; set; }
        public int ADNDCost { get; set; }
        public int ADNDAnnlPrem { get; set; }
        public bool HoldRtn { get; set; }
        public bool NonOwners { get; set; }
        public string ChangeGUID { get; set; }

        public string InsuredPreferredLanguage { get; set; }

        public string EftAccountType { get; set; }

        public string EftAccountLast4 { get; set; }

        public int policyCars { get; set; }
        public int policyDrivers { get; set; }
        public bool Unacceptable { get; set; }
        public int HousholdIndex { get; set; }
        public int RatingID { get; set; }
        public int OID { get; set; }
        public int Cars { get; set; }
        public int Drivers { get; set; }
        public bool IsReturnedMail { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsClaimMisRep { get; set; }
        public bool IsNoREI { get; set; }
        public bool IsSuspense { get; set; }
        public bool isAnnual { get; set; }
        public string Carrier { get; set; }
        public bool HasPriorCoverage { get; set; }
        public bool HasPriorBalance { get; set; }
        public bool HasLapseNone { get; set; }
        public bool HasPD { get; set; }
        public bool HasTOW { get; set; }
        public string PIPLimit { get; set; }
        public bool HasADND { get; set; }
        public int MVRFee { get; set; }
        public int WorkLoss { get; set; }
        public double RentalAnnlPrem { get; set; }
        public double RentalCost { get; set; }
        public bool HasLapse110 { get; set; }
        public bool HasLapse1131 { get; set; }
        public double QuotedAmount { get; set; }
        public string AppQuestionAnswers { get; set; }
        public string AppQuestionExplainOne { get; set; }
        public string AppQuestionExplainTwo { get; set; }
        public bool FromPortal { get; set; }
        public string FromRater { get; set; }
        public string QuoteNo { get; set; }
        public string AgencyRep { get; set; }
        public DateTime QuotedDate { get; set; }
        public bool Bridged { get; set; }
        public bool HasRenewal { get; set; }
        public int RenPymntPlan { get; set; }
        public string RenPremium { get; set; }
        public string RenDepositAmt { get; set; }
        public DateTime RenEffDate { get; set; }
        public string RenMinDueAmtStr { get; set; }
        public decimal RenMinDueAmt { get; set; }

        public string RenDecFilename { get; set; }
        public string RenDecDateTime { get; set; }

        public string RenPaySchdFilename { get; set; }
        public string RenPaySchdDateTime { get; set; }
        public string RenewalDecLink { get; set; }
        public string RenewalInvLink { get; set; }
        public List<System.Web.Mvc.SelectListItem> RenPayPlans { get; set; }
        public bool IsAllowRenPastExp { get; set; }
        public bool AllowPymntWithin15Days { get; set; }
        //------------------------------------------
        public string idcardfilename { get; set; }
        public string idcarddatetime { get; set; }
        //------------------------------------------
        public string decpagefilename { get; set; }
        public string decpagedatetime { get; set; }
        //------------------------------------------
        public string invoicefilename { get; set; }
        public string invoicedatetime { get; set; }
        //------------------------------------------
        public string pymntschedule { get; set; }
        public string pymtnscheduledatetime { get; set; }
        //------------------------------------------
        public string newpolicydocs { get; set; }
        public string newpolicydocsdatetime { get; set; }
        //------------------------------------------
        public bool IsRenewalPolicy { get; set; }
        public string GenNextPolicyNo { get; set; }
        public string GenPrevPolicyNo { get; set; }
        public double MonthlyRenewalPymnt { get; set; }
        //------------------------------------------
        public bool isOnHold { get; set; }
        public UnitOfWork uow = XpoHelper.GetNewUnitOfWork();
        //------------------------------------------
        //Billing Info (Not In Policy Table)
        public double MinDue { get; set; }
        public Guid BillingGuid { get; set; }
        public DateTime DateDue { get; set; }
        public double TotalPremium { get; set; }
        public double TotalFees { get; set; }
        public double TotalPaid { get; set; }
        public double TotalDue { get; set; }
        public double TotalWritten { get; set; }
        public double CurrentLateFee { get; set; }
        public double AmtToPay { get; set; }
        public string DisplayStatus { get; set; }
        [Display(Name = "6 Month")]
        public bool RenQuoteSixMonth { get; set; }
        [Display(Name = "12 Month")]
        public bool RenQuoteAnnual { get; set; }
        public double RenInstallFees { get; set; }
        public double MaintenanceFee { get; set; }
        public double PIPPDOnlyFee { get; set; }
        public string RenewalQuoteNumber { get; set; }
        public DateTime RenewalEffDate { get; set; }
        public string PolicyREIStageText { get; set; }
        public double BalanceAmtToPayForREI { get; set; }
        public DateTime DateOfReinstatement { get; set; }
        public List<PolicyCars> _CarsList { get; set; }
        public List<PolicyCars> CarsList
        {
            get
            {
                if (_CarsList?.Any() != true)
                {
                    _CarsList = GetCars;
                    //_CarsList= _CarsList.OrderBy(x => x.CarIndex).ToList()
                }
                return _CarsList;
            }
        }
        public List<PolicyDrivers> _DriversList { get; set; }
        public List<PolicyDrivers> DriversList
        {
            get
            {
                if (_DriversList?.Any() != true)
                {
                    _DriversList = GetDrivers;
                    //_DriversList = _DriversList.OrderBy(x => x.DriverIndex).ToList()
                }
                return _DriversList;
            }
        }


        //------------------------------------------
        public List<XpoAuditPolicyHistoryReasons> SuspenseReasons
        {
            get { return new XPCollection<XpoAuditPolicyHistoryReasons>(uow, CriteriaOperator.Parse("PolicyNo = ? AND ReasonType = 'SUSPENSE' AND Status != 'Clear'", PolicyNo)).ToList(); }
        }
        //------------------------------------------
        public List<PolicyCars> GetCars
        {
            get { return new XPCollection<PolicyCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyNo)).ToList(); }
        }
        //------------------------------------------
        public List<PolicyDrivers> GetDrivers
        {
            get { return new XPCollection<PolicyDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyNo)).ToList(); }
        }
        //------------------------------------------
        public List<PolicyCarLienHolders> GetLienHolders
        {
            get { return new XPCollection<PolicyCarLienHolders>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyNo)).ToList(); }
        }
        //------------------------------------------
        public List<XpoAuditBillingInfo> GetBillingInfo
        {
            get { return new XPCollection<XpoAuditBillingInfo>(uow, CriteriaOperator.Parse("UI_GUID = ?", BillingGuid)).ToList(); }
        }
        //------------------------------------------
        public List<XpoAgentUploads> GetPendingDocuments
        {
            get { return new XPCollection<XpoAgentUploads>(uow, CriteriaOperator.Parse("RecordKey = ? AND (IsProcessed IS NOT NULL AND IsProcessed <> '1') AND (ErrMsg IS NULL)", PolicyNo)).ToList(); }
        }

        public ESignatureForQuoteModel ESignatureForQuoteModel { get; set; }

        public IEnumerable<AuditImageIndex> AgentUploads { get; set; } = new List<AuditImageIndex>();

        public IEnumerable<AuditImageIndex> PolicyDocuments { get; set; } = new List<AuditImageIndex>();

        public SelectedData GetCoverageInfo { get; set; }
        public SelectedData GetHistory { get; set; }
        public Dictionary<string,List<string>> HistoryReasons { get; set; }
        public int GetOpenClaimsCount
        { 
            get { return new XPCollection<Claimants>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Status = 'OPEN'", PolicyNo)).Count(); } 
        }
        public string GetClaimsAndSuspesneStatus
        {
            get
            {

                    return String.Format(" ({0}, {1})", (GetOpenClaimsCount > 0 ? "Active Claims" : "No active claims"), (SuspenseReasons.Count > 0 ? "Active Suspense" : "No Suspense"));
            }
        }
    }//END ViewPolicyModel
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public class EndorsementDriverModel : IValidatableObject
    {
        public string PolicyID { get; set; }
        public string DriverId { get; set; }
        public bool IsEdit { get; set; }
        public int DriverIndex { get; set; }

        public bool HasMVR { get; set; }
        //---------------------------------------------------------
        [Display(Name = "Unverifiable/No Hit")]
        public bool Unverifiable { get; set; }

        public int OID { get; set; }
        //---------------------------------------------------------
        private string date = DateTime.Now.AddYears(-1).ToString("yyyy/MM/dd");
        #region Personal Information

        public string TempFirstName { get; set; }
        public string TempLastName { get; set; }
        public string TempMI { get; set; }      //LF added 11/18/18

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Initial")]
        [StringLength(1, MinimumLength = 1, ErrorMessage = "Your middle initial should be 1 character long")]
        public string MiddleInitial { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Suffix")]
        [StringLength(5)]
        public string Suffix { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Date Of Birth must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please Select A Gender")]
        public string Gender { get; set; }

        [Display(Name = "Married")]
        public bool IsMarried { get; set; }

        [Required]
        [Display(Name = "Relation To Insured")]
        public string Relation { get; set; }

        [Display(Name = "Is Excluded")]
        public bool IsExcluded { get; set; }

        #endregion

        //---------------------------------------------------------
        #region License Information

        [Display(Name = "International License")]
        public bool IsInternationalLicense { get; set; }

        [Display(Name = "Senior Driving Course?")]
        public bool HasSeniorDefDrvDisc { get; set; }

        [Display(Name = "SR22?")]
        public bool HasSR22 { get; set; }

        [Display(Name = "License Number")]
        //[RegularExpression(Constants.FlDlRegex, ErrorMessage = "The drivers license number has been generated for you. Please add the  last dash and digit to the drivers license number.")]
        public string DriversLicense { get; set; }
        public string OldDlNum { get; set; }

        [Display(Name = "Date of course")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Date Of Course must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime? SeniorDefDrvDate { get; set; }

        [Display(Name = "SR22 Case No")]

        public string SR22CaseNo { get; set; }

        [Required]
        [Display(Name = "License State")]
        public string LicenseState { get; set; }

        [Display(Name = "Date Licensed")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Date Licensed must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public string DateLicensed { get; set; }

        #endregion
        //---------------------------------------------------------

        #region Employer Info

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Employer Name")]
        public string EmployerName { get; set; }

        [Display(Name = "Occupation")]
        public string Occupation { get; set; }

        [Display(Name = "Employer Address")]
        public AddressModel EmployerAddress { get; set; }

        public ModelStateDictionary ValidationErrors { get; set; }

        #endregion
        //---------------------------------------------------------

        #region Not Sure If Still Need

        [Display(Name = "Class")]
        public string Class { get; set; }

        #endregion
        //---------------------------------------------------------

        [UIHint("Violations")]
        public List<EndorsementViolationModel> Violations { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            if (DateOfBirth.Parse<DateTime>() > DateTime.Now)
            {
                yield return new ValidationResult("Date Of Birth cannot be in the future.", new[] { "DateOfBirth" });
            }

            if (!IsExcluded && string.IsNullOrEmpty(DriversLicense))
            {
                yield return new ValidationResult("Drivers license number is required.", new[] { "DriversLicenseNumber" });
            }

            if (!IsExcluded && !string.IsNullOrEmpty(LicenseState) && LicenseState.Equals("FL"))
            {
                string licenseNo = DriversLicense?.Replace("-", string.Empty);
                if (licenseNo?.Length != 13)
                {
                    yield return new ValidationResult("A valid FL Drivers license number must be 13 characters.",
                        new[] { "DriversLicenseNumber" });
                }

                //validate birth year
                string twoDigitBirthYear = DateOfBirth?.Substring(DateOfBirth.Length - 2, 2);
                string licenseBirthDateDigits = licenseNo?.Substring(7, 2);
                if (twoDigitBirthYear != licenseBirthDateDigits)
                {
                    yield return new ValidationResult(
                        "A valid FL Drivers should contain the drivers two digit birth year.",
                        new[] { "DriversLicenseNumber" });
                }

            }

            if (!String.IsNullOrEmpty(MiddleInitial) && !Char.IsLetter(MiddleInitial[0]))
            {
                yield return new ValidationResult("Middle initial must be a alphabetic", new[] { "MiddleInitial" });
            }

            if (!IsExcluded && DateOfBirth.Parse<DateTime>() > DateTime.Now.AddYears(-16))
            {
                yield return new ValidationResult("We only accept drivers over 16 years of age", new[] { "DateOfBirth" });
            }

            if (!IsInternationalLicense && !StaticData.USAStates.Keys.Contains((LicenseState)))
            {
                yield return new ValidationResult("License state must be one of the 50 states", new[] { "LicenseState" });
            }

            if(UtilitiesDrivers.IsDriverUnacceptableRisk(FirstName, LastName, DateOfBirth, DriversLicense, LicenseState))
            {
                 yield return new ValidationResult(String.Format("The driver ({0} {1}) marked in system with Unacceptable Risk.",
                                   FirstName, LastName), new[] { "UnacceptableRisk" });
                
            }
        }
    }
    public class EndorsementViolationAddModel
    {
        // TODO: WHAT AM I?
        public string DriverId { get; set; }

        public int DriverIndex { get; set; }

        /// <summary>
        /// Database column LongDesc
        /// </summary>
        public string Description { get; set; }

        [Required]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime Date { get; set; }

        public bool FromMvr { get; set; }
    }//END ViolationAddModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class EndorsementViolationModel : EndorsementViolationAddModel
    {
        /// <summary>
        /// Id generate when added to a driver.
        /// </summary>
        public int ViolationId { get; set; }

        /// <summary>
        /// ID in the AARateViolations table.
        /// </summary>
        public int IndexID { get; set; }

        /// <summary>
        /// Database column PointsFirst
        /// </summary>
        public int Points { get; set; }

        public string PolicyNo { get; set; }

        new public bool FromMvr { get; set; }

        public bool FromCV { get; set; }



        public bool FromAplus { get; set; }

    }//END ViolationModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class EndorsementVehicleModel
    {

        public string PolicyID { get; set; }
        public string VehicleId { get; set; }
        public int CarIndex { get; set; }
        public bool IsEdit { get; set; }

        public int OID { get; set; }

        [Display(Name = "Comp Ded")]
        public string ComprehensiveDeductible { get; set; }

        [Display(Name = "Coll Ded")]
        public string CollisionDeductible { get; set; }

        public int Year { get; set; }
        public string Make { get; set; }
        [Display(Name = "Model")]
        public string VehModel { get; set; }
        [Display(Name = "Body Style")]
        public string Style { get; set; }

        [MinLength(17)]
        [Required]
        public string VIN { get; set; }

        [Display(Name = "Miles to work")]
        public int? MilesToWork { get; set; }

        [Display(Name = "Airbags")]
        public bool HasAirbag { get; set; }
        

        [Display(Name = "Antilock Brakes")]
        public bool HasAntiLockBrakes { get; set; }

        [Display(Name = "Anti-theft Device")]
        public bool HasAntiTheftDevice { get; set; }

        public bool Artisan { get; set; }

        [Display(Name = "Physical Damage coverage")]
        [Required]
        public bool HasPhyDam { get; set; }

        [Display(Name = "Convertible / TTops")]
        public bool HasConvOrTT { get; set; }

        [Display(Name = "> 10 Miles to Wk?")]
        public bool MoreThan10MilesToWk { get; set; }

        [Display(Name = "Amt Custom")]
        public double? AmountCustom { get; set; }

        [Display(Name = "Purchased New")]
        public bool IsPurchasedNew { get; set; }

        [Display(Name = "Customzied")]
        public bool IsCustomized { get; set; }

        [Display(Name = "Business Use")]
        public bool IsBusinessUse { get; set; }

        public string AntiTheftType { get; set; }

        public int VinIndexNo { get; set; }

        public string CompSymbol { get; set; }

        public string CollSymbol { get; set; }

        public string BISymbol { get; set; }

        public string PDSymbol { get; set; }

        public string MPSymbol { get; set; }

        public string PIPSymbol { get; set; }

        //AirBag info
        public string ARB { get; set; }

        /// <summary>
        /// AntiTeheft info
        /// </summary>
        public string ATD { get; set; }


        public string CostNew { get; set; }

        public string EngineType { get; set; }

        public string GrossVehicleWeight { get; set; }

        public string ClassCode { get; set; }


        [UIHint("LienHolders")]
        public List<EndorsementLienHolderModel> LienHolders { get; set; }

        public bool HasAllSymbols()
        {
            return !string.IsNullOrEmpty(PDSymbol) && !string.IsNullOrEmpty(PIPSymbol) &&
                   !string.IsNullOrEmpty(BISymbol) && !string.IsNullOrEmpty(MPSymbol) &&
                   !string.IsNullOrEmpty(CollSymbol) && !string.IsNullOrEmpty(CompSymbol) &&
                   !string.IsNullOrEmpty(MPSymbol) && !string.IsNullOrEmpty(ATD) &&
                   !string.IsNullOrEmpty(ARB);
        }
    }
    public class EndorsementLienHolderAddModel
    {
        public int ID { get; set; }
        public int VehicleId { get; set; }

        [Display(Name = "Policy No")]
        public string PolicyNo { get; set; }

        [Display(Name = "Vehicle Index")]
        public int CarIndex { get; set; }

        [Required(ErrorMessage ="Please Enter Name")]
        public string Name { get; set; }

        [Display(Name = "Street Address")]
        [Required(ErrorMessage = "Please Enter Street Address")]
        public string StreetAddress { get; set; }

        [Required(ErrorMessage = "Please Enter City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please Enter State")]
        public string State { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        [Required(ErrorMessage = "Please Enter ZIP")]
        public string ZIP { get; set; }

        [RegularExpression(Constants.PhoneRegex, ErrorMessage = "Phone number must be in XXX-XXX-XXXX format")]
        public string Phone { get; set; }

        [Display(Name = "Additional Interest?")]
        public bool isAddInterest { get; set; }

        [Display(Name = "Non-Institutional?")]
        public bool IsNonInstitutional { get; set; }

        public string VIN { get; set; }
    }
    public class EndorsementLienHolderModel : EndorsementLienHolderAddModel
    {
        [Display(Name = "Lienholder Index")]
        public int LienHolderIndex { get; set; }

        new public string VIN { get; set; }

    }
    public class IssueEndorsementDetailsModel
    {
        public List<string> EndorsementReasons { get; set; }
        public List<string> RequiredDocuments { get; set; }

        public string ReceiptDownloadLink { get; set; }
        public decimal PaymentAmt { get; set; }
    }

    public class DisallowedEndorsementModel
    {
        public DisallowedEndorsementModel()
        {
            TodaysDate = DateTime.Now;
            Cars = new List<DisallowedAgentEndorseCarsDO>();
            Drivers = new List<DisallowedAgentEndorseDriversDO>();
            DriverViolations = new List<DisallowedAgentEndorseDriversViolationsDO>();
            LossPayee = new List<DisallowedAgentEndorseLossPayeeDO>();
        }

        public string ErrorMessage { get; set; }
        public int DisallowedEndorsementQuoteId { get; set; }
        public ModelStateDictionary ValidationErrors { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime TodaysDate { get; set; }
        public string EffectiveTime { get; set; }
        public string AgencyCode { get; set; }
        public string InsuredFirstName { get; set; }
        public string InsuredLastName { get; set; }
        public string InsuredMI { get; set; }
        public string Insured2FullName { get; set; }
        public string InsuredAddress { get; set; }
        public bool IsNewAddress { get; set; }
        public double AdditionalPremiumCollected { get; set; }
        public string AgencyName { get; set; }
        public string AgencyAddress { get; set; }
        public DateTime EndorsementEffectiveDate { get; set; }
        public DateTime PolicyInceptionDate { get; set; }
        public DateTime PolicyExpirationDate { get; set; }

        public bool IsAddressChange { get; set; }
        public bool IsDriversChange { get; set; }
        public bool IsVehiclesChange { get; set; }
        public bool IsLossPayeeChange { get; set; }
        public bool IsCoverageChange { get; set; }
        public bool IsFromIssuance { get; set; }


        public bool? NIO { get; set; }
        public bool? NIRR { get; set; }
        public bool? HasPPO { get; set; }
        public bool? Paperless { get; set; }
        public bool? HomeOwner { get; set; }

        public string PIPChangeType { get; set; }
        public string PDChangeType { get; set; }
        public string AgentAltESignEndorseEmailAddr { get; set; }
        public string AgentEndorsementRemarks { get; set; }

        public void SetEndorsementChangesFromDB()
        {
            SetEndorsementChanges();// set PIP change type and then override below
            IsAddressChange = IsNewAddress;
            IsVehiclesChange = DisallowedEndorseQuote.IsVehiclesChange.GetValueOrDefault();
            IsDriversChange = DisallowedEndorseQuote.IsDriversChange.GetValueOrDefault();
            IsLossPayeeChange = DisallowedEndorseQuote.IsLossPayeeChange.GetValueOrDefault();
            IsCoverageChange = DisallowedEndorseQuote.IsCoverageChange.GetValueOrDefault();
        }
        public void SetEndorsementChanges()
        {
            IsAddressChange = IsNewAddress;
            IsVehiclesChange = Cars.Any();
            IsDriversChange = Drivers.Any();
            IsLossPayeeChange = LossPayee.Any();
            //if (DisallowedEndorseQuote != null)
            //{
            //    if(!string.IsNullOrEmpty(DisallowedEndorseQuote.Pip))
            //}
            IsCoverageChange = DisallowedEndorseQuote != null && (DisallowedEndorseQuote.PIPDed != DisallowedEndorseQuote.PIPDedOld ||
                 DisallowedEndorseQuote.PDLimit != DisallowedEndorseQuote.PDLimitOld ||
                  DisallowedEndorseQuote.WorkLoss != DisallowedEndorseQuote.WorkLossOld ||
                  DisallowedEndorseQuote.HasPPO.HasValue || DisallowedEndorseQuote.NIO.GetValueOrDefault() || DisallowedEndorseQuote.NIRR.GetValueOrDefault());
            if (IsCoverageChange)
            {
                if (DisallowedEndorseQuote.PIPDed != DisallowedEndorseQuote.PIPDedOld)
                {
                    if (!string.IsNullOrEmpty(DisallowedEndorseQuote.PIPDed) && DisallowedEndorseQuote.PIPDed != "0")
                    {
                        if (string.IsNullOrEmpty(DisallowedEndorseQuote.PIPDedOld) || DisallowedEndorseQuote.PIPDedOld == "0")
                        {
                            PIPChangeType = "Add";
                        }
                        else
                        {
                            PIPChangeType = "Modify";
                        }
                    }
                    else if (!string.IsNullOrEmpty(DisallowedEndorseQuote.PIPDedOld) && DisallowedEndorseQuote.PIPDedOld != "0")
                    {
                        PIPChangeType = "Delete";
                    }
                }

                if (string.IsNullOrEmpty(PIPChangeType) && DisallowedEndorseQuote.WorkLoss != DisallowedEndorseQuote.WorkLossOld)
                {
                    PIPChangeType = "Modify";
                }

                if (DisallowedEndorseQuote.PDLimit != DisallowedEndorseQuote.PDLimitOld)
                {
                    if (!string.IsNullOrEmpty(DisallowedEndorseQuote.PDLimit) && DisallowedEndorseQuote.PDLimit != "0")
                    {
                        if (string.IsNullOrEmpty(DisallowedEndorseQuote.PDLimitOld) || DisallowedEndorseQuote.PDLimitOld == "0")
                        {
                            PDChangeType = "Delete";
                        }
                        else
                        {
                            PDChangeType = "Modify";
                        }
                    }
                    else if (!string.IsNullOrEmpty(DisallowedEndorseQuote.PIPDedOld) && DisallowedEndorseQuote.PIPDedOld != "0")
                    {
                        PDChangeType = "Add";
                    }
                }
            }
        }


        public AgentsDO AgentInfo { get; set; }
        public DisallowedAgentEndorseQuoteDO DisallowedEndorseQuote { get; set; }
        public List<DisallowedAgentEndorseCarsDO> Cars { get; set; }
        public List<DisallowedAgentEndorseDriversDO> Drivers { get; set; }
        public List<DisallowedAgentEndorseDriversViolationsDO> DriverViolations { get; set; }
        public List<DisallowedAgentEndorseLossPayeeDO> LossPayee { get; set; }

    }

    public class EndorsementPolicyModel : IValidatableObject
    {
        //Endorsement Model Specific
        public static readonly int[] TermMonthsChoices = new int[] { 6, 12 };
        public string EndorsementID { get; set; }
        public string AgentCode { get; set; }
        public int PayPlan { get; set; }
        public bool HasSr22 { get; set; }
        public int SR22Fee { get; set; }

        public bool PolicyRated { get; set; }
        public int DBSetupFee { get; set; }
        public int MVRFee { get; set; }
        public int PolicyFee { get; set; }
        public double FHCFFee { get; set; }
        public double PolicyWritten { get; set; }

        public double OldPolicyWritten { get; set; }
        public bool SixMonth { get; set; }

        public bool IsEditedPalms { get; set; }

        public DateTime DateBound { get; set; }
        public DateTime EndorsementDate { get; set; }
        public bool IsShow { get; set; }

        public List<string> EndorsementReasons { get; set; }

        public bool AddressChanged { get; set; }

        public bool PolicyCoveragesChanged { get; set; }

        public bool MiscPolicyInfoChanged { get; set; }

        public bool CarAdded { get; set; }

        public bool Paperless { get; set; }

        [Display(Name = "Advanced Quote")]
        public bool AdvancedQuote { get; set; }

        [Display(Name = "Current Customer")]
        public bool IsCurrentCustomer { get; set; }

        public bool DriverAdded { get; set; }

        public bool CarDeleted { get; set; }
        public bool DriverDeleted { get; set; }
        public bool CarChanged { get; set; }
        public bool DriverChanged { get; set; }

        public string ErrorMessage { get; set; }
        public string WarningMessage { get; set; }

        public string InsuredPreferredLanguage { get; set; }

        public List<string> RequiredDocuments { get; set; }

        public double PolicyDiffAmt
        {
            get;
            set;
        }
        string fFromRater;
        [Size(20)]


        #region General Policy Info

        public string CreditGuid { get; set; }


        [Display(Name = "Rating State")]
        public string RatingState { get; set; }

        public int PolicyTermMonths { get; set; }
        public int EstimatedCreditScore { get; set; }
        public int CreditScore { get; set; }

        [Display(Name = "Prior Coverage")]
        public bool PriorCoverage { get; set; }

        [Display(Name = "Prior Policy Had BI")]
        public bool PreviousBI { get; set; }

        [Display(Name = "Prior Company")]
        //[Contains("foo", ErrorMessage = "Second must contain the word 'foo'")]
        public string PriorCompany { get; set; }

        [Display(Name = "Prior Policy No")]
        public string PriorPolicyNo { get; set; }
        [Display(Name = "Policy No")]
        public string PolicyNo { get; set; }
        [Display(Name = "Next Policy No")]
        public string NextPolicyNo { get; set; }
        public SelectedData GetCoverageInfo { get; set; }



        [Display(Name = "Effective Date")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Effective Date must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime EffectiveDate { get; set; }

        [Display(Name = "Expiration Date")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Expiration Date must be in the format of mm/dd/yyyy")]
        public DateTime ExpirationDate { get; set; }

        private DateTime? _PreviousExpDate;
        [Display(Name = "Prior Policy Exp Date")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Previous Expiration Date must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime? PreviousExpDate
        {
            get
            {
                return _PreviousExpDate == "1/1/0001".Parse<DateTime>() ? null : _PreviousExpDate;
            }
            set
            {
                _PreviousExpDate = value;
            }
        }

        public string RaterType { get; set; }

        #endregion
        //--------------------------------------------------------------

        #region Insured Information

        [Required]
        public string FirstName1 { get; set; }

        [StringLength(1, ErrorMessage = "This initial should be only one character")]
        public string MiddleInitial1 { get; set; }

        [Required]
        public string LastName1 { get; set; }

        public string Suffix1 { get; set; }
        //#############################################

        public string FirstName2 { get; set; }

        [StringLength(1, ErrorMessage = "This initial should be only one character")]
        public string MiddleInitial2 { get; set; }

        public string LastName2 { get; set; }

        public string Suffix2 { get; set; }
        //#############################################

        [Display(Name = "Home")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(Constants.PhoneRegex, ErrorMessage = "Phone number must be in XXX-XXX-XXXX format")]
        [Required]
        public string HomePhone { get; set; }

        [Display(Name = "Work")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(Constants.PhoneRegex, ErrorMessage = "Phone number must be in XXX-XXX-XXXX format")]
        public string WorkPhone { get; set; }

        [Display(Name = "Primary Email Address")]
        public string EmailAddress { get; set; }

        #endregion
        //--------------------------------------------------------------

        #region Garage/Mail Address

        [Display(Name = "Same as Garage?")]
        public bool SameAsGarage { get; set; }
        //####################################################

        [Required]
        [MaxLength(500, ErrorMessage = "Maximum length for a street address is 500 characters")]
        public string GarageStreetAddress { get; set; }

        public string GarageCity { get; set; }

        [Required]
        public string GarageState { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        [Required]
        public string GarageZIPCode { get; set; }

        [Display(Name = "County")]
        public string GarageCounty { get; set; }
        //####################################################


        [Required]
        [MaxLength(500, ErrorMessage = "Maximum length for a street address is 500 characters")]
        public string MailingStreetAddress { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        [Required]
        public string MailingZIPCode { get; set; }

        #endregion
        //--------------------------------------------------------------

        #region Coverages and Discounts

        [Display(Name = "PIP Deductible")]
        public string PipDeductible { get; set; }

        [Display(Name = "Work Loss Exclusion")]
        public bool? Workloss { get; set; }

        public int WorkLossGroup { get; set; }

        [Display(Name = "Named Insured Only")]
        public bool? NIO { get; set; }

        [Display(Name = "Named Insured Res Relatives")]
        public bool? NIRR { get; set; }

        [Display(Name = "None")]
        public bool None { get; set; }

        [Display(Name = "Direct Repair Program")]
        public bool DirectRepairDiscount { get; set; }

        [Display(Name = "PD Coverage")]
        public string PdLimit { get; set; }

        [Display(Name = "Medical Payments")]
        public string MedPayLimit { get; set; }

        [Display(Name = "BI Coverage")]
        public string BiLimit { get; set; }

        [Display(Name = "Prior BI Coverage")]
        public string PriorBiLimit { get; set; }

        [Display(Name = "UM:")]
        public string UmLimit { get; set; }

        [Display(Name = "UM Stacked")]
        public bool UMstacked { get; set; }

        [Display(Name = "ADND Limit")]
        public string AdndLimit { get; set; }

        [Display(Name = "COMP")]
        //[Compare("CollDed", ErrorMessage = "COMP and COLL must match")]
        public string CompDed { get; set; }

        [Display(Name = "COLL")]
        //[Compare("CompDed", ErrorMessage = "COMP and COLL must match")]
        public string CollDed { get; set; }

        [Display(Name = "Renewal")]
        public bool HasRenewalDisc { get; set; }

        [Display(Name = "Transfer")]
        public int TransDisc { get; set; }

        [Display(Name = "PPO")]
        public bool HasPPO { get; set; }

        [Display(Name = "Homeowners")]
        public bool HasHomeownersDisc { get; set; }

        [Display(Name = "Loss Of Use Coverage?(30/day 900 max)")]
        public bool HasLou { get; set; }
        #endregion
        //--------------------------------------------------------------

        ////////////////////////////////
        // Not Sure If I need Anymore
        public int LapseType { get; set; }

        //[Display(Name = "No Lapse")]
        public bool LapseNone { get; set; }

        //[Display(Name = "1-10 Days")]
        public bool Lapse110 { get; set; }

        //[Display(Name = "11-30 Days")]
        public bool Lapse1131 { get; set; }

        //[Display(Name = "Over 30 Days")]
        public bool Lapse30Plus { get; set; }
        public double MaintenanceFee { get; set; }
        public double PIPPDOnlyFee { get; set; }
        //----------------------------------------------------------

        [UIHint("EndorseDrivers")]
        public List<EndorsementDriverModel> Drivers { get; set; }

        [UIHint("EndorseVehicles")]
        public List<EndorsementVehicleModel> Vehicles { get; set; }

        public List<EndorsementInsallmentsModel> EndorsementInstallments { get; set; }


        public List<AgentEndorseReasons> SelectedReasons { get; set; }

        public string HighlightField(string reasonCode, string licenseno = null, string vin = null)
        {
            if (SelectedReasons!= null && SelectedReasons.Any(x => x.ReasonCode == reasonCode &&
                        (licenseno == null || x.LicenseNo == licenseno) &&
                        (vin == null || x.Vin == vin)))
            {
                return "color:red;background-color:yellow";
            }
            return string.Empty;
        }
        public string HighlightField(string licenseno = null, string vin = null, params string[] reasonCodes)
        {
            if (SelectedReasons!=null && SelectedReasons.Any(x => reasonCodes.Contains(x.ReasonCode) &&
                        (licenseno == null || x.LicenseNo == licenseno) &&
                        (vin == null || x.Vin == vin)))
            {
                return "color:red;background-color:yellow";
            }
            return string.Empty;
        }
        public string HighlightField(params string[] reasonCodes)
        {
            return HighlightField(null, null, reasonCodes);
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            #region Check that addresses are in state

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                if (GarageState != "FL")
                {
                    yield return new ValidationResult("Garage Address Cannot Be Out Of State (FL)");
                    yield return new ValidationResult("Garage Address Cannot Be Out Of State (FL)",
                        new[] { "GarageState" });
                }

                if (MailingState != "FL")
                {
                    yield return new ValidationResult("Mailing Address Cannot Be Out Of State (FL)");
                    yield return new ValidationResult("Mailing Address Cannot Be Out Of State (FL)",
                            new[] { "MailingState" })
                        ;
                }

                #endregion

                #region Prevent binding of annual policies

                if (PolicyTermMonths != 6)
                {
                    yield return new ValidationResult("Only 6 Month Policies May Be Issued Through The Portal");
                }

                #endregion

                #region Mark sure work loss has a value

                if (!Workloss.HasValue)
                {
                    yield return new ValidationResult("Please select a value for Work Loss Exclusion");
                    yield return new ValidationResult("Please select a value for Work Loss Exclusion",
                        new[] { "Workloss" });
                }

                #endregion

                #region Check For EffDate In Future

                if (EffectiveDate > DateTime.Now.AddDays(30))
                {
                    yield return new ValidationResult(
                        "Effective Date of the quote cannot be greater than 30 days in the future.");
                    yield return new ValidationResult(
                        "Effective Date of the quote cannot be greater than 30 days in the future.",
                        new[] { "EffectiveDate" });
                }

                var date1 = new DateTime(EffectiveDate.Year, EffectiveDate.Month, EffectiveDate.Day, 0, 0, 0);
                var date2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                var effdateCompareRes = DateTime.Compare(date1, date2);

                if (effdateCompareRes < 0)
                {
                    yield return new ValidationResult("Effective Date of the quote cannot be in the past.");
                    yield return new ValidationResult("Effective Date of the quote cannot be in the past.",
                        new[] { "EffectiveDate" });
                }

                #endregion

                #region Check Cars/Drivers

                int drivercount =
                    new XPCollection<PolicyQuoteDrivers>(uow,
                        CriteriaOperator.Parse("Exclude != '1' AND PolicyNo = ?",
                            Sessions.Instance.PolicyNo)).Count;
                int carscount =
                    new XPCollection<PolicyQuoteCars>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?",
                            Sessions.Instance.PolicyNo)).Count;

                if (drivercount == 1)
                {
                    if (carscount > 2)
                    {
                        yield return
                            new ValidationResult(
                                $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).");
                    }
                }
                else if (drivercount == 2)
                {
                    if (carscount > 3)
                    {
                        yield return
                            new ValidationResult(
                                $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).");
                    }
                }
                else if (drivercount == 3)
                {
                    if (carscount > 4)
                    {
                        yield return
                            new ValidationResult(
                                $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).");
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Policy For Phy Dam On Car/Check if VIN is < 17 characters && Has PhyDam and Age > 15 years old && Model year <= 1980

                bool hasPhyDam = false;
                int carIndexID = 0;

                var getCars = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                getCars.Reload();

                foreach (var car in getCars)
                {
                    #region Check if has PhyDam

                    if (car.HasPhyDam)
                    {
                        hasPhyDam = true;
                        carIndexID = car.CarIndex;
                    }

                    #endregion

                    #region Check Vin Length/Populated

                    if (string.IsNullOrEmpty(car.VIN))
                    {
                        yield return
                            new ValidationResult(
                                String.Format("Car {0} requires a VIN to be entered", car.CarIndex));
                    }

                    if (!string.IsNullOrEmpty(car.VIN))
                    {
                        if (car.VIN.Length < 17)
                        {
                            yield return
                                new ValidationResult(String.Format("Car {0} Appears to have an incomplete VIN",
                                    car.CarIndex));
                        }
                    }

                    #endregion

                    if (car.BusinessUse)
                    {
                        yield return new ValidationResult(string.Format("Car {0} is used for business.",
                            car.CarIndex));
                    } //LF 12/16/18

                    int vehicleAge = EffectiveDate.Month >= 10
                        ? DateTime.Now.AddYears(1).Year - car.VehYear
                        : DateTime.Now.Year - car.VehYear;
                    //LF 2/19/19  if (car.HasPhyDam && vehicleAge > 15)
                    if (car.HasPhyDam && vehicleAge > 20)
                    {
                        yield return new ValidationResult(string.Format(
                            "Car {0} has physical damage and its model year is older than 20 years.",
                            car.CarIndex));
                    }
                    //-------------------------------------------------------
                    //LF 2/19/19 commented out AA
                    /*if (car.VehYear <= 1980)
                    {
                        yield return new ValidationResult(string.Format("Car {0} model year is older than 1980", car.CarIndex));
                    } */
                    //-------------------------------------------------------
                    /*  LF 2/17/19 replacement code below, call IsAcceptableVehicle once, not twice
                    if (!UtilitiesVehicles.IsAcceptableVehicle(car.VehMake, car.VehModel1, car.VehModel2))
                    {
                        yield return new ValidationResult(string.Format("Car {0} is flagged as unacceptable", car.CarIndex));
                    }
                    //-------------------------------------------------------
                    if (car.HasPhyDam)
                    {
                        if (!UtilitiesVehicles.IsValidCompSymbol(car.COMPSymbol) && vehicleAge <= 5 && UtilitiesVehicles.IsAcceptableVehicle(car.VehMake, car.VehModel1, car.VehModel2))
                        {
                            yield return new ValidationResult(string.Format("Vehicle {0} Is Not Acceptable For Physical Damage (Comp Symbol {1})", car.CarIndex, car.COMPSymbol));
                        }
                    }  */

                    bool isAcceptableVeh =
                        UtilitiesVehicles.IsAcceptableVehicle(car.VehMake, car.VehModel1, car.VehModel2);
                    if (!isAcceptableVeh)
                    {
                        yield return new ValidationResult(string.Format("Car {0} is flagged as unacceptable",
                            car.CarIndex));
                    }
                } //END Loop through Cars Data


                XPCollection<XpoAARateTerritory> rateTerritories = new XPCollection<XpoAARateTerritory>(uow);
                if (!rateTerritories.Any(r => r.ZIP.Equals(GarageZIPCode)))
                {
                    yield return new ValidationResult("Invalid Zip code. Contact UW for assistance.",
                        new[] { "GarageZIPCode" });
                }


                #region Comp/Coll Deductible Amt

                if (hasPhyDam && !string.IsNullOrEmpty(CompDed) && (CompDed == "0" || CompDed == "NONE"))
                {
                    yield return
                        new ValidationResult(
                            "A vehicle on this quote has physical damage, must select Comp/Coll deductibles.");
                    yield return new ValidationResult("Must select Comp/Coll deductibles.", new[] { "CompDed" });
                    yield return new ValidationResult("Must select Comp/Coll deductibles.", new[] { "CollDed" });
                }

                if (!hasPhyDam && !string.IsNullOrEmpty(CompDed) && CompDed != "0" && CompDed != "NONE")
                {
                    yield return
                        new ValidationResult(
                            "You have selected Comp/Coll deductibles but no vehicle has Physical Damage Selected. Please select Physical Damage on a vehicle.");
                }

                #endregion


                if (hasPhyDam)
                {
                    if (CompDed != CollDed)
                    {
                        yield return new ValidationResult("Comp and Coll deductibles must match");
                        yield return new ValidationResult("Must match Coll Deductible.", new[] { "CompDed" });
                        yield return new ValidationResult("Must match Comp Deductible.", new[] { "CollDed" });
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Direct Repair/Phy Dam

                if (!hasPhyDam && DirectRepairDiscount)
                {
                    yield return new ValidationResult(
                        "In order to select Direct Repair Discount a vehicle on this policy must have Physical Damage.");
                    yield return new ValidationResult(
                        "In order to select Direct Repair Discount a vehicle on this policy must have Physical Damage.",
                        new[] { "CollDed" });
                }

                #endregion

                #region Check For Drivers > 10 Points Violations && Check For DUI Violations && Check for complete driver info. && Check Married Drivers

                bool thasSr22 = false;
                bool hasMaxViolations = false;
                bool isDuiViolation = false;
                int countMarriedDrivers = 0;
                int excludedDriverCount = 0;
                List<string> listMaxViolErrMsg = new List<string>();
                List<string> listNaughtyDrivers = new List<string>();
                List<PolicyQuoteDrivers> listDrivers = new List<PolicyQuoteDrivers>();

        
                    var getDrivers = new XPCollection<PolicyQuoteDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                    if (getDrivers.All(d => d.Exclude))
                    {
                        yield return new ValidationResult("All drivers cannot be excluded");
                    }

                    foreach (var drivers in getDrivers)
                    {
                        if (drivers.SR22)
                        {
                            thasSr22 = true;
                        }

                        //-----------------------------------
                        if (drivers.Exclude)
                        {
                            excludedDriverCount += 1;
                        }
                        //Save For Rainy Day - May Need Later - Dont IMplement Just Yet
                        //if (drivers.SR22 && (drivers.RelationToInsured != "INSURED" && drivers.RelationToInsured != "SPOUSE"))
                        //{
                        //    yield return new ValidationResult(string.Format("Driver {0} - {1} - has sr22 and is neither the Insured or Spouse.", drivers.DriverIndex, drivers.FirstName + " " + drivers.LastName));
                        //}
                        //-----------------------------------

                        if (PortalUtils.GetPortalOption("NONINSUREDRELATIONSHIP"))
                        {
                            if (drivers.DriverIndex > 2 && drivers.RelationToInsured == "INSURED")
                            {
                                yield return new ValidationResult(string.Format(
                                    "Driver {0} - {1} - has been listed as a driver with Relationship type as Insured, please select correct relationship type.",
                                    drivers.DriverIndex, drivers.FirstName + " " + drivers.LastName));
                            }
                        }

                        if (string.IsNullOrEmpty(drivers.FirstName) || string.IsNullOrEmpty(drivers.LastName))
                        {
                            yield return new ValidationResult(string.Format(
                                "Driver {0} has incomplete information, please fill required information",
                                drivers.DriverIndex));
                        }

                        if (drivers.DriverIndex == 1 && drivers.Exclude)
                        {
                            yield return new ValidationResult(
                                string.Format("Drivers: The named insured cannot be excluded", drivers.DriverIndex));
                        }

                        listDrivers.Add(drivers);
                    }

                    //-----------------------------------
                    // Check Married Drivers -> If not even then require another driver
                    int modMarried = getDrivers.Count(m => m.Married) % 2;
                    if (modMarried != 0)
                    {
                        yield return new ValidationResult(string.Format(
                            "There are an uneven amount of married drivers on the policy, please add an additional married driver"));
                    }

                    //-----------------------------------
                    for (int i = 0; i < listDrivers.Count; i++)
                    {
                        var getViolations = new XPCollection<PolicyQuoteDriversViolations>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?",
                                Sessions.Instance.PolicyNo, listDrivers[i].DriverIndex));
                        int totalPoints = 0;
                        bool hasDUI = false;
                        int afAccs = 0; //LF 12/16/18 added
                        int nfAccs = 0;
                        bool inexperienced =
                            DateTime.Now <
                            listDrivers[i].DateLicensed.AddYears(3); // listDrivers[i].Inexp;  //LF 2/19/19 added for AA


                        foreach (PolicyQuoteDriversViolations violations in getViolations)
                        {
                            totalPoints += violations.ViolationPoints;
                            DateTime octDate = "10/1/2007".Parse<DateTime>();
                            int dateCompRes = DateTime.Compare(octDate, violations.ViolationDate);

                            if (dateCompRes < 0)
                            {
                                if (violations.ViolationGroup.Upper() == "DUI")
                                {
                                    hasDUI = true;
                                    isDuiViolation = true;
                                }
                            }

                            //LF 2/21/19
                            //added following block of code for new AA Unacceptable Operator
                            //1.   Any  operator/driver/resident  with  any  DUI  or  DWI  conviction  prior  to  10/01/07  with  or  in conjunction with other chargeable violation(s)/accidents; 
                            // not sure that field Chargeable is correct, don't see code anywhere that sets it
                            if (dateCompRes >= 0) //violation occurred on or prior to 10/1/07
                            {
                                if (violations.ViolationGroup.Upper() == "DUI")
                                {
                                    foreach (PolicyQuoteDriversViolations tempViolation in getViolations)
                                    {
                                        if (tempViolation.ViolationDate == violations.ViolationDate &&
                                            tempViolation.ViolationGroup.Upper() !=
                                            "DUI" /* && tempViolation.Chargeable ? */)
                                        {
                                            hasDUI = true;
                                            isDuiViolation = true;
                                        }
                                    }
                                }
                            }

                            if (violations.ViolationCode == "1" || violations.ViolationCode == "ACC")
                            {
                                if (violations.ViolationDate >= DateTime.Now.AddMonths(-35))
                                    afAccs += 1;
                            }

                            if (violations.ViolationCode == "2" || violations.ViolationCode == "ACN")
                            {
                                if (violations.ViolationDate >= DateTime.Now.AddMonths(-35))
                                    nfAccs += 1;
                            }

                        }

                        //LF 2/19/19 commented out old, new below, AA
                        /*
                        if (totalPoints > 10)
                        {
                            hasMaxViolations = true;
                            listMaxViolErrMsg.Add(string.Format("Driver {2} - {0} {1} - has more than 10 points in violations.", listDrivers[i].FirstName, listDrivers[i].LastName, listDrivers[i].DriverIndex));
                        }
                        */

                        if (totalPoints >= 7)
                        {
                            hasMaxViolations = true;
                            listMaxViolErrMsg.Add(string.Format(
                                "Driver {2} - {0} {1} - has 7 or more points in violations.", listDrivers[i].FirstName,
                                listDrivers[i].LastName, listDrivers[i].DriverIndex));
                        }

                        //LF 2/19/19 added, AA
                        if (inexperienced && totalPoints >= 5)
                        {
                            hasMaxViolations = true;
                            listMaxViolErrMsg.Add(string.Format(
                                "Driver {2} - {0} {1} - Inexperienced operator and has 5 or more points in violations.",
                                listDrivers[i].FirstName, listDrivers[i].LastName, listDrivers[i].DriverIndex));
                        }

                        if (hasDUI)
                        {
                            isDuiViolation = hasDUI;
                            listNaughtyDrivers.Add(string.Format(
                                "Driver {0} - {1} {2} - has a violation related to DUI.", listDrivers[i].DriverIndex,
                                listDrivers[i].FirstName, listDrivers[i].LastName));
                        }

                        if (afAccs >= 2)
                        {
                            listNaughtyDrivers.Add(string.Format(
                                "Driver {0} - {1} {2} - has 2 or more At Fault Accident violations.",
                                listDrivers[i].DriverIndex, listDrivers[i].FirstName, listDrivers[i].LastName));
                        }
                        else if (nfAccs + afAccs >= 3)
                        {
                            listNaughtyDrivers.Add(string.Format(
                                "Driver {0} - {1} {2} - has 3 or more Accident violations.", listDrivers[i].DriverIndex,
                                listDrivers[i].FirstName, listDrivers[i].LastName));
                        }


                        if (listDrivers[i].Gender == "SELECT")
                        {
                            listNaughtyDrivers.Add(string.Format(
                                "Driver {0} - {1} {2} - needs a correct gender selected", listDrivers[i].DriverIndex,
                                listDrivers[i].FirstName, listDrivers[i].LastName));
                        }

                        if (listDrivers[i].DriverIndex == 1 &&
                            listDrivers[i].DateLicensed.Ticks > DateTime.Now.AddYears(-1).Ticks)
                        {
                            listNaughtyDrivers.Add(string.Format(
                                "Driver {0} - {1} {2} - Insured must have a year of driving experience",
                                listDrivers[i].DriverIndex, listDrivers[i].FirstName, listDrivers[i].LastName));
                        }
                    }

                HasSr22 = thasSr22;
                if (hasMaxViolations)
                {
                    for (int i = 0; i < listMaxViolErrMsg.Count; i++)
                    {
                        yield return new ValidationResult(listMaxViolErrMsg[i]);
                    }
                }

                if (listNaughtyDrivers.Count > 0)
                {
                    for (int i = 0; i < listNaughtyDrivers.Count; i++)
                    {
                        yield return new ValidationResult(listNaughtyDrivers[i]);
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check for > 2 Excluded Drivers

                if (excludedDriverCount > 2)
                {
                    yield return new ValidationResult("This quote has more than 2 excluded drivers");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check For SR22

                if (thasSr22)
                {
                    if (BiLimit.Upper() == "NONE")
                    {
                        yield return new ValidationResult("A Driver has SR22, BI limit must be selected.");
                        yield return new ValidationResult("A Driver has SR22, BI limit must be selected.",
                            new[] { "BiLimit" });
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Number Of Drivers

                if (Drivers == null || Drivers.Count == 0)
                {
                    yield return new ValidationResult("A quote must have at least one driver");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Number Of Vehicles

                if (Vehicles == null || Vehicles.Count == 0)
                {
                    yield return new ValidationResult("A quote must have at least one vehicle");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check if BI is selected if UM type is not null

                if (UmLimit.Upper() != "NONE")
                {
                    if (BiLimit.Upper() == "NONE")
                    {
                        yield return new ValidationResult("BI Coverage must be selected.");
                        yield return new ValidationResult("BI Coverage must be selected.", new[] { "BiLimit" });
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------


                #region Bad County Check

                bool isNaughtyCounty = PortalUtils.IsBadCounty(GarageZIPCode, EffectiveDate);

                if (isNaughtyCounty)
                {
                    yield return new ValidationResult("Zipcode is unacceptable for new business",
                        new[] { "GarageZIPCode" });
                    yield return new ValidationResult("Zipcode is unacceptable for new business");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check For Naughty Garage Address

                if (GarageStreetAddress.Upper().Contains("PO BOX") ||
                    GarageStreetAddress.Upper().Contains("P.O. BOX") ||
                    GarageStreetAddress.Upper().Contains("P.O BOX") || GarageStreetAddress.Upper().Contains("PO. BOX"))
                {
                    yield return new ValidationResult("The address given cannot be a PO Box.",
                        new[] { "GarageStreetAddress" });
                    yield return new ValidationResult("The address given cannot be a PO Box.");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Don't Re-write NonRenewed/Accident Insured

                if (PortalUtils.GetPortalOption("BLOCKNONRENEWEDINSURED"))
                {
               
                        var isBadInsured = false;
                        var listRealNaughtyDrivers = new List<string>();
                        var listNaughtyCars = new List<string>();

                        var driversForPolicy = new XPCollection<PolicyQuoteDrivers>(uow,
                            CriteriaOperator.Parse("PolicyNo = ?", EndorsementID));
                        var carsForPolicy = new XPCollection<PolicyQuoteCars>(uow,
                            CriteriaOperator.Parse("PolicyNo = ?", EndorsementID));

                        foreach (var getPolDrivers in driversForPolicy.Select(driver =>
                            new XPCollection<PolicyDrivers>(uow,
                                CriteriaOperator.Parse("LastName = ? AND (LicenseNo = ? OR LicenseNo = ?)",
                                    driver.LastName, driver.LicenseNo, driver.LicenseNo.Replace("-", "")))))
                        {
                            listRealNaughtyDrivers.AddRange(getPolDrivers.Select(driver => driver.PolicyNo));
                        }

                        foreach (var getPolCars in carsForPolicy.Select(car =>
                            new XPCollection<PolicyCars>(uow, CriteriaOperator.Parse("VIN = ?", car.VIN))))
                        {
                            listNaughtyCars.AddRange(getPolCars.Select(car => car.PolicyNo));
                        }

                        IEnumerable<string> badPolicies = listRealNaughtyDrivers.Intersect(listNaughtyCars);

                        foreach (var policy in badPolicies)
                        {
                            var getpolicy =
                                new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policy));

                            foreach (var badPolicy in getpolicy.Where(badPolicy =>
                                badPolicy.PolicyStatus == "NONRENEW" || badPolicy.RenewalStatus == "NON-RENEW"))
                            {
                                isBadInsured = true;
                                string msg =
                                    string.Format(
                                        "{0} - Quote cannot be allowed to bind. Previous Policy {1} was non-renewed",
                                        EndorsementID, badPolicy.PolicyNo);

                                PortalUtils.PostUndAlert(msg);
                            }
                        }

                        if (isBadInsured)
                        {
                            yield return new ValidationResult(
                                "This policy is ineligible to be bound due to a drivers previous history");
                        }
                    } //END Using
                 //END Don't re-write an accident/non-renewed insured

                #endregion

                //--------------------------------------------------------------------------------------------
            }
        }
    }

    public class EndorsementInsallmentsModel
    {
        public string PolicyNo { get; set; }

        public decimal InstallNo { get; set; }

        public string Descr { get; set; }

        public DateTime? DueDate { get; set; }

        public decimal CurrInstallmentAmount { get; set; }

        public decimal PymtApplied { get; set; }

        public decimal CurrentBalance { get; set; }

        public decimal NewBalance { get; set; }

        public decimal EndorseChangeAmount { get; set; }

        public decimal PremiumeChangeAmount { get; set; }

        public decimal PremiumeAmount { get; set; }

        public decimal OldPremiumeAmount { get; set; }

        public decimal Fees { get; set; }

        public decimal TotalDue { get; set; }
    }

    public class PaymentScheduleModel
    {
        public string PolicyNo { get; set; }

        public decimal InstallmentNo { get; set; }

        public string Description { get; set; }

        public DateTime? DueDate { get; set; }

        public decimal InstallmentAmount { get; set; }

        public decimal PaymentsApplied { get; set; }

        public decimal CurrentBalance { get; set; }
    }

    public class PaymentsDataForPayScheduleModel
    {
        public string Confirmation { get; set; }

        public DateTime? PostmarkDate { get; set; }

        public string PaymentType { get; set; }

        public decimal TotalAmt { get; set; }

        public decimal FeeAmt { get; set; }

        public string PostedBy { get; set; }
    }


    public class AgentViewModel
    {
        public string AgentCode { get; set; }
        public string PrimaryFirstName { get; set; }
        public string PrimaryLastName { get; set; }
        public bool IsOwnerOperator { get; set; }
        public string DBA { get; set; }
        public string MailAddress { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailZIP { get; set; }
        public string PhysicalAddress { get; set; }
        public string PhysicalCity { get; set; }
        public string PhysicalState { get; set; }
        public string PhysicalZIP { get; set; }
        public string County { get; set; }
        public string FEIN { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string MarketingRep { get; set; }
        public string Group { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime DateLicensed { get; set; }
        public string InitialContact { get; set; }
        public bool IsAllowedHigherBI { get; set; }
        public bool IsPaperless { get; set; }
        public string Status { get; set; }
        public DateTime StatusDate { get; set; }
        public bool IsProbation { get; set; }
        public string TerminationCause { get; set; }
        public string LastReview { get; set; }
        public int OID { get; set; }
        public double CommissionRate { get; set; }
        public bool MailGarageSame { get; set; }
        public string MarketingRepUserID { get; set; }
        public string Password { get; set; }
        public string AgentGuid { get; set; }
        public bool IsRequireCall { get; set; }
    }

    public class AgentEndorseReasons
    {
        public string Reason { get; set; }
        public string ReasonCode { get; set; }
        public bool IsSpecific { get; set; }
        public string Vin { get; set; }
        public string LicenseNo { get; set; }
        public string DriverFullName { get; set; }

    }

    public class AllAgentEndorseReasons
    {
        public List<AgentEndorseReasons> Selected { get; set; }

        public List<AgentEndorseReasons> UnSelected { get; set; }

        public List<AgentEndorseReasons> Missing { get; set; }
    }
}//END Namespace