﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AgentPortal.Support;
using AgentPortal.Support.Attributes;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Utils.Text.Internal;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using Newtonsoft.Json;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Xpo;
using PolicyCarLienHolders = AgentPortal.Support.DataObjects.PolicyCarLienHolders;

namespace AgentPortal.Models
{
    public class QuoteSearchModel
    {
        [Display(Name = "Quote Number")]
        [AtLeastOneRequired("QuoteNumber", "InsuredName", "DriversLicense", "VehicleIdentificationNumber")]
        public string QuoteNumber { get; set; }

        [Display(Name = "Insured Name")]
        public string InsuredName { get; set; }

        [Display(Name = "Drivers License Number")]
        public string DriversLicense { get; set; }

        [Display(Name = "VIN")]
        public string VehicleIdentificationNumber { get; set; }
    }//END QuoteSearchModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class QuoteModel : IValidatableObject
    {
        //Quote Model Specific
        public static readonly int[] TermMonthsChoices = new int[] { 6, 12 };
        public string QuoteId { get; set; }
        public string AgentCode { get; set; }
        public string CreditMsg { get; set; }

        public bool RequestESignature { get; set; }

        public int PayPlan { get; set; }
        public bool HasSr22 { get; set; }
        public bool IsEditedPalms { get; set; }

        public bool Paperless { get; set; }

        public bool AdvancedQuote { get; set; }

        public SelectedData InfinityRatingRelativities { get; set; }

        public DateTime DateBound { get; set; }
        public DateTime QuotedDate { get; set; }
        public bool IsShow { get; set; }

        public string InsuredPreferredLanguage { get; set; }

        #region General Policy Info

        public string CreditGuid { get; set; }

        public string PriorPolicyTerm { get; set; }


        [Display(Name = "Rating State")]
        public string RatingState { get; set; }

        public int PolicyTermMonths { get; set; }
        public int EstimatedCreditScore { get; set; }
        public int CreditScore { get; set; }

        [Display(Name = "Prior Coverage")]
        public bool PriorCoverage { get; set; }

        [Display(Name = "Prior Policy Had BI")]
        public bool PreviousBI { get; set; }

        [Display(Name = "Prior Company")]
        //[Contains("foo", ErrorMessage = "Second must contain the word 'foo'")]
        public string PriorCompany { get; set; }

        [Display(Name = "Prior Policy No")]
        public string PriorPolicyNo { get; set; }

        [Display(Name = "Effective Date")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Effective Date must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime EffectiveDate { get; set; }

        [Display(Name = "Expiration Date")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Expiration Date must be in the format of mm/dd/yyyy")]
        public DateTime ExpirationDate { get; set; }

        private DateTime? _PreviousExpDate;
        [Display(Name = "Prior Policy Exp Date")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Previous Expiration Date must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime? PreviousExpDate
        {
            get
            {
                return _PreviousExpDate == "1/1/0001".Parse<DateTime>() ? null : _PreviousExpDate;
            }
            set
            {
                _PreviousExpDate = value;
            }
        }

        public string RaterType { get; set; }

        #endregion
        //--------------------------------------------------------------

        #region Insured Information

        [Required]
        public string FirstName1 { get; set; }

        [StringLength(1, ErrorMessage = "This initial should be only one character")]
        public string MiddleInitial1 { get; set; }

        [Required]
        public string LastName1 { get; set; }

        public string Suffix1 { get; set; }
        //#############################################

        public string FirstName2 { get; set; }

        [StringLength(1, ErrorMessage = "This initial should be only one character")]
        public string MiddleInitial2 { get; set; }

        public string LastName2 { get; set; }

        public string Suffix2 { get; set; }
        //#############################################

        [Display(Name = "Home")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(Constants.PhoneRegex, ErrorMessage = "Phone number must be in XXX-XXX-XXXX format")]
        [Required]
        public string HomePhone { get; set; }

        [Display(Name = "Work")]
        [DataType(DataType.PhoneNumber)]
        // [RegularExpression(Constants.PhoneRegex, ErrorMessage = "Phone number must be in XXX-XXX-XXXX format")]
        public string WorkPhone { get; set; }

        [Display(Name = "Primary Email Address")]
        [Required]
        //        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|edu|mil|biz|info|mobi|name|aero|jobs|museum)\b", ErrorMessage = "Please enter a valid email address.")]
        //[RegularExpression(@"[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|edu|mil|biz|info|mobi|name|aero|jobs|museum)\b", ErrorMessage = "Please enter a valid email address.")]
        public string EmailAddress { get; set; }

        #endregion
        //--------------------------------------------------------------

        #region Garage/Mail Address

        [Display(Name = "Same as Garage?")]
        public bool SameAsGarage { get; set; }
        //####################################################

        [Required]
        [MaxLength(500, ErrorMessage = "Maximum length for a street address is 500 characters")]
        public string GarageStreetAddress { get; set; }

        public string GarageCity { get; set; }

        [Required]
        public string GarageState { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        [Required]
        public string GarageZIPCode { get; set; }

        [Display(Name = "County")]
        public string GarageCounty { get; set; }
        //####################################################

        [Required]
        [MaxLength(500, ErrorMessage = "Maximum length for a street address is 500 characters")]
        public string MailingStreetAddress { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        [Required]
        public string MailingZIPCode { get; set; }

        #endregion
        //--------------------------------------------------------------

        #region Coverages and Discounts

        [Display(Name = "PIP Deductible")]
        public string PipDeductible { get; set; }

        [Display(Name = "Work Loss Exclusion")]
        public bool? Workloss { get; set; }

        public int WorkLossGroup { get; set; }

        [Display(Name = "Named Insured Only")]
        public bool NIO { get; set; }

        [Display(Name = "Named Insured Res Relatives")]
        public bool NIRR { get; set; }

        [Display(Name = "None")]
        public bool None { get; set; }

        [Display(Name = "Direct Repair Program")]
        public bool DirectRepairDiscount { get; set; }

        [Display(Name = "PD Coverage")]
        public string PdLimit { get; set; }

        [Display(Name = "Medical Payments")]
        public string MedPayLimit { get; set; }

        [Display(Name = "BI Coverage")]
        public string BiLimit { get; set; }

        [Display(Name = "Prior BI Coverage")]
        public string PriorBiLimit { get; set; }

        [Display(Name = "UM:")]
        public string UmLimit { get; set; }

        [Display(Name = "UM Stacked")]
        public bool UMstacked { get; set; }

        [Display(Name = "ADND Limit")]
        public string AdndLimit { get; set; }

        [Display(Name = "COMP")]
        //[Compare("CollDed", ErrorMessage = "COMP and COLL must match")]
        public string CompDed { get; set; }

        [Display(Name = "COLL")]
        //[Compare("CompDed", ErrorMessage = "COMP and COLL must match")]
        public string CollDed { get; set; }

        [Display(Name = "Renewal")]
        public bool HasRenewalDisc { get; set; }

        [Display(Name = "Transfer")] public int TransDisc { get; set; }

        [Display(Name = "PPO")] public bool HasPPO { get; set; } = true;

        [Display(Name = "Homeowners")]
        public bool HasHomeownersDisc { get; set; }

        [Display(Name = "Loss Of Use Coverage?(30/day 900 max)")]
        public bool HasLou { get; set; }

        [Display(Name = "Current Customer")]
        public bool IsCurrentCustomer { get; set; }
        #endregion
        //--------------------------------------------------------------

        ////////////////////////////////
        // Not Sure If I need Anymore
        public int LapseType { get; set; }

        [Display(Name = "No Lapse")]
        public bool LapseNone { get; set; }

        [Display(Name = "1-10 Days")]
        public bool Lapse110 { get; set; }

        [Display(Name = "11-30 Days")]
        public bool Lapse1131 { get; set; }

        [Display(Name = "Over 30 Days")]
        public bool Lapse30Plus { get; set; }
        //----------------------------------------------------------

        public string ReWrittenFromPolicyNo { get; set; }

        [UIHint("Drivers")]
        public List<DriverModel> Drivers { get; set; }

        [UIHint("Vehicles")]
        public List<VehicleModel> Vehicles { get; set; }

        public int TotalVehiclesWithPhysicalDamage { get; set; }

        public int TotalPhysicalDamageImageUploads { get; set; }

        public IList<AuditImageIndex> PhysicalDamageImages { get; set; }

        public string AgentESignEmailAddress { get; set; }

        private string GetCarErrorMsgData(PolicyQuoteCars car)
        {
            if(car == null)
                return string.Empty;
            return $" {car.CarIndex} ({car.VehYear}, {car.VehMake}, {car.VehModel1}) ";
        }
        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {

                var quoteDb = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));

                int excludedDriverCount = 0;
                if (string.IsNullOrEmpty(EmailAddress))
                {
                    yield return new ValidationResult("Email Address is required");
                }

                #region Check that addresses are in state

                if (GarageState != "FL")
                {
                    yield return new ValidationResult("Garage Address Cannot Be Out Of State (FL)");
                    yield return new ValidationResult("Garage Address Cannot Be Out Of State (FL)",
                        new[] { "GarageState" });
                }

                if (MailingState != "FL")
                {
                    yield return new ValidationResult("Mailing Address Cannot Be Out Of State (FL)");
                    yield return new ValidationResult("Mailing Address Cannot Be Out Of State (FL)",
                            new[] { "MailingState" })
                        ;
                }

                #endregion

                #region Prevent binding of annual policies

                if (PolicyTermMonths != 6)
                {
                    yield return new ValidationResult("Only 6 Month Policies May Be Issued Through The Portal");
                }

                #endregion

                #region Mark sure work loss has a value

                if (!Workloss.HasValue)
                {
                    yield return new ValidationResult("Please select a value for Work Loss Exclusion");
                    yield return new ValidationResult("Please select a value for Work Loss Exclusion",
                        new[] { "Workloss" });
                }

                #endregion

                #region Check For EffDate In Future

                if (EffectiveDate > DateTime.Now.AddDays(30))
                {
                    yield return new ValidationResult(
                        "Effective Date of the quote cannot be greater than 30 days in the future.");
                    yield return new ValidationResult(
                        "Effective Date of the quote cannot be greater than 30 days in the future.",
                        new[] { "EffectiveDate" });
                }

                var date1 = new DateTime(EffectiveDate.Year, EffectiveDate.Month, EffectiveDate.Day, 0, 0, 0);
                var date2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                var effdateCompareRes = DateTime.Compare(date1, date2);

                if (effdateCompareRes < 0)
                {
                    /*  LF 4/17/19
                                  yield return new ValidationResult("Effective Date of cannot be in the past.");
                                  yield return new ValidationResult("Effective Date of cannot be in the past.", new[] { "EffectiveDate" }); */
                    yield return new ValidationResult("Effective Date of the quote cannot be in the past.");
                    yield return new ValidationResult("Effective Date of the quote cannot be in the past.",
                        new[] { "EffectiveDate" });

                }

                #endregion

                #region Check Cars/Drivers

                excludedDriverCount =
                    new XPCollection<PolicyQuoteDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND Exclude = 1",
                            Sessions.Instance.PolicyNo)).Count;
                int drivercount =
                    new XPCollection<PolicyQuoteDrivers>(uow,
                        CriteriaOperator.Parse("Exclude != '1' AND PolicyNo = ?",
                            Sessions.Instance.PolicyNo)).Count;
                int carscount =
                    new XPCollection<PolicyQuoteCars>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?",
                            Sessions.Instance.PolicyNo)).Count;

                if (carscount > 5)
                {
                    yield return
                        new ValidationResult(
                            $"Quote cannot have more than 5 vehicles.");
                }
                if (excludedDriverCount > 0 && drivercount < carscount)
                {
                    yield return
                        new ValidationResult(
                            $"Quote with excluded drivers cannot have cars more than number of non-excluded drivers({drivercount}).");
                }
                else if (excludedDriverCount == 0 && drivercount + 1 < carscount)
                {
                    yield return
                          new ValidationResult($"Quote cannot have more than {drivercount + 1} cars with {drivercount} drivers on the policy.");
                }

                if (drivercount == 1)
                {
                    if (carscount > 2)
                    {
                        yield return
                            new ValidationResult(
                                $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).");
                    }
                }
                else if (drivercount == 2)
                {
                    if (carscount > 3)
                    {
                        yield return
                            new ValidationResult(
                                $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).");
                    }
                }
                else if (drivercount == 3)
                {
                    if (carscount > 4)
                    {
                        yield return
                            new ValidationResult(
                                $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).");
                    }
                }
               
                var getCars = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                getCars.Reload();

                XPCollection<PolicyCarLienHolders> lienHoldersForPolicy =
                    new XPCollection<PolicyCarLienHolders>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?",
                            Sessions.Instance.PolicyNo));
                var nonInstLHVehs = getCars.Where(x=> lienHoldersForPolicy.Any(l => l.IsNonInstitutional && x.CarIndex == l.CarIndex)).ToList();
                if (nonInstLHVehs.Any())
                {
                    if(nonInstLHVehs.Count() > 1)
                    {
                        yield return
                       new ValidationResult(
                           $"Unacceptable Risk: {string.Join(", ", nonInstLHVehs.Select(x => "Car "+ GetCarErrorMsgData( x)))} have non-institutional lienholder.");
                    }
                    else
                    {
                        yield return
                       new ValidationResult(
                           $"Unacceptable Risk: Car {GetCarErrorMsgData(nonInstLHVehs.First())} has non-institutional lienholder.");
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Policy For Phy Dam On Car/Check if VIN is < 17 characters && Has PhyDam and Age > 15 years old && Model year <= 1980

                bool hasPhyDam = false;
                int carIndexID = 0;

                foreach (var car in getCars)
                {
                    #region Check if has PhyDam

                    carIndexID = car.CarIndex;
                    if (car.HasPhyDam)
                    {
                        hasPhyDam = true;
                    }

                    #endregion

                    #region Check Vin Length/Populated

                    if (string.IsNullOrEmpty(car.VIN))
                    {
                        yield return
                            new ValidationResult(
                                String.Format("Car {0} requires a VIN to be entered", GetCarErrorMsgData(car)));
                    }

                    if (!string.IsNullOrEmpty(car.VIN))
                    {
                        if (car.VIN.Length < 17)
                        {
                            yield return
                                new ValidationResult(String.Format("Car {0} Appears to have an incomplete VIN",
                                GetCarErrorMsgData(car)));
                        }
                    }

                    #endregion

                    if (car.BusinessUse)
                    {
                        yield return new ValidationResult(string.Format("Car {0} is used for business.",
                            GetCarErrorMsgData(car)));
                    }

                    int vehicleAge = DateTime.Now.Month >= 10
                        ? DateTime.Now.AddYears(1).Year - car.VehYear
                        : DateTime.Now.Year - car.VehYear;
                    if (vehicleAge > 25)
                    {
                        yield return new ValidationResult(
                            string.Format("Car {0}'s model year is older than 25 years.", GetCarErrorMsgData(car)));
                    }

                    if (vehicleAge > 15 && car.HasPhyDam && !string.IsNullOrEmpty(car.CompDed) &&
                        !string.IsNullOrEmpty(car.CollDed))
                    {
                        yield return new ValidationResult(string.Format(
                            "Car {0}'s model year is older than 15 years and includes physical damage.",
                             GetCarErrorMsgData(car)));
                    }

                    if (!string.IsNullOrEmpty(car.GrossVehicleWeight) && !string.IsNullOrEmpty(car.ClassCode))
                    {
                        int weight = Convert.ToInt32(car.GrossVehicleWeight);
                        if ((car.ClassCode.Equals("70") || car.ClassCode.Equals("81") ||
                             car.ClassCode.Equals("83")) && weight > 7850)
                        {
                            yield return new ValidationResult(
                                $"Car {GetCarErrorMsgData(car)}'s weight is more than 7,850 lbs. and of class code: {car.ClassCode}.");
                        }
                        else if (weight > 10000)
                        {
                            yield return new ValidationResult(
                                string.Format("Car {0}'s weight is more than 10,000 lbs.",GetCarErrorMsgData(car)));
                        }
                    }

                    if (car.CostNew > 45000)
                    {
                        yield return new ValidationResult(string.Format(
                            "Unnacceptable Risk: Car {0} has a MSRP greater than $45,000.00", GetCarErrorMsgData(car)));
                    }

                    if (quoteDb != null && quoteDb.QuotedDate < new DateTime(2020, 6, 29)) //for quotes later than these date validating the VIN's info while adding the vehicle itself
                    {

                        var vehicleInfo = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, car.VIN)?.Vehicle;
                        if (vehicleInfo == null)
                        {
                            yield return new ValidationResult($"Car {GetCarErrorMsgData(car)}'s VIN {car.VIN} is not in our records.");
                        }
                        else
                        {
                            var misMatchParms = car.VehYear.ToString() != vehicleInfo.ModelYear ? "Year, " : string.Empty;
                            misMatchParms += car.VehMake != vehicleInfo.Make ? "Make, " : string.Empty;
                            misMatchParms += car.VehModel1 != vehicleInfo.FullModelName ? "Model, " : string.Empty;
                            misMatchParms += car.VehModel2 != vehicleInfo.BodyStyle ? "Style, " : string.Empty;

                            if (!string.IsNullOrEmpty(misMatchParms))
                            {

                                misMatchParms = misMatchParms.Trim().Substring(0, misMatchParms.Length - 2);
                                yield return new ValidationResult($"Car {car.CarIndex}'s {misMatchParms} not matching with the vehicle's VIN info in our records.");
                            }
                            else
                            {
                                bool isAcceptableVeh =
                                                        UtilitiesVehicles.IsAcceptableVehicle(car.VehMake, car.VehModel1, car.VehModel2);
                                if (!isAcceptableVeh)
                                {
                                    yield return new ValidationResult(string.Format("Car {0} is flagged as unacceptable",
                                         GetCarErrorMsgData(car)));
                                }
                            }
                        }
                    }

                    //-------------------------------------------------------
                    if (car.HasPhyDam)
                    {
                        if (vehicleAge > 6 && car.EngineType != null &&
                            (car.EngineType.Equals("H") || car.EngineType.Equals("E")))
                        {
                            string message = "Hybrid";
                            if (car.EngineType.Equals("E"))
                            {
                                message = "Electric";
                            }

                            yield return new ValidationResult(string.Format("Car {0} is older than 6 years and {1}",
                                 GetCarErrorMsgData(car), message));
                        }
                    }

                    #region Comp/Coll Deductible Amt

                    if (car.HasPhyDam && !string.IsNullOrEmpty(car.CompDed) && (car.CompDed == "0" || car.CompDed == "NONE"))
                    {
                        yield return
                            new ValidationResult(
                                $"Car {GetCarErrorMsgData(car)} on this quote has physical damage but no Comp/Coll deductibles. Please select Comp/Coll deductibles.");
                        yield return new ValidationResult("Must select Comp/Coll deductibles.", new[] { "CompDed" });
                        yield return new ValidationResult("Must select Comp/Coll deductibles.", new[] { "CollDed" });
                    }

                    if (!car.HasPhyDam && !string.IsNullOrEmpty(car.CompDed) && car.CompDed != "0" && car.CompDed != "NONE")
                    {
                        yield return
                            new ValidationResult(
                                $"You have selected Comp/Coll deductibles for Car {GetCarErrorMsgData(car)} but Physical Damage is not Selected. Please select Physical Damage.");
                    }

                    #endregion


                    if (car.HasPhyDam)
                    {
                        if (car.CompDed != car.CollDed)
                        {
                            yield return new ValidationResult($"Comp and Coll deductibles must match for Car {GetCarErrorMsgData(car)}");
                            yield return new ValidationResult("Must match Coll Deductible.", new[] { "CompDed" });
                            yield return new ValidationResult("Must match Comp Deductible.", new[] { "CollDed" });
                        }
                    }

                    #endregion
                    //-------------------------------------------------------
                } //END Loop through Cars Data

                // if (getCars.Count == 1 && getCars.First().VehYear <= 2006 && quoteDb != null && quoteDb.QuotedDate >= new DateTime(2020, 8, 25))
                // {
                //     var phoneNumber = DbHelperUtils.getScalarValue("SystemParameters", "CompanyPhoneTollFree ", "CompanyID", "PALMINSURE");
                //
                //     yield return new ValidationResult($"For single car quotes with a 2006 or older vehicle, call for a rate. Please contact UW at {phoneNumber} ext. 2.");
                // }


                //--------------------------------------------------------------------------------------------

                #region Check Direct Repair/Phy Dam

                if (!hasPhyDam && DirectRepairDiscount)
                {
                    yield return new ValidationResult(
                        "In order to select Direct Repair Discount a vehicle on this policy must have Physical Damage.");
                    yield return new ValidationResult(
                        "In order to select Direct Repair Discount a vehicle on this policy must have Physical Damage.",
                        new[] { "CollDed" });
                }

                #endregion

                #region Check For Drivers > 10 Points Violations && Check For DUI Violations && Check for complete driver info. && Check Married Drivers

                bool thasSr22 = false;
                bool hasMaxViolations = false;
                bool isDuiViolation = false;
                int countMarriedDrivers = 0;
                List<string> listMaxViolErrMsg = new List<string>();
                List<string> listNaughtyDrivers = new List<string>();
                List<PolicyQuoteDrivers> listDrivers = new List<PolicyQuoteDrivers>();

                var getDrivers = new XPCollection<PolicyQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                if (getDrivers.Any() && getDrivers.All(d => d.Exclude))
                {
                    yield return new ValidationResult("All drivers cannot be excluded");
                }

                foreach (var drivers in getDrivers)
                {

                    if (string.IsNullOrEmpty(drivers.Gender))
                    {
                        yield return new ValidationResult(
                            string.Format("Driver {0}: Gender is required.", drivers.DriverIndex));
                    }

                    if (drivers.DOB.Equals(DateTime.MinValue) || drivers.DOB.Equals(DateTime.MaxValue))
                    {
                        yield return new ValidationResult(
                            string.Format("Driver {0}: A valid Date of Birth is required.", drivers.DriverIndex));
                    }

                    if (drivers.SR22)
                    {
                        thasSr22 = true;
                    }

                    if (drivers.DriverIndex > 2 && drivers.RelationToInsured == "INSURED")
                    {
                        yield return new ValidationResult(string.Format(
                            "Driver {0} - {1} - has been listed as a driver with Relationship type as Insured, please select correct relationship type.",
                            drivers.DriverIndex, drivers.FirstName + " " + drivers.LastName));
                    }

                    if (string.IsNullOrEmpty(drivers.FirstName) || string.IsNullOrEmpty(drivers.LastName))
                    {
                        yield return new ValidationResult(string.Format(
                            "Driver {0} has incomplete information, please fill required information",
                            drivers.DriverIndex));
                    }

                    if (drivers.DriverIndex == 1 && drivers.Exclude)
                    {
                        yield return new ValidationResult(
                            string.Format("Drivers: The named insured cannot be excluded", drivers.DriverIndex));
                    }

                    if (!drivers.Exclude && string.IsNullOrEmpty(drivers.LicenseNo))
                    {
                        yield return new ValidationResult(
                            string.Format("Driver {0}: The license number is required.", drivers.DriverIndex));
                    }

                    if (drivers.DOB > DateTime.Now)
                    {
                        yield return new ValidationResult("Date Of Birth cannot be in the future.", new[] { "DateOfBirth" });
                    }

                    if (!drivers.Exclude && !string.IsNullOrEmpty(drivers.LicenseSt) && drivers.LicenseSt.Equals("FL") && !string.IsNullOrEmpty(drivers.LicenseNo))
                    {
                        string licenseNo = drivers.LicenseNo.Replace("-", string.Empty);
                        if (licenseNo.Length != 13)
                        {
                            yield return new ValidationResult(string.Format(
                                "Driver {0} has an invalid FL driver's license number. Please ensure it is 13 digits long.", drivers.DriverIndex));
                        }
                        else
                        {
                            //validate birth year
                            string twoDigitBirthYear = drivers.DOB.ToShortDateString()
                                .Substring(drivers.DOB.ToShortDateString().Length - 2, 2);
                            string licenseBirthDateDigits = licenseNo?.Substring(7, 2);
                            if (twoDigitBirthYear != licenseBirthDateDigits)
                            {
                                yield return new ValidationResult(
                                    $"Driver {drivers.DriverIndex} has an invalid license. A valid FL license should contain the drivers two digit birth year.");
                            }
                        }
                    }

                    if (!drivers.Exclude && string.IsNullOrEmpty(drivers.LicenseSt))
                    {
                        yield return new ValidationResult("License state is required.", new[] { "LicenseState" });

                    }

                    if (!String.IsNullOrEmpty(drivers.MI) && !Char.IsLetter(drivers.MI[0]))
                    {
                        yield return new ValidationResult("Middle initial must be a alphabetic", new[] { "MiddleInitial" });
                    }

                    if (!drivers.Exclude && drivers.DOB > DateTime.Now.Date.AddYears(-16))
                    {
                        yield return new ValidationResult("We only accept drivers over 16 years of age", new[] { "DateOfBirth" });
                    }

                    if (!drivers.Exclude && (string.IsNullOrEmpty(drivers.LicenseSt) || (!drivers.InternationalLic && !StaticData.USAStates.Keys.Contains(drivers.LicenseSt))))
                    {
                        yield return new ValidationResult("License state must be one of the 50 states", new[] { "LicenseState" });
                    }

                    listDrivers.Add(drivers);
                }

                //-----------------------------------
                // Check Married Drivers -> If not even then require another driver
                int modMarried = getDrivers.Count(m => m.Married) % 2;
                if (modMarried != 0)
                {
                    yield return new ValidationResult(string.Format(
                        "There are an uneven amount of married drivers on the policy, please add an additional married driver"));
                }

                //-----------------------------------
                for (int i = 0; i < listDrivers.Count; i++)
                {
                    var getViolations = new XPCollection<PolicyQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?",
                            Sessions.Instance.PolicyNo, listDrivers[i].DriverIndex));
                    int totalPoints = 0;
                    int afAccs = 0; //LF 12/16/18 added
                    int nfAccs = 0;
                    bool inexperienced =
                        DateTime.Now < listDrivers[i].DateLicensed.AddYears(3); //LF 2/19/19 added for AA


                    foreach (PolicyQuoteDriversViolations violations in getViolations)
                    {
                        totalPoints += violations.ViolationPoints;
                        if (violations.ViolationCode == "1" || violations.ViolationCode == "ACC001")
                        {
                            if (violations.ViolationDate >= DateTime.Now.AddMonths(-35))
                                afAccs += 1;
                        }

                        if (violations.ViolationCode == "2" || violations.ViolationCode == "NAF008")
                        {
                            if (violations.ViolationDate >= DateTime.Now.AddMonths(-35))
                                nfAccs += 1;
                        }
                    }

                    if (totalPoints >= 8)
                    {
                        hasMaxViolations = true;
                        listMaxViolErrMsg.Add(string.Format(
                            "Driver {2} - {0} {1} - has 8 or more points in violations.", listDrivers[i].FirstName,
                            listDrivers[i].LastName, listDrivers[i].DriverIndex));
                    }

                    //LF 2/19/19 added, AA
                    if (inexperienced && totalPoints >= 5)
                    {
                        hasMaxViolations = true;
                        listMaxViolErrMsg.Add(string.Format(
                            "Driver {2} - {0} {1} - Inexperienced operator and has 5 or more points in violations.",
                            listDrivers[i].FirstName, listDrivers[i].LastName, listDrivers[i].DriverIndex));
                    }

                    if (afAccs >= 2)
                    {
                        listNaughtyDrivers.Add(string.Format(
                            "Driver {0} - {1} {2} - has 2 or more At Fault Accident violations.",
                            listDrivers[i].DriverIndex, listDrivers[i].FirstName, listDrivers[i].LastName));
                    }
                    else if (nfAccs + afAccs >= 3)
                    {
                        listNaughtyDrivers.Add(string.Format(
                            "Driver {0} - {1} {2} - has 3 or more Accident violations.", listDrivers[i].DriverIndex,
                            listDrivers[i].FirstName, listDrivers[i].LastName));
                    }


                    if (listDrivers[i].Gender == "SELECT")
                    {
                        listNaughtyDrivers.Add(string.Format(
                            "Driver {0} - {1} {2} - needs a correct gender selected", listDrivers[i].DriverIndex,
                            listDrivers[i].FirstName, listDrivers[i].LastName));
                    }
                }

                HasSr22 = thasSr22;
                if (hasMaxViolations)
                {
                    for (int i = 0; i < listMaxViolErrMsg.Count; i++)
                    {
                        yield return new ValidationResult(listMaxViolErrMsg[i]);
                    }
                }

                if (listNaughtyDrivers.Count > 0)
                {
                    for (int i = 0; i < listNaughtyDrivers.Count; i++)
                    {
                        yield return new ValidationResult(listNaughtyDrivers[i]);
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check for > 2 Excluded Drivers

                if (excludedDriverCount > 4)
                {
                    yield return new ValidationResult("This quote has more than 4 excluded drivers");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check For SR22

                if (thasSr22)
                {
                    if (BiLimit.Upper() == "NONE")
                    {
                        yield return new ValidationResult("A Driver has SR22, BI limit must be selected.");
                        yield return new ValidationResult("A Driver has SR22, BI limit must be selected.",
                            new[] { "BiLimit" });
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Number Of Drivers

                if (Drivers == null || Drivers.Count == 0)
                {
                    yield return new ValidationResult("A quote must have at least one driver");
                }
                else
                {
                    var primaryDriver = uow.FindObject<PolicyQuoteDrivers>(CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex=1", Sessions.Instance.PolicyNo));
                    var secondaryDriver = uow.FindObject<PolicyQuoteDrivers>(CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex=2", Sessions.Instance.PolicyNo));
                    var primaryInsuredFullName = $"{FirstName1} {MiddleInitial1} {LastName1}".Trim();
                    var secondaryInsuredFullName = $"{FirstName2} {MiddleInitial2} {LastName2}".Trim();
                    if (primaryDriver != null && (string.Compare(primaryDriver.FirstName.SafeTrim(), this.FirstName1.SafeTrim(), true) != 0 || string.Compare(primaryDriver.LastName.SafeTrim(), this.LastName1.SafeTrim(), true) != 0 || string.Compare(primaryDriver.MI.SafeTrim(), this.MiddleInitial1.SafeTrim(), true) != 0))
                    {
                        yield return
                                 new ValidationResult(
                                     $"Primary Insured name '{primaryInsuredFullName}' does not match with Driver 1 name '{primaryDriver.FirstName} {primaryDriver.MI} {primaryDriver.LastName}'.");
                    }
                    //Removed as per PBI 2582
                    //if (primaryDriver != null && DateTime.Now.AddYears(-19) < primaryDriver.DOB)
                    //{
                    //    yield return
                    //          new ValidationResult($"Primary insured '{primaryInsuredFullName}' is less than 19 years old");
                    //}
                    if (!string.IsNullOrEmpty(this.FirstName2.SafeTrim()) || !string.IsNullOrEmpty(this.LastName2.SafeTrim()) || !string.IsNullOrEmpty(this.MiddleInitial2.SafeTrim()))
                    {
                        if (secondaryDriver == null)
                        {
                            yield return new ValidationResult($"Second driver must be added with the name of secondary named insured({secondaryInsuredFullName}).");
                        }
                        else if (string.Compare(secondaryDriver.FirstName.SafeTrim(), this.FirstName2.SafeTrim(), true) != 0 || string.Compare(secondaryDriver.LastName.SafeTrim(), this.LastName2.SafeTrim(), true) != 0 || string.Compare(secondaryDriver.MI.SafeTrim(), this.MiddleInitial2.SafeTrim(), true) != 0)
                        {
                            yield return
                                  new ValidationResult(
                                      $"Secondary Insured name '{secondaryInsuredFullName}' does not match with Driver 2 name '{secondaryDriver.FirstName} {secondaryDriver.MI} {secondaryDriver.LastName}' ");
                        }
                        //Removed as per PBI 2582
                        //if (secondaryDriver != null && DateTime.Now.AddYears(-19) <secondaryDriver.DOB)
                        //{
                        //    yield return
                        //          new ValidationResult($"Secondary insured '{secondaryInsuredFullName}' is less than 19 years old");
                        //}
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check Number Of Vehicles

                if (Vehicles == null || Vehicles.Count == 0)
                {
                    yield return new ValidationResult("A quote must have at least one vehicle");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check if BI is selected if UM type is not null

                if (UmLimit.Upper() != "NONE")
                {
                    if (BiLimit.Upper() == "NONE")
                    {
                        yield return new ValidationResult("BI Coverage must be selected.");
                        yield return new ValidationResult("BI Coverage must be selected.", new[] { "BiLimit" });
                    }
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                if (string.IsNullOrEmpty(GarageCity) || GarageCity.ToUpper().Equals("FIND BY ZIP"))
                {
                    yield return new ValidationResult("Please Select a Valid City.", new[] { "GarageCity" });
                }

                if (string.IsNullOrEmpty(GarageZIPCode))
                {
                    yield return new ValidationResult("Please Enter a Valid ZIP Code.", new[] { "GarageZIPCode" });
                }

                XpoAARateTerritory rateTerritory = new XPCollection<XpoAARateTerritory>(uow, CriteriaOperator.Parse("ZIP = ?", GarageZIPCode)).FirstOrDefault();
                if (rateTerritory == null)
                {
                    yield return new ValidationResult("Invalid Zip code. Contact UW for assistance.",
                        new[] { "GarageZIPCode" });
                }

                //Check if zip code in at risk territory and vlaidate pip claims
                bool isAtRiskTerritory = false;
                string currentRateCycle = DbHelperUtils.getMaxRecID("AARatePayPlans", "RateCycle");
                if (currentRateCycle == ConfigSettings.ReadSetting("CurrentRateCycle"))
                {
                    var currentTerritory =
                        uow.FindObject<XpoAARateTerritory>(CriteriaOperator.Parse("ZIP = ? AND RateCycle = ?", GarageZIPCode, currentRateCycle));
                    if (currentTerritory != null)
                    {
                        string supportAreaCode = currentTerritory.SupportArea;
                        string uwAreaCode = currentTerritory.UWArea;
                        XpoAARateSupUW uwAreaInfo = uow.FindObject<XpoAARateSupUW>(
                            CriteriaOperator.Parse("RateCycle = ? AND SupportArea = ? AND UWArea = ?",
                                currentRateCycle, supportAreaCode, uwAreaCode));
                        if (uwAreaInfo != null)
                        {
                            if (uwAreaInfo.DownPay.Equals(1) && uwAreaInfo.PIPCount.Equals(0))
                            {
                                isAtRiskTerritory = true;
                            }
                        }
                    }
                }
                if (isAtRiskTerritory)
                {
                    var violationsForQuote = new XPCollection<PolicyQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                    if (violationsForQuote.Any(v => v.ViolationDesc.ToUpper().Contains("PIP")))
                    {
                        yield return new ValidationResult(
                            $"No prior PIP claims are allowed in this zip code.");
                    }
                }


                #region Bad County Check


    //              if (PortalUtils.IsAgentRestrictedForZipAndCoverage(quoteDb.AgentCode, GarageZIPCode))
    //             {
    //                yield return new ValidationResult($"Due to your underwriting results, your agency is currently suspended from writing new business in this area{GarageZIPCode}.Please contact your Marketing Representative for additional information.",
    //                     new[] { "GarageZIPCode" });
    //
    //                yield return new ValidationResult($"Due to your underwriting results, your agency is currently suspended from writing new business in this area {GarageZIPCode}.Please contact your Marketing Representative for additional information.");
    //             }
				// else if ((hasPhyDam && PortalUtils.IsAgentRestrictedForZipAndCoverage(quoteDb.AgentCode, GarageZIPCode, "PhyDam")))
    //             {
    //                yield return new ValidationResult($"Due to your underwriting results, your agency is currently suspended from writing new business with physical damage in this area {GarageZIPCode} .Please contact your Marketing Representative for additional information.",new[] { "GarageZIPCode" });
    //
    //                yield return new ValidationResult($"Due to your underwriting results, your agency is currently suspended from writing new business with physical damage in this area {GarageZIPCode} .Please contact your Marketing Representative for additional information.");
    //             }

                //Disable physical damage coverage temporarily for new business.

                //if (getCars.Any(car => car.HasPhyDam || (car.CompDed != null && car.CompDed != "NONE")))
                //{
                //    yield return new ValidationResult(
                //        "Due to Hurricane Elsa, we are disabling the binding of Physical Damage coverage. Please check back soon.");
                //}


                bool isZipCodeAllowedForNewBusiness = IsBusinessAllowedInZipCode(GarageZIPCode).GetAwaiter().GetResult();
                if (!isZipCodeAllowedForNewBusiness)
                {
                    yield return new ValidationResult($"Zipcode {GarageZIPCode} is unacceptable for new business",
                        new[] { "GarageZIPCode" });
                    yield return new ValidationResult($"Zipcode {GarageZIPCode} is unacceptable for new business");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Check For Naughty Garage Address

                if (GarageStreetAddress.Upper().Contains("PO BOX"))
                {
                    yield return new ValidationResult("The address given cannot be a PO Box.",
                        new[] { "GarageStreetAddress" });
                    yield return new ValidationResult("The address given cannot be a PO Box.");
                }

                #endregion

                //--------------------------------------------------------------------------------------------

                #region Don't Re-write NonRenewed/Accident Insured

                if (PortalUtils.GetPortalOption("BLOCKNONRENEWEDINSURED"))
                {
                    var isBadInsured = false;
                    var listRealNaughtyDrivers = new List<string>();
                    var listNaughtyCars = new List<string>();

                    var getDrivers2 =
                        new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", QuoteId));
                    var getCars2 =
                        new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", QuoteId));

                    foreach (var getPolDrivers in getDrivers2.Select(driver =>
                        new XPCollection<PolicyDrivers>(uow,
                            CriteriaOperator.Parse("LastName = ? AND (LicenseNo = ? OR LicenseNo = ?)",
                                driver.LastName, driver.LicenseNo, driver.LicenseNo.Replace("-", "")))))
                    {
                        listRealNaughtyDrivers.AddRange(getPolDrivers.Select(driver => driver.PolicyNo));
                    }

                    foreach (var getPolCars in getCars2.Select(car =>
                        new XPCollection<PolicyCars>(uow, CriteriaOperator.Parse("VIN = ?", car.VIN))))
                    {
                        listNaughtyCars.AddRange(getPolCars.Select(car => car.PolicyNo));
                    }

                    IEnumerable<string> badPolicies = listRealNaughtyDrivers.Intersect(listNaughtyCars);

                    foreach (var policy in badPolicies)
                    {
                        var getpolicy =
                            new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policy));

                        foreach (var badPolicy in getpolicy.Where(badPolicy =>
                            badPolicy.PolicyStatus == "NONRENEW" || badPolicy.RenewalStatus == "NON-RENEW"))
                        {
                            isBadInsured = true;
                            string msg =
                                string.Format(
                                    "{0} - Quote cannot be allowed to bind. Previous Policy {1} was non-renewed",
                                    QuoteId, badPolicy.PolicyNo);

                            PortalUtils.PostUndAlert(msg);
                        }
                    }

                    if (isBadInsured)
                    {
                        yield return new ValidationResult(
                            "This policy is ineligible to be bound due to a drivers previous history");
                    }

                    //END Using
                } //END Don't re-write an accident/non-renewed insured

                #endregion


                #region EmailCheckForPaperLess

                if (quoteDb.Paperless)
                {
                    if (!AgentUtils.ValidateEmailFormat(quoteDb.MainEmail))
                    {
                        yield return new ValidationResult(
                            $"Insured Email address {quoteDb.MainEmail} is invalid. A valid email is required for Paperless discount.");
                    }
                    else if(AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode)?.Trim()?.ToLower()==quoteDb.MainEmail.Trim().ToLower())
                    {
                        yield return new ValidationResult(
                            $"Insured Email address {quoteDb.MainEmail} cannot be same as Agent's email address for Paperless discount.");
                    }
                }

                #endregion
            }
        }
       
        private async Task<bool> IsBusinessAllowedInZipCode(string zip)
        {
            bool businessIsAllowed = false;
            if (string.IsNullOrEmpty(zip))
            {
                return businessIsAllowed;
            }

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string mostRecentRateCycle = DbHelperUtils.getMaxRecID("AARatePayPlans", "RateCycle");
                if (!string.IsNullOrEmpty(mostRecentRateCycle))
                {
                    XpoAARateTerritory territoryInfo = new XPCollection<XpoAARateTerritory>(uow,
                        CriteriaOperator.Parse("ZIP = ? AND RateCycle = ?", zip, mostRecentRateCycle)).FirstOrDefault();
                    if (territoryInfo != null)
                    {
                        string supportAreaCode = territoryInfo.SupportArea;
                        string uwAreaCode = territoryInfo.UWArea;
                        if (!string.IsNullOrWhiteSpace(supportAreaCode) && !string.IsNullOrWhiteSpace(uwAreaCode))
                        {
                            XpoAARateSupUW uwAreaInfo = new XPCollection<XpoAARateSupUW>(uow,
                                CriteriaOperator.Parse("RateCycle = ? AND SupportArea = ? AND UWArea = ?",
                                    mostRecentRateCycle, supportAreaCode, uwAreaCode)).FirstOrDefault();
                            if (uwAreaInfo != null)
                            {
                                businessIsAllowed = uwAreaInfo.AllowNewBusiness;
                            }
                        }
                    }
                }

                return businessIsAllowed;
            }
        }

        private string FormatPhoneNumber(string phoneNumber)
        {
            string phoneFormat = "###-###-####";

            // First, remove everything except of numbers
            Regex regexObj = new Regex(@"[^\d]");
            phoneNumber = regexObj.Replace(phoneNumber, "");

            // Second, format numbers to phone string 
            if (phoneNumber.Length > 0)
            {
                phoneNumber = Convert.ToInt64(phoneNumber).ToString(phoneFormat);
            }

            return phoneNumber;
        }
    }//END QuoteModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class InsuredModel
    {
        public string FirstName { get; set; }

        [StringLength(1, ErrorMessage = "This initial should be only one character")]
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
    }//END InsuredModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class AddressModel
    {
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        public string ZIPCode { get; set; }
    }//END AddressModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class DriverModel : IValidatableObject
    {
        public string QuoteId { get; set; }
        public string DriverId { get; set; }
        public bool HasMvr { get; set; }
        public DateTime MvrDate { get; set; }
        public string LicenseStatus { get; set; }
        public DateTime LicenseExpiration { get; set; }
        public string VeriskMvrOrderNumber { get; set; }
        public bool IsEdit { get; set; }
        public int DriverIndex { get; set; }
        //---------------------------------------------------------
        [Display(Name = "Unverifiable/No Hit")]
        public bool Unverifiable { get; set; }
        //---------------------------------------------------------
        private string date = DateTime.Now.AddYears(-1).ToString("yyyy/MM/dd");
        #region Personal Information


        [Display(Name = "Temp First Name")]

        public string TempFirstName { get; set; } // used to store main insured fist name when first creating a quote it is passed to addDriver
        [Display(Name = "Temp First Name")]

        public string TempLastName { get; set; } // used to store main insured fist name when first creating a quote it is passed to addDriver
        public string TempMI { get; set; }  //LF added 11/18/18

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Initial")]
        [StringLength(1, MinimumLength = 1, ErrorMessage = "Your middle initial should be 1 character long")]
        public string MiddleInitial { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Suffix")]
        [StringLength(5)]
        public string Suffix { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Date Of Birth must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please Select A Gender")]
        public string Gender { get; set; }

        [Display(Name = "Married")]
        public bool IsMarried { get; set; }

        [Required]
        [Display(Name = "Relation To Insured")]
        public string Relation { get; set; }

        [Display(Name = "Is Excluded")]
        public bool IsExcluded { get; set; }

        public string DriverMsg { get; set; }

        #endregion

        //---------------------------------------------------------
        #region License Information

        [Display(Name = "International License")]
        public bool IsInternationalLicense { get; set; }

        [Display(Name = "Senior Driving Course?")]
        public bool HasSeniorDefDrvDisc { get; set; }

        [Display(Name = "SR22?")]
        public bool HasSR22 { get; set; }

        [Display(Name = "License Number")]
        //[RegularExpression(Constants.FlDlRegex, ErrorMessage = "The drivers license number has been generated for you. Please add the  last dash and digit to the drivers license number.")]
        public string DriversLicense { get; set; }
        public string OldDlNum { get; set; }

        [Display(Name = "Date of course")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Date Of Course must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime? SeniorDefDrvDate { get; set; }

        [Display(Name = "SR22 Case No")]
        public string SR22CaseNo { get; set; }

        [Display(Name = "License State")]
        public string LicenseState { get; set; }

        [Display(Name = "Date Licensed")]
        [RegularExpression(Constants.DateTimeRegex, ErrorMessage = "Date Licensed must be in the format of mm/dd/yyyy")]
        [DateRange("1753/01/01", "9999/12/31")]
        public string DateLicensed { get; set; }

        #endregion
        //---------------------------------------------------------

        #region Employer Info

        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Employer Name")]
        public string EmployerName { get; set; }

        [Display(Name = "Occupation")]
        public string Occupation { get; set; }

        [Display(Name = "Employer Address")]
        public AddressModel EmployerAddress { get; set; }

        #endregion
        //---------------------------------------------------------

        #region Not Sure If Still Need

        [Display(Name = "Class")]
        public string Class { get; set; }

        #endregion
        //---------------------------------------------------------

        [UIHint("Violations")]
        public List<ViolationModel> Violations { get; set; }

        public string NewDriverViolationsJson { get; set; }
        public List<ViolationAddModel> NewDriverViolations
        {
            get
            {
                if (string.IsNullOrEmpty(NewDriverViolationsJson))
                    return new List<ViolationAddModel>();
                return JsonConvert.DeserializeObject<List<ViolationAddModel>>(NewDriverViolationsJson);
            }
        }



        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            if (DateOfBirth.Parse<DateTime>() > DateTime.Now)
            {
                yield return new ValidationResult("Date Of Birth cannot be in the future.", new[] { "DateOfBirth" });
            }

            //            if (!IsExcluded && string.IsNullOrEmpty(DriversLicense))
            //            {
            //                yield return new ValidationResult("Drivers license number is required.", new[] { "DriversLicenseNumber" });
            //            }

            if (!IsExcluded && !string.IsNullOrEmpty(LicenseState) && LicenseState.Equals("FL") && DriversLicense != null)
            {
                string licenseNo = DriversLicense.Replace("-", string.Empty);
                bool validFlDL = true;
                if (licenseNo.Length != 13)
                {
                    validFlDL = false;
                    yield return new ValidationResult("A valid FL Drivers license number must be 13 characters.",
                        new[] { "DriversLicenseNumber" });
                }

                //validate birth year
                string twoDigitBirthYear = DateOfBirth.Substring(DateOfBirth.Length - 2, 2);
                if (validFlDL)
                {
                    string licenseBirthDateDigits = licenseNo?.Substring(7, 2);
                    if (twoDigitBirthYear != licenseBirthDateDigits)
                    {
                        yield return new ValidationResult(
                            "A valid FL Drivers should contain the drivers two digit birth year.",
                            new[] { "DriversLicenseNumber" });
                    }
                }
            }

            if (!IsExcluded && string.IsNullOrEmpty(LicenseState))
            {
                yield return new ValidationResult("License state is required.", new[] { "LicenseState" });

            }

            if (!String.IsNullOrEmpty(MiddleInitial) && !Char.IsLetter(MiddleInitial[0]))
            {
                yield return new ValidationResult("Middle initial must be a alphabetic", new[] { "MiddleInitial" });
            }

            if (!IsExcluded && DateOfBirth.Parse<DateTime>() > DateTime.Now.Date.AddYears(-16))
            {
                yield return new ValidationResult("We only accept drivers over 16 years of age", new[] { "DateOfBirth" });
            }

            if (string.IsNullOrEmpty(LicenseState) || (!IsInternationalLicense && !StaticData.USAStates.Keys.Contains(LicenseState)))
            {
                yield return new ValidationResult("License state must be one of the 50 states", new[] { "LicenseState" });
            }

            if (UtilitiesDrivers.IsDriverUnacceptableRisk(FirstName, LastName, DateOfBirth, DriversLicense, LicenseState))
            {
                yield return new ValidationResult(String.Format("The driver ({0} {1}) marked in system with Unacceptable Risk.",
                                  FirstName, LastName), new[] { "UnacceptableRisk" });

            }
        }
    }//END DriverModel
     ////////////////////////////////////////////////////////////////////////////////////////////////

    public enum GenderOption
    {
        SELECT,
        MALE,
        FEMALE
    }//END Enum GenderOption
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public enum WorkLossExclusionType
    {
        NONE,
        NIO,
        NIRR
    }//END WorkLossExclusionType
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class VehicleModel
    {
        public string QuoteId { get; set; }
        public string VehicleId { get; set; }
        public int CarIndex { get; set; }
        public bool IsEdit { get; set; }

        [Display(Name = "Comp Ded")]
        public string ComprehensiveDeductible { get; set; }
        [Display(Name = "Coll Ded")]
        public string CollisionDeductible { get; set; }

        public string CostNew { get; set; }

        public string EngineType { get; set; }

        public string GrossVehicleWeight { get; set; }

        public string ClassCode { get; set; }

        public int Year { get; set; }
        public string Make { get; set; }
        [Display(Name = "Model")]
        public string VehModel { get; set; }
        [Display(Name = "Body Style")]
        public string Style { get; set; }

        [MinLength(17)]
        [Required]
        public string VIN { get; set; }

        [Display(Name = "Miles to work")]
        public int? MilesToWork { get; set; }

        [Display(Name = "Airbags")]
        public bool HasAirbag { get; set; }

        [Display(Name = "Antilock Brakes")]
        public bool HasAntiLockBrakes { get; set; }

        [Display(Name = "Anti-theft Device")]
        public bool HasAntiTheftDevice { get; set; }

        public bool Artisan { get; set; }

        [Display(Name = "Physical Damage coverage")]
        [Required]
        public bool HasPhyDam { get; set; }

        [Display(Name = "Convertible / TTops")]
        public bool HasConvOrTT { get; set; }

        [Display(Name = "> 10 Miles to Wk?")]
        public bool MoreThan10MilesToWk { get; set; }

        [Display(Name = "Amt Custom")]
        public double? AmountCustom { get; set; }

        [Display(Name = "Purchased New")]
        public bool IsPurchasedNew { get; set; }

        [Display(Name = "Date Purchased")]
        public string DatePurchased { get; set; }

        [Display(Name = "Customized")]
        public bool IsCustomized { get; set; }

        [Display(Name = "Business Use")]
        public bool IsBusinessUse { get; set; }

        public string AntiTheftType { get; set; }

        public int VinIndexNo { get; set; }

        public string CompSymbol { get; set; }

        public string CollSymbol { get; set; }

        public string BISymbol { get; set; }

        public string PDSymbol { get; set; }

        public string MPSymbol { get; set; }

        public string PIPSymbol { get; set; }

        //AirBag info
        public string ARB { get; set; }

        /// <summary>
        /// AntiTeheft info
        /// </summary>
        public string ATD { get; set; }


        public bool HasAllSymbols()
        {
            return !string.IsNullOrEmpty(PDSymbol) && !string.IsNullOrEmpty(PIPSymbol) &&
                   !string.IsNullOrEmpty(BISymbol) && !string.IsNullOrEmpty(MPSymbol) &&
                   !string.IsNullOrEmpty(CollSymbol) && !string.IsNullOrEmpty(CompSymbol) &&
                   !string.IsNullOrEmpty(MPSymbol) && !string.IsNullOrEmpty(ATD) &&
                   !string.IsNullOrEmpty(ARB);
        }

        [UIHint("LienHolders")] public List<LienHolderModel> LienHolders { get; set; } = new List<LienHolderModel>();

        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            if (HasPhyDam && (ComprehensiveDeductible == "0" || ComprehensiveDeductible == "NONE"))
            {
                yield return
                    new ValidationResult(
                        "This vehicle has Physical Damage, must select Comp/Coll deductibles.");
                yield return new ValidationResult("Must select Comp/Coll deductibles.", new[] { "ComprehensiveDeductible" });
                yield return new ValidationResult("Must select Comp/Coll deductibles.", new[] { "CollisionDeductible" });
            }
        }

    }//END VehicleModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class LienHolderAddModel
    {
        public int ID { get; set; }
        public int VehicleId { get; set; }

        [Display(Name = "Policy No")]
        public string PolicyNo { get; set; }

        [Display(Name = "Vehicle Index")]
        public int CarIndex { get; set; }

        //[Display(Name = "Company Name")]
        public string Name { get; set; }

        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [RegularExpression(@"^\d{5}?$", ErrorMessage = "Please enter a valid ZIP")]
        public string ZIP { get; set; }

        [RegularExpression(Constants.PhoneRegex, ErrorMessage = "Phone number must be in XXX-XXX-XXXX format")]
        public string Phone { get; set; }

        [Display(Name = "Additional Interest?")]
        public bool isAddInterest { get; set; }


        [Display(Name = "Non Institutional?")]
        public bool IsNonInstitutional { get; set; }

        public string VIN { get; set; }
    }//END LienHolderModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class LienHolderModel : LienHolderAddModel
    {
        [Display(Name = "Lienholder Index")]
        public int LienHolderIndex { get; set; }

        new public string VIN { get; set; }

    }//END LienHolderModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class ViolationAddModel
    {
        // TODO: WHAT AM I?
        public string DriverId { get; set; }

        public int DriverIndex { get; set; }

        /// <summary>
        /// Database column LongDesc
        /// </summary>
        public string Description { get; set; }

        [Required]
        [DateRange("1753/01/01", "9999/12/31")]
        public DateTime Date { get; set; }

        public bool FromMvr { get; set; }
    }//END ViolationAddModel
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public class ViolationModel : ViolationAddModel
    {
        /// <summary>
        /// Id generate when added to a driver.
        /// </summary>
        public int ViolationId { get; set; }

        /// <summary>
        /// ID in the AARateViolations table.
        /// </summary>
        public int IndexID { get; set; }

        /// <summary>
        /// Database column PointsFirst
        /// </summary>
        public int Points { get; set; }

        public string PolicyNo { get; set; }

        new public bool FromMvr { get; set; }

        new public bool FromCV { get; set; }


        new public bool FromAPlus { get; set; }


    }
    public class QuestionnaireNewModel : IValidatableObject
    {
        [Required]
        public string QuoteId { get; set; }

        [Display(Name = "1. Have you listed all persons age 14 or older, residing with the applicant(s) whether or not they drive/operate the listed vehicle(s), including students living away from home, persons in the Armed Services, and any dependents of the applicant or applicant’s spouse between the ages of 14 and 24 who do not reside with applicant(s)?")]
        public bool? hasDependent { get; set; }

        [Display(Name = "2. Is your principal residence outside the state of Florida for two (2) or more months each year or do any of the regular drivers/operators of the listed vehicle(s) work outside of the State of Florida?")]
        public bool? hasFloridaResident { get; set; }

        [Display(Name = "3. Do any drivers have a handicap or physical disability that substantially impairs the applicant(s) /driver(s) driving ability, which is NOT corrected by mechanical assistance?")]
        public bool? hasDrivingImpairments { get; set; }

        [Display(Name = "4.  Have you failed to list any other vehicles in the household? Is the owner of any vehicles listed on the application not listed as a driver?")]
        public bool? hasVehicleOwned { get; set; }


        [Display(Name = "REMARKS: (explain any 'YES' answers to the above questions)")]
        public string hasVehicleOwnedRemarkOne { get; set; }

        [Display(Name = "5. Are any vehicles listed on this application co-owned by a non-resident person or by a resident of the insured's household?")]
        public bool? hasCoownerResident { get; set; }

        [Display(Name = "REMARKS: (explain any 'YES' answers to the above questions)")]
        public string hasCoownerResidentRemarkOne { get; set; }


        [Display(Name = "6. Are any of the vehicles listed on the application not garaged at the garage location shown under 'General Application Information' on page 1 of the application?")]
        public bool? hasVehicleNotGaraged { get; set; }

        [Display(Name = "7. Does any vehicle have customized Equipment, including but not limited to sound equipment, body effects, etc or do any of the vehicles on this application have any existing damage?")]
        public bool? hasExistingDamage { get; set; }

        [Display(Name = "REMARKS: (explain any 'YES' answers to the above questions)")]
        public string hasExistingDamageRemarkOne { get; set; }


        [Display(Name = "8. Is any listed vehicle a “Gray Market”, (i.e. not manufactured for original sale in the U.S.)?")]
        public bool? hasGrayMarketVehicle { get; set; }

        [Display(Name = "9. Has the driver made a claim for Personal Injury Protection benefits in the past 3 years, had auto insurance cancelled, been refused insurance or renewal, or been refused for one of the following reasons: Material Misrepresentation, Claims History, or Multiple PIP claims (more than one.)")]
        public bool? hasAutoInsuranceCancelledorPIPClaim { get; set; }

        [Display(Name = "10. Has any operator been convicted or forfeited bail in relation to an automobile in the past 3 years, had any lawsuit in relation to an automobile in the past 3 years, or had any loan defaults in the past 3 years?")]
        public bool? hasCriminalActivity { get; set; }

        [Display(Name = "REMARKS: (explain any 'YES' answers to the above questions)")]
        public string hasCriminalActivityRemarkOne { get; set; }

        [Display(Name = "11. I have disclosed on this application that none of my vehicles are used for hire (taxi, limo, ridesharing, etc.), commercial or business purposes, delivery (pizza, newspaper, etc.), or used in the course or scope of your employment excluding to / from work.")]
        public bool? hasVehicleForBusinessPurpose { get; set; }

        [Display(Name = "12. I agree to notify the Company prior to any future business or commercial use and I understand the Company does not cover losses if my vehicle(s) is being used for any business or commercial purposes and these purposes are not disclosed prior to the loss.")]
        public bool? hasReportedBusinessUse { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext context)
        {
            #region Force All Questions to have answers

            if (hasDependent == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasDependent" });
            }

            if (hasFloridaResident == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasFloridaResident" });
            }

            if (hasDrivingImpairments == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasDrivingImpairments" });
            }

            if (hasVehicleOwned == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasVehicleOwned" });
            }


            if (hasCoownerResident == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasCoownerResident" });
            }

            if (hasVehicleNotGaraged == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasVehicleNotGaraged" });
            }

            if (hasExistingDamage == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasExistingDamage" });
            }

            if (hasGrayMarketVehicle == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasGrayMarketVehicle" });
            }

            if (hasAutoInsuranceCancelledorPIPClaim == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasAutoInsuranceCancelledorPIPClaim" });
            }

            if (hasCriminalActivity == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasCriminalActivity" });
            }

            if (hasVehicleForBusinessPurpose == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasVehicleForBusinessPurpose" });

            }

            if (hasReportedBusinessUse == null)
            {
                yield return new ValidationResult("Please provide an answer to this question.", new[] { "hasReportedBusinessUse" });
            }


            #endregion
            /***************************************************/

            if (!hasDependent.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Not all children listed.", new[] { "hasDependent" });
                yield return new ValidationResult("Unnacceptable Risk: Not all children listed.");

            }

            if (hasFloridaResident.ToString().Parse<bool>())
            {
                LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: PRINCIPLE RESIDENT ADDRESS AND CAR(S) GARAGING ADDRESS NOT SAME", Sessions.Instance.PolicyNo), "WARN");
                yield return new ValidationResult("The principal resident address and all vehicle(s) garaging address must be same.", new[] { "hasFloridaResident" });
                yield return new ValidationResult("The principal resident address and all vehicle(s) garaging address must be same.");
            }

            if (hasDrivingImpairments.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Impairment of driving ability.", new[] { "hasDrivingImpairments" });
                yield return new ValidationResult("Unnacceptable Risk: Impairment of driving ability.");
            }

            if (hasVehicleOwned.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Application or applicant's spouse not owner of a listed vehicle.", new[] { "hasVehicleOwned" });
                yield return new ValidationResult("Unnacceptable Risk: Application or applicant's spouse not owner of a listed vehicle.");

            }

            if (hasCoownerResident.GetValueOrDefault()) //QUERY//
            {
                yield return new ValidationResult("Unnacceptable Risk: Vehicles listed co-owned by a non-resident person or by a resident of the insured's household", new[] { "hasCoownerResident" });
                yield return new ValidationResult("Unnacceptable Risk: Vehicles listed co-owned by a non-resident person or by a resident of the insured's household");

            }

            if (hasVehicleNotGaraged.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: The principal resident address and all vehicle(s) garaging address must be same", new[] { "hasVehicleNotGaraged" });
                yield return new ValidationResult("Unnacceptable Risk: The principal resident address and all vehicle(s) garaging address must be same");
            }

            if (hasExistingDamage.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Vehicle(s) listed as salvaged and does NOT have physical damage coverage.", new[] { "hasExistingDamage" });
                yield return new ValidationResult("Unnacceptable Risk: Vehicle(s) listed as salvaged and does NOT have physical damage coverage.");
            }

            if (hasGrayMarketVehicle.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Gray Market vehicle.", new[] { "hasGrayMarketVehicle" });
                yield return new ValidationResult("Unnacceptable Risk: Gray Market vehicle.");
            }


            if (hasAutoInsuranceCancelledorPIPClaim.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Insurance has been cancelled or refused.", new[] { "hasAutoInsuranceCancelledorPIPClaim" });
                yield return new ValidationResult("Unnacceptable Risk: Insurance has been cancelled or refused.");
            }

            if (hasCriminalActivity.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Convicted or forfeited bail or any loan defaults in the past 3 years.", new[] { "hasCriminalActivity" });
                yield return new ValidationResult("Unnacceptable Risk: Convicted or forfeited bail or any loan defaults in the past 3 years.");
            }


            if (!hasVehicleForBusinessPurpose.GetValueOrDefault())
            {
                yield return new ValidationResult("Unnacceptable Risk: Vehicle used for commercial or business. All vehicle(s) used in a commercial ridesharing program or similar arrangement must be disclosed", new[] { "hasVehicleForBusinessPurpose" });
                yield return new ValidationResult("Unnacceptable Risk: Vehicle used for commercial or business. All vehicle(s) used in a commercial ridesharing program or similar arrangement must be disclosed");

            }



            if (!hasReportedBusinessUse.ToString().Parse<bool>())
            {
                LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: HAS NOT REPORTED BUSINESS USE", Sessions.Instance.PolicyNo), "WARN");
                yield return new ValidationResult("All vehicle(s) used for any business or commercial purposes must be reported.", new[] { "hasReportedBusinessUse" });
                yield return new ValidationResult("All vehicle(s) used for any business or commercial purposes must be reported."); // I have reported any business or commercial use of my auto(s).");
            }

            if ((!hasDependent.ToString().Parse<bool>()
                || hasFloridaResident.ToString().Parse<bool>()
                || hasDrivingImpairments.ToString().Parse<bool>()
                || hasVehicleOwned.ToString().Parse<bool>()
                || hasCoownerResident.ToString().Parse<bool>()
                || hasVehicleNotGaraged.ToString().Parse<bool>()
                || hasExistingDamage.ToString().Parse<bool>()
                || hasGrayMarketVehicle.ToString().Parse<bool>()
                || hasAutoInsuranceCancelledorPIPClaim.ToString().Parse<bool>()
                || hasCriminalActivity.ToString().Parse<bool>()
                || !hasVehicleForBusinessPurpose.ToString().Parse<bool>()
                || !hasReportedBusinessUse.ToString().Parse<bool>())
                && string.IsNullOrEmpty(hasVehicleOwnedRemarkOne)
                && string.IsNullOrEmpty(hasCoownerResidentRemarkOne)
                && string.IsNullOrEmpty(hasExistingDamageRemarkOne)
                && string.IsNullOrEmpty(hasCriminalActivityRemarkOne))
            {
                int vehiclelength = string.IsNullOrEmpty(hasVehicleOwnedRemarkOne) ? 0 : hasVehicleOwnedRemarkOne.Length;
                int coownerlength = string.IsNullOrEmpty(hasCoownerResidentRemarkOne) ? 0 : hasCoownerResidentRemarkOne.Length;
                int existingdamagelength = string.IsNullOrEmpty(hasExistingDamageRemarkOne) ? 0 : hasExistingDamageRemarkOne.Length;
                int criminalactivitylength = string.IsNullOrEmpty(hasCriminalActivityRemarkOne) ? 0 : hasCriminalActivityRemarkOne.Length;

                StringBuilder sb = new StringBuilder();
                sb.Append("Please provide REMARKS below for the following questions: ");

                if (vehiclelength < 4 && hasVehicleOwned.GetValueOrDefault())
                {
                    yield return new ValidationResult("REMARK required", new[] { "hasVehicleOwned" });
                    sb.Append("4, ");
                }

                if (coownerlength < 4 && hasCoownerResident.GetValueOrDefault())
                {
                    yield return new ValidationResult("REMARK required", new[] { "hasCoownerResident" });
                    sb.Append("5, ");
                }

                if (existingdamagelength < 4 && hasExistingDamage.GetValueOrDefault())
                {
                    yield return new ValidationResult("REMARK required", new[] { "hasExistingDamage" });
                    sb.Append("7, ");
                }

                if (criminalactivitylength < 4 && hasCriminalActivity.GetValueOrDefault())
                {
                    yield return new ValidationResult("REMARK required", new[] { "hasCriminalActivity" });
                    sb.Append("10, ");
                }
                string finalQuestionsString = sb.ToString();
                int lastIndexOf = finalQuestionsString.LastIndexOf(", ");
                if (lastIndexOf > 0)
                {
                    finalQuestionsString = finalQuestionsString.Remove(lastIndexOf, 2);
                    yield return new ValidationResult(finalQuestionsString);
                }
            }
            /**/
        }
    }//END QuestionnaireModel




    ////////////////////////////////////////////////////////////////////////////////////////////////

}//END Namespace
