﻿
jQuery.validator.addMethod("checkage",
    function (value, element, param) {

    var currDate = param;
    var sdoc = currDate.split('/');
    var dobDate = value;
    var sdob = dobDate.split('/');
    var driverIndex = sdoc[3];
         
    //pass year,month,date in new Date object.
    var vDOB = new Date(sdob[2], sdob[0] - 1, sdob[1], 0,0,0,0);
    //? 11/1var vDOC = new Date(sdoc[2], sdoc[1], sdoc[0]);
    var vDOC = new Date(sdoc[2], sdoc[0], sdoc[1] - 1);
    
    //getAge user define function to calculate age.
     var vYrs = calculateAge(vDOB.getMonth(), vDOB.getDate(), vDOB.getFullYear());
    var result = false;
    if (driverIndex == 1 && vYrs >= 18) { result = true; }
    else
    if (driverIndex != 1 && vYrs >= 14) { result = true; }
    
    return result;
});

jQuery.validator.unobtrusive.adapters.add("checkage", ["param"], function (options) {

    options.rules["checkage"] = options.params.param;
    options.messages["checkage"] = options.message;
});
function calculateAge(birthMonth, birthDay, birthYear) {
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();
    age = todayYear - birthYear;

    if (todayMonth < birthMonth - 1) {
        age--;
    }

    if (birthMonth == todayMonth && todayDay < birthDay) {
        age--;
    }
    return age;
}
//-----------------------------------------------------------------------------------------------
(function ($) {
    // The validator function
    $.validator.addMethod('rangeDate',
     function (value, element, param) {
         if (!value) {
             return true; // not testing 'is required' here!
         }
         try {
             var dateValue = $.datepicker.parseDate("mm/dd/yy", value);
         }
         catch (e) {
             return false;
         }
         return param.min <= dateValue && dateValue <= param.max;
     });

    // The adapter to support ASP.NET MVC unobtrusive validation
    $.validator.unobtrusive.adapters.add('rangedate', ['min', 'max'],
     function (options) {
         var params = {
             min: $.datepicker.parseDate("yy/mm/dd", options.params.min),
             max: $.datepicker.parseDate("yy/mm/dd", options.params.max)
         };

         options.rules['rangeDate'] = params;
         if (options.message) {
             options.messages['rangeDate'] = options.message;
         }
     });
 } (jQuery));
//-----------------------------------------------------------------------------------------------

