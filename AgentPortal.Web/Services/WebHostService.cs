﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Services
{
    public interface IWebHostService
    {
        bool IsDev();
        bool IsProduction();
    }

    public class WebHostService : IWebHostService
    {
        public bool IsDev()
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                bool isTestEnv = false;

                SystemVars sysVars = new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = 'SYSTEMISDEV'")).FirstOrDefault();
                if (sysVars != null)
                {
                    isTestEnv = sysVars.Value == "true" ? true : false;
                }

                return isTestEnv;
            }
        }

        public bool IsProduction()
        {
            string serverName = ConfigSettings.ReadSetting("IIS_SERVER");
            if (serverName.Equals("VM-AAINS-IIS"))
            {
                return true;
            }

            return false;
        }
    }
}