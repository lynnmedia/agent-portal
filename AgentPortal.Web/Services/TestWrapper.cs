﻿using AlertAuto.Integrations.Contracts.CardConnect;

namespace AgentPortal.Services
{
    public class TestWrapper
    {
        private readonly ITestService service;
        public TestWrapper(ITestService service)
        {
            this.service = service;
        }

        public string Test(string policynum)
        {
            return service.TestMethod(policynum);
        }
    }
}