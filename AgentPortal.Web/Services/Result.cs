﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Services
{
    public class Result<T>
    {
        public T Value;
        public bool Success;

        public Result()
        {
        }

        public Result(T value, bool success)
        {
            Value = value;
            Success = success;
        }
    }
}