﻿namespace AgentPortal.Services
{
    public interface IPalmsLogger{
        void LogIp(string username, string ipAddress);
        void Log(string username, string desc, string type = "DEBUG");
    }
}