﻿using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using AlertAuto.Integrations.Contracts.CardConnect;
using AlertAuto.Integrations.Contracts.MVR;
using AlertAuto.Integrations.Contracts.RAPA;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using log4net;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Xpo;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AgentLogins = AgentPortal.Support.DataObjects.AgentLogins;
using Blowfish = AgentPortal.Support.Utilities.Blowfish;

namespace AgentPortal.Services
{
    public class AgentService
    {
        private readonly ILog logger;
        private readonly WebHostService _webHostService;
        private readonly Blowfish _blowfish;

        public AgentService(WebHostService webHostService, Blowfish blowfish)
        {
            logger = LogManager.GetLogger("AgentService");
            _webHostService = webHostService;
            _blowfish = blowfish;
        }

        public EndorsementVehicleModel GetVehicle(string vehicleID, EndorsementPolicyModel endorseModel)
        {
            string carIndex;
            EndorsementVehicleModel vehicleModel;
            if (string.IsNullOrEmpty(vehicleID))
            {
                vehicleModel = new EndorsementVehicleModel();
                endorseModel.Vehicles.RemoveAll(x => x.OID == 0);
                vehicleModel.CarIndex = endorseModel.Vehicles.Count + 1;
                endorseModel.Vehicles.Add(vehicleModel);
                carIndex = vehicleModel.CarIndex.ToString();
                //LF 12/6/18  Delete any/all lienholders with endorseModel.PolicyNo and vehicleModel.CarIndex
                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteCarLienHolders", "PolicyNo", "CarIndex",
                    endorseModel.PolicyNo,
                    vehicleModel.CarIndex.ToString()); //LF  Verify**
            }
            else
            {
                vehicleModel = endorseModel.Vehicles.Find(v => v.VehicleId == vehicleID);
            }

            if (vehicleModel.CarIndex > 0)
            {
                if (vehicleModel.LienHolders == null)
                    vehicleModel.LienHolders = new List<EndorsementLienHolderModel>();
                else
                    vehicleModel.LienHolders.Clear();
                using (var uow = new UnitOfWork())
                {
                    var getLienHolders =
                        new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo,
                                vehicleModel.CarIndex));
                    foreach (var lien in getLienHolders)
                    {
                        var tmpModel = UtilitiesRating.MapLienHolderXpoToModel(lien);
                        vehicleModel.LienHolders.Add(tmpModel);
                    }
                }
            }

            return vehicleModel;
        }

        public byte[] GetPolicyDocs(string policyno, string portalcode)
        {
            var pdf = Encoding.ASCII.GetBytes(new string(' ', 100));

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getFormsReader = uow.ExecuteQuery(string.Format(
                    @"SELECT TOP 1 PortalCode, a.FileFolder+a.FileName AS ImageFile, ISNULL(Catalog, 0)
                                                                                    FROM AuditImageIndex a 
                                                                                    WHERE 
	                                                                                    a.FileNo = '{0}'
	                                                                                    AND ISNULL(Catalog, 0) > -1
	                                                                                    AND PortalCode = '{1}'
	                                                                                    ORDER BY IndexID DESC
                                                                                    ", policyno, portalcode));

                foreach (var item in getFormsReader.ResultSet[0].Rows)
                    using (Stream stream = new FileStream(item.Values[1].ToString(), FileMode.Open))
                    {
                        pdf = new byte[stream.Length - 1];
                        stream.Read(pdf, 0, pdf.Length);
                    }
            }

            return pdf;
        }

        public ViewPolicyModel GetPolicy(string id, HttpServerUtilityBase Server, UrlHelper Url)
        {
            ViewPolicyModel model;
            //clear PolicyQuoteRenTemp tables upon load
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                //SendESignRequestForReinstatementNoLossRequest(uow, id, "");
                model = LoadPolicy(id, true, Server, Url);
                uow.ExecuteSproc("spUW_ClearPolicyRenQuoteTemp", id.GenNextPolicyNo(), Sessions.Instance.Username,
                    new Guid().ToString());
                var currentEndorsementTypesForPolicy =
                    new XPCollection<PolicyEndorsementTypes>(uow, CriteriaOperator.Parse("PolicyNo = ?", id));
                if (currentEndorsementTypesForPolicy.Any())
                {
                    foreach (var endorsementType in currentEndorsementTypesForPolicy)
                        DbHelperUtils.DeleteRecord("PolicyEndorsementTypes", "Id", endorsementType.Id.ToString());

                    uow.CommitChanges();
                }

                //E-Sig
                var agent =
                    new XPCollection<XpoAgents>(uow,
                            CriteriaOperator.Parse("AgentCode = ?", Sessions.Instance.AgentCode))
                        .FirstOrDefault();
                var currentSignatures = new XPCollection<PolicySignature>(
                    XpoHelper.GetNewUnitOfWork(),
                    CriteriaOperator.Parse("PolicyNo = ?", id));
                var eSigModel = new ESignatureForQuoteModel(currentSignatures,
                    agent.EmailAddress, model.Ins1First, model.Ins1Last, model.MainEmail);
                if (eSigModel.AllSigned)
                {
                    var documentDownloadUrl =
                        IntegrationsUtility.GetEsignatureDocumentDownloadUrl(currentSignatures.First()
                            .SignatureRequestId);
                    eSigModel.ESignatureDocumentDownloadUrl = documentDownloadUrl;
                }

                model.ESignatureForQuoteModel = eSigModel;

                if (model.EFT)
                {
                    var policyBankInfo =
                        new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber= ?", id))
                            .OrderByDescending(p => p.IndexID).FirstOrDefault();
                    if (policyBankInfo != null)
                    {
                        model.EftAccountType = policyBankInfo.AccountType;
                        model.EftAccountLast4 = policyBankInfo.AccountNumber;
                    }
                }

                //if (model.PolicyStatus.Equals("CANCEL"))
                //{
                //    IList<PolicySignature> currentReinstatementSig = new XPCollection<PolicySignature>(XpoHelper.GetNewUnitOfWork(),
                //                         CriteriaOperator.Parse("PolicyNo = ? AND RequestType= 'ReinstatementRequest'", id)).OrderByDescending(desc => desc.RecordedDate).Take(2).ToList();
                //    ESignatureForQuoteModel eREISigModel = new ESignatureForQuoteModel(currentReinstatementSig,
                //        "aloksingh0701@gmail.com", model.Ins1First, model.Ins1Last, model.MainEmail);
                //    if (eREISigModel.AllSigned)
                //        model.IsReinstateAllowed = 2;
                //}
            }

            if (DateTime.Now > model.ExpDate && model.PolicyStatus.ToUpper().Equals("ACTIVE"))
                model.PolicyStatus = "EXPIRED";

            return model;
        }

        public double GetAmtDueForReinstatingPolicy(string policyNo)
        {
            using (var session = new Session())
            {
                double totalAmtDue = 0;
                var result = session.ExecuteSproc("spUW_GetAmtDueForReinstatementOfPolicy", new OperandValue(policyNo),
                    new OperandValue(Sessions.Instance.Username), new OperandValue(Guid.NewGuid().ToString()),
                    new OperandValue(0));
                if (result?.ResultSet[1] != null && result.ResultSet[1].Rows[0] != null &&
                    result.ResultSet[1].Rows[0].Values[1] != null)
                {
                    var isValidDouble =
                        double.TryParse(result.ResultSet[1].Rows[0].Values[1].ToString(), out totalAmtDue);
                }

                return totalAmtDue;
            }
        }

        public double GetBalanceToPayForReinstatement(string policyNo)
        {
            double amtBalance = 0;
            try
            {
                amtBalance = GetAmtDueForReinstatingPolicy(policyNo);
                if (amtBalance < 0)
                    amtBalance = 0;
            }
            catch (Exception ex)
            {
                amtBalance = 0;
            }
            finally
            {
                LogUtils.Log(Sessions.Instance.Username, policyNo + " : Balance Amount of Policy :" + amtBalance,
                    "INFO");
            }

            return amtBalance;
        }

        public DateTime GetIssuedReinstatementDate(string policyNo)
        {
            var dtReiDate = DateTime.Now.Date;
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    IList<PolicySignature> existingREISignatures =
                        new XPCollection<PolicySignature>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?  AND RequestType = 'ReinstatementRequest'",
                                    policyNo))
                            .OrderByDescending(desc => desc.RecordedDate).Take(1).ToList();

                    if (existingREISignatures != null && existingREISignatures.Count > 0)
                        dtReiDate = existingREISignatures[0].RecordedDate.Date;
                }
            }
            catch (Exception ex)
            {
                dtReiDate = DateTime.Now.Date;
            }

            return dtReiDate;
        }

        public double GetTotalDueOnPolicy(string policyNo)
        {
            using (var session = new Session())
            {
                double totalDue = 0;
                var result = session.ExecuteSproc("spUW_GetTotalDueOnPolicy", new OperandValue(policyNo),
                    new OperandValue(Sessions.Instance.Username), new OperandValue(Guid.NewGuid().ToString()),
                    new OperandValue(0));
                if (result?.ResultSet[1] != null && result.ResultSet[1].Rows[0] != null &&
                    result.ResultSet[1].Rows[0].Values[1] != null)
                {
                    var isValidDouble = double.TryParse(result.ResultSet[1].Rows[0].Values[1].ToString(), out totalDue);
                }

                return totalDue;
            }
        }

        public bool IsNonPayNoticeCancellationForPolToREI(string policyNo)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: IsNonPayNoticeCancellationForREI called ", Sessions.Instance.PolicyNo), "INFO");
            var isReinstatementAllowed = false;

            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var reinsatementAllowed = new OperandValue(isReinstatementAllowed);
                    var result = uow.ExecuteSproc("spUW_GetCancelledPolAllowedForAutoREI", policyNo,
                        Sessions.Instance.Username, reinsatementAllowed);

                    if (result?.ResultSet?[1] != null && result.ResultSet[1].Rows[0] != null &&
                        result.ResultSet[1].Rows[0].Values[1] != null)
                        isReinstatementAllowed = (bool)result.ResultSet[1].Rows[0].Values[1];

                    //  return isNonPaynoticeCancellation;
                }
            }
            catch (Exception ex)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: Exception in IsNonPayNoticeCancellationForREI PolicyNO:{1}", policyNo), "INFO");
            }
            finally
            {
                LogUtils.Log(Sessions.Instance.Username,
                    policyNo + " :Finally IsNonPayNoticeCancellationForREI returned as :" +
                    Convert.ToString(isReinstatementAllowed), "INFO");
            }

            return isReinstatementAllowed;
        }

        public string IsPolicyAllowedToReinstate(string policyNo, string fName, string lName, string insuredMainEmail,
            string agentEmail)
        {
            LogUtils.Log(Sessions.Instance.Username, string.Format(
                "{0}: Reinstatement Validation called from , AgentEMail:{1},InsuredEmail {2}",
                Sessions.Instance.PolicyNo, agentEmail, insuredMainEmail), "INFO");

            var stausOfPolicyToRei = string.Empty; // default : // REI option in UI not shown
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    if (IsNonPayNoticeCancellationForPolToREI(policyNo))
                    {
                        stausOfPolicyToRei = "Request Re-Instatement";
                        IEnumerable<PolicySignature> existingREISignatures = new XPCollection<PolicySignature>(uow,
                            CriteriaOperator.Parse("PolicyNo = ?  AND RequestType = 'ReinstatementRequest'", policyNo));

                        IList<PolicySignature> currentReinstatementSigList = new XPCollection<PolicySignature>(
                                XpoHelper.GetNewUnitOfWork(),
                                CriteriaOperator.Parse("PolicyNo = ? AND RequestType= 'ReinstatementRequest'",
                                    policyNo))
                            .OrderByDescending(desc => desc.RecordedDate).Take(2).ToList();

                        if (currentReinstatementSigList.Count > 0)
                        {
                            var eREISigModel = new ESignatureForQuoteModel(currentReinstatementSigList,
                                agentEmail, fName, lName, insuredMainEmail);


                            if (eREISigModel.AllSigned)
                                stausOfPolicyToRei = "Reinstate Policy To Active"; //Stage 3
                            else if (!eREISigModel.AllSigned)
                                stausOfPolicyToRei = "ESign Pending For Reinstatement";

                            if (eREISigModel.AllSigned)
                                if (GetBalanceToPayForReinstatement(policyNo) > 0)
                                    stausOfPolicyToRei = "Pay Balance To Reinstate"; //Stage 3 with Payment
                        }
                        else if (currentReinstatementSigList.Count <= 0)
                        {
                            stausOfPolicyToRei = "Request Re-Instatement";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtils.Log(Sessions.Instance.Username, "Error in Validation of Policy" + policyNo, "INFO");
                stausOfPolicyToRei = "";
            }

            return stausOfPolicyToRei;
        }

        public ViewPolicyModel LoadPolicy(string policyNo, bool isAgent, HttpServerUtilityBase Server,
            UrlHelper Url, bool isClient = false)
        {
            var model = new ViewPolicyModel();
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var selPolicy = new SelectedPolicyDO();
                selPolicy.agentcode = Sessions.Instance.AgentCode ?? string.Empty;
                selPolicy.policyno = policyNo;

                XpoPolicy getPolicyInfo;
                if (Sessions.Instance.AgentCode == "A00000003")
                {
                    getPolicyInfo = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                        .FirstOrDefault();
                }
                else if (isClient)
                {
                    var getClientInfo = new XPCollection<XpoClientLogin>(uow,
                        CriteriaOperator.Parse("PolicyNumber = ? AND EmailAddress = ?", policyNo,
                            Sessions.Instance.EmailAddress)).FirstOrDefault();

                    if (getClientInfo == null)
                    {
                        ViewPolicyModel nullModel = null;
                        return nullModel;
                    }

                    getPolicyInfo = new XPCollection<XpoPolicy>(uow,
                            CriteriaOperator.Parse("PolicyNo = ?", "PAFL0102132", Sessions.Instance.AgentCode))
                        .FirstOrDefault();
                }
                else
                {
                    getPolicyInfo = new XPCollection<XpoPolicy>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND AgentCode = ?", policyNo,
                                Sessions.Instance.AgentCode))
                        .FirstOrDefault();
                }

                model.PolicyNo = getPolicyInfo.PolicyNo;
                model.PriorPolicyNo = getPolicyInfo.PriorPolicyNo;
                model.NextPolicyNo = getPolicyInfo.NextPolicyNo;
                model.RenCount = getPolicyInfo.RenCount;
                model.State = getPolicyInfo.State;
                model.LOB = getPolicyInfo.LOB;
                model.EffDate = getPolicyInfo.EffDate;
                model.ExpDate = getPolicyInfo.ExpDate;
                model.DisplayStatus = PortalUtils.GetDisplayStatus(getPolicyInfo.PolicyNo);
                model.PolicyStatus = getPolicyInfo.PolicyStatus;
                model.RenewalStatus = getPolicyInfo.RenewalStatus;
                if (!string.IsNullOrEmpty(model.RenewalStatus) && model.RenewalStatus.Equals("QTE"))
                {
                    var policyRenewalQuote =
                        new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", policyNo))
                            .OrderByDescending(prq => prq.OID).FirstOrDefault();
                    if (policyRenewalQuote != null)
                    {
                        model.RenewalQuoteNumber = policyRenewalQuote.PolicyNo;
                        model.RenewalEffDate = policyRenewalQuote.EffDate;
                    }
                }

                model.DateBound = getPolicyInfo.DateBound;
                model.DateIssued = getPolicyInfo.DateIssued;
                model.CancelDate = getPolicyInfo.CancelDate;
                model.CancelType = getPolicyInfo.CancelType;
                model.Ins1First = getPolicyInfo.Ins1First;
                var languagePreference = new XPCollection<PolicyPreferences>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND Type = 'Preferred_Language'", policyNo)).FirstOrDefault();
                var preferredLanguage = languagePreference?.Value;
                if (string.IsNullOrEmpty(preferredLanguage)) preferredLanguage = "ENGLISH";

                model.InsuredPreferredLanguage = preferredLanguage;
                model.Ins1Last = getPolicyInfo.Ins1Last;
                model.Ins1MI = getPolicyInfo.Ins1MI;
                model.Ins1Suffix = getPolicyInfo.Ins1Suffix;
                model.Ins1FullName = getPolicyInfo.Ins1FullName;
                model.Ins2First = getPolicyInfo.Ins2First;
                model.Ins2Last = getPolicyInfo.Ins2Last;
                model.Ins2MI = getPolicyInfo.Ins2MI;
                model.Ins2Suffix = getPolicyInfo.Ins2Suffix;
                model.Ins2FullName = getPolicyInfo.Ins2FullName;
                model.Territory = getPolicyInfo.Territory;
                model.PolicyWritten = getPolicyInfo.PolicyWritten;
                model.PolicyAnnualized = getPolicyInfo.PolicyAnnualized;
                model.CommPrem = getPolicyInfo.CommPrem;
                model.DBSetupFee = getPolicyInfo.DBSetupFee;
                model.MaintenanceFee = getPolicyInfo.MaintenanceFee;
                model.PIPPDOnlyFee = getPolicyInfo.PIPPDOnlyFee;
                model.PolicyFee = getPolicyInfo.PolicyFee;
                model.SR22Fee = getPolicyInfo.SR22Fee;
                model.FHCFFee = getPolicyInfo.FHCFFee;
                model.RateCycle = getPolicyInfo.RateCycle;
                model.SixMonth = getPolicyInfo.SixMonth;
                model.PayPlan = getPolicyInfo.PayPlan;
                model.PaidInFullDisc = getPolicyInfo.PaidInFullDisc;
                model.EFT = getPolicyInfo.EFT;
                model.MailStreet = getPolicyInfo.MailStreet;
                model.MailCity = getPolicyInfo.MailCity;
                model.MailState = getPolicyInfo.MailState;
                model.MailZip = getPolicyInfo.MailZip;
                model.MailGarageSame = getPolicyInfo.MailGarageSame;
                model.GarageStreet = getPolicyInfo.GarageStreet;
                model.GarageCity = getPolicyInfo.GarageCity;
                model.GarageState = getPolicyInfo.GarageState;
                model.GarageZip = getPolicyInfo.GarageZip;
                model.GarageTerritory = getPolicyInfo.GarageTerritory;
                model.GarageCounty = getPolicyInfo.GarageCounty;
                model.HomePhone = getPolicyInfo.HomePhone;
                model.WorkPhone = getPolicyInfo.WorkPhone;
                model.MainEmail = getPolicyInfo.MainEmail;
                model.AltEmail = getPolicyInfo.AltEmail;
                model.Paperless = getPolicyInfo.Paperless;
                model.AgentCode = getPolicyInfo.AgentCode;
                model.CommAtIssue = getPolicyInfo.CommAtIssue;
                model.LOUCommAtIssue = getPolicyInfo.LOUCommAtIssue;
                model.ADNDCommAtIssue = getPolicyInfo.ADNDCommAtIssue;
                model.AgentGross = getPolicyInfo.AgentGross;
                model.PIPDed = getPolicyInfo.PIPDed;
                model.NIO = getPolicyInfo.NIO;
                model.NIRR = getPolicyInfo.NIRR;
                model.UMStacked = getPolicyInfo.UMStacked;
                model.HasBI = getPolicyInfo.HasBI;
                model.HasMP = getPolicyInfo.HasMP;
                model.HasUM = getPolicyInfo.HasUM;
                model.HasLOU = getPolicyInfo.HasLOU;
                model.BILimit = getPolicyInfo.BILimit;
                model.PDLimit = getPolicyInfo.PDLimit;
                model.UMLimit = getPolicyInfo.UMLimit;
                model.MedPayLimit = getPolicyInfo.MedPayLimit;
                model.Homeowner = getPolicyInfo.Homeowner;
                model.RenDisc = getPolicyInfo.RenDisc;
                model.TransDisc = (TransferDiscountOptions) getPolicyInfo.TransDisc;
                model.PreviousCompany = getPolicyInfo.PreviousCompany;
                model.PreviousExpDate = getPolicyInfo.PreviousExpDate;
                model.PreviousBI = getPolicyInfo.PreviousBI;
                model.PreviousBalance = getPolicyInfo.PreviousBalance;
                model.DirectRepairDisc = getPolicyInfo.DirectRepairDisc;
                model.UndTier = getPolicyInfo.UndTier;
                model.OOSEnd = getPolicyInfo.OOSEnd;
                model.LOUCost = getPolicyInfo.LOUCost;
                model.LOUAnnlPrem = getPolicyInfo.LOUAnnlPrem;
                model.ADNDLimit = getPolicyInfo.ADNDLimit;
                model.ADNDCost = getPolicyInfo.ADNDCost;
                model.ADNDAnnlPrem = getPolicyInfo.ADNDAnnlPrem;
                model.HoldRtn = getPolicyInfo.HoldRtn;
                model.NonOwners = getPolicyInfo.NonOwners;
                model.ChangeGUID = getPolicyInfo.ChangeGUID;
                model.policyCars = getPolicyInfo.policyCars;
                model.policyDrivers = getPolicyInfo.policyDrivers;
                model.Unacceptable = getPolicyInfo.Unacceptable;
                model.HousholdIndex = getPolicyInfo.HousholdIndex;
                model.RatingID = getPolicyInfo.RatingID;
                model.OID = getPolicyInfo.OID;
                model.Cars = getPolicyInfo.Cars;
                model.Drivers = getPolicyInfo.Drivers;
                model.IsReturnedMail = getPolicyInfo.IsReturnedMail;
                model.IsOnHold = getPolicyInfo.IsOnHold;
                model.IsClaimMisRep = getPolicyInfo.IsClaimMisRep;
                model.IsNoREI = getPolicyInfo.IsNoREI;
                model.IsSuspense = getPolicyInfo.IsSuspense;
                model.isAnnual = getPolicyInfo.isAnnual;
                model.Carrier = getPolicyInfo.Carrier;
                model.HasPriorCoverage = getPolicyInfo.HasPriorCoverage;
                model.HasPriorBalance = getPolicyInfo.HasPriorBalance;
                model.HasLapseNone = getPolicyInfo.HasLapseNone;
                model.HasPD = getPolicyInfo.HasPD;
                model.HasTOW = getPolicyInfo.HasTOW;
                model.PIPLimit = getPolicyInfo.PIPLimit;
                model.HasADND = getPolicyInfo.HasADND;
                model.MVRFee = getPolicyInfo.MVRFee;
                model.WorkLoss = getPolicyInfo.WorkLoss;
                model.RentalAnnlPrem = getPolicyInfo.RentalAnnlPrem;
                model.RentalCost = getPolicyInfo.RentalCost;
                model.HasLapse110 = getPolicyInfo.HasLapse110;
                model.HasLapse1131 = getPolicyInfo.HasLapse1131;
                model.AppQuestionAnswers = getPolicyInfo.AppQuestionAnswers;
                model.AppQuestionExplainOne = getPolicyInfo.AppQuestionExplainOne;
                model.AppQuestionExplainTwo = getPolicyInfo.AppQuestionExplainTwo;
                model.FromPortal = getPolicyInfo.FromPortal;
                model.FromRater = getPolicyInfo.FromRater;
                model.QuoteNo = getPolicyInfo.QuoteNo;
                model.AgencyRep = getPolicyInfo.AgencyRep;
                model.QuotedDate = getPolicyInfo.QuotedDate;
                model.Bridged = getPolicyInfo.Bridged;
                model.IsAllowRenPastExp = getPolicyInfo.IsAllowRenPastExp;
                model.AllowPymntWithin15Days = getPolicyInfo.AllowPymntWithin15Days;

                var agentUploads = new XPCollection<AuditImageIndex>(uow,
                    CriteriaOperator.Parse("FileNo = ? AND FormDesc = 'AGENT UPLOAD'", policyNo)).ToList();
                model.AgentUploads = agentUploads;


                var policyDocumetns = new XPCollection<AuditImageIndex>(uow,
                    CriteriaOperator.Parse(
                        "FileNo = ? AND FormDesc <> 'AGENT UPLOAD' AND Contains([FormDesc],'ID Cards')=False  AND Contains([FormDesc],'AAIC FLQQ (0120) Application')=False AND Contains([FormDesc],'RENEWAL INVOICE')=False " +
                        " AND Contains([FormDesc], 'AAIC 002 (1018) Policy Declarations') = False AND Contains([FormDesc], 'Payment Schedule') = False AND Contains([FormDesc],'RENEWAL QUOTE PAYMENT SCHEDULE')= False " +
                        " AND Contains([FormDesc],'RENEWAL POLICY DECLARATION')=False AND FormDesc <> 'Invoice' AND Category <> 'PROOF OF MAILING' " +
                        " AND Category <> 'APLUS' AND Category <> 'MVR' AND Category <> 'Policy Not Associated Drivers' AND Category <> 'RISKCHECK'",
                        policyNo)).ToList();


                model.PolicyDocuments = policyDocumetns.GroupBy(x => x.FormDesc)
                    .Select(x => x.OrderByDescending(y => y.IndexID).FirstOrDefault()).ToList();
                //------------------------------------------
                //Get Renewal Info
                var getRenQuote =
                    new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", model.PolicyNo));
                model.HasRenewal = getRenQuote.Count > 0 ? true : false;

                if (model.HasRenewal)
                {
                    model.RenEffDate = getRenQuote.FirstOrDefault().EffDate;
                    model.RenPymntPlan = getRenQuote.FirstOrDefault().PayPlan;
                    model.RenPremium = getRenQuote.FirstOrDefault().PolicyWritten.ToMoney();
                    model.RenQuoteAnnual = getRenQuote.FirstOrDefault().isAnnual;
                    model.RenQuoteSixMonth = getRenQuote.FirstOrDefault().SixMonth;

                    var renewalDepositAmountRequired = "";  // Issue: unused
                    decimal amountToDraft = 0;
                    var renewalDraftAmountresult = uow.ExecuteSproc("spUW_GetRenewalDraftAmount",
                        Sessions.Instance.PolicyNo, "NightlyProcessService", amountToDraft.ToString(),
                        Guid.NewGuid().ToString());
                    if (renewalDraftAmountresult != null &&
                        renewalDraftAmountresult.ResultSet[1] != null &&
                        renewalDraftAmountresult.ResultSet[1].Rows[0] != null &&
                        renewalDraftAmountresult.ResultSet[1].Rows[0].Values[1] != null &&
                        !string.IsNullOrEmpty(renewalDraftAmountresult.ResultSet[1].Rows[0].Values[1].ToString()))
                    {
                        var renewalAmountToDraft =
                            Convert.ToDouble(renewalDraftAmountresult.ResultSet[1].Rows[0].Values[1].ToString());
                        model.RenDepositAmt = renewalAmountToDraft.ToString("C");
                        model.RenMinDueAmt = (decimal) renewalAmountToDraft;
                    }

                    var getIntalls = new XPCollection<RenQuoteInstallments>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND IgnoreRec = '0'",
                            getRenQuote.FirstOrDefault().PolicyNo));
                    foreach (var install in getIntalls.Where(m => m.Descr == "Installment 1"))
                        model.MonthlyRenewalPymnt = install.InstallmentPymt.SafeString().Parse<double>();

                    foreach (var install in getIntalls)
                        model.RenInstallFees += install.InstallmentFee.SafeString().Parse<double>();

                    model.RenInstallFees = model.RenInstallFees + getRenQuote.FirstOrDefault().PolicyWritten;

                    var getPayPlans = new XPCollection<XpoAARatePayPlans>(uow);

                    var isAllowLowDp =
                        PortalRightsUtils.HasRights("LOW DOWN PAYMENT", Sessions.Instance.AgentCode, true);

                    if (isAllowLowDp)
                        getPayPlans.Criteria = CriteriaOperator.Parse(
                            "RateCycle = ? AND Term = ? AND NewOrRen <> 'N'",
                            getRenQuote.FirstOrDefault().RateCycle,
                            getRenQuote.FirstOrDefault().SixMonth ? "6" : "12");
                    else
                        getPayPlans.Criteria = CriteriaOperator.Parse(
                            "RateCycle = ? AND Term = ? AND NewOrRen <> 'N'", // AND Active = '1'",
                            getRenQuote.FirstOrDefault().RateCycle,
                            getRenQuote.FirstOrDefault().SixMonth ? "6" : "12");

                    model.RenPayPlans = new List<SelectListItem>();
                    foreach (var plan in getPayPlans)
                    {
                        var item = new SelectListItem();
                        item.Text = PortalUtils.GetEnglishPayPlan(model.RateCycle, plan.IndexID.SafeString());
                        item.Value = plan.IndexID.SafeString();
                        item.Selected = plan.IndexID == model.RenPymntPlan ? true : false;

                        model.RenPayPlans.Add(item);
                    }
                }

                //------------------------------------------
                //Get Coverage Info
                //ViewData["ViewPolCoverage"] = PalmsClient.GetCoverageInfo(selPolicy);
                model.GetCoverageInfo = PolicyWebUtils.GetCoverageInfo(model.PolicyNo);
                //------------------------------------------
                model.BillingGuid = Guid.NewGuid();
                UtilitiesStoredProcedure.ExecspUW_PrepBillingTab(new Session(), model.PolicyNo,
                    Sessions.Instance.Username,
                    model.BillingGuid, true);

                foreach (var bill in model.GetBillingInfo.OrderByDescending(m => m.IndexID)
                    .Where(m => m.RowDesc.Contains("Installment") || m.RowDesc.Contains("Deposit")).Take(1))
                    model.DateDue = bill.RowDate;

                model.TotalDue = GetTotalDueOnPolicy(policyNo);

                if (model.GetBillingInfo.Count > 0)
                    model.MinDue = model.GetBillingInfo[model.GetBillingInfo.Count - 1].MinDue;
                model.TotalPremium = model.GetBillingInfo.Sum(m => m.AmtBilled);
                model.TotalPaid = model.GetBillingInfo.Sum(m => m.Payment);
                model.TotalFees = model.GetBillingInfo.Sum(m => m.InstallFee) +
                                  model.GetBillingInfo.Sum(m => m.NSFFee) +
                                  model.GetBillingInfo.Sum(m => m.LateFee);

                model.AmtToPay = Math.Round(model.MinDue, 2, MidpointRounding.AwayFromZero) < 0.0
                    ? 0.0
                    : Math.Round(model.MinDue, 2, MidpointRounding.AwayFromZero);
                if (model.AmtToPay == 0) model.AmtToPay = -model.MinDue;

                //------------------------------------------
                //Get History
                model.GetHistory = PolicyWebUtils.GetHistory(model.PolicyNo);
                model.HistoryReasons = PolicyWebUtils.GetHistoryReasons(model.PolicyNo);
                //------------------------------------------
                var priorAmt =
                    model.TotalDue.SafeString().Parse<decimal>(); //------------------------------------------

                //------------------------------------------
                //string decpage = String.Format(@"C:\inetpub\wwwroot\Content\files\policydocs\{0}_DecPage.pdf", selPolicy.policyno);
                var decpage =
                    string.Format(
                        //Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_DecPage.pdf"),
                        HostingEnvironment.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_DecPage.pdf"),
                        selPolicy.policyno);
                using (Stream stream = new FileStream(decpage, FileMode.Create))
                {
                    try
                    {
                        var decBytes = GetPolicyDocs(selPolicy.policyno, "DEC");

                        stream.Write(decBytes, 0, decBytes.Length);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                var idcards =
                    string.Format(
                        Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_IDCard.pdf"),
                        selPolicy.policyno);

                using (Stream stream = new FileStream(idcards, FileMode.Create))
                {
                    try
                    {
                        var decBytes = GetPolicyDocs(selPolicy.policyno, "IDCARD");

                        stream.Write(decBytes, 0, decBytes.Length);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                var invoice =
                    string.Format(
                        Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_Invoice.pdf"),
                        selPolicy.policyno);
                using (Stream stream = new FileStream(invoice, FileMode.Create))
                {
                    try
                    {
                        var decBytes = GetPolicyDocs(selPolicy.policyno, "INVOICE");

                        stream.Write(decBytes, 0, decBytes.Length);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                var pymntsched =
                    string.Format(
                        Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") +
                                       @"policydocs/{0}_PaymentSchedule.pdf"), selPolicy.policyno);
                using (Stream stream = new FileStream(pymntsched, FileMode.Create))
                {
                    try
                    {
                        var decBytes = GetPolicyDocs(selPolicy.policyno, "PYMNTSCHEDULE");

                        stream.Write(decBytes, 0, decBytes.Length);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                var renewalDec =
                    string.Format(
                        Server.MapPath(
                            ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_RenewalDec.pdf"),
                        selPolicy.policyno);
                using (Stream stream = new FileStream(renewalDec, FileMode.Create))
                {
                    try
                    {
                        var decBytes = GetPolicyDocs(selPolicy.policyno, "RENDEC");
                        if (decBytes.Length > 100)
                            stream.Write(decBytes, 0, decBytes.Length);
                        else
                            model.RenDecFilename = "NONE";
                    }
                    catch (Exception ex)
                    {
                    }
                }

                var renewalPaySchd =
                    string.Format(
                        Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") +
                                       @"policydocs/{0}_RenewalPaySchedule.pdf"), selPolicy.policyno);
                using (Stream stream = new FileStream(renewalPaySchd, FileMode.Create))
                {
                    try
                    {
                        var decBytes = GetPolicyDocs(selPolicy.policyno, "RENPYMNTSCHEDULE");

                        if (decBytes.Length > 100)
                            stream.Write(decBytes, 0, decBytes.Length);
                        else
                            model.RenPaySchdFilename = "NONE";
                    }
                    catch (Exception ex)
                    {
                    }
                }

                if (File.Exists(decpage))
                {
                    model.decpagefilename = Url.Content(decpage);
                    model.decpagedatetime = File.GetCreationTime(decpage).ToShortDateString();
                    var auditPolicyHistoryRecordsForPolicy =
                        new XPCollection<AuditPolicyHistory>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?  AND Action = 'ENDORSE'", policyNo))
                            .OrderByDescending(aph => aph.IndexID).FirstOrDefault();
                    try
                    {
                        if (auditPolicyHistoryRecordsForPolicy != null)
                            model.decpagedatetime = auditPolicyHistoryRecordsForPolicy.ProcessDate.ToShortDateString();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                //------------------------------------------
                if (File.Exists(Url.Content(idcards)))
                {
                    model.idcarddatetime = File.GetCreationTime(idcards).ToShortDateString();
                    model.idcardfilename = Url.Content(idcards);
                }

                //------------------------------------------
                if (File.Exists(Url.Content(invoice)))
                {
                    model.invoicedatetime = File.GetCreationTime(invoice).ToShortDateString();
                    model.invoicefilename = Url.Content(invoice);
                }

                //------------------------------------------
                if (File.Exists(Url.Content(pymntsched)))
                {
                    model.pymtnscheduledatetime = File.GetCreationTime(pymntsched).ToShortDateString();
                    model.pymntschedule = Url.Content(pymntsched);
                }

                //------------------------------------------
                if (File.Exists(Url.Content(renewalDec)) && model.RenDecFilename != "NONE")
                {
                    model.RenDecDateTime = File.GetCreationTime(renewalDec).ToShortDateString();
                    model.RenDecFilename = Url.Content(renewalDec);
                }

                //------------------------------------------
                if (File.Exists(Url.Content(renewalPaySchd)) && model.RenPaySchdFilename != "NONE")
                {
                    model.RenPaySchdDateTime = File.GetCreationTime(renewalPaySchd).ToShortDateString();
                    model.RenPaySchdFilename = Url.Content(renewalPaySchd);
                }

                //------------------------------------------
                var getReasons = new XPCollection<XpoAuditPolicyHistoryReasons>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND HistoryAction = 'HOLD'", policyNo)).FirstOrDefault();
                if (getReasons != null) model.isOnHold = true;

                //------------------------------------------
                //Check If Policy Is or Has Been Renewed
                var getIfRenewal =
                    new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.PriorPolicyNo));
                model.IsRenewalPolicy = getIfRenewal.Count > 0 ? true : false;

                var prevPolicyNo = model.PolicyNo.GenPriorPolicyNo();
                var nextPolicyNo = model.PolicyNo.GenNextPolicyNo();


                var getOldPolicy =
                    new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", prevPolicyNo));
                var oldPolicyCount = getOldPolicy.Count;
                prevPolicyNo = oldPolicyCount == 0 ? prevPolicyNo.Split('-')[0] : prevPolicyNo;

                var priorPolicyCount = model.PolicyNo == prevPolicyNo
                    ? 0
                    : new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", prevPolicyNo)).Count();

                var nextPolicyCount =
                    new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", nextPolicyNo))
                        .Count();

                model.GenNextPolicyNo = nextPolicyCount == 0 ? "" : nextPolicyNo;
                model.GenPrevPolicyNo = priorPolicyCount == 0 ? "" : prevPolicyNo;
                if (string.IsNullOrEmpty(model.PolicyREIStageText)) model.PolicyREIStageText = string.Empty;
                var cancDateDiff = DateTime.Now - model.CancelDate;
                if (model.PolicyStatus == "CANCEL" && cancDateDiff.TotalDays <= 7 && model.GetOpenClaimsCount <= 0 &&
                    model.SuspenseReasons.Count <= 0 && cancDateDiff.TotalDays > 0)
                {
                    var agent = uow.FindObject<XpoAgents>(CriteriaOperator.Parse("AgentCode = ?", model.AgentCode));

                    model.PolicyREIStageText = IsPolicyAllowedToReinstate(model.PolicyNo, model.Ins1First,
                        model.Ins1Last,
                        model.MainEmail, agent.EmailAddress);

                    if (!string.IsNullOrEmpty(model.PolicyREIStageText) &&
                        model.PolicyREIStageText.Equals("Pay Balance To Reinstate"))
                        model.BalanceAmtToPayForREI = GetBalanceToPayForReinstatement(model.PolicyNo);

                    if (!string.IsNullOrEmpty(model.PolicyREIStageText))
                        model.DateOfReinstatement = GetIssuedReinstatementDate(model.PolicyNo);
                }
            }

            //ExtPortalUtils.CloseRef(PalmsClient);
            return model;
        }

        public bool HasMvr(string driverIndex)
        {
            bool hasMvr;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                hasMvr = false;
                var mvrForDriver =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ? AND FromMVR = 1",
                            Sessions.Instance.PolicyNo, driverIndex)).FirstOrDefault();
                if (mvrForDriver != null) hasMvr = true;
            }

            return hasMvr;
        }

        public AgentSearchPolicyModel AgentPolicySearch()
        {
            AgentSearchPolicyModel model;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username, "Viewing Search Page", "INFO");
                model = new AgentSearchPolicyModel();

                //10 Most recent quotes
                IList<XpoPolicyQuote> last10Quotes = new XPCollection<XpoPolicyQuote>(uow,
                        CriteriaOperator.Parse("AgentCode = ? AND QuotedDate > ?", Sessions.Instance.AgentCode,
                            DateTime.Now.Date.AddDays(-30))).OrderByDescending(pq => pq.QuotedDate).Take(10)
                    .ToList(); //within past 30 days

                IList<FoundPolicyModel> last10PolicyQuoteModels = new List<FoundPolicyModel>();

                foreach (var policyQuote in last10Quotes)
                {
                    var insuredName = string.Empty;
                    if (!string.IsNullOrEmpty(policyQuote.Ins1Last) && !string.IsNullOrEmpty(policyQuote.Ins1First))
                        insuredName = $"{policyQuote.Ins1Last}, {policyQuote.Ins1First}";
                    else if (!string.IsNullOrEmpty(policyQuote.Ins1Last))
                        insuredName = $"{policyQuote.Ins1Last}";
                    else if (!string.IsNullOrEmpty(policyQuote.Ins1First)) insuredName = $"{policyQuote.Ins1First}";

                    var pq = new FoundPolicyModel
                    {
                        Effdate = policyQuote.EffDate,
                        Insuredname = insuredName,
                        PolicyNo = policyQuote.PolicyNo
                    };
                    last10PolicyQuoteModels.Add(pq);
                }

                model.Last10Quotes = last10PolicyQuoteModels.ToList();

                //Most recent policies
                IList<XpoPolicy> last10Policies =
                    new XPCollection<XpoPolicy>(uow,
                            CriteriaOperator.Parse("AgentCode = ? AND EffDate > ?", Sessions.Instance.AgentCode,
                                DateTime.Now.AddDays(-30))).OrderByDescending(p => p.DateBound).Take(10)
                        .ToList(); //within pass 30 days
                if (last10Policies.Count < 10)
                {
                    last10Policies =
                        new XPCollection<XpoPolicy>(uow,
                                CriteriaOperator.Parse("AgentCode = ? AND EffDate > ?", Sessions.Instance.AgentCode,
                                    DateTime.Now.AddDays(-60))).OrderByDescending(p => p.DateBound).Take(10)
                            .ToList(); //within pass 60 days
                    if (last10Policies.Count < 10)
                        last10Policies =
                            new XPCollection<XpoPolicy>(uow,
                                    CriteriaOperator.Parse("AgentCode = ? AND EffDate > ?", Sessions.Instance.AgentCode,
                                        DateTime.Now.AddDays(-90))).OrderByDescending(p => p.DateBound).Take(10)
                                .ToList(); //within pass 30 days
                }

                IList<FoundPolicyModel> last10PolicyModels = new List<FoundPolicyModel>();
                foreach (var policy in last10Policies)
                {
                    var pq = new FoundPolicyModel
                    {
                        Effdate = policy.EffDate,
                        Insuredname = $"{policy.Ins1Last}, {policy.Ins1First}",
                        PolicyNo = policy.PolicyNo,
                        Status = policy.PolicyStatus
                    };
                    last10PolicyModels.Add(pq);
                }

                model.Last10Policies = last10PolicyModels.ToList();
            }

            return model;
        }

        public List<string> GetEndorseReasons(string policyNo)
        {
            var reasons = new List<string>();
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var result = uow.ExecuteSproc("spUW_GetEndorseReasons", policyNo);
                foreach (var row in result.ResultSet[0].Rows)
                {
                    reasons.Add(row.Values[2].ToString());
                }
            }
            return reasons;
        }

        public void LogIP(string userName, string password, HttpRequestBase request)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    var ipAddress = GetUserIP(request);
                    var dateofLog = DateTime.Now;
                    var log = new XpoDemoPortalLogs(uow);
                    log.IPAddress = ipAddress;
                    log.DateOfLog = dateofLog;
                    log.UserName = userName;
                    log.Save();
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                }
            }
        }

        public XpoPolicyQuote GetQuote(string quoteNumber)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                if (!string.IsNullOrEmpty(quoteNumber))
                {
                    Sessions.Instance.PolicyNo = quoteNumber;
                    var quote =
                        new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNumber))
                            .FirstOrDefault();

                    return quote;
                }
            }

            return null;
        }

        public void UpdateQuoteAgent(string quoteNumber, string AgentCode)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                if (!string.IsNullOrEmpty(quoteNumber))
                {
                    Sessions.Instance.PolicyNo = quoteNumber;
                    var quote =
                        new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNumber))
                            .FirstOrDefault();
                    if (quote != null)
                    {
                        quote.AgentCode = AgentCode;
                        quote.Save();
                        uow.CommitChanges();
                    }
                }
            }
        }

        public string GetUserIP(HttpRequestBase Request)
        {
            var ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList)) return ipList.Split(',')[0];

            return Request.ServerVariables["REMOTE_ADDR"];
        }

        public bool CheckLogin(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password)) return false;

            var isValidLogin = false;
            AgentLogins loginForUser = null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                loginForUser =
                    new XPCollection<AgentLogins>(uow,
                            CriteriaOperator.Parse("Username = ? AND IsActive = 1", userName))
                        .FirstOrDefault();


                if (loginForUser != null)
                {
                    var hashedpass = loginForUser.Password;
                    hashedpass = _blowfish.Decrypt_CBC(hashedpass);
                    var encPass = _blowfish.Encrypt_CBC(password);
                    var userPassHash = password;
                    if (userPassHash.Equals(hashedpass))
                        isValidLogin = true;
                    else
                        isValidLogin = false;
                }
            }

            return isValidLogin;
        }

        public string GetAgentCode(string userName)
        {
            string agentCode = null;
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    agentCode = new XPCollection<AgentLogins>(uow, CriteriaOperator.Parse("Username = ?", userName))
                        .FirstOrDefault().AgentCode;
                }
            }
            catch
            {
                agentCode = "";
            }

            return agentCode;
        }

        public string GetAgencyName(string code)
        {
            string agencyname = null;

            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    agencyname = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", code))
                        .FirstOrDefault().AgencyName;
                }
            }
            catch
            {
                agencyname = "";
            }

            return agencyname;
        }

        public string GetAgentName(string code)
        {
            string agentname = null;
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var agentDetails = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", code))
                        .FirstOrDefault();
                    if (agentDetails != null)
                        agentname = $"{agentDetails.PrimaryFirstName} {agentDetails.PrimaryLastName}";
                }
            }
            catch
            {
                agentname = "";
            }

            return agentname;
        }

        public AgentSearchPolicyModel PolicySearch(SearchPolicyModel msearch)
        {
            AgentSearchPolicyModel model;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var listFoundPolicies = new List<FoundPolicyModel>();
                var listFoundQuotes = new List<FoundPolicyModel>();
                model = new AgentSearchPolicyModel();
                if (!string.IsNullOrEmpty(msearch.PolicyNo) && !string.IsNullOrEmpty(msearch.InsuredName))
                {
                    model.SearchMessage =
                        "No Results Found. Both a Policy Number and an Insured name cannot be entered at the same time.";
                    return model;
                }

                string searchStr = null;
                if (string.IsNullOrEmpty(msearch.PolicyNo) && !string.IsNullOrEmpty(msearch.InsuredName))
                    searchStr = msearch.InsuredName.Replace("'", "''");
                else if (!string.IsNullOrEmpty(msearch.PolicyNo) && string.IsNullOrEmpty(msearch.InsuredName))
                    searchStr = msearch.PolicyNo.Replace("'", "''");
                else
                    searchStr = string.Empty;

                var searchVar = string.IsNullOrEmpty(msearch.PolicyNo) ? "Ins1FullName" : "PolicyNo";
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Search Requested, Insured Name: {0}, PolicyNo: {1}", msearch.InsuredName,
                        msearch.PolicyNo));


                XPCollection<XpoPolicy> getPolicy;
                IEnumerable<XpoPolicy> policies;

                if (Sessions.Instance.AgentCode == "A00000003")
                {
                    getPolicy = new XPCollection<XpoPolicy>(uow,
                        CriteriaOperator.Parse(string.Format("{0} LIKE '%{1}%' AND (IsShow IS NULL OR IsShow != '0')",
                            searchVar, searchStr)));
                    var sortPolicies = new SortingCollection();
                    sortPolicies.Add(new SortProperty("PolicyNo", SortingDirection.Ascending));
                    getPolicy.Sorting = sortPolicies;
                    policies = getPolicy.Where(m => m.EffDate.AddDays(msearch.NumDaysSearch) > DateTime.Now);
                }

                else
                {
                    getPolicy = new XPCollection<XpoPolicy>(uow,
                        CriteriaOperator.Parse(string.Format(
                            "{0} LIKE '%{1}%' AND AgentCode = '{2}' AND (IsShow IS NULL OR IsShow != '0')", searchVar,
                            searchStr, Sessions.Instance.AgentCode)));
                    var sortPolicies = new SortingCollection();
                    sortPolicies.Add(new SortProperty("PolicyNo", SortingDirection.Ascending));
                    getPolicy.Sorting = sortPolicies;
                    if (msearch.HideCancelledPolicies)
                        policies = getPolicy.Where(m =>
                            m.EffDate.AddDays(msearch.NumDaysSearch) > DateTime.Now &&
                            !m.PolicyStatus.Equals("CANCEL"));
                    else
                        policies = getPolicy.Where(m => m.EffDate.AddDays(msearch.NumDaysSearch) > DateTime.Now);
                }

                foreach (var policy in policies.Take(200))
                {
                    var fPolicy = new FoundPolicyModel();
                    fPolicy.PolicyNo = policy.PolicyNo;
                    fPolicy.Effdate = policy.EffDate;
                    fPolicy.Status = PortalUtils.GetDisplayStatus(policy.PolicyNo);
                    if (string.IsNullOrEmpty(policy.Ins1First) && string.IsNullOrEmpty(policy.Ins1Last))
                        fPolicy.Insuredname = string.Empty;
                    else
                        fPolicy.Insuredname = $"{policy.Ins1Last?.Upper()}, {policy.Ins1First?.ToUpper()}";

                    listFoundPolicies.Add(fPolicy);
                }

                XPCollection<XpoPolicyQuote> getQuote;
                IEnumerable<XpoPolicyQuote> quotes;

                if (Sessions.Instance.AgentCode == "A00000003")
                {
                    getQuote = new XPCollection<XpoPolicyQuote>(uow,
                        CriteriaOperator.Parse(string.Format("{0} LIKE '%{1}%' AND IsShow = '1'", searchVar,
                            searchStr)));
                    quotes = getQuote.Where(m => m.EffDate.AddDays(msearch.NumDaysSearch) > DateTime.Now);
                }
                else
                {
                    // getQuote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse(string.Format("{0} LIKE '%{1}%'  AND IsShow = '1' AND AgentCode = '{2}'", searchVar, searchStr, Sessions.Instance.AgentCode)));
                    getQuote = new XPCollection<XpoPolicyQuote>(uow,
                        CriteriaOperator.Parse(string.Format("{0} LIKE '%{1}%' AND AgentCode = '{2}'", searchVar,
                            searchStr, Sessions.Instance.AgentCode)));
                    quotes = getQuote.Where(m => m.EffDate.AddDays(msearch.NumDaysSearch) > DateTime.Now);
                }

                foreach (var quote in quotes.Take(200))
                {
                    var fPolicy = new FoundPolicyModel();
                    fPolicy.PolicyNo = quote.PolicyNo;
                    fPolicy.Effdate = quote.EffDate;
                    fPolicy.Status = "QUOTE";
                    if (string.IsNullOrEmpty(quote.Ins1First) && string.IsNullOrEmpty(quote.Ins1Last))
                        fPolicy.Insuredname = string.Empty;
                    else
                        fPolicy.Insuredname = $"{quote.Ins1Last?.Upper()}, {quote.Ins1First?.ToUpper()}";

                    listFoundQuotes.Add(fPolicy);
                }

                model.FoundPolicys = listFoundPolicies.OrderBy(p => p.Insuredname).OrderByDescending(p => p.Effdate)
                    .ToList();
                model.FoundQuotes = listFoundQuotes.OrderBy(p => p.Insuredname).OrderByDescending(p => p.Effdate)
                    .ToList();
                if (!model.FoundPolicys.Any() && !model.FoundQuotes.Any()) model.SearchMessage = "No Results Found.";

                model.AllSelected = msearch.NumDaysSearch.Equals(36524);
                model.NinetyDaysSelected = msearch.NumDaysSearch.Equals(90);
                model.ThirtyDaysSelected = msearch.NumDaysSearch.Equals(30);
                model.TenDaysSelected = msearch.NumDaysSearch.Equals(10);
                model.HideCancelledPolicies = msearch.HideCancelledPolicies;
            }

            return model;
        }

        public int CalcDriverPoints(string policyNo, int driverIndex)
        {
            var points = 0;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var currViolations =
                    new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse(
                        "PolicyNo = ? AND DriverIndex = ?",
                        policyNo, driverIndex));
                foreach (var driverViolation in currViolations) points += driverViolation.ViolationPoints;
            }

            return points;
        }

        public AgentAuthTokens GetAgentAuthToken(string quoteNumber, Guid? token)
        {
            AgentAuthTokens agentToken;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                agentToken = uow.FindObject<AgentAuthTokens>(CriteriaOperator.Parse("PolicyNo = ? AND Token = ?",
                    quoteNumber,
                    token));
            }

            return agentToken;
        }

        public void AddDriverEndorsement()
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var endorsementType = "ADD_UPDATE_DRIVER";
                IEnumerable<PolicyEndorsementTypes> existingTypes =
                    new XPCollection<PolicyEndorsementTypes>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND EndorsementType = ?", Sessions.Instance.PolicyNo,
                            endorsementType));
                if (!existingTypes.Any())
                {
                    var newType = new PolicyEndorsementTypes(uow)
                    {
                        PolicyNo = Sessions.Instance.PolicyNo,
                        EndorsementType = endorsementType
                    };
                    newType.Save();
                    uow.CommitChanges();
                }
            }
        }

        public void EndorseDriverUpdate(EndorsementDriverModel model, EndorsementPolicyModel endorsePolicy)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getDrivers =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse(
                        "PolicyNo = ? AND DriverIndex = ?",
                        Sessions.Instance.PolicyNo,
                        model.DriverIndex));

                var driver = getDrivers.FirstOrDefault();

                getDrivers.Remove(driver);
                driver.PolicyNo = Sessions.Instance.PolicyNo;
                UtilitiesRating.MapModelToXpo(model, driver);
                driver.Points = CalcDriverPoints(Sessions.Instance.PolicyNo, model.DriverIndex); //LF 11/7/18
                driver.Save();
                uow.CommitChanges();

                //if (model.OldDlNum != model.DriversLicense)
                //{
                //    XPCollection<PolicyEndorseQuoteDrivers> updateDriver = new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", Sessions.Instance.PolicyNo, model.DriverIndex));

                //    foreach (PolicyEndorseQuoteDrivers driver1 in updateDriver)
                //    {
                //        PolicyEndorseQuoteDrivers Update = uow.FindObject<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("OID = ?", driver1.OID));
                //        Update.DriverMsg = null;
                //        Update.Save();
                //        uow.CommitChanges();
                //    }
                //}
                getDrivers.Add(driver);
                endorsePolicy.Drivers.Add(model);
            }
        }

        public void EndorseDriverAdd(EndorsementDriverModel model, EndorsementPolicyModel endorsePolicy)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getDrivers =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));

                if (getDrivers.Count > 10)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: DRIVERS COUNT > 10", Sessions.Instance.PolicyNo), "INFO");
                    throw new HttpException(400, "One policy may only have 10 driver maximum");
                }

                //LF 11/7/18
                var newDriverIndex = getDrivers.Count + 1;
                var dpoints = 0;
                var getViolations =
                    new XPCollection<PolicyQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?",
                            Sessions.Instance.PolicyNo,
                            0));

                foreach (var driverViolation in getViolations)
                {
                    dpoints += driverViolation.ViolationPoints;
                    driverViolation.DriverIndex = newDriverIndex;
                    driverViolation.Save();
                    uow.CommitChanges();
                }
                //LF 11/7/28

                var driver = new PolicyEndorseQuoteDrivers(uow);
                driver.DriverIndex = newDriverIndex; // getDrivers.Count + 1;  //LF 11/7/18
                driver.PolicyNo = Sessions.Instance.PolicyNo;
                UtilitiesRating.MapModelToXpo(model, driver);
                driver.Points = dpoints; //LF 11/7/18
                driver.Save();
                uow.CommitChanges();
                getDrivers.Add(driver);
                endorsePolicy.Drivers.Add(model);
            }
        }

        public EndorsementDriverModel LoadDriverData(string id, EndorsementPolicyModel endorseModel)
        {
            EndorsementDriverModel driverModel;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var driver =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("OID = ?", id))
                        .FirstOrDefault();

                driverModel = UtilitiesRating.MapXpoToModel(driver);
                driverModel.IsEdit = true;
                var getViolations =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", Sessions.Instance.PolicyNo,
                            driverModel.DriverIndex));
                driverModel.Violations = new List<EndorsementViolationModel>();
                var count = getViolations.Count;
                foreach (var driverViolation in getViolations)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: LOADING DRIVER VIOLATIONS DATA", Sessions.Instance.PolicyNo), "INFO");
                    var vModel = new EndorsementViolationModel();
                    vModel.ViolationId = driverViolation.OID;
                    vModel.Description = driverViolation.ViolationDesc;
                    vModel.Date = driverViolation.ViolationDate;
                    vModel.Points = driverViolation.ViolationPoints;
                    vModel.FromMvr = driverViolation.FromMVR;
                    vModel.DriverIndex = driverViolation.DriverIndex;
                    driverModel.Violations.Add(vModel);
                }

                var tmpModel = endorseModel.Drivers.Find(v => v.DriverId == id);
                endorseModel.Drivers.Remove(tmpModel);
                endorseModel.Drivers.Add(driverModel);
            }

            return driverModel;
        }

        public EndorsementDriverModel GetDriver(string tmpfName, string tmplName, string tmpMI, string tmpfName2,
            string tmplName2, string tmpMI2, string driverID, EndorsementPolicyModel endorseModel,
            out int newDriverIndex)
        {
            EndorsementDriverModel driverModel;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                newDriverIndex = new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?",
                    Sessions.Instance.PolicyNo)).Count + 1;
                if (string.IsNullOrEmpty(driverID))
                {
                    driverModel = new EndorsementDriverModel { DriverIndex = newDriverIndex };
                    if (newDriverIndex == 1)
                    {
                        driverModel.FirstName = (tmpfName ?? string.Empty).ToUpper();
                        driverModel.LastName = (tmplName ?? string.Empty).ToUpper();
                        driverModel.MiddleInitial = (tmpMI ?? string.Empty).ToUpper();
                    }
                    else if (newDriverIndex == 2)
                    {
                        driverModel.FirstName = (tmpfName2 ?? string.Empty).ToUpper();
                        driverModel.LastName = (tmplName2 ?? string.Empty).ToUpper();
                        driverModel.MiddleInitial = (tmpMI2 ?? string.Empty).ToUpper();
                    }

                    //LF 12/9/18  Delete any/all violations with endorseModel.PolicyNo and driverModel.DriverIndex (since just created) (copied from QuoteController
                    DbHelperUtils.DeleteRecord("PolicyEndorseQuoteDriversViolations", "PolicyNo", "DriverIndex",
                        endorseModel.PolicyNo, driverModel.DriverIndex.ToString());
                }
                else
                {
                    driverModel = endorseModel.Drivers.Find(v => v.DriverId == driverID);
                }

                if (driverModel.DriverIndex == 0) driverModel.DriverIndex = newDriverIndex;

                var violations =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", Sessions.Instance.PolicyNo,
                            driverModel.DriverIndex));
                if (violations.Any())
                {
                    driverModel.Violations = new List<EndorsementViolationModel>();
                    foreach (var violation in violations)
                    {
                        if (violation.FromMVR) driverModel.HasMVR = true;

                        driverModel.Violations.Add(UtilitiesRating.MapXpoToModel(violation));
                    }
                }
            }

            return driverModel;
        }

        public IList<PolicyEndorseQuoteViolationModel> HandleMvrOrderForEndorsement(string policyNo,
            EndorsementDriverModel driver,
            VeriskMvrSubmitRequestBody mvrReportResult)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                IList<PolicyEndorseQuoteViolationModel> violations = new List<PolicyEndorseQuoteViolationModel>();
                var violationCodes = new XPCollection<ViolationCodes_SVC>(uow);
                var currentViolationsForQuote =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                if (mvrReportResult != null && !string.IsNullOrEmpty(mvrReportResult.OrderNumber))
                {
                    Thread.Sleep(3000);
                    var reportForDriver =
                        IntegrationsUtility.MvrRequestReport(policyNo, mvrReportResult.OrderNumber);
                    if (reportForDriver != null)
                        foreach (var report in reportForDriver.MotorVehicleReports)
                            if (!report.Status.Code.ToUpper().Equals("C"))
                            {
                                if (!report.Status.Code.ToUpper().Equals("1") &&
                                    !report.Status.Code.ToUpper().Equals("2"))
                                {
                                    if (report.Driver != null)
                                    {
                                        if (report.Driver.Violations != null &&
                                            report.Driver.Violations.Count() > 0)
                                        {
                                            dynamic driverViolationsModel = new ExpandoObject();
                                            driverViolationsModel.DriverName =
                                                $"{driver.FirstName} {driver.LastName}";
                                            driverViolationsModel.DriversViolations =
                                                new List<PolicyEndorseQuoteDriversViolations>();
                                            foreach (var violation in report.Driver?.Violations)
                                            {
                                                var svcViolationCode =
                                                    violationCodes.FirstOrDefault(vc =>
                                                        vc.SVC.Equals(violation.CustomCode));

                                                XpoAARateViolations rateViolation = null;
                                                if (svcViolationCode != null)
                                                    rateViolation =
                                                        new XPCollection<XpoAARateViolations>(uow,
                                                                CriteriaOperator.Parse("Code = ?",
                                                                    svcViolationCode.AAICCode))
                                                            .FirstOrDefault();

                                                var indexNo = currentViolationsForQuote.Count + 1;
                                                var violationDate = DateTime.MinValue;
                                                if (!string.IsNullOrEmpty(violation.ViolationDate))
                                                    violationDate = DateTime.ParseExact(violation.ViolationDate,
                                                        "yyyyMMdd", CultureInfo.InvariantCulture);
                                                else if (!string.IsNullOrEmpty(violation.ConvictionDate))
                                                    violationDate = DateTime.ParseExact(violation.ConvictionDate,
                                                        "yyyyMMdd", CultureInfo.InvariantCulture);

                                                var totalPoints = 0;
                                                var aaicGroup = "";
                                                if (rateViolation != null)
                                                {
                                                    totalPoints = rateViolation.PointsFirst;
                                                    aaicGroup = rateViolation.ViolationGroup;
                                                }

                                                var aaicCode = "";
                                                var desc = violation.ViolationType;
                                                if (svcViolationCode != null)
                                                {
                                                    aaicCode = svcViolationCode.AAICCode;
                                                    desc = svcViolationCode.DESCRIPTION;
                                                }

                                                if (violationDate.Date > DateTime.Now.Date) totalPoints = 0;

                                                var newDriverViolation =
                                                    new PolicyEndorseQuoteDriversViolations(uow)
                                                    {
                                                        PolicyNo = policyNo,
                                                        DriverIndex = driver.DriverIndex,
                                                        ViolationPoints = totalPoints,
                                                        FromMVR = true,
                                                        Chargeable = true,
                                                        ViolationCode = aaicCode,
                                                        ViolationDate = violationDate,
                                                        ViolationGroup = aaicGroup,
                                                        ViolationDesc = desc,
                                                        ViolationNo = indexNo,
                                                        ViolationIndex = indexNo
                                                    };

                                                driverViolationsModel.DriversViolations.Add(newDriverViolation);
                                                violations.Add(new PolicyEndorseQuoteViolationModel
                                                {
                                                    Name = desc,
                                                    ViolationDate =
                                                        newDriverViolation.ViolationDate.ToShortDateString(),
                                                    ViolationPoints = newDriverViolation.ViolationPoints
                                                });
                                                currentViolationsForQuote.Add(newDriverViolation);
                                                newDriverViolation.Save();
                                            }
                                        }
                                        else
                                        {
                                            var clearReport =
                                                CreateMVRClearReportForEndorsement(
                                                    policyNo,
                                                    currentViolationsForQuote, driver);
                                            if (clearReport != null) violations.Add(clearReport);
                                        }
                                    }
                                }
                                else
                                {
                                    var noHit = CreateMVRNoHitForEndorsement(policyNo,
                                        currentViolationsForQuote,
                                        driver);
                                    if (noHit != null) violations.Add(noHit);
                                }
                            }
                            else
                            {
                                var clearReport = CreateMVRClearReportForEndorsement(
                                    policyNo, currentViolationsForQuote, driver);
                                if (clearReport != null) violations.Add(clearReport);
                            }
                }
                else
                {
                    var noHit = CreateMVRNoHitForEndorsement(policyNo,
                        currentViolationsForQuote, driver);
                    if (noHit != null) violations.Add(noHit);
                }

                uow.CommitChanges();
                return violations;
            }
        }

        public PolicyEndorseQuoteViolationModel CreateMVRNoHitForEndorsement(string policyNo,
            IList<PolicyEndorseQuoteDriversViolations> currentViolationsForEndorsementQuote,
            EndorsementDriverModel driver)
        {

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var noHitRateViolation =
                    new XPCollection<XpoAARateViolations>(uow,
                            CriteriaOperator.Parse("Code = ?", "UNV002"))
                        .FirstOrDefault();
                if (noHitRateViolation != null)
                {
                    var noHitViolationModel = new MvrViolationsModel();
                    var indexNo = currentViolationsForEndorsementQuote.Count + 1;
                    var newDriverViolation =
                        new PolicyEndorseQuoteDriversViolations(uow)
                        {
                            PolicyNo = policyNo,
                            DriverIndex = driver.DriverIndex,
                            ViolationPoints = Convert.ToInt32(noHitRateViolation.PointsFirst),
                            FromMVR = true,
                            Chargeable = noHitRateViolation.Chargeable,
                            ViolationCode = noHitRateViolation.Code,
                            ViolationDate = DateTime.Now,
                            ViolationGroup = noHitRateViolation.ViolationGroup,
                            ViolationDesc = noHitRateViolation.LongDesc,
                            ViolationIndex = indexNo,
                            ViolationNo = indexNo
                        };
                    if (driver.IsInternationalLicense)
                    {
                        newDriverViolation.ViolationDesc = "INTERNATIONAL NO-HIT MVR";
                        newDriverViolation.ViolationPoints = 0;
                        newDriverViolation.Chargeable = false;
                    }

                    currentViolationsForEndorsementQuote.Add(newDriverViolation);
                    newDriverViolation.Save();
                    uow.CommitChanges();
                    var driverViolationModel = new PolicyEndorseQuoteViolationModel
                    {
                        Name = newDriverViolation.ViolationDesc,
                        ViolationDate = newDriverViolation.ViolationDate.ToShortDateString(),
                        ViolationPoints = newDriverViolation.ViolationPoints
                    };

                    return driverViolationModel;
                }
            }

            return null;
        }

        public PolicyEndorseQuoteViolationModel CreateMVRClearReportForEndorsement(string policyNo,
            IList<PolicyEndorseQuoteDriversViolations> currentViolationsForEndorsementQuote,
            EndorsementDriverModel driver)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var indexNo = currentViolationsForEndorsementQuote.Count + 1;
                var newDriverViolation =
                    new PolicyEndorseQuoteDriversViolations(uow)
                    {
                        PolicyNo = policyNo,
                        DriverIndex = driver.DriverIndex,
                        FromMVR = true,
                        Chargeable = true,
                        ViolationCode = "CLR001",
                        ViolationGroup = "CLEAR",
                        ViolationDesc = "Clear Report",
                        ViolationNo = indexNo,
                        ViolationIndex = indexNo
                    };
                newDriverViolation.Save();
                var driverViolationModel = new PolicyEndorseQuoteViolationModel
                {
                    Name = newDriverViolation.ViolationDesc,
                    ViolationDate = DateTime.Now.ToShortDateString(),
                    ViolationPoints = 0
                };

                return driverViolationModel;
            }
        }

            public XPCollection<PolicyEndorseQuoteDrivers> DeleteDriver(string id)
        {
            XPCollection<PolicyEndorseQuoteDrivers> getDrivers;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var driver =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("OID = ?", id))
                        .FirstOrDefault();
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: DELETE DRIVER WITH OID {1}", Sessions.Instance.PolicyNo, id), "WARN");
                if (driver == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: QUOTE NOT FOUND", Sessions.Instance.PolicyNo), "FATAL");
                    throw new HttpException(404, "Driver not found");
                }

                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteDriversViolations", "PolicyNo", "DriverIndex",
                    driver.PolicyNo,
                    driver.DriverIndex.ToString());

                driver.Delete();
                uow.CommitChanges();
                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteDrivers", "OID", id);

                getDrivers = new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", driver.PolicyNo),
                    new SortProperty("DriverIndex", SortingDirection.Ascending));

                foreach (var quoteDrivers in getDrivers.Where(x => x.DriverIndex > driver.DriverIndex))
                {
                    var getViolations =
                        new XPCollection<PolicyEndorseQuoteDriversViolations>(uow, CriteriaOperator.Parse(
                            "PolicyNo = ? AND DriverIndex = ?", quoteDrivers.PolicyNo, quoteDrivers.DriverIndex));
                    foreach (var violation in getViolations)
                    {
                        violation.DriverIndex--;
                        violation.Save();
                    }

                    quoteDrivers.DriverIndex--;
                    quoteDrivers.Save();
                }

                uow.CommitChanges();
            }

            return getDrivers;
        }

        public XpoPolicy FindPolicy(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var policy = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", id));
                return policy;
            }
        }

        public PolicyEndorseQuote FindPolicyEndorseQuote(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var endorsement =
                    uow.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", id));
                return endorsement;
            }
        }

        public void ClearEndorseQuoteTables(string id, string endorsementReasons)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                // uow.ExecuteSproc("spUW_ClearPolicyEndorseQuote", id, Sessions.Instance.Username, new Guid().ToString());
                uow.ExecuteSproc("spUW_CopyPolicyToPolicyEndorseQuoteTables", id, Sessions.Instance.Username,
                    new Guid().ToString(), endorsementReasons);
            }
        }

        public bool CheckUpdateCar(PolicyEndorseQuoteCars endorsementCar, bool isCarValid,
            PolicyCars carOnPolicy)
        {
            if (carOnPolicy != null)
            {
                if (!carOnPolicy.HasPhyDam.Equals(endorsementCar.HasPhyDam)) isCarValid = false;
                if (carOnPolicy.CompDed != null && !carOnPolicy.CompDed.Equals(endorsementCar.CompDed))
                    isCarValid = false;
                if (carOnPolicy.CollDed != null && !carOnPolicy.CollDed.Equals(endorsementCar.CollDed))
                    isCarValid = false;
                if (carOnPolicy.MilesToWork != endorsementCar.MilesToWork) isCarValid = false;
                if (carOnPolicy.PurchNew != endorsementCar.PurchNew || carOnPolicy.Artisan != endorsementCar.Artisan ||
                    carOnPolicy.ConvTT != endorsementCar.ConvTT || endorsementCar.ARB != carOnPolicy.ARB)
                    isCarValid = false;
            }
            else // new vehicle
            {
                if (endorsementCar.HasPhyDam)
                {
                    double compDed = 0, collDed = 0;
                    if (!string.IsNullOrEmpty(endorsementCar.CompDed))
                        double.TryParse(endorsementCar.CompDed, out compDed);
                    if (!string.IsNullOrEmpty(endorsementCar.CollDed))
                        double.TryParse(endorsementCar.CollDed, out collDed);
                    if (collDed > 0 || compDed > 0) isCarValid = false;
                }
            }

            return isCarValid;
        }

        public bool CheckPolicyInfoUpdated(ref bool isValid, XpoPolicy policyForEndorsementQuote,
            PolicyEndorseQuote policyEndorsementQuote, ref string propertiesUpdated)
        {
            if (policyEndorsementQuote.Ins1First.SafeTrim().ToLower() !=
                policyForEndorsementQuote.Ins1First.SafeTrim().ToLower() ||
                policyEndorsementQuote.Ins1Last.SafeTrim().ToLower() !=
                policyForEndorsementQuote.Ins1Last.SafeTrim().ToLower() ||
                policyEndorsementQuote.Ins1MI.SafeTrim().ToLower() !=
                policyForEndorsementQuote.Ins1MI.SafeTrim().ToLower())
            {
                isValid = false;
                propertiesUpdated += "Insured1Name|";
            }

            if (policyEndorsementQuote.Ins2First.SafeTrim().ToLower() !=
                policyForEndorsementQuote.Ins2First.SafeTrim().ToLower() ||
                policyEndorsementQuote.Ins2Last.SafeTrim().ToLower() !=
                policyForEndorsementQuote.Ins2Last.SafeTrim().ToLower() ||
                policyEndorsementQuote.Ins2MI.SafeTrim().ToLower() !=
                policyForEndorsementQuote.Ins2MI.SafeTrim().ToLower())
            {
                isValid = false;
                propertiesUpdated += "Insured2Name|";
            }

            if (policyEndorsementQuote.MailStreet.SafeTrim().ToLower() !=
                policyForEndorsementQuote.MailStreet.SafeTrim().ToLower() ||
                policyEndorsementQuote.MailCity.SafeTrim().ToLower() !=
                policyForEndorsementQuote.MailCity.SafeTrim().ToLower() ||
                policyEndorsementQuote.MailState.SafeTrim().ToLower() !=
                policyForEndorsementQuote.MailState.SafeTrim().ToLower() ||
                policyEndorsementQuote.MailZip.SafeTrim().ToLower() !=
                policyForEndorsementQuote.MailZip.SafeTrim().ToLower())
            {
                isValid = false;
                propertiesUpdated += "MailAddress|";
            }

            if (policyEndorsementQuote.GarageStreet.SafeTrim().ToLower() !=
                policyForEndorsementQuote.GarageStreet.SafeTrim().ToLower() ||
                policyEndorsementQuote.GarageCity.SafeTrim().ToLower() !=
                policyForEndorsementQuote.GarageCity.SafeTrim().ToLower() ||
                policyEndorsementQuote.GarageState.SafeTrim().ToLower() !=
                policyForEndorsementQuote.GarageState.SafeTrim().ToLower() ||
                policyEndorsementQuote.GarageZip.SafeTrim().ToLower() !=
                policyForEndorsementQuote.GarageZip.SafeTrim().ToLower())
            {
                isValid = false;
                propertiesUpdated += "GarageAddress|";
            }

            if (policyEndorsementQuote.PIPDed != null &&
                !policyEndorsementQuote.PIPDed.Equals(policyForEndorsementQuote.PIPDed))
            {
                isValid = false;
                propertiesUpdated += "PIPDed|";
            }

            if (policyEndorsementQuote.PDLimit != null &&
                !policyEndorsementQuote.PDLimit.Equals(policyForEndorsementQuote.PDLimit))
            {
                isValid = false;
                propertiesUpdated += "PDLimit|";
            }

            if (!policyEndorsementQuote.WorkLoss.Equals(policyForEndorsementQuote.WorkLoss))
            {
                isValid = false;
                propertiesUpdated += "WorkLoss|";
            }

            if (!policyEndorsementQuote.NIO.Equals(policyForEndorsementQuote.NIO))
            {
                isValid = false;
                propertiesUpdated += "NIO|";
            }

            if (!policyEndorsementQuote.NIRR.Equals(policyForEndorsementQuote.NIRR))
            {
                isValid = false;
                propertiesUpdated += "NIRR|";
            }

            if (!policyEndorsementQuote.HasPPO.Equals(policyForEndorsementQuote.HasPPO))
            {
                isValid = false;
                propertiesUpdated += "HasPPO|";
            }

            if (!policyEndorsementQuote.TransDisc.Equals(policyForEndorsementQuote.TransDisc))
            {
                isValid = false;
                propertiesUpdated += "TransDisc|";
            }

            if (!policyEndorsementQuote.Homeowner.Equals(policyForEndorsementQuote.Homeowner))
            {
                isValid = false;
                propertiesUpdated += "Homeowner|";
            }

            if (!policyEndorsementQuote.Paperless.Equals(policyForEndorsementQuote.Paperless))
            {
                isValid = false;
                propertiesUpdated += "Paperless|";
            }

            if (!policyEndorsementQuote.AdvancedQuote.Equals(policyForEndorsementQuote.AdvancedQuote))
            {
                isValid = false;
                propertiesUpdated += "AdvancedQuote|";
            }

            if (!policyEndorsementQuote.Homeowner.Equals(policyForEndorsementQuote.Homeowner))
            {
                isValid = false;
                propertiesUpdated += "Homeowner|";
            }

            if (!policyEndorsementQuote.HomePhone?.Equals(policyForEndorsementQuote.HomePhone) == true)
            {
                isValid = false;
                propertiesUpdated += "HomePhone|";
            }

            if (!policyEndorsementQuote.WorkPhone?.Equals(policyForEndorsementQuote.WorkPhone) == true)
            {
                isValid = false;
                propertiesUpdated += "WorkPhone|";
            }

            if (!policyEndorsementQuote.MainEmail?.Equals(policyForEndorsementQuote.MainEmail) == true)
            {
                isValid = false;
                propertiesUpdated += "MainEmail|";
            }

            if (!policyEndorsementQuote.AdvancedQuote.Equals(policyForEndorsementQuote.AdvancedQuote))
            {
                isValid = false;
                propertiesUpdated += "AdvancedQuote|";
            }

            var isPolicyDataUpdated = false;
            if (!isValid) isPolicyDataUpdated = true;

            return isPolicyDataUpdated;
        }

        public void FillDriverDAE(PolicyEndorseQuoteDrivers addedDriver,
            DisallowedAgentEndorseDriversDO newDriverDAE, int endorseId, string typeOfChange)
        {
            newDriverDAE.DateLicensed = addedDriver.DateLicensed;
            newDriverDAE.DOB = addedDriver.DOB;
            newDriverDAE.DriverAge = addedDriver.DriverAge;
            newDriverDAE.Youthful = addedDriver.Youthful;
            newDriverDAE.Violations = addedDriver.Violations;
            newDriverDAE.Points = addedDriver.Points;

            newDriverDAE.LicenseNo = addedDriver.LicenseNo;
            newDriverDAE.LicenseSt = addedDriver.LicenseSt;
            newDriverDAE.JobTitle = addedDriver.JobTitle;
            newDriverDAE.Occupation = addedDriver.Occupation;
            newDriverDAE.Married = addedDriver.Married;
            newDriverDAE.Gender = addedDriver.Gender;
            newDriverDAE.RelationToInsured = addedDriver.RelationToInsured;
            newDriverDAE.FirstName = addedDriver.FirstName;
            newDriverDAE.LastName = addedDriver.LastName;
            newDriverDAE.MI = addedDriver.MI;

            newDriverDAE.DisallowedAgentEndorseId = endorseId;
            newDriverDAE.TypeOfChange = typeOfChange;
            newDriverDAE.PolicyNo = addedDriver.PolicyNo;
        }

        public void FillDriverDAE(PolicyDrivers addedDriver, DisallowedAgentEndorseDriversDO newDriverDAE,
            int endorseId, string typeOfChange)
        {
            newDriverDAE.DateLicensed = addedDriver.DateLicensed;
            newDriverDAE.DOB = addedDriver.DOB;
            newDriverDAE.DriverAge = addedDriver.DriverAge;
            newDriverDAE.Youthful = addedDriver.Youthful;
            newDriverDAE.Violations = addedDriver.Violations;
            newDriverDAE.Points = addedDriver.Points;

            newDriverDAE.LicenseNo = addedDriver.LicenseNo;
            newDriverDAE.LicenseSt = addedDriver.LicenseSt;
            newDriverDAE.JobTitle = addedDriver.JobTitle;
            newDriverDAE.Occupation = addedDriver.Occupation;
            newDriverDAE.Married = addedDriver.Married;
            newDriverDAE.Gender = addedDriver.Gender;
            newDriverDAE.RelationToInsured = addedDriver.RelationToInsured;
            newDriverDAE.FirstName = addedDriver.FirstName;
            newDriverDAE.LastName = addedDriver.LastName;
            newDriverDAE.MI = addedDriver.MI;

            newDriverDAE.DisallowedAgentEndorseId = endorseId;
            newDriverDAE.TypeOfChange = typeOfChange;
            newDriverDAE.PolicyNo = addedDriver.PolicyNo;
        }

        public void FillDAEDriverViolation(int newDriverID, PolicyEndorseQuoteDriversViolations driverViolation,
            DisallowedAgentEndorseDriversViolationsDO newDriverVioaltion)
        {
            newDriverVioaltion.PolicyNo = driverViolation.PolicyNo;
            newDriverVioaltion.Chargeable = driverViolation.Chargeable;
            newDriverVioaltion.DisallowedAgentEndorseDriverId = newDriverID;
            newDriverVioaltion.FromAPlus = driverViolation.FromAPlus;
            newDriverVioaltion.FromCV = driverViolation.FromCV;
            newDriverVioaltion.FromMVR = driverViolation.FromMVR;

            newDriverVioaltion.ViolationCode = driverViolation.ViolationCode;
            newDriverVioaltion.ViolationDate = driverViolation.ViolationDate;
            newDriverVioaltion.ViolationDesc = driverViolation.ViolationDesc;
            newDriverVioaltion.ViolationGroup = driverViolation.ViolationGroup;

            newDriverVioaltion.ViolationMonths = driverViolation.ViolationMonths;
            newDriverVioaltion.ViolationPoints = driverViolation.ViolationPoints;
        }

        public void FillDAEDriverViolation(int existingDriverID, PolicyDriversViolationsDO driverViolation,
            DisallowedAgentEndorseDriversViolationsDO existingDriverVioaltion)
        {
            existingDriverVioaltion.PolicyNo = driverViolation.PolicyNo;
            existingDriverVioaltion.Chargeable = driverViolation.Chargeable;
            existingDriverVioaltion.DisallowedAgentEndorseDriverId = existingDriverID;

            existingDriverVioaltion.ViolationCode = driverViolation.ViolationCode;
            existingDriverVioaltion.ViolationDate = driverViolation.ViolationDate;
            existingDriverVioaltion.ViolationDesc = driverViolation.ViolationDesc;
            existingDriverVioaltion.ViolationGroup = driverViolation.ViolationGroup;

            existingDriverVioaltion.ViolationMonths = driverViolation.ViolationMonths;
            existingDriverVioaltion.ViolationPoints = driverViolation.ViolationPoints;
        }

        public List<EndorsementLienHolderModel> SaveLienHolders(FormCollection formCollection, int carId, int carIndex,
            string vin)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var newLhIds = formCollection.AllKeys.Where(x => x.StartsWith("newvehLienholder-")).ToList();
                var deleteLienholderIds = formCollection.AllKeys.Contains("LienHolders.DeletedLienHoldersIds")
                    ? formCollection["LienHolders.DeletedLienHoldersIds"]
                    : null;

                var newLhsAdded = new List<EndorsementLienHolderModel>();
                foreach (var newLhId in newLhIds)
                {
                    var lhJson = HttpUtility.UrlDecode(formCollection[newLhId]);
                    var newLhAdded = new JavaScriptSerializer().Deserialize<EndorsementLienHolderModel>(lhJson);
                    if (!string.IsNullOrEmpty(newLhAdded.Name))
                        newLhsAdded.Add(newLhAdded);
                }

                var reOrderLhIndex = false;
                if (!string.IsNullOrEmpty(deleteLienholderIds))
                {
                    var LhIdsToDelete = deleteLienholderIds.Split(',').Where(x => !string.IsNullOrEmpty(x)).Distinct()
                        .ToList();

                    foreach (var LhIdToDelete in LhIdsToDelete)
                    {
                        reOrderLhIndex = true;
                        DbHelperUtils.DeleteRecord("PolicyEndorseQuoteCarLienHolders", "ID", LhIdToDelete);
                    }
                }

                var getLienHolders = new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo, carIndex),
                    new SortProperty("CarIndex", SortingDirection.Ascending));

                var LhIndex = 0; // getLienHolders.Max(x => x.LienHolderIndex);
                if (reOrderLhIndex && getLienHolders.Count > 0)
                {
                    foreach (var lh in getLienHolders)
                    {
                        LhIndex++;
                        lh.LienHolderIndex = LhIndex;
                    }

                    uow.CommitChanges();
                }
                else
                {
                    LhIndex = getLienHolders.Count > 0 ? getLienHolders.Max(x => x.LienHolderIndex) : 0;
                }

                if (newLhsAdded.Count > 0)
                    foreach (var newLhAdded in newLhsAdded)
                    {
                        LhIndex++;
                        var addLien1 = new PolicyEndorseQuoteCarLienHolders(uow);
                        addLien1.CarIndex = carIndex; //LF 11/7/18 model.CarIndex;
                        addLien1.LienHolderIndex = LhIndex;
                        addLien1.PolicyNo = Sessions.Instance.PolicyNo;
                        addLien1.Name = newLhAdded.Name.Upper();
                        addLien1.Address = newLhAdded.StreetAddress.Upper();
                        addLien1.City = newLhAdded.City.Upper();
                        addLien1.State = newLhAdded.State.Upper();
                        addLien1.Zip = newLhAdded.ZIP;
                        addLien1.DateCreated = DateTime.Now;
                        addLien1.Phone = newLhAdded.Phone;
                        addLien1.isAddInterest = newLhAdded.isAddInterest;
                        addLien1.Save();
                        uow.CommitChanges();
                    }

                var LienHolders = new List<EndorsementLienHolderModel>();

                getLienHolders = new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo,
                        carIndex), new SortProperty("CarIndex", SortingDirection.Ascending));
                var commitChanges = false;
                foreach (var lien in getLienHolders)
                {
                    //uow.BeginTransaction();
                    var LienHolder =
                        uow.FindObject<PolicyEndorseQuoteCarLienHolders>(CriteriaOperator.Parse("ID = ?", lien.ID));
                    var tmpModel = UtilitiesRating.MapLienHolderXpoToModel(LienHolder);
                    tmpModel.VehicleId = carId;

                    LienHolders.Add(tmpModel);
                    LienHolder.VIN = vin;
                    LienHolder.Save();
                    commitChanges = true;
                }

                if (commitChanges)
                    uow.CommitChanges();
                return LienHolders;
            }
        }

        public List<AgentEndorseReasons> GetSelectedAgentEndorsementReasons(string policyNo, int? endorsementId = 0)
        {
            var selectedReasons = new List<AgentEndorseReasons>();
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var reasonChangeSet = uow.ExecuteSproc("spUW_GetSelectedAgentEndorsementReasons",
                    new OperandValue(policyNo), new OperandValue(endorsementId ?? 0));
                if (reasonChangeSet.ResultSet.Any() && reasonChangeSet.ResultSet[0].Rows.Any())
                    foreach (var dRow in reasonChangeSet.ResultSet[0].Rows)
                    {
                        var reason = new AgentEndorseReasons();
                        reason.ReasonCode = dRow.Values[0]?.ToString();
                        reason.IsSpecific = dRow.Values[1] != null ? false : Convert.ToBoolean(dRow.Values[1]);
                        reason.Vin = dRow.Values[2]?.ToString();
                        reason.LicenseNo = dRow.Values[5]?.ToString();
                        reason.DriverFullName = dRow.Values[4]?.ToString();
                        selectedReasons.Add(reason);
                    }
            }

            return selectedReasons;
        }

        public XPCollection<PolicyEndorseQuoteCars> PolicyEndorseQuoteCarsList(PolicyEndorseQuote endorsement)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var cars =
                    new XPCollection<PolicyEndorseQuoteCars>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", endorsement.PolicyNo));
                return cars;
            }
        }

        public string ValidateEndorsement(string policyNo, bool skipCompCollCheck)
        {
            //skipCompCollCheck is always setting to true as we need to allow the agents to do the COMP/COll change now
            skipCompCollCheck = true;

            if (string.IsNullOrEmpty(policyNo)) return "PolicyNo cannot be empty. ";

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var endorsementQuote =
                    uow.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                if (endorsementQuote == null)
                    return
                        $"Endorsement Quote not found for Policy: {policyNo}. Please contact underwriting for assistance.";

                var cars = new XPCollection<PolicyEndorseQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                var drivers = new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                var excludedDriverCount =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND Exclude = 1",
                            Sessions.Instance.PolicyNo)).Count;
                var drivercount =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                        CriteriaOperator.Parse("Exclude != '1' AND PolicyNo = ?",
                            Sessions.Instance.PolicyNo)).Count;
                var carscount = cars.Count;
                if (excludedDriverCount > 4) return "Policy cannot have more than 4 excluded drivers.";

                if (drivercount == 1)
                {
                    if (carscount > 2)
                        return
                            $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).";
                }
                else if (drivercount == 2)
                {
                    if (carscount > 3)
                        return
                            $"There are not enough Drivers ({drivercount}) on the policy to match the number of cars({carscount}).";
                }
                else if (drivercount == 3)
                {
                    if (carscount > 4)
                        return
                            $"There are not enough Drivers({drivercount}) on the policy to match the number of cars({carscount}).";
                }

                if (cars.Count == 0) return "A policy must have at least one vehicle.";

                if (carscount > 5) return "Policy cannot have more than 5 vehicles.";

                // if one or more excluded drivers then max no. of cars allowed should equal to no. of regular drivers
                var isPolicyWithExcludeDrivsAndCarsGtRegDrivs = excludedDriverCount > 0 && carscount > drivercount;
                // if no exluded drivers then max no.of cars allowed should eqaul to one more than no. of drivers
                var isCarsGtRegDrivs = excludedDriverCount == 0 && drivercount + 1 < carscount;
                if (isPolicyWithExcludeDrivsAndCarsGtRegDrivs || isCarsGtRegDrivs)
                {
                    var policyCarsCount = new XPCollection<PolicyCars>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo)).Count;
                    var policyDrivers = new XPCollection<PolicyDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                    var excludedDrivers = policyDrivers.Count(x => x.Exclude);
                    var regularDrivers = policyDrivers.Count(x => !x.Exclude);
                    if (carscount != policyCarsCount || excludedDriverCount != excludedDrivers ||
                        drivercount != regularDrivers)
                    {
                        if (isPolicyWithExcludeDrivsAndCarsGtRegDrivs)
                            return
                                $"Quote with excluded drivers cannot have cars more than number of non-excluded drivers({drivercount}).";
                        return
                            $"Quote cannot have more than {drivercount + 1} cars with {drivercount} drivers on the policy.";
                    }
                }

                if (drivers.Count == 0) return "A policy must have at least one driver.";

                var primaryDriver = uow.FindObject<PolicyEndorseQuoteDrivers>(
                    CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex=1", Sessions.Instance.PolicyNo));
                var secondaryDriver = uow.FindObject<PolicyEndorseQuoteDrivers>(
                    CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex=2", Sessions.Instance.PolicyNo));
                var primaryInsuredFullName =
                    $"{endorsementQuote.Ins1First} {endorsementQuote.Ins1MI} {endorsementQuote.Ins1Last}".Trim();
                var secondaryInsuredFullName =
                    $"{endorsementQuote.Ins2First} {endorsementQuote.Ins2MI} {endorsementQuote.Ins2Last}".Trim();

                if (endorsementQuote.DateBound > new DateTime(2020, 5, 24))
                    if (primaryDriver != null &&
                        (string.Compare(primaryDriver.FirstName.SafeTrim(), endorsementQuote.Ins1First.SafeTrim(),
                             true) != 0 ||
                         string.Compare(primaryDriver.LastName.SafeTrim(), endorsementQuote.Ins1Last.SafeTrim(),
                             true) !=
                         0 || string.Compare(primaryDriver.MI.SafeTrim(), endorsementQuote.Ins1MI.SafeTrim(), true) !=
                         0))
                        return
                            $"Primary Insured name '{primaryInsuredFullName}' does not match with Driver 1 name '{primaryDriver.FirstName} {primaryDriver.MI} {primaryDriver.LastName}'.";

                if (endorsementQuote.Ins2First.SafeTrim() != string.Empty ||
                    endorsementQuote.Ins2Last.SafeTrim() != string.Empty ||
                    endorsementQuote.Ins2MI.SafeTrim() != string.Empty)
                    if (endorsementQuote.DateBound > new DateTime(2020, 5, 24))
                    {
                        if (secondaryDriver == null)
                            return
                                $"Second driver must be added with the name of secondary named insured({secondaryInsuredFullName}).";
                        if (string.Compare(secondaryDriver.FirstName.SafeTrim(), endorsementQuote.Ins2First.SafeTrim(),
                                true) != 0 ||
                            string.Compare(secondaryDriver.LastName.SafeTrim(), endorsementQuote.Ins2Last.SafeTrim(),
                                true) != 0 ||
                            string.Compare(secondaryDriver.MI.SafeTrim(), endorsementQuote.Ins2MI.SafeTrim(), true) !=
                            0)
                            return
                                $"Secondary Insured name '{secondaryInsuredFullName}' does not match with Driver 2 name '{secondaryDriver.FirstName} {secondaryDriver.MI} {secondaryDriver.LastName}' ";
                    }
                // Removed as per new PBI 2582
                //if (secondaryDriver != null && DateTime.Now.AddYears(-19) < secondaryDriver.DOB)
                //{
                //    var existingPolicy = uow.FindObject<Policy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                //    bool isSecondaryInsuredInPolicy = (existingPolicy != null && !string.IsNullOrEmpty(existingPolicy.Ins2First) && !string.IsNullOrEmpty(existingPolicy.Ins2Last));
                //    if (!isSecondaryInsuredInPolicy)
                //    {
                //        return $"Secondary insured '{secondaryInsuredFullName}' is less than 19 years old";
                //    }
                //}

                if (string.IsNullOrEmpty(endorsementQuote.MainEmail?.Trim()))
                    return "Email Address is required";
                if (!AgentUtils.ValidateEmailFormat(endorsementQuote.MainEmail?.Trim()))
                    return $"Insured Email address {endorsementQuote.MainEmail} is invalid.";

                if (endorsementQuote.GarageState != "FL") return "Garage Address Cannot Be Out Of State (FL)";

                if (endorsementQuote.MailState != "FL") return "Mailing Address Cannot Be Out Of State (FL)";

                foreach (var car in cars)
                {
                    if (string.IsNullOrEmpty(car.VIN))
                        return string.Format("Car {0} requires a VIN to be entered", car.CarIndex);
                    if (car.VIN.Length < 17)
                        return string.Format("Car {0} Appears to have an incomplete VIN", car.CarIndex);

                    if (car.BusinessUse) return string.Format("Car {0} is used for business.", car.CarIndex);

                    var carOnPolicy =
                        uow.FindObject<PolicyCars>(
                            CriteriaOperator.Parse("PolicyNo = ? AND VIN = ?", policyNo, car.VIN));
                    //Check if comp/coll was added
                    if (!skipCompCollCheck)
                    {
                        if (carOnPolicy != null)
                        {
                            decimal compDed;
                            decimal collDed;
                            decimal.TryParse(car.CompDed, out compDed);
                            decimal.TryParse(car.CollDed, out collDed);
                            if (compDed > 0 || collDed > 0)
                                if (carOnPolicy.CompDed?.Equals(compDed.ToString()) != true ||
                                    carOnPolicy.CollDed?.Equals(collDed.ToString()) != true)
                                    return string.Format(
                                        "Cannot change physical damage coverages for Car {0} on the agent portal. Please contact UW for assistance.",
                                        car.CarIndex);
                        }
                        else
                        {
                            decimal compDed;
                            decimal collDed;
                            decimal.TryParse(car.CompDed, out compDed);
                            decimal.TryParse(car.CollDed, out collDed);
                            if (compDed > 0 || collDed > 0)
                                return string.Format(
                                    "Cannot add physical damage coverage for Car {0} on the agent portal. Please contact UW for assistance.",
                                    car.CarIndex);
                        }
                    }

                    if (carOnPolicy != null)
                        continue;

                    var vehicleAge = DateTime.Now.Month >= 10
                        ? DateTime.Now.AddYears(1).Year - car.VehYear
                        : DateTime.Now.Year - car.VehYear;
                    if (vehicleAge > 25)
                        return string.Format("Car {0}'s model year is older than 25 years.", car.CarIndex);

                    if (vehicleAge > 15)
                    {
                        decimal compDeduct;
                        decimal collDeduct;
                        decimal.TryParse(car.CompDed, out compDeduct);
                        decimal.TryParse(car.CollDed, out collDeduct);
                        if (compDeduct > 0 || collDeduct > 0)
                            return string.Format(
                                "Cannot add physical damage coverage for Car {0} older than 15 years. Please contact UW for assistance.",
                                string.Concat(car.CarIndex, " ", car.VIN));
                    }

                    if (vehicleAge > 20 && car.HasPhyDam && !string.IsNullOrEmpty(car.CompDed) &&
                        !string.IsNullOrEmpty(car.CollDed))
                        if (carOnPolicy == null)
                            return string.Format(
                                "Car {0}'s model year is older than 20 years and includes physical damage.",
                                car.CarIndex);

                    if (!string.IsNullOrEmpty(car.GrossVehicleWeight) && !string.IsNullOrEmpty(car.ClassCode))
                    {
                        var weight = Convert.ToInt32(car.GrossVehicleWeight);
                        if ((car.ClassCode.Equals("70") || car.ClassCode.Equals("81") ||
                             car.ClassCode.Equals("83")) && weight > 7850)
                            return
                                $"Car {car.CarIndex}'s weight is more than 7,850 lbs. and of class code: {car.ClassCode}";
                        if (weight > 10000)
                            return string.Format("Car {0}'s weight is more than 10,000 lbs.", car.CarIndex);
                    }

                    if (car.CostNew > 45000)
                        return string.Format("Unnacceptable Risk: Car {0} has a MSRP greater than $45,000.00",
                            car.CarIndex);

                    var isAcceptableVeh =
                        UtilitiesVehicles.IsAcceptableVehicle(car.VehMake, car.VehModel1, car.VehModel2);
                    if (!isAcceptableVeh) return string.Format("Car {0} is flagged as unacceptable", car.CarIndex);
                }

                foreach (var driver in drivers)
                {
                    if (driver.DriverIndex > 2 && driver.RelationToInsured == "INSURED")
                        return string.Format(
                            "Driver {0} - {1} - has been listed as a driver with Relationship type as Insured, please select correct relationship type.",
                            driver.DriverIndex, driver.FirstName + " " + driver.LastName);

                    if (string.IsNullOrEmpty(driver.FirstName) || string.IsNullOrEmpty(driver.LastName))
                        return string.Format(
                            "Driver {0} has incomplete information, please fill required information",
                            driver.DriverIndex);

                    if (driver.DriverIndex == 1 && driver.Exclude)
                        return string.Format("driver: The named insured cannot be excluded", driver.DriverIndex);

                    if (!driver.Exclude && string.IsNullOrEmpty(driver.LicenseNo))
                        return string.Format("Driver {0}: The license number is required.", driver.DriverIndex);

                    if (driver.DOB > DateTime.Now) return "Date Of Birth cannot be in the future.";

                    if (!driver.Exclude && !string.IsNullOrEmpty(driver.LicenseSt) && driver.LicenseSt.Equals("FL") &&
                        !string.IsNullOrEmpty(driver.LicenseNo))
                    {
                        var licenseNo = driver.LicenseNo.Replace("-", string.Empty);
                        if (licenseNo.Length != 13)
                            return string.Format(
                                "Driver {0} has an invalid FL driver's license number. Please ensure it is 13 digits long.",
                                driver.DriverIndex);

                        //validate birth year
                        var twoDigitBirthYear = driver.DOB.ToShortDateString()
                            .Substring(driver.DOB.ToShortDateString().Length - 2, 2);
                        var licenseBirthDateDigits = licenseNo?.Substring(7, 2);
                        if (twoDigitBirthYear != licenseBirthDateDigits)
                            return
                                $"Driver {driver.DriverIndex} has an invalid license. A valid FL license should contain the driver two digit birth year.";
                    }

                    if (!driver.Exclude && string.IsNullOrEmpty(driver.LicenseSt)) return "License state is required.";

                    if (!string.IsNullOrEmpty(driver.MI) && !char.IsLetter(driver.MI[0]))
                        return "Middle initial must be a alphabetic";

                    if (!driver.Exclude && driver.DOB > DateTime.Now.Date.AddYears(-16))
                        return "We only accept driver over 16 years of age";

                    if (!driver.Exclude && (string.IsNullOrEmpty(driver.LicenseSt) ||
                                            !driver.InternationalLic &&
                                            !StaticData.USAStates.Keys.Contains(driver.LicenseSt)))
                        return "License state must be one of the 50 states";

                    var violationsForDriver = new XPCollection<PolicyQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?",
                            Sessions.Instance.PolicyNo, driver.DriverIndex));
                    if (violationsForDriver.Any())
                    {
                        var totalPoints = 0;
                        var afAccs = 0;
                        var nfAccs = 0;
                        var inexperienced =
                            DateTime.Now < driver.DateLicensed.AddYears(3);


                        foreach (var violation in violationsForDriver)
                        {
                            totalPoints += violation.ViolationPoints;
                            if (violation.ViolationCode == "1" || violation.ViolationCode == "ACC001")
                                if (violation.ViolationDate >= DateTime.Now.AddMonths(-35))
                                    afAccs += 1;

                            if (violation.ViolationCode == "2" || violation.ViolationCode == "NAF008")
                                if (violation.ViolationDate >= DateTime.Now.AddMonths(-35))
                                    nfAccs += 1;
                        }

                        if (totalPoints >= 8)
                            return string.Format("Driver {2} - {0} {1} - has 8 or more points in violations.",
                                driver.FirstName, driver.LastName, driver.DriverIndex);

                        if (inexperienced && totalPoints >= 5)
                            return string.Format(
                                "Driver {2} - {0} {1} - Inexperienced operator and has 5 or more points in violations.",
                                driver.FirstName, driver.LastName, driver.DriverIndex);

                        if (afAccs >= 2)
                            return string.Format(
                                "Driver {0} - {1} {2} - has 2 or more At Fault Accident violations.",
                                driver.DriverIndex, driver.FirstName, driver.LastName);
                        if (nfAccs + afAccs >= 3)
                            return string.Format(
                                "Driver {0} - {1} {2} - has 3 or more Accident violations.", driver.DriverIndex,
                                driver.FirstName, driver.LastName);


                        if (driver.Gender == "SELECT")
                            return string.Format(
                                "Driver {0} - {1} {2} - needs a correct gender selected", driver.DriverIndex,
                                driver.FirstName, driver.LastName);

                        if (driver.SR22)
                            if (endorsementQuote.BILimit.Upper() == "NONE")
                                return "A Driver has SR22, BI limit must be selected.";
                    }
                }

                var modMarried = drivers.Count(m => m.Married) % 2;
                if (modMarried != 0)
                    return
                        "There are an uneven amount of married drivers on the policy, please add an additional married driver";

                if (string.IsNullOrEmpty(endorsementQuote.GarageCity) ||
                    endorsementQuote.GarageCity.ToUpper().Equals("FIND BY ZIP"))
                    return "Please Select a Valid City.";

                if (string.IsNullOrEmpty(endorsementQuote.GarageZip)) return "Please Enter a Valid ZIP Code.";

                // at risk territory validation
                var rateTerritory =
                    new XPCollection<XpoAARateTerritory>(uow,
                        CriteriaOperator.Parse("ZIP = ?", endorsementQuote.GarageZip)).FirstOrDefault();
                if (rateTerritory == null) return "Invalid Zip code. Contact UW for assistance.";

                //Check if zip code in at risk territory and vlaidate pip claims
                var isAtRiskTerritory = false;
                var currentRateCycle = DbHelperUtils.getMaxRecID("AARatePayPlans", "RateCycle");
                if (currentRateCycle == ConfigSettings.ReadSetting("CurrentRateCycle"))
                {
                    var currentTerritory =
                        uow.FindObject<XpoAARateTerritory>(
                            CriteriaOperator.Parse("ZIP = ? AND RateCycle = ?", endorsementQuote.GarageZip,
                                currentRateCycle));
                    if (currentTerritory != null)
                    {
                        var supportAreaCode = currentTerritory.SupportArea;
                        var uwAreaCode = currentTerritory.UWArea;
                        var uwAreaInfo = uow.FindObject<XpoAARateSupUW>(
                            CriteriaOperator.Parse("RateCycle = ? AND SupportArea = ? AND UWArea = ?",
                                currentRateCycle, supportAreaCode, uwAreaCode));
                        if (uwAreaInfo != null)
                            if (uwAreaInfo.DownPay.Equals(1) && uwAreaInfo.PIPCount.Equals(0))
                                isAtRiskTerritory = true;
                    }
                }

                if (isAtRiskTerritory)
                {
                    var violationsForQuote = new XPCollection<PolicyQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                    if (violationsForQuote.Any(v => v.ViolationDesc.ToUpper().Contains("PIP")))
                        return "No prior PIP claims are allowed in this zip code.";
                }

                return null;
            }
        }

        public PolicyPreferences LanguagePreference()
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var languagePreference =
                    new XPCollection<PolicyPreferences>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND Type = 'Preferred_Language'"))
                        .FirstOrDefault();
                return languagePreference;
            }
        }

        public XPCollection<PolicyEndorseQuoteDrivers> PolicyEndorseQuoteDriversList(PolicyEndorseQuote endorsement)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var drivers =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", endorsement.PolicyNo));
                return drivers;
            }
        }

        public XPCollection<PolicyEndorseQuoteDriversViolations> PolicyEndorseQuoteDriversViolationsList(EndorsementDriverModel driverModel)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var violations =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ? AND FromMVR = 1",
                            Sessions.Instance.PolicyNo, driverModel.DriverIndex));
                return violations;
            }
        }

        public Result<double> GetMinimumEndorsementPaymentDue(DateTime? endorsementDate, double policyDiff, string policyNo, string username)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var fresult = new Result<double>();
                double outPut = 0;
                var result = uow.ExecuteSproc("spUW_MinimumEndorsementPaymentDue", policyNo,
                    endorsementDate, policyDiff, username, outPut);

                if (result != null && result.ResultSet?.Length > 0 &&
                    result.ResultSet[0].Rows.FirstOrDefault() != null &&
                    result.ResultSet[0].Rows[0].Values.Length > 0)
                {
                    var numResult = double.Parse(result.ResultSet[0].Rows[0].Values[0].ToString());
                    fresult.Value = numResult;
                    fresult.Success = true;
                }

                return fresult;
            }
        }

            public void AddLogMessage(string logmessage, bool saveToDb = true)
        {
            logger.Info(logmessage);
            if (saveToDb)
                LogUtils.Log(Sessions.Instance.Username, logmessage);
        }

        public void UpdatePreferredLanguage(string policyNo, string preferredLanguage)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var preferredLanguageRecord =
                    new XPCollection<PolicyPreferences>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND Type ='Preferred_Language'", policyNo))
                        .FirstOrDefault();
                if (!string.IsNullOrEmpty(preferredLanguage))
                {
                    if (preferredLanguageRecord != null && !preferredLanguageRecord.Value.Equals(preferredLanguage))
                    {
                        preferredLanguageRecord.Value = preferredLanguage;
                        preferredLanguageRecord.Save();
                        uow.CommitChanges();
                    }
                    else
                    {
                        var newLanguagePreference = new PolicyPreferences(uow);
                        newLanguagePreference.PolicyNo = policyNo;
                        newLanguagePreference.Type = "Preferred_Language";
                        newLanguagePreference.Value = preferredLanguage;
                        newLanguagePreference.Save();
                        uow.CommitChanges();
                    }
                }
            }
        }

        public List<SelectListItem> GetEndorseReasonList(List<PolicyCars> cars, List<PolicyDrivers> drivers,
            int tryCount = 0, bool is2ndInsExists = false)
        {
            var reasonsList = new List<SelectListItem>();

            try
            {
                var groups = new List<SelectListGroup>();
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    //var reasons = new XPCollection<ReasonsDO>(uow, CriteriaOperator.Parse("CodeType='REASONEND' and Code!='E0014' and Code!='E0015' and Code!='E0018' and Code!='E0019' and Code!='E0024' and Code!='E0027' and Code!='E0030'"));
                    var reasons =
                        uow.ExecuteQuery(
                            "Select ReasonUniqId,Reason,ReasonGroup,IsSpecific from AgentEndorseReasons Where IsActive=1");
                    if (reasons.ResultSet.Any() && reasons.ResultSet[0].Rows.Any())
                    {
                        var reasonsInDb = reasons.ResultSet[0].Rows.Select(reason => new
                        {
                            ReasonId = reason.Values[0].ToString(),
                            ReasonText = reason.Values[1].ToString(),
                            ReasonGroupName = reason.Values[2].ToString(),
                            IsSpecific = bool.Parse(reason.Values[3].ToString())
                        }).ToList();

                        foreach (var reasonInDb in reasonsInDb.Where(x => !x.IsSpecific))
                        {
                            if ((reasonInDb.ReasonId?.ToUpper() == "AG036" ||
                                 reasonInDb.ReasonId?.ToUpper() == "AG038") && !is2ndInsExists)
                                continue;
                            if (reasonInDb.ReasonId?.ToUpper() == "AG037" && is2ndInsExists)
                                continue;

                            var reasonGroup = groups.FirstOrDefault(x => x.Name == reasonInDb.ReasonGroupName);

                            if (reasonGroup == null)
                            {
                                reasonGroup = new SelectListGroup { Name = reasonInDb.ReasonGroupName };
                                groups.Add(reasonGroup);
                            }

                            reasonsList.Add(new SelectListItem
                            {
                                Value = reasonInDb.ReasonId,
                                Text = reasonInDb.ReasonText,
                                Group = reasonGroup
                            });
                        }

                        foreach (var reasonInDb in reasonsInDb.Where(x => x.IsSpecific))
                        {
                            string reasonGroupName = null;
                            string reasonValue = null;
                            if (reasonInDb.ReasonGroupName?.ToLower() == "vehicle")
                            {
                                var carIndex = 0;
                                foreach (var car in cars.OrderBy(x => x.CarIndex))
                                {
                                    carIndex++;
                                    if ((reasonInDb.ReasonId == "AG006" || reasonInDb.ReasonId == "AG009") &&
                                        !car.HasPhyDam)
                                        continue;
                                    if (reasonInDb.ReasonId == "AG002" && car.HasPhyDam)
                                        continue;

                                    reasonGroupName = $"Vehicle {carIndex} ({car.VIN?.ToUpper()})";
                                    var reasonGroup = groups.FirstOrDefault(x => x.Name == reasonGroupName);

                                    if (reasonGroup == null)
                                    {
                                        reasonGroup = new SelectListGroup { Name = reasonGroupName };
                                        groups.Add(reasonGroup);
                                    }

                                    reasonsList.Add(new SelectListItem
                                    {
                                        Value = reasonInDb.ReasonId + "-" + car.VIN,
                                        Text = reasonInDb.ReasonText,
                                        Group = reasonGroup
                                    });
                                }
                            }
                            else if (reasonInDb.ReasonGroupName?.ToLower() == "driver")
                            {
                                var driverIndex = 1;
                                foreach (var driver in drivers.OrderBy(x => x.DriverIndex))
                                {
                                    var drivName = $"{driver.FirstName} {driver.MI} {driver.LastName}"?.ToUpper();
                                    reasonGroupName = $"Driver {driverIndex} ({drivName})";
                                    var reasonGroup = groups.FirstOrDefault(x => x.Name == reasonGroupName);

                                    if (reasonGroup == null)
                                    {
                                        reasonGroup = new SelectListGroup { Name = reasonGroupName };
                                        groups.Add(reasonGroup);
                                    }

                                    reasonsList.Add(new SelectListItem
                                    {
                                        //Value = reasonInDb.ReasonId + "-" + (string.IsNullOrEmpty(driver.LicenseNo) ? "PolicyDriverOID-"+ driver.OID.ToString() : driver.LicenseNo),
                                        Value = reasonInDb.ReasonId + "-PolicyDriverOID-" + driver.OID,
                                        Text = reasonInDb.ReasonText,
                                        Group = reasonGroup
                                    });
                                    driverIndex++;
                                }
                            }
                        }
                    }
                }

                return reasonsList;
            }
            catch (Exception ex)
            {
                ++tryCount;
                if (tryCount <= 3)
                    reasonsList = GetEndorseReasonList(cars, drivers, tryCount);
            }

            return reasonsList;
        }

        public class EndorsmentReasonsResult
        {
            public AllAgentEndorseReasons AllReasons { get; set; }
            public string UnSelectedEndorseReasons { get; internal set; }
            public string SelectedEndorseReasons { get; internal set; }
            public string MissingReasonsText { get; set; }
        }
        
        public EndorsmentReasonsResult GetMissingEndorseReasons(string policyNo, string endorsementId)
        {
            EndorsmentReasonsResult result = new EndorsmentReasonsResult();
            var allReasons = new AllAgentEndorseReasons();
            var selectedReasons = new List<AgentEndorseReasons>();
            var missingReasonsText = string.Empty;
            var missingReasons = new List<AgentEndorseReasons>();
            var unSelectedReasonText = string.Empty;
            var allSeletedReasonText = string.Empty;

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var reasonChangeSet = uow.ExecuteSproc("spUW_VerifyAgentEndorsementReasons_UsingMappings",
                    new OperandValue(policyNo), new OperandValue(endorsementId));
                if (reasonChangeSet.ResultSet.Any() && reasonChangeSet.ResultSet[0].Rows.Any())
                    foreach (var item in reasonChangeSet.ResultSet[0].Rows)
                        if (!string.IsNullOrEmpty(item.Values[0]?.ToString()))
                            missingReasonsText += item.Values[0] + " , ";

                if (reasonChangeSet.ResultSet.Count() >= 4 && reasonChangeSet.ResultSet[3].Rows.Any())
                    foreach (var item in reasonChangeSet.ResultSet[3].Rows)
                    {
                        var isMissing = Convert.ToBoolean(item.Values[4]);
                        var rsCode = new AgentEndorseReasons
                        {
                            Reason = item.Values[0]?.ToString(),
                            ReasonCode = item.Values[1]?.ToString(),
                            Vin = item.Values[2]?.ToString(),
                            LicenseNo = item.Values[3]?.ToString()
                        };
                        selectedReasons.Add(rsCode);
                        if (isMissing)
                            missingReasons.Add(rsCode);
                    }

                allReasons.Missing = missingReasons;
                //TempData["EndorsementMissingReasonList"] = missingReasons;

                var unselectedReasons = new List<AgentEndorseReasons>();
                if (reasonChangeSet.ResultSet.Count() >= 5 && reasonChangeSet.ResultSet[4].Rows.Any())
                    foreach (var item in reasonChangeSet.ResultSet[4].Rows)
                        unselectedReasons.Add(new AgentEndorseReasons
                        {
                            //Reason = item.Values[0]?.ToString(),
                            ReasonCode = item.Values[0]?.ToString(),
                            Vin = item.Values[1]?.ToString(),
                            LicenseNo = item.Values[2]?.ToString()
                        });

                allReasons.UnSelected = unselectedReasons;
                allReasons.Selected = selectedReasons;

                result.AllReasons = allReasons;
                

                if (reasonChangeSet.ResultSet.Count() > 1 && reasonChangeSet.ResultSet[1].Rows.Any())
                {
                    foreach (var item in reasonChangeSet.ResultSet[1].Rows)
                        if (!string.IsNullOrEmpty(item.Values[0]?.ToString()))
                            unSelectedReasonText += item.Values[0] + " , ";
                    if (reasonChangeSet.ResultSet.Count() > 2 && reasonChangeSet.ResultSet[2].Rows.Any())
                        foreach (var allReason in reasonChangeSet.ResultSet[2].Rows)
                            if (!string.IsNullOrEmpty(allReason.Values[0]?.ToString()))
                                allSeletedReasonText += allReason.Values[0] + " , ";
                }
            }

            if (missingReasonsText.Length > 0)
                missingReasonsText = missingReasonsText.TrimEnd().TrimEnd(',');
            if (unSelectedReasonText.Length > 0)
                unSelectedReasonText = unSelectedReasonText.TrimEnd().TrimEnd(',');
            if (allSeletedReasonText.Length > 0)
                allSeletedReasonText = allSeletedReasonText.TrimEnd().TrimEnd(',');
            result.UnSelectedEndorseReasons = unSelectedReasonText;
            result.SelectedEndorseReasons = allSeletedReasonText;
            result.MissingReasonsText = missingReasonsText;
            return result;
        }

        public string CleanString(string cleanString = null)
        {
            var value = "";

            if (!string.IsNullOrEmpty(cleanString))
                value = Regex.Replace(cleanString, @"\W", "", RegexOptions.IgnoreCase);

            return value;
        }

        public int GetDisallowedEndorsementChanges(string PolicyNo, DisallowedEndorsementModel model,
            double premiumChange, DateTime? endorsementEffDate)
        {
            var isValid = true;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    var policyForEndorsementQuote =
                        uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                    var policyEndorsementQuote =
                        uow.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                    var propertiesUpdated = string.Empty;
                    if (policyForEndorsementQuote != null)
                    {
                        var endorsementCreatedDate = uow.FindObject<PolicyEndorseQuoteIdsCreatedDO>(
                            CriteriaOperator.Parse("PolicyEndorseQuoteId= ?", policyEndorsementQuote.OID));

                        var isPolicyDataUpdated = CheckPolicyInfoUpdated(ref isValid, policyForEndorsementQuote,
                            policyEndorsementQuote, ref propertiesUpdated); //Check for Deleted drivers
                        //bool isDriversDeleted = false;
                        var driversForEndorsement =
                            new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        var driversForPolicy =
                            new XPCollection<PolicyDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        //if (!driversForEndorsement.Any() || driversForEndorsement?.Count < driversForPolicy?.Count)
                        //{
                        //    isValid = false;
                        //    isDriversDeleted = true;
                        //    propertiesUpdated += "DeleteDriver|";
                        //}

                        //Check for deleted vehicles
                        //bool isCarsDeleted = false;
                        var carsForEndorsement =
                            new XPCollection<PolicyEndorseQuoteCars>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        var carsForPolicy =
                            new XPCollection<PolicyCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        //if (!carsForEndorsement.Any() || carsForEndorsement?.Count < carsForPolicy?.Count)
                        //{
                        //    isValid = false;
                        //    isCarsDeleted = true;
                        //    propertiesUpdated += "DeleteCar|";
                        //}

                        //Check for new or updated vechiles with phy dam / comp/coll
                        //var newCarsWithCompColl = new List<PolicyEndorseQuoteCars>();
                        var newCars = new List<PolicyEndorseQuoteCars>();
                        var exisitngCarsUpdated = new List<PolicyEndorseQuoteCars>();
                        foreach (var endorsementCar in carsForEndorsement)
                        {
                            var isCarValid = true;
                            var carOnPolicy =
                                carsForPolicy.FirstOrDefault(c => c.VIN?.ToLower() == endorsementCar.VIN?.ToLower());
                            if (carOnPolicy != null)
                            {
                                isCarValid = CheckUpdateCar(endorsementCar, isCarValid, carOnPolicy);
                                if (!isCarValid) exisitngCarsUpdated.Add(endorsementCar);
                            }
                            else
                            {
                                newCars.Add(endorsementCar);
                            }
                        }

                        var newDrivers = new List<PolicyEndorseQuoteDrivers>();
                        var existingDrivers = new List<PolicyEndorseQuoteDrivers>();
                        var violationChangeExistingDrivs = new List<PolicyEndorseQuoteDrivers>();
                        var DelDrivViolsForExistingDriv = new List<PolicyDriversViolationsDO>();
                        var addDrivViolsForExistingDriv = new List<PolicyEndorseQuoteDriversViolations>();
                        foreach (var endorsementDriver in driversForEndorsement)
                        {
                            var driverOnPolicy = driversForPolicy.FirstOrDefault(e =>
                                e.LicenseNo?.ToLower()?.Trim() == endorsementDriver.LicenseNo?.ToLower()?.Trim());
                            if (driverOnPolicy != null)
                            {
                                if (driverOnPolicy.FirstName?.ToLower() != endorsementDriver.FirstName?.ToLower() ||
                                    driverOnPolicy.LastName?.ToLower() != endorsementDriver.LastName?.ToLower() ||
                                    driverOnPolicy.MI?.ToLower() != endorsementDriver.MI?.ToLower() ||
                                    driverOnPolicy.DOB != null && endorsementDriver.DOB != null &&
                                    driverOnPolicy.DOB.ToShortDateString() !=
                                    endorsementDriver.DOB.ToShortDateString() ||
                                    driverOnPolicy.LicenseNo?.ToLower() != endorsementDriver.LicenseNo?.ToLower() ||
                                    driverOnPolicy.Gender?.ToLower() != endorsementDriver.Gender?.ToLower() ||
                                    driverOnPolicy.Married != endorsementDriver.Married ||
                                    driverOnPolicy.RelationToInsured?.ToLower() !=
                                    endorsementDriver.RelationToInsured?.ToLower() ||
                                    driverOnPolicy.Suffix?.ToLower() != endorsementDriver.Suffix?.ToLower() ||
                                    driverOnPolicy.Exclude != endorsementDriver.Exclude ||
                                    driverOnPolicy.Occupation?.ToLower() != endorsementDriver.Occupation?.ToLower() ||
                                    driverOnPolicy.Company?.ToLower() != endorsementDriver.Company?.ToLower() ||
                                    driverOnPolicy.SR22 != endorsementDriver.SR22 ||
                                    driverOnPolicy.SR22CaseNo?.ToLower() != endorsementDriver.SR22CaseNo?.ToLower() ||
                                    driverOnPolicy.InternationalLic != endorsementDriver.InternationalLic ||
                                    driverOnPolicy.SeniorDefDrvDisc != endorsementDriver.SeniorDefDrvDisc ||
                                    driverOnPolicy.SeniorDefDrvDate.Date != endorsementDriver.SeniorDefDrvDate.Date
                                )
                                    existingDrivers.Add(endorsementDriver);

                                var voilationChanged = false;
                                var dvOnPolicy = new XPCollection<PolicyDriversViolationsDO>(uow,
                                    CriteriaOperator.Parse("PolicyNo= ? AND DriverIndex = ?", PolicyNo,
                                        driverOnPolicy.DriverIndex));
                                if (dvOnPolicy.Any())
                                {
                                    var dvOnEndorsement = new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                                        CriteriaOperator.Parse("PolicyNo= ? AND DriverIndex = ?", PolicyNo,
                                            endorsementDriver.DriverIndex));

                                    foreach (var driverViolation in dvOnPolicy)
                                        if (!dvOnEndorsement.Any(x =>
                                            x.ViolationDate.Date == driverViolation.ViolationDate.Date &&
                                            x.ViolationCode?.ToLower() == driverViolation.ViolationCode?.ToLower()))
                                        {
                                            DelDrivViolsForExistingDriv.Add(driverViolation);
                                            voilationChanged = true;
                                        }

                                    foreach (var dv in dvOnEndorsement)
                                        if (!dvOnPolicy.Any(x => x.ViolationDate.Date == dv.ViolationDate.Date &&
                                                                 x.ViolationCode?.ToLower() ==
                                                                 dv.ViolationCode?.ToLower()))
                                        {
                                            addDrivViolsForExistingDriv.Add(dv);
                                            voilationChanged = true;
                                        }

                                    if (voilationChanged)
                                        violationChangeExistingDrivs.Add(endorsementDriver);
                                }
                            }
                            else
                            {
                                newDrivers.Add(endorsementDriver);
                            }
                        }

                        var deletedDrivers = driversForPolicy.Where(x =>
                            !driversForEndorsement.Any(e =>
                                e.LicenseNo?.ToLower()?.Trim() == x.LicenseNo?.ToLower()?.Trim()));

                        var addedDrivers = driversForEndorsement.Where(x =>
                            !driversForPolicy.Any(e =>
                                e.LicenseNo?.ToLower()?.Trim() == x.LicenseNo?.ToLower()?.Trim()));

                        var deletedCars = carsForPolicy.Where(x =>
                            !carsForEndorsement.Any(e => e.VIN?.ToLower()?.Trim() == x.VIN?.ToLower()?.Trim()));

                        var addedCars = carsForEndorsement.Where(x =>
                            !carsForPolicy.Any(e => e.VIN?.ToLower()?.Trim() == x.VIN?.ToLower()?.Trim()));


                        if (isPolicyDataUpdated || deletedCars.Any() || addedCars.Any() || exisitngCarsUpdated.Any() ||
                            addedDrivers.Any() || deletedDrivers.Any() || existingDrivers.Any() ||
                            violationChangeExistingDrivs.Any())
                        {
                            var disallowedAgentEndorseQuotes = new XPCollection<DisallowedAgentEndorseQuoteDO>(uow,
                                CriteriaOperator.Parse("PolicyNo = ? AND IsActive=1", PolicyNo));
                            foreach (var disallowedQuote in disallowedAgentEndorseQuotes)
                                disallowedQuote.IsActive = false;

                            var disallowedEndorseQuote = new DisallowedAgentEndorseQuoteDO(uow);
                            disallowedEndorseQuote.AgentCode = policyForEndorsementQuote.AgentCode;
                            disallowedEndorseQuote.EffDate = policyForEndorsementQuote.EffDate;
                            disallowedEndorseQuote.ExpDate = policyForEndorsementQuote.ExpDate;
                            disallowedEndorseQuote.AdditionalPremiumCollected = premiumChange;
                            disallowedEndorseQuote.PolicyNo = policyForEndorsementQuote.PolicyNo;
                            disallowedEndorseQuote.Ins1First = policyEndorsementQuote.Ins1First;
                            disallowedEndorseQuote.Ins1Last = policyEndorsementQuote.Ins1Last;
                            disallowedEndorseQuote.Ins1MI = policyEndorsementQuote.Ins1MI;
                            disallowedEndorseQuote.Ins1Suffix = policyEndorsementQuote.Ins1Suffix;
                            disallowedEndorseQuote.MainEmail = policyEndorsementQuote.MainEmail;
                            disallowedEndorseQuote.MainEmailOld = policyForEndorsementQuote.MainEmail;
                            disallowedEndorseQuote.Ins2FullName = policyEndorsementQuote.Ins2First + " " +
                                                                  policyEndorsementQuote.Ins2MI + " " +
                                                                  policyEndorsementQuote.Ins2Last;

                            disallowedEndorseQuote.MailStreet = policyEndorsementQuote.MailStreet;
                            disallowedEndorseQuote.MailZip = policyEndorsementQuote.MailZip;
                            disallowedEndorseQuote.MailCity = policyEndorsementQuote.MailCity;
                            disallowedEndorseQuote.MailState = policyEndorsementQuote.MailState;

                            disallowedEndorseQuote.IsActive = true;


                            disallowedEndorseQuote.GarageStreet = policyEndorsementQuote.GarageStreet;
                            disallowedEndorseQuote.GarageZip = policyEndorsementQuote.GarageZip;
                            disallowedEndorseQuote.GarageCity = policyEndorsementQuote.GarageCity;
                            disallowedEndorseQuote.GarageState = policyEndorsementQuote.GarageState;
                            disallowedEndorseQuote.GarageCounty = policyEndorsementQuote.GarageState;
                            disallowedEndorseQuote.MailGarageSame = policyEndorsementQuote.MailGarageSame;


                            if (policyEndorsementQuote.Homeowner != policyForEndorsementQuote.Homeowner)
                                disallowedEndorseQuote.Homeowner = policyEndorsementQuote.Homeowner;
                            if (policyEndorsementQuote.Paperless != policyForEndorsementQuote.Paperless)
                                disallowedEndorseQuote.Paperless = policyEndorsementQuote.Paperless;
                            if (policyEndorsementQuote.HasPPO != policyForEndorsementQuote.HasPPO)
                                disallowedEndorseQuote.HasPPO = policyEndorsementQuote.HasPPO;
                            if (policyEndorsementQuote.NIO != policyForEndorsementQuote.NIO)
                                disallowedEndorseQuote.NIO = policyEndorsementQuote.NIO;
                            if (policyEndorsementQuote.NIRR != policyForEndorsementQuote.NIRR)
                                disallowedEndorseQuote.NIRR = policyEndorsementQuote.NIRR;
                            if (policyEndorsementQuote.TransDisc != policyForEndorsementQuote.TransDisc)
                                disallowedEndorseQuote.TransDisc = policyEndorsementQuote.TransDisc;
                            if (policyEndorsementQuote.AdvancedQuote != policyForEndorsementQuote.AdvancedQuote)
                                disallowedEndorseQuote.AdvancedQuote = policyEndorsementQuote.AdvancedQuote;

                            if (isPolicyDataUpdated)
                            {
                                disallowedEndorseQuote.AdvancedQuote = policyEndorsementQuote.AdvancedQuote;
                                disallowedEndorseQuote.PIPDed = policyEndorsementQuote.PIPDed;
                                disallowedEndorseQuote.PDLimit = policyEndorsementQuote.PDLimit;
                                disallowedEndorseQuote.WorkLoss = policyEndorsementQuote.WorkLoss;
                                disallowedEndorseQuote.PIPDedOld = policyForEndorsementQuote.PIPDed;
                                disallowedEndorseQuote.PDLimitOld = policyForEndorsementQuote.PDLimit;
                                disallowedEndorseQuote.WorkLossOld = policyForEndorsementQuote.WorkLoss;
                            }

                            //disallowedEndorseQuote.IsActive = true;
                            disallowedEndorseQuote.CreatedDate = DateTime.Now;

                            disallowedEndorseQuote.ExpDate = policyForEndorsementQuote.ExpDate;
                            disallowedEndorseQuote.EffDate = policyForEndorsementQuote.EffDate;
                            disallowedEndorseQuote.PolicyInceptionDate = policyForEndorsementQuote.DateBound;

                            model.AgencyCode = policyForEndorsementQuote.AgentCode;
                            var agent = uow.FindObject<AgentsDO>(CriteriaOperator.Parse("AgentCode = ?",
                                policyForEndorsementQuote.AgentCode));
                            if (agent != null)
                            {
                                model.AgencyName = agent.AgencyName;
                                model.AgencyAddress = agent.MailAddress;
                                model.AgentInfo = agent;
                            }

                            model.InsuredFirstName = policyEndorsementQuote.Ins1First;
                            model.InsuredLastName = policyEndorsementQuote.Ins1Last;
                            model.InsuredMI = policyEndorsementQuote.Ins1MI;
                            model.Insured2FullName = disallowedEndorseQuote.Ins2FullName;
                            model.AdditionalPremiumCollected = premiumChange;

                            model.InsuredAddress = string.Format("{0}, {1}, {2}, {3}",
                                policyEndorsementQuote.MailStreet, policyEndorsementQuote.MailCity,
                                policyEndorsementQuote.MailState, policyEndorsementQuote.MailZip);
                            model.IsNewAddress =
                                policyEndorsementQuote.MailStreet != policyForEndorsementQuote.MailStreet ||
                                policyEndorsementQuote.MailCity != policyForEndorsementQuote.MailCity ||
                                policyEndorsementQuote.MailState != policyForEndorsementQuote.MailState ||
                                policyEndorsementQuote.MailZip != policyForEndorsementQuote.MailZip;
                            model.PolicyNumber = policyForEndorsementQuote.PolicyNo;
                            model.PolicyExpirationDate = policyForEndorsementQuote.ExpDate;
                            model.EffectiveTime = DateTime.Now.ToShortDateString() + " " +
                                                  DateTime.Now.ToShortTimeString();
                            model.PolicyInceptionDate = policyForEndorsementQuote.DateBound;

                            if (endorsementEffDate == null || endorsementEffDate == DateTime.MinValue)
                            {
                                var getEndorsementInfo =
                                    new XPCollection<AuditPolicyHistory>(uow,
                                        CriteriaOperator.Parse("PolicyNo = ? AND Action = 'ENDORSE'", PolicyNo));
                                model.EndorsementEffectiveDate = getEndorsementInfo.Count() > 0
                                    ? getEndorsementInfo.Max(m => m.EffDate)
                                    : policyEndorsementQuote.EffDate;
                            }
                            else
                            {
                                model.EndorsementEffectiveDate = endorsementEffDate.Value;
                            }

                            disallowedEndorseQuote.EndorsementEffDate = model.EndorsementEffectiveDate;

                            disallowedEndorseQuote.Save();
                            uow.CommitChanges();

                            if (deletedDrivers.Any())
                                foreach (var deletedDriver in deletedDrivers)
                                {
                                    var deletedDriverDAE = new DisallowedAgentEndorseDriversDO(uow);
                                    FillDriverDAE(deletedDriver, deletedDriverDAE, disallowedEndorseQuote.OID,
                                        "Delete");
                                    deletedDriverDAE.Save();
                                    model.Drivers.Add(deletedDriverDAE);
                                }

                            if (existingDrivers.Any())
                                foreach (var existingDriver in existingDrivers)
                                {
                                    var updatedDriverDAE = new DisallowedAgentEndorseDriversDO(uow);
                                    FillDriverDAE(existingDriver, updatedDriverDAE, disallowedEndorseQuote.OID,
                                        "Modify");
                                    updatedDriverDAE.Save();
                                    model.Drivers.Add(updatedDriverDAE);
                                    var voilUpdated =
                                        violationChangeExistingDrivs.FirstOrDefault(x => x.OID == existingDriver.OID);
                                    if (voilUpdated != null)
                                    {
                                        uow.CommitChanges();
                                        foreach (var driverViolation in DelDrivViolsForExistingDriv.Where(x =>
                                            x.DriverIndex == existingDriver.DriverIndex))
                                        {
                                            var newDriverVioaltion = new DisallowedAgentEndorseDriversViolationsDO(uow);
                                            FillDAEDriverViolation(updatedDriverDAE.OID, driverViolation,
                                                newDriverVioaltion);
                                            newDriverVioaltion.TypeOfChange = "Delete";
                                            newDriverVioaltion.Save();
                                            //uow.CommitChanges();
                                            model.DriverViolations.Add(newDriverVioaltion);
                                        }

                                        foreach (var driverViolation in addDrivViolsForExistingDriv.Where(x =>
                                            x.DriverIndex == existingDriver.DriverIndex))
                                        {
                                            var newDriverVioaltion = new DisallowedAgentEndorseDriversViolationsDO(uow);
                                            FillDAEDriverViolation(updatedDriverDAE.OID, driverViolation,
                                                newDriverVioaltion);
                                            newDriverVioaltion.TypeOfChange = "Add";
                                            newDriverVioaltion.Save();
                                            //uow.CommitChanges();
                                            model.DriverViolations.Add(newDriverVioaltion);
                                        }
                                    }
                                }

                            if (DelDrivViolsForExistingDriv.Any() || addDrivViolsForExistingDriv.Any())
                            {
                                var onlyViolationChangedDrivs =
                                    violationChangeExistingDrivs.Where(x => !existingDrivers.Any(y => y.OID == x.OID));
                                foreach (var onlyViolationChangedDriv in onlyViolationChangedDrivs)
                                {
                                    var updatedDriverDAE = new DisallowedAgentEndorseDriversDO(uow);
                                    FillDriverDAE(onlyViolationChangedDriv, updatedDriverDAE,
                                        disallowedEndorseQuote.OID, "Modify Violations");
                                    updatedDriverDAE.Save();
                                    model.Drivers.Add(updatedDriverDAE);
                                    var voilUpdated =
                                        violationChangeExistingDrivs.FirstOrDefault(x =>
                                            x.OID == onlyViolationChangedDriv.OID);
                                    if (voilUpdated != null)
                                    {
                                        uow.CommitChanges();
                                        foreach (var driverViolation in DelDrivViolsForExistingDriv.Where(x =>
                                            x.DriverIndex == onlyViolationChangedDriv.DriverIndex))
                                        {
                                            var newDriverVioaltion = new DisallowedAgentEndorseDriversViolationsDO(uow);
                                            FillDAEDriverViolation(updatedDriverDAE.OID, driverViolation,
                                                newDriverVioaltion);
                                            newDriverVioaltion.TypeOfChange = "Delete";
                                            newDriverVioaltion.Save();
                                            model.DriverViolations.Add(newDriverVioaltion);
                                        }

                                        foreach (var driverViolation in addDrivViolsForExistingDriv.Where(x =>
                                            x.DriverIndex == onlyViolationChangedDriv.DriverIndex))
                                        {
                                            var newDriverVioaltion = new DisallowedAgentEndorseDriversViolationsDO(uow);
                                            FillDAEDriverViolation(updatedDriverDAE.OID, driverViolation,
                                                newDriverVioaltion);
                                            newDriverVioaltion.TypeOfChange = "Add";
                                            newDriverVioaltion.Save();
                                            model.DriverViolations.Add(newDriverVioaltion);
                                        }
                                    }
                                }
                            }


                            if (addedDrivers.Any())
                                foreach (var addedDriver in addedDrivers)
                                {
                                    var newDriverDAE = new DisallowedAgentEndorseDriversDO(uow);
                                    FillDriverDAE(addedDriver, newDriverDAE, disallowedEndorseQuote.OID, "Add");
                                    newDriverDAE.Save();
                                    model.Drivers.Add(newDriverDAE);
                                    var driverViolations = new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                                        CriteriaOperator.Parse("PolicyNo= ? AND DriverIndex = ?", PolicyNo,
                                            addedDriver.DriverIndex));
                                    if (driverViolations.Any())
                                    {
                                        uow.CommitChanges();
                                        var newDriverID = newDriverDAE.OID;
                                        foreach (var driverViolation in driverViolations)
                                        {
                                            var newDriverVioaltion = new DisallowedAgentEndorseDriversViolationsDO(uow);
                                            FillDAEDriverViolation(newDriverID, driverViolation, newDriverVioaltion);
                                            newDriverVioaltion.TypeOfChange = "Add";
                                            newDriverVioaltion.Save();
                                            //uow.CommitChanges();
                                            model.DriverViolations.Add(newDriverVioaltion);
                                        }
                                    }
                                }

                            foreach (var deletedCar in deletedCars)
                            {
                                var newCarDAE = new DisallowedAgentEndorseCarsDO(uow);
                                FillData(newCarDAE, deletedCar);
                                newCarDAE.TypeOfChange = "Delete";
                                newCarDAE.DisallowedAgentEndorseId = disallowedEndorseQuote.OID;
                                newCarDAE.Save();
                                model.Cars.Add(newCarDAE);
                            }

                            if (newCars.Any())
                                foreach (var addedCar in newCars)
                                {
                                    var newCarDAE = new DisallowedAgentEndorseCarsDO(uow);
                                    FillData(newCarDAE, addedCar);
                                    newCarDAE.TypeOfChange = "Add";
                                    newCarDAE.DisallowedAgentEndorseId = disallowedEndorseQuote.OID;
                                    newCarDAE.Save();
                                    model.Cars.Add(newCarDAE);
                                }


                            if (exisitngCarsUpdated.Any())
                                foreach (var newCarWithCompColl in exisitngCarsUpdated)
                                {
                                    var newCarDAE = new DisallowedAgentEndorseCarsDO(uow);
                                    FillData(newCarDAE, newCarWithCompColl);
                                    newCarDAE.TypeOfChange = "Modify";
                                    newCarDAE.DisallowedAgentEndorseId = disallowedEndorseQuote.OID;
                                    newCarDAE.Save();
                                    model.Cars.Add(newCarDAE);
                                }

                            if (endorsementCreatedDate != null &&
                                endorsementCreatedDate.CreateDateTime != DateTime.MinValue)
                            {
                                var carLienHolders = new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow,
                                    CriteriaOperator.Parse("PolicyNo= ? AND [DateCreated]> ?", PolicyNo,
                                        endorsementCreatedDate.CreateDateTime));
                                foreach (var carLienholder in carLienHolders)
                                {
                                    var newLossPayee = new DisallowedAgentEndorseLossPayeeDO(uow);
                                    newLossPayee.NameOfLossPayee = carLienholder.Name;
                                    newLossPayee.LastSixOfVin = carLienholder.VIN;
                                    newLossPayee.AddressOfLossPayee = carLienholder.Address;
                                    newLossPayee.CreatedDate = DateTime.Now;
                                    newLossPayee.DisallowedAgentEndorseId = disallowedEndorseQuote.OID;
                                    newLossPayee.PolicyNo = carLienholder.PolicyNo;
                                    newLossPayee.TypeOfChange = "Add";
                                    newLossPayee.Save();
                                    model.LossPayee.Add(newLossPayee);
                                }
                            }


                            model.DisallowedEndorseQuote = disallowedEndorseQuote;
                            model.SetEndorsementChanges();
                            disallowedEndorseQuote.IsAddressChange = model.IsAddressChange;
                            disallowedEndorseQuote.IsCoverageChange = model.IsCoverageChange;
                            disallowedEndorseQuote.IsLossPayeeChange = model.IsLossPayeeChange;
                            disallowedEndorseQuote.IsVehiclesChange = model.IsVehiclesChange;
                            disallowedEndorseQuote.IsDriversChange = model.IsDriversChange;

                            disallowedEndorseQuote.Save();
                            logger.Info(
                                $"DisallowedEndorsement: Updated Fields- {propertiesUpdated}");

                            uow.CommitChanges();
                            return disallowedEndorseQuote.OID;
                        }
                        else
                        {
                            //no changes found
                            var disallowedAgentEndorseQuotes = new XPCollection<DisallowedAgentEndorseQuoteDO>(uow,
                                CriteriaOperator.Parse("PolicyNo = ? AND IsActive=1", PolicyNo));
                            foreach (var disallowedQuote in disallowedAgentEndorseQuotes)
                                disallowedQuote.IsActive = false;
                            uow.CommitChanges();
                            return int.MaxValue;
                        }
                    }

                    logger.Error(
                        $"Failed DisallowedEndorsement: EndorsementQuote Not Found {PolicyNo}");
                    isValid = false;
                }
                catch (Exception EX)
                {
                    isValid = false;
                }
            }

            logger.Error($"DisallowedEndorsement: isvalid -{isValid}");

            return 0;
        }

        public void FillData(DisallowedAgentEndorseCarsDO endorseCarDA, PolicyEndorseQuoteCars endorseCar)
        {
            endorseCarDA.Artisan = endorseCar.Artisan;
            //endorseCarDA.IsAtOrHD = newCarWithCompColl.has;
            endorseCarDA.CompDed = endorseCar.CompDed;
            endorseCarDA.CollDed = endorseCar.CollDed;
            endorseCarDA.HasPhyDam = endorseCar.HasPhyDam;
            endorseCarDA.ConvTT = endorseCar.ConvTT;
            endorseCarDA.IsTConvertible = endorseCar.ConvTT;
            endorseCarDA.DisallowedAgentEndorseId = endorseCar.OID;
            endorseCarDA.VehYear = endorseCar.VehYear;
            endorseCarDA.VehMake = endorseCar.VehMake;
            endorseCarDA.VehModel1 = endorseCar.VehModel1;
            endorseCarDA.VehModel2 = endorseCar.VehModel2;
            endorseCarDA.VIN = endorseCar.VIN;
            endorseCarDA.PolicyNo = endorseCar.PolicyNo;
            endorseCarDA.MilesToWork = endorseCar.MilesToWork;
            endorseCarDA.IsAntiLockBrake = endorseCar.ABS == 1;
            endorseCarDA.HasAirBag = !string.IsNullOrEmpty(endorseCar.ARB.ToString());
            endorseCarDA.IsAtOrHD = !string.IsNullOrEmpty(endorseCar.ATD);
        }

        public void FillData(DisallowedAgentEndorseCarsDO endorseCarDA, PolicyCars policyCar)
        {
            endorseCarDA.Artisan = policyCar.Artisan;
            //endorseCarDA.IsAtOrHD = newCarWithCompColl.has;
            endorseCarDA.CompDed = policyCar.CompDed;
            endorseCarDA.CollDed = policyCar.CollDed;
            endorseCarDA.HasPhyDam = policyCar.HasPhyDam;
            endorseCarDA.ConvTT = policyCar.ConvTT;
            endorseCarDA.IsTConvertible = policyCar.ConvTT;
            endorseCarDA.DisallowedAgentEndorseId = policyCar.OID;
            endorseCarDA.VehYear = policyCar.VehYear;
            endorseCarDA.VehMake = policyCar.VehMake;
            endorseCarDA.VehModel1 = policyCar.VehModel1;
            endorseCarDA.VehModel2 = policyCar.VehModel2;
            endorseCarDA.VIN = policyCar.VIN;
            endorseCarDA.PolicyNo = policyCar.PolicyNo;
        }

        public bool ReinstateCancelPolicyToActive(string policyNo)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: ReinstateCancelPolicyToActive called", Sessions.Instance.PolicyNo), "INFO");

            var isReinstated = false;

            try
            {
                var guid = Guid.NewGuid();
                using (var uowrk = XpoHelper.GetNewUnitOfWork())
                {
                    uowrk.ExecuteSproc("spUW_IssueReinstatement", policyNo, Sessions.Instance.Username,
                        guid.ToString());
                    isReinstated = true;
                }
            }
            catch (Exception ex)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    "Error in executing Reinstate Policy stored proc spUW_IssueReinstatement.", "INFO");
                isReinstated = false;
            }

            return isReinstated;
        }

        public bool SaveEnodrsementChanges(string policyNo, double premiumChange,
            DateTime? EndorseEffDate)
        {
            var model = new DisallowedEndorsementModel();

            for (var i = 0; i < 10; i++)
            {
                model = new DisallowedEndorsementModel();
                model.PolicyNumber = policyNo;
                model.DisallowedEndorsementQuoteId = GetDisallowedEndorsementChanges(policyNo, model, premiumChange,
                    EndorseEffDate);

                if (model.DisallowedEndorsementQuoteId > 0)
                    break;
            }

            return model.DisallowedEndorsementQuoteId > 0;
        }

        public bool SaveDataInEndorsementDetails(DisallowedAgentEndorseQuoteDO disallowedAgentQuote,
            string endorsementRemarks, bool isEndorsementToIssue, int disAllowedEndorseQuoteId, DateTime? dtEffDateTime)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: SaveDataInEndorsementDetails Func called. Endorse EffDate {1}",
                    Sessions.Instance.PolicyNo, disallowedAgentQuote.EndorsementEffDate?.ToShortDateString()), "INFO");
            //LOG
            var isSavedEndDetails = false;

            try
            {
                var sb = new StringBuilder();
                sb.Append("IsIssuance: " + Convert.ToString(isEndorsementToIssue) + " Additional Prem: " +
                          disallowedAgentQuote.AdditionalPremiumCollected
                          + " IsDriversChanged: " + Convert.ToString(disallowedAgentQuote.IsDriversChange) +
                          " IsVehicleChanged: " + Convert.ToString(disallowedAgentQuote.IsVehiclesChange)
                          + " IsCoverageChanged: " + Convert.ToString(disallowedAgentQuote.IsCoverageChange) +
                          " IsLossPayeeChanged: " + Convert.ToString(disallowedAgentQuote.IsLossPayeeChange));

                var sanitizedRemarksString = string.Empty;
                if (!string.IsNullOrEmpty(endorsementRemarks))
                {
                    sanitizedRemarksString = HttpUtility.HtmlEncode(endorsementRemarks);
                    if (sanitizedRemarksString.Length > 1000)
                        endorsementRemarks = sanitizedRemarksString.Substring(0, 999);
                    else if (sanitizedRemarksString.Length > 0 && sanitizedRemarksString.Length < 999)
                        endorsementRemarks = sanitizedRemarksString;
                }
                else
                {
                    endorsementRemarks = string.Empty;
                }

                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var endorsementDetails = new XpoEndorsementDetails(uow);
                    endorsementDetails.PolicyNo = disallowedAgentQuote.PolicyNo;
                    endorsementDetails.EndorsementDate = dtEffDateTime.GetValueOrDefault(); //DateTime.Now;
                    endorsementDetails.EndorsementReason = "Endorsement Details: " + sb;
                    endorsementDetails.AgentEndorsementNotes =
                        endorsementRemarks; //(sanitizedRemarksString.Length > 0) ? (System.Web.HttpUtility.HtmlEncode(endorsementRemarks)).Substring(0,1000) : String.Empty;
                    endorsementDetails.DisallowedEndorseQuoteId = disAllowedEndorseQuoteId;
                    endorsementDetails.Save();
                    uow.CommitChanges();
                    isSavedEndDetails = true;
                }
            }
            catch (Exception ex)
            {
            }

            return isSavedEndDetails;
        }

        public IssueEndorsementDetailsModel IssueEndorsementDetails(string policyNo, DateTime? effDate, string diffAmount,
            string isNonPremBearingsOnly, string username, string languagePreference)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                double amount;
                double.TryParse(diffAmount, out amount);

                this.AddLogMessage(
                    $"Endorsement-{policyNo}- Endorsmentt Amount-{amount} | amount in text- {diffAmount} | isNonPremBearingsOnly-{isNonPremBearingsOnly}");


                var model = new IssueEndorsementDetailsModel();
                model.EndorsementReasons = new List<string>();
                model.RequiredDocuments = new List<string>();

                var guid2 = new Guid();
                var endorseReasons = this.GetEndorseReasons(policyNo);
                foreach (var reason in endorseReasons)
                {
                    this.AddLogMessage($"Endorsement-{policyNo}- Endorsement Reason -{reason}", false);
                    model.EndorsementReasons.Add(reason);
                }

                if (isNonPremBearingsOnly == "1")
                {
                    this.AddLogMessage(
                        $"Endorsement-{policyNo}- Only Non-Premium changes are applying with sp - spUW_IssueNonPremBearingsEndorsement.");

                    amount = 0;
                    uow.ExecuteSproc("spUW_IssueNonPremBearingsEndorsement", policyNo, username,
                        effDate, guid2.ToString(), guid2.ToString(), DateTime.Now, effDate);
                }
                else
                {
                    uow.ExecuteSproc("spUW_IssueEndorsement", policyNo, username, effDate,
                        guid2.ToString(), guid2.ToString(), 1, 0, DateTime.Now, 0, effDate, 0, 1, 0,
                        Convert.ToString(amount));

                    this.AddLogMessage($"Endorsement-{policyNo}- Issued Endorsement.");

                    uow.ExecuteSproc("spUW_ClearPolicyEndorseQuote", policyNo, username,
                        guid2.ToString());

                }

                this.UpdatePreferredLanguage(policyNo, languagePreference);

                if (amount > 0)
                {
                    SSRSHelpers ssrsHelpers = new SSRSHelpers();
                    var paymentReceiptFilePath = ssrsHelpers.GeneratePaymentReceipt(policyNo, uow);

                    var fileName = Path.GetFileName(paymentReceiptFilePath);
                    var fileLink = string.Format(ConfigSettings.ReadSetting("PolicyDocsBaseUrl") + @"/Files/{0}",
                        fileName);
                    model.ReceiptDownloadLink = fileLink;

                    this.AddLogMessage(
                        $"Endorsement-{policyNo}- Endorsement Receipt Download Link -{fileLink}");
                    this.AddLogMessage(
                        $"Endorsement-{policyNo}- Endorsement Receipt File Path - {paymentReceiptFilePath}");
                }

                var policyRecord = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                if (policyRecord != null && !policyRecord.AllowPymntWithin15Days)
                {
                    policyRecord.AllowPymntWithin15Days = true;
                    uow.CommitChanges();
                }

                model.PaymentAmt = 0;
                return model;
            }
        }

        public void UpdateEftForPolicy(EftAccountUpdateModel model)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var accountType = model.IsAch ? "ECHK" : "CC";
                var requestData = new CardConnectRequest
                {
                    account = model.Account,
                    expiry = model.ExpirationDate,
                    cvv2 = model.Cvv,
                    name = model.Name,
                    address = model.Address,
                    city = model.City,
                    postal = model.Postal,
                    region = model.State,
                    accttype = accountType
                };
                var newProfileId = IntegrationsUtility.CreateCardConnectProfile(model.PolicyNo, requestData);
                if (!string.IsNullOrEmpty(newProfileId))
                {
                    var profileToUpdate =
                        uow.FindObject<XpoPolicyPaymentProfile>(CriteriaOperator.Parse("PolicyNo = ?", model.PolicyNo));
                    if (profileToUpdate != null)
                    {
                        if (model.IsCreditCard)
                            profileToUpdate.AccountType = "CC";
                        else
                            profileToUpdate.AccountType = "ECHK";

                        profileToUpdate.LastUpdatedByUserName = Sessions.Instance.Username;
                        profileToUpdate.LastUpdatedDate = DateTime.Now;
                        profileToUpdate.ProfileId = newProfileId;
                        profileToUpdate.Save();
                    }
                    else
                    {
                        var newEFTProfile = new XpoPolicyPaymentProfile(uow);
                        newEFTProfile.PolicyNo = model.PolicyNo;
                        newEFTProfile.ProfileId = newProfileId;
                        if (model.IsCreditCard)
                            newEFTProfile.AccountType = "CC";
                        else
                            newEFTProfile.AccountType = "ECHK";
                        newEFTProfile.CreatedByUserName = Sessions.Instance.Username;
                        newEFTProfile.CreatedDate = DateTime.Now;
                        newEFTProfile.LastUpdatedByUserName = Sessions.Instance.Username;
                        newEFTProfile.LastUpdatedDate = DateTime.Now;
                        newEFTProfile.Save();
                    }

                    var policyBankInfo =
                        new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber= ?", model.PolicyNo))
                            .OrderByDescending(p => p.IndexID).FirstOrDefault();
                    if (policyBankInfo != null)
                    {
                        if (model.IsCreditCard)
                            policyBankInfo.AccountType = "CreditCard";
                        else
                            policyBankInfo.AccountType = model.AchAcocuntType;

                        policyBankInfo.AccountNumber = model.ExtractLast4OfAccount();
                        policyBankInfo.Save();
                    }
                    else
                    {
                        var newPolicyBankInfo = new PolicyBankInfo(uow);
                        newPolicyBankInfo.Account = "INDIVIDUAL";
                        newPolicyBankInfo.PolicyNumber = model.PolicyNo;
                        if (model.IsCreditCard)
                            newPolicyBankInfo.AccountType = "CreditCard";
                        else
                            newPolicyBankInfo.AccountType = model.AchAcocuntType;

                        newPolicyBankInfo.AccountNumber = model.ExtractLast4OfAccount();
                        newPolicyBankInfo.Save();
                    }
                }

                uow.CommitChanges();
            }
        }

        public DisallowedEndorsementModel GetDisallowedEndorsements(string policyNumber, string errorMessage = null)
        {
            var model = new DisallowedEndorsementModel();
            model.PolicyNumber = policyNumber;
            model.ErrorMessage = errorMessage;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var endorsementChangesQuote = uow.FindObject<DisallowedAgentEndorseQuoteDO>(
                    CriteriaOperator.Parse(string.Format("PolicyNo = '{0}' AND IsActive=1", policyNumber)));

                if (endorsementChangesQuote != null)
                {
                    model.DisallowedEndorseQuote = endorsementChangesQuote;

                    var cars = new XPCollection(uow, typeof(DisallowedAgentEndorseCarsDO),
                        CriteriaOperator.Parse(string.Format("DisallowedAgentEndorseId={0}",
                            endorsementChangesQuote.OID)));
                    foreach (DisallowedAgentEndorseCarsDO carChangeDo in cars) model.Cars.Add(carChangeDo);

                    var drivers = new XPCollection(uow, typeof(DisallowedAgentEndorseDriversDO),
                        CriteriaOperator.Parse(string.Format("DisallowedAgentEndorseId={0}",
                            endorsementChangesQuote.OID)));
                    foreach (DisallowedAgentEndorseDriversDO driverChangeDo in drivers)
                    {
                        model.Drivers.Add(driverChangeDo);

                        var driversViolations = new XPCollection(uow, typeof(DisallowedAgentEndorseDriversViolationsDO),
                            CriteriaOperator.Parse(string.Format("DisallowedAgentEndorseDriverId={0}",
                                driverChangeDo.OID)));
                        foreach (DisallowedAgentEndorseDriversViolationsDO driverViolationChangeDo in driversViolations)
                            model.DriverViolations.Add(driverViolationChangeDo);
                    }


                    var lossPayees = new XPCollection(uow, typeof(DisallowedAgentEndorseLossPayeeDO),
                        CriteriaOperator.Parse(string.Format("DisallowedAgentEndorseId={0}",
                            endorsementChangesQuote.OID)));
                    foreach (DisallowedAgentEndorseLossPayeeDO lossPayee in lossPayees) model.LossPayee.Add(lossPayee);

                    model.AgencyCode = endorsementChangesQuote.AgentCode;
                    var agent = uow.FindObject<AgentsDO>(CriteriaOperator.Parse("AgentCode = ?",
                        endorsementChangesQuote.AgentCode));
                    if (agent != null)
                    {
                        model.AgencyName = agent.AgencyName;
                        model.AgencyAddress = agent.MailAddress;
                        model.AgentInfo = agent;
                    }

                    model.InsuredFirstName = endorsementChangesQuote.Ins1First;
                    model.InsuredLastName = endorsementChangesQuote.Ins1Last;
                    model.InsuredMI = endorsementChangesQuote.Ins1MI;
                    model.Insured2FullName = endorsementChangesQuote.Ins2FullName;
                    model.AdditionalPremiumCollected = endorsementChangesQuote.AdditionalPremiumCollected;

                    model.InsuredAddress = string.Format("{0}, {1}, {2}, {3}", endorsementChangesQuote.MailStreet,
                        endorsementChangesQuote.MailCity, endorsementChangesQuote.MailState,
                        endorsementChangesQuote.MailZip);
                    model.IsNewAddress = endorsementChangesQuote.IsAddressChange.GetValueOrDefault();
                    model.PolicyExpirationDate = endorsementChangesQuote.ExpDate;
                    model.EffectiveTime = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                    model.PolicyInceptionDate = endorsementChangesQuote.PolicyInceptionDate.GetValueOrDefault();
                    model.EndorsementEffectiveDate = endorsementChangesQuote.EndorsementEffDate.GetValueOrDefault();

                    model.Paperless = endorsementChangesQuote.Paperless;
                    model.HomeOwner = endorsementChangesQuote.Homeowner;

                    model.IsFromIssuance = string.IsNullOrEmpty(errorMessage);
                    model.SetEndorsementChangesFromDB();
                    model.DisallowedEndorsementQuoteId = endorsementChangesQuote.OID;
                }
            }

            return model;
        }

        public DisallowedAgentEndorseQuoteDO GetDisallowedAgentEndorsementQuote(DisallowedEndorsementModel model, FormCollection fc, out bool nonPremChanges, out bool premChanges)
        {
            var carChecks = new List<string>
            {
                "IsATorHD", "IsAntiLockBreaks", "HasDriverSideAirBag", "HasPassengerSideAirBag", "IsTConvertible",
                "IsGaragedAtInsRes", "HasDamage", "isCustomized", "IsCoOwoned"
            };
            var carCheckRemarks = new List<string>
            {
                "ATorHDRemarks", "AntiLockBreaksRemarks", "DriverSideAirBagRemarks", "PassengerSideAirBagRemarks",
                "TConvertibleRemarks", "GaragedAtInsResRemarks", "DamageRemarksRemarks", "CustomizedRemarks",
                "CoOwonedRemarks"
            };
            var fcAllKeys = fc.AllKeys.ToList();
            var carOIds = fcAllKeys.Where(x => x.StartsWith("IsATorHD"))
                .Select(x => x.Replace("IsATorHD", string.Empty)).Distinct();
            DisallowedAgentEndorseQuoteDO disallowedQuote = null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                disallowedQuote =
                    uow.FindObject<DisallowedAgentEndorseQuoteDO>(CriteriaOperator.Parse("OID = ?",
                        model.DisallowedEndorsementQuoteId));
                disallowedQuote.Ins1First = model.InsuredFirstName;
                disallowedQuote.Ins1Last = model.InsuredLastName;
                disallowedQuote.Ins1MI = model.InsuredMI;
                disallowedQuote.IsActive = true;
                disallowedQuote.AgentAltESignEndorseEmail = model.AgentAltESignEndorseEmailAddr;
                //disallowedQuote.AgentEndorsementRemarks = model.AgentEndorsementRemarks;
                disallowedQuote.Save();

                foreach (var carOid in carOIds)
                {
                    var car = uow.FindObject<DisallowedAgentEndorseCarsDO>(
                        CriteriaOperator.Parse("OID = ? AND DisallowedAgentEndorseId=?", carOid,
                            model.DisallowedEndorsementQuoteId));
                    if (car != null)
                    {
                        car.IsAtOrHD = Convert.ToBoolean(fc["IsATorHD" + carOid].Split(',')[0]);
                        car.IsAntiLockBrake = Convert.ToBoolean(fc["IsAntiLockBreaks" + carOid].Split(',')[0]);
                        car.IsDriverSideAirBag =
                            Convert.ToBoolean(fc["HasDriverSideAirBag" + carOid].Split(',')[0]);
                        car.IsPassengerSideAirBag =
                            Convert.ToBoolean(fc["HasPassengerSideAirBag" + carOid].Split(',')[0]);
                        car.IsTConvertible = Convert.ToBoolean(fc["IsTConvertible" + carOid].Split(',')[0]);
                        //car. = Convert.ToBoolean(fc["IsGaragedAtInsRes" + carOid].Split(',')[0]);
                        car.HasDamage = Convert.ToBoolean(fc["HasDamage" + carOid].Split(',')[0]);
                        car.IsCustomized = Convert.ToBoolean(fc["isCustomized" + carOid].Split(',')[0]);
                        car.IsCoOwned = Convert.ToBoolean(fc["IsCoOwoned" + carOid].Split(',')[0]);
                        car.AtOrHDRemarks = fc["ATorHDRemarks" + carOid];

                        car.AntiLockBrakeRemarks = fc["AntiLockBreaksRemarks" + carOid];
                        car.DriverSideAirBagRemarks = fc["DriverSideAirBagRemarks" + carOid];
                        car.PassengerSideAirBagRemarks = fc["PassengerSideAirBagRemarks" + carOid];
                        car.TConvertibleRemarks = fc["TConvertibleRemarks" + carOid];
                        //car. = fc["GaragedAtInsResRemarks" + carOid];
                        car.DamageRemarks = fc["DamageRemarksRemarks" + carOid];
                        car.CustomizedRemarks = fc["CustomizedRemarks" + carOid];
                        car.CoOwnedRemarks = fc["CoOwonedRemarks" + carOid];
                    }
                }

                uow.CommitChanges();

                SaveDataInEndorsementDetails(disallowedQuote, model.AgentEndorsementRemarks,
                    model.IsFromIssuance, model.DisallowedEndorsementQuoteId, disallowedQuote.EndorsementEffDate);

                PolicyWebUtils.GetEndorsementChangeTypes(out premChanges, out nonPremChanges,
                    disallowedQuote.PolicyNo);
                return disallowedQuote;
            }
        }

        public List<EndorsementLienHolderModel> DeleteLienHolder(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getLien =
                    new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow, CriteriaOperator.Parse("ID = ?", id))
                        .FirstOrDefault();

                var carIndex = getLien.CarIndex;
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: DELETE LIENHOLDER DATA FROM CAR {1}", Sessions.Instance.PolicyNo, carIndex),
                    "WARN");

                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteCarLienHolders", "ID", id);

                var lienHolders = new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo, carIndex));

                var listLiens = new List<EndorsementLienHolderModel>();
                foreach (var lien in lienHolders)
                {
                    var lModel = new EndorsementLienHolderModel();
                    lModel.ID = lien.ID;
                    lModel.Name = lien.Name;
                    lModel.StreetAddress = lien.Address;
                    lModel.City = lien.City;
                    lModel.State = lien.State;
                    lModel.Phone = lien.Phone;
                    lModel.ZIP = lien.Zip;
                    lModel.LienHolderIndex = lien.ID;
                    listLiens.Add(lModel);
                }
                return listLiens;
            }
        }

        public List<EndorsementLienHolderModel> AddLienHolder(EndorsementLienHolderAddModel model, int carIndex)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var addLien1 = new PolicyEndorseQuoteCarLienHolders(uow);
                addLien1.CarIndex = carIndex; //LF 11/7/18 model.CarIndex;
                addLien1.LienHolderIndex = 1;
                addLien1.PolicyNo = Sessions.Instance.PolicyNo;
                addLien1.Name = model.Name.Upper();
                addLien1.Address = model.StreetAddress.Upper();
                addLien1.City = model.City.Upper();
                addLien1.State = model.State.Upper();
                addLien1.Zip = model.ZIP;
                addLien1.DateCreated = DateTime.Now;
                addLien1.Phone = model.Phone;
                addLien1.isAddInterest = model.isAddInterest;
                addLien1.Save();
                uow.CommitChanges();
                var lienholders = new XPCollection<PolicyEndorseQuoteCarLienHolders>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?",
                        Sessions.Instance.PolicyNo,
                        carIndex)); //LF changed 11/7/18  was model.carIndex
                var listLienHolderModel = new List<EndorsementLienHolderModel>();
                foreach (var lien in lienholders)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: RELOAD LIENHOLDER DATA FOR GRID DISPLAY ON VEHICLE EDIT",
                            Sessions.Instance.PolicyNo), "INFO");
                    var lModel = new EndorsementLienHolderModel();
                    lModel.ID = lien.ID;
                    lModel.Name = lien.Name;
                    lModel.StreetAddress = lien.Address;
                    lModel.City = lien.City;
                    lModel.State = lien.State;
                    lModel.Phone = lien.Phone;
                    lModel.ZIP = lien.Zip;
                    lModel.LienHolderIndex = lien.ID;
                    listLienHolderModel.Add(lModel);
                }

                return listLienHolderModel;
            }
        }

        public XPCollection<PolicyEndorseQuoteCars> VehicleDelete(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: DELETE VEHICLE DATA FOR CAR {1}", Sessions.Instance.PolicyNo, id), "WARN");
                var car =
                    new XPCollection<PolicyEndorseQuoteCars>(uow, CriteriaOperator.Parse("OID = ?", id))
                        .FirstOrDefault();
                if (car == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: CAR DATA NOT FOUND {1}", Sessions.Instance.PolicyNo, id), "FATAL");
                    throw new HttpException(404, "Vehicle not found");
                }

                car.Delete();
                uow.CommitChanges();
                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteCars", "OID", id);
                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteCarLienHolders", "PolicyNo", "CarIndex", car.PolicyNo,
                    car.CarIndex.ToString()); //LF 12/9/18  added
                var getCars =
                    new XPCollection<PolicyEndorseQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", car.PolicyNo));
                var count = 1;
                foreach (var quoteCar in getCars)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: LOAD CAR DATA {1}", Sessions.Instance.PolicyNo, id), "INFO");
                    uow.BeginTransaction();
                    var updateCars = uow.FindObject<PolicyEndorseQuoteCars>(
                        CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", quoteCar.PolicyNo, quoteCar.CarIndex));
                    updateCars.CarIndex = count;
                    updateCars.Save();
                    uow.CommitChanges();
                    count += 1;
                }

                var cars =
                    new XPCollection<PolicyEndorseQuoteCars>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? ", car.PolicyNo));
                return cars;
            }
        }

        public EndorsementVehicleModel VehicleLoad(string id, EndorsementPolicyModel endorseModel)
        {
            EndorsementVehicleModel model;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: LOAD VEHICLE DATA FOR QUOTE", Sessions.Instance.PolicyNo), "INFO");
                var car =
                    new XPCollection<PolicyEndorseQuoteCars>(uow, CriteriaOperator.Parse("OID = ?", id))
                        .FirstOrDefault();
                if (car == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VEHICLE DATA NOT FOUND FOR CAR {1}", Sessions.Instance.PolicyNo, id),
                        "FATAL");
                    throw new HttpException(404, "Car not found");
                }

                model = UtilitiesRating.MapXpoToModel(car);
                model.IsEdit = true;
                var tmpModel = endorseModel.Vehicles.Find(v => v.VehicleId == id);
                endorseModel.Vehicles.Remove(tmpModel);
                endorseModel.Vehicles.Add(model);
            }

            return model;
        }

        public XpoAgentsRestrictionByZipAndCoverage AgentsRestrictionByZipAndCoverage(XpoPolicy xpoPolicy)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var agentRestrictions =
                    uow.FindObject<XpoAgentsRestrictionByZipAndCoverage>(
                        CriteriaOperator.Parse("AgentCode = ? AND Zip = ?", xpoPolicy.AgentCode,
                            xpoPolicy.GarageZip));
                return agentRestrictions;
            }
        }

        public void GetVehicleSymbols(EndorsementVehicleModel model, VeriskVinSearchResultBody vehicleInfo)
        {
            model.PDSymbol = vehicleInfo.Liability?.RiskAnalyzerPropertyDamageRatingSymbol;
            model.PIPSymbol = vehicleInfo.Liability?.RiskAnalyzerPersonalInjuryProtectionRatingSymbol;
            model.BISymbol = vehicleInfo.Liability?.RiskAnalyzerBodilyInjuryRatingSymbol;
            model.MPSymbol = vehicleInfo.Liability?.RiskAnalyzerMedicalPaymentsRatingSymbol;
            if (!string.IsNullOrEmpty(
                vehicleInfo.PhysicalDamage?.RiskAnalyzerCollisionRatingSymbol2010Prior))
                model.CollSymbol = vehicleInfo.PhysicalDamage?.RiskAnalyzerCollisionRatingSymbol2010Prior;
            else
                model.CollSymbol = vehicleInfo.PhysicalDamage
                    ?.RiskAnalyzerCollisionRatingSymbol2011Subsequent;

            if (!string.IsNullOrEmpty(vehicleInfo.PhysicalDamage
                ?.RiskAnalyzerComprehensiveRatingSymbol2010Prior))
                model.CompSymbol = vehicleInfo.PhysicalDamage
                    ?.RiskAnalyzerComprehensiveRatingSymbol2010Prior;
            else
                model.CompSymbol = vehicleInfo.PhysicalDamage
                    ?.RiskAnalyzerComprehensiveRatingSymbol2011Subsequent;
        }

        public void AddVehicleEndorsement(EndorsementVehicleModel model, EndorsementPolicyModel endorseModel)
        {
            if (model.HasPhyDam)
            {
                var vcompded = endorseModel.CompDed;
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var endorsementType = "ADD_VEHICLE_WITH_COMP_WITH_COLL";
                    IEnumerable<PolicyEndorsementTypes> existingTypes =
                        new XPCollection<PolicyEndorsementTypes>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND EndorsementType = ?",
                                Sessions.Instance.PolicyNo,
                                endorsementType));
                    if (!existingTypes.Any())
                    {
                        var newType = new PolicyEndorsementTypes(uow);
                        newType.PolicyNo = Sessions.Instance.PolicyNo;
                        newType.EndorsementType = endorsementType;
                        newType.Save();
                        uow.CommitChanges();
                    }
                }
            }
            else
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var endorsementType = "ADD_VEHICLE_NO_COMP_NO_COLL";
                    IEnumerable<PolicyEndorsementTypes> existingTypes =
                        new XPCollection<PolicyEndorsementTypes>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND EndorsementType = ?", Sessions.Instance.PolicyNo,
                                endorsementType));
                    if (!existingTypes.Any())
                    {
                        var newType = new PolicyEndorsementTypes(uow);
                        newType.PolicyNo = Sessions.Instance.PolicyNo;
                        newType.EndorsementType = endorsementType;
                        newType.Save();
                        uow.CommitChanges();
                    }
                }
            }
        }

        public void AddVehicle(EndorsementVehicleModel model, FormCollection formData, EndorsementPolicyModel endorseModel)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: ADD NEW CAR TO QUOTE", Sessions.Instance.PolicyNo), "INFO");
                // Deny if there are too many drivers.
                var getCars = new XPCollection<PolicyEndorseQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));

                if (getCars.Count > 5)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: HIT MAX CARS ALLOWED", Sessions.Instance.PolicyNo), "WARN");
                    throw new HttpException(400, "One policy may only have 5 Cars maximum");
                }

                model.CarIndex = getCars.Count + 1;
                var car = new PolicyEndorseQuoteCars(uow);
                car.CarIndex = getCars.Count + 1;
                UtilitiesRating.MapModelToXpo(model, car);
                car.Save();
                uow.CommitChanges();
                model.LienHolders = this.SaveLienHolders(formData, car.OID, car.CarIndex, car.VIN);

                getCars.Add(car);
                endorseModel.Vehicles.Add(model);
            }
        }

        public void UpdateVehicle(EndorsementVehicleModel model, FormCollection formData, EndorsementPolicyModel endorseModel)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getCars = new XPCollection<PolicyEndorseQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?",
                        Sessions.Instance.PolicyNo,
                        model.CarIndex));
                var car = getCars.FirstOrDefault();
                getCars.Remove(car);
                car.PolicyNo = Sessions.Instance.PolicyNo;
                UtilitiesRating.MapModelToXpo(model, car);
                car.Save();
                uow.CommitChanges();
                getCars.Add(car);

                model.LienHolders = this.SaveLienHolders(formData, car.OID, car.CarIndex, car.VIN);

                endorseModel.Vehicles.Add(model);
            }
        }

        public string GetPaymentReceiptPath(string id, string pymnttype, HttpServerUtilityBase Server, UrlHelper Url)
        {
            var logo = Path.Combine(Server.MapPath(ConfigSettings.ReadSetting("ContentImagesPath")),
                "MyDemoInsCo-Logo.png");
            var filename = string.Format("{0}_Receipt_{1}.pdf", id, DateTime.Now.ToString("MMddyyyymmhhss"));
            var outputPath = Path.Combine(Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath")),
                filename);

            var insuredname = PortalUtils.GetInsuredName(id);
            var effdate = PortalUtils.GetEffDate(id);
            var expdate = PortalUtils.GetExpDate(id);

            PortalUtils.GeneratePaymentReceipt(id, pymnttype, logo, outputPath);

            if (System.IO.File.Exists(outputPath))
            {
                var imagingSendTo = _webHostService.IsDev() ? "it@palminsure.com" : "system@palminsure.com";
                var imagingSubject = string.Format("{0} CATEGORY-Monthly Payment Receipt", id);
                var imagingBody = "Monthly Payment Imaging Receipt Made On " + DateTime.Now.ToString("F");
                try
                {
                    if (id.SafeString().Contains("PAFL"))
                        //EmailSystem.SendMail(imagingSendTo, "system@palminsure.com", imagingSubject, imagingBody, false, outputPath);
                        PortalUtils.PostAgentUploadRec(id, Path.GetFileName(outputPath),
                            Path.GetFileName(outputPath), outputPath, "PYMNTRECEIPT", Sessions.Instance.Username);
                }
                catch (Exception ex)
                {
                    PortalUtils.PostFailedQueue(Sessions.Instance.Username, id, imagingSendTo, imagingSubject,
                        imagingBody, "MAIL MONTHLY PAYMENT RECEIPT", outputPath, "PORTAL");
                }
                return outputPath;
            }
            return null;
        }
    }
}