﻿using AgentPortal.Models;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using AlertAuto.Integrations.Contracts.MVR;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using log4net;
using PalmInsure.Palms.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using Blowfish = AgentPortal.Support.Utilities.Blowfish;

namespace AgentPortal.Services
{
    public class ReportService
    {
        private List<string> listPDFs = new List<string>();
        private List<string> listParamNames = new List<string>();
        private List<string> listParamValues = new List<string>();
        private string _reportName = null;
        private readonly log4net.ILog quoteControllerLog = LogManager.GetLogger("QuoteControllerLogger");
        public readonly EmailService _emailService;

        public QuoteService QuoteService;
        public AjaxService AjaxService;

        public ReportService(EmailService emailService,QuoteService quoteService, AjaxService ajaxService)
        {
            _emailService = emailService;
            QuoteService = quoteService;
            AjaxService = ajaxService;
        }

        public void ValidateSSRSReport(SSRSUtils Ssrs)
        {
            try
            {
                Ssrs.GenerateSsrsReport();
                Ssrs.ReportName = Ssrs.ReportName.Replace("Underwriting", "");
                Ssrs.AddToImaging(Ssrs.FilePathOut);
                Ssrs.ListPdfs.Add(Ssrs.FilePathOut);
            }
            catch (ReportServerNotFoundException)
            {
                Ssrs.ListFailedPdfs.Add(Ssrs.FilePathOut);
            }
        }

        public void BuildExistingReports(QuoteIntegrationsViewModel vm)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicyQuote quote =
                    new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", vm.QuoteNo))
                        .FirstOrDefault();
                if (quote != null)
                {
                    vm.RiskCheckOrdered = quote.RCPOSOrdered;
                    IEnumerable<PolicyQuoteDrivers> drivers =
                        new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", vm.QuoteNo));
                    XPCollection<PolicyQuoteDriversViolations> mvrViolations =
                        new XPCollection<PolicyQuoteDriversViolations>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND FromMVR = 1", vm.QuoteNo));
                    XPCollection<PolicyQuoteDriversViolations> existingAPlusVilations =
                        new XPCollection<PolicyQuoteDriversViolations>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND FromAPlus = 1", vm.QuoteNo));
                    XPCollection<PolicyQuoteDriversViolations> noHitCVViolations =
                        new XPCollection<PolicyQuoteDriversViolations>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND FromCV = 1", vm.QuoteNo));
                    if (quote.RCPOSOrdered)
                    {
                        var notAssociatedDrivers = new XPCollection<PolicyQuoteNotAssociatedDrivers>(uow,
                           CriteriaOperator.Parse("PolicyNo = ? AND NotAssociatedDriver = 0", vm.QuoteNo));
                        var cars = new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", vm.QuoteNo));

                        var brandedTitleVehicles = new XPCollection<XpoBrandedTitleVehicles>(uow,
                         CriteriaOperator.Parse("PolicyNo = ?", vm.QuoteNo));

                        foreach (var brandedTitleVeh in brandedTitleVehicles)
                        {
                            if (cars.Any(x => x.VIN?.ToLower() == brandedTitleVeh.VIN?.ToLower() && x.HasPhyDam &&
                                 ((!string.IsNullOrEmpty(x.CompDed) && x.CompDed != "0" && x.CompDed != "NONE") ||
                                 (!string.IsNullOrEmpty(x.CollDed) && x.CollDed != "0" && x.CollDed != "NONE"))))
                            {
                                vm.BrandedTitleVehicles.Add(new BrandedTitleVehiclesViewModel(brandedTitleVeh));
                            }
                        }

                        foreach (PolicyQuoteNotAssociatedDrivers notAssociatedDriv in notAssociatedDrivers)
                        {
                            var driver = drivers.FirstOrDefault(x =>
                           notAssociatedDriv.FirstName?.Equals(x.FirstName, StringComparison.OrdinalIgnoreCase) == true &&
                           notAssociatedDriv.LastName?.Equals(x.LastName, StringComparison.OrdinalIgnoreCase) == true);
                            if (driver == null || (notAssociatedDriv.DOB != DateTime.MinValue && driver.DOB.ToShortDateString() != notAssociatedDriv.DOB.ToShortDateString()))
                            {
                                var riskCheckViolationModel = new RiskCheckViolationsModel()
                                {
                                    DriverName = $"{notAssociatedDriv.FirstName} {notAssociatedDriv.LastName}",
                                    DOB = notAssociatedDriv.DOB,
                                    NotAssociatedDriverIndex = notAssociatedDriv.ID
                                };
                                riskCheckViolationModel.DriversViolations.Add(new RiskCheckDriverViolationsModel(notAssociatedDriv.ViolationDesc));
                                vm.RiskCheckDriverViolationsModels.Add(riskCheckViolationModel);
                            }
                        }

                        //Check for non-owned vehicles
                        IList<RiskCheckNonOwnedVehicles> nonOwnedVehicles = new XPCollection<RiskCheckNonOwnedVehicles>(uow, CriteriaOperator.Parse("PolicyNo = ?", vm.QuoteNo)).ToList();
                        if (nonOwnedVehicles.Any())
                        {
                            foreach (var unOwnedVehicle in nonOwnedVehicles)
                            {
                                PolicyQuoteCars unOwnedCar = cars.FirstOrDefault(c => c.VIN.Equals(unOwnedVehicle.VIN));
                                if (unOwnedCar != null)
                                {

                                    if (unOwnedCar.PurchNew && unOwnedCar.DatePurchased != DateTime.MinValue)
                                    {
                                        int daysFromPurchasedDate = DateTime.Now.Date.Subtract(unOwnedCar.DatePurchased.GetValueOrDefault().Date).Days;
                                        // add to unowned vehicles if outside of 14 days since purchased or a future date is used.
                                        if (daysFromPurchasedDate < 0 || daysFromPurchasedDate > 14)
                                        {
                                            vm.UnOwnedVehicles.Add(unOwnedCar);
                                        }
                                    }
                                    else
                                    {
                                        vm.UnOwnedVehicles.Add(unOwnedCar);
                                    }
                                }
                            }
                        }
                    }

                    IList<MvrViolationsModel> mvrViolationsPast36MonthsForDriver = new List<MvrViolationsModel>();
                    foreach (PolicyQuoteDriversViolations mvrViolation in mvrViolations)
                    {
                        PolicyQuoteDrivers driver = drivers.Where(d => d.DriverIndex == mvrViolation.DriverIndex)
                            .FirstOrDefault();
                        if (driver != null)
                        {
                            MvrViolationsModel mvrModel = new MvrViolationsModel()
                            {
                                DriverName = $"{driver.FirstName} {driver.LastName}",
                                DriverIndex = Convert.ToString(driver.DriverIndex),
                                DateLicensed = driver.DateLicensed,
                                DateOfBirth = driver.DOB
                            };
                            mvrModel.DriversViolations.Add(mvrViolation);

                            bool isSameViolationExist = false;
                            foreach (MvrViolationsModel mvrViolationModels in vm.MvrDriverViolationsModels)
                            {
                                foreach (PolicyQuoteDriversViolations polViolation in mvrViolationModels.DriversViolations)
                                {
                                    if (polViolation.ViolationDate == mvrViolation.ViolationDate && polViolation.ViolationCode.Equals(mvrViolation.ViolationCode)
                                      && polViolation.ViolationGroup.Equals(mvrViolation.ViolationGroup))
                                    {
                                        isSameViolationExist = true;
                                    }
                                    if (isSameViolationExist)
                                        break;
                                }

                                if (isSameViolationExist)
                                    break;
                            }

                            if (isSameViolationExist)
                                continue;

                            if (!vm.MvrDriverViolationsModels.Contains(mvrModel))
                                vm.MvrDriverViolationsModels.Add(mvrModel);
                        }
                    }

                    QuoteService.HandleAAPointsForMVRViolations(quote.EffDate, vm.MvrDriverViolationsModels);
                    foreach (PolicyQuoteDriversViolations aPlusViolation in existingAPlusVilations)
                    {
                        PolicyQuoteDrivers driver = drivers.Where(d => d.DriverIndex == aPlusViolation.DriverIndex)
                            .FirstOrDefault();
                        APlusClaimModel aplusModel = new APlusClaimModel()
                        {
                            DriverName = $"{driver.FirstName} {driver.LastName}",
                            ClaimDateOfLoss = aPlusViolation.ViolationDate.ToString("yyyyMMdd"),
                            ClaimDescription = aPlusViolation.ViolationDesc,
                            InsuredAtFault = aPlusViolation.ViolationGroup.Equals("ACC")
                        };
                        vm.APlusClaimModels.Add(aplusModel);
                        if (vm.APlusClaimModels.Where(m => m.ClaimDescription.Contains("No Hit")).FirstOrDefault() !=
                            null)
                        {
                            vm.APlusIsNoHit = true;
                        }
                    }

                    IList<string> carrierClaimNumbers = existingAPlusVilations.Select(v =>
                    {
                        string desc = v.ViolationDesc;
                        int indexOfFirstParenthesis = desc.IndexOf("(");
                        if (indexOfFirstParenthesis > -1)
                        {
                            int length = desc.Length - indexOfFirstParenthesis;
                            desc = desc.Remove(indexOfFirstParenthesis, length);
                            return desc.Trim();
                        }
                        else
                        {
                            return v.ViolationDesc;
                        }
                    }).ToList();
                    vm.TotalNumberOfClaims = carrierClaimNumbers.Count;

                    foreach (var driver in drivers)
                    {
                        QuoteService.HandleAAPointsForAplusViolations(
                            existingAPlusVilations.Where(v => v.DriverIndex.Equals(driver.DriverIndex)),
                            vm.APlusClaimModels, driver.FirstName, driver.LastName);
                    }

                    if (noHitCVViolations != null && noHitCVViolations.Any())
                    {
                        foreach (PolicyQuoteDriversViolations cvViolation in noHitCVViolations)
                        {
                            PolicyQuoteDrivers driver = drivers.Where(d => d.DriverIndex == cvViolation.DriverIndex)
                                .FirstOrDefault();
                            CoverageVerifierReportModel cvModel = new CoverageVerifierReportModel()
                            {
                                DriverName = $"{driver.FirstName} {driver.LastName}",
                                CarrierName = "No Hit CV",
                                PolicyExpiration = quote.PreviousExpDate.ToString("yyyyMMdd"),
                            };
                            vm.CoverageVerifierReportModels.Add(cvModel);
                            // Only no hits exist in policyQuoteDriversViolations
                            vm.CVIsNoHit = true;
                        }
                    }
                    else
                    {
                        if ((!string.IsNullOrEmpty(quote.PreviousCompany)) && (quote.CvOrdered))
                        {
                            foreach (PolicyQuoteDrivers driver in drivers)
                            {
                                CoverageVerifierReportModel cvModel = new CoverageVerifierReportModel()
                                {
                                    DriverName = $"{driver.FirstName} {driver.LastName}",
                                    CarrierName = quote.PreviousCompany,
                                    PolicyExpiration = quote.PreviousExpDate.ToString("yyyyMMdd")
                                };
                                vm.CoverageVerifierReportModels.Add(cvModel);
                            }
                        }
                    }
                    // call policy score sproc if risk check and coverage verifier have been ordered
                    if (vm.RiskCheckOrdered && vm.CoverageVerifierOrdered())
                    {
                        bool isNewBusinessAllowed = false;
                        OperandValue newBusinessAllowed = new OperandValue(isNewBusinessAllowed);
                        var result = uow.ExecuteSproc("spUW_GetPolicyScore", vm.QuoteNo,
                            Sessions.Instance.Username, newBusinessAllowed);
                        if (result?.ResultSet?[1] != null && result.ResultSet[1].Rows[0] != null &&
                            result.ResultSet[1].Rows[0].Values[1] != null)
                        {
                            isNewBusinessAllowed = (bool)result.ResultSet[1].Rows[0].Values[1];
                            vm.PolicyScoreValidForBinding = isNewBusinessAllowed;
                        }
                        else
                        {
                            vm.PolicyScoreValidForBinding = false;
                        }
                    }
                }
            }
        }

        public void HandleMvrOrderingForQuotes(XpoPolicyQuote quote, PolicyQuoteDrivers driver, IList<PolicyQuoteDriversViolations> currentViolationsForQuote, IEnumerable<ViolationCodes_SVC> violationCodes, QuoteIntegrationsViewModel vm)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string quoteNo = quote.PolicyNo;
                DateTime now = DateTime.Now;
                //Check for existing MVR
                if (currentViolationsForQuote.Any(cv =>
                    cv.FromMVR && cv.DriverIndex.Equals(driver.DriverIndex)))
                {
                    foreach (PolicyQuoteDriversViolations violation in currentViolationsForQuote.Where(cv => cv.FromMVR)
                    )
                    {
                        MvrViolationsModel existingMvr = new MvrViolationsModel()
                        {
                            DriverName = $"{driver.FirstName} {driver.LastName}",
                        };
                        existingMvr.DriversViolations.Add(violation);

                        vm.MvrDriverViolationsModels.Add(existingMvr);
                    }
                }
                else
                {
                    //Get Mvr report
                    VeriskMvrSubmitRequestBody mvrReportResult = IntegrationsUtility.MvrSubmitRequest(quoteNo,
                        driver.FirstName,
                        driver.LastName, driver.DOB.ToString(),
                        driver.LicenseNo, driver.LicenseSt);
                    if (mvrReportResult != null && !string.IsNullOrEmpty(mvrReportResult.OrderNumber))
                    {
                        //Wait... per verisk
                        Thread.Sleep(5000);
                        VeriskRetreiveMvrReportBody reportForDriver =
                            IntegrationsUtility.MvrRequestReport(quoteNo, mvrReportResult.OrderNumber);
                        driver.HasMvr = true;
                        driver.MvrDate = DateTime.Now;
                        driver.VeriskMvrOrderNumber = mvrReportResult.OrderNumber;
                        if (reportForDriver != null && reportForDriver.MotorVehicleReports != null &&
                            reportForDriver.MotorVehicleReports.Any())
                        {
                            XPCollection<PolicyQuoteDriversViolations> mvrViolations =
                                new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse("PolicyNo = ? AND FromMVR = 1", quote.PolicyNo));

                            IList<MvrViolationsModel> violations =
                                new List<MvrViolationsModel>();
                            IList<MvrViolationsModel> mvrViolationsPast36MonthsForDriver =
                                new List<MvrViolationsModel>();
                            foreach (var report in reportForDriver.MotorVehicleReports)
                            {
                                if (!report.Status.Code.ToUpper().Equals("C"))
                                {
                                    if (!report.Status.Code.ToUpper().Equals("1") &&
                                        !report.Status.Code.ToUpper().Equals("2"))
                                    {
                                        if (report.Driver != null)
                                        {
                                            if (report.Driver.License != null)
                                            {
                                                driver.LicenseStatus = report.Driver.License.Status;
                                                driver.LicenseExpiration =
                                                    DateTime.ParseExact(report.Driver.License.ExpirationDate,
                                                        "yyyyMMdd",
                                                        CultureInfo.InvariantCulture);
                                                driver.HasMvr = true;
                                                driver.Save();
                                            }

                                            if (report.Driver.Violations != null &&
                                                report.Driver.Violations.Count() > 0)
                                            {
                                                MvrViolationsModel driverViolationsModel = new MvrViolationsModel();
                                                driverViolationsModel.DriverName =
                                                    $"{driver.FirstName} {driver.LastName}";
                                                driverViolationsModel.DriverIndex = driver.DriverIndex.ToString();
                                                driverViolationsModel.DateLicensed = driver.DateLicensed;
                                                driverViolationsModel.DateOfBirth = driver.DOB;
                                                foreach (Violation violation in report.Driver?.Violations)
                                                {
                                                    bool isSameViolationExistForCurrDriver = false;
                                                    bool isViolationFromAnyDriverDBMatch = false;
                                                    bool isSameViolationExist = false;
                                                    ViolationCodes_SVC svcViolationCode =
                                                        violationCodes.FirstOrDefault(vc =>
                                                            vc.SVC.Equals(violation.CustomCode));

                                                    Support.DataObjects.XpoAARateViolations rateViolation = null;
                                                    if (svcViolationCode != null)
                                                    {
                                                        rateViolation =
                                                            new XPCollection<Support.DataObjects.XpoAARateViolations>(uow,
                                                                    CriteriaOperator.Parse("Code = ?",
                                                                        svcViolationCode.AAICCode))
                                                                .FirstOrDefault();
                                                        if (rateViolation == null)
                                                        {
                                                            _emailService.SendEmailNotification(
                                                                new List<string>() { "technical.support@palminsure.com" },
                                                                "AAICCode not found for violation.",
                                                                $"CustomCode: {violation.CustomCode} - Description: {svcViolationCode?.DESCRIPTION} - Points: {violation.CustomPoints}",
                                                                false);
                                                        }
                                                    }


                                                    DateTime violationDate = DateTime.MinValue;
                                                    if (!string.IsNullOrEmpty(violation.ViolationDate))
                                                    {
                                                        violationDate = DateTime.ParseExact(violation.ViolationDate,
                                                            "yyyyMMdd", CultureInfo.InvariantCulture);
                                                    }
                                                    else if (!string.IsNullOrEmpty(violation.ConvictionDate))
                                                    {
                                                        violationDate = DateTime.ParseExact(violation.ConvictionDate,
                                                            "yyyyMMdd", CultureInfo.InvariantCulture);
                                                    }

                                                    int totalPoints = 0;
                                                    string violationGroup = "";
                                                    if (rateViolation != null)
                                                    {
                                                        violationGroup = rateViolation.ViolationGroup;
                                                        totalPoints = rateViolation.PointsFirst;
                                                    }

                                                    string aaicCode = "";
                                                    string desc = violation.ViolationType;
                                                    if (svcViolationCode != null)
                                                    {
                                                        aaicCode = svcViolationCode.AAICCode;
                                                        desc = svcViolationCode.DESCRIPTION;
                                                    }

                                                    if (!string.IsNullOrEmpty(aaicCode) && aaicCode.Equals("ACC001"))
                                                    {
                                                        if (currentViolationsForQuote != null && currentViolationsForQuote.Any(v => v.FromAPlus &&
                                                            v.ViolationDate.Date.Equals(violationDate.Date)))
                                                        {
                                                            aaicCode = "EXC100";
                                                            violationGroup = "EXCEPTION";
                                                        }
                                                    }

                                                    if (violationDate.Date > now.Date)
                                                    {
                                                        totalPoints = 0;
                                                    }


                                                    isViolationFromAnyDriverDBMatch = mvrViolations.Any(y => y.ViolationCode.Equals(aaicCode)
                                                                 && y.ViolationDate == violationDate && y.ViolationGroup.Equals(violationGroup));

                                                    if (isViolationFromAnyDriverDBMatch)
                                                        continue;


                                                    isSameViolationExistForCurrDriver = driverViolationsModel.DriversViolations.Any(y => y.ViolationCode != null && y.ViolationCode.Equals(aaicCode)
                                                                 && y.ViolationDate == violationDate && y.ViolationGroup.Equals(violationGroup));

                                                    if (isSameViolationExistForCurrDriver)
                                                        continue;

                                                    int indexNo = currentViolationsForQuote.Count + 1;

                                                    PolicyQuoteDriversViolations newDriverViolation =
                                                        new PolicyQuoteDriversViolations(uow)
                                                        {
                                                            PolicyNo = quoteNo,
                                                            DriverIndex = driver.DriverIndex,
                                                            ViolationPoints = totalPoints,
                                                            FromMVR = true,
                                                            Chargeable = true,
                                                            ViolationCode = aaicCode,
                                                            ViolationGroup = violationGroup,
                                                            ViolationDate = violationDate,
                                                            ViolationDesc = desc,
                                                            ViolationNo = indexNo,
                                                            ViolationIndex = indexNo
                                                        };


                                                    foreach (PolicyQuoteDriversViolations polViolation in driverViolationsModel.DriversViolations)
                                                    {
                                                        if (polViolation.DriverIndex == newDriverViolation.DriverIndex && polViolation.PolicyNo == (newDriverViolation.PolicyNo)
                                                          && polViolation.ViolationDate == newDriverViolation.ViolationDate && polViolation.ViolationCode.Equals(newDriverViolation.ViolationCode)
                                                          && polViolation.ViolationGroup.Equals(newDriverViolation.ViolationGroup))
                                                        {
                                                            isSameViolationExist = true;
                                                        }

                                                        if (isSameViolationExist)
                                                            break;
                                                    }

                                                    if (isSameViolationExist)
                                                        continue;

                                                    driverViolationsModel.DriversViolations.Add(newDriverViolation);
                                                    violations.Add(driverViolationsModel);
                                                    DateTime threeYearsAgoFromEffectiveDate =
                                                        quote.EffDate.AddMonths(-36).Date;
                                                    if (violationDate >= threeYearsAgoFromEffectiveDate &&
                                                        !mvrViolationsPast36MonthsForDriver.Contains(
                                                            driverViolationsModel))
                                                    {
                                                        mvrViolationsPast36MonthsForDriver.Add(driverViolationsModel);
                                                    }

                                                    currentViolationsForQuote.Add(newDriverViolation);
                                                    newDriverViolation.Save();
                                                }

                                                vm.MvrDriverViolationsModels.Add(driverViolationsModel);
                                            }
                                            else
                                            {
                                                AjaxService.CreateClearMVRReportForQuotes(quoteNo, currentViolationsForQuote,
                                                    driver, vm, uow);
                                            }
                                        }
                                        else
                                        {
                                            AjaxService.CreateNoHitMVRForQuotes(quoteNo, currentViolationsForQuote, driver, vm,
                                                uow);
                                        }
                                    }
                                    else
                                    {
                                        AjaxService.CreateNoHitMVRForQuotes(quoteNo, currentViolationsForQuote, driver, vm, uow);
                                    }
                                }
                                else
                                {
                                    AjaxService.CreateClearMVRReportForQuotes(quoteNo, currentViolationsForQuote, driver, vm, uow);
                                }
                            }

                            AjaxService.HandleAAPointsForMVRViolations(quote.EffDate, mvrViolationsPast36MonthsForDriver);
                        }
                        else
                        {
                            AjaxService.CreateNoHitMVRForQuotes(quoteNo, currentViolationsForQuote, driver, vm, uow);
                        }
                    }
                    else
                    {
                        AjaxService.CreateNoHitMVRForQuotes(quoteNo, currentViolationsForQuote, driver, vm, uow);
                    }
                }
                uow.CommitChanges();
            }

        }

        public void MakeReportParams(Blowfish bfs, string formdesc, string policyno, int historyid)
        {
            bfs = new Blowfish(Constants.blowfishKey);
            switch (formdesc)
            {
                case "APPLICATION COVER":
                    _reportName = @"/Underwriting/Application Cover";

                    listParamNames.Add("PolicyNo");
                    listParamNames.Add("CompanyID");

                    listParamValues.Add(String.Format("{0}", policyno));
                    listParamValues.Add("AGIC");
                    break;

                case "APPLICATION FOR INSURANCE":
                    _reportName = @"/Underwriting/Application For Insurance";

                    listParamNames.Add("PolicyNo");
                    listParamNames.Add("ApplicationReportLevel");
                    listParamNames.Add("CompanyID");

                    listParamValues.Add(String.Format("{0}", policyno));
                    listParamValues.Add("POLICY");
                    listParamValues.Add("AGIC");

                    break;

                case "STATEMENT OF NON-BUSINESS USE":
                    _reportName = @"/Underwriting/Forms/Statement of Non-Business Use";

                    break;

                case "DRIVER AND HOUSEHOLD RESIDENT DISCLOSURE":
                    _reportName = @"/Underwriting/Driver And Household Resident Disclosure";

                    listParamNames.Add("PolicyNo");

                    listParamValues.Add(String.Format("{0}", policyno));
                    break;

                case "NAMED DRIVER EXCLUSION":
                    _reportName = @"/Underwriting/Named Driver Exclusion";

                    listParamNames.Add("PolicyNo");

                    listParamValues.Add(String.Format("{0}", policyno));
                    break;

                case "NEW BUSINESS DEC PAGE":
                    _reportName = @"/Underwriting/Policy Declarations";

                    listParamNames.Add("PolicyHistoryIndex");
                    listParamNames.Add("ReportType");
                    listParamNames.Add("CompanyID");

                    listParamValues.Add(String.Format("{0}_{1}", policyno, historyid));
                    listParamValues.Add("NEW");
                    listParamValues.Add("AGIC");
                    break;
                #region INVOICE
                //case "INVOICE":
                //    reportName = @"/Underwriting/Installment Invoice";
                //    
                //    listParamNames.Add("PolicyNo");
                //    listParamNames.Add("CompanyID");
                //    listParamNames.Add("MailDate");
                //    listParamNames.Add("Action");
                //    listParamNames.Add("CancelDate");
                //    
                //    listParamValues.Add(policyno);
                //    listParamValues.Add("AGIC");
                //    listParamValues.Add(String.Format("{0}", DateTime.Now));
                //    listParamValues.Add("INVOICE");
                //    listParamValues.Add(UtilitiesDatabaseObj.getCancelDate(String.Format("{0}", policyno)).ToString());
                //    
                //    try
                //    {
                //        UtilitiesStoredProcedure.ExecspUW_InstallmentsUpdatePrinting(SessionObj, policyno, DateTime.Now, UserInfo.Instance.Username, Guid.NewGuid().ToString());
                //    }
                //    catch (Exception ex)
                //    {
                //        log.Error(String.Format("[RUN STORED PROC] Error occurred in Installments Update Printing Stored Proc: {0}", ex));
                //    }
                //    break;
                #endregion

                case "EFT DOCUMENT":
                    _reportName = @"/Underwriting/EFT";

                    listParamNames.Add("PolicyNo");
                    listParamNames.Add("CompanyID");
                    listParamNames.Add("AccountType");
                    listParamNames.Add("AccountNo");
                    listParamNames.Add("ABA");

                    listParamValues.Add(String.Format("{0}", policyno));
                    listParamValues.Add("AGIC");
                    listParamValues.Add(bfs.Decrypt_CBC(DbHelperUtils.getScalarValue("PolicyBankInfo",
                        "AccountType", "PolicyNumber", policyno)));
                    listParamValues.Add(bfs.Decrypt_CBC(DbHelperUtils.getScalarValue("PolicyBankInfo",
                        "AccountNumber", "PolicyNumber", policyno)));
                    listParamValues.Add(bfs.Decrypt_CBC(
                        DbHelperUtils.getScalarValue("PolicyBankInfo", "ABA", "PolicyNumber", policyno)));
                    break;

                case "MVR":
                    _reportName = @"/Underwriting/MVR";

                    listParamNames.Add("ListDriverLicense");

                    List<string> listDLNumbers = new List<string>();
                    XPCollection getPolicyDriverInfo = new XPCollection(typeof(PolicyDrivers),
                        CriteriaOperator.Parse("PolicyNo = ?", policyno));
                    foreach (PolicyDrivers DLNumbers in getPolicyDriverInfo)
                    {
                        listDLNumbers.Add(DLNumbers.LicenseNo.Replace("-", ""));
                    }

                    string DLNumbersConcat = "";
                    int DLNumbersCheckMutipleCount = listDLNumbers.Count - 1;
                    for (int i = 0; i < listDLNumbers.Count; i++)
                    {
                        DLNumbersConcat = DLNumbersCheckMutipleCount > i
                            ? listDLNumbers[i] + "@" + DLNumbersConcat
                            : DLNumbersConcat + listDLNumbers[i];
                    }

                    listParamValues.Add(String.Format("{0}", DLNumbersConcat));
                    break;

                case "INSURANCE ID CARD":
                    _reportName = @"/Underwriting/ID Cards";

                    listParamNames.Add("PolicyNo");
                    listParamNames.Add("CompanyID");

                    listParamValues.Add(String.Format("{0}", policyno));
                    listParamValues.Add("AGIC");
                    break;

                //case "UM OPTION NOTICE":
                //    reportName = @"/Underwriting/UM Option Notice";
                //    
                //    listParamNames.Add("PolicyNo");
                //    listParamNames.Add("CompanyID");
                //    
                //    listParamValues.Add(String.Format("{0}", policyno));
                //    listParamValues.Add("AGIC");
                //    break;

                case "PAYMENT RECEIPT":
                    _reportName = @"/Underwriting/Payment Receipt";

                    listParamNames.Add("PolicyNo");
                    listParamNames.Add("Show");

                    listParamValues.Add(String.Format("{0}", policyno));
                    listParamValues.Add("ALL");
                    break;

                case "PAYMENT SCHEDULE":
                    _reportName = @"/Underwriting/Payment Schedule";

                    listParamNames.Add("PolicyNo");
                    listParamNames.Add("CompanyID");

                    listParamValues.Add(String.Format("{0}", policyno));
                    listParamValues.Add("AGIC");
                    break;

                case "SR22":
                    XPCollection getSR22Info = new XPCollection(typeof(PolicyDrivers),
                        CriteriaOperator.Parse("PolicyNo = ? AND SR22 = 1", policyno));
                    if (getSR22Info.Count > 0)
                    {
                        _reportName = @"/Underwriting/SR22";

                        listParamNames.Add("PolicyNo");
                        listParamValues.Add(policyno);
                    }
                    break;

                default:
                    //MessageBox.Show("Report not available.");ahj
                    break;
            }
        }
    }
}