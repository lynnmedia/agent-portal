﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Repositories;
using AgentPortal.Repositories.Xpo;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using AlertAuto.Integrations.Contracts.CardConnect;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities;
using Blowfish = AgentPortal.Support.Utilities.Blowfish;

namespace AgentPortal.Services
{
    public class PaymentService
    {
        //services
        private readonly IWebHostService _webHostService;
        private readonly IEmailService _emailService;
        private readonly QuoteService QuoteService;
        //utility
        private readonly IValidationDictionary _validationDictionary;
        private readonly IPalmsLogger _logUtils;

        private readonly ISessions _sessions;

        //integration
        private readonly IPalmsService _palmsService;
        private readonly IIntegrationsClient _integrationsClient;
        //repos
        private readonly IPolicyRepository _policyRepository;
        private readonly IQuoteRepository _quoteRepository;
        private readonly IEndorsementRepository _endorsementRepository;
        private readonly IPaymentRepository _paymentRepository;

        public PaymentService(
            IValidationDictionary validationDictionary,
            IWebHostService webHostService, 
            IEmailService emailService,
            IIntegrationsClient integrationsClient,
            IPalmsService palmsClient,
            IUnitOfWorkFactory unitOfWorkFactory,
            IPalmsLogger palmsLogger,
            ISessions sessions,
            QuoteService quoteService
        )
        {
            _validationDictionary = validationDictionary;
            _palmsService = palmsClient;

            _logUtils = palmsLogger;
            _sessions = sessions;
            _webHostService = webHostService;
            _emailService = emailService;
            _integrationsClient = integrationsClient;
        }

        public CardConnectPaymentModel CreateCardConnectPaymentModel(string quoteNo, decimal minimumPayAmt, bool isInitialDepostPayment = true,
            string agentESignEmailAddress = "", bool isEndorsementPayment = false, DateTime? endorsementEffectiveDate = null, bool isTitanEndorsementPayment = false,
            bool isRenewalPayment = false, bool isClientPayment = false, bool isPayForReinstatement = false)
        {
            CardConnectPaymentModel model = new CardConnectPaymentModel();
            model.PolicyNo = quoteNo;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                model.IsInitialDepostPayment = isInitialDepostPayment;
                if (isInitialDepostPayment)
                {
                    XpoPolicyQuote quote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo))
                        .FirstOrDefault();
                    model.Amount = minimumPayAmt;
                    model.CreditCardFee = minimumPayAmt * .02m;
                    model.Name = quote.Ins1FullName;
                    model.Address = quote.GarageStreet;
                    model.City = quote.GarageCity;
                    model.State = quote.GarageState;
                    model.Postal = quote.GarageZip;
                    model.IsEft = quote.EFT;

                    if (string.IsNullOrEmpty(quote.AgentESignEmailAddress) && !string.IsNullOrEmpty(agentESignEmailAddress))
                    {
                        quote.AgentESignEmailAddress = agentESignEmailAddress;
                        quote.Save();
                        uow.CommitChanges();
                    }
                }
                else
                {
                    XpoPolicy xpoPolicy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo)).FirstOrDefault();
                    model.Amount = minimumPayAmt;
                    model.CreditCardFee = minimumPayAmt * .02m;
                    model.Name = xpoPolicy.Ins1FullName;
                    model.Address = xpoPolicy.GarageStreet;
                    model.City = xpoPolicy.GarageCity;
                    model.State = xpoPolicy.GarageState;
                    model.Postal = xpoPolicy.GarageZip;
                    model.IsEft = xpoPolicy.EFT;
                }

                if (isEndorsementPayment)
                {
                    model.IsEndorsementPayment = true;
                    model.EndorsementEffectiveDate = endorsementEffectiveDate;
                }
                else if (isTitanEndorsementPayment)
                {
                    model.IsTitanEndorsementPayment = true;
                    model.EndorsementEffectiveDate = endorsementEffectiveDate;
                }
                else if (isRenewalPayment)
                {
                    model.IsRenewalPayment = true;
                }
                else if (isClientPayment)
                {
                    model.IsClientPayment = true;
                }
                else if (isPayForReinstatement)
                {
                    model.IsPayForReinstatement = true;
                }

                model.IFramePort = ConfigSettings.ReadSetting("CardConnect_IFrame_Port");
            }
            return model;
        }

        public void CreatePaymentProfile(CardConnectPaymentModel model, string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string accountType = model.FutureEFTPayType.Equals("ACH") ? "ECHK" : "CC";
                CardConnectRequest profileRequestData = new CardConnectRequest()
                {
                    account = model.Account,
                    expiry = model.ExpirationDate,
                    cvv2 = model.Cvv,
                    name = model.Name,
                    address = model.Address,
                    city = model.City,
                    postal = model.Postal,
                    region = model.State,
                    accttype = accountType
                };
                var newProfileId = _integrationsClient.CreateCardConnectProfile(policyNo, profileRequestData);

                //Save profile
                if (!string.IsNullOrEmpty(newProfileId))
                {
                    XpoPolicyPaymentProfile newProfile = new XpoPolicyPaymentProfile(uow)
                    {
                        PolicyNo = policyNo,
                        ProfileId = newProfileId,
                        AccountType = accountType,
                        CreatedByUserName = Sessions.Instance.Username,
                        CreatedDate = DateTime.Now,
                        LastUpdatedByUserName = Sessions.Instance.Username,
                        LastUpdatedDate = DateTime.Now
                    };

                    PolicyBankInfo existingPolicyBankInfo =
                        new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber = ? ", policyNo))
                            .FirstOrDefault();
                    if (existingPolicyBankInfo == null)
                    {
                        PolicyBankInfo newPolicyBankInfo = new PolicyBankInfo(uow);
                        newPolicyBankInfo.PolicyNumber = policyNo;
                        newPolicyBankInfo.Account = "INDIVIDUAL";
                        newPolicyBankInfo.AccountNumber = model.ExtractLast4OfAccount();
                        if (accountType.Equals("ECHK"))
                        {
                            newPolicyBankInfo.AccountType = model.AchAcocuntType;
                        }
                        else
                        {
                            newPolicyBankInfo.AccountType = "CreditCard";
                        }

                        newPolicyBankInfo.Save();
                    }

                    newProfile.Save();
                    uow.CommitChanges();
                }
                else
                {
                    // generate notification of profile failure
                    _emailService.SendPaymentProfileNotCreatedEmail(policyNo);
                }
            }
        }

        public CardConnectRequest CreateCardConnectRequest(CardConnectPaymentModel model)
        {
            CardConnectRequest requestData = new CardConnectRequest();
            requestData.account = model.Account;
            requestData.expiry = model.ExpirationDate;
            requestData.cvv2 = model.Cvv;
            requestData.name = model.Name;
            requestData.address = model.Address;
            requestData.city = model.City;
            requestData.region = model.State;
            requestData.postal = model.Postal;
            if (model.IsAch)
            {
                requestData.accttype = "ECHK";
                requestData.amount = Math.Round(model.Amount, 2).ToString();
            }
            else if (model.IsCreditCard)
            {
                requestData.amount = Math.Round(model.Amount + model.CreditCardFee, 2).ToString();
            }

            return requestData;
        }

        public void ProcessRenewalPayment(RenQuoteInstallments getRenInstalls, PolicyRenQuote getRenQuote, double amtPrior, double renAmt, double modelAmt,
            string policyno, string agentcode, bool saveBankInfo, string amount, string policyType)
        {
            //-------------------------------------
            //Process The Renewal Payment
            decimal renPymnt = getRenInstalls.DownPymtAmount;
            amtPrior = renPymnt.SafeString().Parse<double>() - modelAmt;
            renAmt = modelAmt + amtPrior;
            double totalboth = renPymnt.SafeString().Parse<double>() + modelAmt;

            DateTime now = DateTime.Now;
            string pymntBatchGuid = Guid.NewGuid().ToString();

            if (modelAmt > 0.0)
            {
                _logUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: SAVING RENEWAL PAYMENT INFORMATION TO PAYMENTS TABLE", policyno),
                    "INFO");

                AgentUtils.SweepAcct(agentcode, getRenInstalls.PolicyNo, renAmt, Sessions.Instance.Username,
                    saveBankInfo);
            }

            PortalUtils.AuditPayments(username: Sessions.Instance.Username,
                location: PymntLocation.AgentSweep,
                pymntType: PymntTypes.Renewal,
                totalAmt: totalboth,
                priorAmt: amtPrior,
                renAmt: renPymnt.SafeString().Parse<double>(),
                modelAmt: modelAmt,
                pymntGuid: pymntBatchGuid,
                isSweep: true,
                isFirstData: false,
                isRen: true);

            if (Math.Abs(amtPrior) > 1.0)
            {
                PortalPaymentUtils.PostToPayments(policyno: policyno,
                    paymentType: "MOV",
                    acctDate: now,
                    postmarkDate: now,
                    checkAmt: amtPrior * -1,
                    totalAmt: amtPrior * -1,
                    username: "system",
                    guid: pymntBatchGuid,
                    checkNotes: "MOVED FROM " + policyno,
                    transId: null,
                    responseCode: null);

                PortalPaymentUtils.PostToPayments(policyno: getRenQuote.PolicyNo,
                    paymentType: "MOV",
                    acctDate: now,
                    postmarkDate: now,
                    checkAmt: amtPrior,
                    totalAmt: amtPrior,
                    username: "system",
                    guid: pymntBatchGuid,
                    checkNotes: "MONEY MOVED TO " + getRenQuote.PolicyNo,
                    transId: null,
                    responseCode: null);
            }
        }

        public List<XpoAARatePayPlans> CreatePayPlan(XpoPolicyQuote quoteChk, List<XpoAARatePayPlans> listPayPlans, out bool isAtRiskTerritory)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                listPayPlans.Clear();
                isAtRiskTerritory = false;
                int intHasSr22 = 0;
                if (quoteChk.SR22Fee > 0)
                    intHasSr22 = 1;
                int onlyPIPPD = 0;
                if (quoteChk.PIPPDOnlyFee > 0)
                    onlyPIPPD = 1;
                PolicyQuoteDrivers getSr22 = new XPCollection<PolicyQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND SR22 = '1'", Sessions.Instance.PolicyNo)).FirstOrDefault();
                bool hasSr22 = getSr22 == null ? false : true;
                string currentRateCycle = DbHelperUtils.getMaxRecID("AARatePayPlans", "RateCycle");
                if (currentRateCycle == ConfigSettings.ReadSetting("CurrentRateCycle"))
                {
                    var currentTerritory =
                        uow.FindObject<XpoAARateTerritory>(CriteriaOperator.Parse("ZIP = ? AND RateCycle = ?",
                            quoteChk.GarageZip, currentRateCycle));
                    if (currentTerritory != null)
                    {
                        string supportAreaCode = currentTerritory.SupportArea;
                        string uwAreaCode = currentTerritory.UWArea;
                        XpoAARateSupUW uwAreaInfo = uow.FindObject<XpoAARateSupUW>(
                            CriteriaOperator.Parse("RateCycle = ? AND SupportArea = ? AND UWArea = ?",
                                currentRateCycle, supportAreaCode, uwAreaCode));
                        if (uwAreaInfo != null)
                        {
                            if (uwAreaInfo.DownPay.Equals(1) && uwAreaInfo.PIPCount.Equals(0))
                            {
                                isAtRiskTerritory = true;
                            }
                        }
                    }
                }

                XPCollection<PolicyQuoteCars> phyDamCars = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND HasPhyDam = '1'", quoteChk.PolicyNo));
                bool hasPhyDam = phyDamCars.Any(c =>
                    c.HasPhyDam && (!string.IsNullOrEmpty(c.CompDed) && c.CompDed != "NONE"));
                bool requires33PercentDown =
                    PortalUtils.AgentAndZipRequires33PercentDown(quoteChk.AgentCode, quoteChk.GarageZip,
                        hasPhyDam);
                bool requiresHighDownPayment = quoteChk.PolicyScore >= 20;
                XPCollection<XpoAARatePayPlans> getPayPlans =
                    new XPCollection<XpoAARatePayPlans>(uow);
                bool isAllowLowDp = PortalUtils.ZipHasLowerDownPayment(currentRateCycle, quoteChk.GarageZip);
                string term = "6"; //TODO: add support for 12 when needed
                if (isAllowLowDp && !requires33PercentDown && !requiresHighDownPayment)
                {
                    getPayPlans.Criteria =
                        CriteriaOperator.Parse("RateCycle = ? AND Term = ? AND BI = 0 AND NewOrRen <> 'R'",
                            currentRateCycle, term);
                }
                else
                {
                    getPayPlans.Criteria = CriteriaOperator.Parse(
                        "RateCycle = ? AND Term = ? AND BI = 0 AND NewOrRen <> 'R' AND (RequiresTerritoryApproval IS NULL OR RequiresTerritoryApproval = 0)",
                        currentRateCycle, term);
                }

                getPayPlans.Reload();
                foreach (XpoAARatePayPlans payPlan in getPayPlans.ToList())
                {
                    if (requiresHighDownPayment)
                    {
                        if (payPlan.DP_AsPerc.Equals(1))
                        {
                            if (!listPayPlans.Exists(pp => pp.DP_AsPerc.Equals(1)))
                            {
                                listPayPlans.Add(payPlan);
                            }
                        }
                    }
                    else if (requires33PercentDown)
                    {
                        if (requires33PercentDown && !requiresHighDownPayment)
                        {
                            if (payPlan.DP_AsPerc.Equals(0.332))
                            {
                                if (!listPayPlans.Exists(pp => pp.DP_AsPerc.Equals(0.332)))
                                {
                                    listPayPlans.Add(payPlan);
                                }
                            }

                            if (payPlan.DP_AsPerc.Equals(1))
                            {
                                if (!listPayPlans.Exists(pp => pp.DP_AsPerc.Equals(1)))
                                {
                                    listPayPlans.Add(payPlan);
                                }
                            }
                        }
                    }
                    else
                    {
                        listPayPlans.Add(payPlan);
                    }
                }

                //if our selected pay plan does not equal allowed list, then default it. 
                if ((requiresHighDownPayment || requires33PercentDown) &&
                    !listPayPlans.Exists(pp => pp.IndexID.Equals(quoteChk.PayPlan)))
                {
                    //TODO: update when we sell BI
                    XpoAARatePayPlans defaultPlan = null;
                    if (requiresHighDownPayment)
                    {
                        defaultPlan = listPayPlans.FirstOrDefault(pp => pp.DP_AsPerc.Equals(1));
                        if (defaultPlan != null && defaultPlan.DP_AsPerc.Equals(1))
                        {
                            if (!listPayPlans.Exists(pp => pp.DP_AsPerc.Equals(1)))
                            {
                                listPayPlans.Add(defaultPlan);
                            }
                        }
                    }
                    else if (requires33PercentDown)
                    {
                        if (requires33PercentDown && !requiresHighDownPayment)
                        {
                            defaultPlan = listPayPlans.FirstOrDefault(pp => pp.DP_AsPerc.Equals(0.332));
                            if (defaultPlan != null && defaultPlan.DP_AsPerc.Equals(0.332))
                            {
                                if (!listPayPlans.Exists(pp => pp.DP_AsPerc.Equals(0.332)))
                                {
                                    listPayPlans.Add(defaultPlan);
                                }
                            }

                            defaultPlan = listPayPlans.FirstOrDefault(pp => pp.DP_AsPerc.Equals(1));
                            if (defaultPlan != null && defaultPlan.DP_AsPerc.Equals(1))
                            {
                                if (!listPayPlans.Exists(pp => pp.DP_AsPerc.Equals(1)))
                                {
                                    listPayPlans.Add(defaultPlan);
                                }
                            }
                        }
                    }

                    quoteChk.PayPlan = defaultPlan?.IndexID ?? listPayPlans.FirstOrDefault().IndexID;
                    quoteChk.Save();
                    uow.CommitChanges();
                }

                //Check if quote has lower down pay plan
                if (!listPayPlans.Exists(pp => pp.IndexID.Equals(quoteChk.PayPlan)))
                {
                    XpoAARatePayPlans defaultPlan = listPayPlans.OrderBy(pp => pp.DP_AsPerc).FirstOrDefault();
                    quoteChk.PayPlan = defaultPlan?.IndexID ?? listPayPlans.FirstOrDefault().IndexID;
                    quoteChk.Save();
                    uow.CommitChanges();
                }

                return listPayPlans;
            }
        }

        public void HandlePaymentModel(MakePaymentModel model)
        {
            //////////////////////////////////////////////////////////
            DateTime today = DateTime.Now;
            DateTime minDuePayDate = PortalUtils.GetMinDuePayDate(model.PolicyNo);
            today = new DateTime(today.Year, today.Month, today.Day);
            minDuePayDate = new DateTime(minDuePayDate.Year, minDuePayDate.Month, minDuePayDate.Day);

            int compareResult = DateTime.Compare(today, minDuePayDate);

            bool allowPayLess = compareResult <= 0;

            if ((model.PaymentAmt < model.MinimumAmount) && (!allowPayLess))
            {
                model.PaymentAmt = model.MinimumAmount;
                _validationDictionary.AddError("PaymentAmt", "You cannot pay less than the minimum amount. The amount has been set to the minumum.");
            }
            //////////////////////////////////////////////////////////
            //model.PaymentAmt = 1;
            //model.MinimumAmount = 1;
            //////////////////////////////////////////////////////////
            if (model.PaymentAmt == 0)
            {
                _validationDictionary.AddError("PaymentAmt", "You cannot pay $0.00.");
                model.PaymentAmt = 1.01;
            }
            //////////////////////////////////////////////////////////
            _logUtils.Log(Sessions.Instance.Username, string.Format("{0}: PAYMENT PASSED AMOUNT VALIDATIONS", Sessions.Instance.PolicyNo), "INFO");

            _logUtils.Log(Sessions.Instance.Username, string.Format("{0}: COMPLETED LOADING PAYMENT DATA", Sessions.Instance.PolicyNo), "INFO");
        }

        public PaymentResult ProcessPayment(CardConnectPaymentModel model, bool savePaymentProfile)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));
            if (model.IsAgentAccountSweep) throw new InvalidOperationException("Process payment does not handle agent sweep");
            if (model.Amount <= 0) throw new InvalidOperationException("Amount must be greater than zero");
            if (model.Account != null && !Regex.IsMatch(model.Account,"^[\\d\\s]+$")) throw new InvalidOperationException("Invalid account number");
            if (model.IsCreditCard)
            {
                if (model.Cvv.IsNullOrEmpty()) throw new Exception($"{nameof(model.Cvv)} is required for credit card payment");
                if (!Regex.IsMatch(model.Cvv,"^[\\d\\s]+$")) throw new InvalidOperationException("Invalid security code");
                if (model.ExpirationDate.IsNullOrEmpty()) throw new Exception($"{nameof(model.ExpirationDate)} required for credit card payment");
                if (!Regex.IsMatch(model.ExpirationDate,"^[\\d]{1,2}/[\\d]{2,4}$")) throw new InvalidOperationException("Invalid expiration date");
            }
            var requestData = CreateCardConnectRequest(model);

            var response = _integrationsClient.AuthorizeWithCardConnect(model.PolicyNo, requestData, savePaymentProfile);
            var result = new PaymentResult();
            if (response != null && response.respstat.Equals(ResponseCodes.APPROVED))
            {
                result.Success = true;
                result.Amount = requestData.amount;

                if (savePaymentProfile)
                {
                    if (!string.IsNullOrEmpty(response.profileid))
                    {
                        SavePaymentProfile(model, model.PolicyNo, response);
                    }
                    else
                    {
                        // generate notification of profile failure
                        _logUtils.Log(_sessions.Username, $"Profile Creation Error, Policy#: {_sessions.PolicyNo}. Time: {DateTime.Now}", "info");
                        _emailService.SendPaymentProfileNotCreatedEmail(model.PolicyNo);
                    }
                }
                _paymentRepository.WriteCardConnectResponseToPaymentTables(model.PolicyNo, requestData, response, model);
                var fileName = Path.GetFileName(_palmsService.GeneratePaymentReceipt(model.PolicyNo, true));
                var fileLink = string.Format(ConfigSettings.ReadSetting("PolicyDocsBaseUrl") + @"/Files/{0}", fileName);
                result.ReceiptLink = fileLink;
                return result;
            }
            result.Success = false;
            result.FailureReason = response?.resptext;
            return result;
        }

        public bool HasPolicyViolations(string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var violations =
                    new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                if (violations.Any())
                {
                    if (!violations.Any(v => v.FromMVR) || !violations.Any(v => v.FromAPlus))
                    {
                        Sessions.Instance.RatingErrMsg =
                            @"<div class='error'>Not all underwriting reports were found and this policy cannot be bound. Please contact underwriting@palminsure.com with any questions.</div>";
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    Sessions.Instance.RatingErrMsg =
                        @"<div class='error'>Not all underwriting reports were found and this policy cannot be bound. Please contact underwriting@palminsure.com with any questions.</div>";
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        private void SavePaymentProfile(CardConnectPaymentModel model, string policyNo, CardConnectResponse response)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var newProfile = new XpoPolicyPaymentProfile(uow);
                newProfile.PolicyNo = policyNo;
                newProfile.ProfileId = response.profileid;
                if (model.IsCreditCard)
                {
                    newProfile.AccountType = "CC";
                    var existingPolicyBankInfo =
                        new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber = ? ", policyNo))
                            .FirstOrDefault();
                    if (existingPolicyBankInfo == null)
                    {
                        var newPolicyBankInfo = new PolicyBankInfo(uow);
                        newPolicyBankInfo.PolicyNumber = policyNo;
                        newPolicyBankInfo.Account = "INDIVIDUAL";
                        newPolicyBankInfo.AccountNumber = model.ExtractLast4OfAccount();
                        newPolicyBankInfo.AccountType = "CreditCard";
                        newPolicyBankInfo.Save();
                    }
                }
                else if (model.IsAch)
                {
                    newProfile.AccountType = "ECHK";
                    var existingPolicyBankInfo =
                        new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber = ? ", policyNo))
                            .FirstOrDefault();
                    if (existingPolicyBankInfo == null)
                    {
                        var newPolicyBankInfo = new PolicyBankInfo(uow);
                        newPolicyBankInfo.PolicyNumber = policyNo;
                        newPolicyBankInfo.Account = "INDIVIDUAL";
                        newPolicyBankInfo.AccountNumber = model.ExtractLast4OfAccount();
                        newPolicyBankInfo.AccountType = model.AchAcocuntType;
                        newPolicyBankInfo.Save();
                    }
                }

                newProfile.CreatedByUserName = Sessions.Instance.Username;
                newProfile.CreatedDate = DateTime.Now;
                newProfile.LastUpdatedByUserName = Sessions.Instance.Username;
                newProfile.LastUpdatedDate = DateTime.Now;
                newProfile.Save();
                uow.CommitChanges();
            }
        }

        public bool HasExistingPayment(CardConnectPaymentModel model)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var amountToTest = Convert.ToDouble(model.Amount);
                //Agent Sweep payment
                IList<AuditACHTransactions> agentSweepPayments =
                    new XPCollection<AuditACHTransactions>(uow, CriteriaOperator.Parse("RecordKey = ?", model.PolicyNo));
                if (agentSweepPayments != null && agentSweepPayments.Any())
                    if (agentSweepPayments.Any(asp =>
                        asp.Date.Date.Equals(DateTime.Now.Date) && asp.Amt.Equals(amountToTest)))
                    {
                        _validationDictionary.AddError("InvalidPaymentData",
                            $"Unable to process payment. An agency sweep payment in the amount of {amountToTest.ToString("C")} was already made for this policy today. Please contact underwriting.");
                        {
                            return true;
                        }
                    }


                //Non-Agent Sweep Payment
                IList<Payments> payments =
                    new XPCollection<Payments>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.PolicyNo));
                if (payments != null && payments.Any())
                    if (payments.Any(p =>
                        p.CreateDate.Date.Equals(DateTime.Now.Date) &&
                        (p.TotalAmt - p.FeeAmt).Equals(amountToTest)))
                    {
                        _validationDictionary.AddError("InvalidPaymentData",
                            $"Unable to process payment. A payment in the amount of {amountToTest.ToString("C")} was already made for this policy today. Please contact underwriting.");
                        {
                            return true;
                        }
                    }

                return false;
            }
        }

        public bool ValidateCreditCardInfo(CardConnectPaymentModel model)
        {
            if (string.IsNullOrEmpty(model.Account) || string.IsNullOrEmpty(model.ExpirationDate) ||
                string.IsNullOrEmpty(model.Cvv) || model.ExpirationDate.Length != 4 || model.Cvv.Length > 4 ||
                model.Cvv.Length < 3 || model.Amount <= 0)
            {
                _validationDictionary.AddError("InvalidPaymentData", "You must enter valid credit card information.");
                {
                    return false;
                }
            }
            return true;
        }

        public bool ValidateACHInformation(CardConnectPaymentModel model)
        {
            if (string.IsNullOrEmpty(model.Account) || model.Amount <= 0)
            {
                _validationDictionary.AddError("InvalidPaymentData", "You must enter valid ACH information.");
                {
                    return false;
                }
            } 
            return true;
        }

        public bool ValidateAgentSweepPaymentInfo(CardConnectPaymentModel model)
        {
            if (string.IsNullOrEmpty(model.FutureEFTPayType))
            {
                _validationDictionary.AddError("InvalidPaymentData",
                    "An EFT payment plan requires you to enter valid Credit Card OR ACH information so it can be saved for future billing. Please select ACH or EFT.");
                {
                    return false;
                }
            }

            if (model.FutureEFTPayType.Equals("Credit Card") &&
                (string.IsNullOrEmpty(model.Account) || string.IsNullOrEmpty(model.ExpirationDate) ||
                 string.IsNullOrEmpty(model.Cvv) || model.ExpirationDate.Length != 4 ||
                 model.Cvv.Length > 4 || model.Cvv.Length < 3))
            {
                _validationDictionary.AddError("InvalidPaymentData",
                    "An EFT payment plan requires you to enter valid Credit Card OR ACH information so it can be saved for future billing. Please enter valid credit card information.");
                {
                    return false;
                }
            }

            if (model.FutureEFTPayType.Equals("ACH") && string.IsNullOrEmpty(model.Account))
            {
                _validationDictionary.AddError("InvalidPaymentData",
                    "An EFT payment plan requires you to enter valid Credit Card OR ACH information so it can be saved for future billing. Please enter valid ACH information.");
                {
                    return false;
                }
            }

            return true;
        }

        public bool AgentHasAccess(CardConnectPaymentModel model)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var policyNo = model.PolicyNo;
                if (model.IsInitialDepostPayment)
                {
                    var quoteInfoForAgent =
                        uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                    if (quoteInfoForAgent == null) return false;
                    if (!quoteInfoForAgent.AgentCode.IsNullOrEmpty() && quoteInfoForAgent.AgentCode.Equals(Sessions.Instance.AgentCode))
                    {
                        return true;
                    }
                }
                else
                {
                    var policyInfoForAgent =
                        uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                    if (policyInfoForAgent == null) return false;
                    if (!policyInfoForAgent.AgentCode.IsNullOrEmpty() && policyInfoForAgent.AgentCode.Equals(Sessions.Instance.AgentCode))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public XpoPolicyPaymentProfile GetPolicyPaymentProfiles(string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var existingProfile =
                    new XPCollection<XpoPolicyPaymentProfile>(uow, CriteriaOperator.Parse("PolicyNo = ?",
                        policyNo)).FirstOrDefault();
                return existingProfile;
            }
        }

        public void AgentSweepRedirectMsg(Blowfish bfs, TempDataDictionary TempData, string policyno, string policyType, ref string successMsg,
            ref string continueMsg, ref string msg)
        {
            switch (policyType)
            {
                case "QUOTE":
                    _logUtils.Log(Sessions.Instance.Username, string.Format("{0}: SWEEP MADE FROM QUOTE", policyno),
                        "INFO");
                    //successMsg = string.Format("Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype=ACH'>here</a> for your receipt", policyno);
                    continueMsg = string.Format(
                        "<div class='success'>Payment Successful. Please click the button below to issue this policy.<br/><br/><a class='btnSubmit' href='/Quote/GeneratePolicyDocs/{0}'>Get Policy Documents</a></div>",
                        bfs.Encrypt_CBC(policyno));
                    msg = "SUCCESS";
                    break;
                case "RENEWAL":
                    _logUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: SWEEP MADE FROM RENEWAL POLICY", policyno), "INFO");
                    msg = "SUCCESS";
                    //successMsg = string.Format("Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype=ACH'>here</a> for your receipt", policyno);
                    continueMsg = string.Format(
                        "<div class='success'>Payment Successful. Please click the button below to issue this renewal.<br/><br/><a class='btnSubmit' href='/Renewal/GeneratePolicyDocs/{0}'>Get Renewal Documents</a></div>",
                        bfs.Encrypt_CBC(policyno));
                    break;
                case "ENDORSEMENT":
                    _logUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: SWEEP MADE FROM ENDORSE POLICY", policyno), "INFO");
                    msg = "SUCCESS";
                    successMsg =
                        string.Format(
                            "Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype=ACH'>here</a> for your receipt",
                            policyno);
                    continueMsg = string.Format(
                        "<div class='success'>Payment Successful. Please click the button below to issue this endorsement.<br/><br/><a class='btnSubmit' href='/Agent/IssueEndorsementDetails/?policyNo={0}&effDate={1}&diffAmount=0&fromPaymentScreen={2}'>Get Documents and Issue Endorsement</a></div>",
                        policyno, Convert.ToDateTime(TempData["EndorsementEffDate"]).Date.ToString("MM-dd-yyyy"), 1);
                    break;
                default:
                    _logUtils.Log(Sessions.Instance.Username, string.Format("{0}: SWEEP MADE FROM POLICY", policyno),
                        "INFO");
                    msg = "SUCCESS";
                    successMsg =
                        string.Format(
                            "Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype=ACH'>here</a> for your receipt",
                            policyno);
                    continueMsg =
                        string.Format(
                            "<div class='success'>Payment Successful<br/><br/><a class='btnSubmit' href='/Agent/Policy/{0}'>Go Back To Policy</a></div>",
                            policyno);
                    break;
            }
        }

        public XpoPolicyQuote RateDriver(List<RatingInstallmentsDO> listInstallments, out DataSet data)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicyQuote quote = new XPCollection<XpoPolicyQuote>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo)).FirstOrDefault();
                quote.Reload();
                if (quote == null)
                {
                    _logUtils.Log(Sessions.Instance.Username,
                        string.Format("Quote Not Found: {0}".ToUpper(), Sessions.Instance.PolicyNo), "FATAL");
                    throw new HttpException(404, "Quote not found");
                }

                Sessions.Instance.RatingErrMsg = quote.RatingID == Sessions.Instance.PrevRatingId
                    ? "Error Code 989: There appears to have been an error. Please click retry b"
                    : string.Empty;
                Sessions.Instance.RatingErrMsg = "";
                //DataSet data = PalmsClient.GetQuoteCoverageInfo(Sessions.Instance.PolicyNo);
                data = QuoteService.GetQuoteCoverageInfo(Sessions.Instance.PolicyNo);
                string termMonths = quote.SixMonth ? "6" : "12";
                _logUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: QUOTE TERM MONTHS {1}", Sessions.Instance.PolicyNo, termMonths), "DEBUG");

                XPCollection<XpoPolicyQuote> getQuote = new XPCollection<XpoPolicyQuote>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                getQuote.Reload();
                XpoPolicyQuote polQuote = getQuote.FirstOrDefault();
                //polQuote.DBSetupFee + polQuote.FHCFFee + polQuote.MVRFee + polQuote.PolicyFee + polQuote.SR22Fee;
                double feeTotal = polQuote.MaintenanceFee + polQuote.PIPPDOnlyFee + polQuote.MVRFee +
                                  polQuote.PolicyFee + polQuote.SR22Fee;

                Guid installGuid = Guid.NewGuid();
                //PalmsClient.GetProjectedInstallments(termMonths.Parse<int>(), polQuote.EffDate, polQuote.PayPlan.ToString(), polQuote.PolicyWritten.ToString(), feeTotal.ToString(), installGuid);
                uow.ExecuteSproc("spUW_GetListProjectedInstallments",
                    new OperandValue(polQuote.PolicyNo),
                    new OperandValue(polQuote.RatingID),
                    new OperandValue(polQuote.PayPlan.ToString()),
                    new OperandValue(installGuid));
                XPCollection<RatingInstallmentsDO> getInstallments =
                    new XPCollection<RatingInstallmentsDO>(uow, CriteriaOperator.Parse("GUID = ?", installGuid));
                foreach (RatingInstallmentsDO installment in getInstallments)
                {
                    listInstallments.Add(installment);
                }

                return polQuote;
            }
           
        }
    }
}