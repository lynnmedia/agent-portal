﻿using AgentPortal.Models;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using log4net;
using PalmInsure.Palms.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AgentPortal.Repositories;
using Blowfish = AgentPortal.Support.Utilities.Blowfish;
using PolicyCarLienHolders = AgentPortal.Support.DataObjects.PolicyCarLienHolders;

namespace AgentPortal.Services
{
    public class QuoteService
    {
        private IUnitOfWorkFactory UnitOfWorkFactory { get; }
        private List<string> listPDFs = new List<string>();
        private List<string> listParamNames = new List<string>();
        private List<string> listParamValues = new List<string>();
        private string reportName = null;
        private readonly log4net.ILog quoteControllerLog = LogManager.GetLogger("QuoteControllerLogger");

        public QuoteService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            UnitOfWorkFactory = unitOfWorkFactory;
        }
        public QuoteModel CreateQuote()
        {
            var model = new QuoteModel
            {
                QuoteId = DbHelperUtils.GenerateQuoteNumber(),
                EffectiveDate = DateTime.Today,
                ExpirationDate = DateTime.Today.AddMonths(6),
                AgentCode = Sessions.Instance.AgentCode,
                QuotedDate = DateTime.Now,
                IsShow = true,
                SameAsGarage = true,
                Workloss = true,//LF 3/15/19 aa
                WorkLossGroup = 2//LF 3/15/19 aa
            };
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                uow.QuoteRepository.CreateQuote(model);
                uow.Save();
                return model;
            }
        }

        public QuoteModel CreateQuoteCars(XpoPolicyQuote quote, string languagePreference, string compDed, string collDed, string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                 //----------------------
                XPCollection<PolicyQuoteCars> cars =
                    new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", quote.PolicyNo));
                XPCollection<PolicyQuoteDrivers> drivers =
                    new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", quote.PolicyNo));
                QuoteModel model = UtilitiesRating.MapXpoToModel(quote);
                model.InsuredPreferredLanguage = languagePreference;
                model.Vehicles = new List<VehicleModel>();
                model.Drivers = new List<DriverModel>();
                //----------------------
                for (int i = 0; i < cars.Count; i++)
                {
                    // if(i == 0)
                    {
                        if (cars[i].HasPhyDam)
                        {
                            compDed = cars[i].CompDed;
                            collDed = cars[i].CollDed;
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("Quote {0} has PhyDam: Comp Ded: {1}, Coll Ded: {2}".ToUpper(), id,
                                    compDed, collDed), "FATAL");
                        }
                    }
                    model.Vehicles.Add(UtilitiesRating.MapXpoToModel(cars[i]));
                }

                foreach (PolicyQuoteDrivers driver in drivers)
                {
                    model.Drivers.Add(UtilitiesRating.MapXpoToModel(driver));
                }

                //----------------------
                model.CompDed = compDed;
                model.CollDed = collDed;

                model.InfinityRatingRelativities = uow.ExecuteQuery(string.Format(
                    @"SELECT * FROM RatingRelativities where MainIndexID={0} order by VehicleNo,RatedOperatorNo,RateOrderNo",
                    quote.RatingID));
                if (model.CreditScore == 0)
                    model.CreditScore = 700;
                //----------------------
                if (Sessions.Instance.IsNewQuote)
                {
                    /*
                                        //model.Workloss      = true;
                                        model.Workloss = false; // null;   changed 9/2018  to default WorkLoss to No and WorkLossGroup to None
                                        model.WorkLossGroup = 3; // 2;
                                        model.NIO = false;   //added 9/28/2018 LF
                                        model.NIRR = false;  //added 9/28/2018 LF
                                        model.SameAsGarage  = true; */
                    //LF 3/15/19 changed for Alert Auto
                    model.Workloss = true;
                    model.WorkLossGroup = 2;
                    model.NIO = false;
                    model.NIRR = true;
                }

                quote.Bridged = string.IsNullOrEmpty(quote.FromRater) ? false : true;
                quote.Save();
                uow.CommitChanges();

                return model;
            }
        }

        public void EditQuote(UnitOfWork uow, VehicleModel model, QuoteModel quoteModel, FormCollection formCollection, TempDataDictionary tmp)
        {
            XPCollection<PolicyQuoteCars> getCars = new XPCollection<PolicyQuoteCars>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?",
                            Sessions.Instance.PolicyNo,
                            model.CarIndex));
            PolicyQuoteCars car = getCars.FirstOrDefault();
            if (model.VIN?.ToLower() != car.VIN?.ToLower())
            {
                IntegrationsUtility.UpdateFlagForAllIntegrationCall(Sessions.Instance.PolicyNo, false, true, false);
            }
            getCars.Remove(car);
            car.PolicyNo = Sessions.Instance.PolicyNo;

            UtilitiesRating.MapModelToXpo(model, car);
            car.Save();
            uow.CommitChanges();

            getCars.Add(car);

            //XPCollection<PolicyCarLienHolders> getLienHolders = new XPCollection<PolicyCarLienHolders>(uow,
            //    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo,
            //        model.CarIndex));

            if (!string.IsNullOrEmpty(model.GrossVehicleWeight) || !string.IsNullOrEmpty(model.ClassCode))
            {
                XPCollection<PolicyQuoteCars> allCarsForQuote = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                if (allCarsForQuote.Any())
                {
                    foreach (PolicyQuoteCars quoteCar in allCarsForQuote)
                    {
                        if (quoteCar.CarIndex == model.CarIndex)
                        {
                            quoteCar.GrossVehicleWeight = model.GrossVehicleWeight;
                            quoteCar.ClassCode = model.ClassCode;
                            quoteCar.Save();
                            uow.CommitChanges();
                        }
                    }
                }
            }

            model.LienHolders = SaveLienHolders(formCollection, car.OID, car.CarIndex, car.VIN, uow);

            quoteModel.Vehicles.Add(model);
            tmp["QuoteModel"] = quoteModel;
            tmp["VehicleModel"] = model;
            tmp["VehicleID"] = ""; //LF 11/2/18
            if (model.HasPhyDam)
            {
                XPCollection<PolicyQuoteCars> quoteCars = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND HasPhyDam = 1", Sessions.Instance.PolicyNo));
                if (quoteCars.Any())
                {
                    foreach (PolicyQuoteCars quoteCar in quoteCars)
                    {
                        if (quoteCar.CarIndex == model.CarIndex)
                        {
                            DateTime currentDate = DateTime.Now;
                            int sixYearsAgo = currentDate.AddYears(-6).Year;

                            if (model.Year < sixYearsAgo && model.EngineType != null &&
                                (model.EngineType.Equals("H") ||
                                 model.EngineType.Equals("E")))
                            {
                                quoteCar.EngineType = model.EngineType;
                                quoteCar.Save();
                                uow.CommitChanges();
                            }
                        }
                    }
                }
            }
        }

        public void DenyQuote(UnitOfWork uow, VehicleModel model, QuoteModel quoteModel, FormCollection formCollection, TempDataDictionary tmp)
        {
            LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: ADD NEW CAR TO QUOTE", Sessions.Instance.PolicyNo), "INFO");
            // Deny if there are too many drivers.
            XPCollection<PolicyQuoteCars> getCars = new XPCollection<PolicyQuoteCars>(uow,
                CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));

            if (getCars.Count > 5)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: HIT MAX CARS ALLOWED", Sessions.Instance.PolicyNo), "WARN");
                throw new HttpException(400, "One policy may only have 5 Cars maximum");
            }

            model.CarIndex = getCars.Count + 1;
            PolicyQuoteCars car = new PolicyQuoteCars(uow);
            car.CarIndex = getCars.Count + 1;
            UtilitiesRating.MapModelToXpo(model, car);
            car.Save();
            uow.CommitChanges();
            IntegrationsUtility.UpdateFlagForAllIntegrationCall(Sessions.Instance.PolicyNo, false, true, false);
            if (!string.IsNullOrEmpty(model.GrossVehicleWeight) || !string.IsNullOrEmpty(model.ClassCode))
            {
                XPCollection<PolicyQuoteCars> allCarsForQuote = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                if (allCarsForQuote.Any())
                {
                    foreach (PolicyQuoteCars quoteCar in allCarsForQuote)
                    {
                        if (quoteCar.CarIndex == model.CarIndex)
                        {
                            quoteCar.GrossVehicleWeight = model.GrossVehicleWeight;
                            quoteCar.ClassCode = model.ClassCode;
                            quoteCar.Save();
                            uow.CommitChanges();
                        }
                    }
                }
            }

            model.LienHolders = SaveLienHolders(formCollection, car.OID, car.CarIndex, car.VIN, uow);
            //XPCollection<PolicyCarLienHolders> getLienHolders = new XPCollection<PolicyCarLienHolders>(uow,
            //    CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo,
            //        0)); //LF 10/2/18 model.CarIndex));

            //foreach (PolicyCarLienHolders lien in getLienHolders)
            //{
            //    PolicyCarLienHolders LienHolder =
            //        uow.FindObject<PolicyCarLienHolders>(CriteriaOperator.Parse("ID = ?", lien.ID));
            //    LienHolderModel tmpModel = UtilitiesRating.MapXpoToModel(LienHolder);
            //    tmpModel.VehicleId = car.OID;
            //    if (model.LienHolders == null) //LF 10/2/2018
            //        model.LienHolders = new List<LienHolderModel>();
            //    model.LienHolders.Add(tmpModel);
            //    LienHolder.VIN = model.VIN;
            //    LienHolder.CarIndex = car.CarIndex;
            //    LienHolder.Save();
            //    uow.CommitChanges();
            //}

            getCars.Add(car);
            quoteModel.Vehicles.Add(model);
            tmp["QuoteModel"] = quoteModel;
            tmp["VehicleModel"] = model;
            tmp["VehicleID"] = ""; //LF 11/2/18

            if (model.HasPhyDam)
            {
                XPCollection<PolicyQuoteCars> quoteCars = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND HasPhyDam = 1", Sessions.Instance.PolicyNo));
                if (quoteCars.Any())
                {
                    foreach (PolicyQuoteCars quoteCar in quoteCars)
                    {
                        if (quoteCar.CarIndex == model.CarIndex)
                        {
                            DateTime currentDate = DateTime.Now;
                            int sixYearsAgo = currentDate.AddYears(-6).Year;

                            if (model.Year < sixYearsAgo && model.EngineType != null &&
                                (model.EngineType.Equals("H") ||
                                 model.EngineType.Equals("E")))
                            {
                                quoteCar.EngineType = model.EngineType;
                                quoteCar.Save();
                                uow.CommitChanges();
                            }
                        }
                    }
                }
            }
        }

        public void CreateQuotePolicy(QuoteModel model, string cycle)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                //------------------------------
                XpoPolicyQuote quote =
                    new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.QuoteId))
                        .FirstOrDefault();
                //	AARatePayPlans payplan = new XPCollection<AARatePayPlans>(uow, CriteriaOperator.Parse("RateCycle = ? AND DownPayment = ? AND PolicyTerm = ?", cycle, "1", model.PolicyTermMonths)).FirstOrDefault();
                Support.DataObjects.XpoAARatePayPlans payplan = new XPCollection<Support.DataObjects.XpoAARatePayPlans>(
                    uow,
                    CriteriaOperator.Parse("RateCycle = ? AND DP_AsPerc = ? AND Term = ?", cycle, "1",
                        model.PolicyTermMonths)).FirstOrDefault();
                //------------------------------
                if (quote == null)
                {
                    quote = new XpoPolicyQuote(uow);
                }

                //-------------------------------
                model.PayPlan = quote.PayPlan == 0 ? payplan.IndexID : quote.PayPlan;
                model.AgentCode = AgentUtils.GetCorrectAgentCode(Sessions.Instance.PolicyNo);
                model.QuotedDate = quote.QuotedDate;
                model.IsShow = quote.IsShow;
                model.PriorCompany = quote.PreviousCompany;
                model.PreviousExpDate = quote.PreviousExpDate;
                model.Workloss = quote.WorkLoss == 1 ? true : false;
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Saving Quote For Agent {0}".ToUpper(), model.AgentCode), "DEBUG");
                UtilitiesRating.MapModelToXpo(model, quote);
                quote.IsEditedPalms = true;
                quote.Save();
                uow.CommitChanges();
                /*XPCollection<PolicyQuoteCars> getCarsData = new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));
                foreach (PolicyQuoteCars car in getCarsData)
                {
                    PolicyQuoteCars updateCars = uow.FindObject<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo, car.CarIndex));
    
                    if (car.HasPhyDam)
                    {
                        updateCars.CompDed = model.CompDed;
                        updateCars.CollDed = model.CollDed;
                        LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: Saving Comp {1} and Coll {2} Deductibles".ToUpper(), Sessions.Instance.PolicyNo, model.CompDed, model.CollDed), "DEBUG");
                        updateCars.Save();
                        uow.CommitChanges();
                    }
                }*/
            }
        }

        public QuestionnaireNewModel CreateQuestionnaireModel(string id, Blowfish bfs)
        {
            QuestionnaireNewModel model = new QuestionnaireNewModel();
            //------------------------------
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("VIEWING QUESTIONNAIRE FOR {0}", Sessions.Instance.PolicyNo), "INFO");
            using (UnitOfWork uow = new DevExpress.Xpo.UnitOfWork())
            {
                //PolicyQuoteQuestionnaire getQuote =
                //    new XPCollection<PolicyQuoteQuestionnaire>(uow,
                //        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo)).FirstOrDefault();

                PolicyQuoteQuestionnaire getQuote =
                    new XPCollection<PolicyQuoteQuestionnaire>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo)).FirstOrDefault();

                //------------------------------
                model.QuoteId = bfs.Encrypt_CBC(id);

                if (getQuote != null)
                {
                    model.hasVehicleOwnedRemarkOne = getQuote.hasVehicleOwnedRemarkOne;
                    model.hasCoownerResidentRemarkOne = getQuote.hasCoownerResidentRemarkOne;
                    model.hasExistingDamageRemarkOne = getQuote.hasExistingDamageRemarkOne;
                    model.hasCriminalActivityRemarkOne = getQuote.hasCriminalActivityRemarkOne;

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("LOADING QUESTIONNAIRE INFO FOR {0}: {1}", Sessions.Instance.PolicyNo, "Answers"));
                    model.hasDependent = getQuote.hasDependent.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasFloridaResident = getQuote.hasFloridaResident.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasDrivingImpairments = getQuote.hasDrivingImpairments.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasVehicleOwned = getQuote.hasVehicleOwned.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasCoownerResident = getQuote.hasCoownerResident.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasVehicleNotGaraged = getQuote.hasVehicleNotGaraged.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasExistingDamage = getQuote.hasExistingDamage.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasGrayMarketVehicle = getQuote.hasGrayMarketVehicle.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasAutoInsuranceCancelledorPIPClaim = getQuote.hasAutoInsuranceCancelledorPIPClaim.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasCriminalActivity = getQuote.hasCriminalActivity.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasVehicleForBusinessPurpose = getQuote.hasVehicleForBusinessPurpose.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                    model.hasReportedBusinessUse = getQuote.hasReportedBusinessUse.ToString() == "True" ? "True".Parse<bool>() : "False".Parse<bool>();
                }
            }
            return model;
        }

        public QuestionnaireNewModel CreatePolicyQuoteQuestionnaire(QuestionnaireNewModel model, string policyNo)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                PolicyQuoteQuestionnaire updateQuote = uow.QuoteRepository.GetQuoteQuestionnaire(policyNo);
                if (updateQuote == null)
                {
                    updateQuote = new PolicyQuoteQuestionnaire();
                }

                updateQuote.PolicyNo = policyNo;
                updateQuote.hasDependent = model.hasDependent.GetValueOrDefault();
                updateQuote.hasFloridaResident = model.hasFloridaResident.GetValueOrDefault();
                updateQuote.hasDrivingImpairments = model.hasDrivingImpairments.GetValueOrDefault();
                updateQuote.hasVehicleOwned = model.hasVehicleOwned.GetValueOrDefault();
                updateQuote.hasCoownerResident = model.hasCoownerResident.GetValueOrDefault();
                updateQuote.hasVehicleNotGaraged = model.hasVehicleNotGaraged.GetValueOrDefault();
                updateQuote.hasExistingDamage = model.hasExistingDamage.GetValueOrDefault();
                updateQuote.hasGrayMarketVehicle = model.hasGrayMarketVehicle.GetValueOrDefault();
                updateQuote.hasAutoInsuranceCancelledorPIPClaim = model.hasAutoInsuranceCancelledorPIPClaim.GetValueOrDefault();
                updateQuote.hasCriminalActivity = model.hasCriminalActivity.GetValueOrDefault();
                updateQuote.hasVehicleForBusinessPurpose = model.hasVehicleForBusinessPurpose.GetValueOrDefault();
                updateQuote.hasReportedBusinessUse = model.hasReportedBusinessUse.GetValueOrDefault();
                updateQuote.hasVehicleOwnedRemarkOne = model.hasVehicleOwnedRemarkOne;
                updateQuote.hasCoownerResidentRemarkOne = model.hasCoownerResidentRemarkOne;
                updateQuote.hasExistingDamageRemarkOne = model.hasExistingDamageRemarkOne;
                updateQuote.hasCriminalActivityRemarkOne = model.hasCriminalActivityRemarkOne;
                uow.QuoteRepository.SaveQuoteQuestionnaire(updateQuote);
                uow.Save();
                return model;
            }
           
        }

        public DriverModel AddDriverViolations(PolicyQuoteDrivers driver)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                DriverModel model = UtilitiesRating.MapXpoToModel(driver);
                model.IsEdit = true;
                XPCollection<PolicyQuoteDriversViolations> getViolations =
                    new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse(
                        "PolicyNo = ? AND DriverIndex = ?", Sessions.Instance.PolicyNo,
                        model.DriverIndex));
                model.Violations = new List<ViolationModel>();

                foreach (PolicyQuoteDriversViolations driverViolation in getViolations)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: LOADING DRIVER VIOLATIONS DATA", Sessions.Instance.PolicyNo), "INFO");
                    ViolationModel vModel = new ViolationModel();
                    vModel.ViolationId = driverViolation.OID;
                    vModel.Description = driverViolation.ViolationDesc;
                    vModel.Date = driverViolation.ViolationDate;
                    vModel.Points = driverViolation.ViolationPoints;
                    vModel.FromMvr = driverViolation.FromMVR;
                    vModel.FromAPlus = driverViolation.FromAPlus;
                    vModel.FromCV = driverViolation.FromCV;
                    vModel.DriverIndex = driverViolation.DriverIndex;
                    model.Violations.Add(vModel);
                }

                return model;
            }
        }

        public XPCollection<PolicyQuoteDrivers> DeleteDriverRecords(UnitOfWork uow, string id)
        {
            PolicyQuoteDrivers driver =
                    new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("OID = ?", id)).FirstOrDefault();
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: DELETE DRIVER WITH OID {1}", Sessions.Instance.PolicyNo, id), "WARN");
            if (driver == null)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: QUOTE NOT FOUND", Sessions.Instance.PolicyNo), "FATAL");
                //throw new HttpException(404, "Vehicle not found"); //LF 12/12/18
                throw new HttpException(404, "Driver not found");
            }

            DbHelperUtils.DeleteRecord("PolicyQuoteDriversViolations", "PolicyNo", "DriverIndex", driver.PolicyNo,
                driver.DriverIndex.ToString());

            driver.Delete();
            uow.CommitChanges();
            DbHelperUtils.DeleteRecord("PolicyQuoteDrivers", "OID", id);

            uow.BeginTransaction();
            XPCollection<PolicyQuoteDrivers> getDrivers = new XPCollection<PolicyQuoteDrivers>(uow,
                CriteriaOperator.Parse("PolicyNo = ?", driver.PolicyNo),
                new SortProperty("DriverIndex", SortingDirection.Ascending));

            foreach (PolicyQuoteDrivers quoteDrivers in getDrivers.Where(x => x.DriverIndex > driver.DriverIndex))
            {
                XPCollection<PolicyQuoteDriversViolations> getViolations =
                    new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse(
                        "PolicyNo = ? AND DriverIndex = ?", quoteDrivers.PolicyNo, quoteDrivers.DriverIndex));
                foreach (PolicyQuoteDriversViolations violation in getViolations)
                {
                    violation.DriverIndex--;
                    violation.Save();
                }

                quoteDrivers.DriverIndex--;
                quoteDrivers.Save();
            }

            uow.CommitChanges();
            return getDrivers;
        }

        public PolicyQuoteCars DeleteVehicleRecords(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: DELETE VEHICLE DATA FOR CAR {1}", Sessions.Instance.PolicyNo, id), "WARN");
                PolicyQuoteCars car = new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("OID = ?", id))
                    .FirstOrDefault();

                if (car == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: CAR DATA NOT FOUND {1}", Sessions.Instance.PolicyNo, id), "FATAL");
                    throw new HttpException(404, "Vehicle not found");
                }

                car.Delete();
                uow.CommitChanges();

                DbHelperUtils.DeleteRecord("PolicyQuoteCars", "OID", id);
                DbHelperUtils.DeleteRecord("PolicyCarLienHolders", "PolicyNo", "CarIndex", car.PolicyNo,
                    car.CarIndex.ToString()); //LF 12/9/18  added
                XPCollection<PolicyQuoteCars> getCars =
                    new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", car.PolicyNo));

                int count = 1;
                foreach (PolicyQuoteCars quoteCar in getCars)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: LOAD CAR DATA {1}", Sessions.Instance.PolicyNo, id), "INFO");
                    uow.BeginTransaction();
                    PolicyQuoteCars updateCars = uow.FindObject<PolicyQuoteCars>(
                        CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", quoteCar.PolicyNo, quoteCar.CarIndex));
                    updateCars.CarIndex = count;
                    updateCars.Save();
                    uow.CommitChanges();
                    count += 1;
                }
                return car;
            }
        }

        public VehicleModel FindVehicle(QuoteModel quoteModel, int carindex)
        {
            VehicleModel vehicleModel;
            vehicleModel = quoteModel.Vehicles.Find(v => v.CarIndex == carindex);
            if (vehicleModel == null)
            {
                vehicleModel = new VehicleModel();
                vehicleModel.Year = 0;
                vehicleModel.Make = "";
                vehicleModel.VehModel = "";
                vehicleModel.Style = "";
                vehicleModel.VIN = "";
                vehicleModel.Artisan = false;
                vehicleModel.HasAirbag = false;
                vehicleModel.HasAntiLockBrakes = false;
                vehicleModel.HasAntiTheftDevice = false;
                vehicleModel.HasConvOrTT = false;
                vehicleModel.HasPhyDam = false;
                vehicleModel.IsBusinessUse = false;
                vehicleModel.IsCustomized = false;
                vehicleModel.ComprehensiveDeductible = "NONE";
                vehicleModel.CollisionDeductible = "NONE";
                quoteModel.Vehicles.RemoveAll(x => String.IsNullOrEmpty(x.VehicleId) || x.VehicleId == "0");
                vehicleModel.CarIndex = quoteModel.Vehicles.Count + 1;
                quoteModel.Vehicles.Add(vehicleModel);

            }
            return vehicleModel;
        }

        public bool QuestionnaireYesAnswers(string PolicyNo, string AppQuestionAnswers)
        {
            string answers = AppQuestionAnswers;
            bool hardStop = false;
            int qNo;
            for (int i = 0; i < AppQuestionAnswers?.Length; i++)
            {
                if (AppQuestionAnswers[i] == '1')
                {
                    qNo = i + 1;
                    switch (qNo)
                    {
                        case 2:
                            Sessions.Instance.RatingErrMsg =
                                @"<div class='error'>Quote " + PolicyNo +
                                " is invalid due to license suspended, cancelled or revoked.</div>";
                            hardStop = true;
                            break;
                        case 3: // 5:
                            Sessions.Instance.RatingErrMsg =
                                @"<div class='error'>Quote " + PolicyNo +
                                " is invalid due to impairment of driving ability.</div>";
                            hardStop = true;
                            break;
                        case 4: // 16:
                            if (AppQuestionAnswers[25] == '1')
                            {
                                Sessions.Instance.RatingErrMsg =
                                    @"<div class='error'>Quote " + PolicyNo +
                                    " is invalid due to 2 or more PIP claims in past 3 years.</div>";
                                hardStop = true;
                            }

                            break;
                        case 6: // 8:
                            Sessions.Instance.RatingErrMsg =
                                @"<div class='error'>Quote " + PolicyNo +
                                " is invalid due to driver residing in Florida less than 10 months.</div>";
                            hardStop = true;
                            break;
                        case 7: // 10:
                            Sessions.Instance.RatingErrMsg =
                                @"<div class='error'>Quote " + PolicyNo +
                                " is invalid due to Gray Market vehicle.</div>";
                            hardStop = true;
                            break;
                        case 8: // 12:
                            Sessions.Instance.RatingErrMsg =
                                @"<div class='error'>Quote " + PolicyNo +
                                " is invalid due to a vehicle used for commercial or business.</div>";
                            hardStop = true;
                            break;
                        case 22: // 15:
                            Sessions.Instance.RatingErrMsg =
                                @"<div class='error'>Quote " + PolicyNo +
                                " is invalid due to a vehicle having existing damage.</div>";
                            hardStop = true;
                            break;
                        default:
                            break;
                    } //switch

                    if (hardStop)
                        break; //from for loop
                } //Answers[i] == '1'
            }

            return (hardStop);
        }


        public bool Has2PlusPIPClaims(XpoPolicyQuote quote)
        {
            string answers = quote.AppQuestionAnswers;
            if (answers[3] == '1' && answers[24] == '1') //q4 q4a
                return (true);
            else
                return (false);
        } //Has2PlusPIPClaims

        public bool HasAnyDriverResideLessThanTenMonths(XpoPolicyQuote quote)
        {
            string answers = quote.AppQuestionAnswers;
            if (answers[5] == '1') //q6
                return (true);
            else
                return (false);
        } //HasAnyDriverResideLessThanTenMonths

        public DataSet GetQuoteCoverageInfo(string policyno)
        {
            string query = "select l.IndexID, "
                           + "        case l.Coverage"
                           + "          when 'BI' then 'Bodily Injury'"
                           + "          when 'PIP' then 'Personal Injury Protection'"
                           + "          when 'PD' then 'Property Damage'"
                           + "          when 'UM' then 'Uninsured Motorist'"
                           + "          when 'MP' then 'Med Pay'"
                           + "          when 'COMP' then 'Comprehensive'"
                           + "          when 'COLL' then 'Collision'"
                           + "          when 'LOU' then 'Loss Of Use'"
                           + "          when 'ADND' then 'Accidental Death'"


                           + "          else ''"
                           + "        end as Coverage,"
                           + "             "
                           + "        case l.Coverage"
                           + "          when 'BI' then"
                           + "              case "
                           + "                when BILimit = '10/20' then cast('10,000/20,000' as varchar(50)) "
                           + "                else cast('N/A' AS varchar(50))"
                           + "              end "
                           + "          when 'PD' then '10,000' "
                           + "          when 'PIP' then '10,000, '+CAST(PIPDed AS varchar(20))+' Deduct '+"
                           + "                                    case when NIO=1 then 'NIO' else case when NIRR=1 then 'NIRR' else '' end +"
                           + "                                    case WorkLoss when Null then ', W.L.E.' when 1 then ', W.L.E.' when 0 then '' end end "
                           + "          when 'UM' then "
                           + "              case "
                           + "                when UMLimit = '10/20' then cast('10,000/20,000' as varchar(50)) +"
                           + "                                    case when UMStacked=1 then ' Stacked' else ' Unstacked' end "
                           + "                else cast('N/A' AS varchar(50))"
                           + "              end  "
                           + "          when 'MP' then"
                           + "              case "
                           + "                when MedPayLimit!='' and MedPayLimit!='0' then CAST(MedPayLimit AS varchar(30)) else ''"
                           + "              end"
                           + "          when 'COMP' then"
                           + "              case when exists (select 1 from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='NONE')  "
                           + "                   then (select MAX(CompDed) from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='NONE')+' Deduct, ACV up to $45k' else '' "
                           + "              end"
                           + "          when 'COLL' then"
                           + "              case when exists (select 1 from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='NONE')  "
                           + "                   then (select MAX(CompDed) from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='NONE')+' Deduct, ACV up to $45k' else '' "
                           + "              end"
                           + "          when 'LOU' then "
                           + "              case   "
                           + "                   when HasLOU=1 then '$20/Day Max $600' else 'N/A' "
                           + "              end"
                           + "          when 'ADND' then "
                           + "              case   "
                           + "                   when ADNDLimit>0 then CAST(ADNDLimit as varchar(20)) else 'N/A' "
                           + "              end    "
                           + "        end as LimitDesc,"
                           + "                                     "
                           + "        case l.Coverage"
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'')  "
                           + "          when 'LOU' then ISNULL(p.LOUCost,'')  "
                           + "          when 'ADND' then ISNULL(p.ADNDCost,'')  "
                           + "        end as 'Vehicle 1', "
                           + "             "
                           + "        case l.Coverage "
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'LOU' then '' "
                           + "          when 'ADND' then '' "
                           + "        end as 'Vehicle 2', "
                           + "         "
                           + "        case l.Coverage "
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'LOU' then '' "
                           + "          when 'ADND' then '' "
                           + "        end as 'Vehicle 3',"
                           + "         "
                           + "        case l.Coverage"
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'LOU' then ''"
                           + "          when 'ADND' then '' "
                           + "        end as 'Vehicle 4',"
                           + "         "
                           + "        case l.Coverage"
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'')"
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'LOU' then ''"
                           + "          when 'ADND' then '' "
                           //					+ "        end as 'Vehicle 5', FHCFFee, PolicyWritten, PolicyFee, mvrfee, DBSetupfee, SR22fee "
                           + "        end as 'Vehicle 5', PIPPDOnlyFee, PolicyWritten, PolicyFee, mvrfee, MaintenanceFee, SR22fee "
                           + "             "
                           + "   from PolicyQuote p, AARateCoverages l"
                           + " where p.PolicyNo='@PolicyNo'"
                           + "        and l.Coverage!='RENTAL'"
                           + "  UNION "
                           + "  Select 99 as IndexID,"
                           + "         '' as Coverage,"
                           + "         'Vehicle Totals:' as LimitDesc,"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=1),0)+"
                           + "         ISNULL((Select isNULL(LOUCost,0)+isNULL(ADNDCost,0) from PolicyQuote where PolicyNo='@PolicyNo'),0) as 'Vehicle 1',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=2),0) as 'Vehicle 2',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=3),0) as 'Vehicle 3',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=4),0) as 'Vehicle 4',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=5),0) as 'Vehicle 5',"
                           + "         '', '', '', '', '', ''"
                           + " order by IndexID";
            query = query.Replace("@PolicyNo", policyno.ToString());

            SqlDataReader viewCoverageInfo = Database.ExecuteQuery(query, CommandType.Text);

            DataSet CoverageDataSet = new DataSet();
            CoverageDataSet.Load(viewCoverageInfo, LoadOption.OverwriteChanges, "QuoteCoverageTable");
            return CoverageDataSet;
        }

        public int CalcDriverPoints(string policyNo, int driverIndex)
        {
            int points = 0;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                XPCollection<PolicyQuoteDriversViolations> currViolations =
                    new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse(
                        "PolicyNo = ? AND DriverIndex = ?",
                        policyNo, driverIndex));
                foreach (PolicyQuoteDriversViolations driverViolation in currViolations)
                {
                    points += driverViolation.ViolationPoints;
                }
            }

            return (points);
        } 

        public void UpdateDriverViolations(DateTime violationDate, string violationDescription, int driverIndex, int violationsCount)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string cycle = XpoAARateCycles.GetCurrentCycle(uow).RateCycle;
                Support.DataObjects.XpoAARateViolations lookupViolation =
                    new XPCollection<Support.DataObjects.XpoAARateViolations>(uow,
                            CriteriaOperator.Parse("RateCycle = ? AND LongDesc = ?", cycle, violationDescription))
                        .FirstOrDefault();

                if (lookupViolation == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VIOLATION DOES NOT EXIST", Sessions.Instance.PolicyNo), "FATAL");
                    throw new HttpException(400,
                        String.Format("Could not find violation in cycle {0} by description: {1}", cycle, violationDescription));
                }

                PolicyQuoteDriversViolations violation = new PolicyQuoteDriversViolations(uow);
                violation.DriverIndex = driverIndex;
                violation.PolicyNo = Sessions.Instance.PolicyNo;
                violation.ViolationNo = violationsCount;
                violation.ViolationIndex = violationsCount;
                violation.ViolationDate = violationDate;
                violation.ViolationDesc = violationDescription;
                violation.ViolationCode = lookupViolation.Code;
                violation.ViolationPoints = lookupViolation.PointsFirst;
                violation.ViolationGroup = lookupViolation.ViolationGroup;
                violation.Save();
            }
        }

        public List<LienHolderModel> SaveLienHolders(FormCollection formCollection, int carId, int carIndex, string vin, UnitOfWork uow)
        {
            var newLhIds = formCollection.AllKeys.Where(x => x.StartsWith("newvehLienholder-")).ToList();
            string deleteLienholderIds = formCollection.AllKeys.Contains("LienHolders.DeletedLienHoldersIds") ? formCollection["LienHolders.DeletedLienHoldersIds"] : null;

            var newLhsAdded = new List<LienHolderModel>();
            foreach (var newLhId in newLhIds)
            {
                var lhJson = System.Web.HttpUtility.UrlDecode(formCollection[newLhId]);
                var newLhAdded = new JavaScriptSerializer().Deserialize<LienHolderModel>(lhJson);
                if (!string.IsNullOrEmpty(newLhAdded.Name))
                    newLhsAdded.Add(newLhAdded);
            }

            bool reOrderLhIndex = false;
            if (!string.IsNullOrEmpty(deleteLienholderIds))
            {
                var LhIdsToDelete = deleteLienholderIds.Split(new char[] { ',' }).Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();

                foreach (var LhIdToDelete in LhIdsToDelete)
                {
                    reOrderLhIndex = true;
                    DbHelperUtils.DeleteRecord("PolicyCarLienHolders", "ID", LhIdToDelete);
                }
            }

            var getLienHolders = new XPCollection<PolicyCarLienHolders>(uow,
                       CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo, carIndex),
                       new SortProperty("CarIndex", SortingDirection.Ascending));

            int LhIndex = 0; // getLienHolders.Max(x => x.LienHolderIndex);
            if (reOrderLhIndex && getLienHolders.Count > 0)
            {
                foreach (var lh in getLienHolders)
                {
                    LhIndex++;
                    lh.LienHolderIndex = LhIndex;
                }
                uow.CommitChanges();
            }
            else
            {
                LhIndex = getLienHolders.Count > 0 ? getLienHolders.Max(x => x.LienHolderIndex) : 0;
            }

            if (newLhsAdded.Count > 0)
            {
                foreach (var newLhAdded in newLhsAdded)
                {
                    LhIndex++;
                    var addLien1 = new PolicyCarLienHolders(uow);
                    addLien1.CarIndex = carIndex; //LF 11/7/18 model.CarIndex;
                    addLien1.LienHolderIndex = LhIndex;
                    addLien1.PolicyNo = Sessions.Instance.PolicyNo;
                    addLien1.Name = newLhAdded.Name.Upper();
                    addLien1.Address = newLhAdded.StreetAddress.Upper();
                    addLien1.City = newLhAdded.City.Upper();
                    addLien1.State = newLhAdded.State.Upper();
                    addLien1.Zip = newLhAdded.ZIP;
                    addLien1.DateCreated = DateTime.Now;
                    addLien1.Phone = newLhAdded.Phone;
                    addLien1.isAddInterest = newLhAdded.isAddInterest;
                    addLien1.Save();
                    uow.CommitChanges();
                }
            }

            var LienHolders = new List<LienHolderModel>();

            getLienHolders = new XPCollection<PolicyCarLienHolders>(uow,
                       CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo,
                           carIndex), new SortProperty("CarIndex", SortingDirection.Ascending));
            bool commitChanges = false;
            foreach (PolicyCarLienHolders lien in getLienHolders)
            {
                //uow.BeginTransaction();
                var LienHolder =
                    uow.FindObject<PolicyCarLienHolders>(CriteriaOperator.Parse("ID = ?", lien.ID));

                LienHolderModel tmpModel = UtilitiesRating.MapXpoToModel(LienHolder);
                tmpModel.VehicleId = carId;

                LienHolders.Add(tmpModel);
                LienHolder.VIN = vin;
                LienHolder.Save();
                commitChanges = true;
            }

            if (commitChanges)
                uow.CommitChanges();
            return LienHolders;
        }

        public void HandleAAPointsForMVRViolations(DateTime quoteEffectiveDate,
            IEnumerable<MvrViolationsModel> mvrViolationModelsWithin3Years)
        {
            DateTime threeYearsAgoFromEffectiveDate = quoteEffectiveDate.AddMonths(-36).Date;

            foreach (MvrViolationsModel mvrModel in mvrViolationModelsWithin3Years)
            {
                int totalMinorViolationsForCurrentDirverWithin18Months = 0;
                int totalMinorViolationsForCurrentDirverWithin36Months = 0;
                foreach (PolicyQuoteDriversViolations violation in mvrModel.DriversViolations)
                {
                    if (threeYearsAgoFromEffectiveDate > violation.ViolationDate)
                        continue;

                    if (violation.ViolationCode != null && (violation.ViolationCode.Contains("MAJ") || violation.ViolationCode.Contains("EXC005")))
                    {
                        violation.AAPoints = violation.ViolationPoints;
                    }
                    else if (violation.ViolationCode != null && violation.ViolationCode.Contains("MIN"))
                    {
                        totalMinorViolationsForCurrentDirverWithin36Months++;
                        DateTime eighteenMonthsAgoFromQuoteEffDate = quoteEffectiveDate.AddMonths(-18).Date;
                        if (violation.ViolationDate > eighteenMonthsAgoFromQuoteEffDate)
                        {
                            totalMinorViolationsForCurrentDirverWithin18Months++;
                        }
                        if (totalMinorViolationsForCurrentDirverWithin18Months > 1 ||
                            totalMinorViolationsForCurrentDirverWithin36Months > 2)
                        {
                            violation.AAPoints = violation.ViolationPoints;
                        }
                    }
                }
            }
        }

        public void HandleAAPointsForAplusViolations(
            IEnumerable<PolicyQuoteDriversViolations> violationsForDriver,
            IEnumerable<APlusClaimModel> aPlusClaimModels, string driverFirstName, string driverLastName)
        {
            int totalViolationsForCurrentDirver = 0;
            foreach (PolicyQuoteDriversViolations violation in violationsForDriver)
            {
                totalViolationsForCurrentDirver++;
                APlusClaimModel modelToUpdate = aPlusClaimModels.FirstOrDefault(apm =>
                    apm != null && !string.IsNullOrEmpty(apm.DriverName) &&
                    !string.IsNullOrEmpty(apm.ClaimDateOfLoss) &&
                    apm.DriverName.ToLower().Contains(driverFirstName.ToLower()) &&
                    apm.DriverName.ToLower().Contains(driverLastName.ToLower()) &&
                    DateTime.ParseExact(apm.ClaimDateOfLoss,
                        "yyyyMMdd", CultureInfo.InvariantCulture).Equals(violation.ViolationDate));
                if (modelToUpdate == null)
                {
                    continue;

                }
                if (violation.ViolationGroup.Equals("ACC"))
                {
                    if (totalViolationsForCurrentDirver == 1)
                    {
                        modelToUpdate.AAPoints = 3;
                    }
                    else if (totalViolationsForCurrentDirver > 1)
                    {
                        modelToUpdate.AAPoints = 4;
                    }
                }
            }
        }

        public void UpdateDriversExcludedAndNotAssociated(IEnumerable<string> notAssociatedDrivIndexesToExclude, Dictionary<string, string> confirmNotAssociatedDrivIndexes, string quoteNo)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicyQuote quote =
                    new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo))
                        .FirstOrDefault();
                if (quote != null)
                {
                    if (notAssociatedDrivIndexesToExclude.Any())
                    {
                        AddToExcludedDrivers(notAssociatedDrivIndexesToExclude, uow, quote);
                    }

                    ConfirmNotAssociatedDrivers(confirmNotAssociatedDrivIndexes, uow, quote);

                    uow.CommitChanges();
                }
            }
        }

        private void ConfirmNotAssociatedDrivers(Dictionary<string, string> confirmNotAssociatedDrivIndexes, UnitOfWork uow, XpoPolicyQuote quote)
        {
            foreach (KeyValuePair<string, string> notAssociatedDriverIndex in confirmNotAssociatedDrivIndexes)
            {
                var criteria = CriteriaOperator.Parse("PolicyNo = ? AND ID = ?", quote.PolicyNo, notAssociatedDriverIndex.Key);
                var notAssociatedDriver = new XPCollection<PolicyQuoteNotAssociatedDrivers>(uow, criteria).FirstOrDefault();

                if (notAssociatedDriver != null)
                {
                    notAssociatedDriver.NADConfirmedDateTime = DateTime.Now;
                    notAssociatedDriver.NADConfirmedAgentId = Sessions.Instance.AgentCode;
                    notAssociatedDriver.NotAssociatedDriver = true;
                    notAssociatedDriver.NADReason = notAssociatedDriverIndex.Value;
                    notAssociatedDriver.Save();
                }
            }
        }

        private void AddToExcludedDrivers(IEnumerable<string> notAssociatedDrivIndexesToExclude, UnitOfWork uow, XpoPolicyQuote quote)
        {
            XPCollection<PolicyQuoteDrivers> getDrivers = new XPCollection<PolicyQuoteDrivers>(uow,
              CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));


            int maxDriverIndex = getDrivers.Count;
            foreach (string notAssociatedDrivIndexToExclude in notAssociatedDrivIndexesToExclude)
            {
                var criteria = CriteriaOperator.Parse("PolicyNo = ? AND ID = ?", quote.PolicyNo, notAssociatedDrivIndexToExclude);
                var notAssociatedDriver = new XPCollection<PolicyQuoteNotAssociatedDrivers>(uow, criteria).FirstOrDefault();

                if (notAssociatedDriver != null)
                {
                    ++maxDriverIndex;
                    if (maxDriverIndex > 10)
                    {
                        LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: DRIVERS COUNT > 10", Sessions.Instance.PolicyNo), "INFO");
                        throw new HttpException(400, "One policy may only have 10 driver maximum");
                    }
                    var isSameDriverExists = getDrivers.Any(x =>
                      notAssociatedDriver?.FirstName?.Equals(x.FirstName, StringComparison.OrdinalIgnoreCase) == true &&
                      notAssociatedDriver?.LastName?.Equals(x.LastName, StringComparison.OrdinalIgnoreCase) == true &&
                      (notAssociatedDriver.DOB == DateTime.MinValue || notAssociatedDriver.DOB.ToShortDateString() == x.DOB.ToShortDateString()));


                    if (!isSameDriverExists)
                    {
                        var newExcludedDriver = new PolicyQuoteDrivers(uow)
                        {
                            DOB = notAssociatedDriver.DOB,
                            FirstName = notAssociatedDriver.FirstName,
                            LastName = notAssociatedDriver.LastName,
                            MI = notAssociatedDriver.MI,
                            Married = notAssociatedDriver.Married,
                            JobTitle = notAssociatedDriver.JobTitle,
                            Occupation = notAssociatedDriver.Occupation,
                            PolicyNo = notAssociatedDriver.PolicyNo,
                            Suffix = notAssociatedDriver.Suffix,
                            DriverAge = notAssociatedDriver.DriverAge,
                            Gender = notAssociatedDriver.Gender,
                            Exclude = false,
                            DriverIndex = maxDriverIndex,
                            RelationToInsured = "RE"
                        };
                        newExcludedDriver.Save();
                        notAssociatedDriver.Delete();
                        notAssociatedDriver.Save();
                    }
                }
            }
        }

        private PolicyQuoteNotAssociatedDrivers CreateNotAssociatedDriver(string quoteNo, UnitOfWork uow, PolicyQuoteDrivers driver, PolicyQuoteDriversViolations policyQuoteDriversViolations)
        {
            return new PolicyQuoteNotAssociatedDrivers(uow)
            {
                PolicyNo = quoteNo,
                ViolationDesc = policyQuoteDriversViolations.ViolationDesc,
                DriverAge = driver.DriverAge,
                ActionMessage = policyQuoteDriversViolations.ViolationDesc,
                DateTimeCreated = DateTime.Now,
                DOB = driver.DOB,
                FirstName = driver.FirstName,
                LastName = driver.LastName,
                MI = driver.MI,
                Gender = driver.Gender,
                Occupation = driver.Occupation,
                Married = driver.Married,
                Youthful = driver.Youthful,
                DriverIndex = driver.DriverIndex,
                AgentCodeCreatedBy = Sessions.Instance.AgentCode,
                NotAssociatedDriver = true,
                NADConfirmedAgentId = Sessions.Instance.AgentCode,
                NADConfirmedDateTime = DateTime.Now,
                FromRCPOS = true
            };
        }
    }
}