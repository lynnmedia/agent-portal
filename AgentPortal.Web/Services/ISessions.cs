﻿namespace AgentPortal.Services
{
    public interface ISessions
    {
        bool IsAgent { get; set; }
        string AgentCode { get; set; }
        string AgentName { get; set; }
        string AgencyName { get; set; }
        string Username { get; set; }
        bool IsAuth { get; set; }
        bool IsCompany { get; set; }
        bool IsClient { get; set; }
        string EmailAddress { get; set; }
        string PolicyNumber { get; set; }
        string PolicyNo { get; set; }
    }
}