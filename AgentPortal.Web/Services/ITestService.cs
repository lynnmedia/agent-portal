﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentPortal.Models;
using AlertAuto.Integrations.Contracts.CardConnect;

namespace AgentPortal.Services
{
    public interface ITestService
    {
        string TestMethod(string policyNo);
    }
}
