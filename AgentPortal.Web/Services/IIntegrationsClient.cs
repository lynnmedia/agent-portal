﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentPortal.Models;
using AlertAuto.Integrations.Contracts.CardConnect;
using AlertAuto.Integrations.Contracts.MVR;
using AlertAuto.Integrations.Contracts.RAPA;
using AlertAuto.Integrations.Contracts.Verisk.APlus;
using AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier;
using AlertAuto.Integrations.Contracts.Verisk.RiskCheck;

namespace AgentPortal.Services
{
    public interface IIntegrationsClient
    {
        VeriskVinSearchResultBody SearchVin(string policyNo, string vin, string year = "", string make = "", string model = "", string bodyStyle = "");
        VeriskVehicleSearchResultBody SearchVehicle(string policyNo, string year, string make, string model = "");
        VeriskMvrSubmitRequestBody MvrSubmitRequest(string policyNo, string givenName, string surname, string dob, string dlNumber, string dlState);
        VeriskRetreiveMvrReportBody MvrRequestReport(string policyNo, string orderNumber);
        VeriskAPlusCapWithClaimsBody AplusReportWithClaims(string policyNo, APlusReportModel data);
        CoverageVerifierResponseBody CoverageVerifierReport(string policyNo, CoverageVerifierRequest data);
        Task<VeriskRiskCheckReportResultBody> RiskCheckReport(string policyNo, RiskCheckRquest data);
        void SendESignatureRequest(string policyNo, Dictionary<string, string> userEmailMap, IEnumerable<string> documentLocations,List<string> ccEmails=null, string requestType = "");
        string GetEsignatureDocumentDownloadUrl(string signatureRequestId);
        CardConnectResponse AuthorizeWithCardConnect(string policyNo, CardConnectRequest requestData, bool createNewProfile);
        string CreateCardConnectProfile(string policyNo, CardConnectRequest requestData);
        bool UpdateFlagForAllIntegrationCall(string pPolicyNo, bool pSetAllIntegrationCalFlagValue, bool pHasToSetRCPOSFlag = false, bool pSetRcposOrderedValue = false);
        bool GetFlagForAllIntegrationCall(string pPolicyNo, ref bool pIsRCPOSOrdered);
        string GetDeepLink(string policyNo, bool existingUrlOnly= false);
        bool SendUploadImagesDeepLink(string policyNo,string deepLink);
        void GenerateNewPolicyDocuments(string policyNo, string historyIndex);
    }
}
