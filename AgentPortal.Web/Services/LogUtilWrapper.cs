﻿using AgentPortal.Support.Utilities;

namespace AgentPortal.Services
{
    public class LogUtilWrapper : IPalmsLogger
    {
        public void LogIp(string username, string ipAddress)
        {
            LogUtils.Log(username, ipAddress);
        }

        public void Log(string username, string desc, string type = "DEBUG")
        {
            LogUtils.Log(username, desc, type);
        }
    }
}