﻿using AgentPortal.Models;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using AlertAuto.Integrations.Contracts.Verisk.APlus;
using AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Address = AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Address;
using Driver = AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Driver;
using RiskCheck = AlertAuto.Integrations.Contracts.Verisk.RiskCheck;
using Subject = AlertAuto.Integrations.Contracts.Verisk.APlus.Subject;

namespace AgentPortal.Services
{
    public class AjaxService 
    {
        private readonly EmailService _emailService;
        private readonly ReportService _reportService;

        public AjaxService(EmailService emailService, ReportService reportService)
        {
            _emailService = emailService;
            _reportService = reportService;
        }
        

        [HttpPost]
        public void UpdatePolicyEndorsementType(string policyNo, string endorsementType)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                IEnumerable<PolicyEndorsementTypes> existingTypes =
                    new XPCollection<PolicyEndorsementTypes>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND EndorsementType = ?", policyNo, endorsementType));
                if (!existingTypes.Any())
                {
                    PolicyEndorsementTypes newType = new PolicyEndorsementTypes(uow)
                    {
                        PolicyNo = policyNo,
                        EndorsementType = endorsementType
                    };
                    newType.Save();
                    uow.CommitChanges();
                }
            }
        }

        public List<int> HandleRiskCheckPOSForQuotes(string quoteNo, IEnumerable<PolicyQuoteDrivers> drivers, IEnumerable<PolicyQuoteDrivers> excludedDrivers, IEnumerable<PolicyQuoteCars> cars,
           XpoPolicyQuote policyQuote, XPCollection<PolicyQuoteDriversViolations> currentViolations, QuoteIntegrationsViewModel vm, UnitOfWork uow)
        {
            var voilatedDriverIndexes = new List<int>();
            var undisiclosedSubjects = new List<string>();
            var allDrivers = drivers.Concat(excludedDrivers);
            RiskCheck.RiskCheckRquest requestData = GetRiskCheckRequestData(drivers, cars, policyQuote);
            var rcResponse = IntegrationsUtility.RiskCheckReport(quoteNo, requestData).Result;
            var existingNotAssociatedDrivs = new XPCollection<PolicyQuoteNotAssociatedDrivers>(uow,
                CriteriaOperator.Parse("PolicyNo = ?", quoteNo));

            if (rcResponse != null)
            {
                var connString = System.Configuration.ConfigurationManager.ConnectionStrings["AlertAutoConnectionString"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand cmd = new SqlCommand("spMAINT_RCPOSConversion", conn))
                {
                    conn.Open();
                    cmd.CommandTimeout = 360;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SqlParameter("@PolicyNo", SqlDbType.VarChar, 50) { Value = quoteNo });
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.ExecuteNonQuery();
                }
            }
            if (rcResponse?.ScoreDetails?.Any(x => x.ReasonCodeDetails.Any()) == true)
            {
                var unDisclosedDrivActionMsg = "ask about an undisclosed driver";
                var scoreDetails = rcResponse.ScoreDetails.Where(x => x.ReasonCodeDetails.Any(r => r.ActionMessage?.Trim()?.ToLower()?.Contains(unDisclosedDrivActionMsg) == true));

                foreach (var scoreDetail in scoreDetails)
                {
                    var reasonCodeDetails = scoreDetail.ReasonCodeDetails.First(x => x.ActionMessage?.ToLower()?.Contains(unDisclosedDrivActionMsg) == true);
                    var subject = rcResponse.Subjects?.FirstOrDefault(x => x.Sequence == scoreDetail.EntityNumber);
                    if (subject != null)
                    {
                        AddPolicyQuoteNotAssociatedDrivers(policyQuote, vm, uow, reasonCodeDetails.ActionMessage, subject, existingNotAssociatedDrivs);
                        undisiclosedSubjects.Add(subject.Sequence);
                    }
                }
            }
            if (rcResponse.Subjects?.Any() == true)
            {
                foreach (var subject in rcResponse.Subjects.Where(x => !undisiclosedSubjects.Any(ds => ds == x.Sequence)))
                {
                    if (subject?.SubjectName != null)
                    {
                        var driver = allDrivers.FirstOrDefault(x =>
                        subject.SubjectName.GivenName?.Equals(x.FirstName, StringComparison.OrdinalIgnoreCase) == true &&
                        subject.SubjectName.Surname?.Equals(x.LastName, StringComparison.OrdinalIgnoreCase) == true);
                        if (driver == null || driver.DOB.ToString("yyyyMMdd", CultureInfo.InvariantCulture) != subject.DOB)
                        {
                            AddPolicyQuoteNotAssociatedDrivers(policyQuote, vm, uow, "Additional household", subject, existingNotAssociatedDrivs);
                        }
                    }
                }
            }
            if (rcResponse?.Vehicles?.Any() == true)
            {
                var owners = new List<RiskCheck.RegisteredOwnerName>();
                Action<RiskCheck.RegisteredOwnerName> AddOwner = delegate (RiskCheck.RegisteredOwnerName owner)
                {
                    if (owner != null && !owners.Any(x => x.GivenName == owner.GivenName && x.Surname == owner.Surname))
                    {
                        owners.Add(owner);
                        CheckVehicleOwnerName(allDrivers, policyQuote.PolicyNo, uow, rcResponse.Subjects, owner, "Vehicle Owner", vm, existingNotAssociatedDrivs);
                    }
                };

                var brandedTitleVehicles = new XPCollection<XpoBrandedTitleVehicles>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", quoteNo));

                foreach (var vehicle in rcResponse.Vehicles)
                {
                    AddOwner(vehicle.RegisteredOwnerName);
                    AddOwner(vehicle.SecondOwnerName);

                    if (vehicle.IsBranded)
                        AddBrandedTitlesVehicle(cars, quoteNo, uow, vehicle, brandedTitleVehicles);
                }


                //Check for non-owned vehicles
                if (rcResponse?.ScoreDetails?.Any(x => x.ReasonCodeDetails.Any()) == true)
                {
                    if (rcResponse.ScoreDetails.Any(sd =>
                        sd.ReasonCodeDetails.Any(rcd => rcd.Reason.Code.Equals("AO-5.01"))))
                    {
                        IList<RiskCheck.ResponseVehicle> existingVehicles = new List<RiskCheck.ResponseVehicle>();
                        foreach (var scoreDetail in rcResponse.ScoreDetails.Where(sd =>
                            sd.ReasonCodeDetails.Any(rcd => rcd.Reason.Code.Equals("AO-5.01"))))
                        {
                            RiskCheck.ResponseVehicle vehicle =
                                rcResponse.Vehicles.FirstOrDefault(v => v.Sequence.Equals(scoreDetail.EntityNumber));
                            if (vehicle != null && !existingVehicles.Contains(vehicle))
                            {
                                // save RiskCheckNonOwnedVehicle
                                RiskCheckNonOwnedVehicles newRiskCheckNonOwnedVehicles = new RiskCheckNonOwnedVehicles(uow);
                                newRiskCheckNonOwnedVehicles.PolicyNo = quoteNo;
                                newRiskCheckNonOwnedVehicles.DateCreated = DateTime.Now;
                                newRiskCheckNonOwnedVehicles.VIN = vehicle.VIN;
                                newRiskCheckNonOwnedVehicles.Year = vehicle.VIN;
                                newRiskCheckNonOwnedVehicles.VehicleIndex = vehicle.Sequence;
                                newRiskCheckNonOwnedVehicles.Make = vehicle.Make;
                                newRiskCheckNonOwnedVehicles.Model = vehicle.Model;
                                newRiskCheckNonOwnedVehicles.RegisteredOwnerName =
                                    $"{vehicle.RegisteredOwnerName.GivenName} {vehicle.RegisteredOwnerName.Surname}";
                                newRiskCheckNonOwnedVehicles.Save();
                                existingVehicles.Add(vehicle);
                            }
                        }
                    }
                }
            }

            policyQuote.RCPOSOrdered = true;
            policyQuote.Save();
            uow.CommitChanges();
            return voilatedDriverIndexes;
        }

        private void AddPolicyQuoteNotAssociatedDrivers(XpoPolicyQuote policyQuote, QuoteIntegrationsViewModel vm, UnitOfWork uow, string voialtionDesc,
            RiskCheck.Subject subject, IEnumerable<PolicyQuoteNotAssociatedDrivers> existingNotAssociatedDrivs)
        {
            var isSubjectInNotAssociatedDrivs = existingNotAssociatedDrivs?.Any(x =>
                           subject.SubjectName.GivenName?.Equals(x.FirstName, StringComparison.OrdinalIgnoreCase) == true &&
                           subject.SubjectName.Surname?.Equals(x.LastName, StringComparison.OrdinalIgnoreCase) == true) ?? false;
            if (!isSubjectInNotAssociatedDrivs)
            {
                var notAssociatedDriver = new PolicyQuoteNotAssociatedDrivers(uow)
                {
                    PolicyNo = policyQuote.PolicyNo,
                    ViolationDesc = voialtionDesc,
                    DriverAge = subject.Age,
                    ActionMessage = voialtionDesc,
                    DateTimeCreated = DateTime.Now,
                    DOB = DateTime.ParseExact(subject.DOB, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None),//, Convert.ToDateTime(subject.DOB),
                    EducationLevel = subject.EducationLevel?.Description,
                    FirstName = subject.SubjectName?.GivenName,
                    LastName = subject.SubjectName?.Surname,
                    MI = subject.SubjectName?.MiddleName,
                    Gender = subject.Gender?.Description,
                    Occupation = subject.Occupation?.Description,
                    Married = subject.MaritalStatus?.Code == "1",
                    Youthful = subject.Youths11to15?.Code != null || subject.Youths16to17?.Code != null,
                    DriverIndex = Convert.ToInt32(subject.Sequence),
                    AgentCodeCreatedBy = Sessions.Instance.AgentCode,
                    FromRCPOS = true,
                    NotAssociatedDriver = false
                };
                notAssociatedDriver.Save();
                var rcPosViolation = new RiskCheckViolationsModel()
                {
                    DOB = notAssociatedDriver.DOB,
                    NotAssociatedDriverIndex = notAssociatedDriver.ID,
                    DriverName = $"{notAssociatedDriver.FirstName} {notAssociatedDriver.LastName}",
                };
                rcPosViolation.DriversViolations.Add(new RiskCheckDriverViolationsModel(notAssociatedDriver.ViolationDesc));
                vm.RiskCheckDriverViolationsModels.Add(rcPosViolation);
            }
        }

        private void AddBrandedTitlesVehicle(IEnumerable<PolicyQuoteCars> cars, string policyNo, UnitOfWork uow, RiskCheck.ResponseVehicle vehicle,
            IEnumerable<XpoBrandedTitleVehicles> brandedTitleVehicles)
        {
            var existingCar = cars.FirstOrDefault(x => x.VIN?.ToLower() == vehicle.VIN?.ToLower());
            var brandedTitleVehicle = brandedTitleVehicles.FirstOrDefault(x => x.VIN?.ToLower() == vehicle.VIN?.ToLower());
            if (vehicle.IsBranded && existingCar != null)
            {
                brandedTitleVehicle = brandedTitleVehicle ?? new XpoBrandedTitleVehicles(uow);
                UtilitiesVehicles.MapModelToXpo(vehicle, brandedTitleVehicle);

                brandedTitleVehicle.PolicyNo = policyNo;
                brandedTitleVehicle.VehicleNbr = existingCar.CarIndex;
                brandedTitleVehicle.Save();
            }
        }
        private void CheckVehicleOwnerName(IEnumerable<PolicyQuoteDrivers> drivers, string policyNo, UnitOfWork uow,
            List<RiskCheck.Subject> subjects, RiskCheck.RegisteredOwnerName owner, string ownerDesc, QuoteIntegrationsViewModel vm, IEnumerable<PolicyQuoteNotAssociatedDrivers> existingNotAssociatedDrivs)
        {
            if (owner != null)
            {
                var isOwnerNameInNotAssoicatedDrivs = existingNotAssociatedDrivs?.Any(x =>
                           owner.GivenName?.Equals(x.FirstName, StringComparison.OrdinalIgnoreCase) == true &&
                           owner.Surname?.Equals(x.LastName, StringComparison.OrdinalIgnoreCase) == true) ?? false;

                if (!isOwnerNameInNotAssoicatedDrivs)
                {
                    if (!drivers.Any(x => x.FirstName.ToLower().Contains(owner.GivenName?.ToLower())))
                    {
                        if (subjects?.Any(x => x.SubjectName?.GivenName?.ToLower()?.Contains(owner.GivenName.ToLower()) == true) != true)
                        {
                            var notAssociatedDriver = new PolicyQuoteNotAssociatedDrivers(uow)
                            {
                                PolicyNo = policyNo,
                                ViolationDesc = $"{ownerDesc} not found",
                                ActionMessage = $"{ownerDesc} not found",
                                DateTimeCreated = DateTime.Now,
                                FirstName = owner.GivenName,
                                LastName = owner.Surname,
                                MI = owner.MiddleName,
                                FromRCPOS = true,
                                Gender = "Unknown",
                                NotAssociatedDriver = false
                            };
                            notAssociatedDriver.Save();

                            var riskCheckViolationsModel = new RiskCheckViolationsModel()
                            {
                                DOB = notAssociatedDriver.DOB,
                                NotAssociatedDriverIndex = notAssociatedDriver.ID,
                                DriverName = $"{notAssociatedDriver.FirstName} {notAssociatedDriver.LastName}",
                            };
                            riskCheckViolationsModel.DriversViolations.Add(new RiskCheckDriverViolationsModel(notAssociatedDriver.ViolationDesc));
                            vm.RiskCheckDriverViolationsModels.Add(riskCheckViolationsModel);
                        }
                    }
                }
            }
        }

        public RiskCheck.RiskCheckRquest GetRiskCheckRequestData(IEnumerable<PolicyQuoteDrivers> drivers,
            IEnumerable<PolicyQuoteCars> vehicles, XpoPolicyQuote policyQuote)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                RiskCheck.RiskCheckRquest requestData = new RiskCheck.RiskCheckRquest();

                var driverDtos = new List<RiskCheck.Driver>();
                var vehicleDtos = new List<RiskCheck.Vehicle>();
                foreach (PolicyQuoteDrivers driver in drivers)
                {
                    var driverDto = new RiskCheck.Driver()
                    {
                        Sequence = driver.DriverIndex.ToString(),
                        GivenName = driver.FirstName,
                        Surname = driver.LastName,
                        DOB = driver.DOB.ToString("yyyyMMdd"),
                        RelationshipToInsured = "PR",
                        Addresses = new List<RiskCheck.Address>()
                    };
                    if (!string.IsNullOrEmpty(policyQuote?.GarageStreet) && !string.IsNullOrEmpty(policyQuote?.GarageCity) &&
                        !string.IsNullOrEmpty(policyQuote?.GarageState) && !string.IsNullOrEmpty(policyQuote?.GarageZip))
                    {
                        driverDto.Addresses.Add(new RiskCheck.Address()
                        {
                            AddressType = "Current",
                            Street1 = policyQuote.GarageStreet,
                            City = policyQuote.GarageCity,
                            StateCode = policyQuote.GarageState,
                            Zip = policyQuote.GarageZip
                        });
                    }

                    driverDtos.Add(driverDto);
                }

                foreach (PolicyQuoteCars vehicle in vehicles)
                {
                    RiskCheck.Vehicle vehicleDto = new RiskCheck.Vehicle()
                    {
                        Sequence = vehicle.CarIndex.ToString(),
                        VIN = vehicle.VIN.Replace("-", ""),
                    };
                    vehicleDtos.Add(vehicleDto);
                }

                requestData.Drivers = driverDtos.ToList();
                requestData.Vehicles = vehicleDtos.ToList();

                return requestData;
            }
        }

        public void HandleAAPointsForMVRViolations(DateTime quoteEffectiveDate,
            IEnumerable<MvrViolationsModel> mvrViolationModelsWithin3Years)
        {
            foreach (MvrViolationsModel mvrModel in mvrViolationModelsWithin3Years)
            {
                int totalMinorViolationsForCurrentDirverWithin18Months = 0;
                int totalMinorViolationsForCurrentDirverWithin36Months = 0;
                foreach (PolicyQuoteDriversViolations violation in mvrModel.DriversViolations)
                {
                    if (violation.ViolationCode != null && (violation.ViolationCode.Contains("MAJ") || violation.ViolationCode.Contains("EXC005")))
                    {
                        violation.AAPoints = violation.ViolationPoints;
                    }
                    else if (violation.ViolationCode != null && violation.ViolationCode.Contains("MIN"))
                    {
                        totalMinorViolationsForCurrentDirverWithin36Months++;
                        DateTime eighteenMonthsAgoFromQuoteEffDate = quoteEffectiveDate.AddMonths(-18).Date;
                        if (violation.ViolationDate > eighteenMonthsAgoFromQuoteEffDate)
                        {
                            totalMinorViolationsForCurrentDirverWithin18Months++;
                        }
                        if (totalMinorViolationsForCurrentDirverWithin18Months > 1 ||
                            totalMinorViolationsForCurrentDirverWithin36Months > 2)
                        {
                            violation.AAPoints = violation.ViolationPoints;
                        }
                    }
                }
            }
        }

        public void CreateClearMVRReportForQuotes(string quoteNo,
            IList<PolicyQuoteDriversViolations> currentViolationsForQuote, PolicyQuoteDrivers driver,
            QuoteIntegrationsViewModel vm, UnitOfWork uow)
        {
            int indexNo = currentViolationsForQuote.Count + 1;
            PolicyQuoteDriversViolations newDriverViolation =
                new PolicyQuoteDriversViolations(uow)
                {
                    PolicyNo = quoteNo,
                    DriverIndex = driver.DriverIndex,
                    FromMVR = true,
                    Chargeable = true,
                    ViolationCode = "CLR001",
                    ViolationGroup = "CLEAR",
                    ViolationDesc = "Clear Report",
                    ViolationNo = indexNo,
                    ViolationIndex = indexNo,
                    ViolationDate = DateTime.Now
                };
            MvrViolationsModel model = new MvrViolationsModel()
            {
                DriverName = $"{driver.FirstName} {driver.LastName}",
                DriversViolations = new List<PolicyQuoteDriversViolations>()
                {
                    newDriverViolation
                }
            };
            vm.MvrDriverViolationsModels.Add(model);
            currentViolationsForQuote.Add(newDriverViolation);
            newDriverViolation.Save();
            uow.CommitChanges();
        }

        public void CreateNoHitMVRForQuotes(string quoteNo,
            IList<PolicyQuoteDriversViolations> currentViolationsForQuote, PolicyQuoteDrivers driver,
            QuoteIntegrationsViewModel vm, UnitOfWork uow)
        {
            //No-Hit
            Support.DataObjects.XpoAARateViolations noHitRateViolation =
                new XPCollection<Support.DataObjects.XpoAARateViolations>(uow,
                        CriteriaOperator.Parse("Code = ?", "UNV002"))
                    .FirstOrDefault();
            if (noHitRateViolation != null)
            {
                MvrViolationsModel noHitViolationModel = new MvrViolationsModel();
                int indexNo = currentViolationsForQuote.Count + 1;
                PolicyQuoteDriversViolations newDriverViolation =
                    new PolicyQuoteDriversViolations(uow)
                    {
                        PolicyNo = quoteNo,
                        DriverIndex = driver.DriverIndex,
                        ViolationPoints = Convert.ToInt32(noHitRateViolation.PointsFirst),
                        FromMVR = true,
                        Chargeable = noHitRateViolation.Chargeable,
                        ViolationCode = noHitRateViolation.Code,
                        ViolationDate = DateTime.Now,
                        ViolationGroup = noHitRateViolation.ViolationGroup,
                        ViolationDesc = noHitRateViolation.LongDesc,
                        ViolationIndex = indexNo,
                        ViolationNo = indexNo
                    };
                if (driver.InternationalLic)
                {
                    newDriverViolation.ViolationDesc = "INTERNATIONAL NO-HIT MVR";
                    newDriverViolation.AAPoints = 0;
                    newDriverViolation.ViolationPoints = 0;
                    newDriverViolation.Chargeable = false;
                }
                currentViolationsForQuote.Add(newDriverViolation);
                newDriverViolation.Save();
                uow.CommitChanges();
                noHitViolationModel.DriverName = $"{driver.FirstName} {driver.LastName}";
                noHitViolationModel.DriversViolations.Add(newDriverViolation);
                vm.MvrDriverViolationsModels.Add(noHitViolationModel);
            }
        }
        public void CreateNoHitAPlus(string quoteNo,
            IList<PolicyQuoteDriversViolations> currentViolationsForQuote, IEnumerable<PolicyQuoteDrivers> drivers,
            QuoteIntegrationsViewModel vm, UnitOfWork uow)
        {
            //No-Hit
            foreach (var driver in drivers)
            {
                Support.DataObjects.XpoAARateViolations noHitRateViolation =
                    new XPCollection<Support.DataObjects.XpoAARateViolations>(uow,
                            CriteriaOperator.Parse("Code = ?", "UNV003"))
                        .FirstOrDefault();
                if (noHitRateViolation != null)
                {
                    APlusClaimModel noHitViolationModel = new APlusClaimModel();
                    int indexNo = currentViolationsForQuote.Count + 1;
                    int? driverIndex = null;
                    if (driver != null)
                    {
                        driverIndex = driver.DriverIndex;
                    }

                    PolicyQuoteDriversViolations newDriverViolation =
                        new PolicyQuoteDriversViolations(uow)
                        {
                            PolicyNo = quoteNo,
                            DriverIndex = driverIndex.GetValueOrDefault(),
                            ViolationPoints = Convert.ToInt32(noHitRateViolation.PointsFirst),
                            Chargeable = noHitRateViolation.Chargeable,
                            FromAPlus = true,
                            ViolationCode = noHitRateViolation.Code,
                            ViolationDate = DateTime.Now,
                            ViolationGroup = noHitRateViolation.ViolationGroup,
                            ViolationDesc = noHitRateViolation.LongDesc,
                            ViolationIndex = indexNo,
                            ViolationNo = indexNo
                        };

                    currentViolationsForQuote.Add(newDriverViolation);
                    noHitViolationModel.DriverName = $"{driver?.FirstName} {driver?.LastName}";
                    noHitViolationModel.ClaimDescription = "No-Hit";
                    if (driver.InternationalLic)
                    {
                        newDriverViolation.ViolationDesc = "INTERNATIONAL NO-HIT APLUS";
                        noHitViolationModel.ClaimDescription = "INTERNATIONAL NO-HIT APLUS";
                        newDriverViolation.AAPoints = 0;
                        newDriverViolation.ViolationPoints = 0;
                        newDriverViolation.Chargeable = false;
                    }
                    newDriverViolation.Save();
                    uow.CommitChanges();
                    vm.APlusClaimModels.Add(noHitViolationModel);
                }
            }
        }

        private void CreateCVNoHit(string quoteNo, IList<PolicyQuoteDriversViolations> currentViolationsForQuote,
            PolicyQuoteDrivers driver, QuoteIntegrationsViewModel vm, UnitOfWork uow)
        {
            //No-Hit
            Support.DataObjects.XpoAARateViolations noHitRateViolation =
                new XPCollection<Support.DataObjects.XpoAARateViolations>(uow,
                        CriteriaOperator.Parse("Code = ?", "UNV004"))
                    .FirstOrDefault();
            if (noHitRateViolation != null)
            {
                CoverageVerifierReportModel noHitViolationModel = new CoverageVerifierReportModel();
                int indexNo = currentViolationsForQuote.Count + 1;
                PolicyQuoteDriversViolations newDriverViolation;
                newDriverViolation = uow.FindObject<PolicyQuoteDriversViolations>(
                CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ? AND ViolationCode = ?", quoteNo, driver.DriverIndex, noHitRateViolation.Code));

                if (newDriverViolation == null)
                {
                    newDriverViolation = new PolicyQuoteDriversViolations(uow)
                    {
                        PolicyNo = quoteNo,
                        DriverIndex = driver.DriverIndex,
                        ViolationPoints = Convert.ToInt32(noHitRateViolation.PointsFirst),
                        Chargeable = noHitRateViolation.Chargeable,
                        ViolationCode = noHitRateViolation.Code,
                        ViolationDate = DateTime.Now,
                        FromCV = true,
                        ViolationGroup = noHitRateViolation.ViolationGroup,
                        ViolationDesc = noHitRateViolation.LongDesc,
                        ViolationIndex = indexNo,
                        ViolationNo = indexNo
                    };
                    currentViolationsForQuote.Add(newDriverViolation);
                    newDriverViolation.Save();
                    uow.CommitChanges();
                }
                noHitViolationModel.DriverName = $"{driver.FirstName} {driver.LastName}";
                noHitViolationModel.CarrierName = "No-Hit";
                vm.CoverageVerifierReportModels.Add(noHitViolationModel);

            }
        }

        private void CreateClearAPlusReportForQuote(string quoteNo,
            IList<PolicyQuoteDriversViolations> currentViolationsForQuote, PolicyQuoteDrivers driver,
            QuoteIntegrationsViewModel vm, UnitOfWork uow)
        {
            {
                int indexNo = currentViolationsForQuote.Count + 1;
                PolicyQuoteDriversViolations newDriverViolation =
                    new PolicyQuoteDriversViolations(uow)
                    {
                        PolicyNo = quoteNo,
                        DriverIndex = driver.DriverIndex,
                        FromAPlus = true,
                        Chargeable = true,
                        ViolationCode = "CLR001",
                        ViolationGroup = "CLEAR",
                        ViolationDesc = "Clear Report",
                        ViolationNo = indexNo,
                        ViolationIndex = indexNo,
                        ViolationDate = DateTime.Now
                    };
                APlusClaimModel model = new APlusClaimModel()
                {
                    DriverName = $"{driver.FirstName} {driver.LastName}",
                    ClaimDescription = newDriverViolation.ViolationDesc,
                    ClaimDateOfLoss = newDriverViolation.ViolationDate.ToString("yyyyMMdd")
                };
                vm.APlusClaimModels.Add(model);
                currentViolationsForQuote.Add(newDriverViolation);
                newDriverViolation.Save();
                uow.CommitChanges();
            }
        }

        public void HandleAPlusOrdering(XpoPolicyQuote quote, IEnumerable<PolicyQuoteDrivers> drivers,
            IList<PolicyQuoteDriversViolations> currentViolationsForQuote, IEnumerable<PolicyQuoteCars> cars,
            QuoteIntegrationsViewModel vm, out bool isViolationsDeleted)
        {
            isViolationsDeleted = false;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                PolicyQuoteDrivers driverForClaim = null;
                //Check for existing APlus data for quote
                if (currentViolationsForQuote.Any(cv => cv.FromAPlus) && quote.IsAllIntegrationCompleted)
                {
                    foreach (var driver in drivers)
                    {
                        foreach (PolicyQuoteDriversViolations violation in currentViolationsForQuote.Where(cv =>
                            cv.FromAPlus && cv.DriverIndex.Equals(driver.DriverIndex)))
                        {
                            APlusClaimModel existingAPlus = new APlusClaimModel()
                            {
                                DriverName = $"{driver.FirstName} {driver.LastName}",
                                ClaimDateOfLoss = violation.ViolationDate.ToString("yyyyMMdd"),
                                ClaimDescription = violation.ViolationDesc,
                                InsuredAtFault = violation.ViolationGroup.Equals("ACC")
                            };
                            vm.APlusClaimModels.Add(existingAPlus);
                        }
                    }
                }
                else
                {
                    // Remove Violations From APLus=true and RE-index ViolationIndex,Nos
                    if (currentViolationsForQuote.Any(cv => cv.FromAPlus))
                    {
                        isViolationsDeleted = true;
                        foreach (var currViolation in currentViolationsForQuote.Where(x => x.FromAPlus && !x.ViolationDesc.Contains("INTERNATIONAL NO-HIT APLUS")))
                        {
                            currViolation.Delete();
                        }

                        currentViolationsForQuote = currentViolationsForQuote.Where(x => !x.FromAPlus).ToList();

                        var driverIndexes = currentViolationsForQuote.Select(x => x.DriverIndex).Distinct();
                        foreach (var drivIndex in driverIndexes)
                        {
                            int violationIndex = 1;
                            foreach (var currViolation in currentViolationsForQuote.Where(x => x.DriverIndex == drivIndex).OrderBy(x => x.ViolationIndex))
                            {
                                currViolation.ViolationIndex = violationIndex++;
                                currViolation.ViolationNo = currViolation.ViolationIndex;
                            }

                        }
                    }
                    APlusReportModel model = new APlusReportModel();
                    if (!string.IsNullOrEmpty(quote.GarageStreet) && !string.IsNullOrEmpty(quote.GarageCity) &&
                        !string.IsNullOrEmpty(quote.GarageState) && !string.IsNullOrEmpty(quote.GarageZip))
                    {
                        model.Addresses.Add(new APlusAddressModel()
                        {
                            Street1 = quote.GarageStreet,
                            City = quote.GarageCity,
                            StateCode = quote.GarageState,
                            Zip = quote.GarageZip
                        });
                    }

                    if (!string.IsNullOrEmpty(quote.MailCity) && !string.IsNullOrEmpty(quote.MailCity) &&
                        !string.IsNullOrEmpty(quote.MailCity) && !string.IsNullOrEmpty(quote.MailCity))
                    {
                        if (!quote.GarageStreet.Equals(quote.MailStreet))
                        {
                            model.Addresses.Add(new APlusAddressModel()
                            {
                                Street1 = quote.MailStreet,
                                City = quote.MailCity,
                                StateCode = quote.MailState,
                                Zip = quote.MailZip
                            });
                        }
                    }

                    if (!string.IsNullOrEmpty(quote.HomePhone))
                    {
                        model.PhoneNumbers.Add(quote.HomePhone);
                    }

                    if (!string.IsNullOrEmpty(quote.WorkPhone))
                    {
                        model.PhoneNumbers.Add(quote.WorkPhone);
                    }

                    foreach (PolicyQuoteCars car in cars)
                    {
                        model.Vins.Add(car.VIN);
                    }

                    foreach (var driver in drivers)
                    {
                        model.Drivers.Add(driver);
                    }

                    VeriskAPlusCapWithClaimsBody aplusReportResult =
                        IntegrationsUtility.AplusReportWithClaims(quote.PolicyNo, model);
                    DateTime currentDate = DateTime.Today;
                    if (aplusReportResult != null && aplusReportResult.Claims?.Count() > 0)
                    {
                        DateTime threeYearsAgoFromEffectiveDate = quote.EffDate.AddMonths(-36).Date;
                        IList<PolicyQuoteDriversViolations> violationsWithinThreeYearsForDrivers =
                            new List<PolicyQuoteDriversViolations>();
                        int totalAccidentsAtFaultPastThreeYears = 0;
                        int totalAccidentsRegarlessOfFaultPastThreeYears = 0;
                        IDictionary<DateTime, IList<string>> dateOfLossAndCoverageInfo = new Dictionary<DateTime, IList<string>>();
                        foreach (var claim in aplusReportResult.Claims.Where(c => !c.MatchReasons.All(mr => mr.Code.Equals("V"))))
                        {
                            string atFaultGroup = "NAF";
                            driverForClaim = ClaimSubjectsContainsOurInsured(drivers, claim.Subjects);
                            if (driverForClaim != null)
                            {
                                DateTime lossDateTime = DateTime.ParseExact(claim.LossInformation?.LossDate,
                                    "yyyyMMdd", CultureInfo.InvariantCulture);
                                var losses = claim.LossInformation.Losses.Where(l =>
                                    l.DispositionStatus?.Code != null &&
                                    !l.DispositionStatus.Code.ToUpper().Equals("W")); // do not look at withdrawn losses
                                bool liabilityLossExists = losses.Any(l =>
                                    l.CoverageType.Code.Equals("LIAB") && !l.DispositionStatus.Code.Equals("W"));
                                string lossDescription = claim.CarrierClaimNumber + " (";
                                bool isWithinPastThreeYears =
                                    lossDateTime <= currentDate && lossDateTime >= threeYearsAgoFromEffectiveDate;
                                bool towingOnly = false;
                                bool compOnly = false;
                                bool sameDayClaimsExistsForCoverage = false;

                                if (!vm.APlusCarrierClaimNumbers.Contains(claim.CarrierClaimNumber))
                                {
                                    vm.APlusCarrierClaimNumbers.Add(claim.CarrierClaimNumber);
                                    vm.TotalNumberOfClaims++;
                                }

                                foreach (Loss loss in losses)
                                {
                                    towingOnly = loss.CoverageType.Code.Equals("TOWL") && losses.Count() == 1;
                                    compOnly = loss.CoverageType.Code.Equals("COMP") && losses.Count() == 1;
                                    lossDescription += $"{loss.LossType.Code}, ";
                                    decimal amount;
                                    Decimal.TryParse(loss.Amount, out amount);

                                    if (!isWithinPastThreeYears)
                                    {
                                        atFaultGroup = "EXCEPTION";
                                    }
                                    else if (compOnly)
                                    {
                                        atFaultGroup = "EXCEPTION";
                                    }
                                    else if (loss.DispositionStatus.Code.Equals("C") && amount == 0)
                                    {
                                        atFaultGroup = "NAF";
                                    }
                                    else if ((loss.CoverageType.Code.Equals("LIAB") ||
                                         loss.CoverageType.Code.Equals("COLL")) && amount > 0)
                                    {
                                        atFaultGroup = "ACC";
                                    }
                                    else if (loss.CoverageType.Code.Equals("PIP") && amount > 0)
                                    {
                                        vm.TotalPipClaimsPastThreeYearsForAllDrivers++;
                                        if (liabilityLossExists)
                                        {
                                            atFaultGroup = "ACC";
                                        }
                                    }
                                }

                                if (towingOnly)
                                {
                                    continue;
                                }

                                if (atFaultGroup.Equals("ACC") || atFaultGroup.Equals("NAF"))
                                {
                                    if (dateOfLossAndCoverageInfo.ContainsKey(lossDateTime))
                                    {
                                        sameDayClaimsExistsForCoverage = losses.Any(loss =>
                                            dateOfLossAndCoverageInfo[lossDateTime].Contains(loss.LossType.Code));
                                    }
                                    else
                                    {
                                        dateOfLossAndCoverageInfo.Add(lossDateTime,
                                            losses.Select(l => l.LossType.Code).Distinct().ToList());
                                    }
                                }

                                if (!sameDayClaimsExistsForCoverage)
                                {
                                    if (atFaultGroup.Equals("ACC"))
                                    {
                                        totalAccidentsAtFaultPastThreeYears++;
                                        totalAccidentsRegarlessOfFaultPastThreeYears++;
                                    }
                                    else if (atFaultGroup.Equals("NAF"))
                                    {
                                        totalAccidentsRegarlessOfFaultPastThreeYears++;
                                    }
                                }

                                if (losses.Any() && !towingOnly && !sameDayClaimsExistsForCoverage)
                                {
                                    if (lossDescription.EndsWith(", "))
                                    {
                                        int lastCommaIndex = lossDescription.LastIndexOf(", ");
                                        if (lastCommaIndex > -1)
                                        {
                                            lossDescription = lossDescription.Remove(lastCommaIndex, 2);
                                        }
                                    }

                                    lossDescription += ")";

                                    int indexNo = currentViolationsForQuote.Count + 1;
                                    string mappedViolationCode = "";
                                    if (atFaultGroup.Equals("ACC"))
                                    {
                                        mappedViolationCode = "ACC001";
                                    }
                                    else if (atFaultGroup.Equals("NAF"))
                                    {
                                        mappedViolationCode = "NAF008";
                                    }
                                    else
                                    {
                                        mappedViolationCode = "EXC100";
                                    }

                                    PolicyQuoteDriversViolations newViolation = new PolicyQuoteDriversViolations(uow)
                                    {
                                        PolicyNo = quote.PolicyNo,
                                        DriverIndex = driverForClaim.DriverIndex,
                                        FromAPlus = true,
                                        ViolationGroup = atFaultGroup,
                                        ViolationCode = mappedViolationCode,
                                        ViolationDesc = lossDescription,
                                        ViolationDate = DateTime.ParseExact(claim.LossInformation.LossDate,
                                            "yyyyMMdd", CultureInfo.InvariantCulture),
                                        ViolationIndex = indexNo,
                                        ViolationNo = indexNo,
                                    };
                                    currentViolationsForQuote.Add(newViolation);
                                    if (isWithinPastThreeYears)
                                    {
                                        violationsWithinThreeYearsForDrivers.Add(newViolation);
                                    }

                                    newViolation.Save();
                                    APlusClaimModel claimModel = new APlusClaimModel();
                                    claimModel.DriverName = $"{driverForClaim?.FirstName} {driverForClaim?.LastName}";
                                    claimModel.DriverIndex = driverForClaim.DriverIndex.ToString();
                                    claimModel.DateLicensed = driverForClaim.DateLicensed;
                                    claimModel.DateOfBirth = driverForClaim.DOB;
                                    claimModel.ClaimDescription = lossDescription;
                                    claimModel.ClaimDateOfLoss = claim.LossInformation.LossDate;
                                    claimModel.InsuredAtFault = (atFaultGroup == "ACC");

                                    vm.APlusClaimModels.Add(claimModel);
                                    if (totalAccidentsAtFaultPastThreeYears > 1)
                                    {
                                        vm.SingleDriverHas2OrMoreClaimsPastThreeYearsAtFault = true;
                                    }

                                    if (totalAccidentsRegarlessOfFaultPastThreeYears > 2)
                                    {
                                        vm.SingleDriverHasMoreThan3ClaimsPastThreeYearsRegardlessOfFault = true;
                                    }
                                }
                            }
                        }

                        if (vm.APlusClaimModels.Count == 0)
                        {
                            CreateNoHitAPlus(quote.PolicyNo, currentViolationsForQuote, drivers, vm, uow);
                            vm.APlusIsNoHit = true;
                        }

                        HandleAAPointsForAplusViolations(violationsWithinThreeYearsForDrivers, vm.APlusClaimModels, drivers);
                    }
                    else if (!currentViolationsForQuote.Any(v => v.DriverIndex.Equals(driverForClaim?.DriverIndex)))
                    {
                        CreateNoHitAPlus(quote.PolicyNo, currentViolationsForQuote, drivers, vm, uow);
                        vm.APlusIsNoHit = true;
                    }
                    else
                    {
                        CreateNoHitAPlus(quote.PolicyNo, currentViolationsForQuote, drivers, vm, uow);
                        vm.APlusIsNoHit = true;
                    }
                }
                uow.CommitChanges();
            }
        }

        public PolicyQuoteDrivers ClaimSubjectsContainsOurInsured(IEnumerable<PolicyQuoteDrivers> drivers, List<Subject> subjects)
        {
            PolicyQuoteDrivers matchedDriver = null;
            foreach (var subject in subjects)
            {
                foreach (var driver in drivers)
                {
                    bool isNameMatch = false;
                    if (!string.IsNullOrEmpty(driver.LicenseNo))
                    {
                        isNameMatch = driver.LicenseNo.Replace("-", "").Trim().ToUpper()
                            .Equals(subject.DLNumber?.Replace("-", "").Trim().ToUpper());
                    }

                    if (!isNameMatch)
                    {
                        using (var uow = XpoHelper.GetNewUnitOfWork())
                        {
                            string combinedInternalName = $"{driver.FirstName} {driver.LastName}";
                            string combinedName = $"{subject.GivenName} {subject.Surname}";
                            var result = uow.ExecuteSproc("spFunction_NameMatch", combinedInternalName,
                                combinedName);
                            isNameMatch = ((int)result.ResultSet[0].Rows.FirstOrDefault()?.Values[0] <= 2);
                        }
                    }
                    if (isNameMatch && (subject.RoleInClaim.Code.Equals("ID") || subject.RoleInClaim.Code.Equals("SA")))
                    {
                        matchedDriver = driver;
                    }
                }

            }

            return matchedDriver;
        }

        public void HandleAAPointsForAplusViolations(
            IEnumerable<PolicyQuoteDriversViolations> violationsWithin3YearsForCurrentDriver,
            IEnumerable<APlusClaimModel> aPlusClaimModels, IEnumerable<PolicyQuoteDrivers> drivers)
        {
            int totalViolationsForCurrentDirver = 0;
            foreach (var driver in drivers)
            {
                foreach (PolicyQuoteDriversViolations violation in violationsWithin3YearsForCurrentDriver.Where(v => v.DriverIndex.Equals(driver.DriverIndex)))
                {
                    totalViolationsForCurrentDirver++;
                    APlusClaimModel modelToUpdate = aPlusClaimModels.FirstOrDefault(apm =>
                        apm != null && !string.IsNullOrEmpty(apm.DriverName) &&
                        !string.IsNullOrEmpty(apm.ClaimDateOfLoss) &&
                        !string.IsNullOrEmpty(apm.ClaimDescription) &&
                        apm.ClaimDescription.Equals(violation.ViolationDesc) &&
                        apm.DriverName.ToLower().Contains(driver.FirstName.ToLower()) &&
                        apm.DriverName.ToLower().Contains(driver.LastName.ToLower()) &&
                        DateTime.ParseExact(apm.ClaimDateOfLoss,
                            "yyyyMMdd", CultureInfo.InvariantCulture).Equals(violation.ViolationDate));
                    if (modelToUpdate == null)
                    {
                        continue;
                    }

                    if (violation.ViolationGroup.Equals("ACC"))
                    {
                        if (totalViolationsForCurrentDirver == 1)
                        {
                            modelToUpdate.AAPoints = 3;
                        }
                        else if (totalViolationsForCurrentDirver > 1)
                        {
                            modelToUpdate.AAPoints = 4;
                        }
                    }
                }
            }
        }

        public void HandleCoverageVerifierOrdering(XpoPolicyQuote quote,
            IList<PolicyQuoteDriversViolations> currentViolationsForQuote, PolicyQuoteDrivers driver,
            QuoteIntegrationsViewModel vm)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                //Coverage Verifier
                if (quote.CvOrdered && !string.IsNullOrEmpty(quote.PreviousCompany))
                {
                    CoverageVerifierReportModel cvModel = new CoverageVerifierReportModel();
                    cvModel.DriverName = $"{driver.FirstName} {driver.LastName}";
                    cvModel.CarrierName = quote.PreviousCompany;
                    if (quote.PreviousExpDate != null && !quote.PreviousExpDate.Equals(DateTime.MinValue))
                    {
                        cvModel.PolicyExpiration = quote.PreviousExpDate.ToString("yyyyMMdd");
                    }

                    vm.CoverageVerifierReportModels.Add(cvModel);
                }
                else
                {
                    CoverageVerifierRequest data = new CoverageVerifierRequest();
                    data.Drivers = new List<Driver>();
                    data.Addresses = new List<Address>();
                    data.Drivers.Add(new AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Driver()
                    {
                        GivenName = driver.FirstName,
                        Surname = driver.LastName,
                        DriverLicenseNumber = driver.LicenseNo,
                        DriversLicenseState = driver.LicenseSt,
                        DateOfBirth = driver.DOB
                    });

                    if (!string.IsNullOrEmpty(quote.GarageStreet) && !string.IsNullOrEmpty(quote.GarageCity) &&
                        !string.IsNullOrEmpty(quote.GarageState) && !string.IsNullOrEmpty(quote.GarageZip))
                    {
                        data.Addresses.Add(new AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Address()
                        {
                            AddressType = "Current",
                            Street1 = quote.GarageStreet,
                            City = quote.GarageCity,
                            StateCode = quote.GarageState,
                            Zip = quote.GarageZip
                        });
                    }

                    if (!string.IsNullOrEmpty(quote.MailCity) && !string.IsNullOrEmpty(quote.MailCity) &&
                        !string.IsNullOrEmpty(quote.MailCity) && !string.IsNullOrEmpty(quote.MailCity))
                    {
                        if (!quote.GarageStreet.Equals(quote.MailStreet))
                        {
                            data.Addresses.Add(new AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Address()
                            {
                                AddressType = "Mailing",
                                Street1 = quote.MailStreet,
                                City = quote.MailCity,
                                StateCode = quote.MailState,
                                Zip = quote.MailZip
                            });
                        }
                    }

                    CoverageVerifierResponseBody coverageVerifierResult =
                        IntegrationsUtility.CoverageVerifierReport(quote.PolicyNo, data);
                    if (coverageVerifierResult.policies != null && coverageVerifierResult.policies.Any())
                    {
                        var policy = coverageVerifierResult.policies
                            .OrderByDescending(p => DateTime.ParseExact(
                                p.lastReportedTermExpirationDate, "yyyyMMdd",
                                CultureInfo.InvariantCulture)).FirstOrDefault();
                        var coverageLapseInfo = coverageVerifierResult.coverageLapseInformation.FirstOrDefault();
                        CoverageVerifierReportModel cvModel = new CoverageVerifierReportModel();
                        cvModel.DriverName = $"{driver.FirstName} {driver.LastName}";
                        cvModel.CarrierName = policy.detail.carrier.name;
                        cvModel.PolicyExpiration = policy.lastReportedTermExpirationDate;
                        vm.CoverageVerifierReportModels.Add(cvModel);


                        if (coverageLapseInfo != null && coverageLapseInfo.coverageIntervals?.FirstOrDefault() != null &&
                            !string.IsNullOrEmpty(coverageLapseInfo.coverageIntervals?.FirstOrDefault().carrier?.name))
                        {
                            quote.PreviousCompany = coverageLapseInfo?.coverageIntervals.FirstOrDefault().carrier?.name;

                        }
                        else
                        {
                            quote.PreviousCompany = policy.detail.carrier.name;
                        }

                        if (coverageLapseInfo != null && coverageLapseInfo.coverageIntervals?.FirstOrDefault() != null &&
                            !string.IsNullOrEmpty(coverageLapseInfo.coverageIntervals?.FirstOrDefault().endDate))
                        {
                            quote.PreviousExpDate = DateTime.ParseExact(
                                coverageLapseInfo?.coverageIntervals.FirstOrDefault().endDate, "yyyyMMdd",
                                CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            quote.PreviousExpDate = DateTime.ParseExact(
                                policy.lastReportedTermExpirationDate, "yyyyMMdd",
                                CultureInfo.InvariantCulture);
                        }

                        var biCoverage =
                            policy.detail.coverages.FirstOrDefault(
                                cv => cv.coverageType.code.Equals("BINJ"));
                        if (biCoverage != null)
                        {
                            quote.PreviousBI = true;
                            quote.PriorBILimit = biCoverage.occurrenceLimitAmount;
                        }

                        quote.CvOrdered = true;
                        quote.Save();

                    }
                    else
                    {
                        CreateCVNoHit(quote.PolicyNo, currentViolationsForQuote, driver, vm, uow);
                        if (String.IsNullOrEmpty(quote.PreviousCompany))
                            quote.PreviousCompany = "No-Hit";
                        quote.CvOrdered = true;
                        quote.Save();
                    }
                }
                uow.CommitChanges();
            }
        }

        public DateTime CalcExpDate(DateTime effDate, int termMonths)
        {
            DateTime effectiveDate = effDate;
            DateTime expDate;
            if (effectiveDate >= new DateTime(effectiveDate.Year, 8, 29) &&
                effectiveDate <= new DateTime(effectiveDate.Year, 8, 31))
            {
                expDate = termMonths == 6 ? effectiveDate.AddDays(184) : effectiveDate.AddMonths(termMonths);
            }
            else
            {
                expDate = effectiveDate.AddMonths(termMonths);
            }

            return expDate;
        } //END CalcExpDate
          ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private bool IsAgentRestrictedTo26Down()
        {
            bool isRestrictedTo26Down = false;

            return isRestrictedTo26Down;
        }

        public DataSet GetQuoteCoverageInfo(string policyno)
        {
            string query = "select l.IndexID, "
                           + "        case l.Coverage"
                           + "          when 'BI' then 'Bodily Injury'"
                           + "          when 'PIP' then 'Personal Injury Protection'"
                           + "          when 'PD' then 'Property Damage'"
                           + "          when 'UM' then 'Uninsured Motorist'"
                           + "          when 'MP' then 'Med Pay'"
                           + "          when 'COMP' then 'Comprehensive'"
                           + "          when 'COLL' then 'Collision'"
                           + "          when 'LOU' then 'Loss Of Use'"
                           + "          when 'ADND' then 'Accidental Death'"
                           + "          else ''"
                           + "        end as Coverage,"
                           + "             "
                           + "        case l.Coverage"
                           + "          when 'BI' then"
                           + "              case "
                           + "                when BILimit = '10/20' then cast('10,000/20,000' as varchar(50)) "
                           + "                else cast('N/A' AS varchar(50))"
                           + "              end "
                           + "          when 'PD' then '10,000' "
                           + "          when 'PIP' then '10,000, '+CAST(PIPDed AS varchar(20))+' Deduct '+"
                           + "                                    case when NIO=1 then 'NIO' else case when NIRR=1 then 'NIRR' else '' end end "
                           + "          when 'UM' then "
                           + "              case "
                           + "                when UMLimit = '10/20' then cast('10,000/20,000' as varchar(50)) +"
                           + "                                    case when UMStacked=1 then ' Stacked' else ' Unstacked' end "
                           + "                else cast('N/A' AS varchar(50))"
                           + "              end  "
                           + "          when 'MP' then"
                           + "              case "
                           + "                when MedPayLimit!='' and MedPayLimit!='0' then CAST(MedPayLimit AS varchar(30)) else ''"
                           + "              end"
                           + "          when 'COMP' then"
                           + "              case when exists (select 1 from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='')  "
                           + "                   then (select MAX(CompDed) from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='')+' Deduct, ACV up to $45k' else '' "
                           + "              end"
                           + "          when 'COLL' then"
                           + "              case when exists (select 1 from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='')  "
                           + "                   then (select MAX(CompDed) from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed!='')+' Deduct, ACV up to $45k' else '' "
                           + "              end"
                           + "          when 'LOU' then "
                           + "              case   "
                           + "                   when HasLOU=1 then '$20/Day Max $600' else 'N/A' "
                           + "              end"
                           + "          when 'ADND' then "
                           + "              case   "
                           + "                   when ADNDLimit>0 then CAST(ADNDLimit as varchar(20)) else 'N/A' "
                           + "              end    "
                           + "        end as LimitDesc,"
                           + "                                     "
                           + "        case l.Coverage"
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'')  "
                           + "          when 'LOU' then ISNULL(p.LOUCost,'')  "
                           + "          when 'ADND' then ISNULL(p.ADNDCost,'')  "
                           + "        end as 'Vehicle 1', "
                           + "             "
                           + "        case l.Coverage "
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') "
                           + "          when 'LOU' then '' "
                           + "          when 'ADND' then '' "
                           + "        end as 'Vehicle 2', "
                           + "         "
                           + "        case l.Coverage "
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') "
                           + "          when 'LOU' then '' "
                           + "          when 'ADND' then '' "
                           + "        end as 'Vehicle 3',"
                           + "         "
                           + "        case l.Coverage"
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') "
                           + "          when 'LOU' then ''"
                           + "          when 'ADND' then '' "
                           + "        end as 'Vehicle 4',"
                           + "         "
                           + "        case l.Coverage"
                           + "          when 'BI' then ISNULL((Select c2.BIWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'PIP' then ISNULL((Select c2.PIPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'')"
                           + "          when 'PD' then ISNULL((Select c2.PDWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'UM' then ISNULL((Select c2.UMWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'MP' then ISNULL((Select c2.MPWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'COMP' then ISNULL((Select c2.CompWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'COLL' then ISNULL((Select c2.CollWritten from PolicyQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') "
                           + "          when 'LOU' then ''"
                           + "          when 'ADND' then '' "
                           //					+ "        end as 'Vehicle 5', FHCFFee, PolicyWritten, PolicyFee, mvrfee, DBSetupfee, SR22fee "
                           + "        end as 'Vehicle 5', PIPPDOnlyFee, PolicyWritten, PolicyFee, mvrfee, MaintenanceFee, SR22fee "
                           + "             "
                           + "   from PolicyQuote p, AARateCoverages l"
                           + " where p.PolicyNo='@PolicyNo'"
                           + "        and l.Coverage!='RENTAL'"
                           + "  UNION "
                           + "  Select 99 as IndexID,"
                           + "         '' as Coverage,"
                           + "         'Vehicle Totals:' as LimitDesc,"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=1),0)+"
                           + "         ISNULL((Select isNULL(LOUCost,0)+isNULL(ADNDCost,0) from PolicyQuote where PolicyNo='@PolicyNo'),0) as 'Vehicle 1',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=2),0) as 'Vehicle 2',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=3),0) as 'Vehicle 3',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=4),0) as 'Vehicle 4',"
                           + "         ISNULL((Select BIWritten+PDWritten+PIPWritten+MPWritten+UMWritten+CompWritten+CollWritten+isNULL(StatedAmtCost,0) from PolicyQuoteCars where PolicyNo='@PolicyNo' and CarIndex=5),0) as 'Vehicle 5',"
                           + "         '', '', '', '', '', ''"
                           + " order by IndexID";
            query = query.Replace("@PolicyNo", policyno.ToString());

            SqlDataReader viewCoverageInfo = Database.ExecuteQuery(query, CommandType.Text);

            DataSet CoverageDataSet = new DataSet();
            CoverageDataSet.Load(viewCoverageInfo, LoadOption.OverwriteChanges, "QuoteCoverageTable");
            return CoverageDataSet;
        }

        public EndorsementPolicyModel CreateEndorsement(UnitOfWork uow, out XPCollection<PolicyEndorseQuoteDrivers> drivers, out PolicyEndorseQuote endorsement,
            double oldPrem, double newPrem, string policyNo, string garageStreetAddress, string garageCity,
            string garageState, string garageZIPCode, string mailingStreetAddress, string mailingCity,
            string mailingState, string mailingZIPCode, string pipDeductible, string biLimit,
            bool? workLoss, string pdLimit, bool? NIO, string medPayLimit, bool? NIRR, string umLimit, string adndLimit,
            bool? hasLou, string insFirstName, string insuredLastName, string ins2FirstName, string ins2LastName, bool? hasPPO,
            string homePhone = null, string workPhone = null, string email = null)
        {
            endorsement =
                        uow.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyNo), false);
            XpoPolicy existingPolicy = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo), false);
            oldPrem = existingPolicy.PolicyWritten;
            // Save any changes
            endorsement.Ins1Last = insuredLastName.SafeTrim().ToUpper(); //LF 11/25/18 added ToUpper()s
            endorsement.Ins1First = insFirstName.SafeTrim().ToUpper();
            endorsement.Ins1FullName =
                $"{endorsement.Ins1Last}, {endorsement.Ins1First}"; //changed from insuredLastName, insFirstName
            endorsement.Ins2First = ins2FirstName.SafeTrim();
            endorsement.Ins2Last = ins2LastName.SafeTrim();
            endorsement.Ins2FullName = $"{endorsement.Ins2Last}, {endorsement.Ins2First}";
            endorsement.GarageStreet = garageStreetAddress.SafeTrim().ToUpper();
            endorsement.GarageCity = garageCity.SafeTrim().ToUpper();
            endorsement.GarageState = garageState.SafeTrim();
            endorsement.GarageZip = garageZIPCode.SafeTrim();
            endorsement.MailStreet = mailingStreetAddress.SafeTrim().ToUpper();
            endorsement.MailCity = mailingCity.SafeTrim().ToUpper();
            endorsement.MailState = mailingState;
            endorsement.MailZip = mailingZIPCode.SafeTrim();
            endorsement.PIPDed = pipDeductible;
            endorsement.BILimit = biLimit;
            endorsement.WorkLoss = Convert.ToInt32(workLoss);
            endorsement.PDLimit = pdLimit;
            endorsement.NIO = (bool)NIO;
            endorsement.MedPayLimit = medPayLimit;
            endorsement.NIRR = (bool)NIRR;

            //if (transDisc == null)
            //{
            //    endorsement.TransDisc = (int)TransferDiscountOptions.None;
            //}
            //else
            //{
            //    endorsement.TransDisc = Convert.ToInt32(transDisc);
            //}
            if (hasPPO == null)
            {
                endorsement.HasPPO = false;
            }
            else
            {
                endorsement.HasPPO = hasPPO.Value;
            }
            //if (hasPaperlessDiscount == null || hasPaperlessDiscount.GetValueOrDefault() == false)
            //{
            //    endorsement.Paperless = false;
            //}
            //else
            //{
            //    endorsement.Paperless = true;
            //}
            //if (hasAdvancedQuotesDiscount == null || hasAdvancedQuotesDiscount.GetValueOrDefault() == false)
            //{
            //    endorsement.AdvancedQuote = false;
            //}
            //else
            //{
            //    endorsement.AdvancedQuote = true;
            //}

            if (!string.IsNullOrEmpty(homePhone))
            {
                endorsement.HomePhone = homePhone;
            }
            if (!string.IsNullOrEmpty(workPhone))
            {
                endorsement.WorkPhone = workPhone;
            }
            if (!string.IsNullOrEmpty(email))
            {
                endorsement.MainEmail = email;
            }
            endorsement.ADNDLimit = Convert.ToInt32(adndLimit);
            //endorsement.TransDisc = (int)Enum.Parse(typeof(TransferDiscountOptions), transDisc);
            //endorsement.Homeowner = (bool)hasHomeownersDisc;
            endorsement.DirectRepairDisc = false; // (bool)hasDirectRepairDisc;
            endorsement.HasLOU = (bool)hasLou;
            endorsement.UMStacked = false; // (bool)umStacked;
            endorsement.UMLimit = umLimit;
            XPCollection<PolicyEndorseQuoteCars> cars =
                new XPCollection<PolicyEndorseQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo));
            drivers =
                new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", policyNo));
            XPCollection<PolicyEndorseQuoteDriversViolations> driversViolations =
                new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", policyNo));
            EndorsementPolicyModel model = UtilitiesRating.MapXpoToModel(endorsement);
            model.Vehicles = new List<EndorsementVehicleModel>();
            model.Drivers = new List<EndorsementDriverModel>();
            foreach (PolicyEndorseQuoteCars car in cars)
            {
                //if (car.HasPhyDam)
                //{
                //    car.CompDed = compDed;
                //    car.CollDed = collDed;
                //    LogUtils.Log(Sessions.Instance.Username,
                //        string.Format("Quote {0} has PhyDam: Comp Ded: {1}, Coll Ded: {2}".ToUpper(), policyNo,
                //            compDed, collDed), "FATAL");
                //}

                model.Vehicles.Add(UtilitiesRating.MapXpoToModel(car));
            }

            //foreach (EndorsementVehicleModel carModel in model.Vehicles)
            //{
            //    PolicyEndorseQuoteCars xpo = uow.FindObject<PolicyEndorseQuoteCars>(
            //        CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", policyNo, carModel.CarIndex));
            //    xpo.PolicyNo = carModel.PolicyID;
            //    UtilitiesRating.MapModelToXpo(carModel, xpo);
            //    if (xpo.HasPhyDam)
            //    {
            //        xpo.CompDed = compDed;
            //        xpo.CollDed = collDed;
            //    }
            //    xpo.Save();
            //    uow.CommitChanges();
            //}

            return model;
        }

        public void CreatePolicyEndorsementQuote(UnitOfWork uow, string policyNo, string fieldName, string fieldValue)
        {
            PolicyEndorseQuote quote =
                    new XPCollection<PolicyEndorseQuote>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
            if (quote != null)
            {
                fieldValue = fieldValue.SafeTrim();
                switch (fieldName)
                {
                    case "FirstName1":
                        quote.Ins1First = fieldValue;
                        quote.Ins1FullName = $"{quote.Ins1Last?.ToUpper()}, {quote.Ins1First?.ToUpper()} {quote.Ins1MI?.ToUpper()}";
                        break;
                    case "MiddleInitial1":
                        quote.Ins1MI = fieldValue;
                        quote.Ins1FullName = $"{quote.Ins1Last?.ToUpper()}, {quote.Ins1First?.ToUpper()} {quote.Ins1MI?.ToUpper()}";
                        break;
                    case "LastName1":
                        quote.Ins1Last = fieldValue;
                        quote.Ins1FullName = $"{quote.Ins1Last?.ToUpper()}, {quote.Ins1First?.ToUpper()} {quote.Ins1MI?.ToUpper()}";
                        break;
                    case "Suffix1":
                        quote.Ins1Suffix = fieldValue;
                        break;
                    case "FirstName2":
                        quote.Ins2First = fieldValue;
                        quote.Ins2FullName = $"{quote.Ins2Last?.ToUpper()}, {quote.Ins2First?.ToUpper()}";
                        break;
                    case "MiddleInitial2":
                        quote.Ins2MI = fieldValue;
                        break;
                    case "LastName2":
                        quote.Ins2Last = fieldValue;
                        quote.Ins2FullName = $"{quote.Ins2Last?.ToUpper()}, {quote.Ins2First?.ToUpper()}";
                        break;
                    case "Suffix2":
                        quote.Ins2Suffix = fieldValue;
                        break;
                    case "HomePhone":
                        quote.HomePhone = fieldValue;
                        break;
                    case "WorkPhone":
                        quote.WorkPhone = fieldValue;
                        break;
                    case "GarageStreetAddress":
                        quote.GarageStreet = fieldValue;
                        if (quote.MailGarageSame)
                            quote.MailStreet = fieldValue;
                        break;
                    case "GarageCity":
                        if (!fieldValue.ToUpper().Equals("FIND BY ZIP"))
                        {
                            quote.GarageCity = fieldValue;
                        }
                        if (quote.MailGarageSame)
                            quote.MailCity = fieldValue;
                        break;
                    case "GarageState":
                        quote.GarageState = fieldValue;
                        if (quote.MailGarageSame)
                            quote.MailState = fieldValue;
                        break;
                    case "GarageZIPCode":
                        quote.GarageZip = fieldValue;
                        if (quote.MailGarageSame)
                            quote.MailZip = fieldValue;
                        break;
                    case "MailingStreetAddress":
                        quote.MailStreet = fieldValue;
                        break;
                    case "MailingCity":
                        if (!fieldValue.ToUpper().Equals("FIND BY ZIP"))
                        {
                            quote.MailCity = fieldValue;
                        }
                        break;
                    case "MailingState":
                        quote.MailState = fieldValue;
                        break;
                    case "MailingZIPCode":
                        quote.MailZip = fieldValue;
                        break;
                    case "Mail_Same_As_Garage":
                        quote.MailGarageSame = Convert.ToBoolean(fieldValue);
                        break;
                    case "PipDeductible":
                        quote.PIPDed = fieldValue;
                        break;
                    case "EmailAddress":
                        quote.MainEmail = fieldValue;
                        break;
                    case "WorkLoss":
                        bool hasWorkLoss = Convert.ToBoolean(fieldValue);
                        quote.WorkLoss = Convert.ToInt32(hasWorkLoss);
                        break;
                    case "WorkLossGroup":
                        bool isNio = Convert.ToBoolean(fieldValue);
                        if (isNio)
                        {
                            quote.NIO = true;
                            quote.NIRR = false;
                        }
                        else
                        {
                            quote.NIO = false;
                            quote.NIRR = true;
                        }
                        break;
                    case "Transfer_Discount":
                        quote.TransDisc = Convert.ToInt32(fieldValue);
                        break;
                    case "Homeowners_Discount":
                        quote.Homeowner = Convert.ToBoolean(fieldValue);
                        break;
                    case "Paperless_Discount":
                        quote.Paperless = Convert.ToBoolean(fieldValue);
                        break;
                    case "Advanced_Quote_Discount":
                        quote.AdvancedQuote = Convert.ToBoolean(fieldValue);
                        break;
                    case "Has_PPO":
                        quote.HasPPO = Convert.ToBoolean(fieldValue);
                        break;
                    default:
                        break;
                }
                quote.Save();
                uow.CommitChanges();
            }
        }

        public QuoteIntegrationsViewModel CreateQuoteIntegrations(UnitOfWork uow, string quoteNo, bool isRiskCheck = false, bool orderCoverageVerifierOnly = false)
        {
            XPCollection<PolicyQuoteDrivers> drivers = null;

            XPCollection<PolicyQuoteDrivers> excludedDrivers = null;
            XPCollection<PolicyQuoteCars> cars = null;

            QuoteIntegrationsViewModel vm = new QuoteIntegrationsViewModel();
            vm.QuoteNo = quoteNo;
            
                IList<PolicyQuoteDriversViolations> existingViolations = new List<PolicyQuoteDriversViolations>();
                XPCollection<ViolationCodes_SVC> violationCodes = new XPCollection<ViolationCodes_SVC>(uow);
                drivers = new XPCollection<PolicyQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND Exclude = 0", quoteNo));
                cars = new XPCollection<PolicyQuoteCars>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                XPCollection<PolicyQuoteDriversViolations> currentViolationsForQuote =
                    new XPCollection<PolicyQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                excludedDrivers = new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Exclude = 1", quoteNo));
                foreach (PolicyQuoteDriversViolations existingViolation in currentViolationsForQuote)
                {
                    existingViolations.Add(existingViolation);
                }

                XpoPolicyQuote quote = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                var isViolationsDeleted = false;
                if (drivers.Any())
                {
                    if (isRiskCheck)
                    {
                        HandleRiskCheckPOSForQuotes(quoteNo, drivers, excludedDrivers, cars, quote, currentViolationsForQuote, vm, uow);
                    }
                    else if (orderCoverageVerifierOnly)
                    {

                        //CV
                        if (!string.IsNullOrEmpty(quote?.Ins1First) && !string.IsNullOrEmpty(quote?.Ins1Last))
                        {
                            var primaryDriver = drivers.FirstOrDefault(d => d.DriverIndex.Equals(1));
                            if (primaryDriver != null && quote.Ins1First.ToUpper().Equals(primaryDriver.FirstName.ToUpper()) &&
                                quote.Ins1Last.ToUpper().Equals(primaryDriver.LastName.ToUpper()))
                            {
                                HandleCoverageVerifierOrdering(quote, existingViolations, primaryDriver, vm);
                            }
                        }
                    }
                    else
                    {

                        //APLUS
                        //Ensure no-hits aplus recoreds are added for any international drivers
                        if (drivers.Any(d => d.InternationalLic))
                        {
                            CreateNoHitAPlus(quoteNo, existingViolations, drivers.Where(d => d.InternationalLic), vm, uow);
                        }

                        HandleAPlusOrdering(quote, drivers.Where(d => !d.InternationalLic), existingViolations, cars, vm, out isViolationsDeleted);

                        foreach (PolicyQuoteDrivers driver in drivers)
                        {
                            if (vm.IsValidToProceeed())
                            {
                                if (driver.InternationalLic)
                                {
                                    CreateNoHitMVRForQuotes(quoteNo, existingViolations, driver, vm, uow);
                                }
                                else
                                {
                                    _reportService.HandleMvrOrderingForQuotes(quote, driver, existingViolations, violationCodes, vm);
                                }
                            }
                        }

                    }
                }
                else
                {
                    drivers = new XPCollection<PolicyQuoteDrivers>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND Exclude = 0 AND InternationalLic = 1", quoteNo));
                    CreateNoHitAPlus(quoteNo, existingViolations, drivers, vm, uow);
                    if (isRiskCheck)
                    {
                        HandleRiskCheckPOSForQuotes(quoteNo, drivers, excludedDrivers, cars, quote, currentViolationsForQuote, vm, uow);
                    }
                    else
                    {
                        foreach (PolicyQuoteDrivers driver in drivers)
                        {
                            CreateNoHitMVRForQuotes(quoteNo, existingViolations, driver, vm, uow);
                            HandleCoverageVerifierOrdering(quote, existingViolations, driver, vm);
                        }
                    }
                }

            // call policy score sproc if risk check and coverage verifier have been ordered
            if (vm.RiskCheckOrdered && vm.CoverageVerifierOrdered())
            {
                bool isNewBusinessAllowed = false;
                OperandValue newBusinessAllowed = new OperandValue(isNewBusinessAllowed);
                var result = uow.ExecuteSproc("spUW_GetPolicyScore", vm.QuoteNo,
                    Sessions.Instance.Username, newBusinessAllowed);
                if (result?.ResultSet?[1] != null && result.ResultSet[1].Rows[0] != null &&
                    result.ResultSet[1].Rows[0].Values[1] != null)
                {
                    isNewBusinessAllowed = (bool)result.ResultSet[1].Rows[0].Values[1];
                    vm.PolicyScoreValidForBinding = isNewBusinessAllowed;
                }
                else
                {
                    vm.PolicyScoreValidForBinding = false;
                }
            }

            if (isViolationsDeleted)
                DbHelperUtils.DeleteGcRecord("PolicyQuoteDriversViolations", quoteNo);

            return vm;
        }

        public Dictionary<int, string> CreateDamagedCarQuote(UnitOfWork uow, string termMonths, XpoPolicyQuote quoteUpdate, bool hasPhyDam, string quoteno, 
            string pipDeductible, string transferDiscountValue, bool hasWorkLossYesSelected, bool hasNamedInsuredOnlyChecked, bool hasHomeOwnersDiscount, bool hasPPO, 
            bool? hasPaperlessDiscount = false, bool? hasadvancedQuoteDiscount = false, bool emailQuote = false, string city = null, string state = null, string county = null,
            bool? iscurrentCustomerChecked = false, string priorCoverageTerm = null, bool mailSameAsGarage = true)
        {
            string strRateCycle = !String.IsNullOrEmpty(quoteUpdate.RateCycle) ? quoteUpdate.RateCycle : DbHelperUtils.getMaxRecID("AARatePayPlans", "RateCycle");

            XPCollection<Support.DataObjects.XpoAARatePayPlans> getPayPlans = new XPCollection<Support.DataObjects.XpoAARatePayPlans>(uow);

            //if(quoteUpdate.Territory.Equals())

            getPayPlans.Criteria = CriteriaOperator.Parse("RateCycle = ? AND Term = ? AND DP_AsPerc = '1' AND NewOrRen <> 'R'",
                strRateCycle, termMonths);
            getPayPlans.Reload();


            quoteUpdate.PIPDed = pipDeductible;
            quoteUpdate.TransDisc = Convert.ToInt32(transferDiscountValue);

            if (mailSameAsGarage)
            {
                quoteUpdate.MailStreet = quoteUpdate.GarageStreet;
                quoteUpdate.MailZip = quoteUpdate.GarageZip;

            }

            if (!string.IsNullOrEmpty(city))
            {
                quoteUpdate.GarageCity = city;
                if (mailSameAsGarage)
                {
                    quoteUpdate.MailCity = city;
                }
            }
            if (!string.IsNullOrEmpty(state))
            {
                quoteUpdate.GarageState = state;
                if (mailSameAsGarage)
                {
                    quoteUpdate.MailState = state;
                }
            }
            if (!string.IsNullOrEmpty(county))
            {
                quoteUpdate.GarageCounty = county;
            }
            if (hasWorkLossYesSelected)
            {
                quoteUpdate.WorkLoss = 1;
            }
            else
            {
                quoteUpdate.WorkLoss = 0;
            }
            if (hasNamedInsuredOnlyChecked == true)
            {
                quoteUpdate.NIO = true;
                quoteUpdate.NIRR = false;
            }
            else
            {
                quoteUpdate.NIRR = true;
                quoteUpdate.NIO = false;
            }

            quoteUpdate.IsCurrentCustomer = iscurrentCustomerChecked.GetValueOrDefault();
            quoteUpdate.HasPPO = hasPPO;
            quoteUpdate.Homeowner = hasHomeOwnersDiscount;
            if (hasPaperlessDiscount == null || !hasPaperlessDiscount.GetValueOrDefault())
            {
                quoteUpdate.Paperless = false;
            }
            else
            {
                quoteUpdate.Paperless = true;
            }

            if (hasadvancedQuoteDiscount == null || !hasadvancedQuoteDiscount.GetValueOrDefault())
            {
                quoteUpdate.AdvancedQuote = false;
            }
            else
            {
                quoteUpdate.AdvancedQuote = true;
            }
            if (quoteUpdate.PayPlan == 0)
            {
                quoteUpdate.PayPlan = getPayPlans.FirstOrDefault().IndexID;
            }
            quoteUpdate.PriorPolicyTerm = priorCoverageTerm;

            string term = "6";
            if (!quoteUpdate.SixMonth)
            {
                term = "12";
            }

            var payPlansDict = new Dictionary<int, string>();
            string currentRateCycle = !String.IsNullOrEmpty(quoteUpdate.RateCycle) ? quoteUpdate.RateCycle : DbHelperUtils.getMaxRecID("AARatePayPlans", "RateCycle");
            bool requires33PercentDown =
                PortalUtils.AgentAndZipRequires33PercentDown(quoteUpdate.AgentCode, quoteUpdate.GarageZip,
                    hasPhyDam);
            bool requiresHighDownPayment = quoteUpdate.PolicyScore >= 20;
            IList<XpoAARatePayPlans> allPayPlans = new List<XpoAARatePayPlans>();
            bool territoryAndZipHasLowerDownPay =
                PortalUtils.ZipHasLowerDownPayment(currentRateCycle, quoteUpdate.GarageZip);
            List<XpoAARatePayPlans> tempPayPlans = new List<XpoAARatePayPlans>();

            if (territoryAndZipHasLowerDownPay && !requiresHighDownPayment && !requires33PercentDown)
            {
                allPayPlans =
                    new XPCollection<XpoAARatePayPlans>(uow,
                        CriteriaOperator.Parse("RateCycle = ? AND Term = ? AND BI = 0 AND NewOrRen <> 'R'", currentRateCycle,
                            term)).OrderBy(aa => aa.DP_AsPerc).ToList();
            }
            else
            {
                allPayPlans =
                    new XPCollection<XpoAARatePayPlans>(uow,
                        CriteriaOperator.Parse("RateCycle = ? AND Term = ? AND BI = 0 AND NewOrRen <> 'R' AND (RequiresTerritoryApproval IS NULL OR RequiresTerritoryApproval = 0)", currentRateCycle,
                            term)).OrderBy(aa => aa.DP_AsPerc).ToList();
            }

            for (int i = 0; i < allPayPlans.Count; i++)
            {
                double payPlanDownPayment = allPayPlans[i].DP_AsPerc * 100;
                int installmentCount = (int)allPayPlans[i].Installments;
                bool EFT = allPayPlans[i].EFT == 1;
                string payPlanInstallments = installmentCount == 1
                    ? string.Format("{0} installment", installmentCount)
                    : string.Format("{0} installments", installmentCount);
                string payPlanEnglish =
                    EFT
                        ? String.Format("{0}% down with {1} (EFT)", payPlanDownPayment, payPlanInstallments)
                        : String.Format("{0}% down with {1}", payPlanDownPayment, payPlanInstallments);

                if (requiresHighDownPayment)
                {
                    if (allPayPlans[i].DP_AsPerc.Equals(1))
                    {
                        if (!payPlansDict.ContainsKey(allPayPlans[i].IndexID))
                        {
                            payPlansDict.Add(allPayPlans[i].IndexID, payPlanEnglish);
                            tempPayPlans.Add(allPayPlans[i]);
                        }
                    }
                }
                else if (requires33PercentDown && !requiresHighDownPayment)
                {
                    if ((allPayPlans[i].DP_AsPerc.Equals(0.332) ||
                         allPayPlans[i].DP_AsPerc.Equals(1)))
                    {
                        if (!payPlansDict.ContainsKey(allPayPlans[i].IndexID))
                        {
                            payPlansDict.Add(allPayPlans[i].IndexID, payPlanEnglish);
                            tempPayPlans.Add(allPayPlans[i]);
                        }
                    }
                }
                else
                {
                    if (!payPlansDict.ContainsKey(allPayPlans[i].IndexID))
                    {
                        payPlansDict.Add(allPayPlans[i].IndexID, payPlanEnglish);
                        tempPayPlans.Add(allPayPlans[i]);
                    }
                }
            }

            //if our selected pay plan does not equal allowed list, then default it. 
            if ((requiresHighDownPayment || requires33PercentDown) && !tempPayPlans.Exists(pp => pp.IndexID.Equals(quoteUpdate.PayPlan)))
            {
                //default to 33% plan
                //TODO: update when we sell BI
                XpoAARatePayPlans defaultPlan = null;
                if (requiresHighDownPayment)
                {
                    defaultPlan = tempPayPlans.FirstOrDefault(pp => pp.DP_AsPerc.Equals(1));
                    if (defaultPlan != null && defaultPlan.DP_AsPerc.Equals(1))
                    {
                        if (!tempPayPlans.Exists(pp => pp.DP_AsPerc.Equals(1)))
                        {
                            tempPayPlans.Add(defaultPlan);
                        }
                    }
                }
                else if (requires33PercentDown)
                {
                    if (requires33PercentDown && !requiresHighDownPayment)
                    {
                        defaultPlan = tempPayPlans.FirstOrDefault(pp => pp.DP_AsPerc.Equals(0.332));
                        if (defaultPlan != null && defaultPlan.DP_AsPerc.Equals(0.332))
                        {
                            if (!tempPayPlans.Exists(pp => pp.DP_AsPerc.Equals(0.332)))
                            {
                                tempPayPlans.Add(defaultPlan);
                            }
                        }
                        defaultPlan = tempPayPlans.FirstOrDefault(pp => pp.DP_AsPerc.Equals(1));
                        if (defaultPlan != null && defaultPlan.DP_AsPerc.Equals(1))
                        {
                            if (!tempPayPlans.Exists(pp => pp.DP_AsPerc.Equals(1)))
                            {
                                tempPayPlans.Add(defaultPlan);
                            }
                        }
                    }
                }
                quoteUpdate.PayPlan = defaultPlan?.IndexID ?? tempPayPlans.FirstOrDefault().IndexID;
                quoteUpdate.Save();
                uow.CommitChanges();
            }

            quoteUpdate.Save();
            uow.CommitChanges();

            return payPlansDict;
        }

        public string WritePolicyInstallments(UnitOfWork uow, string quoteno, DataSet coverages, string policyWritten, Dictionary<int, string> payPlansDict, 
            QuoteModel quoteModel, Guid installGuid, string rtnHtml, string msgreturn, bool hasPhyDam, bool emailQuote, string returnHtmlFinal)
        {
            foreach (DataRow dr in coverages.Tables["QuoteCoverageTable"].Rows)
            {
                if (!dr["LimitDesc"].ToString().Contains("Vehicle"))
                {
                    policyWritten = dr["PolicyWritten"] == DBNull.Value ? "" : dr["PolicyWritten"].ToString();
                }
            }

            StringBuilder selectHtmlSB = new StringBuilder("<select name='paymentPlansDropDown' id='paymentPlansDropDown'> ");
            foreach (var kvp in payPlansDict)
            {
                if (kvp.Key.Equals(quoteModel.PayPlan))
                {
                    selectHtmlSB.Append($"<option value={kvp.Key} selected='selected'>{kvp.Value}</option>");
                }
                else
                {
                    selectHtmlSB.Append($"<option value={kvp.Key}>{kvp.Value}</option>");
                }
            }
            selectHtmlSB.Append("</select>");

            rtnHtml = string.Format(@"<table class='tbl100'>
									<thead>					
	                                <tr>
                                        <th colspan='5'>Payment Plan Options: {0}</th>                                       
									</tr>
									<tr class='tblHeaderRec'>
										<td style='width: 120px;'>
											Description
										</td>
										<td style='width: 120px;'>
											Date Due
										</td>
										<td style='width: 120px;'>
											Premium Due
										</td>
										<td style='width: 120px;'>
											Installment Fee
										</td>
										<td colspan='3' style='width: 120px;'>
											Amount Due
										</td>
									</tr>
								</thead>
								<tbody>", selectHtmlSB);

            XPCollection<RatingInstallmentsDO> getInstallments =
                new XPCollection<RatingInstallmentsDO>(uow, CriteriaOperator.Parse("GUID = ?", installGuid));
            foreach (RatingInstallmentsDO install in getInstallments)
            {
                string premDue = install.DownPymtAmount > 0
                    ? install.DownPymtAmount.ToString("C")
                    : (install.InstallmentPymt - install.InstallmentFee).ToString("C");
                string amtDue = install.DownPymtAmount > 0
                    ? install.DownPymtAmount.ToString("C")
                    : install.InstallmentPymt.ToString("C");
                string installFee = string.IsNullOrEmpty(install.InstallmentFee.ToString("C"))
                    ? "0.00"
                    : install.InstallmentFee.ToString("C");

                rtnHtml += string.Format(@"<tr>
														<td>
															{0}
														</td>
														<td>
															{1}
														</td>
														<td>
															{2}
														</td>
														<td>
															{3}
														</td>
														<td>
															{4}
														</td>
													</tr>", install.Descr, install.DueDate.ToShortDateString(),
                    premDue, installFee, amtDue);
            }

            string policyWrittenTotal = Convert.ToDecimal(policyWritten).ToString("C");
            rtnHtml += string.Format(@" <tr>
                                                    <td colspan='3'></td>
													<td><b>Total Policy</b></td>
													<td>{0}</td>
												</tr>
												<tr>
													<td  colspan='5' style='text-align: center;'><b>Rates subject to change</b></td>
												</tr>
											</tbody>
										</table>", policyWrittenTotal);
            msgreturn = "SUCCESSFUL";
            returnHtmlFinal = rtnHtml;
            if (emailQuote && !string.IsNullOrEmpty(returnHtmlFinal))
            {
                IList<string> emailAddresses = new List<string>();
                string agentEmailAddress =
                    new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?",
                            Sessions.Instance.AgentCode))
                        .FirstOrDefault().EmailAddress;
                if (string.IsNullOrEmpty(agentEmailAddress))
                {
                    agentEmailAddress = "technical.support@palminsure.com";
                }
                else
                {
                    emailAddresses.Add(agentEmailAddress);
                }

                int indexOfSelectEnd = rtnHtml.IndexOf("</select>");
                int indexOfSelectStart = rtnHtml.IndexOf("<select");
                rtnHtml = rtnHtml.Remove(indexOfSelectStart, (indexOfSelectEnd - indexOfSelectStart) + 9);
                rtnHtml = rtnHtml.Replace("Payment Plan Options: ", $"Payment Plan Options: {payPlansDict.FirstOrDefault(pp => pp.Key.Equals(quoteModel.PayPlan)).Value}");
                _emailService.SendEmailNotification(emailAddresses, $"Alert Auto Quick Quote - {quoteno}",
                    rtnHtml,
                    true);
                //rtnHtml += $"</br><p>Email successfully sent to {agentEmailAddress}.</p>";
            }
            return returnHtmlFinal;
        }
    }
}