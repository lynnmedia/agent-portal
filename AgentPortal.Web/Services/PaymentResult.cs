﻿namespace AgentPortal.Services
{
    public class PaymentResult
    {
        public bool Success { get; set; }
        public string Amount { get; set; }
        public string FailureReason { get; set; }
        public string ReceiptLink { get; set; }
    }
}