﻿namespace AgentPortal.Services
{
    public enum PaymentTypes
    {
        InitialDepositPayment,
        EndorsementPayment,
        ReinstatementPayment,
        RenewalPayment,
        InstallmentPayment,
        TitanEndorsementPayment
    }
}