﻿using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using AgentPortal.Support.Utilities.LexisNexis;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using log4net;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Xpo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Blowfish = AgentPortal.Support.Utilities.Blowfish;

namespace AgentPortal.Services
{
    public class DocumentService
    {
        private readonly ILog logger;
        private WebHostService _webHostService;
        public EmailService _emailService;
        private readonly Blowfish _blowfish;

        public DocumentService(WebHostService webHostService, EmailService emailService, Blowfish blowfish)
        {
            _webHostService = webHostService;
            _emailService = emailService;
            _blowfish = blowfish;
            logger = LogManager.GetLogger("DocumentService");
        }
        public NewPolicyDocumentsStatusModel AuditPolicyDocDownloads(UnitOfWork uow, String policyNo)
        {
            NewPolicyDocumentsStatusModel documentStatusModel = new NewPolicyDocumentsStatusModel();
            XPCollection<AuditImageIndex> newPolicyDocuments = new XPCollection<AuditImageIndex>(uow, CriteriaOperator.Parse("FileNo = ? AND IsNewPolicyDocument = 1", policyNo));
            bool isEftPolicy = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo)).EFT;
            AuditImageIndex policyDeclarations = newPolicyDocuments.FirstOrDefault(npd =>
                npd.FormDesc.ToUpper().Contains("POLICY DECLARATIONS") && npd.IsNewPolicyDocument);
            if (policyDeclarations != null)
            {
                documentStatusModel.PolicyDeclarationsFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{policyDeclarations.FileName.Replace("/", "")}";
            }

            AuditImageIndex idCards = newPolicyDocuments.FirstOrDefault(npd =>
                npd.FormDesc.ToUpper().Contains("ID CARDS") && npd.IsNewPolicyDocument);
            if (idCards != null)
            {
                documentStatusModel.IDCardsFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{idCards.FileName.Replace("/", "")}";
            }

            AuditImageIndex eft = newPolicyDocuments.FirstOrDefault(npd =>
                npd.FormDesc.ToUpper().Contains("EFT") && npd.IsNewPolicyDocument);
            if (isEftPolicy && eft != null)
            {
                documentStatusModel.EFTFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{eft.FileName.Replace("/", "")}";
            }

            AuditImageIndex applicationCover = newPolicyDocuments.FirstOrDefault(npd =>
                npd.FormDesc.ToUpper().Contains("APPLICATION COVER") && npd.IsNewPolicyDocument);
            if (applicationCover != null && !string.IsNullOrEmpty(applicationCover.FileName))
            {
                documentStatusModel.ApplicationCoverFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{applicationCover.FileName.Replace("/", "")}";
            }

            AuditImageIndex insuranceApplication = newPolicyDocuments.FirstOrDefault(npd =>
                !npd.FormDesc.ToUpper().Contains("APPLICATION COVER") &&
                npd.FormDesc.ToUpper().Contains("APPLICATION") && npd.IsNewPolicyDocument);
            if (insuranceApplication != null)
            {
                documentStatusModel.ApplicationForInsuranceFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{insuranceApplication.FileName.Replace("/", "")}";
            }

            AuditImageIndex paymentSchedule = newPolicyDocuments.FirstOrDefault(npd =>
                npd.FormDesc.ToUpper().Contains("PAYMENT SCHEDULE") && npd.IsNewPolicyDocument);
            if (paymentSchedule != null)
            {
                documentStatusModel.PaymentScheduleFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{paymentSchedule.FileName.Replace("/", "")}";
            }

            AuditImageIndex paymentReceipt = newPolicyDocuments.FirstOrDefault(npd =>
                npd.FormDesc.ToUpper().Contains("PAYMENT RECEIPT") && npd.IsNewPolicyDocument);
            if (paymentReceipt != null)
            {
                documentStatusModel.PaymentReceiptFile =
                    $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{paymentReceipt.FileName.Replace("/", "")}";
            }

            string newPolicyDocsFileName = $"{policyNo}_NewPolicyDocs.pdf";
            string newPolicyDocs = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\PDFS\{newPolicyDocsFileName}";
            if (System.IO.File.Exists(newPolicyDocs))
            {
                documentStatusModel.NewPolicyDocumentsFile = $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Files/{newPolicyDocsFileName.Replace("/", "")}";
            }

            documentStatusModel.IsEftPolicy = isEftPolicy;
            documentStatusModel.IsAllDocumentsGenerated = documentStatusModel.AllDocumentsGenerated(isEftPolicy);

            return documentStatusModel;
        }

        public void CreateLexisDocument(UnitOfWork uow, XpoPolicyQuote update)
        {
            var lexis = new LexisWriter
            {
                ClientId = "IP1053600",
                QuoteBackName = "caseId",
                RulePlanName = "",
                NcfType = "Independent Agent",
                SubjectId = "s1",
                CreditVendor = "Experian",
                ModelId = "Attract",
                Format = "CP XML",
                AcctNo = "770201TST",
                QuoteBackVal = "Test01XML_INT Cond01"
            };
            //------------------
            lexis.Create("order");
            lexis.AddNode("client", new XAttribute("id", lexis.ClientId), null, "order");
            lexis.AddNode("quoteback", lexis.QuoteBackVal, new XAttribute("name", lexis.QuoteBackName), null,
                "client");
            lexis.AddNode("accounting", null, null, "order");
            lexis.AddNode("pnc", null, null, "accounting");
            lexis.AddNode("account", lexis.AcctNo, null, "pnc");
            lexis.AddNode("rule_plan", null, null, "order");
            //lexis.AddNode("parameter", new XAttribute("name", lexis.RulePlanName), null, "rule_plan");
            lexis.AddNode("products", null, null, "order");
            //lexis.AddNode("national_credit_file", new XAttribute("primary_subject", lexis.SubjectId), null, "products");
            lexis.AddNode("carrier_discovery_live", new XAttribute("subject", lexis.SubjectId), null, "products");
            lexis.AddNode("reportType", "B", null, null, "carrier_discovery_live");
            //lexis.AddNode("type", lexis.NcfType, null, null, "national_credit_file");
            //lexis.AddNode("report_code", "2121", null, null, "national_credit_file");
            //lexis.AddNode("vendor", lexis.CreditVendor, null, null, "national_credit_file");
            //lexis.AddNode("format", lexis.Format, null, null, "national_credit_file");
            //lexis.AddNode("model_id", lexis.ModelId, null, null, "national_credit_file");
            lexis.AddNode("dataset", null, null, "order");
            lexis.AddNode("subjects", null, null, "dataset");
            lexis.AddNode("subject", new XAttribute("id", lexis.SubjectId), null, "subjects");
            lexis.AddNode("name", null, null, "subject");
            lexis.AddNode("prefix", null, null, "name");
            lexis.AddNode("first", update.Ins1First, null, null, "name");
            lexis.AddNode("middle", "", null, null, "name");
            lexis.AddNode("last", update.Ins1Last, null, null, "name");
            lexis.AddNode("suffix", "", null, null, "name");
            lexis.AddNode("maiden", "", null, null, "name");
            //lexis.AddNode("ssn", null, null, "subject");

            lexis.AddNode("vehicles", null, null, "dataset");
            lexis.AddNode("vehicle", new XAttribute("id", "V1"), null, "vehicles");
            lexis.AddNode("year", null, null, "vehicle");
            lexis.AddNode("make", null, null, "vehicle");
            lexis.AddNode("model", null, null, "vehicle");
            lexis.AddNode("vin", null, null, "vehicle");

            lexis.AddNode("addresses", null, null, "dataset");
            lexis.AddNode("address", new XAttribute("id", "A2"), null, "addresses");
            lexis.AddNode("house",
                update.MailStreet.Contains(" ") ? update.MailStreet.Split(' ')[0] : update.MailStreet, null, null,
                "address");
            lexis.AddNode("street1",
                update.MailStreet.Contains(" ")
                    ? update.MailStreet.Replace(update.MailStreet.Split(' ')[0], "")
                    : update.MailStreet, null, null, "address");
            lexis.AddNode("city", update.MailCity, null, null, "address");
            lexis.AddNode("state", update.MailState, null, null, "address");
            lexis.AddNode("postalcode", update.MailZip, null, null, "address");

            string NodeExpression = @"//a:order/a:dataset/a:subjects/a:subject";

            XDocument Doc = XDocument.Parse(lexis.Write().ToString());
            XmlNamespaceManager NamespaceMgr = new XmlNamespaceManager(Doc.CreateReader().NameTable);
            NamespaceMgr.AddNamespace("a", "http://cp.com/rules/client");
            var node = Doc.XPathSelectElement(NodeExpression, NamespaceMgr);
            node.Add(new XElement(lexis.Namespace + "address", new XAttribute("ref", "A2"),
                new XAttribute("type", "residence")));

            string soapEnvelope = lexis.BuildSoapEnv(Doc.ToString());

            var processor = new LexisProcessor
            {
                UserAgent = "Palm Insure",
                ContentType = "application/soap+xml",
                RequestMethod = "POST",
                ServiceUrl = @"https://twebxml.choicepoint.com/agic",
                SendMsg = soapEnvelope
            };
            processor.Send();
            processor.Receive();

            var reader = new LexisReader(LexisFormat.Edits)
            {
                EditsMsgLines = processor.ReturnMsg.Split('\n')
            };

            string headerInfo = reader.GetEditsScalar(LexisEditsValues.HeaderRecord);
            string creditscore = reader.GetEditsScalar(LexisEditsValues.CreditScore);
            string exclusionCode = reader.GetEditsScalar(LexisEditsValues.ErrCode);

            if (creditscore.Trim().All(char.IsDigit))
            {
                Console.WriteLine("Credit Score: " + creditscore);
                update.CreditScore = creditscore.Parse<int>();
            }
            else
            {
                Console.WriteLine("Exclusion Code: " + exclusionCode);
                update.CreditScore = -1;
            }


            string guid = Guid.NewGuid().ToString();

            foreach (var line in reader.EditsMsgLines)
            {
                using (var uow2 = XpoHelper.GetNewUnitOfWork())
                {
                    AgentPortal.Support.DataObjects.XpoAuditLexisCreditResponse save = new AgentPortal.Support.DataObjects.XpoAuditLexisCreditResponse(uow2);

                    save.GuidCredit = guid;
                    save.Response = line;
                    save.DateCreated = DateTime.Now;
                    save.Save();
                    uow2.CommitChanges();
                }
            }

            using (var uow3 = XpoHelper.GetNewUnitOfWork())
            {
                XpoAuditLexisCreditRequest save = new XpoAuditLexisCreditRequest(uow3);
                save.Request = soapEnvelope;
                save.DateCreated = DateTime.Now;
                save.GuidCredit = guid;
                save.Save();
                uow3.CommitChanges();
            }

            update.CreditGuid = guid;
            update.Save();
            uow.CommitChanges();
        }

        public string CreatePolicyDocs(string id, PalmsServiceClient psc)
        {
            string quoteNo = null; //Issues
            string policyNo = null;
            string subject = null;
            string body = null;
            bool isIssued = true;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    quoteNo = _blowfish.Decrypt_CBC(id);
                }
                catch
                {
                    quoteNo = id;
                }

                XpoPolicyQuote quoteExists =
                    new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo))
                        .FirstOrDefault();

                if (quoteExists != null)
                {
                    UtilitiesObj Utils = new UtilitiesObj();
                    policyNo = Utils.GeneratePolicyNumber();
                    Sessions.Instance.PolicyNo = policyNo;
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format($"{policyNo}: GENERATING POLICY DOCUMENTS. PolicyGenerationMeasurementGPD - Start Time: {DateTime.Now}", policyNo), "INFO");

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format($"{Sessions.Instance.PolicyNo}: PolicyGenerationMeasurementIQ - Start Time: {DateTime.Now}", Sessions.Instance.PolicyNo), "INFO");
                    psc.IssueQuote(policyNo, quoteNo, Sessions.Instance.Username, Guid.NewGuid().ToString());
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format($"{Sessions.Instance.PolicyNo}: PolicyGenerationMeasurementIQ - End Time: {DateTime.Now}", Sessions.Instance.PolicyNo), "INFO");

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format($"{Sessions.Instance.PolicyNo}: PolicyGenerationMeasurementIP - Start Time: {DateTime.Now}", Sessions.Instance.PolicyNo), "INFO");
                    psc.IssuePolicy(policyNo, Sessions.Instance.Username, Guid.NewGuid().ToString());
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format($"{Sessions.Instance.PolicyNo}: PolicyGenerationMeasurementIP - END Time: {DateTime.Now}", Sessions.Instance.PolicyNo), "INFO");

                    LogUtils.Log(Sessions.Instance.Username, $"PolicyGenerationMeasurementRIPA, Policy#: {Sessions.Instance.PolicyNo}. StartTime: {DateTime.Now}", "INFO");
                    psc.RunInstallmentsPaymentApply(policyNo, DateTime.Now, Sessions.Instance.Username,
                        Guid.NewGuid().ToString());
                    LogUtils.Log(Sessions.Instance.Username, $"PolicyGenerationMeasurementRIPA, Policy#: {Sessions.Instance.PolicyNo}. EndTime: {DateTime.Now}", "INFO");

                    try
                    {
                        uow.BeginTransaction();

                        int historyIndex = 0;

                        XPCollection<AuditPolicyHistory> getHistory =
                            new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo));

                        try
                        {
                            historyIndex = getHistory.Min(m => m.IndexID);
                        }
                        catch (NullReferenceException)
                        {
                            string.Format("[NULLREFERENCE] Could not grab history IndexID for quote: {0} and PolicyNo: {1} ",
                                quoteNo, policyNo).Log("FATAL");
                        }
                        catch (Exception)
                        {
                            string.Format("[NULLREFERENCE] Could not grab history IndexID for quote: {0} and PolicyNo: {1} ",
                                quoteNo, policyNo).Log("FATAL");
                        }
                        try
                        {
                            Task.Run(() =>
                            {
                                IntegrationsUtility.GenerateNewPolicyDocuments(policyNo, historyIndex.ToString());
                            }).ConfigureAwait(false);
                            SSRSHelpers ssrsHelpers = new SSRSHelpers();
                            ssrsHelpers.GenerateAplusReport(policyNo, Sessions.Instance.Username, quoteNo, uow);
                            ssrsHelpers.GenerateMVRs(policyNo, Sessions.Instance.Username, quoteNo, uow);
                            ssrsHelpers.GenerateRiskCheckReport(policyNo, Sessions.Instance.Username, quoteNo, uow);
                            ssrsHelpers.GenerateDriversNotAssociatedReport(policyNo, Sessions.Instance.Username, uow);
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: GENERATED FORMS USING HISTORY INDEX {1}", Sessions.Instance.PolicyNo,
                                    historyIndex), "INFO");
                        }
                        catch (NullReferenceException ex)
                        {
                            string.Format(
                                "[NULLREFERENCE] Could not generate Documents for IndexID for quote: {0} and PolicyNo: {1} and History IndexID: {2} - StackTrace: {3} ",
                                quoteNo, policyNo, historyIndex, ex.StackTrace).Log("FATAL");
                        }
                        catch (Exception ex)
                        {
                            string.Format(
                                "[NULLREFERENCE] Could not generate Documents for IndexID for quote: {0} and PolicyNo: {1} and History IndexID: {2} - StrackTrace: {3} ",
                                quoteNo, policyNo, historyIndex, ex.StackTrace).Log("FATAL");
                        }
                        XpoPolicy updatePolicy = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                        updatePolicy.DateBound = DateTime.Now;
                        updatePolicy.CommAtIssue = AgentUtils.GetAgentComm(updatePolicy.AgentCode) == 0.0
                            ? .15
                            : AgentUtils.GetAgentComm(updatePolicy.AgentCode);
                        updatePolicy.IsShow = true;
                        updatePolicy.QuoteNo = quoteNo;
                        updatePolicy.AllowPymntWithin15Days = true;
                        updatePolicy.AgentESignEmailAddress = quoteExists.AgentESignEmailAddress;
                        updatePolicy.Save();
                        uow.CommitChanges();

                        IList<XpoPolicyPaymentProfile> profilesToUpdate =
                            new XPCollection<XpoPolicyPaymentProfile>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                        if (profilesToUpdate.Any())
                        {
                            foreach (var profile in profilesToUpdate)
                            {
                                profile.PolicyNo = policyNo;
                                profile.LastUpdatedByUserName = Sessions.Instance.Username;
                                profile.LastUpdatedDate = DateTime.Now;
                                profile.Save();
                            }
                        }

                        IList<PolicyPreferences> preferencesToUpdate = new XPCollection<PolicyPreferences>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                        if (preferencesToUpdate.Any())
                        {
                            foreach (var preference in preferencesToUpdate)
                            {
                                preference.PolicyNo = policyNo;
                                preference.Save();
                            }
                        }

                        IList<AuditImageIndex> imagesWithQuoteIdToUpdate =
                            new XPCollection<AuditImageIndex>(uow, CriteriaOperator.Parse("FileNo = ?", quoteNo));
                        if (imagesWithQuoteIdToUpdate.Any())
                        {
                            foreach (var imageToUpdate in imagesWithQuoteIdToUpdate)
                            {
                                imageToUpdate.FileNo = policyNo;
                                imageToUpdate.Save();
                            }
                        }
                        if (!string.IsNullOrEmpty(quoteExists.ReWrittenFromPolicyNo))
                        {
                            var rewritePolicyNotes =
                               new XPCollection<AuditRewritePolicyReasons>(uow, CriteriaOperator.Parse("PolicyNo = ? AND IsNote = 1", quoteNo));
                            if (rewritePolicyNotes.Any())
                            {
                                foreach (var rewritePolicyNote in rewritePolicyNotes)
                                {
                                    rewritePolicyNote.PolicyNo = policyNo;
                                    rewritePolicyNote.Save();
                                    var policyNote = new PolicyNotesDO(new DevExpress.Xpo.Session());
                                    policyNote.Notes = rewritePolicyNote.Reason;
                                    policyNote.PolicyNo = policyNo;
                                    policyNote.Date = DateTime.Now;
                                    policyNote.UserBy = Sessions.Instance.Username;
                                    policyNote.Save();
                                }
                            }
                        }

                        uow.CommitChanges();
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: SET DATE BOUND ON POLICY", Sessions.Instance.PolicyNo), "INFO");

                        PolicyESignatureRequests request =
                            new XPCollection<PolicyESignatureRequests>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", quoteNo)).FirstOrDefault();
                        //If record exists, requesting esignature is implicit.
                        if (request != null)
                        {
                            _emailService.RequestESignaturesForPolicy(policyNo);
                            request.Delete();
                            uow.CommitChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: ERROR OCCURED SETTING DATE BOUND", Sessions.Instance.PolicyNo),
                            "FATAL");
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: ERROR: " + ex, Sessions.Instance.PolicyNo), "FATAL");
                    }

                    isIssued = false;
                }
                else
                {
                    XpoPolicy getQuoteNo = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo))
                        .FirstOrDefault();
                    if (getQuoteNo == null)
                    {
                        throw new HttpException(404, "Quote Not Found");
                    }

                    policyNo = getQuoteNo.PolicyNo;
                    isIssued = true;
                }

                XpoPolicy getPolicyInfo = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .FirstOrDefault();

                if (getPolicyInfo != null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: LOAD POLICY INFO GENERATING FORMS", Sessions.Instance.PolicyNo), "INFO");
                    string agentcode = getPolicyInfo.AgentCode;
                    string policyTotal = getPolicyInfo.PolicyWritten.ToString();
                    string insuredName = getPolicyInfo.Ins1First + " " + getPolicyInfo.Ins1Last;
                    string raterSource = string.IsNullOrEmpty(getPolicyInfo.FromRater)
                        ? "PORTAL"
                        : getPolicyInfo.FromRater;
                    string agentName = null;
                    string bodytitle = null;
                    //----------------
                    XpoAgents getAgent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ? ", agentcode))
                        .FirstOrDefault();
                    if (getAgent != null)
                    {
                        agentName = getAgent.PrimaryFirstName + " " + getAgent.PrimaryLastName;
                    }

                    //----------------
                    // bodytitle = IsDev() ? "[TEST ENVIRONMENT] Policy Issued" : "Policy Issued"; //Issue: Dev vs Poduction?
                    bodytitle = "Policy Issued";
                    body = EmailRenderer.PolicyIssued(bodytitle, policyNo, insuredName, agentcode, agentName,
                        policyTotal, raterSource);
                    subject = string.Format("[PORTAL] Policy {0} was issued from portal.", policyNo);

                    if (!isIssued)
                    {
                        try
                        {
                            //if (IsProduction())
                            //{
                            string sendTo = "newbusiness@palminsure.com";
                            _emailService.SendEmailNotification(new List<string>() { sendTo, "robert.eikill@alertauto.com", "cliff.karlin@palminsure.com" }, subject, body,
                                true);
                            //}
                        }
                        catch (Exception ex)
                        {
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: ERROR OCCURED SENDING POLICY ISSUED MAIL. {1}",
                                    Sessions.Instance.PolicyNo, ex), "FATAL");
                        }

                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: SENT EMAIL POLICY ISSUED EMAIL", Sessions.Instance.PolicyNo), "INFO");
                    }

                    if (!string.IsNullOrEmpty(getPolicyInfo.MainEmail))
                    {
                        try
                        {
                            // Send client portal email
                            MailAddress emailAddress = new MailAddress(getPolicyInfo.MainEmail);
                            XpoClientLogin existingClientAccount =
                                uow.FindObject<XpoClientLogin>(CriteriaOperator.Parse("EmailAddress = ?",
                                    getPolicyInfo.MainEmail));
                            if (existingClientAccount == null)
                            {
                                using (SHA256 hashTool = SHA256.Create())
                                {
                                    string guidString = Guid.NewGuid().ToString();
                                    string hashedGuid = _blowfish.Encrypt_CBC(guidString);
                                    XpoClientLogin newClientLogin = new XpoClientLogin(uow)
                                    {
                                        EmailAddress = emailAddress.Address,
                                        ValidationCode = guidString,
                                        HashedPassword = ""
                                    };
                                    newClientLogin.Save();
                                    uow.CommitChanges();
                                    string link =
                                        $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Client/SetPassword?key={hashedGuid}";
                                    _emailService.SendEmailNotification(new List<string>() { emailAddress.Address },
                                        "Access your new auto insurance policy with Palm Insure!",
                                        $@"<div><p>Please click the link below to finish setting up your account to access our client portal and maintain your paperless discount.  The client portal is where you can access your new auto insurance policy documents and make payments.</p></div><br/><div><p>Policy #: {getPolicyInfo.PolicyNo} </p></div> <div> <a href='{link}'>Click here to access your account.</a> </div><br/>",
                                        true);

                                }
                            }
                            else
                            {
                                XpoAgents agentInfo =
                                    uow.FindObject<XpoAgents>(CriteriaOperator.Parse("AgentCode = ?",
                                        getPolicyInfo.AgentCode));
                                if (agentInfo != null)
                                {
                                    //if (IsProduction())
                                    //{
                                    _emailService.SendEmailNotification(
                                        new List<string>() { agentInfo.EmailAddress },
                                        $"Warning - Cannot create login for Client. Policy#: {getPolicyInfo.PolicyNo}",
                                        $@"<div><p>Unable to create a login for EmailAddress: {getPolicyInfo.MainEmail}. An existing client account was already found for this insured's email address.</a> </div><br/>",
                                        true);
                                    //}
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            // invalid email address if we get here.
                            logger.Warn($"Invalid Email Address provided for Policy#: {getPolicyInfo.PolicyNo}, EmailAddress: {getPolicyInfo.MainEmail}");
                            _emailService.SendEmailNotification(new List<string>() { getAgent.EmailAddress },
                                $"Invalid Client Email Address for Policy#: {getPolicyInfo.PolicyNo}",
                                $"<div><p>An invalid email address was entered for Policy#: {getPolicyInfo.PolicyNo}. The insured will not be able to access the insured portal, and if a paperless discount was selected, it will be removed after 24 hours unless a valid email addresss is updated.</p></div>",
                                true);

                        }
                    }
                }
            }
            LogUtils.Log(Sessions.Instance.Username, $"GENERATING POLICY DOCUMENTS. PolicyNo: {Sessions.Instance.PolicyNo} PolicyGenerationMeasurementGPD - End Time: {DateTime.Now}", "INFO");

            psc.CloseRef();

            return policyNo;
        }

        public void SaveToImaging(UploadFileDo upload, bool isPhysicalDamageImage)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string server = ConfigSettings.ReadSetting("IIS_SERVER");
                string saveFileName = string.Format(@"\\{0}\PALMS\apps\pImaging\Imaging\Images\{1}", server,
                    upload.FileName);

                using (Stream stream = new FileStream(saveFileName, FileMode.Create))
                {
                    stream.Write(Convert.FromBase64String(upload.FileUploadBase64), 0,
                        Convert.FromBase64String(upload.FileUploadBase64).Length);
                }

                string indexInfoPath = string.Format(@"\\{0}\palms\apps\pImaging\Imaging\IndexInfo\{1}_{2}_{3}.xml",
                    server,
                    upload.FileNo,
                    CleanString(upload.FormDesc),
                    DateTime.Now.ToString("yyMMddhhmmss"));

                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = upload.FileNo;
                Images.Dept = upload.Department;
                Images.FormType = upload.FormType;
                Images.FormDesc = upload.FormDesc;
                Images.FileFolder = upload.FileFolder;
                Images.FileName = upload.FileName;
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = upload.Username;
                Images.Thumbnail = upload.ThumbnailPath;
                Images.IsDiary = true;
                Images.Category = upload.Category;
                Images.PortalCode = upload.PortalCode;
                Images.PerilDesc = upload.PerilDesc;
                Images.IndexNo = upload.IndexNo;
                Images.IsPhysicalDamageImage = isPhysicalDamageImage;
                Images.Save();
                uow.CommitChanges();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = upload.FileNo;
                Index.historyID = null;
                Index.department = upload.Department;
                Index.formType = upload.FormType;
                Index.formDesc = upload.FormDesc;
                Index.fileFolder = upload.FileFolder;
                Index.fileName = upload.FileName;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = upload.Username;
                Index.thumbnail = upload.ThumbnailPath;
                Index.isDiary = true;
                Index.category = upload.Category;
                Index.portalcode = upload.PortalCode;
                Index.PerilDesc = upload.PerilDesc;
                Index.IndexNo = upload.IndexNo;
                Index.Create(indexInfoPath);
                Index.Dispose(Index);
            }
        }

        public void UploadImages(string PolicyNo, List<HttpPostedFileBase> files, log4net.ILog quoteControllerLog)
        {
            List<string> listFiles = new List<string>();
            listFiles.Clear();
            int count = 0;
            if (files != null)
            {
                foreach (HttpPostedFileBase file in files)
                {
                    if (file != null)
                    {
                        if (file.ContentLength > 0)
                        {
                            string fileName = Path.GetFileName(file.FileName);
                            string path =
                                Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), fileName);
                            file.SaveAs(path);
                            string origFilename = path;
                            if (System.IO.File.Exists(path))
                            {
                                string filename = string.Format(@"{0}{1}", Guid.NewGuid().ToString(),
                                    Path.GetExtension(fileName));
                                System.IO.File.Move(path,
                                    Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), filename));
                                path = Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), filename);
                                listFiles.Add(path);

                                var upload = new UploadFileDo();
                                upload.FileName = $"{PolicyNo}_{DateTime.Now.Ticks}_{Path.GetFileName(origFilename)}";
                                upload.FileFolder = string.Format(@"\\{0}\PALMS\apps\pImaging\Imaging\Images\",
                                    ConfigSettings.ReadSetting("IIS_SERVER"));
                                upload.Category = "Agent Uploads";
                                upload.FileNo = Sessions.Instance.PolicyNo;
                                upload.Department = "UNDERWRITING";
                                upload.FormType = Path.GetExtension(filename);
                                upload.FormDesc = "VEHICLE PHYSICAL DAMAGE IMAGE";
                                upload.Username = Sessions.Instance.Username;
                                upload.IndexNo = count.SafeString();
                                try
                                {
                                    using (Stream stream = new FileStream(path, FileMode.Open))
                                    {
                                        byte[] uploadBytes = new byte[stream.Length - 1];
                                        stream.Read(uploadBytes, 0, uploadBytes.Length);

                                        upload.FileUploadBase64 = Convert.ToBase64String(uploadBytes);
                                    }


                                    SaveToImaging(upload, true);
                                    PortalUtils.PostAgentUploadRec(Sessions.Instance.PolicyNo, upload.FileName,
                                        fileName, path,
                                        "VEHICLE PHYSICAL DAMAGE UPLOAD", Sessions.Instance.Username);
                                }
                                catch (Exception ex)
                                {
                                    quoteControllerLog.Error(ex.Message, ex);
                                }

                                count += 1;
                            }
                        }
                    }
                }
            }
        }

        // Issue: Not being used
        public void FTPFile(string FTPAddress, string filePath, string username, string password)
        {
            try
            {
                FtpWebRequest request =
                    (FtpWebRequest)FtpWebRequest.Create(FTPAddress + "/" + Path.GetFileName(filePath));

                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = false;
                request.KeepAlive = false;

                FileStream stream = System.IO.File.OpenRead(filePath);
                byte[] buffer = new byte[stream.Length];

                stream.Read(buffer, 0, buffer.Length);
                stream.Close();

                Stream reqStream = request.GetRequestStream();
                reqStream.Write(buffer, 0, buffer.Length);
                reqStream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("[FATAL] Error occured FTPing File: " + ex);
                FtpWebRequest reqFTP =
                    (FtpWebRequest)FtpWebRequest.Create(FTPAddress + "/" + Path.GetFileName(filePath));

                reqFTP.Credentials = new NetworkCredential(username, password);
                reqFTP.KeepAlive = false;
                reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;

                string result = String.Empty;
                FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse();
                long size = response.ContentLength;
                Stream datastream = response.GetResponseStream();
                StreamReader sr = new StreamReader(datastream);
                result = sr.ReadToEnd();
                sr.Close();
                datastream.Close();
                response.Close();
            }
        }

        public string CleanString(string cleanString = null)
        {
            string value = "";

            if (!string.IsNullOrEmpty(cleanString))
            {
                value = Regex.Replace(cleanString, @"\W", "", RegexOptions.IgnoreCase);
            }

            return value;
        }

        public void UploadImage(string AgentCode, string PolicyNo, IEnumerable<HttpPostedFileWrapper> files)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var isSendMail = false;
                var listFiles = new List<string>();
                listFiles.Clear();
                var count = 0;
                if (files != null)
                {
                    foreach (var file in files)
                        if (file != null)
                            if (file.ContentLength > 0)
                            {
                                isSendMail = true;
                                var fileName = Path.GetFileName(file.FileName);
                                var path =
                                    Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), fileName);
                                file.SaveAs(path);
                                var origFilename = path;
                                if (System.IO.File.Exists(path))
                                {
                                    var filename = string.Format(@"{0}{1}", Guid.NewGuid().ToString(),
                                        Path.GetExtension(fileName));
                                    System.IO.File.Move(path,
                                        Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), filename));
                                    path = Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), filename);

                                    LogUtils.Log(Sessions.Instance.Username,
                                        string.Format("[{0}-UPLOAD] Uploading File {1} changed name to {2} by {3}",
                                            PolicyNo, origFilename, filename, AgentCode), "INFO");

                                    listFiles.Add(path);

                                    var upload = new UploadFileDo();
                                    upload.FileName =
                                        $"{PolicyNo}_{DateTime.Now.Ticks}_{Path.GetFileName(origFilename)}";
                                    upload.FileFolder = string.Format(@"\\{0}\PALMS\apps\pImaging\Imaging\Images\",
                                        ConfigSettings.ReadSetting("IIS_SERVER"));

                                    upload.FileNo = PolicyNo;
                                    upload.Department = "UNDERWRITING";
                                    upload.FormType = Path.GetExtension(filename);
                                    upload.Username = Sessions.Instance.Username;
                                    upload.IndexNo = count.SafeString();

                                    if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                    {
                                        upload.Category = "Client Uploads";
                                        upload.FormDesc = "CLIENT UPLOAD";
                                        upload.Username = Sessions.Instance.EmailAddress;
                                    }
                                    else
                                    {
                                        upload.Category = "Agent Uploads";
                                        upload.FormDesc = "AGENT UPLOAD";
                                    }

                                    try
                                    {
                                        using (Stream stream = new FileStream(path, FileMode.Open))
                                        {
                                            var uploadBytes = new byte[stream.Length - 1];
                                            stream.Read(uploadBytes, 0, uploadBytes.Length);

                                            upload.FileUploadBase64 = Convert.ToBase64String(uploadBytes);
                                        }

                                        SaveToImaging(upload);
                                        if (!Sessions.Instance.IsClient)
                                            PortalUtils.PostAgentUploadRec(PolicyNo, upload.FileName, fileName, path,
                                                "AGENTUPLOAD", Sessions.Instance.Username);
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(ex.Message, ex);
                                    }

                                    count += 1;
                                }
                            }

                    if (isSendMail && count > 0)
                    {
                        var subject = string.Format(@"{0} CATEGORY-AGENT UPLOAD", PolicyNo);
                        var agencyname = AgentUtils.GetAgencyName(AgentCode);
                        var confSubject = string.Format("[Palminsure {0}] Document Upload Confirmation", PolicyNo);
                        var uploadMsg = string.Format("You uploaded {0} files successfully for {1}.", count,
                            PolicyNo);
                        var confBody = EmailRenderer.UploadConfirmation(uploadMsg, PolicyNo);
                        try
                        {
                            //                        if (IsProduction())
                            //                        {
                            if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                _emailService.SendEmailNotification(
                                    new List<string> { Sessions.Instance.EmailAddress }, confSubject, confBody, true);
                            else
                                _emailService.SendEmailNotification(
                                    new List<string> { "uploads@palminsure.com" }, confSubject, confBody, true);
                            ///}
                            //-------
                            if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                PortalUtils.AddDiary("CLIENTUPLOADS", PolicyNo, Sessions.Instance.EmailAddress,
                                    "CLIENT UPLOADED FILES");
                            else
                                PortalUtils.AddDiary("AGENTUPLOADS", PolicyNo, AgentCode, "AGENT UPLOADED FILES");
                            //-------
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format(
                                    "[{0}-EMAIL IMAGE] Sent Image To Imaging From {2} And Added Diary For Files: {1}",
                                    PolicyNo, listFiles.ToComma(m => m), AgentCode), "DEBUG");
                        }
                        catch (Exception)
                        {
                            var errSubject = string.Format("[Palminsure {0}] Document Upload Error", PolicyNo);
                            if (_webHostService.IsProduction())
                            {
                                var sendTo = "technical.supporrt@palminsure.com";
                                _emailService.SendEmailNotification(new List<string> { sendTo }, errSubject,
                                    @"<div class='error'>There was an error processing your documents, please try again or forward your documents to underwiting@palminsure.com</div>",
                                    false);
                            }

                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("[{0}-UPLOAD IMAGE] Failed To Send {1} To Imaging ", PolicyNo,
                                    listFiles.ToComma(m => m), AgentCode), "FATAL");
                        }
                    }
                }

                Sessions.Instance.DocUploadResponse =
                    @"<div class='success'>Your documents were uploaded successfully</div>";
            }
        }


        public void UploadFiles(List<HttpPostedFileBase> files)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var isSendMail = false;
                var listFiles = new List<string>();
                listFiles.Clear();
                var count = 0;
                if (files != null)
                {
                    foreach (var file in files)
                        if (file != null)
                            if (file.ContentLength > 0)
                            {
                                isSendMail = true;
                                var fileName = $"{Sessions.Instance.PolicyNo}_{DateTime.Now.Ticks}" +
                                               Path.GetFileName(file.FileName);
                                var path =
                                    Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), fileName);
                                if (!System.IO.File.Exists(path))
                                {
                                    file.SaveAs(path);
                                    var origFilename = path;
                                    if (System.IO.File.Exists(path))
                                    {
                                        listFiles.Add(path);
                                        var upload = new UploadFileDo();
                                        //upload.FileName = Path.GetFileName(path);
                                        upload.FileName =
                                            fileName; //$"{Sessions.Instance.PolicyNo}_{DateTime.Now.Ticks}_{Path.GetFileName(file.FileName)}";
                                        upload.FileFolder = string.Format(@"\\{0}\PALMS\apps\pImaging\Imaging\Images\",
                                            ConfigSettings.ReadSetting("IIS_SERVER"));
                                        upload.FileNo = Sessions.Instance.PolicyNo;
                                        upload.Department = "UNDERWRITING";
                                        upload.FormType = Path.GetExtension(fileName);
                                        upload.Username = Sessions.Instance.Username;
                                        upload.IndexNo = count.SafeString();

                                        if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                        {
                                            upload.Category = "Client Uploads";
                                            upload.FormDesc = "CLIENT UPLOAD";
                                            upload.Username = Sessions.Instance.EmailAddress;
                                        }
                                        else
                                        {
                                            upload.Category = "Agent Uploads";
                                            upload.FormDesc = "AGENT UPLOAD";
                                        }

                                        try
                                        {
                                            using (Stream stream = new FileStream(path, FileMode.Open))
                                            {
                                                var uploadBytes = new byte[stream.Length - 1];
                                                stream.Read(uploadBytes, 0, uploadBytes.Length);

                                                upload.FileUploadBase64 = Convert.ToBase64String(uploadBytes);
                                            }

                                            SaveToImaging(upload);
                                            if (!Sessions.Instance.IsClient)
                                                PortalUtils.PostAgentUploadRec(Sessions.Instance.PolicyNo,
                                                    upload.FileName,
                                                    fileName, path, "AGENTUPLOAD", Sessions.Instance.Username);
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error(ex.Message, ex);
                                        }

                                        count += 1;
                                    }
                                }
                            }

                    if (isSendMail && count > 0)
                    {
                        var subject = string.Format(@"{0} CATEGORY-AGENT UPLOAD", Sessions.Instance.PolicyNo);
                        var agencyname = AgentUtils.GetAgencyName(Sessions.Instance.AgentCode);
                        var confSubject = string.Format("[Palminsure {0}] Document Upload Confirmation",
                            Sessions.Instance.PolicyNo);
                        var uploadMsg = string.Format("You uploaded {0} files successfully for {1}.", count,
                            Sessions.Instance.PolicyNo);
                        var confBody = EmailRenderer.UploadConfirmation(uploadMsg, Sessions.Instance.PolicyNo);
                        try
                        {
                            if (!_webHostService.IsProduction())
                            {
                                if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                    _emailService.SendEmailNotification(
                                        new List<string> { Sessions.Instance.EmailAddress }, confSubject, confBody, true);
                                else
                                    _emailService.SendEmailNotification(
                                        new List<string> { Sessions.Instance.EmailAddress }, confSubject, confBody, true);
                            }

                            if (_webHostService.IsProduction())
                            {
                                if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                    _emailService.SendEmailNotification(
                                        new List<string> { Sessions.Instance.EmailAddress }, confSubject, confBody, true);
                                else
                                    _emailService.SendEmailNotification(
                                        new List<string> { "uploads@palminsure.com" }, confSubject, confBody, true);
                            }

                            if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                                PortalUtils.AddDiary("CLIENTUPLOADS", Sessions.Instance.PolicyNo,
                                    Sessions.Instance.EmailAddress, "CLIENT UPLOADED FILES");
                            else
                                PortalUtils.AddDiary("AGENTUPLOADS", Sessions.Instance.PolicyNo,
                                    Sessions.Instance.AgentCode, "AGENT UPLOADED FILES");
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format(
                                    "[{0}-EMAIL IMAGE] Sent Image To Imaging From {2} And Added Diary For Files: {1}",
                                    Sessions.Instance.PolicyNo, listFiles.ToComma(m => m), Sessions.Instance.AgentCode),
                                "DEBUG");
                        }
                        catch (Exception)
                        {
                            var errSubject = string.Format("[Palminsure {0}] Document Upload Error",
                                Sessions.Instance.PolicyNo);
                            if (_webHostService.IsProduction())
                            {
                                var sendTo = "technical.supporrt@palminsure.com";
                                _emailService.SendEmailNotification(new List<string> { sendTo }, errSubject,
                                    @"<div class='error'>There was an error processing your documents, please try again or forward your documents to underwiting@palminsure.com</div>",
                                    false);
                            }

                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("[{0}-UPLOAD IMAGE] Failed To Send {1} To Imaging ",
                                    Sessions.Instance.PolicyNo,
                                    listFiles.ToComma(m => m), Sessions.Instance.AgentCode), "FATAL");
                        }
                    }
                }
            }
        }

        public void SaveToImaging(UploadFileDo upload)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var server = ConfigSettings.ReadSetting("IIS_SERVER");
                var saveFileName = string.Format(@"\\{0}\PALMS\apps\pImaging\Imaging\Images\{1}", server,
                    upload.FileName);

                using (Stream stream = new FileStream(saveFileName, FileMode.Create))
                {
                    stream.Write(Convert.FromBase64String(upload.FileUploadBase64), 0,
                        Convert.FromBase64String(upload.FileUploadBase64).Length);
                }

                var indexInfoPath = string.Format(@"\\{0}\palms\apps\pImaging\Imaging\IndexInfo\{1}_{2}_{3}.xml",
                    server,
                    upload.FileNo, CleanString(upload.FormDesc),
                    DateTime.Now.ToString("yyMMddhhmmss"));

                var Images = new AuditImageIndex(uow);
                Images.FileNo = upload.FileNo;
                Images.Dept = upload.Department;
                Images.FormType = upload.FormType;
                Images.FormDesc = upload.FormDesc;
                Images.FileFolder = upload.FileFolder;
                Images.FileName = upload.FileName;
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = upload.Username;
                Images.Thumbnail = upload.ThumbnailPath;
                Images.IsDiary = true;
                Images.Category = upload.Category;
                Images.PortalCode = upload.PortalCode;
                Images.PerilDesc = upload.PerilDesc;
                Images.IndexNo = upload.IndexNo;
                Images.Save();
                uow.CommitChanges();

                var Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = upload.FileNo;
                Index.historyID = null;
                Index.department = upload.Department;
                Index.formType = upload.FormType;
                Index.formDesc = upload.FormDesc;
                Index.fileFolder = upload.FileFolder;
                Index.fileName = upload.FileName;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = upload.Username;
                Index.thumbnail = upload.ThumbnailPath;
                Index.isDiary = true;
                Index.category = upload.Category;
                Index.portalcode = upload.PortalCode;
                Index.PerilDesc = upload.PerilDesc;
                Index.IndexNo = upload.IndexNo;
                Index.Create(indexInfoPath);
                Index.Dispose(Index);
            }
        }

        public List<string> AddPdfFileToList(List<string> listPdfs, string filePath)
        {
            var fileName = Path.GetFileName(filePath);
            filePath = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + fileName;

            if (File.Exists(filePath))
                listPdfs.Add(filePath);
            return listPdfs;
        }

        public string DownloadAgentSuspenseReport(string agentCode, string userName)
        {
            if (agentCode.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var agentRptFile =
                    ssrsHelpers.DownloadAgentSuspenseReport(agentCode, userName, uow);
                var agentRptFilename = Path.GetFileName(agentRptFile);
                var agentRptFileLocation =
                    $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + agentRptFilename;
                if (System.IO.File.Exists(agentRptFileLocation))
                {
                    return agentRptFileLocation;
                }
                return null;
            }
        }

        public string DownloadRenewalPolicyDec(string policyNo, PalmsServiceClient palmsClient)
        {
            if (policyNo.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var renewalPolicyDecPath =
                    ssrsHelpers.DownloadRenewalPolicyDec(policyNo, uow);
                var renewalPolicyDecFilename = Path.GetFileName(renewalPolicyDecPath);
                var renewalPolicyDecFileLocation =
                    $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + renewalPolicyDecFilename;
                if (System.IO.File.Exists(renewalPolicyDecFileLocation))
                {
                    return renewalPolicyDecFileLocation;
                }
                //TODO: why are we closing a client we aren't using here?
                palmsClient.CloseRef();
                return null;
            }
        }

        public string DownloadRenewalPaymentSchedule(string renewalPolicyNo)
        {
            if (string.IsNullOrEmpty(renewalPolicyNo)) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var renewalPaymentSchedulePath =
                    ssrsHelpers.DownloadRenewalQuotePaymentSchedule(renewalPolicyNo);
                var renewalPaymentScheduleFilename = Path.GetFileName(renewalPaymentSchedulePath);
                var renewalPaymentScheduleFileLocation =
                    $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + renewalPaymentScheduleFilename;
                if (System.IO.File.Exists(renewalPaymentScheduleFileLocation))
                {
                    return renewalPaymentScheduleFileLocation;
                }
                return null;
            }
        }

        public string DownloadRenewalInvoice(string renewalPolicyNo)
        {
            if (string.IsNullOrEmpty(renewalPolicyNo)) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var renewalInvoicePath =
                    ssrsHelpers.DownloadRenewalInvoice(renewalPolicyNo, Sessions.Instance.Username);
                var renewalInvoiceFilename = Path.GetFileName(renewalInvoicePath);
                var renewalInvoiceFileLocation =
                    $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + renewalInvoiceFilename;
                if (System.IO.File.Exists(renewalInvoiceFileLocation))
                {
                    return renewalInvoiceFileLocation;
                }
                return null;
            }
        }

        public string DownloadAgentInforceReport(string agentCode)
        {
            if (Sessions.Instance.AgentCode.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var agentRptFile =
                    ssrsHelpers.DownloadAgentInforceReport(Sessions.Instance.AgentCode, Sessions.Instance.Username,
                        uow);
                var agentRptFilename = Path.GetFileName(agentRptFile);
                var agentRptFileLocation =
                    $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + agentRptFilename;
                if (System.IO.File.Exists(agentRptFileLocation))
                {
                    return agentRptFileLocation;
                }

                return null;
            }
        }

        public string DownloadAgentStatusReport(string agentCode, PalmsServiceClient palmsClient)
        {
            if (agentCode.IsNullOrEmpty()) return null;
            var ssrsHelpers = new SSRSHelpers();
            var agentRptFile = ssrsHelpers.DownloadAgentStatusReport(agentCode);
            var agentRptFilename = Path.GetFileName(agentRptFile);
            var agentRptFileLocation = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + agentRptFilename;
            if (System.IO.File.Exists(agentRptFileLocation))
            {
                return agentRptFileLocation;
            }
            palmsClient.CloseRef();
            return null;
        }

        public string DownloadAgentLossRatioReport(string agentCode, PalmsServiceClient PalmsClient)
        {
            if (agentCode.IsNullOrEmpty()) return null;
            var ssrsHelpers = new SSRSHelpers();
            var agentRptFile = ssrsHelpers.DownloadAgentLossRatioReport(agentCode, DateTime.Now);
            var agentRptFilename = Path.GetFileName(agentRptFile);
            var agentRptFileLocation = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + agentRptFilename;
            if (System.IO.File.Exists(agentRptFileLocation))
            {
                return agentRptFileLocation;
            }
            PalmsClient.CloseRef();
            return null;
        }

        public string DownloadPolicyIdCards(string policyNo, PalmsServiceClient PalmsClient)
        {
            if (policyNo.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var idCardsFile = ssrsHelpers.DownloadPolicyIDCards(policyNo, uow);
                var idCardsFilename = Path.GetFileName(idCardsFile);
                var location = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + idCardsFilename;
                if (System.IO.File.Exists(location))
                {
                    return location;
                }
                PalmsClient.CloseRef();
                return null;
            }
        }

        public string DownloadPolicyDocument(string id, out string filename, PalmsServiceClient palmsClient)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var policyDoc = new XPCollection<AuditImageIndex>(uow, CriteriaOperator.Parse("IndexID = ?", id))
                    .FirstOrDefault();
                if (policyDoc != null)
                {
                    var locationInDb = policyDoc.FileFolder + policyDoc.FileName;
                    filename = Path.GetFileName(locationInDb);
                    if (System.IO.File.Exists(locationInDb))
                    {
                        return locationInDb;
                    }
                }
                palmsClient.CloseRef();
                filename = null;
                return null;
            }
        }

        public string GetCompiledPdf(string policyNo, bool policyDec, bool idCards, bool paySchedule,
            bool paymentReceiptLink, PalmsServiceClient PalmClient)
        {
            if (policyNo.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var listPdfs = new List<string>();

                if (idCards)
                {
                    var idCardsFile = ssrsHelpers.DownloadPolicyIDCards(policyNo, uow);
                    listPdfs = AddPdfFileToList(listPdfs, idCardsFile);
                }

                if (policyDec)
                {
                    var policyDecFile = ssrsHelpers.DownloadPolicyDec(policyNo, uow);
                    listPdfs = AddPdfFileToList(listPdfs, policyDecFile);
                }

                if (paySchedule)
                {
                    var policyInvoiceFile = ssrsHelpers.DownloadPolicyInvoice(policyNo, DateTime.Now, uow, true);
                    listPdfs = AddPdfFileToList(listPdfs, policyInvoiceFile);

                    var policyPaySchdFile = ssrsHelpers.DownloadPaymentSchedule(policyNo);
                    listPdfs = AddPdfFileToList(listPdfs, policyPaySchdFile);
                }

                if (paymentReceiptLink)
                {
                    var paymentReceiptFilePath = ssrsHelpers.GeneratePaymentReceipt(policyNo, uow);
                    listPdfs = AddPdfFileToList(listPdfs, paymentReceiptFilePath);
                }

                var destinationConcatPDFFile = string.Format(@"\\{0}\pdfs\{1}_EndorsementPolicyDocs-{2}.pdf",
                    ConfigSettings.ReadSetting("IIS_Server"), policyNo, DateTime.Now.ToString("yyyyMMddhhmmssfff"));
                if (listPdfs.Any())
                {
                    SSRSHelpers.ConcatPDFs(listPdfs, destinationConcatPDFFile);

                    if (System.IO.File.Exists(destinationConcatPDFFile))
                    {
                        return destinationConcatPDFFile;
                    }
                }
                ExtPortalUtils.CloseRef(PalmClient);
                return null;
            }
        }

        public string DownloadPolicyDec(string policyno, PalmsServiceClient PalmsClient)
        {
            if (policyno.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var policyDecFile = ssrsHelpers.DownloadPolicyDec(policyno, uow);
                var policyDecFilename = Path.GetFileName(policyDecFile);
                var location = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + policyDecFilename;
                if (System.IO.File.Exists(location))
                {
                    return location;
                }
                PalmsClient.CloseRef();
                return null;
            }
        }

        public string DownloadApplicationForInsurance(string policyno, PalmsServiceClient PalmsClient)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var insruanceAppFileLocation =
                    ssrsHelpers.GenerateApplication(policyno, Sessions.Instance.Username, uow);
                var insuranceAppFilename = Path.GetFileName(insruanceAppFileLocation);
                var location = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + insuranceAppFilename;
                if (System.IO.File.Exists(location))
                {
                    return location;
                }
                PalmsClient.CloseRef();
                return null;
            }
        }

        public string DownloadPolicyPaymentSchedule(string policyno, PalmsServiceClient PalmsClient)
        {
            if (policyno.IsNullOrEmpty()) return null;
            var ssrsHelpers = new SSRSHelpers();
            var policyPaySchdFile = ssrsHelpers.DownloadPaymentSchedule(policyno);
            var policyPaySchdFilename = Path.GetFileName(policyPaySchdFile);
            var location = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + policyPaySchdFilename;
            if (System.IO.File.Exists(location))
            {
                return location;
            }
            return null;
        }

        public string DownloadPolicyInvoice(string policyno, PalmsServiceClient PalmsClient)
        {
            if (policyno.IsNullOrEmpty()) return null;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var ssrsHelpers = new SSRSHelpers();
                var policyInvoiceFile = ssrsHelpers.DownloadPolicyInvoice(policyno, DateTime.Now, uow, true);
                var policyInvoiceFilename = Path.GetFileName(policyInvoiceFile);
                var location = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + policyInvoiceFilename;
                if (System.IO.File.Exists(location))
                {
                    return location;
                }
                PalmsClient.CloseRef();
                return null;
            }
        }
    }
}