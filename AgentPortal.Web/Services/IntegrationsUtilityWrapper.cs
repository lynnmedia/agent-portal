﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AgentPortal.Models;
using AgentPortal.Support.Integrations;
using AlertAuto.Integrations.Contracts.CardConnect;
using AlertAuto.Integrations.Contracts.MVR;
using AlertAuto.Integrations.Contracts.RAPA;
using AlertAuto.Integrations.Contracts.Verisk.APlus;
using AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier;
using AlertAuto.Integrations.Contracts.Verisk.RiskCheck;

namespace AgentPortal.Services
{
    public class IntegrationsUtilityWrapper : IIntegrationsClient
    {
        public VeriskVinSearchResultBody SearchVin(string policyNo, string vin, string year = "", string make = "", string model = "", string bodyStyle = "")
        {
            return IntegrationsUtility.SearchVin(policyNo, vin, year, make, model, bodyStyle);
        }

        public VeriskVehicleSearchResultBody SearchVehicle(string policyNo, string year, string make, string model = "")
        {
            throw new NotImplementedException();
        }

        public VeriskMvrSubmitRequestBody MvrSubmitRequest(string policyNo, string givenName, string surname, string dob,
            string dlNumber, string dlState)
        {
            throw new NotImplementedException();
        }

        public VeriskRetreiveMvrReportBody MvrRequestReport(string policyNo, string orderNumber)
        {
            throw new NotImplementedException();
        }

        public VeriskAPlusCapWithClaimsBody AplusReportWithClaims(string policyNo, APlusReportModel data)
        {
            throw new NotImplementedException();
        }

        public CoverageVerifierResponseBody CoverageVerifierReport(string policyNo, CoverageVerifierRequest data)
        {
            throw new NotImplementedException();
        }

        public Task<VeriskRiskCheckReportResultBody> RiskCheckReport(string policyNo, RiskCheckRquest data)
        {
            throw new NotImplementedException();
        }

        public void SendESignatureRequest(string policyNo, Dictionary<string, string> userEmailMap, IEnumerable<string> documentLocations,
            List<string> ccEmails = null, string requestType = "")
        {
            throw new NotImplementedException();
        }

        public string GetEsignatureDocumentDownloadUrl(string signatureRequestId)
        {
            throw new NotImplementedException();
        }

        public CardConnectResponse AuthorizeWithCardConnect(string policyNo, CardConnectRequest requestData,
            bool createNewProfile)
        {
            return IntegrationsUtility.AuthorizeWithCardConnect(policyNo, requestData, createNewProfile);
        }

        public string CreateCardConnectProfile(string policyNo, CardConnectRequest requestData)
        {
            return IntegrationsUtility.CreateCardConnectProfile(policyNo, requestData);
        }

        public bool UpdateFlagForAllIntegrationCall(string pPolicyNo, bool pSetAllIntegrationCalFlagValue,
            bool pHasToSetRCPOSFlag = false, bool pSetRcposOrderedValue = false)
        {
            throw new NotImplementedException();
        }

        public bool GetFlagForAllIntegrationCall(string pPolicyNo, ref bool pIsRCPOSOrdered)
        {
            throw new NotImplementedException();
        }

        public string GetDeepLink(string policyNo, bool existingUrlOnly = false)
        {
            throw new NotImplementedException();
        }

        public bool SendUploadImagesDeepLink(string policyNo, string deepLink)
        {
            throw new NotImplementedException();
        }

        public void GenerateNewPolicyDocuments(string policyNo, string historyIndex)
        {
            throw new NotImplementedException();
        }
    }
}