﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Web;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Xpo;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using PalmInsure.Palms.Xpo;
using log4net;

namespace AgentPortal.Services
{
    public interface IEmailService
    {
        bool SendEmailNotification(IEnumerable<string> toEmailAddresses, string subject, string message, bool messageisHtml);
        void ReSendESignatureRequest(string policyNo, string signatureRequestId, Dictionary<string, string> userEmailMap);
        void SendPaymentProfileNotCreatedEmail(string policyNo);
        void RequestESignaturesForPolicy(string policyNo);
        bool SendESignRequestForEndorsementRequest(string policyNo, DisallowedAgentEndorseQuoteDO model);
    }

    public class EmailService : IEmailService
    {
        private readonly ILog logger;
        public EmailService()
        {
            logger = LogManager.GetLogger("EmailService");
        }
        public bool SendEmailNotification(IEnumerable<string> toEmailAddresses, string subject, string message, bool messageisHtml)
        {
            if ((toEmailAddresses == null || (toEmailAddresses != null && !toEmailAddresses.Any())) ||
                string.IsNullOrEmpty(subject) || string.IsNullOrEmpty(message))
            {
                return false;
            }
            string emailNotificationUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                          "/Notification/sendEmailNotification";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(emailNotificationUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;

            dynamic dto = new
            {
                toEmailAddresses,
                subject,
                message,
                isHtml = messageisHtml
            };
            var dtoContent = JsonConvert.SerializeObject(dto);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK || response.StatusCode == HttpStatusCode.Accepted)
            {
                return true;
            }

            return false;
        }

        public void ReSendESignatureRequest(string policyNo, string signatureRequestId, Dictionary<string, string> userEmailMap)
        {
            if (string.IsNullOrEmpty(policyNo) || string.IsNullOrEmpty(signatureRequestId) || userEmailMap == null || !userEmailMap.Any())
            {
                return;
            }
            string reSendESigRequestUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                        "/helloSign/resendSignatureRequest?policyNo={policyNo}&signatureRequestId={signatureRequestId}";
            reSendESigRequestUrl = reSendESigRequestUrl.Replace("{policyNo}", policyNo);
            reSendESigRequestUrl = reSendESigRequestUrl.Replace("{signatureRequestId}", signatureRequestId);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(reSendESigRequestUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;

            var dtoContent = JsonConvert.SerializeObject(userEmailMap);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        }

        public void SendPaymentProfileNotCreatedEmail(string policyNo)
        {
            //if (IsProduction())
            //{
            var toEmailAddrs = new List<string>() { "paymentprofiles@palminsure.com" };
            var agentEmailAddr = AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode);

            if (!string.IsNullOrEmpty(agentEmailAddr))
            {
                toEmailAddrs.Add(agentEmailAddr);
            }
            SendEmailNotification(toEmailAddrs,
                $"Policy {policyNo} - EFT Payment Profile Failed to Save.",
                $"An automatic EFT payment profile was not correctly generated for policy {policyNo} using the supplied payment information.  Please verify the information is entered correctly and resubmit.  If you still receive an error, please contact UW at 800 ext. 2.", false);
            //}
        }

        public void RequestESignaturesForPolicy(string policyNo)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                if (string.IsNullOrEmpty(policyNo))
                {
                    throw new ArgumentException("PolicyNo cannot be null or empty.");
                }

                XpoPolicy xpoPolicy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .FirstOrDefault();
                XpoAgents xpoAgent =
                    new XPCollection<XpoAgents>(uow,
                        CriteriaOperator.Parse("AgentCode = ?", Sessions.Instance.AgentCode)).FirstOrDefault();
                Dictionary<string, string> userEmailMap = new Dictionary<string, string>();
                //TODO: abstract into object.
                //Until object abstracted away, insured must be first in the dictionary.
                if (xpoPolicy != null && !string.IsNullOrEmpty(xpoPolicy.Ins1First) && !string.IsNullOrEmpty(xpoPolicy.Ins1Last) &&
                    !string.IsNullOrEmpty(xpoPolicy.MainEmail))
                {
                    string insuredName = $"{xpoPolicy.Ins1First} {xpoPolicy.Ins1Last}";
                    userEmailMap.Add(insuredName, xpoPolicy.MainEmail);
                }

                if (xpoPolicy != null && !string.IsNullOrEmpty(xpoPolicy.AgentESignEmailAddress))
                {
                    userEmailMap.Add(xpoAgent.AgencyName, xpoPolicy.AgentESignEmailAddress);
                }
                else if (xpoAgent != null && !string.IsNullOrEmpty(xpoAgent.AgencyName) &&
                         !string.IsNullOrEmpty(xpoAgent.EmailAddress))
                {
                    userEmailMap.Add(xpoAgent.AgencyName, xpoAgent.EmailAddress);
                }

                SSRSHelpers ssrsHelpers = new SSRSHelpers();
                string insuranceApplicationLocation = ssrsHelpers.GenerateApplication(policyNo, Sessions.Instance.Username, uow);
                string umRejectionLocation = ssrsHelpers.GenerateUMRejection(policyNo, Sessions.Instance.Username, uow);
                string biRejectionLocation = ssrsHelpers.GenerateBIRejection(policyNo, Sessions.Instance.Username, uow);
                string eftLocation = ssrsHelpers.GenerateEFT(policyNo, Sessions.Instance.Username, uow);
                string driverDisclosureLocation = ssrsHelpers.GenerateDriverDisclosure(policyNo, Sessions.Instance.Username, uow);

                IntegrationsUtility.SendESignatureRequest(policyNo, userEmailMap,
                    new List<string>() { insuranceApplicationLocation, umRejectionLocation, biRejectionLocation, eftLocation, driverDisclosureLocation });
            }
        }

        // Issue: Unused
        public bool SendESignRequestForEndorsementRequest(string policyNo,
            DisallowedAgentEndorseQuoteDO model)
        {
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {


                    if (model == null)
                        model = uow.FindObject<DisallowedAgentEndorseQuoteDO>(
                            CriteriaOperator.Parse(string.Format("PolicyNo = '{0}' AND IsActive=1", policyNo)));
                    var ssrsHelpers = new SSRSHelpers();
                    var endorsementRequestFile =
                        ssrsHelpers.GenerateEndorsementRequest(policyNo, Sessions.Instance.Username, uow);
                    logger.Info(
                        $"SendESignRequestForEndorsementRequest: PolicyNo={policyNo}, request PDF File Created= {endorsementRequestFile}");

                    if (!string.IsNullOrEmpty(endorsementRequestFile)) //System.IO.File.Exists(endorsementRequestFile))
                    {
                        var existingPolicy = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                        var userEmailMap = new Dictionary<string, string>();
                        var insuredFirst = model.Ins1First;
                        var insuredLast = model.Ins1Last;
                        if (string.IsNullOrEmpty(insuredFirst) || string.IsNullOrEmpty(insuredLast))
                        {
                            insuredFirst = existingPolicy.Ins1First;
                            insuredLast = existingPolicy.Ins1Last;
                        }

                        if (model != null && !string.IsNullOrEmpty(insuredFirst) && !string.IsNullOrEmpty(insuredLast) &&
                            !string.IsNullOrEmpty(model.MainEmail))
                        {
                            var insuredName = $"{insuredFirst} {insuredLast}";
                            userEmailMap.Add(insuredName, model.MainEmail);
                        }
                        else
                        {
                            logger.Info(
                                $"SendESignRequestForEndorsementRequest:{policyNo} doesnot have Isnured name or email. FirstName={model.Ins1First} LastName={model.MainEmail} MainEmail={model.MainEmail}");
                        }

                        var agent = uow.FindObject<XpoAgents>(CriteriaOperator.Parse("AgentCode = ?", model.AgentCode));
                        if (string.IsNullOrEmpty(model.AgentAltESignEndorseEmail))
                        {
                            if (agent != null && !string.IsNullOrEmpty(agent.AgencyName)
                                              && existingPolicy != null
                                              && !string.IsNullOrEmpty(existingPolicy.AgentESignEmailAddress))
                                userEmailMap.Add(agent.AgencyName, existingPolicy.AgentESignEmailAddress);
                            else if (agent != null && !string.IsNullOrEmpty(agent.AgencyName) &&
                                     !string.IsNullOrEmpty(agent.EmailAddress))
                                userEmailMap.Add(agent.AgencyName, agent.EmailAddress);
                            else
                                logger.Info(
                                    $"SendESignRequestForEndorsementRequest:{policyNo} doesnot have Agency email id for Agent code {agent.AgentCode}");
                        }


                        if (!string.IsNullOrEmpty(model.AgentAltESignEndorseEmail) && agent != null &&
                            !string.IsNullOrEmpty(agent.AgencyName))
                        {
                            if (!userEmailMap.ContainsKey(agent.AgencyName))
                                userEmailMap.Add(agent.AgencyName, model?.AgentAltESignEndorseEmail);
                            else
                                userEmailMap.Add(agent.AgencyName + policyNo, model?.AgentAltESignEndorseEmail);
                        }
                        else if (string.IsNullOrEmpty(model.AgentAltESignEndorseEmail))
                        {
                            logger.Info(
                                $"SendESignRequestForEndorsementRequest:{policyNo} doesnot have optional alternate agent email address for Agent code {agent.AgentCode}");
                        }

                        if (userEmailMap.Any())
                        {
                            var ccEmails = new List<string>();
                            if (ConfigSettings.ReadSetting("IIS_SERVER")?.ToUpper() == "IIS-AAINS-DEV")
                            {
                                ccEmails.Add("aloks@agsft.com");
                            }
                            else
                            {
                                ccEmails.Add("endorsements@palminsure.com");
                                ccEmails.Add("underwriting@palminsure.com");
                            }

                            IntegrationsUtility.SendESignatureRequest(policyNo, userEmailMap,
                                new List<string> { endorsementRequestFile }, ccEmails);
                            model.IsActive = false;
                            uow.CommitChanges();
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(
                    $"SendESignRequestForEndorsementRequest: Error occured while sending endorsement request Policyno={policyNo}",
                    ex);
            }

            return false;
        }
    }
}