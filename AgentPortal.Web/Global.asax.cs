﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Web.Mvc;
using System.Web.Routing;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Repositories;
using AgentPortal.Repositories.Xpo;
using AgentPortal.Services;
using AgentPortal.Support;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using Autofac;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using Autofac.Integration.Mvc;
using log4net;

namespace AgentPortal
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private Session sess;

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        protected void Application_BeginRequest()
        {
            if (ConfigSettings.ReadSetting("IIS_SERVER").Equals("VM-AAINS-IIS") &&
                !Context.Request.Url.ToString().Contains("localhost") && !Context.Request.IsSecureConnection)
            {
                Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            
            // Do something with the error.
            System.Diagnostics.Debug.WriteLine(exception);

            bool isProduction = ConfigSettings.ReadSetting("DB_Server").Equals("VM-AAINS-SQL");
            if (isProduction)
            {
                if (exception != null && !exception.Message.Contains("/images/") &&
                    !exception.Message.Contains("DoPostResolveRequestCache") &&
                    !exception.Message.Contains("The controller for path"))
                {
                    string policyNo = "none";
                    string userName = "none";
                    string agentCode = "none";
                    string agencyName = "none";
                    if (!string.IsNullOrEmpty(Sessions.Instance.PolicyNo))
                    {
                        policyNo = Sessions.Instance.PolicyNo;
                    }

                    if (!string.IsNullOrEmpty(Sessions.Instance.Username))
                    {
                        userName = Sessions.Instance.Username;
                    }

                    if (!string.IsNullOrEmpty(Sessions.Instance.AgentCode))
                    {
                        agentCode = Sessions.Instance.AgentCode;
                    }

                    if (!string.IsNullOrEmpty(Sessions.Instance.AgencyName))
                    {
                        agencyName = Sessions.Instance.AgencyName;
                    }

                    EmailService service = new EmailService();
                    service.SendEmailNotification(new List<string>() {"technical.support@palminsure.com"},
                        $"Agent Portal Error - PolicyNo: {policyNo}, Username: {userName}, AgentCode: {agentCode}, AgencyName: {agencyName}",
                        exception.Message + " ---- " + exception.StackTrace, false);
                    ILog errorLogger = LogManager.GetLogger("GlobalAsax");
                    errorLogger.Error(exception.Message, exception);
                    Response.Redirect("/Home/Error");
                }
            }
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("loading.gif");
            routes.IgnoreRoute("arrows.png");
            routes.Ignore(".");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }


        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            log4net.Config.XmlConfigurator.Configure();
            XpoDefault.Session = null;
            XpoDefault.UseFastAccessors = false;
            XpoDefault.IdentityMapBehavior = IdentityMapBehavior.Strong;

            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();
           /// ClientLoginManager.CreateClientLogins();
            //            string serverName = ConfigSettings.ReadSetting("DB_Server");
            //            string userName = ConfigSettings.ReadSetting("DB_UserName");
            //            string pw = ConfigSettings.ReadSetting("DB_Pass");
            //            string catalog = ConfigSettings.ReadSetting("DB_Catalog");
            //            sess = new DevExpress.Xpo.Session();
            //            XpoDefault.DataLayer =
            //                XpoDefault.GetDataLayer(
            //                    MSSqlConnectionProvider.GetConnectionString(serverName, userName, pw, catalog),
            //                    AutoCreateOption.SchemaOnly);
            //            sess.CreateObjectTypeRecords();
            //            XpoDefault.Session = sess;
            RegisterAutofac();
        }

        protected void RegisterAutofac()
        {
            // MVC setup documentation here:
            // https://autofac.readthedocs.io/en/latest/integration/mvc.html

            var builder = new ContainerBuilder();

            // MVC - Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // MVC - OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            // MVC - OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // MVC - OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // MVC - OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Register application dependencies.
            //builder.RegisterAssemblyTypes().AsImplementedInterfaces();

            //register services
            builder.RegisterType<AgentService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<AjaxService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<DocumentService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<EmailService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<PaymentService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<QuoteService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<ReportService>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<WebHostService>().AsSelf().AsImplementedInterfaces();

            //register repositories
            builder.RegisterType<XpoPolicyRepository>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<XpoQuoteRepository>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<XpoEndorsementRepository>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<XpoPaymentRepository>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<XpoUnitOfWorkFactory>().AsSelf().AsImplementedInterfaces();
            
            //register wrappers
            builder.RegisterType<IntegrationsUtilityWrapper>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<LogUtilWrapper>().AsSelf().AsImplementedInterfaces();
            builder.RegisterType<SessionsWrapper>().AsSelf().AsImplementedInterfaces();

            //register palm client
            builder.RegisterType<PalmsServiceClient>().AsSelf().As<IPalmsService>();

            //register blowfish
            builder.RegisterInstance(new Blowfish(Constants.blowfishKey)).SingleInstance();
 
            //register model state dictionary as wrapper
            builder.RegisterInstance(new ModelStateDictionary()).SingleInstance();
            builder.Register(c => new ModelStateWrapper(c.Resolve<ModelStateDictionary>())).As<IValidationDictionary>().InstancePerRequest();

            // MVC - Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Url.Scheme == "https")
            {
                Response.AddHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            }

        }
    }
}