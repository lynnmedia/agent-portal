﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace AgentPortal.Support
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString AbbrLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            ModelMetadata meta = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            if (String.IsNullOrWhiteSpace(meta.ShortDisplayName))
            {
                return LabelExtensions.LabelFor<TModel, TProperty>(htmlHelper, expression);
            }
            else
            {
                var abbr = new TagBuilder("abbr");
                abbr.Attributes.Add("title", meta.DisplayName);
                abbr.SetInnerText(meta.ShortDisplayName);

                var label = new TagBuilder("label");
                label.Attributes.Add("for", meta.PropertyName);
                label.InnerHtml = abbr.ToString();

                return new MvcHtmlString(label.ToString(TagRenderMode.Normal));
            }
        }//END AbbrLabelFor
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }//END Class
}//END Namespace