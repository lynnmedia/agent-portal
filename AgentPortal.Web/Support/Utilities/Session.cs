﻿/*
    Written By: David Wimbley
  
    Date Originally Completed: 06/13/2012
  
    Purpose: Singleton class as alternative to storing data in sessions. Allows use of class as the session to store any data we need such as logged in username
                    agent code, is authenticated, is admin, is agent. Logging the user out is simply nulling out the class. When creating permission based views
                    make sure to wrap the call to the View() within an if statement to prevent unauthenticated users from accessing content that is outside of their right.
 
    Usage:
        //Prevents the user from seeing the login screen if they are already logged in
        public ActionResult Login()
        {
            if (Sessions.Instance.isAuth)
            {
                return RedirectToAction("Search", "Agent");
            }
            else
            {
                return View();
            }
        }
    
    Update History:
        Added documentation to class - DMW - 06/15/2012
 */

using System;
using System.Web;
using AgentPortal.Services;

namespace AgentPortal.Support.Utilities
{
    public class SessionsWrapper : ISessions
    {
        public bool IsAgent
        {
            get => Sessions.Instance.IsAgent;
            set => Sessions.Instance.IsAgent = value;
        }

        public string AgentCode
        {
            get => Sessions.Instance.AgentCode;
            set => Sessions.Instance.AgentCode = value;
        }

        public string AgentName
        {
            get => Sessions.Instance.AgentName;
            set => Sessions.Instance.AgentName = value;
        }

        public string AgencyName
        {
            get => Sessions.Instance.AgencyName;
            set => Sessions.Instance.AgencyName = value;
        }

        public string Username
        {
            get => Sessions.Instance.Username;
            set => Sessions.Instance.Username = value;
        }

        public bool IsAuth
        {
            get => Sessions.Instance.IsAuth;
            set => Sessions.Instance.IsAuth = value;
        }

        public bool IsCompany
        {
            get => Sessions.Instance.IsCompany;
            set => Sessions.Instance.IsCompany = value;
        }

        public bool IsClient
        {
            get => Sessions.Instance.IsClient;
            set => Sessions.Instance.IsClient = value;
        }

        public string EmailAddress
        {
            get => Sessions.Instance.EmailAddress;
            set => Sessions.Instance.EmailAddress = value;
        }

        public string PolicyNumber
        {
            get => Sessions.Instance.PolicyNumber;
            set => Sessions.Instance.PolicyNumber = value;
        }

        public string PolicyNo
        {
            get => Sessions.Instance.PolicyNo;
            set => Sessions.Instance.PolicyNo = value;
        }
    }
    [Serializable]
    public sealed class Sessions
    {
        #region Singleton

        private const string SESSION_SINGLETON_NAME = "Singleton_502E69E5-668B-E011-951F-00155DF26207";

        private Sessions()
        {

        }

        public static Sessions Instance
        {
            get
            {
                if (HttpContext.Current.Session[SESSION_SINGLETON_NAME] == null)
                {
                    HttpContext.Current.Session[SESSION_SINGLETON_NAME] = new Sessions();
                }

                return HttpContext.Current.Session[SESSION_SINGLETON_NAME] as Sessions;
            }
        }

        #endregion

        public bool IsAgent { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }
        public string AgencyName { get; set; }
        
        public string Username { get; set; }
        public bool IsAuth { get; set; }

        public bool IsCompany { get; set; }

        public bool IsClient { get; set; }
        public string EmailAddress { get; set; }
        public string PolicyNumber { get; set; }

        public string PolicyNo { get; set; }
        public bool IsNewQuote { get; set; }
        public string RatingGuid { get; set; }
        public string PrevAction { get; set; }

        public int PrevRatingId { get; set; }
        public string RatingErrMsg { get; set; }

        public int SweepBtnHitCount { get; set; }

        public string PaymntResponseLink { get; set; }
        public string DocUploadResponse { get; set; }

        public string CompanyName { get; set; }


        public string CompanyLogoName { get; set; }
        public string CompanyPrimaryColor { get; set; }

        public string CompanySecondaryColor { get; set; }
        public string WhoWeAre { get; set; }

        public string FullServiceCo { get; set; }

        public string WhoWeRepresent { get; set; }

        public string WhyThisCompany { get; set; }

        public string AboutUs { get; set; }

        public string Welcome { get; set; }

        public string OurStaff { get; set; }

        public string UnderwritingAndAccounting { get; set; }

        public string ClaimsManagement { get; set; }

        public string SpecialUnit { get; set; }

        public string InformationTechnology { get; set; }

        public string CustomerService { get; set; }

        public string AboutImageName { get; set; }

        public string SliderImageName1 { get; set; }

        public string SliderImageName2 { get; set; }

        public string SliderImageName3 { get; set; }

        public string ContactPhone { get; set; }

        public string ContactEmail { get; set; }

        public string FooterMiddle { get; set; }

        public string FooterAboutUs { get; set; }
     
    }//END Class
}//END Namespace