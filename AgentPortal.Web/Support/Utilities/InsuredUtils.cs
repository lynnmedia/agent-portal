﻿using System.Linq;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Support.Utilities
{
    public class InsuredUtils
    {
        public static string GetInsuredEmail(string policyno)
        {
            string insEmailAddr = null;
            
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getClientInfo = new XPCollection<XpoClientLogin>(uow, CriteriaOperator.Parse("PolicyNumber = ?", policyno)).FirstOrDefault();

                if (getClientInfo != null)
                {
                    insEmailAddr = getClientInfo.EmailAddress;
                }
            }
            
            return insEmailAddr;
        }//END GetInsuredEmail
        ////////////////////////////////////////////////////////////////////////////////////

    }//END Class
}//END Namespace