﻿using System;
using System.Linq;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Support.Utilities

{
    public static class WorkFlowUtilities
    {

        //private static void SaveToDiary(string description, string fileNo, int claimant, string peril, string userName, string dept, DateTime startTime)
        //{
        //    DateTime endTime = startTime.AddHours(1);
        //    string reminderInfo = string.Format("<Reminders>  <Reminder AlertTime=\"{0}\" TimeBeforeStart=\"00:00:00\" />  </Reminders>", startTime);
        //    //Insert Into Diary
        //    DiaryDO Diary = new DiaryDO();
        //    Diary.Username = userName;
        //    Diary.SetBy = userName;
        //    Diary.Status = 2;
        //    Diary.Subject = string.Format("Image Received: {0}", fileNo);
        //    Diary.Description = "Image Received: " + description;
        //    Diary.Label = 0;
        //    Diary.StartTime = startTime;
        //    Diary.EndTime = endTime;
        //    Diary.Location = fileNo;
        //    Diary.EventType = 0;
        //    Diary.ReminderInfo = reminderInfo;
        //    Diary.AddedFrom = "ADD TO IMAGING";

        //    if (!IsDuplicate(Diary))
        //    {
        //        try
        //        {
        //            Diary.Save();
        //        }
        //        catch (Exception ex)
        //        {
        //            //("[INSERT DIARY] Error Occured Inserting User Diary: " + ex).Log();
        //        }

        //        //Insert Into System Diary
        //        AuditDiaryDO AuditDiary = new AuditDiaryDO();
        //        AuditDiary.SetFor = userName;
        //        AuditDiary.SetBy = userName;
        //        AuditDiary.RemindDate = startTime;
        //        AuditDiary.EntryDate = DateTime.Now;
        //        AuditDiary.RemindType = "SYSTEM";
        //        AuditDiary.RemindText = "Image Received: " + description;
        //        AuditDiary.Cleared = false;
        //        AuditDiary.Viewed = false;
        //        AuditDiary.RemindID = "1";

        //        try
        //        {
        //            AuditDiary.Save();
        //        }
        //        catch (Exception ex)
        //        {
        //            //("[INSERT AUDIT DIARY] Error Occured Inserting Audit Diary: " + ex).Log();
        //        }
        //    }
        //    //}//END foreach
        //}//END SaveToDiary
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Assign an item to a specific group, and in particular the group member with the least amount of work.
        /// </summary>
        /// <param name="groupID">group id of the group the work should be assigned to</param>
        private static string AssignToGroup(int? groupID)
        {
            string returnUser = "";
            if (groupID == null)
                throw new Exception("Cannot assign to group. ID invalid.");
            var users = new XPCollection(typeof(PalmInsure.Palms.Xpo.WorkFlowGroupMembers), CriteriaOperator.Parse("GroupId = ?", groupID));
            int? totalWorkItems = null;
            int tmpWorkItems = 0;
            foreach (var user in users)
            {
                PalmInsure.Palms.Xpo.WorkFlowGroupMembers member = user as PalmInsure.Palms.Xpo.WorkFlowGroupMembers;
                // get work items for user. Assign to user with least amount of work.
                var userWFItems = new XPCollection(typeof(PalmInsure.Palms.Xpo.AuditImageIndexDO), CriteriaOperator.Parse("AssignedTo = ? AND IsProcessed = 0", member.Username));                
                tmpWorkItems = userWFItems.Count;
                if (totalWorkItems == null)
                    totalWorkItems = tmpWorkItems;
                if (string.IsNullOrEmpty(returnUser))
                    returnUser = member.Username;
                if (tmpWorkItems < totalWorkItems)
                    returnUser = member.Username;
            }
            return returnUser;
        }
        /// <summary>
        /// Process the work flow of a paritcular form id
        /// </summary>
        /// <param name="formID">id of form to run the work flow for.</param>
        /// <param name="fileNo">policy/claim/agent number</param>
        /// 
        public static string ProcessWorkFlow(int? formID, string fileNo, out string reclassifiedUser, string peril = null, string claimaint = null)
        {
            string returnName = "";
            reclassifiedUser = "";
            bool breakFromIteration = false;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var wfActivities = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActivity>(uow, CriteriaOperator.Parse("FormID = ?", formID));
                foreach (var item in wfActivities.Where(a => a.ItemID != 0).OrderBy(a => a.ItemID))
                {
                    if (breakFromIteration)
                        break;
                    // Primary Activities = Conditions, Actions, and Assignments ( NOT ConditionActions or ConditionAssignments)
                    if (item.IsCondition)
                    {
                        switch (item.ConditionDescID)
                        {
                            case (int)WorkFlowEnums.WorkFlowConditions.DoesPolicyExists:
                                var policy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", fileNo));
                                var policyExists = (policy.Count > 0);
                                if (!policyExists)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    returnName = AssignToGroup(conditionAssignment.AssignmentGroupID);
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = "insert into Diary (Username,Subject,Description,StartTime,EndTime,Location,ReminderInfo,AddedFrom,SetBy) values (@ChosenUser,'Workflow Exception',@Description,dateadd(day,@day,cast(GetDate() as Date)),dateadd(day,@day,cast(GetDate() as Date)),@Key,'<Reminders>  <Reminder AlertTime=\"'+cast(dateadd(day,@day,cast(GetDate() as Date)) as varchar(10))+' 12:00:00 AM\" TimeBeforeStart=\"00:00:00\" /> </Reminders>','WORKFLOW','SYSTEM')";
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else
                                    continue;
                                break;
                            case (int)WorkFlowEnums.WorkFlowConditions.IsPolicyExpired:
                                var policy2 = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ? AND ExpDate < ?", fileNo, DateTime.Now));
                                var policyExpired = (policy2.Count > 0);
                                if (!policyExpired)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    returnName = AssignToGroup(Convert.ToInt32(conditionAssignment.AssignmentGroupID));
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = "insert into Diary (Username,Subject,Description,StartTime,EndTime,Location,ReminderInfo,AddedFrom,SetBy) values (@ChosenUser,'Workflow Exception',@Description,dateadd(day,@day,cast(GetDate() as Date)),dateadd(day,@day,cast(GetDate() as Date)),@Key,'<Reminders>  <Reminder AlertTime=\"'+cast(dateadd(day,@day,cast(GetDate() as Date)) as varchar(10))+' 12:00:00 AM\" TimeBeforeStart=\"00:00:00\" /> </Reminders>','WORKFLOW','SYSTEM')";
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else
                                    continue;
                                break;
                            case (int)WorkFlowEnums.WorkFlowConditions.IsPolicyCancelled:
                                var policy3 = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ? AND PolicyStatus != 'ACTIVE'", fileNo));
                                var policyCancelled = (policy3.Count > 0);
                                if (!policyCancelled)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    returnName = AssignToGroup(conditionAssignment.AssignmentGroupID);
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = "insert into Diary (Username,Subject,Description,StartTime,EndTime,Location,ReminderInfo,AddedFrom,SetBy) values (@ChosenUser,'Workflow Exception',@Description,dateadd(day,@day,cast(GetDate() as Date)),dateadd(day,@day,cast(GetDate() as Date)),@Key,'<Reminders>  <Reminder AlertTime=\"'+cast(dateadd(day,@day,cast(GetDate() as Date)) as varchar(10))+' 12:00:00 AM\" TimeBeforeStart=\"00:00:00\" /> </Reminders>','WORKFLOW','SYSTEM')";
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else
                                    continue;
                                break;
                            case (int)WorkFlowEnums.WorkFlowConditions.DoesClaimExists:
                                var claim = new XPCollection<PalmInsure.Palms.Xpo.ClaimsDO>(uow, CriteriaOperator.Parse("ClaimNo = ?", fileNo));
                                var claimExists = (claim.Count > 0);
                                if (!claimExists)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    returnName = AssignToGroup(conditionAssignment.AssignmentGroupID);
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = action.SQLInsertQuery;
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else // move on to the next primary activity
                                    continue;
                                break;
                            case (int)WorkFlowEnums.WorkFlowConditions.DoesPerilExists:
                                var tmpClaim2 = new XPCollection<PalmInsure.Palms.Xpo.ClaimantsDO>(uow, CriteriaOperator.Parse("ClaimNo = ? AND PerilDesc = ?", fileNo, peril));
                                var perilExists = (tmpClaim2.Count > 0);
                                if (!perilExists)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    returnName = AssignToGroup(conditionAssignment.AssignmentGroupID);
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = "insert into Diary (Username,Subject,Description,StartTime,EndTime,Location,ReminderInfo,AddedFrom,SetBy) values (@ChosenUser,'Workflow Exception',@Description,dateadd(day,@day,cast(GetDate() as Date)),dateadd(day,@day,cast(GetDate() as Date)),@Key,'<Reminders>  <Reminder AlertTime=\"'+cast(dateadd(day,@day,cast(GetDate() as Date)) as varchar(10))+' 12:00:00 AM\" TimeBeforeStart=\"00:00:00\" /> </Reminders>','WORKFLOW','SYSTEM')";
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else
                                    continue;
                                break;
                            case (int)WorkFlowEnums.WorkFlowConditions.IsClaimOpen:
                                var tmpClaim = new XPCollection<PalmInsure.Palms.Xpo.ClaimantsDO>(uow, CriteriaOperator.Parse("ClaimNo = ? AND PerilDesc = ? AND Status = 'OPEN'", fileNo, peril));
                                var claimIsOpen = (tmpClaim.Count > 0);
                                if (!claimIsOpen)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    returnName = AssignToGroup(conditionAssignment.AssignmentGroupID);
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = action.SQLInsertQuery;
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else
                                    continue;
                                break;
                            case (int)WorkFlowEnums.WorkFlowConditions.DoesAgentExists:
                                var agent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentNo = ?", fileNo));
                                var agentExists = (agent.Count > 0);
                                if (!agentExists)
                                {
                                    // execute NO action and assignment
                                    var conditionNoAction = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAction).FirstOrDefault();
                                    var conditionAssignment = wfActivities.Where(a => a.ConditionParentID == item.ItemID && a.IsConditionAssignment).FirstOrDefault();
                                    returnName = AssignToGroup(conditionAssignment.AssignmentGroupID);
                                    string diaryUserName = AssignToGroup(Convert.ToInt32(conditionNoAction.DiaryAssignTo));
                                    switch (conditionNoAction.ActionDescID)
                                    {
                                        case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                            PalmInsure.Palms.Xpo.WorkFlowActions action = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", conditionNoAction.ActionDescID)).FirstOrDefault();
                                            string insertStmt = "insert into Diary (Username,Subject,Description,StartTime,EndTime,Location,ReminderInfo,AddedFrom,SetBy) values (@ChosenUser,'Workflow Exception',@Description,dateadd(day,@day,cast(GetDate() as Date)),dateadd(day,@day,cast(GetDate() as Date)),@Key,'<Reminders>  <Reminder AlertTime=\"'+cast(dateadd(day,@day,cast(GetDate() as Date)) as varchar(10))+' 12:00:00 AM\" TimeBeforeStart=\"00:00:00\" /> </Reminders>','WORKFLOW','SYSTEM')";
                                            insertStmt = insertStmt.Replace("@ChosenUser", "'" + diaryUserName + "'");
                                            insertStmt = insertStmt.Replace("@Description", "'" + conditionNoAction.DiaryDesc + "'");
                                            insertStmt = insertStmt.Replace("@day", conditionNoAction.DiaryDays.ToString());
                                            insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                            int rows = uow.ExecuteNonQuery(insertStmt);
                                            breakFromIteration = true;
                                            break;
                                    }
                                }
                                else
                                    continue;
                                break;
                        }
                    }
                    else if (item.IsAction)
                    {
                        switch (item.ActionDescID)
                        {
                            case (int)WorkFlowEnums.WorkFlowActions.AssignDiary:
                                PalmInsure.Palms.Xpo.WorkFlowActions actionItem = new XPCollection<PalmInsure.Palms.Xpo.WorkFlowActions>(CriteriaOperator.Parse("IndexID = ?", item.ActionDescID)).FirstOrDefault();
                                string userName = AssignToGroup(Convert.ToInt32(item.DiaryAssignTo));
                                string insertStmt = actionItem.SQLInsertQuery;
                                insertStmt = insertStmt.Replace("@ChosenUser", "'" + userName + "'");
                                insertStmt = insertStmt.Replace("@Description", "'" + item.DiaryDesc + "'");
                                insertStmt = insertStmt.Replace("@day", item.DiaryDays.ToString());
                                insertStmt = insertStmt.Replace("@Key", "'" + fileNo + "'");
                                int rows = uow.ExecuteNonQuery(insertStmt);
                                break;
                        }
                    }
                    else if (item.IsAssignment)
                    {
                        returnName = AssignToGroup(item.AssignmentGroupID);
                    }                                      
                    
                }
            }
            return returnName;
        }
        //private static bool IsDuplicate(DiaryDO diary)
        //{
        //    bool isDuplicate = false;
        //    var diaryIndex = new XPCollection(typeof(DiaryDO), CriteriaOperator.Parse(string.Format("Location = '{0}' AND Status = 2 and ClearedBy is NULL", diary.Location)));
        //    return isDuplicate = diaryIndex != null && diaryIndex.Count > 0;
        //}

    }
}
