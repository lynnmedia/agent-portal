﻿using System;
using System.Collections.Generic;
using AgentPortal.Support.DataObjects;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Utilities.Extensions;
using AgentPortal.Models;
using System.Data;
using System.Data.SqlClient;
using PalmInsure.Palms.Xpo;
using DevExpress.Data.Filtering;
using System.Linq;

namespace AgentPortal.Support.Utilities
{
    public class PortalPaymentUtils
    {
        public static UtilitiesEmail EmailNoReply = new UtilitiesEmail("smtp.office365.com", "noreply@palminsure.com", "4g1c1nsur4nc3#", 587);
        public static UtilitiesEmail EmailSystem = new UtilitiesEmail("smtp.office365.com", "system@palminsure.com", "4g1c1nsur4nc3$", 587);
        //--------------------------
        public static bool HasErrors { get; set; }
        public static string ErrMsg { get; set; }
        public static string TransId { get; set; }
        public static string OrderId { get; set; }
        public static string ReturnMsg { get; set; }

        private static void Reset()
        {
            HasErrors = false;
            ErrMsg = null;
            TransId = null;
            ReturnMsg = null;
        }

        public static void AgentSweep()
        {
            
        }

        public static void CheckPayment()
        {
            
        }

        public static void PostCreditCard(string cardNo, string expMonth, string expYear, string cardCvv, double amt, string payorname, string policyno)
        {
            Reset();
            IPaymentProcessor post = new FirstData();
            post.CardNo = cardNo;
            post.Cvv = cardCvv;
            post.CardExpYear = expYear.Last(2);
            post.CardExpMonth = expMonth;
            post.Amount = amt;
            post.PayorName = payorname;
            post.PolicyNo = policyno;
            //-----------
            post.PostCredit();
            //-----------
            HasErrors = post.ResponseCode.ToUpper() != "APPROVED" || post.HasErrors;
            ErrMsg = post.ErrMsg;
            TransId = post.TransId;
            ReturnMsg = post.ResponseCode;
            OrderId = post.OrderId;
        }

        public static void PostToPayments(string policyno, 
                                            string paymentType, 
                                            DateTime acctDate, 
                                            DateTime postmarkDate, 
                                            double checkAmt, 
                                            double totalAmt, 
                                            string username, 
                                            string guid, 
                                            string checkNotes,
                                            string transId,
                                            string responseCode)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var save = new XpoPayments(uow)
                {
                    PolicyNo = policyno,
                    PaymentType = paymentType,
                    PostmarkDate = postmarkDate,
                    AcctDate = acctDate,
                    CheckAmt = checkAmt,
                    TotalAmt = totalAmt,
                    UserName = username,
                    BatchGUID = guid,
                    CreateDate = DateTime.Now,
                    CheckNotes = checkNotes,
                    TokenID =  transId,
                    PortalRespCode =  responseCode
                };
                save.Save();
                uow.CommitChanges();
            }
        }

        public static void SendSweepSuccessMail(string policyno, 
            string name, 
            string agentEmail, 
            string agentcode, 
            double renAmt, 
            double amtPrior, 
            double modelAmt, 
            bool isRenewal, 
            bool isDev,
            bool isCheck)
        {
            string paymentType = isCheck ? "Check Payment" : "Agent Sweep";
            string subject = String.Format("[PALMINSURE.COM] {2} received for {0} on {1}.",
                                               DateTime.Now.ToShortDateString(), policyno, paymentType);
            //////////////////////////////////

            string pymntMailNotes = isRenewal
                ? string.Format(@"{3} {0}. <br/> Renewal Payment: {1}. <br/> Prior Policy Payment: {2}", agentcode, renAmt, amtPrior, isCheck ? "Check Payment" : "Sweep by agent code")
                                        : string.Format("{1} {0}", agentcode, isCheck ? "Check Payment" : "Sweep by agent code");

            string body = EmailRenderer.PaymentSuccess(insuredName: name,
                                                       policyNo: policyno,
                                                       agentcode: agentcode,
                                                       paymentType: "Sweep",
                                                       date: DateTime.Now,
                                                       amount: modelAmt,
                                                       notes: pymntMailNotes);

            try
            {
                string sendTo = isDev ? "it@palminsure.com" : "underwriting@palminsure.com";
                EmailSystem.SendMail(sendTo, "system@palminsure.com", subject, body, true);
                LogUtils.Log(Sessions.Instance.Username,
                             string.Format("{0}: SEND SUCCESS {2} EMAIL TO UNDERWRITING FOR AGENT {1}",
                                           Sessions.Instance.PolicyNo, 
                                           Sessions.Instance.AgentCode,
                                           isCheck ? " CHECK" : "SWEEP"), "INFO");
            }
            catch
            {
                LogUtils.Log(Sessions.Instance.Username,
                             string.Format(
                                 "{0}: ERROR OCCURED SENDING SUCCESS {2} EMAIL TO UNDERWRITING FOR AGENT {1}",
                                 Sessions.Instance.PolicyNo, Sessions.Instance.AgentCode,
                                           isCheck ? " CHECK" : "SWEEP"), "FATAL");
            }

            //////////////////////////////////

            if (!String.IsNullOrEmpty(agentEmail))
            {
                try
                {
                    EmailNoReply.SendMail(agentEmail, "noreply@palminsure.com", subject, body, true);
                    LogUtils.Log(Sessions.Instance.Username,
                                 string.Format("{0}: SEND SUCCESS {2} EMAIL TO AGENT AT {1}",
                                               Sessions.Instance.PolicyNo, agentEmail,
                                           isCheck ? " CHECK" : "SWEEP"), "INFO");
                }
                catch (NullReferenceException)
                {
                    "Agent email returned null, email now craps out".Log();
                    LogUtils.Log(Sessions.Instance.Username,
                                 string.Format(
                                     "{0}: SENDING EMAIL TO AGENT CRAPPED OUT, EMAIL SENT TO AGENT AT {1}",
                                     Sessions.Instance.PolicyNo, agentEmail), "INFO");
                }
                catch (System.Net.Mail.SmtpException)
                {
                    "Office 365 is being stupid".Log();
                    LogUtils.Log(Sessions.Instance.Username,
                                 string.Format(
                                     "{0}: SENDING EMAIL TO AGENT CRAPPED OUT, EMAIL SENT TO AGENT AT {1}",
                                     Sessions.Instance.PolicyNo, agentEmail), "INFO");
                }
            }
        }

        public static void SendVendorPaymentSuccess(string policyno, string subject, string body, string agentEmailAddr, string insEmailAddr, bool isTestEnv, bool sendToBoth)
        {
            try
            {
                string sendTo = isTestEnv ? "it@palminsure.com" : "underwriting@palminsure.com";
                EmailSystem.SendMail(sendTo, "system@palminsure.com", subject, body, true);
                LogUtils.Log(agentEmailAddr, string.Format("{0}: SENT EMAIL TO UNDERWRITING AT PALMINSURE.COM", policyno), "INFO");
            }
            catch
            {
                LogUtils.Log(agentEmailAddr, string.Format("{0}: ERROR OCCURED SENDING MAIL TO UND AT PALMINSURE.COM", policyno), "FATAL");
            }
            //--------------------
            try
            {
                EmailNoReply.SendMail(agentEmailAddr, "noreply@palminsure.com", subject, body, true);
                LogUtils.Log(agentEmailAddr, string.Format("{0}: SENT EMAIL TO AGENT AT {1}", policyno, agentEmailAddr), "INFO");
            }
            catch
            {
                LogUtils.Log(agentEmailAddr, string.Format("{0}: FAILED SENDING EMAIL TO AGENT AT {1}", policyno, agentEmailAddr), "INFO");
            }
            //--------------------
            if (sendToBoth)
            {
                try
                {
                    EmailNoReply.SendMail(insEmailAddr, "noreply@palminsure.com", subject, body, true);
                }
                catch (Exception)
                {
                    LogUtils.Log(insEmailAddr, string.Format("{0}: FAILED SENDING PAYMENT EMAIL TO {2}", policyno, agentEmailAddr, insEmailAddr), "WARN");
                }
            }
        }//END SendVendorPaymentFailed

        public static void SendVendorPymntFailed(string policyno, 
                                                string subject, 
                                                string body, 
                                                string agentEmailAddr,
                                                string insEmailAddr, 
                                                bool isTestEnv, 
                                                bool sendToBoth)
        {
            try
            {
                EmailNoReply.SendMail(agentEmailAddr, "noreply@palminsure.com", subject, body, true);
            }
            catch
            {
                "Sending mail to agent/user crapped out, could not send".Log();
                LogUtils.Log(agentEmailAddr, string.Format("{0}: SENT FAILED PAYMENT EMAIL TO AGENT AT {1}", policyno, agentEmailAddr), "WARN");
            }
            //-------------------------------
            if (sendToBoth)
            {
                try
                {
                    EmailNoReply.SendMail(insEmailAddr, "noreply@palminsure.com", subject, body, true);
                }
                catch
                {
                    LogUtils.Log(insEmailAddr, string.Format("{0}: FAILED SENDING FAILED PAYMENT EMAIL TO {2}", policyno, agentEmailAddr, insEmailAddr), "WARN");
                }
            }
        }


        public static List<EndorsementInsallmentsModel> GetEndorsementInstallments(string pPolicyNo, bool isRated =false)
        {
            var endorsementInstalls = new List<EndorsementInsallmentsModel>();
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                //if (isRated)
                //{
                    DataTable dt = new DataTable();
                    var connString = System.Configuration.ConfigurationManager.ConnectionStrings["AlertAutoConnectionString"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connString))
                    using (SqlCommand cmd = new SqlCommand("spUW_GetEndorsementInstallmentsPolicy", conn))
                    {
                        cmd.CommandTimeout = 3600;
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add(new SqlParameter()
                        {
                            ParameterName = "@PolicyNo",
                            Value = Sessions.Instance.PolicyNo
                        });
                        cmd.Parameters.Add(new SqlParameter()
                        {
                            ParameterName = "@isRated",
                            Value =  isRated
                        });
                        SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                        adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
                        adapt.Fill(dt);
                    }
                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow dRow in dt.Rows)
                        {
                            var endorsementInstall = new EndorsementInsallmentsModel();
                            endorsementInstall.PolicyNo = dRow["PolicyNo"] != null ? dRow["PolicyNo"].ToString() : string.Empty;
                            endorsementInstall.InstallNo = dRow["InstallNo"] != null ? Convert.ToDecimal(dRow["InstallNo"].ToString()) : 0m;
                            endorsementInstall.Descr = dRow["Descr"] != null ? dRow["Descr"].ToString() : string.Empty;
                            if (dRow["DueDate"] != null)
                                endorsementInstall.DueDate = Convert.ToDateTime(dRow["DueDate"].ToString());
                            endorsementInstall.CurrInstallmentAmount = dRow["CurrInstallmentAmount"] != null ? Convert.ToDecimal(dRow["CurrInstallmentAmount"].ToString()) : 0m;
                            endorsementInstall.PymtApplied = dRow["PymtApplied"] != null ? Convert.ToDecimal(dRow["PymtApplied"].ToString()) : 0m;
                            endorsementInstall.CurrentBalance = dRow["CurrentBalance"] != null ? Convert.ToDecimal(dRow["CurrentBalance"].ToString()) : 0m;
                            endorsementInstall.NewBalance = dRow["NewBalance"] != null ? Convert.ToDecimal(dRow["NewBalance"].ToString()) : 0m;
                            endorsementInstall.EndorseChangeAmount = dRow["EndorseChangeAmount"] != null ? Convert.ToDecimal(dRow["EndorseChangeAmount"].ToString()) : 0m;

                            endorsementInstall.PremiumeChangeAmount = dRow["PremiumChangeAmount"] != null ? Convert.ToDecimal(dRow["PremiumChangeAmount"].ToString()) : 0m;
                            endorsementInstall.PremiumeAmount = dRow["PremiumAmount"] != null ? Convert.ToDecimal(dRow["PremiumAmount"].ToString()) : 0m;
                            endorsementInstall.Fees = dRow["Fees"] != null ? Convert.ToDecimal(dRow["Fees"].ToString()) : 0m;
                            endorsementInstall.TotalDue = dRow["TotalDue"] != null ? Convert.ToDecimal(dRow["TotalDue"].ToString()) : 0m;

                            endorsementInstall.OldPremiumeAmount = dRow["OldPremiumAmount"] != null ? Convert.ToDecimal(dRow["OldPremiumAmount"].ToString()) : 0m;


                        endorsementInstalls.Add(endorsementInstall);
                        }
                    }
                //}
                //else
                //{
                //    var policyPaymentSchds = PortalPaymentUtils.GetPaymentSchedule(pPolicyNo);
                //    if (policyPaymentSchds?.Any() == true)
                //    {
                //        foreach (var paymentSch in policyPaymentSchds)
                //        {
                //            if(paymentSch.Description?.ToLower()?.Contains("deposit") ==true || paymentSch.Description?.ToLower()?.Contains("installment") == true)
                //            {
                //                var endorsementInstall = new EndorsementInsallmentsModel();
                //                endorsementInstall.PolicyNo = paymentSch.PolicyNo;
                //                endorsementInstall.InstallNo = paymentSch.InstallmentNo;
                //                endorsementInstall.Descr = paymentSch.Description;
                //                if (paymentSch.DueDate != null)
                //                    endorsementInstall.DueDate = paymentSch.DueDate;
                //                endorsementInstall.CurrInstallmentAmount = paymentSch.InstallmentAmount;
                //                endorsementInstall.PymtApplied = paymentSch.PaymentsApplied;
                //                endorsementInstall.CurrentBalance = paymentSch.CurrentBalance;
                //                endorsementInstall.NewBalance = 0m;
                //                endorsementInstall.EndorseChangeAmount = 0m;
                //                endorsementInstalls.Add(endorsementInstall);
                //            }
                //        }
                //    }
                //}
                return endorsementInstalls;
            }
        }

        public static List<PaymentScheduleModel> GetPaymentSchedule(string pPolicyNo)
        {
            var paymentScheduleModels = new List<PaymentScheduleModel>();
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                DataTable dt = new DataTable();
                var connString = System.Configuration.ConfigurationManager.ConnectionStrings["AlertAutoConnectionString"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand cmd = new SqlCommand("spuw_GetPolicyPaymentSchedule", conn))
                {
                    cmd.CommandTimeout = 360;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@PolicyNo",
                        Value = pPolicyNo
                    } );
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@UserName",
                        Value = Sessions.Instance.Username
                    });
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapt.Fill(dt);
                }
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                      
                        var paymentSchedule = new PaymentScheduleModel();
                        try
                        {
                            paymentSchedule.PolicyNo = dRow["PolicyNo"] != null ? dRow["PolicyNo"].ToString() : string.Empty;
                            paymentSchedule.InstallmentNo = dRow["DisplayInstallNo"] != null ? Convert.ToDecimal(dRow["DisplayInstallNo"].ToString()) : 0m;
                            paymentSchedule.Description = dRow["Descr"] != null ? dRow["Descr"].ToString() : string.Empty;
                            //paymentSchedule.DueDate = dRow["DueDate"] != null ? dRow["DueDate"].ToString() : string.Empty;
                            if (dRow["DueDate"] != null)
                                paymentSchedule.DueDate = Convert.ToDateTime(dRow["DueDate"].ToString());

                            if (!dRow.IsNull("CurrInstallmentAmount"))
                                paymentSchedule.InstallmentAmount = dRow["CurrInstallmentAmount"] != null ? Convert.ToDecimal(dRow["CurrInstallmentAmount"].ToString()) : 0m;
                            if (!dRow.IsNull("PymtApplied"))
                                paymentSchedule.PaymentsApplied = dRow["PymtApplied"] != null ? Convert.ToDecimal(dRow["PymtApplied"].ToString()) : 0m;
                            if (!dRow.IsNull("CurrentBalance"))
                                paymentSchedule.CurrentBalance = dRow["CurrentBalance"] != null ? Convert.ToDecimal(dRow["CurrentBalance"].ToString()) : 0m;
                        }
                        catch(Exception ex)
                        {}

                        paymentScheduleModels.Add(paymentSchedule);
                    }
                }
                return paymentScheduleModels;
            }
        }

        public static List<PaymentsDataForPayScheduleModel> GetPolicyPaymentsDataForPaySchedule(string pPolicyNo)
        {
            var paymentsDataModels = new List<PaymentsDataForPayScheduleModel>();
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                DataTable dt = new DataTable();
                var connString = System.Configuration.ConfigurationManager.ConnectionStrings["AlertAutoConnectionString"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(connString))
                using (SqlCommand cmd = new SqlCommand("spUW_GetPolicyPaymentsData", conn))
                {
                    cmd.CommandTimeout = 360;
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@PolicyNo",
                        Value = pPolicyNo
                    });
                    cmd.Parameters.Add(new SqlParameter()
                    {
                        ParameterName = "@UserName",
                        Value = Sessions.Instance.Username
                    });
                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    adapt.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapt.Fill(dt);
                }
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        var paymentData = new PaymentsDataForPayScheduleModel();
                        try
                        {
                            paymentData.Confirmation = dRow["Confirmation"] != null ? dRow["Confirmation"].ToString() : string.Empty;
                            //paymentData.PostmarkDate = dRow["PostmarkDate"] != null ? dRow["PostmarkDate"].ToString() : string.Empty;
                            if (dRow["PostmarkDate"] != null)
                                paymentData.PostmarkDate = Convert.ToDateTime(dRow["PostmarkDate"].ToString());

                            paymentData.PaymentType = dRow["PaymentType"] != null ? dRow["PaymentType"].ToString() : string.Empty;
                            paymentData.PostedBy = dRow["PostedBy"] != null ? dRow["PostedBy"].ToString() : string.Empty;

                            if(!dRow.IsNull("TotalAmt"))
                               paymentData.TotalAmt = dRow["TotalAmt"] != null ? Convert.ToDecimal(Convert.ToString(dRow["TotalAmt"])) : 0m;

                            if (!dRow.IsNull("FeeAmt"))
                               paymentData.FeeAmt   = dRow["FeeAmt"] != null ? Convert.ToDecimal(Convert.ToString(dRow["FeeAmt"])) : 0m;

                        }
                        catch (Exception ex)
                        { }
                        paymentsDataModels.Add(paymentData);
                    }
                }

                return paymentsDataModels;
            }
        }
    }//END Class
}//END Namespace