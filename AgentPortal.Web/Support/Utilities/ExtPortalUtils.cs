﻿using System;
using System.ServiceModel;
using AgentPortal.PalmsServiceReference;

namespace AgentPortal.Support.Utilities
{
    public static class ExtPortalUtils
    {
        public static void CloseRef(this PalmsServiceClient client)
        {
            try
            {
                client.Close();
            }
            catch (CommunicationException e)
            {
                client.Abort();
            }
            catch (TimeoutException e)
            {
    
                client.Abort();
            }
            catch (Exception e)
            {
                client.Abort();
            }
        }
    }//END Class
}//END Namespace