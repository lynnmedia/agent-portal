﻿using System;
using System.Configuration;
using System.Reflection;
using System.Xml;

namespace AgentPortal.Support.Utilities
{
    public class ConfigSettings
    {
        private ConfigSettings() { }

        public static string ReadSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// Write Settings to app.config
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void WriteSetting(string key, string value)
        {
            XmlDocument doc = loadConfigDocument();
            XmlNode node = doc.SelectSingleNode("//appSettings");

            if (node == null)
            {
                throw new InvalidOperationException("appSettings section not found in config file.");
            }

            try
            {
                XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

                if (elem != null)
                {
                    elem.SetAttribute("value", value);
                }
                else
                {
                    elem = doc.CreateElement("add");
                    elem.SetAttribute("key", key);
                    elem.SetAttribute("value", value);
                    node.AppendChild(elem);
                }
                doc.Save(getConfigFilePath());
            }
            catch
            {
                throw;
            }
        }//WriteSetting

        /// <summary>
        /// Remove settings from app config file
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveSetting(string key)
        {
            XmlDocument doc = loadConfigDocument();

            XmlNode node = doc.SelectSingleNode("//appSettings");

            try
            {
                if (node == null)
                    throw new InvalidOperationException("appSettings section not found in config file.");
                else
                {
                    node.RemoveChild(node.SelectSingleNode(string.Format("//add[@key='{0}']", key)));
                    doc.Save(getConfigFilePath());
                }
            }
            catch (NullReferenceException e)
            {
                throw new Exception(string.Format("The key {0} does not exist.", key), e);
            }
        }//Remove Settings

        private static XmlDocument loadConfigDocument()
        {
            XmlDocument doc = null;
            try
            {
                doc = new XmlDocument();
                doc.Load(getConfigFilePath());
                return doc;
            }
            catch (System.IO.FileNotFoundException)
            {
                //throw new Exception("No config filed found.", e);
                return new XmlDocument();
            }
        }

        private static string getConfigFilePath()
        {
            return Assembly.GetExecutingAssembly().Location + ".config";
        }
    }
}
