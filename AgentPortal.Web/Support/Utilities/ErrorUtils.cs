﻿using System.Linq;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Support.Utilities
{
    public class ErrorUtils
    {
        public static string ErrCodeToId(DErrors error)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getErrId = new XPCollection<SystemErrorCodes>(uow, CriteriaOperator.Parse("ShortErrMsg = ?", error.ToString())).FirstOrDefault();

                return getErrId == null ? "-1" : string.Format("D{0}", getErrId.IndexID);
            }
        }

        public static string ErrCodeToLongDesc(DErrors error)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getErrId = new XPCollection<SystemErrorCodes>(uow, CriteriaOperator.Parse("ShortErrMsg = ?", error.ToString())).FirstOrDefault();

                return getErrId == null ? "-1" : getErrId.ErrorMsg;
            }
        }

    }//END Class
}//END Namespace