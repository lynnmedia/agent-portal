﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities;

namespace AgentPortal.Support.Utilities
{
    public class AgentUtils
    {
        public static void SweepAcct(string agentCode, string policyNumber, double amount, string username, bool useAltBankInfo = false)
        {
            Session session = new Session();
            AuditACHTransactions ach = new AuditACHTransactions(session)
            {
                AgentCode      = agentCode,
                Amt            = amount,
                Date           = DateTime.Now,
                RecordKey      = policyNumber,
                RecordType     = "BUS",
                IsReported     = false,
                Username       = username,
                SourceLocation = "WEBSITE-AGENTSWEEP",
                UseTempAcct = useAltBankInfo
            };
            ach.Save();
        }//END AgentSweep
        ////////////////////////////////////////////////////////////////////////////

        public static string GetEmailAddr(string agentcode)
        {
            string emailAddr = null;

            if (!string.IsNullOrEmpty(agentcode))
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var agent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentcode)).FirstOrDefault();
                    if (agent != null)
                    {
                        emailAddr = string.IsNullOrEmpty(agent.EmailAddress) ? agent.AltEmailAddress : agent.EmailAddress;
                    }
                }   
            }
            return emailAddr;
        }//END GetAgentEmailAddr
        ////////////////////////////////////////////////////////////////////////////
        
        public static string GetCorrectAgentCode(string policyNo, bool isQuote = true, bool isRenewal = false)
        {
            string correctCode = null;

            if (isQuote)
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    XpoPolicyQuote quote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
                    correctCode = quote == null ? "" : quote.AgentCode;
                }
            }
            else if (isRenewal)
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    PolicyRenQuote quote = new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", policyNo)).FirstOrDefault();
                    correctCode = quote == null ? "" : quote.AgentCode;
                }
            }
            else
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    XpoPolicy quote = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
                    correctCode = quote == null ? "" : quote.AgentCode;
                }
            }
            
            return correctCode;
        }
        ////////////////////////////////////////////////////////////////////////////
        
        public static string GetAgentCode(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string agentcode = null;

                XpoPolicy PolicyDO = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno)).FirstOrDefault();

                if(PolicyDO == null)
                {
                    agentcode = "INVALID POLICYNO";
                }
                else
                {
                    agentcode = PolicyDO.AgentCode;
                }

                return agentcode;
            }
        }
        ////////////////////////////////////////////////////////////////////////////

        public static string GetAgencyName(string agentcode)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string agencyname = null;

                XpoAgents xpoAgent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentcode)).FirstOrDefault();

                if (xpoAgent == null)
                {
                    agencyname = "INVALID AGENCT CODE";
                }
                else
                {
                    agencyname = xpoAgent.AgencyName;
                }

                return agencyname;
            }
        }
        ////////////////////////////////////////////////////////////////////////////

        public static double GetAgentComm(string agentcode)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                double amt = 0;

                XpoAgents xpoAgent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentcode)).FirstOrDefault();
                amt = xpoAgent == null ? 0.0 : xpoAgent.CommissionRate;

                return amt;
            }
        }

        public static T GetScalar<T>(string fieldname, string agentcode)
        {
            using(UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoAgents xpoAgent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentcode)).FirstOrDefault();

                if (xpoAgent == null)
                {
                    return default(T);
                }

                return (T)xpoAgent.GetPropValue(fieldname);
            }
        }

        public static AgentViewModel MapXpotoModel(XpoAgents xpoAgent)
        {
            if (xpoAgent == null)
                return new AgentViewModel();

            return new AgentViewModel()
            {
                AgentCode = xpoAgent.AgentCode,
                AgentGuid = xpoAgent.AgentGuid,
                CommissionRate = xpoAgent.CommissionRate,
                County = xpoAgent.County,
                DateLicensed = xpoAgent.DateLicensed,
                DateOpened = xpoAgent.DateOpened,
                DBA = xpoAgent.DBA,
                EmailAddress = xpoAgent.EmailAddress,
                Fax = xpoAgent.Fax,
                FEIN = xpoAgent.FEIN,
                Group = xpoAgent.Group,
                InitialContact = xpoAgent.InitialContact,
                IsAllowedHigherBI = xpoAgent.IsAllowedHigherBI,
                IsOwnerOperator = xpoAgent.IsOwnerOperator,
                IsPaperless = xpoAgent.IsPaperless,
                IsProbation = xpoAgent.IsProbation,
                IsRequireCall = xpoAgent.IsRequireCall,
                LastReview = xpoAgent.LastReview,
                MailAddress = xpoAgent.MailAddress,
                MailCity = xpoAgent.MailCity,
                MailGarageSame = xpoAgent.MailGarageSame,
                MailState = xpoAgent.MailState,
                MailZIP = xpoAgent.MailZIP,
                MarketingRep = xpoAgent.MarketingRep,
                MarketingRepUserID = xpoAgent.MarketingRepUserID,
                OID = xpoAgent.OID,
                Password = xpoAgent.Password,
                Phone = xpoAgent.Phone,
                PhysicalAddress = xpoAgent.PhysicalAddress,
                PhysicalCity = xpoAgent.PhysicalCity,
                PhysicalState = xpoAgent.PhysicalState,
                PhysicalZIP = xpoAgent.PhysicalZIP,
                PrimaryFirstName = xpoAgent.PrimaryFirstName,
                PrimaryLastName = xpoAgent.PrimaryLastName,
                Status = xpoAgent.Status,
                StatusDate = xpoAgent.StatusDate,
                TerminationCause = xpoAgent.TerminationCause,
                Website = xpoAgent.Website
            };
        }
        public static XpoAgents GetAgent(string agentCode)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                var agent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentCode)).FirstOrDefault();
                return agent;
            }
        }

        public static AgentViewModel GetAgentModel(string agentCode)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                var agent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentCode)).FirstOrDefault();
                var agentVM = MapXpotoModel(agent);
                return agentVM;
            }
        }
        ////////////////////////////////////////////////////////////////////////////

        public static bool ValidateEmailFormat(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            return new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(email.Trim()).Success;

        }

        public static string HighlightEndorseField(List<AgentEndorseReasons> reasons, string licenseno = null, string vin = null, params string[] reasonCodes)
        {
            if (reasons.Any(x => reasonCodes.Contains(x.ReasonCode) &&
                        (licenseno == null || x.LicenseNo == licenseno) &&
                        (vin == null || x.Vin == vin)))
            {
                return "color:red;background-color:yellow";
            }
            return string.Empty;
        }
        public static string HighlightEndorseField(List<AgentEndorseReasons> reasons, params string[] reasonCodes)
        {
            return HighlightEndorseField(reasons, null, null, reasonCodes);
        }
        public static string ValidateEndorseField(List<AgentEndorseReasons> missingReasons, List<AgentEndorseReasons> unselectedReasons, string fieldName, params string[] verifyReasonCodes)
        {
            var errorMessage = string.Empty;
            if (missingReasons?.Any() == true)
            {
                    var missingReason = missingReasons.FirstOrDefault(x => verifyReasonCodes.Any(v=>x.ReasonCode == v));
                    if (missingReason != null)
                    {
                        errorMessage = $"<span style=\"color:red;margin:3px;display:block\">Update Required for {fieldName??missingReason.Reason}</span>";
                    }
            }
            if (string.IsNullOrEmpty(errorMessage) && unselectedReasons?.Any() == true)
            {
                var unselectedReason = unselectedReasons.FirstOrDefault(x => verifyReasonCodes.Any(i => i == x.ReasonCode));
                if (unselectedReason!=null)
                {
                    errorMessage = $"<span style=\"color:red;margin:3px;display:block\">Update Identifed for unselected reason {fieldName??unselectedReason.Reason}</span>";
                }
            }
            return errorMessage;
        }
    }//END CLass
}//END Namesapce