﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using iTextSharp.text.html;
using PalmInsure.Palms.Utilities;

namespace AgentPortal.Support.Utilities
{
    public static class PortalUtils
    {
        public static bool IsDevEnvironment()
        {
            bool isTrue = false;

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                SystemVars sysVars =
                    new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = 'SYSTEMISDEV'")).FirstOrDefault();
                if (sysVars != null)
                {
                    isTrue = sysVars.Value == "true" ? true : false;
                }
            }

            return isTrue;
        }
        //END IsDevEnvironment
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void AuditCybersourcePymnt(PaymentResponseModel model)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoAuditCyberSourcePayments Pymnt = new XpoAuditCyberSourcePayments(uow);
                Pymnt.requestID = model.csrequestID;
                Pymnt.pymntPolicyNumber = model.pymntPolicyNumber;
                Pymnt.portalController = model.portalController;
                Pymnt.paymentOption = model.cspaymentOption;
                Pymnt.orderAmount = model.csorderAmount.Parse<double>();
                Pymnt.ccAuthReply_amount = model.csccAuthReply_amount.Parse<double>();
                Pymnt.reasonCode = model.csreasonCode;
                Pymnt.decision = model.csdecision;
                Pymnt.billTo_firstName = model.csbillTo_firstName;
                Pymnt.billTo_lastName = model.csbillTo_lastName;
                Pymnt.billTo_street1 = model.csbillTo_street1;
                Pymnt.billTo_city = model.csbillTo_city;
                Pymnt.billTo_state = model.csbillTo_state;
                Pymnt.billTo_postalCode = model.csbillTo_postalCode;
                Pymnt.billTo_country = model.csbillTo_country;
                Pymnt.card_expirationYear = model.cscard_expirationYear;
                Pymnt.card_expirationMonth = model.cscard_expirationMonth;
                Pymnt.ccAuthReply_authorizedDateTime = model.csccAuthReply_authorizedDateTime;
                Pymnt.ccAuthReply_authorizationCode = model.csccAuthReply_authorizationCode;
                Pymnt.ccAuthReply_avsCodeRaw = model.csccAuthReply_avsCodeRaw;
                Pymnt.ccAuthReply_reasonCode = model.csccAuthReply_reasonCode;
                Pymnt.ccAuthReply_cvCode = model.csccAuthReply_cvCode;
                Pymnt.ccAuthReply_avsCode = model.csccAuthReply_avsCode;
                Pymnt.ccAuthReply_processorResponse = model.csccAuthReply_processorResponse;
                Pymnt.ccAuthReply_cvCodeRaw = model.csccAuthReply_cvCodeRaw;
                Pymnt.orderAmount_publicSignature = model.csorderAmount_publicSignature;
                Pymnt.orderPage_serialNumber = model.csorderPage_serialNumber;
                Pymnt.orderCurrency = model.csorderCurrency;
                Pymnt.orderPage_requestToken = model.csorderPage_requestToken;
                Pymnt.orderCurrency_publicSignature = model.csorderCurrency_publicSignature;
                Pymnt.orderPage_transactionType = model.csorderPage_transactionType;
                Pymnt.orderNumber_publicSignature = model.csorderNumber_publicSignature;
                Pymnt.reconciliationID = model.csreconciliationID;
                Pymnt.signedDataPublicSignature = model.cssignedDataPublicSignature;
                Pymnt.transactionSignature = model.cstransactionSignature;
                Pymnt.decision_publicSignature = model.csdecision_publicSignature;
                Pymnt.merchantID = model.csmerchantID;
                Pymnt.orderNumber = model.csorderNumber;
                Pymnt.Save();
                uow.CommitChanges();
            }
        }
        //END AuditCyberSourcePayments
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        public static string GetEnglishPayPlan(string ratecycle, string payplan)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string payPlanToString = null;

                try
                {
                    AARatePayPlans getPayPlan =
                        new XPCollection<AARatePayPlans>(uow, CriteriaOperator.Parse("RateCycle = ? AND PayPlan = ?",
                                                                                  ratecycle,
                                                                                  payplan)).FirstOrDefault();

                    double payPlanDownPayment = getPayPlan.DownPayment*100;
                    int installmentCount = getPayPlan.InstallCount;
                    bool EFT = getPayPlan.EFT;
                    string payPlanInstallments = installmentCount == 1
                                                     ? string.Format("{0} installment", installmentCount)
                                                     : string.Format("{0} installments", installmentCount);
                    payPlanToString = EFT
                                          ? String.Format("{0}% down with {1} (EFT Discount)", payPlanDownPayment,
                                                          payPlanInstallments)
                                          : String.Format("{0}% down with {1}", payPlanDownPayment, payPlanInstallments);
                }
                catch (Exception ex)
                {
                    LogUtils.Log(Sessions.Instance.Username, "ERROR SHOWING PAY PLAN AS ENGLISH: " + ex.Message, "WARN");
                }

                return payPlanToString;
            }
        }
        //END GetEnglishPayPlan
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        */
        public static string GetEnglishPayPlan(string ratecycle, string payplan)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string payPlanToString = null;

                try
                {
                    XpoAARatePayPlans getPayPlan =
                        new XPCollection<XpoAARatePayPlans>(uow, CriteriaOperator.Parse("RateCycle = ? AND IndexID = ?",
                                                                                  ratecycle,
                                                                                  payplan)).FirstOrDefault();

                    double payPlanDownPayment = getPayPlan.DP_AsPerc * 100;
                    int installmentCount = (int)getPayPlan.Installments;
                    bool EFT = getPayPlan.EFT == 1;
                    string payPlanInstallments = installmentCount == 1
                                                     ? string.Format("{0} installment", installmentCount)
                                                     : string.Format("{0} installments", installmentCount);
                    payPlanToString = EFT
                                          ? String.Format("{0}% down with {1} (EFT Discount)", payPlanDownPayment,
                                                          payPlanInstallments)
                                          : String.Format("{0}% down with {1}", payPlanDownPayment, payPlanInstallments);
                }
                catch (Exception ex)
                {
                    LogUtils.Log(Sessions.Instance.Username, "ERROR SHOWING PAY PLAN AS ENGLISH: " + ex.Message, "WARN");
                }

                return payPlanToString;
            }
        }
        //END GetEnglishPayPlan
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        public static string GetAgentInfo(string agentCode)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string agentHtmlInfo = null;

                XpoAgents getAgentInfo =
                    new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentCode)).FirstOrDefault();

                if (getAgentInfo != null)
                {
                    string mailingAddress = string.Format("{0}, {1} {2} {3}", getAgentInfo.MailAddress,
                                                          getAgentInfo.MailCity, getAgentInfo.MailState,
                                                          getAgentInfo.MailZIP);
                    string physicalAddress = string.Format("{0}, {1} {2} {3} {4}", getAgentInfo.PhysicalAddress,
                                                           getAgentInfo.PhysicalCity, getAgentInfo.PhysicalState,
                                                           getAgentInfo.PhysicalZIP, getAgentInfo.County);
                    string primaryContact = string.Format("{0}, {1}", getAgentInfo.PrimaryLastName,
                                                          getAgentInfo.PrimaryFirstName);

                    agentHtmlInfo =
                        string.Format(
                            @"<div class=agentinfobubble><b>Mailing Address:</b> <span class=agentinfotxt>{0}</span><br/><b>Physical Address:</b> <span class=agentinfotxt>{1}</span><br/><b>Primary Contact:</b> <span class=agentinfotxt>{2}</span><br/><b>Phone:</b> <span class=agentinfotxt>{3}</span><br/><b>Email Address:</b> <span class=agentinfotxt>{4}</span></div>",
                            mailingAddress, physicalAddress, primaryContact, getAgentInfo.Phone,
                            getAgentInfo.EmailAddress);
                }
                return HttpUtility.HtmlEncode(agentHtmlInfo);
            }
        }
        //END GetAgentInfo
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string AlertDevEnvironment()
        {
            string msg = null;
            try
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    SystemVars sysVars =
                        new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = 'SYSTEMISDEV'")).FirstOrDefault // use SYSTEMISDEV to show dev message.
                            ();
                    if (sysVars != null)
                    {
                        if (sysVars.Value == "true")
                        {
                            msg = "<div class='warning'>Warning: This is the DEVELOPMENT environment</div>";
                        }
                    }
                }
            }
            catch (Exception)
            {
                msg = "";
            }

            return msg;
        }
        //END AlertDevEnvironment
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetHistoryReason(string policyno, string indexID)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string reason = null;

                XpoAuditPolicyHistoryReasons Reason1 =
                    new XPCollection<XpoAuditPolicyHistoryReasons>(uow,
                                                                CriteriaOperator.Parse(
                                                                    "PolicyNo = ? AND HistoryKey = ? AND ReasonNo = '1'",
                                                                    policyno, indexID)).FirstOrDefault();
                XpoAuditPolicyHistoryReasons Reason2 =
                    new XPCollection<XpoAuditPolicyHistoryReasons>(uow,
                                                                CriteriaOperator.Parse(
                                                                    "PolicyNo = ? AND HistoryKey = ? AND ReasonNo = '2'",
                                                                    policyno, indexID)).FirstOrDefault();
                XpoAuditPolicyHistoryReasons Reason3 =
                    new XPCollection<XpoAuditPolicyHistoryReasons>(uow,
                                                                CriteriaOperator.Parse(
                                                                    "PolicyNo = ? AND HistoryKey = ? AND ReasonNo = '3'",
                                                                    policyno, indexID)).FirstOrDefault();
                XpoAuditPolicyHistoryReasons Reason4 =
                    new XPCollection<XpoAuditPolicyHistoryReasons>(uow,
                                                                CriteriaOperator.Parse(
                                                                    "PolicyNo = ? AND HistoryKey = ? AND ReasonNo = '4'",
                                                                    policyno, indexID)).FirstOrDefault();

                reason = Reason1 != null ? Reason1.Reason : "";
                reason += Reason2 != null ? Reason2.Reason : "";
                reason += Reason3 != null ? Reason3.Reason : "";
                reason += Reason4 != null ? Reason4.Reason : "";

                return reason;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static DateTime GetMinDuePayDate(string policyno)
        {
            DateTime MinDuePayDate = DateTime.Now;

            if (!string.IsNullOrEmpty(policyno))
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    if (policyno.Contains("Q-FL"))
                    {
                        XpoPolicyQuote getQuote =
                            new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno))
                                .FirstOrDefault();
                        MinDuePayDate = getQuote != null ? getQuote.MinDuePayDate : DateTime.Now;
                    }
                    else
                    {
                        XpoPolicy getPolicy =
                            new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno))
                                .FirstOrDefault();
                        MinDuePayDate = getPolicy != null ? getPolicy.MinDuePayDate : DateTime.Now;
                    }

                    return MinDuePayDate;
                }
            }
            else
            {
                return MinDuePayDate;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetRenPolicyNo(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string renpolicyno = null;

                PolicyRenQuote getRen =
                    new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", policyno))
                        .FirstOrDefault();

                if (getRen != null)
                {
                    renpolicyno = getRen.PolicyNo;
                }

                return renpolicyno;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static bool IsBadCounty(string zip, DateTime effdate)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string rateCycle = GetRateCycle(zip, effdate);
                int numItems =
                    new XPCollection<XpoAARateBaseRates>(uow,
                                                      CriteriaOperator.Parse("Zip = ? AND RateCycle = ?", zip, rateCycle))
                        .Count;
                bool isBad = numItems < 1 ? true : false;

                return isBad;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private static string GetRateCycle(string zip, DateTime effdate)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string ratecycle = null;

                XPCollection<XpoAARatePayPlans> getRate = new XPCollection<XpoAARatePayPlans>(uow, CriteriaOperator.Parse("RateCycle = 'AA20190101'"));   // 'FLAGIC120515'"));
                ratecycle = getRate.Count < 1 ? "" : getRate.Max(m => m.RateCycle);

                return ratecycle;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void SaveApprovalRec(string policyno, string quoteno, string agentcode)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoAuditPolicyApprovalDo Save = new XpoAuditPolicyApprovalDo(uow);
                Save.PolicyNo = policyno;
                Save.QuoteNo = quoteno;
                Save.AgentCode = agentcode;
                Save.DateBound = DateTime.Now;
                Save.Save();
                uow.CommitChanges();
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void AddDiary(string userFor, string policyno, string agentcode, string descr)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                Diary Save = new Diary(uow);
                Save.Username = userFor;
                Save.Status = 2;
                Save.Subject = string.Format("Image Received: {0}", policyno);
                Save.Label = 0;
                Save.StartTime = DateTime.Now;
                Save.EndTime = DateTime.Now;
                Save.Location = policyno;
                Save.EventType = 0;
                Save.ReminderInfo =
                    string.Format(
                        "<Reminders>  <Reminder AlertTime=\"{0}\" TimeBeforeStart=\"00:00:00\" />  </Reminders>",
                        DateTime.Now);
                Save.AddedFrom = "AGENT UPLOADS";
                Save.SetBy = agentcode;
                Save.Description = descr;
                Save.Save();
                uow.CommitChanges();
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetInsuredName(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string insuredname = "";

                XpoPolicy getPolicy =
                    new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno)).FirstOrDefault();

                if (getPolicy != null)
                {
                    insuredname = getPolicy.Ins1FullName;
                }

                return insuredname;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetEffDate(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string effdate = "";

                XpoPolicy getPolicy =
                    new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno)).FirstOrDefault();

                if (getPolicy != null)
                {
                    effdate = getPolicy.EffDate.ToShortDateString();
                }

                return effdate;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetExpDate(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string expdate = "";

                XpoPolicy getPolicy =
                    new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno)).FirstOrDefault();

                if (getPolicy != null)
                {
                    expdate = getPolicy.ExpDate.ToShortDateString();
                }

                return expdate;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string TranslateLyonsCode(int reasoncode, string fieldname)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                SystemLyonsReasonCodes ReasonCodes =
                    new XPCollection<SystemLyonsReasonCodes>(uow, CriteriaOperator.Parse("ReasonCode = ?", reasoncode))
                        .FirstOrDefault();

                if (reasoncode == null)
                {
                    return null;
                }

                return (string)ReasonCodes.GetPropValue(fieldname);
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetSysVars(string name)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                SystemVars Vars =
                    new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = ?", name)).FirstOrDefault();
                return Vars == null ? "" : Vars.Value;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void PostFailedQueue(string username,
                                           string recordkey,
                                           string sendto,
                                           string subject,
                                           string body,
                                           string failedlocation,
                                           string attachments,
                                           string faillocation)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                FailedQueue save = new FailedQueue(uow)
                {
                    ApplicationName = "PORTAL",
                    Attachments = attachments,
                    Body = body,
                    DateCreated = DateTime.Now,
                    FailedLocation = faillocation,
                    HasAttachments = !string.IsNullOrEmpty(attachments),
                    IsProcessed = false,
                    RecordKey = recordkey,
                    SendTo = sendto,
                    Subject = subject,
                    Username = username
                };

                save.Save();
                uow.CommitChanges();
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void PostAgentUploadRec(string recordno, string newfilename, string oldfilename, string fullpath, string doctype, string username)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                uow.BeginTransaction();
                XpoAgentUploads upload = new XpoAgentUploads(uow);
                upload.DateCreated = DateTime.Now;
                upload.ImagingFileName = newfilename;
                upload.RecordKey = recordno;
                upload.RenamedFileName = newfilename;
                upload.UploadFileName = oldfilename;
                upload.FullPath = fullpath;
                upload.DocType = doctype;
                upload.Username = username;
                upload.Save();
                uow.CommitChanges();
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static bool HasFnrReasons(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                bool isTrue = false;

                XPCollection<XpoAuditPolicyHistoryReasons> getFnrReasons =
                    new XPCollection<XpoAuditPolicyHistoryReasons>(
                        uow, CriteriaOperator.Parse("PolicyNo = ? AND ReasonType = 'FNR' AND Status != 'Clear'", policyno));
                XPCollection<XpoAuditPolicyHistoryReasons> getFnrReasons2 =
                    new XPCollection<XpoAuditPolicyHistoryReasons>(
                        uow, CriteriaOperator.Parse(
                            "PolicyNo = ? AND ReasonType = 'SUSPENSE' AND Cancel = '1' AND Status != 'Clear'", policyno));

                isTrue = getFnrReasons.Count > 0 ? true : getFnrReasons2.Count > 0 ? true : false;

                return isTrue;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetNoticedPolicyStatus(string policyno)
        {
            string status = null;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XPCollection<AuditPolicyHistory> getHistory = new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Action = 'NOTICE'", policyno));

                foreach (var aphPolData in getHistory.OrderByDescending(m => m.EffDate))
                {
                    PolicyFutureActions getPFActionsData = new XPCollection<PolicyFutureActions>(uow, CriteriaOperator.Parse("PolicyNo = ? AND MailDate =? AND Cancel= '0' AND Action='CANCEL'", policyno, aphPolData.MailDate)).FirstOrDefault();
                    if (getPFActionsData == null)
                        continue;
                    if (getPFActionsData != null)
                    {
                        switch (aphPolData.CancelType.ToUpper())
                        {
                            case "NON-PAY":
                                status = string.Format("NOTICED - {0} {1}", aphPolData.CancelType, aphPolData.NoticeAmount);
                                break;
                            default:
                                status = string.Format("NOTICED - {0}", aphPolData.CancelType);
                                break;
                        }
                    }

                    if (!String.IsNullOrEmpty(status))
                        break;
                }
            }


            return status;
        }//END GetNoticedPolicyStatus
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string GetDisplayStatus(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string policystatus = null;

                XpoPolicy xpoPolicy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno)).FirstOrDefault();

                if (xpoPolicy != null)
                {
                    xpoPolicy.Reload();
                    DateTime expdate = new DateTime(xpoPolicy.ExpDate.Year, xpoPolicy.ExpDate.Month, xpoPolicy.ExpDate.Day, 0, 0, 0);
                    DateTime now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    int compare = DateTime.Compare(expdate, now);

                    policystatus = HasFnrReasons(policyno)
                                       ? xpoPolicy.PolicyStatus + " (FUTURE NON-RENEWAL)"
                                       : xpoPolicy.PolicyStatus;

                    if (xpoPolicy.RenewalStatus == "NON-RENEW")
                    {
                        policystatus = xpoPolicy.PolicyStatus;
                    }

                    if (xpoPolicy.PolicyStatus == "NOTICE")
                    {
                        policystatus = GetNoticedPolicyStatus(policyno);
                    }

                    if (xpoPolicy.PolicyStatus == "ACTIVE" && compare < 1)
                    {
                        policystatus = "EXPIRED";
                    }

                }
                return policystatus;
            }
        }//END GetDisplayStatus
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static void AuditPayments(string username,
                                         PymntLocation location,
                                         PymntTypes pymntType,
                                         double totalAmt,
                                         double priorAmt,
                                         double renAmt,
                                         double modelAmt,
                                         string pymntGuid,
                                         bool isSweep,
                                         bool isFirstData,
                                         bool isRen)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var pymnt = new XpoAuditPortalPaymentsDo(uow)
                {
                    Username = username,
                    PymntLocation = PymntLocationToString(location),
                    PymntType = PymntTypeToString(pymntType),
                    TotalAmount = totalAmt,
                    RenAmt = renAmt,
                    PriorAmt = priorAmt,
                    ModelAmt = modelAmt,
                    PymntBatchGuid = pymntGuid,
                    IsSweep = isSweep,
                    IsFirstData = isFirstData,
                    IsRenewal = isRen,
                    DateCreated = DateTime.Now
                };
                pymnt.Save();
                uow.CommitChanges();
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string PymntTypeToString(PymntTypes type)
        {
            switch (type)
            {
                case PymntTypes.NewBusiness:
                    return "NEW BUSINESS";
                case PymntTypes.Renewal:
                    return "RENEWAL";
                case PymntTypes.Reinstatement:
                    return "REINSTATEMENT";
                default:
                    return "";
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string PymntLocationToString(PymntLocation location)
        {
            switch (location)
            {
                case PymntLocation.AgentSweep:
                    return "AGENT SWEEP";
                case PymntLocation.CyberSource:
                    return "CYBERSOURCE";
                default:
                    return "";
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static bool GetPortalOption(string optionName)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                PortalOptions option = new XPCollection<PortalOptions>(uow, CriteriaOperator.Parse("OptionName = ?", optionName)).FirstOrDefault();

                return option == null ? false : option.IsActive;
            }
        }

        public static void PostUndAlert(string msg)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var save = new XpoAuditUndAlerts
                {
                    AlertLocation = "PORTAL",
                    DateCreated = DateTime.Now,
                    Msg = msg,
                    IsActive = true
                };
                save.Save();
                uow.CommitChanges();
            }
        }

        public static void GeneratePaymentReceipt(string id, string pymnttype, string logo, string filepath)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string insuredname = PortalUtils.GetInsuredName(id);
                string effdate = PortalUtils.GetEffDate(id);
                string expdate = PortalUtils.GetExpDate(id);
                string paytype = null;
                string paynotes = null;
                string paydate = null;
                string payamt = null;

                switch (pymnttype)
                {
                    case "PAYMENTVENDOR":
                        Payments getPayment =
                            new XPCollection<Payments>(uow, CriteriaOperator.Parse("PolicyNo = ?", id))
                                .OrderByDescending(m => m.IndexID).FirstOrDefault();
                        paytype = "BUS";
                        paynotes = "First Data";
                        if (getPayment != null)
                        {
                            paydate = getPayment.AcctDate.ToShortDateString();
                            payamt = getPayment.TotalAmt.ToMoney<double>();
                        }
                        break;
                    case "CHECKING":
                        AuditACHTransactions getChecking =
                            new XPCollection<AuditACHTransactions>(uow, CriteriaOperator.Parse("RecordKey = ?", id))
                                .OrderByDescending(m => m.IndexID).FirstOrDefault();
                        paytype = "BUS";
                        paynotes = "First Data";
                        if (getChecking != null)
                        {
                            paydate = getChecking.Date.ToShortDateString();
                            payamt = getChecking.Amt.ToMoney<double>();
                        }
                        break;
                    case "ACH":
                        AuditACHTransactions getAch =
                            new XPCollection<AuditACHTransactions>(uow, CriteriaOperator.Parse("RecordKey = ?", id))
                                .OrderByDescending(m => m.IndexID).FirstOrDefault();
                        paytype = "BUS";
                        paynotes = "AGENT SWEEP";
                        if (getAch != null)
                        {
                            paydate = getAch.Date.ToShortDateString();
                            payamt = getAch.Amt.ToMoney<double>();
                        }
                        break;
                }

                ReportRenderer.PaymentReceipt(outputPdfPath: filepath,
                                              logoPath: logo,
                                              address: "Representing Palm Insure, Inc. P.O. Box 25277 Sarasota, FL 34277-2277",
                                              policyNo: id,
                                              insuredName: insuredname,
                                              policyEffDate: effdate,
                                              policyExpDate: expdate,
                                              payType: paytype,
                                              payNotes: paynotes,
                                              payDate: paydate,
                                              payAmt: payamt);
            }
        }//END GeneratePaymentReceipt

        public static bool EnableCreditScore()
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                SystemVars getVar = new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = 'CREDITSCORE'")).FirstOrDefault();

                return getVar == null ? false : getVar.Value.Parse<bool>();
            }
        }

        public static string GetCancelTypeOfPolicyWithNotice(string pPolicyno)
        {
            string canceltype = string.Empty;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicy xpoPolicy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", pPolicyno)).FirstOrDefault();

                if (xpoPolicy != null && (xpoPolicy.PolicyStatus == "NOTICE"))
                {
                    List<AuditPolicyHistory> listAuditPolHistory = new List<AuditPolicyHistory>();
                    XPCollection<AuditPolicyHistory> getHistory = new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Action = 'NOTICE'", pPolicyno));
                    if (getHistory != null && getHistory.Count > 0)
                    {
                        listAuditPolHistory = getHistory.OrderByDescending(m => m.IndexID).ToList();
                        listAuditPolHistory = listAuditPolHistory.Take(1).ToList();
                        canceltype = listAuditPolHistory[0].CancelType.Upper();
                    }
                }

                return canceltype;

            }
        }


        public static string GetInsuredEmail(string policyno)
        {
            if (String.IsNullOrEmpty(policyno))
                return String.Empty;

            string insEmailAddr = null;

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getPolicyInfo = new XPCollection<PolicyEndorseQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno)).FirstOrDefault();

                if (getPolicyInfo != null)
                {
                    insEmailAddr = getPolicyInfo.MainEmail;
                    if (String.IsNullOrEmpty(insEmailAddr))
                        insEmailAddr = getPolicyInfo.AltEmail;
                }
            }

            return insEmailAddr;
        }

        public static bool ZipHasLowerDownPayment(string rateCycle, string zip)
        {
            if (string.IsNullOrEmpty(rateCycle) || string.IsNullOrEmpty(zip))
            {
                return false;
            }

            bool hasLowerDownPayment = false;
            try
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    XpoAARateTerritory territoryInfo =
                        uow.FindObject<XpoAARateTerritory>(CriteriaOperator.Parse("RateCycle = ? AND ZIP = ?", rateCycle, zip));
                    if (territoryInfo != null)
                    {
                        if (territoryInfo.HasLowerDownPayment)
                        {
                            hasLowerDownPayment = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(ex.Message, "WARN");
            }
            
            return hasLowerDownPayment;
        }

        public static bool AgentAndZipRequires33PercentDown(string agentCode, string zipCode, bool physicalDamSelected)
        {
            if (string.IsNullOrEmpty(agentCode) || string.IsNullOrEmpty(zipCode))
            {
                return false;
            }

            try
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    XpoAgentsRestrictionByZipAndCoverage restrictionForAgent =
                        uow.FindObject<XpoAgentsRestrictionByZipAndCoverage>(
                            CriteriaOperator.Parse("AgentCode = ? AND Zip = ?", agentCode, zipCode));

                    if (restrictionForAgent == null)
                    {
                        return false;
                    }


                    //At this point the agent and zip are in the restricted table.
                    bool agentHasPhyDamFlag = restrictionForAgent.RestrictedCoverageName != null && restrictionForAgent.RestrictedCoverageName.Equals("PhyDam");
                    if (agentHasPhyDamFlag && !physicalDamSelected)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log(ex.Message, "WARN");
            }

            return false;
        }

        public static bool IsPolicyPayPlan33PercentDownOrPaidInFull(XpoPolicy xpoPolicy)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                using (Session session = XpoHelper.GetNewSession())
                {
                    string currentRateCycle = xpoPolicy.RateCycle;
                    //Get all pay plans for current rate cycle
                    IList<XpoAARatePayPlans> payPlansForCurrentCycle =
                        new XPCollection<XpoAARatePayPlans>(uow, CriteriaOperator.Parse("RateCycle = ?", currentRateCycle));

                    //Is current plan 33% down or paid in full?
                    IList<XpoAARatePayPlans> thirtyThreePercentDownAndPaidInFullPlans =
                        payPlansForCurrentCycle.Where(pp => pp.DP_AsPerc.Equals(0.332) || pp.DP_AsPerc.Equals(1)).ToList();

                    //if its not 33% or PIF plan, return false
                    if (thirtyThreePercentDownAndPaidInFullPlans.Any(pp => pp.IndexID.Equals(xpoPolicy.PayPlan)))
                    {
                        return false;
                    }

                    return true;
                }
            }
        }

        public static bool AuditDataForReinstatementACH(string pPaymentFor, double pPayment, string pPolicyNo, string guid,string pUserName)
        {
            try
            {
                using (UnitOfWork uowork = XpoHelper.GetNewUnitOfWork())
                {
                    AuditAutoREIForPostACHTrans reiACHDetails = new AuditAutoREIForPostACHTrans(uowork);
                    reiACHDetails.PolicyNo = pPolicyNo;
                    reiACHDetails.PaidDate = DateTime.Now;
                    reiACHDetails.PaymentType = pPaymentFor;
                    reiACHDetails.PayAmt = pPayment;
                    reiACHDetails.PymntBatchGuid = guid;
                    reiACHDetails.UserName = pUserName;
                    reiACHDetails.Save();
                    uowork.CommitChanges();
                   
                }
            }
            catch (Exception ex)
            {
                ex.Log($"AuditDataForReinstatementACH: {ex.Message}", "WARN");
            }
            return true;
        }
		
    } //END Class
} //END Namespace