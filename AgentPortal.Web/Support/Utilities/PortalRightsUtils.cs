﻿using System.Linq;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Support.Utilities
{
    public class PortalRightsUtils
    {
        public static bool HasRights(string rightsDesc, string userId, bool isAgent)
        {
            bool isTrue = false;

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                PortalRights Rights = new XPCollection<PortalRights>(uow, CriteriaOperator.Parse("ShortDesc = ?", rightsDesc)).FirstOrDefault();
                
                int rightID = Rights.IndexID;

                PortalRightsMask RightsMask = new XPCollection<PortalRightsMask>(uow, CriteriaOperator.Parse("AgentCode = ?", userId)).FirstOrDefault();
                if (RightsMask == null)
                {
                    return false;
                }

                int zeroBasedTag = rightID - 1;
                string maskedRight = RightsMask.Rights.Substring(zeroBasedTag, 1);

                if (maskedRight != "0")
                {
                    isTrue = true;
                }
            }
            return isTrue;
        }//END HasRights
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }//END Class
}//END Namespace