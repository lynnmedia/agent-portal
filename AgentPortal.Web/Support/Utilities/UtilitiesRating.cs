﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using Newtonsoft.Json;
using PalmInsure.Palms.Utilities;

namespace AgentPortal.Support.Utilities
{
    public static class UtilitiesRating
    {

        #region Model to Rating mappers

        public static void MapModelToXpo(QuoteModel GetQuote, XpoPolicyQuote Quote, bool isNewPolicyQuote = false)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                DateTime PrevExpDate = new DateTime(GetQuote.PreviousExpDate.ToString().Parse<DateTime>().Year, GetQuote.PreviousExpDate.ToString().Parse<DateTime>().Month, GetQuote.PreviousExpDate.ToString().Parse<DateTime>().Day, 0, 0, 0);
                DateTime EffDate = new DateTime(GetQuote.EffectiveDate.ToString().Parse<DateTime>().Year, GetQuote.EffectiveDate.ToString().Parse<DateTime>().Month, GetQuote.EffectiveDate.ToString().Parse<DateTime>().Day, 0, 0, 0);
                TimeSpan span = EffDate - PrevExpDate;
                double totalDays = span.TotalDays;
                bool has110Lapse = totalDays >= 1 && totalDays <= 10 ? true : false;
                bool has1131Lapse = totalDays >= 11 && totalDays <= 30 ? true : false;
                bool isUndoPriorCov = totalDays >= 31 ? true : false;

                XPCollection<PolicyDrivers> getDrivers = new XPCollection<PolicyDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", GetQuote.QuoteId));
                XPCollection<PolicyCars> getCars = new XPCollection<PolicyCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", GetQuote.QuoteId));

                Quote.PolicyNo = GetQuote.QuoteId;
                Quote.PriorPolicyNo = GetQuote.PriorPolicyNo.Upper();
                Quote.EffDate = GetQuote.EffectiveDate;
                Quote.ExpDate = GetQuote.EffectiveDate.AddMonths(GetQuote.PolicyTermMonths);
                Quote.Ins1First = GetQuote.FirstName1.Upper();
                Quote.Ins1Last = GetQuote.LastName1.Upper();
                Quote.Ins1MI = GetQuote.MiddleInitial1.Upper();
                Quote.Ins1FullName = string.Format("{0} {1}", GetQuote.FirstName1, GetQuote.LastName1).Upper();
                Quote.Ins1Suffix = GetQuote.Suffix1.Upper();
                Quote.Ins2First = GetQuote.FirstName2.Upper();
                Quote.Ins2Last = GetQuote.LastName2.Upper();
                Quote.Ins2MI = GetQuote.MiddleInitial2.Upper();
                Quote.Ins2Suffix = GetQuote.Suffix2.Upper();
                Quote.Ins2FullName = string.IsNullOrEmpty(GetQuote.FirstName2) && string.IsNullOrEmpty(GetQuote.LastName2) ? null : string.Format("{0} {1}", GetQuote.FirstName2, GetQuote.LastName2).Upper();
                Quote.SixMonth = (GetQuote.PolicyTermMonths == 6);
                Quote.isAnnual = (GetQuote.PolicyTermMonths == 12);

                if (GetQuote.SameAsGarage)
                {
                    Quote.MailStreet = GetQuote.SameAsGarage ? GetQuote.GarageStreetAddress.Upper() : GetQuote.MailingStreetAddress.Upper();
                    Quote.MailCity = GetQuote.SameAsGarage ? GetQuote.GarageCity.Upper() : GetQuote.MailingCity.Upper();
                    Quote.MailState = GetQuote.SameAsGarage ? GetQuote.GarageState.Upper() : GetQuote.MailingState.Upper();
                    Quote.MailZip = GetQuote.SameAsGarage ? GetQuote.GarageZIPCode.Upper() : GetQuote.MailingZIPCode.Upper();
                }
                else
                {
                    Quote.MailStreet = string.IsNullOrEmpty(GetQuote.MailingStreetAddress) ? GetQuote.MailingStreetAddress.Upper() : GetQuote.MailingStreetAddress.Upper();
                    Quote.MailCity = string.IsNullOrEmpty(GetQuote.MailingCity) ? GetQuote.GarageCity.Upper() : GetQuote.MailingCity.Upper();
                    Quote.MailState = string.IsNullOrEmpty(GetQuote.MailingState) ? GetQuote.GarageState.Upper() : GetQuote.MailingState.Upper();
                    Quote.MailZip = string.IsNullOrEmpty(GetQuote.MailingZIPCode) ? GetQuote.GarageZIPCode.Upper() : GetQuote.MailingZIPCode.Upper();
                    Quote.MailGarageSame = true;
                }

                Quote.GarageStreet = GetQuote.GarageStreetAddress.Upper();
                Quote.GarageCity = GetQuote.GarageCity.Upper();
                Quote.GarageState = GetQuote.GarageState.Upper();
                Quote.GarageZip = GetQuote.GarageZIPCode.Upper();
                Quote.GarageCounty = GetQuote.GarageCounty.Upper();
                Quote.HomePhone = GetQuote.HomePhone.Upper();
                Quote.WorkPhone = GetQuote.WorkPhone.Upper();
                Quote.MainEmail = "";
                if (!string.IsNullOrEmpty(GetQuote.EmailAddress))
                    Quote.MainEmail = GetQuote.EmailAddress.ToLower();
                //Quote.MainEmail = GetQuote.EmailAddress.Lower();
                Quote.PIPDed = GetQuote.PipDeductible.Upper();
                Quote.NIO = (GetQuote.WorkLossGroup == 1);
                Quote.NIRR = (GetQuote.WorkLossGroup == 2); // commented out 9/28/18 || (GetQuote.WorkLossGroup == 3);

                if (GetQuote.Workloss.HasValue)
                {
                    Quote.WorkLoss = GetQuote.Workloss.GetValueOrDefault() == true ? 1 : 0;
                }

                Quote.BILimit = GetQuote.BiLimit.Upper();
                Quote.PDLimit = GetQuote.PdLimit.Upper();
                Quote.UMLimit = GetQuote.UmLimit.Upper();
                Quote.MedPayLimit = GetQuote.MedPayLimit == null ? GetQuote.MedPayLimit : GetQuote.MedPayLimit.Upper().Replace(",", "");
                Quote.Homeowner = GetQuote.HasHomeownersDisc;
                Quote.TransDisc = (int)GetQuote.TransDisc;
                Quote.Paperless = GetQuote.Paperless;
                Quote.AdvancedQuote = GetQuote.AdvancedQuote;
                Quote.HasPPO = GetQuote.HasPPO;
                Quote.PreviousCompany = GetQuote.PriorCompany.Upper();
                Quote.PreviousBI = GetQuote.PreviousBI;
                Quote.DirectRepairDisc = GetQuote.DirectRepairDiscount;
                Quote.ADNDLimit = GetQuote.AdndLimit.Parse<int>();
                Quote.PayPlan = GetQuote.PayPlan;
                Quote.PreviousExpDate = GetQuote.PreviousExpDate == null ? "1/1/0001".Parse<DateTime>() : (DateTime)GetQuote.PreviousExpDate;
                Quote.AgentCode = GetQuote.AgentCode.Upper();
                Quote.MailGarageSame = GetQuote.SameAsGarage;
                Quote.UMStacked = GetQuote.UMstacked;
                Quote.QuotedDate = GetQuote.QuotedDate;
                Quote.IsShow = GetQuote.IsShow;
                Quote.HasLOU = GetQuote.HasLou;
                Quote.HasBI = GetQuote.BiLimit.Upper() != "NONE" ? true : false;
                Quote.HasUM = GetQuote.UmLimit.Upper() != "NONE" ? true : false;
                Quote.HasMP = GetQuote.MedPayLimit.Upper() != "0" ? true : false;
                Quote.HasADND = GetQuote.AdndLimit.Upper() != "0" ? true : false;
                Quote.PIPLimit = "10000";
                Quote.HasPD = true;
                Quote.Cars = getCars.Count();
                Quote.Drivers = getDrivers.Count();
                Quote.HasLapse110 = has110Lapse;
                Quote.HasLapse1131 = has1131Lapse;
                Quote.HasPriorCoverage = isUndoPriorCov ? false : GetQuote.PriorCoverage;
                Quote.PriorBILimit = GetQuote.PriorBiLimit;
                if (isNewPolicyQuote)
                {
                    Quote.IsAllIntegrationCompleted = isNewPolicyQuote;
                    Quote.RCPOSOrdered = isNewPolicyQuote;
                }
                else if (!isNewPolicyQuote)
                {
                    bool isRCPOSOrdered = false;
                    Quote.IsAllIntegrationCompleted = IntegrationsUtility.GetFlagForAllIntegrationCall(GetQuote.QuoteId, ref isRCPOSOrdered);
                    Quote.RCPOSOrdered = isRCPOSOrdered;
                    //  Quote.MaintenanceFee = GetQuote.Main
                }
            }
        }

        public static void MapModelToXpo(VehicleModel Cars, PolicyQuoteCars SaveCars)
        {
            SaveCars.PolicyNo = Sessions.Instance.PolicyNo;
            SaveCars.CarIndex = Cars.CarIndex;
            SaveCars.VINIndex = Cars.VinIndexNo;
            SaveCars.VIN = Cars.VIN;
            SaveCars.VehYear = Cars.Year;
            SaveCars.VehMake = Cars.Make;
            SaveCars.VehModel1 = Cars.VehModel;
            SaveCars.VehModel2 = Cars.Style;
            SaveCars.BusinessUse = Cars.IsBusinessUse;
            SaveCars.CompDed = Cars.ComprehensiveDeductible;
            SaveCars.CollDed = Cars.CollisionDeductible;
            SaveCars.HasColl = Cars.HasPhyDam;
            SaveCars.HasComp = Cars.HasPhyDam;
            SaveCars.HasPhyDam = Cars.HasPhyDam;
            SaveCars.GrossVehicleWeight = Cars.GrossVehicleWeight;
            SaveCars.ClassCode = Cars.ClassCode;
            SaveCars.ABS = Convert.ToInt32(Cars.HasAntiLockBrakes);
            SaveCars.ARB = Convert.ToInt32(Cars.ARB);
            SaveCars.ATD = Cars.ATD;
            SaveCars.AmountCustom = Cars.AmountCustom == null ? 0.0 : (double)Cars.AmountCustom;
            SaveCars.Artisan = Cars.Artisan;
            SaveCars.MilesToWork = Cars.MilesToWork == null ? 0 : (int)Cars.MilesToWork;
            SaveCars.EngineType = Cars.EngineType;
            SaveCars.COMPSymbol = Cars.CompSymbol;
            SaveCars.COLLSymbol = Cars.CollSymbol;
            SaveCars.BISymbol = Cars.BISymbol;
            SaveCars.PDSymbol = Cars.PDSymbol;
            SaveCars.MPSymbol = Cars.MPSymbol;
            SaveCars.PurchNew = Cars.IsPurchasedNew;
            if (!string.IsNullOrEmpty(Cars.DatePurchased))
            {
                DateTime purchasedDate;
                bool validDate =  DateTime.TryParse(Cars.DatePurchased, out purchasedDate);
                if (validDate)
                {
                    SaveCars.DatePurchased = purchasedDate;
                }
            }
            else
            {
                SaveCars.DatePurchased = null;
            }
            DateTime currentDate = DateTime.Now;
            int sixYearsAgo = currentDate.AddYears(-6).Year;
            SaveCars.PIPSymbol = Cars.PIPSymbol;
            double costNewAmt;
            if (double.TryParse(Cars.CostNew, out costNewAmt))
            {
                SaveCars.CostNew = costNewAmt;
            }
        }

        public static void MapModelToXpo(DriverModel Driver, PolicyQuoteDrivers SaveDrivers)
        {
            SaveDrivers.PolicyNo = Sessions.Instance.PolicyNo;
            SaveDrivers.FirstName = Driver.FirstName.Upper();
            SaveDrivers.LastName = Driver.LastName.Upper();
            SaveDrivers.HasMvr = Driver.HasMvr;
            SaveDrivers.MvrDate = Driver.MvrDate;
            SaveDrivers.LicenseStatus = Driver.LicenseStatus;
            SaveDrivers.LicenseExpiration = Driver.LicenseExpiration;
            SaveDrivers.VeriskMvrOrderNumber = Driver.VeriskMvrOrderNumber;
            SaveDrivers.MI = Driver.MiddleInitial.Upper();
            SaveDrivers.Suffix = Driver.Suffix.Upper();
            SaveDrivers.Class = Driver.Class;
            SaveDrivers.Gender = Driver.Gender.ToString().Upper();
            SaveDrivers.Married = Driver.IsMarried;
            SaveDrivers.DOB = Driver.DateOfBirth.Parse<DateTime>();
            SaveDrivers.LicenseNo = Driver.DriversLicense.Upper();
            SaveDrivers.LicenseSt = Driver.LicenseState.Upper();
            SaveDrivers.Unverifiable = Driver.Unverifiable;
            SaveDrivers.InternationalLic = Driver.IsInternationalLicense;
            SaveDrivers.Exclude = Driver.IsExcluded;
            SaveDrivers.RelationToInsured = Driver.Relation.ToString().Upper();
            SaveDrivers.Occupation = Driver.Occupation.Upper();
            SaveDrivers.Company = Driver.EmployerName.Upper();
            SaveDrivers.DateLicensed = Driver.DateLicensed == string.Empty ? "1/1/1900".Parse<DateTime>() : Driver.DateLicensed.Parse<DateTime>();
            SaveDrivers.SeniorDefDrvDisc = Driver.HasSeniorDefDrvDisc;
            SaveDrivers.SeniorDefDrvDate = Driver.SeniorDefDrvDate == null ? "1/1/1900".Parse<DateTime>() : (DateTime)Driver.SeniorDefDrvDate;
            SaveDrivers.SR22 = Driver.HasSR22;
            SaveDrivers.SR22CaseNo = Driver.SR22CaseNo;
            SaveDrivers.Exclude = Driver.IsExcluded;
        }

        public static void MapModelToXpo(EndorsementPolicyModel PolicyModel, PolicyEndorseQuote PolicyEndorsement)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                DateTime PrevExpDate = new DateTime(PolicyModel.PreviousExpDate.ToString().Parse<DateTime>().Year, PolicyModel.PreviousExpDate.ToString().Parse<DateTime>().Month, PolicyModel.PreviousExpDate.ToString().Parse<DateTime>().Day, 0, 0, 0);
                DateTime EffDate = new DateTime(PolicyModel.EffectiveDate.ToString().Parse<DateTime>().Year, PolicyModel.EffectiveDate.ToString().Parse<DateTime>().Month, PolicyModel.EffectiveDate.ToString().Parse<DateTime>().Day, 0, 0, 0);
                TimeSpan span = EffDate - PrevExpDate;
                double totalDays = span.TotalDays;
                bool has110Lapse = totalDays >= 1 && totalDays <= 10 ? true : false;
                bool has1131Lapse = totalDays >= 11 && totalDays <= 30 ? true : false;
                bool isUndoPriorCov = totalDays >= 31 ? true : false;

                XPCollection<PolicyEndorseQuoteDrivers> getDrivers = new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyModel.PolicyNo));
                XPCollection<PolicyEndorseQuoteCars> getCars = new XPCollection<PolicyEndorseQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", PolicyModel.PolicyNo));

                PolicyEndorsement.PolicyNo = PolicyModel.PolicyNo;
                PolicyEndorsement.PriorPolicyNo = PolicyModel.PriorPolicyNo.Upper();
                PolicyEndorsement.EffDate = PolicyModel.EffectiveDate;
                PolicyEndorsement.ExpDate = PolicyModel.EffectiveDate.AddMonths(PolicyModel.PolicyTermMonths);
                PolicyEndorsement.Ins1First = PolicyModel.Drivers.FirstOrDefault()?.FirstName.Upper(); //LF 6/17/19 LastName;
                PolicyEndorsement.Ins1Last = PolicyModel.Drivers.FirstOrDefault()?.LastName.Upper();
                PolicyEndorsement.Ins1MI = PolicyModel.MiddleInitial1.Upper();
                PolicyEndorsement.Ins1FullName = string.Format("{0} {1}", PolicyModel.FirstName1, PolicyModel.LastName1).Upper();
                PolicyEndorsement.Ins1Suffix = PolicyModel.Suffix1.Upper();
                PolicyEndorsement.Ins2First = PolicyModel.FirstName2.Upper();
                PolicyEndorsement.Ins2Last = PolicyModel.LastName2.Upper();
                PolicyEndorsement.Ins2MI = PolicyModel.MiddleInitial2.Upper();
                PolicyEndorsement.Ins2Suffix = PolicyModel.Suffix2.Upper();
                PolicyEndorsement.Ins2FullName = string.IsNullOrEmpty(PolicyModel.FirstName2) && string.IsNullOrEmpty(PolicyModel.LastName2) ? null : string.Format("{0} {1}", PolicyModel.FirstName2, PolicyModel.LastName2).Upper();
                PolicyEndorsement.SixMonth = (PolicyModel.PolicyTermMonths == 6);
                PolicyEndorsement.isAnnual = (PolicyModel.PolicyTermMonths == 12);

                if (PolicyModel.SameAsGarage)
                {
                    PolicyEndorsement.MailStreet = PolicyModel.SameAsGarage ? PolicyModel.GarageStreetAddress.Upper() : PolicyModel.MailingStreetAddress.Upper();
                    PolicyEndorsement.MailCity = PolicyModel.SameAsGarage ? PolicyModel.GarageCity.Upper() : PolicyModel.MailingCity.Upper();
                    PolicyEndorsement.MailState = PolicyModel.SameAsGarage ? PolicyModel.GarageState.Upper() : PolicyModel.MailingState.Upper();
                    PolicyEndorsement.MailZip = PolicyModel.SameAsGarage ? PolicyModel.GarageZIPCode.Upper() : PolicyModel.MailingZIPCode.Upper();
                }
                else
                {
                    PolicyEndorsement.MailStreet = string.IsNullOrEmpty(PolicyModel.MailingStreetAddress) ? PolicyModel.MailingStreetAddress.Upper() : PolicyModel.MailingStreetAddress.Upper();
                    PolicyEndorsement.MailCity = string.IsNullOrEmpty(PolicyModel.MailingCity) ? PolicyModel.GarageCity.Upper() : PolicyModel.MailingCity.Upper();
                    PolicyEndorsement.MailState = string.IsNullOrEmpty(PolicyModel.MailingState) ? PolicyModel.GarageState.Upper() : PolicyModel.MailingState.Upper();
                    PolicyEndorsement.MailZip = string.IsNullOrEmpty(PolicyModel.MailingZIPCode) ? PolicyModel.GarageZIPCode.Upper() : PolicyModel.MailingZIPCode.Upper();
                    PolicyEndorsement.MailGarageSame = true;
                }

                PolicyEndorsement.GarageStreet = PolicyModel.GarageStreetAddress.Upper();
                PolicyEndorsement.GarageCity = PolicyModel.GarageCity.Upper();
                PolicyEndorsement.GarageState = PolicyModel.GarageState.Upper();
                PolicyEndorsement.GarageZip = PolicyModel.GarageZIPCode.Upper();
                PolicyEndorsement.GarageCounty = PolicyModel.GarageCounty.Upper();
                PolicyEndorsement.HomePhone = PolicyModel.HomePhone.Upper();
                PolicyEndorsement.WorkPhone = PolicyModel.WorkPhone.Upper();
                PolicyEndorsement.MainEmail = PolicyModel.EmailAddress.ToLower();
                PolicyEndorsement.PIPDed = PolicyModel.PipDeductible.Upper();
                PolicyEndorsement.NIO = (PolicyModel.WorkLossGroup == 1);
                PolicyEndorsement.NIRR = (PolicyModel.WorkLossGroup == 2); //  9/28/18 || (PolicyModel.WorkLossGroup == 3);

                if (PolicyModel.Workloss.HasValue)
                {
                    PolicyEndorsement.WorkLoss = PolicyModel.Workloss.Value == true ? 1 : 0;
                }

                PolicyEndorsement.BILimit = PolicyModel.BiLimit.Upper();
                PolicyEndorsement.PDLimit = PolicyModel.PdLimit.Upper();
                PolicyEndorsement.UMLimit = PolicyModel.UmLimit.Upper();
                PolicyEndorsement.MedPayLimit = PolicyModel.MedPayLimit == null ? PolicyModel.MedPayLimit : PolicyModel.MedPayLimit.Upper().Replace(",", "");
                PolicyEndorsement.Homeowner = PolicyModel.HasHomeownersDisc;
                PolicyEndorsement.TransDisc = (int)PolicyModel.TransDisc;
                PolicyEndorsement.Paperless = PolicyModel.Paperless;
                PolicyEndorsement.AdvancedQuote = PolicyModel.AdvancedQuote;
                PolicyEndorsement.HasPPO = PolicyModel.HasPPO;
                PolicyEndorsement.PreviousCompany = PolicyModel.PriorCompany.Upper();
                PolicyEndorsement.PreviousBI = PolicyModel.PreviousBI;
                PolicyEndorsement.DirectRepairDisc = PolicyModel.DirectRepairDiscount;
                PolicyEndorsement.ADNDLimit = PolicyModel.AdndLimit.Parse<int>();
                PolicyEndorsement.PayPlan = PolicyModel.PayPlan;
                PolicyEndorsement.PreviousExpDate = PolicyModel.PreviousExpDate == null ? "1/1/0001".Parse<DateTime>() : (DateTime)PolicyModel.PreviousExpDate;
                PolicyEndorsement.AgentCode = PolicyModel.AgentCode.Upper();
                PolicyEndorsement.MailGarageSame = PolicyModel.SameAsGarage;
                PolicyEndorsement.UMStacked = PolicyModel.UMstacked;
                //PolicyEndorsement.IsShow = PolicyModel.IsShow;
                PolicyEndorsement.HasLOU = PolicyModel.HasLou;
                PolicyEndorsement.HasBI = PolicyModel.BiLimit.Upper() != "NONE" ? true : false;
                PolicyEndorsement.HasUM = PolicyModel.UmLimit.Upper() != "NONE" ? true : false;
                PolicyEndorsement.HasMP = PolicyModel.MedPayLimit.Upper() != "0" ? true : false;
                PolicyEndorsement.HasADND = PolicyModel.AdndLimit.Upper() != "0" ? true : false;
                PolicyEndorsement.PIPLimit = "10000";
                PolicyEndorsement.HasPD = true;
                PolicyEndorsement.Cars = getCars.Count();
                PolicyEndorsement.Drivers = getDrivers.Count();
                PolicyEndorsement.HasLapse110 = has110Lapse;
                PolicyEndorsement.HasLapse1131 = has1131Lapse;
                PolicyEndorsement.HasPriorCoverage = isUndoPriorCov ? false : PolicyModel.PriorCoverage;
                //                PolicyEndorsement.PriorBILimit = PolicyModel.PriorBiLimit;


            }
        }

        public static void MapModelToXpo(EndorsementVehicleModel Cars, PolicyEndorseQuoteCars SaveCars)
        {
            SaveCars.PolicyNo = Sessions.Instance.PolicyNo;
            SaveCars.CarIndex = Cars.CarIndex;
            SaveCars.VINIndex = Cars.VinIndexNo;
            SaveCars.VIN = Cars.VIN;
            SaveCars.VehYear = Cars.Year;
            SaveCars.VehMake = Cars.Make;
            SaveCars.VehModel1 = Cars.VehModel;
            SaveCars.VehModel2 = Cars.Style;
            SaveCars.BusinessUse = Cars.IsBusinessUse;
            SaveCars.CompDed = Cars.ComprehensiveDeductible;
            SaveCars.CollDed = Cars.CollisionDeductible;
            SaveCars.HasColl = Cars.HasPhyDam;
            SaveCars.HasComp = Cars.HasPhyDam;
            SaveCars.HasPhyDam = Cars.HasPhyDam;
            SaveCars.ABS = Convert.ToInt32(Cars.HasAntiLockBrakes);
            SaveCars.ARB = Convert.ToInt32(Cars.ARB);
            SaveCars.ATD = Cars.ATD;
            SaveCars.AmountCustom = Cars.AmountCustom == null ? 0.0 : (double)Cars.AmountCustom;
            SaveCars.Artisan = Cars.Artisan;
            SaveCars.MilesToWork = Cars.MilesToWork == null ? 0 : (int)Cars.MilesToWork;
            SaveCars.COMPSymbol = Cars.CompSymbol;
            SaveCars.COLLSymbol = Cars.CollSymbol;
            SaveCars.BISymbol = Cars.BISymbol;
            SaveCars.PIPSymbol = Cars.PIPSymbol;
            SaveCars.PDSymbol = Cars.PDSymbol;
            SaveCars.MPSymbol = Cars.MPSymbol;
            SaveCars.ConvTT = Cars.HasConvOrTT;
            SaveCars.PurchNew = Cars.IsPurchasedNew;
            SaveCars.GrossVehicleWeight = Cars.GrossVehicleWeight;
            SaveCars.ClassCode = Cars.ClassCode;
            double costNewAmt;
            if (double.TryParse(Cars.CostNew, out costNewAmt))
            {
                SaveCars.CostNew = costNewAmt;
            }
        }

        public static void MapModelToXpo(EndorsementDriverModel Driver, PolicyEndorseQuoteDrivers SaveDrivers)
        {
            SaveDrivers.PolicyNo = Sessions.Instance.PolicyNo;
            SaveDrivers.FirstName = Driver.FirstName.Upper();
            SaveDrivers.LastName = Driver.LastName.Upper();
            SaveDrivers.MI = Driver.MiddleInitial.Upper();
            SaveDrivers.Suffix = Driver.Suffix.Upper();
            SaveDrivers.Class = Driver.Class;
            SaveDrivers.Unverifiable = Driver.Unverifiable;
            SaveDrivers.Gender = Driver.Gender.ToString().Upper();
            SaveDrivers.Married = Driver.IsMarried;
            SaveDrivers.DOB = Driver.DateOfBirth.Parse<DateTime>();
            SaveDrivers.LicenseNo = Driver.DriversLicense.Upper();
            SaveDrivers.LicenseSt = Driver.LicenseState.Upper();
            SaveDrivers.InternationalLic = Driver.IsInternationalLicense;
            SaveDrivers.Exclude = Driver.IsExcluded;
            SaveDrivers.RelationToInsured = Driver.Relation.ToString().Upper();
            SaveDrivers.Occupation = Driver.Occupation.Upper();
            SaveDrivers.Company = Driver.EmployerName.Upper();
            SaveDrivers.DateLicensed = Driver.DateLicensed == string.Empty ? "1/1/1900".Parse<DateTime>() : Driver.DateLicensed.Parse<DateTime>();
            SaveDrivers.SeniorDefDrvDisc = Driver.HasSeniorDefDrvDisc;
            SaveDrivers.SeniorDefDrvDate = Driver.SeniorDefDrvDate == null ? "1/1/1900".Parse<DateTime>() : (DateTime)Driver.SeniorDefDrvDate;
            SaveDrivers.SR22 = Driver.HasSR22;
            SaveDrivers.SR22CaseNo = Driver.SR22CaseNo;
            SaveDrivers.Exclude = Driver.IsExcluded;
        }

        #endregion

        #region XPO to model mappers

        public static QuoteModel MapXpoToModel(XpoPolicyQuote Quote)
        {
            QuoteModel SaveQuote = new QuoteModel();

            SaveQuote.QuoteId = Quote.PolicyNo;
            SaveQuote.EffectiveDate = Quote.EffDate;
            SaveQuote.ExpirationDate = Quote.ExpDate;
            SaveQuote.FirstName1 = Quote.Ins1First.Upper();
            SaveQuote.LastName1 = Quote.Ins1Last.Upper();
            SaveQuote.MiddleInitial1 = Quote.Ins1MI.Upper();
            SaveQuote.Suffix1 = Quote.Ins1Suffix.Upper();
            SaveQuote.FirstName2 = Quote.Ins2First.Upper();
            SaveQuote.LastName2 = Quote.Ins2Last.Upper();
            SaveQuote.MiddleInitial2 = Quote.Ins2MI.Upper();
            SaveQuote.PriorPolicyTerm = Quote.PriorPolicyTerm;
            SaveQuote.IsCurrentCustomer = Quote.IsCurrentCustomer;
            SaveQuote.Suffix2 = Quote.Ins2Suffix.Upper();
            string isannual = Quote.isAnnual.ToString();
            SaveQuote.PolicyTermMonths = isannual == "True" ? 12 : 6;
            SaveQuote.MailingStreetAddress = Quote.MailStreet.Upper();
            SaveQuote.MailingCity = Quote.MailCity.Upper();
            SaveQuote.MailingState = Quote.MailState.Upper();
            SaveQuote.MailingZIPCode = Quote.MailZip.Upper() == null ? Quote.MailZip.Upper() : Quote.MailZip.Upper().Length > 5 ? Quote.MailZip.Upper().Substring(0, 5) : Quote.MailZip.Upper(); ;
            SaveQuote.GarageStreetAddress = Quote.GarageStreet.Upper();
            SaveQuote.GarageCity = Quote.GarageCity.Upper();
            SaveQuote.GarageState = Quote.GarageState.Upper();
            SaveQuote.GarageZIPCode = Quote.GarageZip.Upper() == null ? Quote.GarageZip.Upper() : Quote.GarageZip.Upper().Length > 5 ? Quote.GarageZip.Upper().Substring(0, 5) : Quote.GarageZip.Upper();
            SaveQuote.GarageCounty = Quote.GarageCounty.Upper();
            SaveQuote.HomePhone = Quote.HomePhone.Upper();
            SaveQuote.WorkPhone = Quote.WorkPhone.Upper();
            SaveQuote.EmailAddress = Quote.MainEmail.ToLower();
            SaveQuote.PipDeductible = Quote.PIPDed.Upper();
            SaveQuote.NIO = Quote.NIO;
            SaveQuote.NIRR = Quote.NIRR;
            SaveQuote.WorkLossGroup = SaveQuote.NIO ? 1 : SaveQuote.NIRR ? 2 : 2; // 3;
            SaveQuote.Workloss = Quote.WorkLoss == 1 ? true : false; 
            SaveQuote.BiLimit = Quote.BILimit.Upper();
            SaveQuote.PdLimit = Quote.PDLimit.Upper();
            SaveQuote.UmLimit = Quote.UMLimit.Upper();
            SaveQuote.MedPayLimit = Quote.MedPayLimit == null ? Quote.MedPayLimit : Quote.MedPayLimit.Upper().Replace(",", "");
            SaveQuote.HasHomeownersDisc = Quote.Homeowner;
            SaveQuote.TransDisc = Quote.TransDisc;
            SaveQuote.Paperless = Quote.Paperless;
            SaveQuote.AdvancedQuote = Quote.AdvancedQuote;
            SaveQuote.HasPPO = Quote.HasPPO;
            SaveQuote.PriorCompany = Quote.PreviousCompany.Upper();
            SaveQuote.PreviousBI = Quote.PreviousBI;
            SaveQuote.DirectRepairDiscount = Quote.DirectRepairDisc;
            SaveQuote.AdndLimit = Quote.ADNDLimit.ToString();
            SaveQuote.LapseNone = Quote.HasLapseNone;
            SaveQuote.Lapse110 = Quote.HasLapse110;
            SaveQuote.Lapse1131 = Quote.HasLapse1131;
            SaveQuote.PayPlan = Quote.PayPlan;
            SaveQuote.PreviousExpDate = Quote.PreviousExpDate;
            SaveQuote.PriorCoverage = Quote.HasPriorCoverage;
            SaveQuote.PriorPolicyNo = Quote.PriorPolicyNo.Upper();
            SaveQuote.SameAsGarage = Quote.MailGarageSame;
            SaveQuote.UMstacked = Quote.UMStacked;
            string raterTypeMsg = null;
            switch (Quote.FromRater)
            {
                case "Vertafore":
                    raterTypeMsg = "(Vertafore Bridge)";
                    break;
                case "ITC":
                    raterTypeMsg = "(ITC Bridge)";
                    break;
                case "Quote Rush":
                    raterTypeMsg = "(Quote Rush Bridge)";
                    break;
                case "EzLynx":
                    raterTypeMsg = "(EZ Lynx Bridge)";
                    break;
                default:
                    raterTypeMsg = "(Direct Quote)";
                    break;
            }
            SaveQuote.RaterType = raterTypeMsg;
            SaveQuote.QuotedDate = Quote.QuotedDate;
            SaveQuote.HasLou = Quote.HasLOU;
            SaveQuote.PriorBiLimit = Quote.PriorBILimit;
            SaveQuote.EstimatedCreditScore = Quote.EstimatedCreditScore;
            SaveQuote.CreditScore = Quote.CreditScore;
            SaveQuote.CreditGuid = Quote.CreditGuid;
            SaveQuote.CreditMsg = Quote.CreditMsg;
            SaveQuote.ReWrittenFromPolicyNo = Quote.ReWrittenFromPolicyNo;
            return SaveQuote;

        }

        public static EndorsementPolicyModel MapXpoToModel(PolicyEndorseQuote endorsement)
        {
            EndorsementPolicyModel SaveQuote = new EndorsementPolicyModel();
            
            SaveQuote.EndorsementID = endorsement.PolicyNo;
            SaveQuote.PolicyNo = endorsement.PolicyNo;
            SaveQuote.EffectiveDate = endorsement.EffDate;
            SaveQuote.ExpirationDate = endorsement.ExpDate;
            SaveQuote.FirstName1 = endorsement.Ins1First.Upper();
            SaveQuote.LastName1 = endorsement.Ins1Last.Upper();
            SaveQuote.MiddleInitial1 = endorsement.Ins1MI.Upper();
            SaveQuote.Suffix1 = endorsement.Ins1Suffix.Upper();
            SaveQuote.FirstName2 = endorsement.Ins2First.Upper();
            SaveQuote.LastName2 = endorsement.Ins2Last.Upper();
            SaveQuote.MiddleInitial2 = endorsement.Ins2MI.Upper();
            SaveQuote.Suffix2 = endorsement.Ins2Suffix.Upper();
            SaveQuote.SR22Fee = endorsement.SR22Fee;
            SaveQuote.DBSetupFee = endorsement.DBSetupFee;
            SaveQuote.MVRFee = endorsement.MVRFee;
            SaveQuote.PolicyFee = endorsement.PolicyFee;
            SaveQuote.FHCFFee = endorsement.FHCFFee;
            SaveQuote.MaintenanceFee = endorsement.MaintenanceFee;
            SaveQuote.PIPPDOnlyFee = endorsement.PIPPDOnlyFee;
            SaveQuote.PolicyWritten = endorsement.PolicyWritten;
            SaveQuote.SixMonth = endorsement.SixMonth;
            string isannual = endorsement.isAnnual.ToString();
            SaveQuote.PolicyTermMonths = isannual == "True" ? 12 : 6;
            SaveQuote.MailingStreetAddress = endorsement.MailStreet.Upper();
            SaveQuote.MailingCity = endorsement.MailCity.Upper();
            SaveQuote.MailingState = endorsement.MailState.Upper();
            SaveQuote.MailingZIPCode = endorsement.MailZip.Upper() == null ? endorsement.MailZip.Upper() : endorsement.MailZip.Upper().Length > 5 ? endorsement.MailZip.Upper().Substring(0, 5) : endorsement.MailZip.Upper(); ;
            SaveQuote.GarageStreetAddress = endorsement.GarageStreet.Upper();
            SaveQuote.GarageCity = endorsement.GarageCity.Upper();
            SaveQuote.GarageState = endorsement.GarageState.Upper();
            SaveQuote.GarageZIPCode = endorsement.GarageZip.Upper() == null ? endorsement.GarageZip.Upper() : endorsement.GarageZip.Upper().Length > 5 ? endorsement.GarageZip.Upper().Substring(0, 5) : endorsement.GarageZip.Upper();
            SaveQuote.GarageCounty = endorsement.GarageCounty.Upper();
            SaveQuote.HomePhone = endorsement.HomePhone.Upper();
            SaveQuote.WorkPhone = endorsement.WorkPhone.Upper();
            SaveQuote.EmailAddress = endorsement.MainEmail.ToLower();
            SaveQuote.PipDeductible = endorsement.PIPDed.Upper();
            SaveQuote.NIO = endorsement.NIO;
            SaveQuote.NIRR = endorsement.NIRR;

            SaveQuote.WorkLossGroup = 2; // 3;
            if (SaveQuote.NIO.HasValue)
                if (SaveQuote.NIO.Value == true)
                    SaveQuote.WorkLossGroup = 1;
            if (SaveQuote.NIRR.HasValue)
                if (SaveQuote.NIRR.Value == true)
                    SaveQuote.WorkLossGroup = 2;
           // SaveQuote.WorkLossGroup = SaveQuote.NIO ? 1 : SaveQuote.NIRR ? 2 : 3;  //LF 11/12/18  This line was previo


            SaveQuote.Workloss = endorsement.WorkLoss == 1 ? true : false;
            SaveQuote.BiLimit = endorsement.BILimit.Upper();
            SaveQuote.PdLimit = endorsement.PDLimit.Upper();
            SaveQuote.UmLimit = endorsement.UMLimit.Upper();
            SaveQuote.MedPayLimit = endorsement.MedPayLimit == null ? endorsement.MedPayLimit : endorsement.MedPayLimit.Upper().Replace(",", "");
            SaveQuote.HasHomeownersDisc = endorsement.Homeowner;
            SaveQuote.TransDisc = endorsement.TransDisc;
            SaveQuote.Paperless = endorsement.Paperless;
            SaveQuote.AdvancedQuote = endorsement.AdvancedQuote;
            SaveQuote.HasPPO = endorsement.HasPPO;
            SaveQuote.PriorCompany = endorsement.PreviousCompany.Upper();
            SaveQuote.PreviousBI = endorsement.PreviousBI;
            SaveQuote.DirectRepairDiscount = endorsement.DirectRepairDisc;
            SaveQuote.AdndLimit = endorsement.ADNDLimit.ToString();
            SaveQuote.LapseNone = endorsement.HasLapseNone;
            SaveQuote.Lapse110 = endorsement.HasLapse110;
            SaveQuote.Lapse1131 = endorsement.HasLapse1131;
            SaveQuote.PayPlan = endorsement.PayPlan;
            SaveQuote.PreviousExpDate = endorsement.PreviousExpDate;
            SaveQuote.PriorCoverage = endorsement.HasPriorCoverage;
            SaveQuote.PriorPolicyNo = endorsement.PriorPolicyNo.Upper();
            SaveQuote.SameAsGarage = endorsement.MailGarageSame;
            SaveQuote.UMstacked = endorsement.UMStacked;
            SaveQuote.CreditScore = endorsement.CreditScore;
            SaveQuote.EstimatedCreditScore = endorsement.EstimatedCreditScore;
            string raterTypeMsg = null;
            switch (endorsement.FromRater.Upper())
            {
                case "AA":
                    raterTypeMsg = "(AccuAuto Bridge)";
                    break;
                case "QQ":
                    raterTypeMsg = "(QQ Bridge)";
                    break;
                default:
                    raterTypeMsg = "(Direct Quote)";
                    break;
            }
            SaveQuote.RaterType = raterTypeMsg;
            SaveQuote.HasLou = endorsement.HasLOU;
            return SaveQuote;

        }

        /// <summary>
        /// Used For Endorsements only
        /// </summary>
        /// <param name="lienHolder"></param>
        /// <returns></returns>
        public static EndorsementLienHolderModel MapLienHolderXpoToModel(PolicyCarLienHolders lienHolder)
        {
            var model = new EndorsementLienHolderModel();
            model.ID = lienHolder.ID;
            model.CarIndex = lienHolder.CarIndex;
            model.City = lienHolder.City;
            model.LienHolderIndex = lienHolder.LienHolderIndex;
            model.Name = lienHolder.Name;
            model.Phone = lienHolder.Phone;
            model.PolicyNo = lienHolder.PolicyNo;
            model.State = lienHolder.State;
            model.StreetAddress = lienHolder.Address;
            model.isAddInterest = lienHolder.isAddInterest;
            model.VIN = lienHolder.VIN;
            model.ZIP = lienHolder.Zip;
            model.IsNonInstitutional = lienHolder.IsNonInstitutional;
            return model;
        }


        public static LienHolderModel MapXpoToModel(PolicyCarLienHolders lienHolder)
        {
            var model = new LienHolderModel();
            model.ID = lienHolder.ID;
            model.CarIndex = lienHolder.CarIndex;
            model.City = lienHolder.City;
            model.LienHolderIndex = lienHolder.LienHolderIndex;
            model.Name = lienHolder.Name;
            model.Phone = lienHolder.Phone;
            model.PolicyNo = lienHolder.PolicyNo;
            model.State = lienHolder.State;
            model.StreetAddress = lienHolder.Address;
            model.isAddInterest = lienHolder.isAddInterest;
            model.VIN = lienHolder.VIN;
            model.ZIP = lienHolder.Zip;
            model.IsNonInstitutional = lienHolder.IsNonInstitutional;
            return model;
        }

        /// <summary>
        /// Used For Endorsements only
        /// </summary>
        /// <param name="lienHolder"></param>
        /// <returns></returns>
        public static EndorsementLienHolderModel MapLienHolderXpoToModel(PolicyEndorseQuoteCarLienHolders lienHolder)
        {
            var model = new EndorsementLienHolderModel();
            model.ID = lienHolder.ID;
            model.CarIndex = lienHolder.CarIndex;
            model.City = lienHolder.City;
            model.LienHolderIndex = lienHolder.LienHolderIndex;
            model.Name = lienHolder.Name;
            model.Phone = lienHolder.Phone;
            model.PolicyNo = lienHolder.PolicyNo;
            model.State = lienHolder.State;
            model.StreetAddress = lienHolder.Address;
            model.isAddInterest = lienHolder.isAddInterest;
            model.VIN = lienHolder.VIN;
            model.ZIP = lienHolder.Zip;
            model.IsNonInstitutional = lienHolder.IsNonInstitutional;
            return model;
        }


        public static LienHolderModel MapXpoToModel(PolicyEndorseQuoteCarLienHolders lienHolder)
        {
            var model = new LienHolderModel();
            model.ID = lienHolder.ID;
            model.CarIndex = lienHolder.CarIndex;
            model.City = lienHolder.City;
            model.LienHolderIndex = lienHolder.LienHolderIndex;
            model.Name = lienHolder.Name;
            model.Phone = lienHolder.Phone;
            model.PolicyNo = lienHolder.PolicyNo;
            model.State = lienHolder.State;
            model.StreetAddress = lienHolder.Address;
            model.isAddInterest = lienHolder.isAddInterest;
            model.VIN = lienHolder.VIN;
            model.ZIP = lienHolder.Zip;
            model.IsNonInstitutional = lienHolder.IsNonInstitutional;
            return model;
        }


        public static EndorsementVehicleModel MapXpoToModel(PolicyCars GetCars)
        {
            EndorsementVehicleModel CarsModel = new EndorsementVehicleModel();

            CarsModel.VinIndexNo = GetCars.VINIndex;
            CarsModel.PolicyID = GetCars.PolicyNo;
            CarsModel.VIN = GetCars.VIN;
            CarsModel.Year = GetCars.VehYear;
            CarsModel.Make = GetCars.VehMake;
            CarsModel.VehModel = GetCars.VehModel1;
            CarsModel.IsBusinessUse = GetCars.BusinessUse;
            CarsModel.ComprehensiveDeductible = GetCars.CompDed;
            CarsModel.CollisionDeductible = GetCars.CollDed;
            CarsModel.HasPhyDam = GetCars.HasPhyDam;
            CarsModel.HasAntiLockBrakes = (GetCars.ABS == 1);
            CarsModel.HasAirbag = !string.IsNullOrEmpty(GetCars.ARB.ToString());
            CarsModel.HasAntiTheftDevice = !string.IsNullOrEmpty(GetCars.ATD);
            CarsModel.AmountCustom = GetCars.AmountCustom;
            CarsModel.OID = GetCars.OID;
            CarsModel.Artisan = GetCars.Artisan;
            CarsModel.MilesToWork = GetCars.MilesToWork;
            CarsModel.CarIndex = GetCars.CarIndex;
            CarsModel.VehicleId = GetCars.OID.ToString();
            CarsModel.IsEdit = true;
            CarsModel.MoreThan10MilesToWk = CarsModel.MilesToWork > 10 ? true : false;
            CarsModel.Style = GetCars.VehModel2;
            CarsModel.BISymbol = GetCars.BISymbol;
            CarsModel.PDSymbol = GetCars.PDSymbol;
            CarsModel.PIPSymbol = GetCars.PIPSymbol;
            CarsModel.CollSymbol = GetCars.COLLSymbol;
            CarsModel.CompSymbol = GetCars.COMPSymbol;

            Session SessionObj = new Session();
            XPCollection<PolicyCarLienHolders> getLienHolders = new XPCollection<PolicyCarLienHolders>(SessionObj, CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?",
                                                                                                                                       Sessions.Instance.PolicyNo,
                                                                                                                                       GetCars.CarIndex));

            CarsModel.LienHolders = new List<EndorsementLienHolderModel>();

            foreach (PolicyCarLienHolders lien in getLienHolders)
            {
                EndorsementLienHolderModel LienHolder = new EndorsementLienHolderModel();
                LienHolder.CarIndex = lien.CarIndex;
                LienHolder.LienHolderIndex = lien.ID;
                LienHolder.PolicyNo = lien.PolicyNo;
                LienHolder.Name = lien.Name;
                LienHolder.StreetAddress = lien.Address;
                LienHolder.City = lien.City;
                LienHolder.State = lien.State;
                LienHolder.ZIP = lien.Zip;
                LienHolder.Phone = lien.Phone;
                LienHolder.isAddInterest = lien.isAddInterest;
                LienHolder.IsNonInstitutional = lien.IsNonInstitutional;
                CarsModel.LienHolders.Add(LienHolder);
            }

            return CarsModel;
        }
        public static EndorsementDriverModel MapXpoToModel(PolicyDrivers GetDrivers)
        {
            EndorsementDriverModel Driver = new EndorsementDriverModel();

            Driver.DriverIndex = GetDrivers.DriverIndex;
            Driver.PolicyID = GetDrivers.PolicyNo;
            Driver.FirstName = GetDrivers.FirstName.Upper();
            Driver.LastName = GetDrivers.LastName.Upper();
            Driver.MiddleInitial = GetDrivers.MI.Upper();
            Driver.Suffix = GetDrivers.Suffix.Upper();
            Driver.Class = GetDrivers.Class;
            string gender = GetDrivers.Gender.Upper().Trim();
            Driver.Gender = gender;
            Driver.IsMarried = GetDrivers.Married;
            Driver.DateOfBirth = GetDrivers.DOB.ToShortDateString();
            Driver.DriversLicense = GetDrivers.LicenseNo.Upper();
            Driver.Unverifiable = GetDrivers.Unverifiable;
            Driver.LicenseState = GetDrivers.LicenseSt.Upper();
            Driver.IsInternationalLicense = GetDrivers.InternationalLic;
            Driver.IsExcluded = GetDrivers.Exclude;
            string relationship = GetDrivers.RelationToInsured;
            if (relationship.Length <= 2)
            {
                switch (relationship)
                {
                    case "IN":
                        relationship = "Insured";
                        break;
                    case "BS":
                        relationship = "BROTHER/SISTER";
                        break;
                    case "CH":
                        relationship = "Child";
                        break;
                    case "PA":
                        relationship = "Parent";
                        break;
                    case "SP":
                        relationship = "Spouse";
                        break;
                    case "RE":
                        relationship = "Other";
                        break;
                    case "OT":
                        relationship = "NonRelative";
                        break;
                }
            }
            Driver.Relation = relationship.Upper();
            Driver.JobTitle = GetDrivers.Occupation.Upper();
            Driver.Occupation = GetDrivers.Occupation.Upper();
            Driver.EmployerName = GetDrivers.Company.Upper();
            Driver.DateLicensed = GetDrivers.DateLicensed.ToShortDateString();
            Driver.HasSeniorDefDrvDisc = GetDrivers.SeniorDefDrvDisc;
            Driver.SeniorDefDrvDate = GetDrivers.SeniorDefDrvDate;
            Driver.HasSR22 = GetDrivers.SR22;
            Driver.SR22CaseNo = GetDrivers.SR22CaseNo.Upper();
            Driver.DriverId = GetDrivers.OID.ToString();
            Driver.PolicyID = GetDrivers.PolicyNo;

            return Driver;
        }

        public static VehicleModel MapXpoToModel(PolicyQuoteCars GetCars)
        {
            VehicleModel CarsModel = new VehicleModel();

            CarsModel.CarIndex = GetCars.CarIndex;
            CarsModel.VehicleId = GetCars.OID.ToString();   //LF 10/2/18 added this line and line above - need to see why Model.VehicleId is a string?
            CarsModel.VinIndexNo = GetCars.VINIndex;
            CarsModel.QuoteId = GetCars.PolicyNo;
            CarsModel.VIN = GetCars.VIN;
            CarsModel.Year = GetCars.VehYear;
            CarsModel.Make = GetCars.VehMake;
            CarsModel.VehModel = GetCars.VehModel1;
            CarsModel.IsBusinessUse = GetCars.BusinessUse;
            CarsModel.ComprehensiveDeductible = GetCars.CompDed;
            CarsModel.IsPurchasedNew = GetCars.PurchNew;
            CarsModel.DatePurchased = GetCars.DatePurchased.GetValueOrDefault().ToShortDateString();
            CarsModel.CollisionDeductible = GetCars.CollDed;
            CarsModel.HasPhyDam = GetCars.HasPhyDam;
            CarsModel.HasAntiLockBrakes = (GetCars.ABS == 1);
            CarsModel.HasAirbag = !string.IsNullOrEmpty(GetCars.ARB.ToString());
            CarsModel.HasAntiTheftDevice = !string.IsNullOrEmpty(GetCars.ATD);
            CarsModel.ATD = GetCars.ATD;
            CarsModel.ARB = GetCars.ARB.ToString();
            CarsModel.AmountCustom = GetCars.AmountCustom;
            CarsModel.Artisan = GetCars.Artisan;
            CarsModel.MilesToWork = GetCars.MilesToWork;
            CarsModel.CarIndex = GetCars.CarIndex;
            CarsModel.VehicleId = GetCars.OID.ToString();
            CarsModel.IsEdit = true;
            CarsModel.MoreThan10MilesToWk = CarsModel.MilesToWork > 10 ? true : false;
            CarsModel.Style = GetCars.VehModel2;
            CarsModel.CollSymbol = GetCars.COLLSymbol;
            CarsModel.EngineType = GetCars.EngineType;
            CarsModel.CompSymbol = GetCars.COMPSymbol;
            CarsModel.GrossVehicleWeight = GetCars.GrossVehicleWeight;
            CarsModel.ClassCode = GetCars.ClassCode;
            CarsModel.BISymbol = GetCars.BISymbol;
            CarsModel.PDSymbol = GetCars.PDSymbol;
            CarsModel.MPSymbol = GetCars.MPSymbol;
            CarsModel.PIPSymbol = GetCars.PIPSymbol;

            Session SessionObj = new Session();
            XPCollection<PolicyCarLienHolders> getLienHolders = new XPCollection<PolicyCarLienHolders>(SessionObj, CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?",
                                                                                                                                       Sessions.Instance.PolicyNo,
                                                                                                                                       GetCars.CarIndex));

            CarsModel.LienHolders = new List<LienHolderModel>();

            foreach (PolicyCarLienHolders lien in getLienHolders)
            {
                LienHolderModel LienHolder = new LienHolderModel();
                LienHolder.CarIndex = lien.CarIndex;
                LienHolder.LienHolderIndex = lien.ID;
                LienHolder.PolicyNo = lien.PolicyNo;
                LienHolder.Name = lien.Name;
                LienHolder.StreetAddress = lien.Address;
                LienHolder.ID = lien.ID;
                LienHolder.City = lien.City;
                LienHolder.State = lien.State;
                LienHolder.ZIP = lien.Zip;
                LienHolder.Phone = lien.Phone;
                LienHolder.isAddInterest = lien.isAddInterest;
                LienHolder.IsNonInstitutional = lien.IsNonInstitutional;
                CarsModel.LienHolders.Add(LienHolder);
            }

            return CarsModel;
        }

        public static DriverModel MapXpoToModel(PolicyQuoteDrivers GetDrivers)
        {
            DriverModel Driver = new DriverModel();

            Driver.DriverIndex = GetDrivers.DriverIndex;
            Driver.QuoteId = GetDrivers.PolicyNo;
            Driver.FirstName = GetDrivers.FirstName.Upper();
            Driver.LastName = GetDrivers.LastName.Upper();
            Driver.HasMvr = GetDrivers.HasMvr;
            Driver.MvrDate = GetDrivers.MvrDate;
            Driver.LicenseStatus = GetDrivers.LicenseStatus;
            Driver.LicenseExpiration = GetDrivers.LicenseExpiration;
            Driver.Unverifiable = GetDrivers.Unverifiable;
            Driver.VeriskMvrOrderNumber = GetDrivers.VeriskMvrOrderNumber;
            Driver.MiddleInitial = GetDrivers.MI.Upper();
            Driver.Suffix = GetDrivers.Suffix.Upper();
            Driver.Class = GetDrivers.Class;
            string gender = GetDrivers.Gender?.Upper()?.Trim();
            Driver.Gender = gender;
            //Driver.Gender = (GenderOption)Enum.Parse(typeof(GenderOption), gender);
            Driver.IsMarried = GetDrivers.Married;
            Driver.DateOfBirth = GetDrivers.DOB.ToShortDateString();
            Driver.DriversLicense = GetDrivers.LicenseNo?.Upper();
            Driver.LicenseState = GetDrivers.LicenseSt?.Upper();
            Driver.IsInternationalLicense = GetDrivers.InternationalLic;
            Driver.IsExcluded = GetDrivers.Exclude;

            string relationship = GetDrivers.RelationToInsured;
            if (relationship.Length <= 2)
            {
                switch (relationship)
                {
                    case "IN":
                        relationship = "Insured";
                        break;
                    case "BS":
                        relationship = "BROTHER/SISTER";
                        break;
                    case "CH":
                        relationship = "Child";
                        break;
                    case "PA":
                        relationship = "Parent";
                        break;
                    case "SP":
                        relationship = "Spouse";
                        break;
                    case "RE":
                        relationship = "Other";
                        break;
                    case "OT":
                        relationship = "NonRelative";
                        break;
                }
            }
            //Driver.Relation             = (RelationType)Enum.Parse(typeof(RelationType), relationship.Upper());
            Driver.Relation = relationship?.Upper();
            Driver.JobTitle = GetDrivers.Occupation?.Upper();
            Driver.Occupation = GetDrivers.Occupation?.Upper();
            Driver.EmployerName = GetDrivers.Company?.Upper();
            Driver.DateLicensed = GetDrivers.DateLicensed.ToShortDateString();
            Driver.HasSeniorDefDrvDisc = GetDrivers.SeniorDefDrvDisc;
            Driver.SeniorDefDrvDate = GetDrivers.SeniorDefDrvDate;
            Driver.HasSR22 = GetDrivers.SR22;
            Driver.SR22CaseNo = GetDrivers.SR22CaseNo?.Upper();
            Driver.DriverId = GetDrivers.OID.ToString();
            Driver.QuoteId = GetDrivers.PolicyNo;
            Driver.DriverMsg = GetDrivers.DriverMsg;
            return Driver;
        }

        public static ViolationModel MapXpoToModel(PolicyQuoteDriversViolations violation)
        {
            ViolationModel model = new ViolationModel();

            model.DriverIndex = violation.DriverIndex;
            model.Description = violation.ViolationDesc;
            model.Date = violation.ViolationDate;
            model.DriverId = violation.DriverIndex.ToString();
            model.Points = violation.ViolationPoints;
            model.PolicyNo = Sessions.Instance.PolicyNo;
            model.FromMvr = violation.FromMVR;
            model.FromAPlus = violation.FromAPlus;
            model.FromCV = violation.FromCV;
            model.ViolationId = violation.OID;

            return model;
        }

        public static EndorsementVehicleModel MapXpoToModel(PolicyEndorseQuoteCars GetCars)
        {
            EndorsementVehicleModel CarsModel = new EndorsementVehicleModel();

            CarsModel.VinIndexNo = GetCars.VINIndex;
            CarsModel.PolicyID = GetCars.PolicyNo;
            CarsModel.VIN = GetCars.VIN;
            CarsModel.OID = GetCars.OID;
            CarsModel.Year = GetCars.VehYear;
            CarsModel.Make = GetCars.VehMake;
            CarsModel.VehModel = GetCars.VehModel1;
            CarsModel.IsBusinessUse = GetCars.BusinessUse;
            CarsModel.ComprehensiveDeductible = GetCars.CompDed;
            CarsModel.CollisionDeductible = GetCars.CollDed;
            CarsModel.HasPhyDam = GetCars.HasPhyDam;
            CarsModel.HasAntiLockBrakes = (GetCars.ABS == 1);
            CarsModel.HasAirbag = !string.IsNullOrEmpty(GetCars.ARB.ToString());
            CarsModel.ATD =  GetCars.ATD;
            CarsModel.ARB = GetCars.ARB.ToString();
            CarsModel.AmountCustom = GetCars.AmountCustom;
            CarsModel.Artisan = GetCars.Artisan;
            CarsModel.MilesToWork = GetCars.MilesToWork;
            CarsModel.CarIndex = GetCars.CarIndex;
            CarsModel.VehicleId = GetCars.OID.ToString();
            CarsModel.IsEdit = true;
            CarsModel.MoreThan10MilesToWk = CarsModel.MilesToWork > 10 ? true : false;
            CarsModel.Style = GetCars.VehModel2;
            CarsModel.BISymbol = GetCars.BISymbol;
            CarsModel.CompSymbol = GetCars.COMPSymbol;
            CarsModel.CollSymbol = GetCars.COLLSymbol;
            CarsModel.PDSymbol = GetCars.PDSymbol;
            CarsModel.PIPSymbol = GetCars.PIPSymbol;
            CarsModel.MPSymbol = GetCars.MPSymbol;
            CarsModel.HasConvOrTT = GetCars.ConvTT;
            CarsModel.IsPurchasedNew = GetCars.PurchNew;
            CarsModel.Artisan = GetCars.Artisan;
            Session SessionObj = new Session();
            XPCollection<PolicyEndorseQuoteCarLienHolders> getLienHolders = new XPCollection<PolicyEndorseQuoteCarLienHolders>(SessionObj, CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?",
                                                                                                                                       Sessions.Instance.PolicyNo,
                                                                                                                                       GetCars.CarIndex));

            CarsModel.LienHolders = new List<EndorsementLienHolderModel>();

            foreach (PolicyEndorseQuoteCarLienHolders lien in getLienHolders)
            {
                EndorsementLienHolderModel LienHolder = new EndorsementLienHolderModel();
                LienHolder.CarIndex = lien.CarIndex;
                LienHolder.LienHolderIndex = lien.ID;
                LienHolder.ID = lien.ID;
                LienHolder.PolicyNo = lien.PolicyNo;
                LienHolder.Name = lien.Name;
                LienHolder.StreetAddress = lien.Address;
                LienHolder.City = lien.City;
                LienHolder.State = lien.State;
                LienHolder.ZIP = lien.Zip;
                LienHolder.Phone = lien.Phone;
                LienHolder.isAddInterest = lien.isAddInterest;
                LienHolder.IsNonInstitutional = lien.IsNonInstitutional;
                CarsModel.LienHolders.Add(LienHolder);
            }

            return CarsModel;
        }

        public static EndorsementDriverModel MapXpoToModel(PolicyEndorseQuoteDrivers GetDrivers)
        {
            EndorsementDriverModel Driver = new EndorsementDriverModel();

            Driver.DriverIndex = GetDrivers.DriverIndex;
            Driver.PolicyID = GetDrivers.PolicyNo;
            Driver.FirstName = GetDrivers.FirstName.Upper();
            Driver.LastName = GetDrivers.LastName.Upper();
            Driver.MiddleInitial = GetDrivers.MI.Upper();
            Driver.Suffix = GetDrivers.Suffix.Upper();
            Driver.Class = GetDrivers.Class;
            Driver.Unverifiable = GetDrivers.Unverifiable;
            string gender = GetDrivers.Gender.Upper().Trim();
            Driver.Gender = gender;
            //Driver.Gender = (GenderOption)Enum.Parse(typeof(GenderOption), gender);
            Driver.IsMarried = GetDrivers.Married;
            Driver.DateOfBirth = GetDrivers.DOB.ToShortDateString();
            Driver.DriversLicense = GetDrivers.LicenseNo.Upper();
            Driver.LicenseState = GetDrivers.LicenseSt.Upper();
            Driver.IsInternationalLicense = GetDrivers.InternationalLic;
            Driver.IsExcluded = GetDrivers.Exclude;
            Driver.OID = GetDrivers.OID;

            string relationship = GetDrivers.RelationToInsured;
            if (relationship.Length <= 2)
            {
                switch (relationship)
                {
                    case "IN":
                        relationship = "Insured";
                        break;
                    case "BS":
                        relationship = "BROTHER/SISTER";
                        break;
                    case "CH":
                        relationship = "Child";
                        break;
                    case "PA":
                        relationship = "Parent";
                        break;
                    case "SP":
                        relationship = "Spouse";
                        break;
                    case "RE":
                        relationship = "Other";
                        break;
                    case "OT":
                        relationship = "NonRelative";
                        break;
                }
            }
            //Driver.Relation             = (RelationType)Enum.Parse(typeof(RelationType), relationship.Upper());
            Driver.Relation = relationship.Upper();
            Driver.JobTitle = GetDrivers.Occupation.Upper();
            Driver.Occupation = GetDrivers.Occupation.Upper();
            Driver.EmployerName = GetDrivers.Company.Upper();
            Driver.DateLicensed = GetDrivers.DateLicensed.ToShortDateString();
            Driver.HasSeniorDefDrvDisc = GetDrivers.SeniorDefDrvDisc;
            Driver.SeniorDefDrvDate = GetDrivers.SeniorDefDrvDate;
            Driver.HasSR22 = GetDrivers.SR22;
            Driver.SR22CaseNo = GetDrivers.SR22CaseNo.Upper();
            Driver.DriverId = GetDrivers.OID.ToString();
            Driver.PolicyID = GetDrivers.PolicyNo;

            return Driver;
        }

        public static EndorsementViolationModel MapXpoToModel(PolicyEndorseQuoteDriversViolations violation)
        {
            EndorsementViolationModel model = new EndorsementViolationModel();

            model.DriverIndex = violation.DriverIndex;
            model.Description = violation.ViolationDesc;
            model.Date = violation.ViolationDate;
            model.DriverId = violation.DriverIndex.ToString();
            model.Points = violation.ViolationPoints;
            model.PolicyNo = Sessions.Instance.PolicyNo;
            model.FromMvr = violation.FromMVR;
            model.FromAplus = violation.FromAPlus;
            model.FromCV = violation.FromCV;
            model.ViolationId = violation.OID;

            return model;
        }

        public static void SavePrevCompanyAndExpDateInQuote(string pPolicyNo,string pPrevCompany, DateTime pPrevExpDate)
        {
            if (string.IsNullOrEmpty(pPolicyNo) || string.IsNullOrEmpty(pPrevCompany))
                return;

            using (UnitOfWork uowork = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicyQuote quote =
                    new XPCollection<XpoPolicyQuote>(uowork,
                        CriteriaOperator.Parse("PolicyNo = ?", pPolicyNo)).FirstOrDefault();
                if (quote != null)
                {
                    quote.PreviousCompany = pPrevCompany;
                    quote.PreviousExpDate = pPrevExpDate;
                    quote.Save();
                    uowork.CommitChanges();
                }
            }

        }
        #endregion

    }
}