﻿using System;
using System.Reflection;
using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Support.DataObjects;
using DevExpress.Xpo;

namespace AgentPortal.Support.Utilities
{
    public class LogUtils
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static void Log(object obj, string prefix) 
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                log.DebugFormat("{0}{1}: {2}", prefix, property.Name, property.GetValue(obj, BindingFlags.Instance, null, null, null));
            }
        }
        ////////////////////////////////////////////////////////////////////////

        public static void Log(PaymentResponseModel payment)
        {
            Log(payment, "[DEBUG PAYMENT RESPONSE] payment.");
        }
        ////////////////////////////////////////////////////////////////////////
        
        public static void Log(PaymentSaveModels Payment)
        {
            Log(Payment, "[DEBUG ADD PAYMENT] payment.");
        }
        ////////////////////////////////////////////////////////////////////////

        public static void Log(string username, string ipAddress)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                PortalLogonActivity Activity = new PortalLogonActivity(uow);
                Activity.UserIp = ipAddress;
                Activity.Username = username;
                Activity.DateLoggedIn = DateTime.Now;

                try{ Activity.Save(); } catch (Exception) { }
            }
        }
        ////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// Portal activity logging. 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="desc"></param>
        /// <param name="type">Debug level for the message being logged. Use either DEBUG, INFO, WARN, FATAL.</param>
        public static void Log(string username, string desc, string type = "DEBUG")
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoActivityLog Log = new XpoActivityLog(uow);
                Log.Application = "PORTAL";
                Log.ActivityDate = DateTime.Now;
                Log.ActivityDesc = desc;
                Log.ActivityType = type;
                Log.Username = username;
                Log.Save();
                uow.CommitChanges();

                switch (type)
                {
                    case "DEBUG":
                        log.Debug(desc);
                        break;
                    case "INFO":
                        log.Info(desc);
                        break;
                    case "WARN":
                        log.Warn(desc);
                        break;
                    case "ERROR":
                        log.Error(desc);
                        break;
                    case "FATAL":
                        log.Fatal(desc);
                        break;
                    default:
                        log.Debug(desc);
                        break;
                }
            }
        }
        ////////////////////////////////////////////////////////////////////////

    }//END Class
}//END Namespace