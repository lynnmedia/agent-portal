﻿using System;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace AgentPortal.Support.Utilities
{
    public class ReportRenderer
    {
        public static void PaymentReceipt(string outputPdfPath, 
                                          string logoPath, 
                                          string address, 
                                          string policyNo,
                                          string insuredName,
                                          string policyEffDate,
                                          string policyExpDate, 
                                          string payType, 
                                          string payNotes, 
                                          string payDate, 
                                          string payAmt)
        {
            PdfBuilder Pdf = new PdfBuilder();

            Pdf.OutputPdf = outputPdfPath;
            Pdf.Logo = logoPath;
            Pdf.Setup();
            

            //First table
            PdfPTable table = new PdfPTable(4); // Columns

            float[] widths = new float[] { 1f, 1f, 2f, 4f };
            table.SetWidths(widths);

            //1ST ROW
            PdfPCell cell = new PdfPCell();
            cell.Border = 0;
            cell.Colspan = 4;
            cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(Pdf.Logo);
            Pdf.AddImageInCell(cell, image, 175f, 175f, 1);
            table.AddCell(cell);
            table.SpacingAfter = 30f;

            //2nd row 
            Pdf.AddtextCell(table, new PdfPCell(new Phrase(address)), 4, 1, 2);

            // 3rd row (empty)
            Pdf.AddtextCell(table, new PdfPCell(new Phrase(" ")), 4, 1, 0);

            // 4th row
            Pdf.AddtextCell(table, new PdfPCell(new Phrase(" ")), 2, 0, 0);
            Pdf.AddtextCell(table, new PdfPCell(new Phrase("Policy Number:", Pdf.SetFont())), 1, 2, 0);
            table.AddCell(policyNo);

            // 5th row
            Pdf.AddtextCell(table, new PdfPCell(new Phrase(" ")), 2, 0, 0);
            Pdf.AddtextCell(table, new PdfPCell(new Phrase("Insured Name:", Pdf.SetFont())), 1, 2, 0);
            table.AddCell(insuredName);

            // 6th row
            Pdf.AddtextCell(table, new PdfPCell(new Phrase(" ")), 2, 0, 0);
            Pdf.AddtextCell(table, new PdfPCell(new Phrase("Eff Date:", Pdf.SetFont())), 1, 2, 0);
            table.AddCell(policyEffDate);

            // 7th row
            Pdf.AddtextCell(table, new PdfPCell(new Phrase(" ")), 2, 2, 0);
            Pdf.AddtextCell(table, new PdfPCell(new Phrase("Exp Date:", Pdf.SetFont())), 1, 2, 0);
            table.AddCell(policyExpDate);

            table.SpacingAfter = 10f;
            Pdf.Doc.Add(table);

            //Second table
            PdfPTable table1 = new PdfPTable(4); // Columns

            //1st row

            Pdf.AddtextCell(table1, new PdfPCell(new Phrase("Payment Receipt", Pdf.SetFont())), 4, 1, 0);

            //2nd row 
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase(" ")), 4, 1, 0);

            //3rd row
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase("Type", Pdf.SetFont())), 1, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase("Notes", Pdf.SetFont())), 1, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase("Date", Pdf.SetFont())), 1, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase("Amount", Pdf.SetFont())), 1, 1, PdfPCell.BOX);

            //4th row
            //table1.AddCell(payType);
            //table1.AddCell(payNotes);
            //table1.AddCell(payDate);
            //table1.AddCell(payAmt);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase(payType)), 1, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase(payNotes)), 1, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase(payDate)), 1, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table1, new PdfPCell(new Phrase(payAmt)), 1, 1, PdfPCell.BOX);
            table1.SpacingAfter = 450f;
            Pdf.Doc.Add(table1);

            //Third table
            PdfPTable table2 = new PdfPTable(4); // Columns
            Pdf.AddtextCell(table2, new PdfPCell(new Phrase("Access your policy information online. Visit www.palminsure.com. Thank you for your Business.", FontFactory.GetFont("Arial", 10, Font.NORMAL))), 4, 1, PdfPCell.BOX);
            Pdf.AddtextCell(table2, new PdfPCell(new Phrase(" ")), 4, 1, 0);
            Pdf.AddtextCell(table2, new PdfPCell(new Phrase("Printed: " + DateTime.Now.ToShortDateString(), FontFactory.GetFont("Arial", 7, Font.NORMAL))), 4, 2, 0);
            Pdf.Doc.Add(table2);
            Pdf.Doc.Close();

        }


    }//END Class
}//END Namespace