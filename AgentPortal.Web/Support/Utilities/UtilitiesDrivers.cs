﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System.Globalization;

namespace AgentPortal.Support.Utilities
{
    public class UtilitiesDrivers
    {
        public static bool  IsDriverUnacceptableRisk(string pFName, string pLName, string pDateOfBirth, string pLicenseNumber, string pLicenseState)
        {
            bool isDriverUnacceptableRisk = false;
            //int verifyDrivers = 0;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                CultureInfo culture = new CultureInfo("en-US");
                DateTime dateOfBirth = Convert.ToDateTime(pDateOfBirth, culture);
                XPCollection<UnacceptableRiskDO> unacceptableDrivers = null;

                unacceptableDrivers = new XPCollection<UnacceptableRiskDO>(uow,(CriteriaOperator.Parse("FirstName = ? AND LastName = ? AND DOB = ?", pFName, pLName, dateOfBirth)));

                if (unacceptableDrivers != null && unacceptableDrivers.Count > 0)
                    isDriverUnacceptableRisk = true;

                if (unacceptableDrivers.Count > 0 && !String.IsNullOrEmpty(pLicenseNumber))
                {
                    if (!unacceptableDrivers.FirstOrDefault().LicenseNo.Equals(pLicenseNumber, StringComparison.OrdinalIgnoreCase))
                        isDriverUnacceptableRisk = false;

                    if (!String.IsNullOrEmpty(pLicenseState) && (!unacceptableDrivers.FirstOrDefault().LicenseState.Equals(pLicenseState, StringComparison.OrdinalIgnoreCase)))
                        isDriverUnacceptableRisk = false;
                }

            }
            
            return isDriverUnacceptableRisk;
        }
    }
}