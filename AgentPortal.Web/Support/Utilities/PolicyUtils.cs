﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.DataObjects.AuditRenewalInfoCode;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;

namespace AgentPortal.Support.Utilities
{
    public class PolicyWebUtils
    {
        public static SelectedData GetCoverageInfo(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string query = string.Format(@"select l.IndexID, 
							    case l.Coverage 
									    when 'BI' then 'Bodily Injury'               
									    when 'PIP' then 'Personal Injury Protection' 
									    when 'PD' then 'Property Damage'             
									    when 'UM' then 'Uninsured Motorist'          
									    when 'MP' then 'Med Pay'
									    when 'COMP' then 'Comprehensive'
									    when 'COLL' then 'Collision'
									    when 'LOU' then 'Loss Of Use'
									    else ''
							    end as Coverage,

							    case l.Coverage
									    when 'BI' then
												    case 
													    when BILimit = '10/20' then cast('10,000/20,000' as varchar(50)) 
													    else cast('N/A' AS varchar(50))
												    end 
									    when 'PD' then '10,000' 
									    when 'PIP' then '10,000, '+CAST(PIPDed AS varchar(20))+' Deduct '+
															    case when NIO=1 then 'NIO' 
															    else 
															    case when NIRR=1 then 'NIRR' 
															    else '' end 
															    +
															    Case WorkLoss when Null then ', W.L.E.' when 1 then ', W.L.E.' when 0 then '' end
															    end 
									    when 'UM' then 
												    case 
													    when UMLimit = '10/20' then cast('10,000/20,000' as varchar(50)) +
																													    case when UMStacked=1 then ' Stacked' else ' Unstacked' end 
													    else cast('N/A' AS varchar(50))
												    end  
									    when 'MP' then
												    case 
													    when MedPayLimit!='' and MedPayLimit!='0' then CAST(MedPayLimit AS varchar(30)) else ''                                                                                                                                             end
									    when 'COMP' then
												    case when exists (select 1 from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and CompDed != '' and CompDed IS NOT NULL and CompDed != 'NONE')  
																    then (select MAX(CompDed) from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and CompDed != '' and CompDed IS NOT NULL and CompDed!='NONE')+' Deduct, ACV up to $45k' else '' 
												    end
									    when 'COLL' then
												    case when exists (select 1 from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and CollDed != '' and CollDed IS NOT NULL and CollDed != 'NONE')  
																    then (select MAX(CollDed) from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and CollDed !='' and CollDed IS NOT NULL and CollDed!='NONE')+' Deduct, ACV up to $45k' else '' 
												    end
									    when 'LOU' then 
												    case   
																    when HasLOU=1 then '$20/Day Max $600' else 'N/A' 
												    end
									    when 'ADND' then 
												    case   
																    when ADNDLimit>0 then CAST(ADNDLimit as varchar(20)) else 'N/A' 
												    end    
							    end as LimitDesc,
		  
							    case l.Coverage
									    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
									    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
									    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
									    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
									    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
									    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
									    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'')  
									    when 'LOU' then ISNULL(p.LOUCost,'')  
									    when 'ADND' then ISNULL(p.ADNDCost,'')  
							    end as 'Vehicle 1', 

							    case l.Coverage 
									    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
									    when 'LOU' then '' 
									    when 'ADND' then '' 
							    end as 'Vehicle 2', 

							    case l.Coverage 
									    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
									    when 'LOU' then '' 
									    when 'ADND' then '' 
							    end as 'Vehicle 3',

							    case l.Coverage
									    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
									    when 'LOU' then ''
									    when 'ADND' then '' 
							    end as 'Vehicle 4',

							    case l.Coverage
									    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
									    when 'LOU' then ''
									    when 'ADND' then '' 
							    end as 'Vehicle 5'

							    from Policy p, AARateCoverages l
							    where p.PolicyNo='{0}'
							    and l.Coverage!='RENTAL'
							    UNION
							    Select 99 as IndexID,
								    '' as Coverage,
								    'Vehicle Totals:' as LimitDesc,
									    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
														    case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
														    case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
														    case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
														    case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
														    case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
														    case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
														    isNULL(StatedAmtCost,0) from PolicyCars c,Policy p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=1),0)+
									    ISNULL((Select isNULL(LOUCost,0)+isNULL(ADNDCost,0) from Policy where PolicyNo='{0}'),0) as 'Vehicle 1',
									    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
														    case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
														    case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
														    case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
														    case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
														    case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
														    case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
														    isNULL(StatedAmtCost,0) from PolicyCars c,Policy p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=2),0) as 'Vehicle 2',
									    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
														    case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
														    case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
														    case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
														    case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
														    case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
														    case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
														    isNULL(StatedAmtCost,0) from PolicyCars c,Policy p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=3),0) as 'Vehicle 3',
									    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
														    case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
														    case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
														    case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
														    case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
														    case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
														    case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
														    isNULL(StatedAmtCost,0) from PolicyCars c,Policy p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=4),0) as 'Vehicle 4',
									    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
														    case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
														    case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
														    case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
														    case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
														    case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
														    case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
														    isNULL(StatedAmtCost,0) from PolicyCars c,Policy p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=5),0) as 'Vehicle 5'
							    order by IndexID", policyno);

                return uow.ExecuteQuery(query);
            }
        }//END GetCoverageInfo
        public static SelectedData GetEndorsementCoverageInfo(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string query = string.Format(@"select l.IndexID, 
  case l.Coverage 
                    when 'BI' then 'Bodily Injury'               
                    when 'PIP' then 'Personal Injury Protection' 
                    when 'PD' then 'Property Damage'             
                    when 'UM' then 'Uninsured Motorist'          
                    when 'MP' then 'Med Pay'
                    when 'COMP' then 'Comprehensive'
                    when 'COLL' then 'Collision'
                    when 'LOU' then 'Loss Of Use'
                    else ''
  end as Coverage,

  case l.Coverage
                    when 'BI' then
                                     case 
                                              when BILimit = '10/20' then cast('10,000/20,000' as varchar(50)) 
                                              else cast('N/A' AS varchar(50))
                                     end 
                    when 'PD' then '10,000' 
                    when 'PIP' then '10,000, '+CAST(PIPDed AS varchar(20))+' Deduct '+
case when NIO=1 then 'NIO' 
else 
case when NIRR=1 then 'NIRR' 
else '' end 
+
Case WorkLoss when Null then ', W.L.E.' when 0 then ', W.L.E.' when 1 then '' end
end 
                    when 'UM' then 
                                     case 
                                              when UMLimit = '10/20' then cast('10,000/20,000' as varchar(50)) +
                                  case when UMStacked=1 then ' Stacked' else ' Unstacked' end 
                                              else cast('N/A' AS varchar(50))
                                     end  
                    when 'MP' then
                                     case 
                                                       when MedPayLimit!='' and MedPayLimit!='0' then CAST(MedPayLimit AS varchar(30)) else ''               end
                    when 'COMP' then
                                     case when exists (select 1 from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed != '' and CompDed IS NOT NULL and CompDed!='NONE')  
         then (select MAX(CompDed) from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CompDed != '' and CompDed IS NOT NULL and CompDed!='NONE')+' Deduct, ACV up to $45k' else '' 
                                     end
                    when 'COLL' then
                                     case when exists (select 1 from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CollDed != '' and CollDed IS NOT NULL and CollDed != 'NONE')  
         then (select MAX(CollDed) from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and CollDed != '' and CollDed IS NOT NULL and CollDed!='NONE')+' Deduct, ACV up to $45k' else '' 
                                     end
                    when 'LOU' then 
                                     case   
         when HasLOU=1 then '$20/Day Max $600' else 'N/A' 
                                     end
                    when 'ADND' then 
                                     case   
         when ADNDLimit>0 then CAST(ADNDLimit as varchar(20)) else 'N/A' 
                                     end    
  end as LimitDesc,
                   
  case l.Coverage
                    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
                    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
                    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
                    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
                    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
                    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'') 
                    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=1),'')  
                    when 'LOU' then ISNULL(p.LOUCost,'')  
                    when 'ADND' then ISNULL(p.ADNDCost,'')  
  end as 'Vehicle 1', 

  case l.Coverage 
                    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=2),'') 
                    when 'LOU' then '' 
                    when 'ADND' then '' 
  end as 'Vehicle 2', 

  case l.Coverage 
                    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=3),'') 
                    when 'LOU' then '' 
                    when 'ADND' then '' 
  end as 'Vehicle 3',

  case l.Coverage
                    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                   when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=4),'') 
                    when 'LOU' then ''
                    when 'ADND' then '' 
  end as 'Vehicle 4',

  case l.Coverage
                    when 'BI' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.BIAnnlPRem else c2.BIWritten end from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'PIP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PIPAnnlPRem else c2.PIPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'PD' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.PDAnnlPRem else c2.PDWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'UM' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.UMAnnlPRem else c2.UMWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'MP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.MPAnnlPRem else c2.MPWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'COMP' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CompAnnlPRem else c2.CompWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'COLL' then ISNULL((Select Top 1 case when p.isAnnual=1 then c2.CollAnnlPRem else c2.CollWritten end  from PolicyEndorseQuoteCars c2 where c2.PolicyNo=p.PolicyNo and c2.CarIndex=5),'') 
                    when 'LOU' then ''
                    when 'ADND' then '' 
  end as 'Vehicle 5'

  from PolicyEndorseQuote p, AARateCoverages l
  where p.PolicyNo='{0}'
  and l.Coverage!='RENTAL'
  UNION
  Select 99 as IndexID,
           '' as Coverage,
           'Vehicle Totals:' as LimitDesc,
                    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
                                                      case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
                                                      case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
                                                      case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
                                                      case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
                                                      case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
                                                      case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
                                                      isNULL(StatedAmtCost,0) from PolicyEndorseQuoteCars c,PolicyEndorseQuote p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=1),0)+
                    ISNULL((Select isNULL(LOUCost,0)+isNULL(ADNDCost,0) from Policy where PolicyNo='{0}'),0) as 'Vehicle 1',
                    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
                                                      case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
                                                      case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
                                                      case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
                                                      case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
                                                      case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
                                                      case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
                                                      isNULL(StatedAmtCost,0) from PolicyEndorseQuoteCars c,PolicyEndorseQuote p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=2),0) as 'Vehicle 2',
                    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
                                                      case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
                                                      case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
                                                      case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
                                                      case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
                                                      case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
                                                      case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
                                                      isNULL(StatedAmtCost,0) from PolicyEndorseQuoteCars c,PolicyEndorseQuote p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=3),0) as 'Vehicle 3',
                    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
                                                      case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
                                                      case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
                                                      case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
                                                      case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
                                                      case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
                                                      case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
                                                      isNULL(StatedAmtCost,0) from PolicyEndorseQuoteCars c,PolicyEndorseQuote p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=4),0) as 'Vehicle 4',
                    ISNULL((Select Top 1 case when p.isAnnual=1 then BIAnnlPrem else BIWritten end +
                                                      case when p.isAnnual=1 then PDAnnlPrem else PDWritten end +
                                                      case when p.isAnnual=1 then PIPAnnlPrem else PIPWritten end +
                                                      case when p.isAnnual=1 then MPAnnlPRem else MPWritten end +
                                                       case when p.isAnnual=1 then UMAnnlPRem else UMWritten end +
                                                      case when p.isAnnual=1 then CompAnnlPRem else CompWritten end +
                                                      case when p.isAnnual=1 then CollAnnlPRem else CollWritten end +
                                                      isNULL(StatedAmtCost,0) from PolicyEndorseQuoteCars c,PolicyEndorseQuote p where p.PolicyNo=c.PolicyNo and p.PolicyNo='{0}' and CarIndex=5),0) as 'Vehicle 5'
  order by IndexID", policyno);

                return uow.ExecuteQuery(query);
            }
        }//END GetCoverageInfo

        public static SelectedData GetHistory(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string query = string.Format(@"
							select 
                                   cast(ProcessDate as DATE) as ProcessDate,
                                   cast(EffDate as DATE) as Effective,
								   case Action 
										when 'NEW' then 'NEW POLICY ISSUED'
										when 'ENDORSE' then 'POLICY ENDORSED'
										when 'NOTICE' then 'NOTICE OF CANCELLCATION'
										when 'REI' then 'POLICY REINSTATED'
										when 'SUS' then 'SUSPENSE LETTER'
										when 'SUS2' then '2nd SUSPENSE LETTER'
										when 'CANCEL' then 'POLICY CANCELLED'
										when 'REN' then 'POLICY RENEWAL'
										when 'QTE' then 'RENEWAL QUOTE ISSUED'
										when 'NONRENEW' then 'NON_RENEWAL ISSUED'
										else 'UKN ON POLICY'
								   end as ActionDesc,      
								   upper(UserName) as UserName, 
								   convert(money,OrigWritten) as OriginalWritten, 
								   convert(money,NewWritten) as NewWritten,
								   CONVERT(money,NewWritten-OrigWritten) as ChangeInWritten,
								   case when Action='NOTICE' and CancelType='NON-PAY' then 'Non Payment of Premium: $'+cast(CONVERT(money,NoticeAmount) as varchar(10)) 
										else (Select MAX(Reason) from AuditPolicyHistoryReasons where HistoryKey=h.IndexID and ReasonNo=1)
								   end AS Desc1,
								   (Select MAX(Reason) from AuditPolicyHistoryReasons where HistoryKey=h.IndexID and ReasonNo=2) AS Desc2,
								   (Select MAX(Reason) from AuditPolicyHistoryReasons where HistoryKey=h.IndexID and ReasonNo=3) AS Desc3,
								   (Select MAX(Reason) from AuditPolicyHistoryReasons where HistoryKey=h.IndexID and ReasonNo=4) AS Desc4,
								   IndexID as HistoryKey
							  from AuditPolicyHistory h where PolicyNo='{0}' AND (IsShow = '1' OR IsShow IS NULL)
							order by IndexID asc", policyno);

                return uow.ExecuteQuery(query);
            }
        }

        public static Dictionary<string,List<string>> GetHistoryReasons(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string query = string.Format(@"
	                    ;with reasonsCte as(
				                    select distinct
			                        case when ISNULL(r.ReasonCode,'')='' and a.Reason <>'' and ISNULL(IndexDesc,'')<>'' 
                                         Then a.Reason +'('+UPPER(IndexDesc)+')'
				                        when ISNULL(r.ReasonCode,'')='' and a.Reason <>'' and ISNULL(IndexDesc,'')='' Then a.Reason
				                     Else '' END as titanReason,
				                    a.HistoryKey,r.ReasonGroup,a.PolicyNo, a.ReasonCode AS SelectedReasonCode,r.ReasonCode, r.Reason, r.IsSpecific,a.IndexDesc,
				                    CASE	WHEN r.ReasonGroup='Vehicle' AND a.ReasonCode like '%-%' 
						                    THEN SUBSTRING(a.ReasonCode, CHARINDEX('-', a.ReasonCode)+1, LEN(a.ReasonCode)) 
						                    ELSE NULL END AS Vin,
				                    CASE	WHEN r.ReasonGroup='Driver' AND a.ReasonCode like '%-%' 
						                    THEN SUBSTRING(a.ReasonCode, CHARINDEX('-', a.ReasonCode)+1, LEN(a.ReasonCode)) 
						                    ELSE NULL END As LicenseNo
				                    From AuditPolicyHistory h join AuditPolicyHistoryReasons a on a.PolicyNo = h.PolicyNo and a.HistoryKey=h.IndexID
				                    left join AgentEndorseReasons r on (a.ReasonCode = r.ReasonUniqId OR a.ReasonCode like r.ReasonUniqId +'-%')
				                    Where a.ReasonType='ENDORSE' AND a.Policyno='{0}'
			                    ),
			                    reasonsFinal as(
		                    select  
		                    CASE WHEN titanReason <>'' Then  titanReason Else
		                    Reason +''+(
		                    case when ReasonGroup='vehicle' and ISNULL(Vin,'')<>'' Then '(Car '+ISNULL(IndexDesc,'')+'-' + UPPER(Vin) +')'
		                        when ReasonGroup='driver' and ISNULL(LicenseNo,'')<>'' AND ISNULL(IndexDesc,'')='' 
                                    Then '(Driver '+UPPER(ISNULL(IndexDesc,'L-'+LicenseNo)) +')' 
		                        when ReasonGroup='driver' and ISNULL(LicenseNo,'')<>'' Then '(Driver -'+UPPER(IndexDesc) +')' 
		                        Else '' End)
		                        END As Reason,
		                    HistoryKey,PolicyNo from reasonsCte
		                    )
		                    select rf.HistoryKey,
		                    STUFF((
				                    SELECT '||' + Reason FROM  reasonsFinal rf2
				                    WHERE (rf.HistoryKey=rf2.HistoryKey)
				                    FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,2,'') As Reasons 
		                    from reasonsFinal rf
		                    group by rf.HistoryKey
                    ", policyno);

                var reasons = uow.ExecuteQuery(query);

                var historyReasons = new Dictionary<string, List<string>>();
                if(reasons!= null && reasons.ResultSet.Any() && reasons.ResultSet[0].Rows.Any())
                {
                    var reasonRows = reasons.ResultSet[0].Rows;
                    foreach (SelectStatementResultRow row in reasonRows)
                    {
                        var historyKey = row.Values[0]?.ToString();
                        if (!string.IsNullOrEmpty(historyKey) && !historyReasons.ContainsKey(historyKey))
                        {
                            var reasonsText = row.Values[1]?.ToString()?.ToUpper();
                            if (!string.IsNullOrEmpty(reasonsText))
                            {
                                var reasonText = reasonsText.Split('|', '|').Where(x=> !string.IsNullOrEmpty(x)).ToList();
                                if (reasonText.Any())
                                    historyReasons.Add(historyKey, reasonText);
                            }
                        }
                    }
                }
                return historyReasons;
            }
        }

        public static void SaveRenewalSnapshot(string policyno, int term, bool isannual, bool issixmonth, int payplan,
                                               string payplanenglish)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var save = new AuditRenewalInfo(uow)
                {
                    PolicyNo = policyno,
                    PolicyTerm = term,
                    IsAnnual = isannual,
                    SixMonth = issixmonth,
                    PayPlan = payplan,
                    PayPlanEnglish = payplanenglish,
                    DateCreated = DateTime.Now
                };

                save.Save();
                uow.CommitChanges();
            }
        }

        public static bool ShowEftPaymentProfileMissingMsg(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var policy = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyno));
                if (policy?.EFT == true)
                {
                    var pPaymentProfiles = new XPCollection<XpoPolicyPaymentProfile>(uow,
                     CriteriaOperator.Parse("PolicyNo = ?", policyno));
                    return pPaymentProfiles == null || pPaymentProfiles.Count == 0;
                }
                return false;
            }
        }

        public static string GetPolicyInsuredEmail(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var policy = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyno));
                return policy?.MainEmail;
            }
        }


        public static void GetEndorsementChangeTypes(out bool premiumBearingChanges, out bool nonPremiumBearingChanges, string policyNo, bool isFromFieldChange = false)
        {
            premiumBearingChanges = false;
            nonPremiumBearingChanges = false;
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    SelectedData result = uow.ExecuteSproc("spUW_GetEndorsementChangeTypes", new OperandValue(policyNo), new OperandValue(isFromFieldChange));

                    foreach (SelectStatementResultRow row in result.ResultSet[0].Rows)
                    {
                        premiumBearingChanges = Convert.ToBoolean(row.Values[0].ToString());
                        nonPremiumBearingChanges = Convert.ToBoolean(row.Values[1].ToString());
                        break;
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}