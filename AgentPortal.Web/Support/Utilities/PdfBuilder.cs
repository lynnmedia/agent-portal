﻿using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace AgentPortal.Support.Utilities
{
    public class PdfBuilder
    {
        /// <summary>
        /// File Path To Output/Save PDF
        /// </summary>
        public string OutputPdf { get; set; }
        /// <summary>
        /// File Path To Logo
        /// </summary>
        public string Logo { get; set; }
        public int FontSize1 { get; set; }
        public string FontType { get; set; }
        public Document Doc { get; set; }
        //--------------------------------------
        
        public PdfBuilder()
        {
            FontSize1 = 12;
            FontType = "Arial";
        }//Constructor
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public PdfBuilder(string tOutputPdf, string tLogo, int tFontSize1, string tFontType)
        {
            OutputPdf = tOutputPdf;
            Logo = tLogo;
            FontSize1 = tFontSize1;
            FontType = tFontType;
        }//Constructor
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void Setup()
        {
            Doc = new Document(PageSize.LETTER, 15F, 15F, 35F, 35F);
            PdfWriter.GetInstance(Doc, new FileStream(OutputPdf, FileMode.Create));
            Doc.Open();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public Font SetFont()
        {
            Font arial = FontFactory.GetFont("Arial", FontSize1, Font.BOLD);

            return FontFactory.GetFont(FontType, FontSize1, Font.BOLD);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void AddImageInCell(PdfPCell tcell, iTextSharp.text.Image image, float fitWidth, float fitHight, int Alignment)
        {
            image.ScaleToFit(fitWidth, fitHight);
            image.Alignment = Alignment;
            tcell.AddElement(image);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void AddtextCell(PdfPTable ttable, PdfPCell tcell)
        {
            tcell.Colspan = 3;
            tcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tcell.Border = 0;
            ttable.AddCell(tcell);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void AddtextCell(PdfPTable ttable, PdfPCell tcell, float paddingLeft, float paddingRight)
        {
            tcell.Colspan = 3;
            tcell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
            tcell.PaddingLeft = paddingLeft;
            tcell.PaddingRight = paddingRight;
            tcell.Border = 0;
            ttable.AddCell(tcell);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void AddtextCell(PdfPTable ttable, PdfPCell tcell, float paddingLeft, float paddingRight, int hAlign)
        {
            tcell.Colspan = 3;
            tcell.HorizontalAlignment = hAlign; //0=Left, 1=Centre, 2=Right
            tcell.PaddingLeft = paddingLeft;
            tcell.PaddingRight = paddingRight;
            tcell.Border = 0;
            ttable.AddCell(tcell);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public void AddtextCell(PdfPTable ttable, PdfPCell tcell, int Colspan, int HorizontalAlignment, int Border)
        {
            tcell.Colspan = Colspan;
            tcell.HorizontalAlignment = HorizontalAlignment; //0=Left, 1=Centre, 2=Right
            tcell.VerticalAlignment = iTextSharp.text.Element.ALIGN_CENTER;
            tcell.Border = Border;
            ttable.AddCell(tcell);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }//END Class
}//END Namespace