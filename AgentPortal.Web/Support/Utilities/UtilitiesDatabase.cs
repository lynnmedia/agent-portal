﻿/*
	Written By: David Wimbley
	
	Date Originally Completed:
	
	Purpose: Generic Data Access Layer for PALMS Applications
	
	Usage:
		-ExecuteScalar using parameters
		string[,] a = new string[,]
		{
			{"@PolicyNo", "PAFL0100475"}
		};
		Console.WriteLine("Result: " + Database.ExecuteScalar("SELECT Ins1First FROM Policy WHERE PolicyNo = @PolicyNo", a, System.Data.CommandType.Text));
	
	Update History:
		Added notes/comments - David Wimbley - 3/12/2012
*/

using System;
using System.Data;
using System.Data.SqlClient;

namespace AgentPortal.Support.Utilities
{
	public class Database
	{
		#region Init Vars
        public static string SERVER = ConfigSettings.ReadSetting("DB_Server");
        public static string USER = ConfigSettings.ReadSetting("DB_UserName");
        public static string PASS = ConfigSettings.ReadSetting("DB_Pass");
        public static string DBNAME = ConfigSettings.ReadSetting("DB_Catalog");

        private static string SqlConnectionString = String.Format(@"Data Source = {0};User Id={1}; password={2}; Initial Catalog = {3};", SERVER, USER, PASS, DBNAME);
       
		private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		#endregion

		private static string SetParametersValue(string query, string[,] parameters)
		{
			for (int i = 0; i < parameters.Length / 2; i++)
			{
				if (!string.IsNullOrEmpty(parameters[i, 0]))
					query = query.Replace(parameters[i, 0], "" + parameters[i, 1] + "");
			}
			return query;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public static object ExecuteScalar(string sp, CommandType commandType)
		{
			SqlConnection con = new SqlConnection(SqlConnectionString);
			SqlCommand com = new SqlCommand(sp, con);
			object result = null;

			com.CommandType = commandType;
			try
			{
				con.Open();
				result = com.ExecuteScalar();
				con.Close();
			}
			catch (Exception ex)
			{
				log.Fatal(string.Format("[DATABASE ERROR] Error In Execute Scalar: {0}", ex));
			}
			return result;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public static object ExecuteScalar(string sp, string[,] parameters, CommandType commandType)
		{
			log.Debug("asfdasf");
			SqlConnection con = new SqlConnection(SqlConnectionString);
			SqlCommand com = new SqlCommand(SetParametersValue(sp, parameters), con);
			object result = null;


			com.CommandType = commandType;
			Console.WriteLine("Query: " + com.CommandText);
			for (int i = 0; i < parameters.Length / 2; i++)
			{
				com.Parameters.AddWithValue(parameters[i, 0], parameters[i, 1]);
			}

			try
			{
				con.Open();
				result = com.ExecuteScalar();
				con.Close();
			}
			catch (System.Exception ex)
			{
				log.Fatal(string.Format("[DATABASE ERROR] Error In Execute Scalar: {0}", ex));
			}
			return result;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public static SqlDataReader ExecuteQuery(string sp, CommandType commandType)
		{
			SqlConnection con = new SqlConnection(SqlConnectionString);
			SqlCommand com = new SqlCommand(sp, con);
			SqlDataReader reader = null;

			com.CommandType = commandType;

			try
			{
				con.Open();
				reader = com.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch (System.Exception ex)
			{
				log.Fatal(string.Format("[DATABASE ERROR] Error In Execute Query: {0}", ex));
			}
			return reader;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public static SqlDataReader ExecuteQuery(string sp, string[,] parameters, CommandType commandType)
		{
			SqlConnection con = new SqlConnection(SqlConnectionString);
			SqlCommand com = new SqlCommand(SetParametersValue(sp, parameters), con);
			SqlDataReader reader = null;

			com.CommandType = commandType;
			Console.WriteLine(com.CommandText.ToString());
			for (int i = 0; i < parameters.Length / 2; i++)
			{
				com.Parameters.AddWithValue(parameters[i, 0], parameters[i, 1]);
			}

			try
			{
				con.Open();
				reader = com.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch (System.Exception ex)
			{
				log.Fatal(string.Format("[DATABASE ERROR] Error In Execute Query w Params: {0}", ex));
				Console.WriteLine(ex);
			}
			return reader;
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public static void ExecuteNonQuery(string sp, CommandType commandType)
		{
			SqlConnection con = new SqlConnection(SqlConnectionString);
			SqlCommand com = new SqlCommand(sp, con);


			com.CommandType = CommandType.StoredProcedure;

			try
			{
				con.Open();
				com.ExecuteNonQuery();
				con.Close();
			}
			catch (System.Exception ex)
			{
				log.Fatal(string.Format("[DATABASE ERROR] Error In Execute Non Query: {0}", ex));
			}
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public static void ExecuteNonQuery(string sp, string[,] parameters, CommandType commandType)
		{
			SqlConnection con = new SqlConnection(SqlConnectionString);
			SqlCommand com = new SqlCommand(SetParametersValue(sp, parameters), con);


			com.CommandType = commandType;

			for (int i = 0; i < parameters.Length / 2; i++)
			{
				com.Parameters.AddWithValue(parameters[i, 0], parameters[i, 1]);
			}

			log.Debug("Execute Non Query, Query: " + com.CommandText.ToString());
			try
			{
				con.Open();
				com.ExecuteNonQuery();
				con.Close();
			}
			catch (System.Exception ex)
			{
				log.Fatal(String.Format("[DATABASE ERROR] Fatal Error In Execute Non Query With Params: {0}", ex));
			}
		}//END ExecuteNonQuery
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}//END Class Dec.
}//END Namespace Dec.
