﻿using System;
using System.Globalization;
using System.Linq;
using AgentPortal.Support.DataObjects;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using AlertAuto.Integrations.Contracts.Verisk.RiskCheck;

namespace AgentPortal.Support.Utilities
{
    public class UtilitiesVehicles
    {
        public static bool IsAcceptableVehicle(string make, string model, string bodystyle)
        {
            bool isAcceptable = true;

            make = make?.ToUpper();  //LF 2/17/19
            model = model?.ToUpper();
            bodystyle = bodystyle?.ToUpper();

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
               /* previous XPCollection<UnacceptableVehiclesDO> getBadCars = new XPCollection<UnacceptableVehiclesDO>(uow);  LF 2/17/19 */
               if (!string.IsNullOrEmpty(make))
               {
                   XPCollection<UnacceptableVehiclesDO> getBadCars =
                       new XPCollection<UnacceptableVehiclesDO>(uow,
                           CriteriaOperator.Parse("Upper(ISOCode) = ?", make.ToUpper()));
                   if (getBadCars.Any())
                   {
                       foreach (UnacceptableVehiclesDO badCar in getBadCars)
                       {
                           if (badCar.ISOCode.ToUpper() == make.ToUpper()) //LF 2/17/19
                           {
                               if ((badCar.Model == "ALL" || badCar.Model.ToUpper() == model ||
                                    model.Contains(badCar.Model.ToUpper())) &&
                                   (badCar.BodyStyle == "ALL" || badCar.BodyStyle.ToUpper() == bodystyle))
                               {
                                   isAcceptable = false;
                               }
                           }
                       }
                   }
               }
            }
            return isAcceptable;
        }

        public static void MapModelToXpo(ResponseVehicle vehicle, XpoBrandedTitleVehicles brandedTitleVehicle)
        {
            brandedTitleVehicle.VIN = vehicle.VIN;
            brandedTitleVehicle.BrandedTitle1Code = vehicle.BrandedTitle1?.Code;
            brandedTitleVehicle.BrandedTitle1Description = vehicle.BrandedTitle1?.Description;
            brandedTitleVehicle.BrandedTitleState1 = vehicle.BrandedTitleState1;
            brandedTitleVehicle.LicensePlateNumber = vehicle.LicensePlateNumber;
            brandedTitleVehicle.Make = vehicle.Make;
            brandedTitleVehicle.Model = vehicle.Model;
            brandedTitleVehicle.VehYear = vehicle.ModelYear.ToString();
            brandedTitleVehicle.DateCreated = DateTime.Now;
            brandedTitleVehicle.VehTypeCode = vehicle.VehicleType?.Code;
            brandedTitleVehicle.VehTypeDesc = vehicle.VehicleType?.Description;
            brandedTitleVehicle.CreatedAgentCode = Sessions.Instance.AgentCode;
            if (vehicle.BrandedTitleDateFull1 != null)
                brandedTitleVehicle.BrandedTitleDate1 = DateTime.ParseExact(vehicle.BrandedTitleDateFull1, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None);

        }
    }
}