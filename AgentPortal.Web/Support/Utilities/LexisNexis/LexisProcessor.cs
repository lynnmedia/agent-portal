﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace AgentPortal.Support.Utilities.LexisNexis
{
    public interface IWebRequestProcessor
    {
        string ServiceUrl { get; set; }
        string RequestMethod { get; set; }
        string ContentType { get; set; }
        string UserAgent { get; set; }
        string ReturnMsg { get; set; }
        string SendMsg { get; set; }

        void Send();
        void Receive();
    }

    public class LexisProcessor : IWebRequestProcessor, IDisposable
    {
        public string ServiceUrl { get; set; }
        public string RequestMethod { get; set; }
        public string ContentType { get; set; }
        public string UserAgent { get; set; }
        public string ReturnMsg { get; set; }
        public string SendMsg { get; set; }
        private WebResponse WebResp { get; set; }

        public void Send()
        {
            var request = WebRequest.Create(ServiceUrl);

            ((HttpWebRequest)request).UserAgent = UserAgent;
            request.Method = RequestMethod;
            request.ContentType = ContentType;

            using (var writer = new StreamWriter(request.GetRequestStream()))
            {
                writer.Write(SendMsg);
                writer.Close();

                WebResp = request.GetResponse();
            }
        }

        public void Receive()
        {
            Stream webstrm = WebResp.GetResponseStream();

            if (webstrm == null)
            {
                //TODO: LOG THE ERROR MESSAGE HERE
                return;
            }

            var responseReader = new StreamReader(webstrm);
            var txtread = new StringReader(responseReader.ReadToEnd());
            var doc = XDocument.Load(txtread);
            XNamespace xmlns = "http://www.cp.rules/web-services/interactiveorderhandler";
            var listelements = doc.Descendants(xmlns + "handleInteractiveOrderResponse").ToList();

            foreach (var list in listelements)
            {
                ReturnMsg = list.Value;
            }
        }

        public void Dispose()
        {
            ServiceUrl = null;
            RequestMethod = null;
            ContentType = null;
            UserAgent = null;
            ReturnMsg = null;
            SendMsg = null;
            WebResp = null;
        }
    }
}
