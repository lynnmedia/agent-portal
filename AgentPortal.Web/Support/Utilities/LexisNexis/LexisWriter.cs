﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace AgentPortal.Support.Utilities.LexisNexis
{
    class LexisWriter
    {
        //Client Node Data
        public string ClientId { get; set; }
        public string QuoteBackVal { get; set; }
        public string QuoteBackName { get; set; }
        //----------------------------------------------
        //Accounting/Rule_Plan Node Data
        public string AcctNo { get; set; }
        public string RulePlanName { get; set; }
        //----------------------------------------------
        //Subjects Node Data
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string MiddleInitial { get; set; }
        public string Ssn { get; set; }
        public string AddressType { get; set; }

        public string SubjectId { get; set; }
        public string AddressId { get; set; }
        //----------------------------------------------
        //Products Node/NCF File Node
        public string NcfType { get; set; }
        public string CreditVendor { get; set; }
        public string Format { get; set; }
        public string ModelId { get; set; }
        //----------------------------------------------
        public StringBuilder OutputXml { get; set; }
        public XNamespace Namespace { get; set; }
        //----------------------------------------------
        //All access to Doc should be handled through AddNode, make private and create Read method
        private XDocument Doc { get; set; }
        //----------------------------------------------
        private string SoapEnvHead = @"<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:int='http://www.cp.rules/web-services/interactiveorderhandler'  >
                                   <soapenv:Header/>
                                   <soapenv:Body>
                                      <int:handleInteractiveOrder>
                                 <string><![CDATA[<?xml version='1.0' encoding='UTF-8'?>";
        private string SoapEnvFoot = @"]]></string>
                                      </int:handleInteractiveOrder>
                                   </soapenv:Body>
                                </soapenv:Envelope>";

        public string TestMsg = @"<soapenv:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:int='http://www.cp.rules/web-services/interactiveorderhandler'  >
                                   <soapenv:Header/>
                                   <soapenv:Body>
                                      <int:handleInteractiveOrder>
                                 <string><![CDATA[<?xml version='1.0' encoding='UTF-8'?>
                                <order xsi:schemaLocation='http://cp.com/rules/client file:/C:/projects/framework/xml/formats/CPRulesOrderSchema.xsd' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://cp.com/rules/client'>
	                                            <client id='IP1053600'>
	                                                <quoteback name='caseId'>Test01XML_INT Cond01</quoteback>
	                                            </client>
	                                            <accounting>
	                                                <pnc>
	                                                    <account>770201TST</account>
	                                                    <special_billing_id/>
	                                                </pnc>
	                                            </accounting>
	                                            <rule_plan>
	                                            </rule_plan>
	                                            <products>
	                                                   <carrier_discovery_live subject='s1'>
	                                               <reportType>B</reportType>           
	                                                </carrier_discovery_live>
	                                            </products>
	                                            <dataset>
	                                                <subjects>
	                                                    <subject id='s1'>
	                                                        <name>
	                                                            <prefix/>
	                                                            <first>LINDA</first>
	                                                            <middle/>
	                                                            <last>TUTTLE</last>
	                                                            <suffix/>
	                                                            <maiden/>
	                                                        </name>
	                                                <address type='residence' ref='A2'/>
	                                                    </subject>
	                                                </subjects>
	                                                <vehicles>
	                                                    <vehicle id='V1'>
	                                                        <year>2010</year>
	                                                        <make>DODGE</make>
	                                                        <model>CHARGER</model>
    			                                            <vin>ABCD1234ABCD26283</vin>
	                                                    </vehicle>
	                                                </vehicles>
	                                             <addresses>
	                                               <address id='A2'>
	                                                   <house>1000</house>
	  		                                            <street1>8th</street1>
	                                                    <city>REEDSPORT</city>
	                                                    <state>OR</state>
	                                                    <postalcode>97467</postalcode>
	                                                 </address>		 
	                                            </addresses>
	                                            </dataset>
                                            </order>]]></string>
                                      </int:handleInteractiveOrder>
                                   </soapenv:Body>
                                </soapenv:Envelope>";
        public LexisWriter()
        {
            Namespace = @"http://cp.com/rules/client";
        }

        public string CleanString(string cleanString = null)
        {
            cleanString = Regex.Replace(cleanString, @"\W", "", RegexOptions.IgnoreCase);
            return cleanString;
        }
        
        public void Create(string startNode)
        {
            //XElement lexisXml = new XElement(Namespace + startNode);
            string xsistring = @"http://cp.com/rules/client file:/C:/projects/framework/xml/formats/CPRulesOrderSchema.xsd";

            XNamespace xsi = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");
            XNamespace defaultNamespace = Namespace;
            XElement lexisXml = new XElement(defaultNamespace + startNode,
                new XAttribute(xsi + "schemaLocation", xsistring));

            lexisXml.Add(new XAttribute(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance"));

            Doc = XDocument.Parse(lexisXml.ToString());
        }
        
        public void AddNode(string newNode, object newNodeAttr, object nestedNodes, string addNodeAfter)
        {
            XElement node;
            if (newNodeAttr == null)
            {
                node = new XElement(Namespace + newNode);
            }
            else
            {
                node = new XElement(Namespace + newNode, newNodeAttr);
            }

            if (nestedNodes != null)
                node.Add(nestedNodes);
            Doc.Descendants(Namespace + addNodeAfter).Single().Add(node);
        }

        public void AddNode(string newNode, string newNodeVal, object newNodeAttr, object nestedNodes, string addNodeAfter)
        {
            XElement node;
            if (newNodeAttr == null && string.IsNullOrEmpty(newNodeVal))
            {
                node = new XElement(Namespace + newNode);
            }
            else
            {
                node = new XElement(Namespace + newNode, newNodeVal, newNodeAttr);
            }

            if (nestedNodes != null)
                node.Add(nestedNodes);
            Doc.Descendants(Namespace + addNodeAfter).Single().Add(node);
        }

        public XDocument Write()
        {
            return Doc;
        }

        public string BuildSoapEnv(string xml)
        {
            return string.Format("{0}{1}{2}", SoapEnvHead, xml, SoapEnvFoot);
        }
    }//END Class
}//END Namespace
