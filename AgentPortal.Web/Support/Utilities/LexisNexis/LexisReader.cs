﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace AgentPortal.Support.Utilities.LexisNexis
{
    public enum LexisFormat
    {
        Edits,
        Xml
    }

    public enum LexisEditsValues
    {
        HeaderRecord,
        CreditScore,
        ErrCode
    }

    public interface IEdits
    {
        string[] EditsMsgLines { get; set; }
        string EdistMsg { get; set; }
        bool HasEditsErr { get; set; }
        string EditsErrMsg { get; set; }

        string GetEditsScalar(LexisEditsValues values);
    }

    public interface ILexisXml
    {
        XElement GetXmlScalar();
    }

    public class LexisReader : ILexisXml, IEdits
    {
        public LexisFormat Format { get; set; }
        //XML Specific
        public string LexisXml { get; set; }
        public string Filename { get; set; }
        public string Namespace { get; set; }
        public string NodeExpression { get; set; }
        //EDITS Specific
        public string[] EditsMsgLines { get; set; }
        public string EdistMsg { get; set; }
        public bool HasEditsErr { get; set; }
        public string EditsErrMsg { get; set; }
        //-------------------------------------------------------
        private XDocument Doc { get; set; }
        private XmlNamespaceManager NamespaceMgr { get; set; }
        
        public LexisReader(){ }

        public LexisReader(LexisFormat format)
        {
            Format = format;
        }

        public LexisReader(string tfilename, string tnamespace, string tnodeexpression)
        {
            Filename = tfilename;
            Namespace = tnamespace;
            NodeExpression = tnodeexpression;
        }

        private void LoadXml()
        {
            Doc = XDocument.Load(Filename);
            NamespaceMgr = new XmlNamespaceManager(Doc.CreateReader().NameTable);
            NamespaceMgr.AddNamespace("a", Namespace);   
        }

        public XElement GetXmlScalar()
        {
            LoadXml();
            var node = Doc.XPathSelectElement(NodeExpression, NamespaceMgr);
            return node;
        }
        
        //-----------------------------------------------------------------

        public string GetEditsScalar(LexisEditsValues values)
        {
            string line;
            switch (values)
            {
                case LexisEditsValues.CreditScore:
                    return ProcessCreditScore();
                case LexisEditsValues.ErrCode:
                    return ProcessErrorMsg();
                    
                case LexisEditsValues.HeaderRecord:
                    ProcessHeaderRecords();
                    return "";
                default:
                    return "";
            }
        }

        private string ProcessHeaderRecords()
        {
            foreach (var line in EditsMsgLines.Where(m => m.Contains("RI52") || m.Contains("RI51")))
            {
                Console.WriteLine(line);
            }

            return "";
        }

        private string ProcessCreditScore()
        {
            string creditscore = null;
            int numCreditLines = EditsMsgLines.Count(m => m.Contains("NR520000CM"));

            if (numCreditLines > 0)
            {
                creditscore = EditsMsgLines.Where(m => m.Contains("NR520000CM")).ToArray()[0];
                creditscore = creditscore.Length > 45 ? creditscore.Substring(42, 3) : "ERROR PROCESSING CREDIT SCORE";
            }
         
            return creditscore;
        }

        private string ProcessErrorMsg()
        {
            string errCode = null;

            int numCreditLines = EditsMsgLines.Count(m => m.Contains("NR52"));

            if (numCreditLines > 2)
            {
                errCode = EditsMsgLines.Where(m => m.Contains("NR52")).ToArray()[1];
                errCode = errCode.Length > 45 ? errCode.Substring(17, 4) : "ERROR PROCESSING CODE";
            }

            return errCode;
        }
        //-----------------------------------------------------------------

    }//END Class
}//END Namespace
