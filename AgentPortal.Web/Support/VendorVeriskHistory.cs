﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support
{
    public class VendorVeriskHistory : XPLiteObject
    {
        public VendorVeriskHistory(Session session) : base(session) { }
        public VendorVeriskHistory() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }


        [Key(true)]
        public int IndexID { get; set; }

        private string policyNo;
        public string PolicyNo
        {
            get { return policyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref policyNo, value); }
        }

        private string integrationType;
        public string IntegrationType
        {
            get { return integrationType; }
            set { SetPropertyValue<string>("IntegrationType", ref integrationType, value); }
        }

        private DateTime transactionDate;
        public DateTime TransactionDate
        {
            get { return transactionDate; }
            set { SetPropertyValue<DateTime>("TransactionDate", ref transactionDate, value); }
        }

        private string requestJson;
        public string RequestJson
        {
            get { return requestJson; }
            set { SetPropertyValue<string>("RequestJson", ref requestJson, value); }
        }

        private string responseCode;
        public string ResponseCode
        {
            get { return responseCode; }
            set { SetPropertyValue<string>("ResponseCode", ref responseCode, value); }
        }
        private string responseJson;
        public string ResponseJson
        {
            get { return responseJson; }
            set { SetPropertyValue<string>("ResponseJson", ref responseJson, value); }
        }

    }

    public class RiskCheckActionMessages : XPLiteObject
    {
        public RiskCheckActionMessages(Session session) : base(session) { }
        public RiskCheckActionMessages() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }


        [Key(true)]
        public int IndexID { get; set; }

        private string policyNo;
        public string PolicyNo
        {
            get { return policyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref policyNo, value); }
        }

        private string transactionId;
        public string TransactionId
        {
            get { return transactionId; }
            set { SetPropertyValue<string>("TransactionId", ref transactionId, value); }
        }


        private string reasonCode;
        public string ReasonCode
        {
            get { return reasonCode; }
            set { SetPropertyValue<string>("ReasonCode", ref reasonCode, value); }
        }


        private string entityType;
        public string EntityType
        {
            get { return entityType; }
            set { SetPropertyValue<string>("EntityType", ref entityType, value); }
        }


        private string entityNumber;
        public string EntityNumber
        {
            get { return entityNumber; }
            set { SetPropertyValue<string>("EntityNumber", ref entityNumber, value); }
        }

    }

    public class RiskCheckPolicyVehicles : XPLiteObject
    {
        public RiskCheckPolicyVehicles(Session session) : base(session) { }
        public RiskCheckPolicyVehicles() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }


        [Key(true)]
        public int IndexID { get; set; }

        private string policyNo;
        public string PolicyNo
        {
            get { return policyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref policyNo, value); }
        }

        private string transactionId;
        public string TransactionId
        {
            get { return transactionId; }
            set { SetPropertyValue<string>("TransactionId", ref transactionId, value); }
        }

        private string sequenceNumber;
        public string SequenceNumber
        {
            get { return sequenceNumber; }
            set { SetPropertyValue<string>("SequenceNumber", ref sequenceNumber, value); }
        }
        private string vin;
        public string VIN
        {
            get { return vin; }
            set { SetPropertyValue<string>("VIN", ref vin, value); }
        }

    }
}