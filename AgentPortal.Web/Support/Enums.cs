﻿namespace AgentPortal.Support
{
    public enum PymntLocation
    {
        AgentSweep,
        CyberSource
    }

    public enum PymntTypes
    {
        Renewal,
        NewBusiness,
        Reinstatement
    }
}