﻿
namespace AgentPortal.Support
{
    public class Constants
    {
        ////////////////////////
        //REGEX Patterns
        public const string EmailRegex    = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b";
        public const string DateTimeRegex = @"^(0?[1-9]|1[012])[/](0?[1-9]|[12][0-9]|3[01])[/][0-9]{4}(\s((0[1-9]|1[012])\:([0-5][0-9])((\s)|(\:([0-5][0-9])\s))([AM|PM|]{2,2})))?$";
        public const string PhoneRegex    = @"\d\d\d\-\d{1,3}-\d\d\d\d";
        public const string FlDlRegex     = @"^([a-z|A-Z][0-9]+)[-]([0-9]+)[-]([0-9]+)[-]([0-9]+)[-]([0-9]+)";
        ////////////////////////
        public const string blowfishKey    = "4g!c!n5ur4nc3>";
        public const string SuperAgentCode = "A00000003";

    }//END Class
}//END Namespace