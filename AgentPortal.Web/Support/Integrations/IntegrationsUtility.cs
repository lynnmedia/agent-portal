﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using AgentPortal.Models;
using AgentPortal.Services;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using Newtonsoft.Json;
using AlertAuto.Integrations.Contracts;
using AlertAuto.Integrations.Contracts.CardConnect;
using AlertAuto.Integrations.Contracts.MVR;
using AlertAuto.Integrations.Contracts.RAPA;
using AlertAuto.Integrations.Contracts.Verisk.APlus;
using AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier;
using AlertAuto.Integrations.Contracts.Verisk.RiskCheck;
using Newtonsoft.Json.Linq;
using Address = AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Address;
using DocumentType = AlertAuto.Integrations.Contracts.HelloSign.DocumentType;
using Driver = AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Driver;
using Task = System.Threading.Tasks.Task;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;

namespace AgentPortal.Support.Integrations
{
    public static class IntegrationsUtility
    {
        public static VeriskVinSearchResultBody SearchVin(string policyNo, string vin, string year = "", string make = "", string model = "",
            string bodyStyle = "")
        {
            if (string.IsNullOrEmpty(policyNo) || (!string.IsNullOrEmpty(vin) && vin.Length < 10))
            {
                return new VeriskVinSearchResultBody();
            }

            string vinSearchUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") + "/verisk/vinSearch?policyNo={policyNo}&VIN={vin}&ModelYear={year}&Make={make}&FullModelName={model}&BodyStyle={bodyStyle}";
            vinSearchUrl = vinSearchUrl.Replace("{policyNo}", policyNo);
            vinSearchUrl = vinSearchUrl.Replace("{vin}", vin);
            vinSearchUrl = vinSearchUrl.Replace("{year}", year);
            vinSearchUrl = vinSearchUrl.Replace("{make}", make);
            vinSearchUrl = vinSearchUrl.Replace("{model}", model);
            vinSearchUrl = vinSearchUrl.Replace("{bodyStyle}", bodyStyle);
            Uri uri = new Uri(vinSearchUrl);
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(uri);
            request.ContentType = "application/json";
            request.Method = WebRequestMethods.Http.Get;

            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
            VeriskListSearchResult<VeriskVinSearchResultBody> result;
            try
            {
                result = jObject["result"].ToObject<VeriskListSearchResult<VeriskVinSearchResultBody>>();

            }
            catch
            {
                try
                {
                    result = jObject["vinSearchResult"].ToObject<VeriskListSearchResult<VeriskVinSearchResultBody>>();
                }
                catch
                {
                    return new VeriskVinSearchResultBody();
                }
            }
            VeriskVinSearchResultBody resultBody = result.Body?.FirstOrDefault();
            if (resultBody == null)
            {
                return new VeriskVinSearchResultBody();
            }

            return resultBody;
        }

        public static VeriskVehicleSearchResultBody SearchVehicle(string policyNo, string year, string make, string model = "")
        {
            if (string.IsNullOrEmpty(policyNo) || string.IsNullOrEmpty(year) || string.IsNullOrEmpty(make))
            {
                return new VeriskVehicleSearchResultBody();
            }

            string vinSearchUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") + "/verisk/vehicleSearch?policyNo={policyNo}&year={year}&make={make}&model={model}";
            vinSearchUrl = vinSearchUrl.Replace("{policyNo}", policyNo);
            vinSearchUrl = vinSearchUrl.Replace("{year}", year);
            vinSearchUrl = vinSearchUrl.Replace("{make}", make);
            vinSearchUrl = vinSearchUrl.Replace("{model}", model);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(vinSearchUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Get;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
            var result = jObject?.ToObject<VeriskSingleResult<VeriskVehicleSearchResultBody>>();
            
            if (result?.Body == null)
            {
                return new VeriskVehicleSearchResultBody();
            }

            return result.Body;
        }

        public static VeriskMvrSubmitRequestBody MvrSubmitRequest(string policyNo, string givenName, string surname, string dob,
            string dlNumber, string dlState)
        {
            if (string.IsNullOrEmpty(policyNo) || string.IsNullOrEmpty(givenName) || string.IsNullOrEmpty(surname) || string.IsNullOrEmpty(dob) ||
                string.IsNullOrEmpty(dlNumber) || string.IsNullOrEmpty(dlState))
            {
                return new VeriskMvrSubmitRequestBody();
            }

            string mvrSubmitUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                  "/verisk/mvr/submit?givenName={givenName}&surname={surname}&dob={dob}&dlNumber={dlNumber}&dlState={dlState}&policyNo={policyNo}";
            mvrSubmitUrl = mvrSubmitUrl.Replace("{policyNo}", policyNo);

            DateTime dateOfBirth = Convert.ToDateTime(dob);
            dlNumber = dlNumber.Replace("-", "");
            mvrSubmitUrl = mvrSubmitUrl.Replace("{givenName}", givenName);
            mvrSubmitUrl = mvrSubmitUrl.Replace("{surname}", surname);
            mvrSubmitUrl = mvrSubmitUrl.Replace("{dob}", dateOfBirth.ToString("yyyyMMdd"));
            mvrSubmitUrl = mvrSubmitUrl.Replace("{dlNumber}", dlNumber);
            mvrSubmitUrl = mvrSubmitUrl.Replace("{dlState}", dlState);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(mvrSubmitUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Get;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            if (string.IsNullOrEmpty(responseString))
            {
                return new VeriskMvrSubmitRequestBody();
            }
            JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
            if (jObject == null)
            {
                return new VeriskMvrSubmitRequestBody();
            }
            
            var result = jObject?.ToObject<VeriskSingleResult<VeriskMvrSubmitRequestBody>>();
            if (result?.Body == null)
            {
                return new VeriskMvrSubmitRequestBody();
            }

            return result.Body;
        }

        public static VeriskRetreiveMvrReportBody MvrRequestReport(string policyNo, string orderNumber)
        {
            if (string.IsNullOrEmpty(orderNumber))
            {
                return new VeriskRetreiveMvrReportBody();
            }

            string mvrSubmitUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                  "/verisk/mvr/report?orderNumber={orderNumber}&policyNo={policyNo}";
            mvrSubmitUrl = mvrSubmitUrl.Replace("{orderNumber}", orderNumber);
            if (string.IsNullOrEmpty(policyNo))
            {
                mvrSubmitUrl = mvrSubmitUrl.Replace("={policyNo}", string.Empty);

            }
            else
            {
                mvrSubmitUrl = mvrSubmitUrl.Replace("{policyNo}", policyNo);
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(mvrSubmitUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Get;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            VeriskSingleResult<VeriskRetreiveMvrReportBody> result = null;
            if (response.StatusCode.Equals(HttpStatusCode.Accepted))
            {
                // max 6 attemps
                int totalAttempts = 0;
                while (response.StatusCode != HttpStatusCode.OK && totalAttempts < 6)
                {
                    totalAttempts++;
                    response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        Stream stream = response.GetResponseStream();
                        StreamReader streamReader = new StreamReader(stream);
                        string responseString = streamReader.ReadToEnd();
                        JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
                        result = jObject?.ToObject<VeriskSingleResult<VeriskRetreiveMvrReportBody>>();
                    }
                    Thread.Sleep(4000);
                }
            }
            else
            {
                Stream stream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(stream);
                string responseString = streamReader.ReadToEnd();
                JObject jObject = (JObject) JsonConvert.DeserializeObject(responseString);
                result = jObject?.ToObject<VeriskSingleResult<VeriskRetreiveMvrReportBody>>();
            }

            if (result?.Body == null)
            {
                return new VeriskRetreiveMvrReportBody();
            }

            return result.Body;
        }

        public static VeriskAPlusCapWithClaimsBody AplusReportWithClaims(string policyNo, APlusReportModel data)
        {
            VeriskAPlusCapWithClaimsBody report = new VeriskAPlusCapWithClaimsBody();
            if (string.IsNullOrEmpty(policyNo) || data == null || !data.Drivers.Any())
            {
                return report;
            }
            string aPlusReportUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                  "/verisk/aplus/capWithClaims?policyNo={policyNo}";
            aPlusReportUrl = aPlusReportUrl.Replace("{policyNo}", policyNo);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(aPlusReportUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;
            dynamic dto = new
            {
                Drivers = new List<dynamic>(),
                Addresses = new List<dynamic>(),
                PhoneNumbers = new List<string>(),
                VINS = new List<string>(),
            };
            int count = 0;
            foreach (PolicyQuoteDrivers driver in data.Drivers)
            {
                count++;
                string dob = driver.DOB.ToString("yyyyMMdd");
                dynamic driverDto = new
                {
                    Sequence = count,
                    GivenName = driver.FirstName,
                    Surname = driver.LastName,
                    DOB = dob,
                    SSN = driver.SSN?.Replace("-", ""),
                    DLNumber = driver.LicenseNo?.Replace("-", ""),
                    DLState = driver.LicenseSt
                };
                dto.Drivers.Add(driverDto);
            }
            foreach (APlusAddressModel address in data.Addresses)
            {
                dynamic addressDto = new
                {
                    address.AddressType,
                    address.Street1,
                    address.Street2,
                    address.City,
                    address.StateCode,
                    address.Zip
                };
                dto.Addresses.Add(addressDto);
            }

            foreach (string vin in data.Vins)
            {
                dto.VINS.Add(vin);
            }

            foreach (string phoneNo in data.PhoneNumbers)
            {
                dto.PhoneNumbers.Add(phoneNo.Replace("-", ""));
            }

            //Add body
            var dtoContent = JsonConvert.SerializeObject(dto);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
            if (jObject != null)
            {
                var result = jObject?.ToObject<VeriskSingleResult<VeriskAPlusCapWithClaimsBody>>();
                if (result?.Body == null)
                {
                    return new VeriskAPlusCapWithClaimsBody();
                }
                report = result.Body;
            }
            else
            {
                return new VeriskAPlusCapWithClaimsBody();
            }
            return report;
        }

        public static CoverageVerifierResponseBody CoverageVerifierReport(string policyNo, CoverageVerifierRequest data)
        {
            CoverageVerifierResponseBody report = new CoverageVerifierResponseBody();
            if (string.IsNullOrEmpty(policyNo) || data == null || !data.Drivers.Any())
            {
                return report;
            }
            string cvReportUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                    "/verisk/cv/coverageVerifierReport?policyNo={policyNo}";
            cvReportUrl = cvReportUrl.Replace("{policyNo}", policyNo);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(cvReportUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;

            dynamic dto = new
            {
                Drivers = new List<dynamic>(),
                Addresses = new List<dynamic>(),
            };
            int count = 0;
            foreach (Driver driver in data.Drivers)
            {
                count++;
                dynamic driverDto = new
                {
                    Sequence = count,
                    driver.GivenName,
                    driver.Surname,
                    driver.DriverLicenseNumber,
                    driver.DriversLicenseState,
                    driver.DateOfBirth
                };
                dto.Drivers.Add(driverDto);
            }
            foreach (Address address in data.Addresses)
            {
                dynamic addressDto = new
                {
                    address.AddressType,
                    address.Street1,
                    address.City,
                    address.StateCode,
                    address.Zip
                };
                dto.Addresses.Add(addressDto);
            }

            //Add body
            var dtoContent = JsonConvert.SerializeObject(dto);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
            var result = jObject?.ToObject<VeriskSingleResult<CoverageVerifierResponseBody>>();

            if (result?.Body == null)
            {
                return new CoverageVerifierResponseBody();
            }

            report = result.Body;
            return report;
        }

        public static async Task<VeriskRiskCheckReportResultBody> RiskCheckReport(string policyNo, RiskCheckRquest data)
        {
            VeriskRiskCheckReportResultBody report = new VeriskRiskCheckReportResultBody();
            if (string.IsNullOrEmpty(policyNo) || data == null || !data.Drivers.Any())
            {
                return report;
            }

            string riskCheckUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                    "/verisk/riskCheck/reportWithVehicle?policyNo={policyNo}";
            riskCheckUrl = riskCheckUrl.Replace("{policyNo}", policyNo);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(riskCheckUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;

            dynamic dto = new
            {
                Drivers = new List<dynamic>(),
                Vehicles = new List<dynamic>(),
            };
            foreach (AlertAuto.Integrations.Contracts.Verisk.RiskCheck.Driver driver in data.Drivers)
            {
                dynamic driverDto = new
                {
                    driver.Sequence,
                    driver.GivenName,
                    driver.Surname,
                    driver.DOB,
                    driver.Addresses
                };
                dto.Drivers.Add(driverDto);
            }
            
            foreach (AlertAuto.Integrations.Contracts.Verisk.RiskCheck.Vehicle vehicle in data.Vehicles)
            {
                dynamic vehicleDto = new
                {
                    Sequence = vehicle.Sequence,
                    vehicle.VIN,
                };
                dto.Vehicles.Add(vehicleDto);
            }

            //Add body
            var dtoContent = JsonConvert.SerializeObject(dto);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            VeriskSingleResult<VeriskRiskCheckReportResultBody> result = null;
            if (!string.IsNullOrEmpty(responseString))
            {
                JObject jObject = (JObject) JsonConvert.DeserializeObject(responseString);
                result = jObject?.ToObject<VeriskSingleResult<VeriskRiskCheckReportResultBody>>();

                if (result?.Body == null)
                {
                    return new VeriskRiskCheckReportResultBody();
                }
            }
            else
            {
                return new VeriskRiskCheckReportResultBody();
            }

            report = result.Body;
            return (report);
        }

        public static void SendESignatureRequest(string policyNo, Dictionary<string, string> userEmailMap, IEnumerable<string> documentLocations,List<string> ccEmails=null, string requestType = "")
        {
            if (userEmailMap == null || !userEmailMap.Any() || string.IsNullOrEmpty(policyNo) || documentLocations == null || !documentLocations.Any())
            {
                return;
            }
            string sendESigRequestUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                 "/helloSign/signatureRequest?policyNo={policyNo}"; 
            sendESigRequestUrl = sendESigRequestUrl.Replace("{policyNo}", policyNo);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sendESigRequestUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;

            dynamic dto = new
            {
                userEmailMap,
                DocumentNames = new List<string>(),
                CCEmailIds = ccEmails,
                requestType
            };
            foreach (string documentLocation in documentLocations)
            {
                string documentName = Path.GetFileName(documentLocation);
                dto.DocumentNames.Add(documentName);

            }
            var dtoContent = JsonConvert.SerializeObject(dto);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        }

        public static string GetEsignatureDocumentDownloadUrl(string signatureRequestId)
        {
            if (string.IsNullOrEmpty(signatureRequestId))
            {
                return string.Empty;
            }

            string documentDownloadUrl = string.Empty;
            string getDocumentDownloadUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                        "/helloSign/getDocumentsDownloadUrl?signatureRequestId={signatureRequestId}";
            getDocumentDownloadUrl = getDocumentDownloadUrl.Replace("{signatureRequestId}", signatureRequestId);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(getDocumentDownloadUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Get;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            if (!string.IsNullOrEmpty(responseString))
            {
                documentDownloadUrl = responseString;
            }

            return documentDownloadUrl;
        }

        public static CardConnectResponse AuthorizeWithCardConnect(string policyNo, CardConnectRequest requestData, bool createNewProfile)
        {
            if (string.IsNullOrEmpty(policyNo) || requestData == null)
            {
                return null;
            }

            string createProfileString = "false";
            if (createNewProfile)
            {
                createProfileString = "true";
            }
            string authorizeCardUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                        $"/CardConnect/authorizeWithCapture?policyNo={policyNo}&createProfile={createProfileString}";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(authorizeCardUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;
            
            var dtoContent = JsonConvert.SerializeObject(requestData);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream stream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(stream);
                string responseString = streamReader.ReadToEnd();
                JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
                var result = jObject?.ToObject<CardConnectResponse>();
                return result;
            }

            return null;
        }

        public static string CreateCardConnectProfile(string policyNo, CardConnectRequest requestData)
        {
            if (requestData == null)
            {
                return null;
            }
         
            string createCardConnectProfileUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                        $"/CardConnect/createProfile?policyNo={policyNo}";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(createCardConnectProfileUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;

            var dtoContent = JsonConvert.SerializeObject(requestData);
            var buffer = Encoding.UTF8.GetBytes(dtoContent);
            request.ContentType = "application/json";
            request.ContentLength = buffer.Length;
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream stream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(stream);
                string responseString = streamReader.ReadToEnd();
                return responseString;
            }

            return null;
        }

        public static bool UpdateFlagForAllIntegrationCall(string pPolicyNo, bool pSetAllIntegrationCalFlagValue, bool pHasToSetRCPOSFlag = false, bool pSetRcposOrderedValue = false)
        {
            bool isAllIntegrationFlagSet = false;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                uow.BeginTransaction();
                XpoPolicyQuote polQuote = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", pPolicyNo));
                if (polQuote != null)
                {
                    if (!pHasToSetRCPOSFlag && polQuote.IsAllIntegrationCompleted == pSetAllIntegrationCalFlagValue)
                        return isAllIntegrationFlagSet = true;

                    polQuote.IsAllIntegrationCompleted = pSetAllIntegrationCalFlagValue;

                    if (pHasToSetRCPOSFlag && !(polQuote.RCPOSOrdered == pSetRcposOrderedValue))
                    {
                        polQuote.RCPOSOrdered = pSetRcposOrderedValue;
                    }

                    polQuote.Save();
                    uow.CommitChanges();
                    isAllIntegrationFlagSet = true;
                }
            }

            return isAllIntegrationFlagSet;
        }

        public static bool GetFlagForAllIntegrationCall(string pPolicyNo, ref bool pIsRCPOSOrdered)
        {
            bool getFlagvalue = false;

            try
            {
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    XpoPolicyQuote polQuote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", pPolicyNo)).FirstOrDefault();
                    if (polQuote != null)
                    {
                        getFlagvalue = polQuote.IsAllIntegrationCompleted;
                        pIsRCPOSOrdered = polQuote.RCPOSOrdered;
                    }
                }
            }
            catch(Exception ex)
            { return false;    }

            return getFlagvalue;
        }


        public static string GetDeepLink(string policyNo, bool existingUrlOnly= false)
        {
            if (string.IsNullOrEmpty(policyNo))
                return string.Empty;
            
            string aPlusReportUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                  $"/BranchIO/GetDeepLink?policyNo={policyNo}&existingUrlOnly={existingUrlOnly}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(aPlusReportUrl);
            request.ContentType = "application/json";
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Get;
           
         
            //Add body
            request.ContentType = "application/json";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(stream);
            string responseString = streamReader.ReadToEnd();
            JObject jObject = (JObject)JsonConvert.DeserializeObject(responseString);
            if (jObject != null)
            {
                var result = jObject?.ToObject<AlertAuto.Integrations.Contracts.BranchIO.CreateShortLinkResponse>();
                if (!string.IsNullOrEmpty(result?.Url))
                {
                   return result.Url;
                }
            }
            else if (existingUrlOnly)
            {
                return null;
            }
            throw new Exception("Unable to create deep link for policy " + policyNo);
        }

        public static bool SendUploadImagesDeepLink(string policyNo,string deepLink)
        {
            var deepLinkSent = false;
            if (string.IsNullOrEmpty(deepLink))
            {
                deepLink = GetDeepLink(policyNo);
            }

            if (string.IsNullOrEmpty(deepLink))
                return deepLinkSent;

            var toEmailAddrs = new List<string>();
            if (Sessions.Instance.IsAgent)
            {
                var agentEmailAddr = AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode);
                if (!string.IsNullOrEmpty(agentEmailAddr))
                    toEmailAddrs.Add(agentEmailAddr);
            }

            var insuredEmailAddr = PolicyWebUtils.GetPolicyInsuredEmail(policyNo);
            if (!string.IsNullOrEmpty(insuredEmailAddr))
                toEmailAddrs.Add(insuredEmailAddr);

            if (toEmailAddrs.Any())
            {
                EmailService emailService = new EmailService();
                deepLinkSent = emailService.SendEmailNotification(toEmailAddrs,
                    $"Upload vehicle Images for Policy {policyNo}.",
                    $"<p>Use the below link to upload vehicle images for the policy {policyNo}. \n <a href='{deepLink}'>Click Here </a></p>", true);
            }
            return deepLinkSent;
        }

        public static void GenerateNewPolicyDocuments(string policyNo, string historyIndex)
        {
            if (string.IsNullOrEmpty(policyNo) || string.IsNullOrEmpty(historyIndex))
            {
                return;
            }
            string sendESigRequestUrl = ConfigSettings.ReadSetting("BaseInternalIntegrationsApiUrl") +
                                 $"/document/generateNewPolicyDocuments?policyNo={policyNo}&historyIndex={historyIndex}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sendESigRequestUrl);
            string adminUsername = ConfigSettings.ReadSetting("AdminUsername");
            string internalApiPassword = ConfigSettings.ReadSetting("AlertAutoInternalApiPassword");
            string credentials = Convert.ToBase64String(
                Encoding.ASCII.GetBytes(adminUsername + ":" + internalApiPassword));
            request.Headers.Add("Authorization", "Basic " + credentials);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentLength = 0;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode != HttpStatusCode.OK && response.StatusCode != HttpStatusCode.Accepted)
            {
                // add logging message to warn.
            }
        }
    }
}