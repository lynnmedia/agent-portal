using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [DeferredDeletion(false)]
    public class PolicyRenQuoteDriversViolationsTemp : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        bool fFromMVR;
        public bool FromMVR
        {
            get { return fFromMVR; }
            set { SetPropertyValue<bool>("FromMVR", ref fFromMVR, value); }
        }
        public PolicyRenQuoteDriversViolationsTemp(Session session) : base(session) { }
        public PolicyRenQuoteDriversViolationsTemp() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
    public class PolicyRenQuoteDriversViolations : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        bool fFromMVR;
        public bool FromMVR
        {
            get { return fFromMVR; }
            set { SetPropertyValue<bool>("FromMVR", ref fFromMVR, value); }
        }
        public PolicyRenQuoteDriversViolations(Session session) : base(session) { }
        public PolicyRenQuoteDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyRenQuoteTemp : XPCustomObject
    {
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPriorPolicyNo;
        [Size(150)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fpolicyCars;
        public int policyCars
        {
            get { return fpolicyCars; }
            set { SetPropertyValue<int>("policyCars", ref fpolicyCars, value); }
        }
        int fpolicyDrivers;
        public int policyDrivers
        {
            get { return fpolicyDrivers; }
            set { SetPropertyValue<int>("policyDrivers", ref fpolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }
        string fAppQuestionAnswers;
        [Size(500)]
        public string AppQuestionAnswers
        {
            get { return fAppQuestionAnswers; }
            set { SetPropertyValue<string>("AppQuestionAnswers", ref fAppQuestionAnswers, value); }
        }
        string fAppQuestionExplainOne;
        [Size(8000)]
        public string AppQuestionExplainOne
        {
            get { return fAppQuestionExplainOne; }
            set { SetPropertyValue<string>("AppQuestionExplainOne", ref fAppQuestionExplainOne, value); }
        }
        string fAppQuestionExplainTwo;
        [Size(8000)]
        public string AppQuestionExplainTwo
        {
            get { return fAppQuestionExplainTwo; }
            set { SetPropertyValue<string>("AppQuestionExplainTwo", ref fAppQuestionExplainTwo, value); }
        }
        bool fFromPortal;
        public bool FromPortal
        {
            get { return fFromPortal; }
            set { SetPropertyValue<bool>("FromPortal", ref fFromPortal, value); }
        }
        string fFromRater;
        [Size(20)]
        public string FromRater
        {
            get { return fFromRater; }
            set { SetPropertyValue<string>("FromRater", ref fFromRater, value); }
        }
        string fQuoteNo;
        [Size(50)]
        public string QuoteNo
        {
            get { return fQuoteNo; }
            set { SetPropertyValue<string>("QuoteNo", ref fQuoteNo, value); }
        }
        string fAgencyRep;
        [Size(500)]
        public string AgencyRep
        {
            get { return fAgencyRep; }
            set { SetPropertyValue<string>("AgencyRep", ref fAgencyRep, value); }
        }
        DateTime fQuotedDate;
        public DateTime QuotedDate
        {
            get { return fQuotedDate; }
            set { SetPropertyValue<DateTime>("QuotedDate", ref fQuotedDate, value); }
        }
        bool fBridged;
        public bool Bridged
        {
            get { return fBridged; }
            set { SetPropertyValue<bool>("Bridged", ref fBridged, value); }
        }
        string fRatingReturnCode;
        [Size(50)]
        public string RatingReturnCode
        {
            get { return fRatingReturnCode; }
            set { SetPropertyValue<string>("RatingReturnCode", ref fRatingReturnCode, value); }
        }
        int fOldWorkLoss;
        public int OldWorkLoss
        {
            get { return fOldWorkLoss; }
            set { SetPropertyValue<int>("OldWorkLoss", ref fOldWorkLoss, value); }
        }
        DateTime fMinDuePayDate;
        public DateTime MinDuePayDate
        {
            get { return fMinDuePayDate; }
            set { SetPropertyValue<DateTime>("MinDuePayDate", ref fMinDuePayDate, value); }
        }
        /*
         * Until these columns exist in the database, do not use them
        double fMaintenanceFee;
        public double MaintenanceFee
        {
            get { return fMaintenanceFee; }
            set { SetPropertyValue<double>("MaintenanceFee", ref fMaintenanceFee, value); }
        }
        double fPIPPDOnlyFee;
        public double PIPPDOnlyFee
        {
            get { return fPIPPDOnlyFee; }
            set { SetPropertyValue<double>("PIPPDOnlyFee", ref fPIPPDOnlyFee, value); }
        }
        */
        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        public PolicyRenQuoteTemp(Session session) : base(session) { }
        public PolicyRenQuoteTemp() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyRenQuote : XPCustomObject
    {
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPriorPolicyNo;
        [Size(150)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fpolicyCars;
        public int policyCars
        {
            get { return fpolicyCars; }
            set { SetPropertyValue<int>("policyCars", ref fpolicyCars, value); }
        }
        int fpolicyDrivers;
        public int policyDrivers
        {
            get { return fpolicyDrivers; }
            set { SetPropertyValue<int>("policyDrivers", ref fpolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }
        string fAppQuestionAnswers;
        [Size(500)]
        public string AppQuestionAnswers
        {
            get { return fAppQuestionAnswers; }
            set { SetPropertyValue<string>("AppQuestionAnswers", ref fAppQuestionAnswers, value); }
        }
        string fAppQuestionExplainOne;
        [Size(8000)]
        public string AppQuestionExplainOne
        {
            get { return fAppQuestionExplainOne; }
            set { SetPropertyValue<string>("AppQuestionExplainOne", ref fAppQuestionExplainOne, value); }
        }
        string fAppQuestionExplainTwo;
        [Size(8000)]
        public string AppQuestionExplainTwo
        {
            get { return fAppQuestionExplainTwo; }
            set { SetPropertyValue<string>("AppQuestionExplainTwo", ref fAppQuestionExplainTwo, value); }
        }
        bool fFromPortal;
        public bool FromPortal
        {
            get { return fFromPortal; }
            set { SetPropertyValue<bool>("FromPortal", ref fFromPortal, value); }
        }
        string fFromRater;
        [Size(20)]
        public string FromRater
        {
            get { return fFromRater; }
            set { SetPropertyValue<string>("FromRater", ref fFromRater, value); }
        }
        string fQuoteNo;
        [Size(50)]
        public string QuoteNo
        {
            get { return fQuoteNo; }
            set { SetPropertyValue<string>("QuoteNo", ref fQuoteNo, value); }
        }
        string fAgencyRep;
        [Size(500)]
        public string AgencyRep
        {
            get { return fAgencyRep; }
            set { SetPropertyValue<string>("AgencyRep", ref fAgencyRep, value); }
        }
        DateTime fQuotedDate;
        public DateTime QuotedDate
        {
            get { return fQuotedDate; }
            set { SetPropertyValue<DateTime>("QuotedDate", ref fQuotedDate, value); }
        }
        bool fBridged;
        public bool Bridged
        {
            get { return fBridged; }
            set { SetPropertyValue<bool>("Bridged", ref fBridged, value); }
        }
        string fRatingReturnCode;
        [Size(50)]
        public string RatingReturnCode
        {
            get { return fRatingReturnCode; }
            set { SetPropertyValue<string>("RatingReturnCode", ref fRatingReturnCode, value); }
        }
        int fOldWorkLoss;
        public int OldWorkLoss
        {
            get { return fOldWorkLoss; }
            set { SetPropertyValue<int>("OldWorkLoss", ref fOldWorkLoss, value); }
        }
        DateTime fMinDuePayDate;
        public DateTime MinDuePayDate
        {
            get { return fMinDuePayDate; }
            set { SetPropertyValue<DateTime>("MinDuePayDate", ref fMinDuePayDate, value); }
        }
        double fMaintenanceFee;
        public double MaintenanceFee
        {
            get { return fMaintenanceFee; }
            set { SetPropertyValue<double>("MaintenanceFee", ref fMaintenanceFee, value); }
        }
        double fPIPPDOnlyFee;
        public double PIPPDOnlyFee
        {
            get { return fPIPPDOnlyFee; }
            set { SetPropertyValue<double>("PIPPDOnlyFee", ref fPIPPDOnlyFee, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        public PolicyRenQuote(Session session) : base(session) { }
        public PolicyRenQuote() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyRenQuoteDriversTemp : XPCustomObject
       {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(500)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        string fDriverMsg;
        [Size(200)]
        public string DriverMsg
        {
            get { return fDriverMsg; }
            set { SetPropertyValue<string>("DriverMsg", ref fDriverMsg, value); }
        }
        bool fChargeMVR;
        public bool ChargeMVR
        {
            get { return fChargeMVR; }
            set { SetPropertyValue<bool>("ChargeMVR", ref fChargeMVR, value); }
        }
        bool fIsExpiredLicense;
        public bool IsExpiredLicense
        {
            get { return fIsExpiredLicense; }
            set { SetPropertyValue<bool>("IsExpiredLicense", ref fIsExpiredLicense, value); }
        }
        public PolicyRenQuoteDriversTemp(Session session) : base(session) { }
        public PolicyRenQuoteDriversTemp() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyRenQuoteDrivers : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(500)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        string fDriverMsg;
        [Size(200)]
        public string DriverMsg
        {
            get { return fDriverMsg; }
            set { SetPropertyValue<string>("DriverMsg", ref fDriverMsg, value); }
        }
        bool fChargeMVR;
        public bool ChargeMVR
        {
            get { return fChargeMVR; }
            set { SetPropertyValue<bool>("ChargeMVR", ref fChargeMVR, value); }
        }
        bool fIsExpiredLicense;
        public bool IsExpiredLicense
        {
            get { return fIsExpiredLicense; }
            set { SetPropertyValue<bool>("IsExpiredLicense", ref fIsExpiredLicense, value); }
        }
        public PolicyRenQuoteDrivers(Session session) : base(session) { }
        public PolicyRenQuoteDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyRenQuoteCarsTemp : XPCustomObject
    {
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(200)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(200)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public PolicyRenQuoteCarsTemp(Session session) : base(session) { }
        public PolicyRenQuoteCarsTemp() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
    
    public class PolicyRenQuoteCars : XPCustomObject
    {
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(200)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(200)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public PolicyRenQuoteCars(Session session) : base(session) { }
        public PolicyRenQuoteCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
