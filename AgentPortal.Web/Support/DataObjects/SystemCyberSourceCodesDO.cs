using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class SystemCyberSourceCodes : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fResponseCode;
        [Size(200)]
        public string ResponseCode
        {
            get { return fResponseCode; }
            set { SetPropertyValue<string>("ResponseCode", ref fResponseCode, value); }
        }
        string fResponseDesc;
        [Size(900)]
        public string ResponseDesc
        {
            get { return fResponseDesc; }
            set { SetPropertyValue<string>("ResponseDesc", ref fResponseDesc, value); }
        }
        string fActionType;
        [Size(900)]
        public string ActionType
        {
            get { return fActionType; }
            set { SetPropertyValue<string>("ActionType", ref fActionType, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        public SystemCyberSourceCodes(Session session) : base(session) { }
        public SystemCyberSourceCodes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
