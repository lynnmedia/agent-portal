﻿using DevExpress.Xpo;
using System;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditMinimumDueEndorsement
    {
        int IndexID { get; set; }
        string PolicyNo { get; set; }
        DateTime EndorsementDate { get; set; }
        decimal FullAmountOfEndorsement { get; set; }
        string Username { get; set; }
        DateTime EffDate { get; set; }
        DateTime ExpDate { get; set; }
        int PayPlan { get; set; }
        double DP_AsPerc { get; set; }
        int DaysPolicyRemaining { get; set; }
        DateTime DueDateNextInstallment { get; set; }
        int? DaysUntilNextPaymentDue { get; set; }
        decimal MinimumAmountDue { get; set; }
    }

    public class AuditMinimumDueEndorsement : IAuditMinimumDueEndorsement
    {
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public DateTime EndorsementDate { get; set; }
        public decimal FullAmountOfEndorsement { get; set; }
        public string Username { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public int PayPlan { get; set; }
        public double DP_AsPerc { get; set; }
        public int DaysPolicyRemaining { get; set; }
        public DateTime DueDateNextInstallment { get; set; }
        public int? DaysUntilNextPaymentDue { get; set; }
        public decimal MinimumAmountDue { get; set; }
    }

    [NullableBehavior(NullableBehavior.ByUnderlyingType)]
    [Persistent("AuditMinimumDueEndorsement")]
    public class XpoAuditMinimumDueEndorsement : XPLiteObject, IAuditMinimumDueEndorsement
    {
		int fIndexID;
		[Key(true)]
		public int IndexID
		{
			get { return fIndexID; }
			set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
		}
		string fPolicyNo;
		[Size(50)]
		public string PolicyNo
		{
			get { return fPolicyNo; }
			set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
		}
		DateTime fEndorsementDate;
		public DateTime EndorsementDate
        {
			get { return fEndorsementDate; }
			set { SetPropertyValue<DateTime>("EndorsementDate", ref fEndorsementDate, value); }
        }
		decimal fFullAmountOfEndorsement;
		public decimal FullAmountOfEndorsement
        {
			get { return fFullAmountOfEndorsement; }
			set { SetPropertyValue<decimal>("FullAmountOfEndorsement", ref fFullAmountOfEndorsement, value); }
        }
		string fUsername;
		[Size(50)]
		public string Username
		{
			get { return fUsername; }
			set { SetPropertyValue<string>("Username", ref fUsername, value); }
		}
		DateTime fEffDate;
		public DateTime EffDate
		{
			get { return fEffDate; }
			set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
		}
		DateTime fExpDate;
		public DateTime ExpDate
		{
			get { return fExpDate; }
			set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
		}
		int fPayPlan;
		public int PayPlan
        {
			get { return fPayPlan; }
			set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
		double fDP_AsPerc;
		public double DP_AsPerc
        {
			get { return fDP_AsPerc; }
			set { SetPropertyValue<double>("DP_AsPerc", ref fDP_AsPerc, value); }
        }
		int fDaysPolicyRemaining;
		public int DaysPolicyRemaining
        {
			get { return fDaysPolicyRemaining; }
			set { SetPropertyValue<int>("DaysPolicyRemaining", ref fDaysPolicyRemaining, value); }
        }
		double fEndorsementRunRate;
		public double EndorsementRunRate
        {
			get { return fEndorsementRunRate; }
			set { SetPropertyValue<double>("EndorsementRunRate", ref fEndorsementRunRate, value); }
        }
		DateTime fDueDateNextInstallment;
		public DateTime DueDateNextInstallment
        {
			get { return fDueDateNextInstallment; }
			set { SetPropertyValue<DateTime>("DueDateNextInstallment", ref fDueDateNextInstallment, value); }
        }
		int? fDaysUntilNextPaymentDue;
		public int? DaysUntilNextPaymentDue
        {
            get { return fDaysUntilNextPaymentDue; }
			set { SetPropertyValue("DaysUntilNextPaymentDue", ref fDaysUntilNextPaymentDue, value); }
        }
		decimal fMinimumAmountDue;
		public decimal MinimumAmountDue
        {
            get { return fMinimumAmountDue; }
			set { SetPropertyValue<decimal>("MinimumAmountDue", ref fMinimumAmountDue, value); }
        }
		public XpoAuditMinimumDueEndorsement(Session session) : base(session) { }
		public XpoAuditMinimumDueEndorsement() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
        
    }
}
