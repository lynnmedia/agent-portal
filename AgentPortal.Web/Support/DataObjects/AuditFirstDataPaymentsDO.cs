using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditFirstDataPaymentsDo
    {
        int IndexID { get; set; }
        string ApprovalCode { get; set; }
        string AVSResponse { get; set; }
        string CommercialServiceProvider { get; set; }
        string ErrorMessage { get; set; }
        string OrderId { get; set; }
        string ProcessorApprovalCode { get; set; }
        string ProcessorReferenceNumber { get; set; }
        string ProcessorResponseCode { get; set; }
        string ProcessorResponseMessage { get; set; }
        string TDate { get; set; }
        string TransactionResult { get; set; }
        string TransactionTime { get; set; }
        string TransactionID { get; set; }
        string CalculatedTax { get; set; }
        string CalculatedShipping { get; set; }
        string TransactionScore { get; set; }
        string AuthenticationResponseCode { get; set; }
        string FraudAction { get; set; }
        string LastFourOfCard { get; set; }
        string NameOnCard { get; set; }
        DateTime DateCreated { get; set; }
        string PolicyNo { get; set; }
    }

    public class AuditFirstDataPaymentsDo : IAuditFirstDataPaymentsDo
    {
        public int IndexID { get; set; }
        public string ApprovalCode { get; set; }
        public string AVSResponse { get; set; }
        public string CommercialServiceProvider { get; set; }
        public string ErrorMessage { get; set; }
        public string OrderId { get; set; }
        public string ProcessorApprovalCode { get; set; }
        public string ProcessorReferenceNumber { get; set; }
        public string ProcessorResponseCode { get; set; }
        public string ProcessorResponseMessage { get; set; }
        public string TDate { get; set; }
        public string TransactionResult { get; set; }
        public string TransactionTime { get; set; }
        public string TransactionID { get; set; }
        public string CalculatedTax { get; set; }
        public string CalculatedShipping { get; set; }
        public string TransactionScore { get; set; }
        public string AuthenticationResponseCode { get; set; }
        public string FraudAction { get; set; }
        public string LastFourOfCard { get; set; }
        public string NameOnCard { get; set; }
        public DateTime DateCreated { get; set; }
        public string PolicyNo { get; set; }
    }

    [Persistent("AuditFirstDataPayments")]
    public class XpoAuditFirstDataPaymentsDo : XPLiteObject, IAuditFirstDataPaymentsDo
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fApprovalCode;
        [Size(500)]
        public string ApprovalCode
        {
            get { return fApprovalCode; }
            set { SetPropertyValue<string>("ApprovalCode", ref fApprovalCode, value); }
        }
        string fAVSResponse;
        [Size(500)]
        public string AVSResponse
        {
            get { return fAVSResponse; }
            set { SetPropertyValue<string>("AVSResponse", ref fAVSResponse, value); }
        }
        string fCommercialServiceProvider;
        [Size(500)]
        public string CommercialServiceProvider
        {
            get { return fCommercialServiceProvider; }
            set { SetPropertyValue<string>("CommercialServiceProvider", ref fCommercialServiceProvider, value); }
        }
        string fErrorMessage;
        [Size(500)]
        public string ErrorMessage
        {
            get { return fErrorMessage; }
            set { SetPropertyValue<string>("ErrorMessage", ref fErrorMessage, value); }
        }
        string fOrderId;
        [Size(105)]
        public string OrderId
        {
            get { return fOrderId; }
            set { SetPropertyValue<string>("OrderId", ref fOrderId, value); }
        }
        string fProcessorApprovalCode;
        [Size(500)]
        public string ProcessorApprovalCode
        {
            get { return fProcessorApprovalCode; }
            set { SetPropertyValue<string>("ProcessorApprovalCode", ref fProcessorApprovalCode, value); }
        }
        string fProcessorReferenceNumber;
        [Size(500)]
        public string ProcessorReferenceNumber
        {
            get { return fProcessorReferenceNumber; }
            set { SetPropertyValue<string>("ProcessorReferenceNumber", ref fProcessorReferenceNumber, value); }
        }
        string fProcessorResponseCode;
        [Size(500)]
        public string ProcessorResponseCode
        {
            get { return fProcessorResponseCode; }
            set { SetPropertyValue<string>("ProcessorResponseCode", ref fProcessorResponseCode, value); }
        }
        string fProcessorResponseMessage;
        [Size(500)]
        public string ProcessorResponseMessage
        {
            get { return fProcessorResponseMessage; }
            set { SetPropertyValue<string>("ProcessorResponseMessage", ref fProcessorResponseMessage, value); }
        }
        string fTDate;
        [Size(500)]
        public string TDate
        {
            get { return fTDate; }
            set { SetPropertyValue<string>("TDate", ref fTDate, value); }
        }
        string fTransactionResult;
        [Size(500)]
        public string TransactionResult
        {
            get { return fTransactionResult; }
            set { SetPropertyValue<string>("TransactionResult", ref fTransactionResult, value); }
        }
        string fTransactionTime;
        [Size(500)]
        public string TransactionTime
        {
            get { return fTransactionTime; }
            set { SetPropertyValue<string>("TransactionTime", ref fTransactionTime, value); }
        }
        string fTransactionID;
        [Size(500)]
        public string TransactionID
        {
            get { return fTransactionID; }
            set { SetPropertyValue<string>("TransactionID", ref fTransactionID, value); }
        }
        string fCalculatedTax;
        [Size(500)]
        public string CalculatedTax
        {
            get { return fCalculatedTax; }
            set { SetPropertyValue<string>("CalculatedTax", ref fCalculatedTax, value); }
        }
        string fCalculatedShipping;
        [Size(500)]
        public string CalculatedShipping
        {
            get { return fCalculatedShipping; }
            set { SetPropertyValue<string>("CalculatedShipping", ref fCalculatedShipping, value); }
        }
        string fTransactionScore;
        [Size(500)]
        public string TransactionScore
        {
            get { return fTransactionScore; }
            set { SetPropertyValue<string>("TransactionScore", ref fTransactionScore, value); }
        }
        string fAuthenticationResponseCode;
        [Size(500)]
        public string AuthenticationResponseCode
        {
            get { return fAuthenticationResponseCode; }
            set { SetPropertyValue<string>("AuthenticationResponseCode", ref fAuthenticationResponseCode, value); }
        }
        string fFraudAction;
        [Size(500)]
        public string FraudAction
        {
            get { return fFraudAction; }
            set { SetPropertyValue<string>("FraudAction", ref fFraudAction, value); }
        }
        string fLastFourOfCard;
        [Size(4)]
        public string LastFourOfCard
        {
            get { return fLastFourOfCard; }
            set { SetPropertyValue<string>("LastFourOfCard", ref fLastFourOfCard, value); }
        }
        string fNameOnCard;
        public string NameOnCard
        {
            get { return fNameOnCard; }
            set { SetPropertyValue<string>("NameOnCard", ref fNameOnCard, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }

        private string _fPolicyNo;
        public string PolicyNo
        {
            get { return _fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref _fPolicyNo, value); }
        }
        public XpoAuditFirstDataPaymentsDo(Session session) : base(session) { }
        public XpoAuditFirstDataPaymentsDo() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
