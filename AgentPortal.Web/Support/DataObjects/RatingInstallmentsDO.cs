using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("RatingInstallments")]
    public class RatingInstallmentsDO : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        Guid fGUID;
        public Guid GUID
        {
            get { return fGUID; }
            set { SetPropertyValue<Guid>("GUID", ref fGUID, value); }
        }
        int fMainIndexID;
        public int MainIndexID
        {
            get { return fMainIndexID; }
            set { SetPropertyValue<int>("MainIndexID", ref fMainIndexID, value); }
        }
        decimal fInstallNo;
        public decimal InstallNo
        {
            get { return fInstallNo; }
            set { SetPropertyValue<decimal>("InstallNo", ref fInstallNo, value); }
        }
        string fDescr;
        [Size(40)]
        public string Descr
        {
            get { return fDescr; }
            set { SetPropertyValue<string>("Descr", ref fDescr, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        DateTime fDueDate;
        public DateTime DueDate
        {
            get { return fDueDate; }
            set { SetPropertyValue<DateTime>("DueDate", ref fDueDate, value); }
        }
        decimal fPolicyWritten;
        public decimal PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<decimal>("PolicyWritten", ref fPolicyWritten, value); }
        }
        decimal fInstallmentFee;
        public decimal InstallmentFee
        {
            get { return fInstallmentFee; }
            set { SetPropertyValue<decimal>("InstallmentFee", ref fInstallmentFee, value); }
        }
        decimal fPolicyFee;
        public decimal PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<decimal>("PolicyFee", ref fPolicyFee, value); }
        }
        decimal fSR22Fee;
        public decimal SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<decimal>("SR22Fee", ref fSR22Fee, value); }
        }
        decimal fFHCF_Fee;
        public decimal FHCF_Fee
        {
            get { return fFHCF_Fee; }
            set { SetPropertyValue<decimal>("FHCF_Fee", ref fFHCF_Fee, value); }
        }
        decimal fDBSetupFee;
        public decimal DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<decimal>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        decimal fLateFee;
        public decimal LateFee
        {
            get { return fLateFee; }
            set { SetPropertyValue<decimal>("LateFee", ref fLateFee, value); }
        }
        decimal fFeeTotal;
        public decimal FeeTotal
        {
            get { return fFeeTotal; }
            set { SetPropertyValue<decimal>("FeeTotal", ref fFeeTotal, value); }
        }
        decimal fDownPymtPercent;
        public decimal DownPymtPercent
        {
            get { return fDownPymtPercent; }
            set { SetPropertyValue<decimal>("DownPymtPercent", ref fDownPymtPercent, value); }
        }
        decimal fDownPymtAmount;
        public decimal DownPymtAmount
        {
            get { return fDownPymtAmount; }
            set { SetPropertyValue<decimal>("DownPymtAmount", ref fDownPymtAmount, value); }
        }
        decimal fInstallmentPymt;
        public decimal InstallmentPymt
        {
            get { return fInstallmentPymt; }
            set { SetPropertyValue<decimal>("InstallmentPymt", ref fInstallmentPymt, value); }
        }
        decimal fPremiumBalance;
        public decimal PremiumBalance
        {
            get { return fPremiumBalance; }
            set { SetPropertyValue<decimal>("PremiumBalance", ref fPremiumBalance, value); }
        }
        decimal fPymtApplied;
        public decimal PymtApplied
        {
            get { return fPymtApplied; }
            set { SetPropertyValue<decimal>("PymtApplied", ref fPymtApplied, value); }
        }
        decimal fBalanceInstallment;
        public decimal BalanceInstallment
        {
            get { return fBalanceInstallment; }
            set { SetPropertyValue<decimal>("BalanceInstallment", ref fBalanceInstallment, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        decimal fMaintenanceFee;
        public decimal MaintenanceFee
        {
            get { return fMaintenanceFee; }
            set { SetPropertyValue<decimal>("MaintenanceFee", ref fMaintenanceFee, value); }
        }
        decimal fPIPPDOnlyFee;
        public decimal PIPPDOnlyFee
        {
            get { return fPIPPDOnlyFee; }
            set { SetPropertyValue<decimal>("PIPPDOnlyFee", ref fPIPPDOnlyFee, value); }
        }

        public RatingInstallmentsDO(Session session) : base(session) { }
        public RatingInstallmentsDO() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
