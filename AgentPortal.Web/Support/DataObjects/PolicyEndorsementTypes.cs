﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyEndorsementTypes : XPLiteObject
    {
        int id;
        [Key(true)]
        public int Id
        {
            get { return id; }
            set { SetPropertyValue<int>(nameof(Id), ref id, value); }
        }

        string policyNo;
        public string PolicyNo
        {
            get { return policyNo; }
            set { SetPropertyValue<string>(nameof(PolicyNo), ref policyNo, value); }
        }

        string endorsementType;
        public string EndorsementType
        {
            get { return endorsementType; }
            set { SetPropertyValue<string>(nameof(EndorsementType), ref endorsementType, value); }
        }

        public PolicyEndorsementTypes(Session session) : base(session) { }
        public PolicyEndorsementTypes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}