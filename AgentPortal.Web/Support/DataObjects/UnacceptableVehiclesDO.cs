using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("UnacceptableVehicles")]
    public class UnacceptableVehiclesDO : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fMake;
        [Size(15)]
        public string Make
        {
            get { return fMake; }
            set { SetPropertyValue<string>("Make", ref fMake, value); }
        }
        string fModel;
        [Size(10)]
        public string Model
        {
            get { return fModel; }
            set { SetPropertyValue<string>("Model", ref fModel, value); }
        }
        string fBodyStyle;
        [Size(8)]
        public string BodyStyle
        {
            get { return fBodyStyle; }
            set { SetPropertyValue<string>("BodyStyle", ref fBodyStyle, value); }
        }
        string fIsoCode;
        public string ISOCode
        {
            get { return fIsoCode; }
            set { SetPropertyValue<string>("ISOCode", ref fIsoCode, value); }
        }

        public UnacceptableVehiclesDO(Session session) : base(session) { }
        public UnacceptableVehiclesDO() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
