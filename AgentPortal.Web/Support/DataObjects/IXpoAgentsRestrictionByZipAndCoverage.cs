﻿using System;

namespace AgentPortal.Support.DataObjects
{
    public partial interface IXpoAgentsRestrictionByZipAndCoverage
    {
        int IndexID { get; set; }
        string AgentCode { get; set; }
        string Zip { get; set; }
        string County { get; set; }
        string Territory { get; set; }
        string RestrictedCoverageName { get; set; }
        bool IsRestricted { get; set; }
        DateTime DateAdded { get; set; }
    }
}