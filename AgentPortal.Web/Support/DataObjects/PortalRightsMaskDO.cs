﻿using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PortalRightsMask : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fAgentCode;
        [Size(500)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        string fRights;
        [Size(8000)]
        public string Rights
        {
            get { return fRights; }
            set { SetPropertyValue<string>("Rights", ref fRights, value); }
        }
        public PortalRightsMask(Session session) : base(session) { }
        public PortalRightsMask() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}