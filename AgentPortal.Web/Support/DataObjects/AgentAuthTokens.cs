﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAgentAuthTokens
    {
        int Id { get; set; }
        string PolicyNo { get; set; }
        string AgentCode { get; set; }
        Guid Token { get; set; }
        DateTime ExpirationDate { get; set; }
    }

    public class AgentAuthTokens : IAgentAuthTokens
    {
        public int Id { get; set; }
        public string PolicyNo { get; set; }
        public string AgentCode { get; set; }
        public Guid Token { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
	
	[Persistent("AgentAuthTokens")]
    public class XpoAgentAuthTokens : XPLiteObject, IAgentAuthTokens
    {
        public XpoAgentAuthTokens()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public XpoAgentAuthTokens(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        int fId;
        [Key(true)]
        public int Id
        {
            get { return fId; }
            set { SetPropertyValue<int>(nameof(Id), ref fId, value); }
        }

        string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        string fAgentCode;
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }

        Guid fToken;
        public Guid Token
        {
            get { return fToken; }
            set { SetPropertyValue<Guid>("Token", ref fToken, value); }
        }

        DateTime fExpirationDate;
        public DateTime ExpirationDate
        {
            get { return fExpirationDate; }
            set { SetPropertyValue<DateTime>("ExpirationDate", ref fExpirationDate, value); }
        }
    }
}