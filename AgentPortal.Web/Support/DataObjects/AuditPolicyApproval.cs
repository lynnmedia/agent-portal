using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditPolicyApprovalDo
    {
        string PolicyNo { get; set; }
        string QuoteNo { get; set; }
        DateTime DateBound { get; set; }
        string AgentCode { get; set; }
        DateTime DateApproved { get; set; }
        string ApprovalUserName { get; set; }
        int IndexID { get; set; }
    }

    public class AuditPolicyApprovalDo : IAuditPolicyApprovalDo
    {
        public string PolicyNo { get; set; }
        public string QuoteNo { get; set; }
        public DateTime DateBound { get; set; }
        public string AgentCode { get; set; }
        public DateTime DateApproved { get; set; }
        public string ApprovalUserName { get; set; }
        public int IndexID { get; set; }
    }

    [Persistent("AuditPolicyApproval")]
    public class XpoAuditPolicyApprovalDo : XPLiteObject, IAuditPolicyApprovalDo
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fQuoteNo;
        [Size(20)]
        public string QuoteNo
        {
            get { return fQuoteNo; }
            set { SetPropertyValue<string>("QuoteNo", ref fQuoteNo, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        string fAgentCode;
        [Size(20)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        DateTime fDateApproved;
        public DateTime DateApproved
        {
            get { return fDateApproved; }
            set { SetPropertyValue<DateTime>("DateApproved", ref fDateApproved, value); }
        }
        string fApprovalUserName;
        [Size(30)]
        public string ApprovalUserName
        {
            get { return fApprovalUserName; }
            set { SetPropertyValue<string>("ApprovalUserName", ref fApprovalUserName, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public XpoAuditPolicyApprovalDo(Session session) : base(session) { }
        public XpoAuditPolicyApprovalDo() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditRewritePolicyReasons: XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        string fReason;
        [Size(20)]
        public string Reason
        {
            get { return fReason; }
            set { SetPropertyValue<string>("Reason", ref fReason, value); }
        }

        bool fIsNote;
        [Size(20)]
        public bool IsNote
        {
            get { return fIsNote; }
            set { SetPropertyValue<bool>("IsNote", ref fIsNote, value); }
        }

        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public AuditRewritePolicyReasons(Session session) : base(session) { }
        public AuditRewritePolicyReasons() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
