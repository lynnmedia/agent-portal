﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AlertAuto.Integrations.Contracts.HelloSign;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicySignature : XPLiteObject
    {

        private Guid id;
        private string policyNo;
        private string signatureRequestId;
        private string emailAddress;
        private DateTime recordedDate;
        private SignatureStatus status;
        private string requestType; 

        public PolicySignature(string policyNo, string signatureRequestId, string emailAddress)
        {
            this.id = Guid.NewGuid();
            this.policyNo = policyNo;
            this.signatureRequestId = signatureRequestId;
            this.emailAddress = emailAddress;
            this.recordedDate = DateTime.Now;
            this.status = SignatureStatus.Unsigned;
            this.requestType = "";
        }

        [Persistent]
        [Key]
        public Guid Id
        {
            get { return id; }
            set { SetPropertyValue<Guid>("Id", ref id, value); }
        }

        public string SignatureRequestId
        {
            get { return signatureRequestId; }
            set
            {
                signatureRequestId = value;
            }
        }

        public string EmailAddress
        {
            get { return emailAddress; }
            set
            {
                emailAddress = value;
            }
        }

        public DateTime RecordedDate
        {
            get { return recordedDate; }
            set { recordedDate = value; }
        }

        public SignatureStatus Status
        {
            get { return status; }
            set
            {
                status = value;
            }
        }

        public string PolicyNo
        {
            get { return policyNo; }
            set
            {
                policyNo = value;
            }
        }

        public string RequestType
        {
            get { return requestType; }
            set
            {
                requestType = value;
            }
        }

        public PolicySignature(Session session) : base(session) { }
        public PolicySignature() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}