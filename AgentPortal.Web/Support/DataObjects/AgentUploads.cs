using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAgentUploads
    {
        int IndexID { get; set; }
        string RecordKey { get; set; }
        string UploadFileName { get; set; }
        string RenamedFileName { get; set; }
        string ImagingFileName { get; set; }
        DateTime DateCreated { get; set; }
        string FullPath { get; set; }
        DateTime DateStarted { get; set; }
        DateTime DateCompleted { get; set; }
        bool IsProcessed { get; set; }
        string ErrMsg { get; set; }
        string DocType { get; set; }
        string Username { get; set; }
    }

    public class AgentUploads : IAgentUploads
    {
        public int IndexID { get; set; }
        public string RecordKey { get; set; }
        public string UploadFileName { get; set; }
        public string RenamedFileName { get; set; }
        public string ImagingFileName { get; set; }
        public DateTime DateCreated { get; set; }
        public string FullPath { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime DateCompleted { get; set; }
        public bool IsProcessed { get; set; }
        public string ErrMsg { get; set; }
        public string DocType { get; set; }
        public string Username { get; set; }
    }

	[Persistent("AgentUploads")]
    public class XpoAgentUploads : XPLiteObject, IAgentUploads
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fRecordKey;
        [Size(75)]
        public string RecordKey
        {
            get { return fRecordKey; }
            set { SetPropertyValue<string>("RecordKey", ref fRecordKey, value); }
        }
        string fUploadFileName;
        [Size(1000)]
        public string UploadFileName
        {
            get { return fUploadFileName; }
            set { SetPropertyValue<string>("UploadFileName", ref fUploadFileName, value); }
        }
        string fRenamedFileName;
        [Size(1000)]
        public string RenamedFileName
        {
            get { return fRenamedFileName; }
            set { SetPropertyValue<string>("RenamedFileName", ref fRenamedFileName, value); }
        }
        string fImagingFileName;
        [Size(1000)]
        public string ImagingFileName
        {
            get { return fImagingFileName; }
            set { SetPropertyValue<string>("ImagingFileName", ref fImagingFileName, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        string fFullPath;
        public string FullPath
        {
            get { return fFullPath; }
            set { SetPropertyValue<string>("FullPath", ref fFullPath, value); }
        }
        DateTime fDateStarted;
        public DateTime DateStarted
        {
            get { return fDateStarted; }
            set { SetPropertyValue<DateTime>("DateStarted", ref fDateStarted, value); }
        }
        DateTime fDateCompleted;
        public DateTime DateCompleted
        {
            get { return fDateCompleted; }
            set { SetPropertyValue<DateTime>("DateCompleted", ref fDateCompleted, value); }
        }

        bool fIsProcessed;
        public bool IsProcessed
        {
            get { return fIsProcessed; }
            set { SetPropertyValue<bool>("IsProcessed", ref fIsProcessed, value); }
        }
        string fErrMsg;
        public string ErrMsg
        {
            get { return fErrMsg; }
            set { SetPropertyValue<string>("ErrMsg", ref fErrMsg, value); }
        }
        string fDocType;
        public string DocType
        {
            get { return fDocType; }
            set { SetPropertyValue<string>("DocType", ref fDocType, value); }
        }
        string fUsername;
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }

        
        public XpoAgentUploads(Session session) : base(session) { }
        public XpoAgentUploads() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
