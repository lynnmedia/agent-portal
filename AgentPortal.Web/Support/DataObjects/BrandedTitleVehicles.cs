﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;
//using DevExpress.XtraCharts.Native;

namespace AgentPortal.Support.DataObjects
{
    public interface IBrandedTitleVehicles
    {
        int IndexID { get; set; }
        string PolicyNo { get; set; }
        int VehicleNbr { get; set; }
        string Make { get; set; }
        string Model { get; set; }
        string VehYear { get; set; }
        string VehTypeCode { get; set; }
        string VehTypeDesc { get; set; }
        string VIN { get; set; }
        string LicensePlateNumber { get; set; }
        string BrandedTitleState1 { get; set; }
        string BrandedTitle1Code { get; set; }
        string BrandedTitle1Description { get; set; }
        DateTime BrandedTitleDate1 { get; set; }
        DateTime DateCreated { get; set; }
        string CreatedAgentCode { get; set; }
    }

    public class BrandedTitleVehicles : IBrandedTitleVehicles
    {
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public int VehicleNbr { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string VehYear { get; set; }
        public string VehTypeCode { get; set; }
        public string VehTypeDesc { get; set; }
        public string VIN { get; set; }
        public string LicensePlateNumber { get; set; }
        public string BrandedTitleState1 { get; set; }
        public string BrandedTitle1Code { get; set; }
        public string BrandedTitle1Description { get; set; }
        public DateTime BrandedTitleDate1 { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedAgentCode { get; set; }
    }

    [Persistent("BrandedTitleVehicles")]
    public class XpoBrandedTitleVehicles : XPCustomObject, IBrandedTitleVehicles
    {
        public XpoBrandedTitleVehicles(Session session) : base(session) { }
        public XpoBrandedTitleVehicles() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }

        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }

        }

        int fVehicleNbr;
        public int VehicleNbr
        {
            get { return fVehicleNbr; }
            set { SetPropertyValue<int>("VehicleNbr", ref fVehicleNbr, value); }
        }

        string fMake;
        [Size(20)]
        public string Make
        {
            get { return fMake; }
            set { SetPropertyValue<string>("Make", ref fMake, value); }
        }

        string fModel;
        [Size(20)]
        public string Model
        {
            get { return fModel; }
            set { SetPropertyValue<string>("Model", ref fModel, value); }
        }

        string fVehYear;
        [Size(4)]
        public string VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<string>("VehYear", ref fVehYear, value); }
        }

        string fVehTypeCode;
        [Size(2)]
        public string VehTypeCode
        {
            get { return fVehTypeCode; }
            set { SetPropertyValue<string>("VehTypeCode", ref fVehTypeCode, value); }
        }

        string fVehTypeDesc;
        [Size(100)]
        public string VehTypeDesc
        {
            get { return fVehTypeDesc; }
            set { SetPropertyValue<string>("VehTypeDesc", ref fVehTypeDesc, value); }
        }

        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }

        string fLicensePlateNumber;
        [Size(10)]
        public string LicensePlateNumber
        {
            get { return fLicensePlateNumber; }
            set { SetPropertyValue<string>("LicensePlateNumber", ref fLicensePlateNumber, value); }
        }

        string fBrandedTitleState1;
        [Size(2)]
        public string BrandedTitleState1
        {
            get { return fBrandedTitleState1; }
            set { SetPropertyValue<string>("BrandedTitleState1", ref fBrandedTitleState1, value); }
        }

        string fBrandedTitle1Code;
        [Size(2)]
        public string BrandedTitle1Code
        {
            get { return fBrandedTitle1Code; }
            set { SetPropertyValue<string>("BrandedTitle1Code", ref fBrandedTitle1Code, value); }
        }

        string fBrandedTitle1Description;
        [Size(100)]
        public string BrandedTitle1Description
        {
            get { return fBrandedTitle1Description; }
            set { SetPropertyValue<string>("BrandedTitle1Description", ref fBrandedTitle1Description, value); }
        }
        
        DateTime fBrandedTitleDate1;
        public DateTime BrandedTitleDate1
        {
            get { return fBrandedTitleDate1; }
            set { SetPropertyValue<DateTime>("BrandedTitleDate1", ref fBrandedTitleDate1, value); }
        }
        
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }

        string fCreatedAgentCode;
        [Size(50)]
        public string CreatedAgentCode
        {
            get { return fCreatedAgentCode; }
            set { SetPropertyValue<string>("CreatedAgentCode", ref fCreatedAgentCode, value); }
        }

    }
}