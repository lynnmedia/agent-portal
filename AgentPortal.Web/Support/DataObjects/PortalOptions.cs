using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PortalOptions : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fOptionName;
        public string OptionName
        {
            get { return fOptionName; }
            set { SetPropertyValue<string>("OptionName", ref fOptionName, value); }
        }
        string fOptionDesc;
        public string OptionDesc
        {
            get { return fOptionDesc; }
            set { SetPropertyValue<string>("OptionDesc", ref fOptionDesc, value); }
        }
        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        public PortalOptions(Session session) : base(session) { }
        public PortalOptions() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
