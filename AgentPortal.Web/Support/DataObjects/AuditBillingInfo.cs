using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditBillingInfo
    {
        int IndexID { get; set; }
        string PolicyNo { get; set; }
        Guid UI_GUID { get; set; }
        DateTime RowDate { get; set; }
        string RowDesc { get; set; }
        double AmtBilled { get; set; }
        double InstallFee { get; set; }
        double NSFFee { get; set; }
        double LateFee { get; set; }
        double SubTotal { get; set; }
        double Payment { get; set; }
        double MinDue { get; set; }
        double Principle { get; set; }
        double Balance { get; set; }
        string Hover { get; set; }
    }

    public class AuditBillingOnfo : IAuditBillingInfo
    {
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public Guid UI_GUID { get; set; }
        public DateTime RowDate { get; set; }
        public string RowDesc { get; set; }
        public double AmtBilled { get; set; }
        public double InstallFee { get; set; }
        public double NSFFee { get; set; }
        public double LateFee { get; set; }
        public double SubTotal { get; set; }
        public double Payment { get; set; }
        public double MinDue { get; set; }
        public double Principle { get; set; }
        public double Balance { get; set; }
        public string Hover { get; set; }
    }

	[Persistent("AuditBillingInfo")]
    public class XpoAuditBillingInfo : XPLiteObject, IAuditBillingInfo
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        Guid fUI_GUID;
        public Guid UI_GUID
        {
            get { return fUI_GUID; }
            set { SetPropertyValue<Guid>("UI_GUID", ref fUI_GUID, value); }
        }
        DateTime fRowDate;
        public DateTime RowDate
        {
            get { return fRowDate; }
            set { SetPropertyValue<DateTime>("RowDate", ref fRowDate, value); }
        }
        string fRowDesc;
        [Size(30)]
        public string RowDesc
        {
            get { return fRowDesc; }
            set { SetPropertyValue<string>("RowDesc", ref fRowDesc, value); }
        }
        double fAmtBilled;
        public double AmtBilled
        {
            get { return fAmtBilled; }
            set { SetPropertyValue<double>("AmtBilled", ref fAmtBilled, value); }
        }
        double fInstallFee;
        public double InstallFee
        {
            get { return fInstallFee; }
            set { SetPropertyValue<double>("InstallFee", ref fInstallFee, value); }
        }
        double fNSFFee;
        public double NSFFee
        {
            get { return fNSFFee; }
            set { SetPropertyValue<double>("NSFFee", ref fNSFFee, value); }
        }
        double fLateFee;
        public double LateFee
        {
            get { return fLateFee; }
            set { SetPropertyValue<double>("LateFee", ref fLateFee, value); }
        }
        double fSubTotal;
        public double SubTotal
        {
            get { return fSubTotal; }
            set { SetPropertyValue<double>("SubTotal", ref fSubTotal, value); }
        }
        double fPayment;
        public double Payment
        {
            get { return fPayment; }
            set { SetPropertyValue<double>("Payment", ref fPayment, value); }
        }
        double fMinDue;
        public double MinDue
        {
            get { return fMinDue; }
            set { SetPropertyValue<double>("MinDue", ref fMinDue, value); }
        }
        double fPrinciple;
        public double Principle
        {
            get { return fPrinciple; }
            set { SetPropertyValue<double>("Principle", ref fPrinciple, value); }
        }
        double fBalance;
        public double Balance
        {
            get { return fBalance; }
            set { SetPropertyValue<double>("Balance", ref fBalance, value); }
        }
        string fHover;
        public string Hover
        {
            get { return fHover; }
            set { SetPropertyValue<string>("Hover", ref fHover, value); }
        }
        public XpoAuditBillingInfo(Session session) : base(session) { }
        public XpoAuditBillingInfo() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
