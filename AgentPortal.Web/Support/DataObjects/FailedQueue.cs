using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class FailedQueue : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        [Size(200)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fRecordKey;
        public string RecordKey
        {
            get { return fRecordKey; }
            set { SetPropertyValue<string>("RecordKey", ref fRecordKey, value); }
        }
        string fFailedLocation;
        public string FailedLocation
        {
            get { return fFailedLocation; }
            set { SetPropertyValue<string>("FailedLocation", ref fFailedLocation, value); }
        }
        string fApplicationName;
        public string ApplicationName
        {
            get { return fApplicationName; }
            set { SetPropertyValue<string>("ApplicationName", ref fApplicationName, value); }
        }
        string fSendTo;
        public string SendTo
        {
            get { return fSendTo; }
            set { SetPropertyValue<string>("SendTo", ref fSendTo, value); }
        }
        string fSubject;
        [Size(500)]
        public string Subject
        {
            get { return fSubject; }
            set { SetPropertyValue<string>("Subject", ref fSubject, value); }
        }
        string fBody;
        [Size(1000)]
        public string Body
        {
            get { return fBody; }
            set { SetPropertyValue<string>("Body", ref fBody, value); }
        }
        string fAttachments;
        [Size(SizeAttribute.Unlimited)]
        public string Attachments
        {
            get { return fAttachments; }
            set { SetPropertyValue<string>("Attachments", ref fAttachments, value); }
        }
        bool fHasAttachments;
        public bool HasAttachments
        {
            get { return fHasAttachments; }
            set { SetPropertyValue<bool>("HasAttachments", ref fHasAttachments, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        bool fIsProcessed;
        public bool IsProcessed
        {
            get { return fIsProcessed; }
            set { SetPropertyValue<bool>("IsProcessed", ref fIsProcessed, value); }
        }
        public FailedQueue(Session session) : base(session) { }
        public FailedQueue() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
