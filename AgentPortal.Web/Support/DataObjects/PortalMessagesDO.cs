using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class PortalMessages : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fMsgType;
        public string MsgType
        {
            get { return fMsgType; }
            set { SetPropertyValue<string>("MsgType", ref fMsgType, value); }
        }
        string fMessage;
        [Size(500)]
        public string Message
        {
            get { return fMessage; }
            set { SetPropertyValue<string>("Message", ref fMessage, value); }
        }
        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        string fUsername;
        [Size(300)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        public PortalMessages(Session session) : base(session) { }
        public PortalMessages() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
