using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class SystemLyonsReasonCodes : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fReasonCode;
        public int ReasonCode
        {
            get { return fReasonCode; }
            set { SetPropertyValue<int>("ReasonCode", ref fReasonCode, value); }
        }
        string fReasonDesc;
        [Size(2000)]
        public string ReasonDesc
        {
            get { return fReasonDesc; }
            set { SetPropertyValue<string>("ReasonDesc", ref fReasonDesc, value); }
        }
        string fActionType;
        [Size(500)]
        public string ActionType
        {
            get { return fActionType; }
            set { SetPropertyValue<string>("ActionType", ref fActionType, value); }
        }
        DateTime fCreatedDate;
        public DateTime CreatedDate
        {
            get { return fCreatedDate; }
            set { SetPropertyValue<DateTime>("CreatedDate", ref fCreatedDate, value); }
        }
        public SystemLyonsReasonCodes(Session session) : base(session) { }
        public SystemLyonsReasonCodes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
