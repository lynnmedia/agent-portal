﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class SystemDocType : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fDescription;
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }

        

        public SystemDocType(Session session) : base(session) { }
        public SystemDocType() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class DocumentUpload : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public string fRecordKey;
        public string RecordKey
        {
            get { return fRecordKey; }
            set { SetPropertyValue<string>("RecordKey", ref fRecordKey, value); }
        }
        string fDocType;
        public string DocType
        {
            get { return fDocType; }
            set { SetPropertyValue<string>("DocType", ref fDocType, value); }
        }

        string fFileToUpload;
        public string FileToUpload
        {
            get { return fFileToUpload; }
            set { SetPropertyValue<string>("FileToUpload", ref fFileToUpload, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        DateTime fDateStarted;
        public DateTime DateStarted
        {
            get { return fDateStarted; }
            set { SetPropertyValue<DateTime>("DateStarted", ref fDateStarted, value); }
        }
        DateTime fDateCompleted;
        public DateTime DateCompleted
        {
            get { return fDateCompleted; }
            set { SetPropertyValue<DateTime>("DateCompleted", ref fDateCompleted, value); }
        }
        bool fIsProcessed;
        public bool IsProcessed
        {
            get { return fIsProcessed; }
            set { SetPropertyValue<bool>("IsProcessed", ref fIsProcessed, value); }
        }

        public DocumentUpload(Session session) : base(session) { }
        public DocumentUpload() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}