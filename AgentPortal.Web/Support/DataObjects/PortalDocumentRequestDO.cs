using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class PortalDocumentRequest : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        [Size(500)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fType;
        public string Type
        {
            get { return fType; }
            set { SetPropertyValue<string>("Type", ref fType, value); }
        }
        string fRecordNo;
        public string RecordNo
        {
            get { return fRecordNo; }
            set { SetPropertyValue<string>("RecordNo", ref fRecordNo, value); }
        }
        string fReportName;
        [Size(500)]
        public string ReportName
        {
            get { return fReportName; }
            set { SetPropertyValue<string>("ReportName", ref fReportName, value); }
        }
        bool fIsNewDocs;
        public bool IsNewDocs
        {
            get { return fIsNewDocs; }
            set { SetPropertyValue<bool>("IsNewDocs", ref fIsNewDocs, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        DateTime fDateStarted;
        public DateTime DateStarted
        {
            get { return fDateStarted; }
            set { SetPropertyValue<DateTime>("DateStarted", ref fDateStarted, value); }
        }
        DateTime fDateFinished;
        public DateTime DateFinished
        {
            get { return fDateFinished; }
            set { SetPropertyValue<DateTime>("DateFinished", ref fDateFinished, value); }
        }
        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }
        public PortalDocumentRequest(Session session) : base(session) { }
        public PortalDocumentRequest() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
