using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PortalActivityLog : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        [Size(200)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fActivityType;
        [Size(200)]
        public string ActivityType
        {
            get { return fActivityType; }
            set { SetPropertyValue<string>("ActivityType", ref fActivityType, value); }
        }
        string fActivityDesc;
        [Size(8000)]
        public string ActivityDesc
        {
            get { return fActivityDesc; }
            set { SetPropertyValue<string>("ActivityDesc", ref fActivityDesc, value); }
        }
        DateTime fActivityDate;
        public DateTime ActivityDate
        {
            get { return fActivityDate; }
            set { SetPropertyValue<DateTime>("ActivityDate", ref fActivityDate, value); }
        }
        public PortalActivityLog(Session session) : base(session) { }
        public PortalActivityLog() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
