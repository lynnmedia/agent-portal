﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyPaymentProfile : IPolicyPaymentProfile
    {
        public int Id { get; set; }
        public string PolicyNo { get; set; }
        public string ProfileId { get; set; }
        public string AccountType { get; set; }
        public string CreatedByUserName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastUpdatedByUserName { get; set; }
        public DateTime LastUpdatedDate { get; set; }
    }

    public interface IPolicyPaymentProfile
    {
        int Id { get; set; }
        string PolicyNo { get; set; }
        string ProfileId { get; set; }
        string AccountType { get; set; }
        string CreatedByUserName { get; set; }
        DateTime CreatedDate { get; set; }
        string LastUpdatedByUserName { get; set; }
        DateTime LastUpdatedDate { get; set; }
    }

    [Persistent("PolicyPaymentProfile")]
    public class XpoPolicyPaymentProfile : XPLiteObject, IPolicyPaymentProfile
    {
        private int id;
        private string policyNo;
        private string profileId;
        private string accountType;


        public XpoPolicyPaymentProfile(Session session) : base(session) { }
        public XpoPolicyPaymentProfile() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        [Key(true)]
        public int Id
        {
            get { return id; }
            set { SetPropertyValue<int>("Id", ref id, value); }
        }

        public string PolicyNo
        {
            get { return policyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref policyNo, value); }
        }

        public string ProfileId
        {
            get { return profileId; }
            set { SetPropertyValue<string>("ProfileId", ref profileId, value); }
        }
        public string AccountType
        {
            get { return accountType; }
            set { SetPropertyValue<string>("AccountType", ref accountType, value); }
        }

        string fCreatedByUsername;
        [Size(200)]
        public string CreatedByUserName
        {
            get { return fCreatedByUsername; }
            set { SetPropertyValue<string>("CreatedByUserName", ref fCreatedByUsername, value); }
        }

        DateTime fCreatedDate;
        public DateTime CreatedDate
        {
            get { return fCreatedDate; }
            set { SetPropertyValue<DateTime>("CreatedDate", ref fCreatedDate, value); }
        }

        string fLastUpdatedUsername;
        [Size(200)]
        public string LastUpdatedByUserName
        {
            get { return fLastUpdatedUsername; }
            set { SetPropertyValue<string>("LastUpdatedByUserName", ref fLastUpdatedUsername, value); }
        }

        DateTime fLastUpdatedDate;
        public DateTime LastUpdatedDate
        {
            get { return fLastUpdatedDate; }
            set { SetPropertyValue<DateTime>("LastUpdatedDate", ref fLastUpdatedDate, value); }
        }
    }
}