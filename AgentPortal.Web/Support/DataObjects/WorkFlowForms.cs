﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class WorkFlowForms : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fFormName;
        [Size(500)]
        public string FormName
        {
            get { return fFormName; }
            set { SetPropertyValue<string>("FormName", ref fFormName, value); }
        }
        string fGroupName;
        [Size(500)]
        public string GroupName
        {
            get { return fGroupName; }
            set { SetPropertyValue<string>("GroupName", ref fGroupName, value); }
        }
        string fCreatedBy;
        [Size(200)]
        public string CreatedBy
        {
            get { return fCreatedBy; }
            set { SetPropertyValue<string>("CreatedBy", ref fCreatedBy, value); }
        }
        DateTime fDateUpdated;
        public DateTime DateUpdated
        {
            get { return fDateUpdated; }
            set { SetPropertyValue<DateTime>("DateUpdated", ref fDateUpdated, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        string fDepartment;
        public string Department
        {
            get { return fDepartment; }
            set { SetPropertyValue<string>("Department", ref fDepartment, value); }
        }

        bool fisInactive;
        public bool IsInactive
        {
            get { return fisInactive; }
            set { SetPropertyValue<bool>("IsInactive", ref fisInactive, value); }
        }
        public WorkFlowForms(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}