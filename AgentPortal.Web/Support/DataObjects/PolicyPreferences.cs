﻿using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyPreferences : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexId
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexId", ref fIndexID, value); }
        }

        string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        string fType;
        public string Type
        {
            get { return fType; }
            set { SetPropertyValue<string>("Type", ref fType, value); }
        }

        string fValue;
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }

        public PolicyPreferences(Session session) : base(session) { }
        public PolicyPreferences() : base(Session.DefaultSession) { }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}