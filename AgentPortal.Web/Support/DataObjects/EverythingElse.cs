using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IClaimHistory
    {
        int IndexID { get; set; }
        string ClaimNo { get; set; }
        int Claimant { get; set; }
        DateTime AcctDate { get; set; }
        string Action { get; set; }
        string UserName { get; set; }
        bool ChangeStatus { get; set; }
        bool ClaimClosed { get; set; }
        string OldStatus { get; set; }
        string NewStatus { get; set; }
        bool ChangeReserve { get; set; }
        int OldReserve { get; set; }
        int NewReserve { get; set; }
        bool ChangeLAEReserve { get; set; }
        int OldLAEReserve { get; set; }
        int NewLAEReserve { get; set; }
    }

    public class ClaimHistory : IClaimHistory
    {
        public int IndexID { get; set; }
        public string ClaimNo { get; set; }
        public int Claimant { get; set; }
        public DateTime AcctDate { get; set; }
        public string Action { get; set; }
        public string UserName { get; set; }
        public bool ChangeStatus { get; set; }
        public bool ClaimClosed { get; set; }
        public string OldStatus { get; set; }
        public string NewStatus { get; set; }
        public bool ChangeReserve { get; set; }
        public int OldReserve { get; set; }
        public int NewReserve { get; set; }
        public bool ChangeLAEReserve { get; set; }
        public int OldLAEReserve { get; set; }
        public int NewLAEReserve { get; set; }
    }

    [Persistent("ClaimHistory")]
    public class XpoClaimHistory : XPLiteObject, IClaimHistory
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fClaimNo;
        [Size(20)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        string fAction;
        [Size(20)]
        public string Action
        {
            get { return fAction; }
            set { SetPropertyValue<string>("Action", ref fAction, value); }
        }
        string fUserName;
        [Size(50)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        bool fChangeStatus;
        public bool ChangeStatus
        {
            get { return fChangeStatus; }
            set { SetPropertyValue<bool>("ChangeStatus", ref fChangeStatus, value); }
        }
        bool fClaimClosed;
        public bool ClaimClosed
        {
            get { return fClaimClosed; }
            set { SetPropertyValue<bool>("ClaimClosed", ref fClaimClosed, value); }
        }
        string fOldStatus;
        [Size(20)]
        public string OldStatus
        {
            get { return fOldStatus; }
            set { SetPropertyValue<string>("OldStatus", ref fOldStatus, value); }
        }
        string fNewStatus;
        [Size(20)]
        public string NewStatus
        {
            get { return fNewStatus; }
            set { SetPropertyValue<string>("NewStatus", ref fNewStatus, value); }
        }
        bool fChangeReserve;
        public bool ChangeReserve
        {
            get { return fChangeReserve; }
            set { SetPropertyValue<bool>("ChangeReserve", ref fChangeReserve, value); }
        }
        int fOldReserve;
        public int OldReserve
        {
            get { return fOldReserve; }
            set { SetPropertyValue<int>("OldReserve", ref fOldReserve, value); }
        }
        int fNewReserve;
        public int NewReserve
        {
            get { return fNewReserve; }
            set { SetPropertyValue<int>("NewReserve", ref fNewReserve, value); }
        }
        bool fChangeLAEReserve;
        public bool ChangeLAEReserve
        {
            get { return fChangeLAEReserve; }
            set { SetPropertyValue<bool>("ChangeLAEReserve", ref fChangeLAEReserve, value); }
        }
        int fOldLAEReserve;
        public int OldLAEReserve
        {
            get { return fOldLAEReserve; }
            set { SetPropertyValue<int>("OldLAEReserve", ref fOldLAEReserve, value); }
        }
        int fNewLAEReserve;
        public int NewLAEReserve
        {
            get { return fNewLAEReserve; }
            set { SetPropertyValue<int>("NewLAEReserve", ref fNewLAEReserve, value); }
        }
        public XpoClaimHistory(Session session) : base(session) { }
        public XpoClaimHistory() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimsClaimantsAddresses
    {
        string ClaimNo { get; set; }
        int ClaimantNo { get; set; }
        int PartyNo { get; set; }
        string TaxID { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string FirmName { get; set; }
        string Street1 { get; set; }
        string Street2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Phone { get; set; }
        string Ext { get; set; }
        int IndexID { get; set; }
    }

    public class ClaimsClaimantsAddresses : IClaimsClaimantsAddresses
    {
        public string ClaimNo { get; set; }
        public int ClaimantNo { get; set; }
        public int PartyNo { get; set; }
        public string TaxID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FirmName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string Ext { get; set; }
        public int IndexID { get; set; }
    }

    [Persistent("ClaimsClaimantsAddresses")]
    public class XpoClaimsClaimantsAddresses : XPLiteObject, IClaimsClaimantsAddresses
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimantNo;
        public int ClaimantNo
        {
            get { return fClaimantNo; }
            set { SetPropertyValue<int>("ClaimantNo", ref fClaimantNo, value); }
        }
        int fPartyNo;
        public int PartyNo
        {
            get { return fPartyNo; }
            set { SetPropertyValue<int>("PartyNo", ref fPartyNo, value); }
        }
        string fTaxID;
        [Size(15)]
        public string TaxID
        {
            get { return fTaxID; }
            set { SetPropertyValue<string>("TaxID", ref fTaxID, value); }
        }
        string fLastName;
        [Size(30)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fFirstName;
        [Size(25)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fFirmName;
        [Size(35)]
        public string FirmName
        {
            get { return fFirmName; }
            set { SetPropertyValue<string>("FirmName", ref fFirmName, value); }
        }
        string fStreet1;
        [Size(35)]
        public string Street1
        {
            get { return fStreet1; }
            set { SetPropertyValue<string>("Street1", ref fStreet1, value); }
        }
        string fStreet2;
        [Size(35)]
        public string Street2
        {
            get { return fStreet2; }
            set { SetPropertyValue<string>("Street2", ref fStreet2, value); }
        }
        string fCity;
        [Size(30)]
        public string City
        {
            get { return fCity; }
            set { SetPropertyValue<string>("City", ref fCity, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fZipCode;
        [Size(5)]
        public string ZipCode
        {
            get { return fZipCode; }
            set { SetPropertyValue<string>("ZipCode", ref fZipCode, value); }
        }
        string fPhone;
        [Size(14)]
        public string Phone
        {
            get { return fPhone; }
            set { SetPropertyValue<string>("Phone", ref fPhone, value); }
        }
        string fExt;
        [Size(5)]
        public string Ext
        {
            get { return fExt; }
            set { SetPropertyValue<string>("Ext", ref fExt, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public XpoClaimsClaimantsAddresses(Session session) : base(session) { }
        public XpoClaimsClaimantsAddresses() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface ISystem_DLEncode
    {
        string WholeName { get; set; }
        string Initial { get; set; }
        string Type { get; set; }
        int EncodedValue { get; set; }
        int OID { get; set; }
    }

    public class System_DLEncode : ISystem_DLEncode
    {
        public string WholeName { get; set; }
        public string Initial { get; set; }
        public string Type { get; set; }
        public int EncodedValue { get; set; }
        public int OID { get; set; }
    }

    [Persistent("System_DLEncode")]
    public class XpoSystem_DLEncode : XPCustomObject, ISystem_DLEncode
    {
        string fWholeName;
        public string WholeName
        {
            get { return fWholeName; }
            set { SetPropertyValue<string>("WholeName", ref fWholeName, value); }
        }
        string fInitial;
        [Size(1)]
        public string Initial
        {
            get { return fInitial; }
            set { SetPropertyValue<string>("Initial", ref fInitial, value); }
        }
        string fType;
        [Size(20)]
        public string Type
        {
            get { return fType; }
            set { SetPropertyValue<string>("Type", ref fType, value); }
        }
        int fEncodedValue;
        public int EncodedValue
        {
            get { return fEncodedValue; }
            set { SetPropertyValue<int>("EncodedValue", ref fEncodedValue, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        public XpoSystem_DLEncode(Session session) : base(session) { }
        public XpoSystem_DLEncode() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimsPayments
    {
        string ClaimNo { get; set; }
        int Claimant { get; set; }
        string ClmPayKey { get; set; }
        DateTime AcctDate { get; set; }
        string Type { get; set; }
        double Amount { get; set; }
        string PayeeOne { get; set; }
        string PayeeTwo { get; set; }
        string TaxIDNo { get; set; }
        string Street { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        string CheckNo { get; set; }
        bool Cleared { get; set; }
        DateTime DateCleared { get; set; }
        bool Voided { get; set; }
        DateTime DateVoided { get; set; }
        string xxUser { get; set; }
        bool Allocated { get; set; }
        bool VoidRec { get; set; }
        string Note { get; set; }
        int ID { get; set; }
        string BillKey { get; set; }
        string Transmittal { get; set; }
        double GrossSalvageAmt { get; set; }
        double StorageCharges { get; set; }
        double OtherCharges { get; set; }
    }

    public class ClaimsPayments : IClaimsPayments
    {
        public string ClaimNo { get; set; }
        public int Claimant { get; set; }
        public string ClmPayKey { get; set; }
        public DateTime AcctDate { get; set; }
        public string Type { get; set; }
        public double Amount { get; set; }
        public string PayeeOne { get; set; }
        public string PayeeTwo { get; set; }
        public string TaxIDNo { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string CheckNo { get; set; }
        public bool Cleared { get; set; }
        public DateTime DateCleared { get; set; }
        public bool Voided { get; set; }
        public DateTime DateVoided { get; set; }
        public string xxUser { get; set; }
        public bool Allocated { get; set; }
        public bool VoidRec { get; set; }
        public string Note { get; set; }
        public int ID { get; set; }
        public string BillKey { get; set; }
        public string Transmittal { get; set; }
        public double GrossSalvageAmt { get; set; }
        public double StorageCharges { get; set; }
        public double OtherCharges { get; set; }
    }

    [Persistent("ClaimsPayments")]
    public class XpoClaimsPayments : XPLiteObject, IClaimsPayments
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fClmPayKey;
        [Size(20)]
        public string ClmPayKey
        {
            get { return fClmPayKey; }
            set { SetPropertyValue<string>("ClmPayKey", ref fClmPayKey, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        string fType;
        [Size(4)]
        public string Type
        {
            get { return fType; }
            set { SetPropertyValue<string>("Type", ref fType, value); }
        }
        double fAmount;
        public double Amount
        {
            get { return fAmount; }
            set { SetPropertyValue<double>("Amount", ref fAmount, value); }
        }
        string fPayeeOne;
        [Size(150)]
        public string PayeeOne
        {
            get { return fPayeeOne; }
            set { SetPropertyValue<string>("PayeeOne", ref fPayeeOne, value); }
        }
        string fPayeeTwo;
        [Size(40)]
        public string PayeeTwo
        {
            get { return fPayeeTwo; }
            set { SetPropertyValue<string>("PayeeTwo", ref fPayeeTwo, value); }
        }
        string fTaxIDNo;
        [Size(150)]
        public string TaxIDNo
        {
            get { return fTaxIDNo; }
            set { SetPropertyValue<string>("TaxIDNo", ref fTaxIDNo, value); }
        }
        string fStreet;
        [Size(35)]
        public string Street
        {
            get { return fStreet; }
            set { SetPropertyValue<string>("Street", ref fStreet, value); }
        }
        string fCity;
        [Size(25)]
        public string City
        {
            get { return fCity; }
            set { SetPropertyValue<string>("City", ref fCity, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fZip;
        [Size(5)]
        public string Zip
        {
            get { return fZip; }
            set { SetPropertyValue<string>("Zip", ref fZip, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        bool fCleared;
        public bool Cleared
        {
            get { return fCleared; }
            set { SetPropertyValue<bool>("Cleared", ref fCleared, value); }
        }
        DateTime fDateCleared;
        public DateTime DateCleared
        {
            get { return fDateCleared; }
            set { SetPropertyValue<DateTime>("DateCleared", ref fDateCleared, value); }
        }
        bool fVoided;
        public bool Voided
        {
            get { return fVoided; }
            set { SetPropertyValue<bool>("Voided", ref fVoided, value); }
        }
        DateTime fDateVoided;
        public DateTime DateVoided
        {
            get { return fDateVoided; }
            set { SetPropertyValue<DateTime>("DateVoided", ref fDateVoided, value); }
        }
        string fxxUser;
        [Size(30)]
        public string xxUser
        {
            get { return fxxUser; }
            set { SetPropertyValue<string>("xxUser", ref fxxUser, value); }
        }
        bool fAllocated;
        public bool Allocated
        {
            get { return fAllocated; }
            set { SetPropertyValue<bool>("Allocated", ref fAllocated, value); }
        }
        bool fVoidRec;
        public bool VoidRec
        {
            get { return fVoidRec; }
            set { SetPropertyValue<bool>("VoidRec", ref fVoidRec, value); }
        }
        string fNote;
        [Size(35)]
        public string Note
        {
            get { return fNote; }
            set { SetPropertyValue<string>("Note", ref fNote, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fBillKey;
        [Size(20)]
        public string BillKey
        {
            get { return fBillKey; }
            set { SetPropertyValue<string>("BillKey", ref fBillKey, value); }
        }
        string fTransmittal;
        [Size(8000)]
        public string Transmittal
        {
            get { return fTransmittal; }
            set { SetPropertyValue<string>("Transmittal", ref fTransmittal, value); }
        }
        double fGrossSalvageAmt;
        public double GrossSalvageAmt
        {
            get { return fGrossSalvageAmt; }
            set { SetPropertyValue<double>("GrossSalvageAmt", ref fGrossSalvageAmt, value); }
        }
        double fStorageCharges;
        public double StorageCharges
        {
            get { return fStorageCharges; }
            set { SetPropertyValue<double>("StorageCharges", ref fStorageCharges, value); }
        }
        double fOtherCharges;
        public double OtherCharges
        {
            get { return fOtherCharges; }
            set { SetPropertyValue<double>("OtherCharges", ref fOtherCharges, value); }
        }
        public XpoClaimsPayments(Session session) : base(session) { }
        public XpoClaimsPayments() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Installments : IInstallments
    {
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public decimal InstallNo { get; set; }
        public short InstallCount { get; set; }
        public string Descr { get; set; }
        public int PayPlan { get; set; }
        public DateTime DueDate { get; set; }
        public decimal PolicyWritten { get; set; }
        public decimal InstallmentFee { get; set; }
        public decimal PolicyFee { get; set; }
        public decimal SR22Fee { get; set; }
        public decimal FHCF_Fee { get; set; }
        public decimal DBSetupFee { get; set; }
        public decimal LateFee { get; set; }
        public decimal FeeTotal { get; set; }
        public decimal DownPymtPercent { get; set; }
        public decimal DownPymtAmount { get; set; }
        public decimal InstallmentPymt { get; set; }
        public decimal PremiumBalance { get; set; }
        public decimal PymtApplied { get; set; }
        public decimal BalanceInstallment { get; set; }
        public bool Locked { get; set; }
        public bool IgnoreRec { get; set; }
        public DateTime DateIgnored { get; set; }
        public DateTime LastPayDate { get; set; }
        public double LastPayAmt { get; set; }
        public int Written { get; set; }
        public DateTime PrintDate { get; set; }
        public string UserName { get; set; }
        public int MVRFee { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal NSF_Fee { get; set; }
        public double RewriteFee { get; set; }
        public double? CreditCardFee { get; set; }
    }

    public interface IInstallments
    {
        int IndexID { get; set; }
        string PolicyNo { get; set; }
        decimal InstallNo { get; set; }
        short InstallCount { get; set; }
        string Descr { get; set; }
        int PayPlan { get; set; }
        DateTime DueDate { get; set; }
        decimal PolicyWritten { get; set; }
        decimal InstallmentFee { get; set; }
        decimal PolicyFee { get; set; }
        decimal SR22Fee { get; set; }
        decimal FHCF_Fee { get; set; }
        decimal DBSetupFee { get; set; }
        decimal LateFee { get; set; }
        decimal FeeTotal { get; set; }
        decimal DownPymtPercent { get; set; }
        decimal DownPymtAmount { get; set; }
        decimal InstallmentPymt { get; set; }
        decimal PremiumBalance { get; set; }
        decimal PymtApplied { get; set; }
        decimal BalanceInstallment { get; set; }
        bool Locked { get; set; }
        bool IgnoreRec { get; set; }
        DateTime DateIgnored { get; set; }
        DateTime LastPayDate { get; set; }
        double LastPayAmt { get; set; }
        int Written { get; set; }
        DateTime PrintDate { get; set; }
        string UserName { get; set; }
        int MVRFee { get; set; }
        DateTime CreateDate { get; set; }
        decimal NSF_Fee { get; set; }
        double RewriteFee { get; set; }
        double? CreditCardFee { get; set; }
    }

    [Persistent("Installments")]
    public class XpoInstallments : XPLiteObject, IInstallments
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(15)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        decimal fInstallNo;
        public decimal InstallNo
        {
            get { return fInstallNo; }
            set { SetPropertyValue<decimal>("InstallNo", ref fInstallNo, value); }
        }
        short fInstallCount;
        public short InstallCount
        {
            get { return fInstallCount; }
            set { SetPropertyValue<short>("InstallCount", ref fInstallCount, value); }
        }
        string fDescr;
        [Size(40)]
        public string Descr
        {
            get { return fDescr; }
            set { SetPropertyValue<string>("Descr", ref fDescr, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        DateTime fDueDate;
        public DateTime DueDate
        {
            get { return fDueDate; }
            set { SetPropertyValue<DateTime>("DueDate", ref fDueDate, value); }
        }
        decimal fPolicyWritten;
        public decimal PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<decimal>("PolicyWritten", ref fPolicyWritten, value); }
        }
        decimal fInstallmentFee;
        public decimal InstallmentFee
        {
            get { return fInstallmentFee; }
            set { SetPropertyValue<decimal>("InstallmentFee", ref fInstallmentFee, value); }
        }
        decimal fPolicyFee;
        public decimal PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<decimal>("PolicyFee", ref fPolicyFee, value); }
        }
        decimal fSR22Fee;
        public decimal SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<decimal>("SR22Fee", ref fSR22Fee, value); }
        }
        decimal fFHCF_Fee;
        public decimal FHCF_Fee
        {
            get { return fFHCF_Fee; }
            set { SetPropertyValue<decimal>("FHCF_Fee", ref fFHCF_Fee, value); }
        }
        decimal fDBSetupFee;
        public decimal DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<decimal>("DBSetupFee", ref fDBSetupFee, value); }
        }
        decimal fLateFee;
        public decimal LateFee
        {
            get { return fLateFee; }
            set { SetPropertyValue<decimal>("LateFee", ref fLateFee, value); }
        }
        decimal fFeeTotal;
        public decimal FeeTotal
        {
            get { return fFeeTotal; }
            set { SetPropertyValue<decimal>("FeeTotal", ref fFeeTotal, value); }
        }
        decimal fDownPymtPercent;
        public decimal DownPymtPercent
        {
            get { return fDownPymtPercent; }
            set { SetPropertyValue<decimal>("DownPymtPercent", ref fDownPymtPercent, value); }
        }
        decimal fDownPymtAmount;
        public decimal DownPymtAmount
        {
            get { return fDownPymtAmount; }
            set { SetPropertyValue<decimal>("DownPymtAmount", ref fDownPymtAmount, value); }
        }
        decimal fInstallmentPymt;
        public decimal InstallmentPymt
        {
            get { return fInstallmentPymt; }
            set { SetPropertyValue<decimal>("InstallmentPymt", ref fInstallmentPymt, value); }
        }
        decimal fPremiumBalance;
        public decimal PremiumBalance
        {
            get { return fPremiumBalance; }
            set { SetPropertyValue<decimal>("PremiumBalance", ref fPremiumBalance, value); }
        }
        decimal fPymtApplied;
        public decimal PymtApplied
        {
            get { return fPymtApplied; }
            set { SetPropertyValue<decimal>("PymtApplied", ref fPymtApplied, value); }
        }
        decimal fBalanceInstallment;
        public decimal BalanceInstallment
        {
            get { return fBalanceInstallment; }
            set { SetPropertyValue<decimal>("BalanceInstallment", ref fBalanceInstallment, value); }
        }
        bool fLocked;
        public bool Locked
        {
            get { return fLocked; }
            set { SetPropertyValue<bool>("Locked", ref fLocked, value); }
        }
        bool fIgnoreRec;
        public bool IgnoreRec
        {
            get { return fIgnoreRec; }
            set { SetPropertyValue<bool>("IgnoreRec", ref fIgnoreRec, value); }
        }
        DateTime fDateIgnored;
        public DateTime DateIgnored
        {
            get { return fDateIgnored; }
            set { SetPropertyValue<DateTime>("DateIgnored", ref fDateIgnored, value); }
        }
        DateTime fLastPayDate;
        public DateTime LastPayDate
        {
            get { return fLastPayDate; }
            set { SetPropertyValue<DateTime>("LastPayDate", ref fLastPayDate, value); }
        }
        double fLastPayAmt;
        public double LastPayAmt
        {
            get { return fLastPayAmt; }
            set { SetPropertyValue<double>("LastPayAmt", ref fLastPayAmt, value); }
        }
        int fWritten;
        public int Written
        {
            get { return fWritten; }
            set { SetPropertyValue<int>("Written", ref fWritten, value); }
        }
        DateTime fPrintDate;
        public DateTime PrintDate
        {
            get { return fPrintDate; }
            set { SetPropertyValue<DateTime>("PrintDate", ref fPrintDate, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        decimal fNSF_Fee;
        public decimal NSF_Fee
        {
            get { return fNSF_Fee; }
            set { SetPropertyValue<decimal>("NSF_Fee", ref fNSF_Fee, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        double? fCreditCardFee;
        public double? CreditCardFee
        {
            get { return fCreditCardFee; }
            set { fCreditCardFee = value; }
        }

        public XpoInstallments(Session session) : base(session) { }
        public XpoInstallments() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface ITimeClocks
    {
        int ID { get; set; }
        string Username { get; set; }
        string Computername { get; set; }
        string TimeStampType { get; set; }
        DateTime DateCreated { get; set; }
        bool ManagerOverride { get; set; }
        string ManagerName { get; set; }
        DateTime ManagerDateUpdated { get; set; }
        DateTime OrigDateCreated { get; set; }
    }

    public class TimeClocks : ITimeClocks
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Computername { get; set; }
        public string TimeStampType { get; set; }
        public DateTime DateCreated { get; set; }
        public bool ManagerOverride { get; set; }
        public string ManagerName { get; set; }
        public DateTime ManagerDateUpdated { get; set; }
        public DateTime OrigDateCreated { get; set; }
    }

    [Persistent("TimeClocks")]
    public class XpoTimeClocks : XPLiteObject, ITimeClocks
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fUsername;
        [Size(50)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fComputername;
        public string Computername
        {
            get { return fComputername; }
            set { SetPropertyValue<string>("Computername", ref fComputername, value); }
        }
        string fTimeStampType;
        [Size(20)]
        public string TimeStampType
        {
            get { return fTimeStampType; }
            set { SetPropertyValue<string>("TimeStampType", ref fTimeStampType, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        bool fManagerOverride;
        public bool ManagerOverride
        {
            get { return fManagerOverride; }
            set { SetPropertyValue<bool>("ManagerOverride", ref fManagerOverride, value); }
        }
        string fManagerName;
        public string ManagerName
        {
            get { return fManagerName; }
            set { SetPropertyValue<string>("ManagerName", ref fManagerName, value); }
        }
        DateTime fManagerDateUpdated;
        public DateTime ManagerDateUpdated
        {
            get { return fManagerDateUpdated; }
            set { SetPropertyValue<DateTime>("ManagerDateUpdated", ref fManagerDateUpdated, value); }
        }
        DateTime fOrigDateCreated;
        public DateTime OrigDateCreated
        {
            get { return fOrigDateCreated; }
            set { SetPropertyValue<DateTime>("OrigDateCreated", ref fOrigDateCreated, value); }
        }
        public XpoTimeClocks(Session session) : base(session) { }
        public XpoTimeClocks() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IPolicyDriversViolations
    {
        string PolicyNo { get; set; }
        int DriverIndex { get; set; }
        int ViolationNo { get; set; }
        int ViolationIndex { get; set; }
        string ViolationCode { get; set; }
        string ViolationGroup { get; set; }
        string ViolationDesc { get; set; }
        DateTime ViolationDate { get; set; }
        int ViolationMonths { get; set; }
        int ViolationPoints { get; set; }
        bool Chargeable { get; set; }
        int OID { get; set; }
        int PTSDriverIndex { get; set; }
    }

    public class PolicyDriversViolations : IPolicyDriversViolations
    {
        public string PolicyNo { get; set; }
        public int DriverIndex { get; set; }
        public int ViolationNo { get; set; }
        public int ViolationIndex { get; set; }
        public string ViolationCode { get; set; }
        public string ViolationGroup { get; set; }
        public string ViolationDesc { get; set; }
        public DateTime ViolationDate { get; set; }
        public int ViolationMonths { get; set; }
        public int ViolationPoints { get; set; }
        public bool Chargeable { get; set; }
        public int OID { get; set; }
        public int PTSDriverIndex { get; set; }
    }

    [Persistent("PolicyDriversViolations")]
    public class XpoPolicyDriversViolations : XPCustomObject, IPolicyDriversViolations
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public XpoPolicyDriversViolations(Session session) : base(session) { }
        public XpoPolicyDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimsPIPInvoice_old
    {
        int PALMSInvNo { get; set; }
        string InvoiceNo { get; set; }
        string ProviderID { get; set; }
        DateTime InvoiceDate { get; set; }
        DateTime DateReceived { get; set; }
        string ClaimNo { get; set; }
        int Claimant { get; set; }
        string PolicyNo { get; set; }
        string PatientID { get; set; }
        string Patient { get; set; }
        string PatientAddress { get; set; }
        string PatientCity { get; set; }
        string PatientState { get; set; }
        string PatientZip { get; set; }
        bool HospitalBill { get; set; }
        bool ERCharges { get; set; }
        string IC9Code1 { get; set; }
        string IC9Code2 { get; set; }
        string IC9Code3 { get; set; }
        string IC9Code4 { get; set; }
        int InvoiceLineCount { get; set; }
        string ProviderName { get; set; }
        string ProviderStreet { get; set; }
        string ProviderCity { get; set; }
        string ProviderState { get; set; }
        string ProviderZip { get; set; }
        int InvoiceTerritory { get; set; }
        string ImageFileName { get; set; }
        string PIPBillKey { get; set; }
        string UPIN { get; set; }
        string PhyName { get; set; }
        int IndexID { get; set; }
    }

    public class ClaimsPIPInvoice_old : IClaimsPIPInvoice_old
    {
        public int PALMSInvNo { get; set; }
        public string InvoiceNo { get; set; }
        public string ProviderID { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime DateReceived { get; set; }
        public string ClaimNo { get; set; }
        public int Claimant { get; set; }
        public string PolicyNo { get; set; }
        public string PatientID { get; set; }
        public string Patient { get; set; }
        public string PatientAddress { get; set; }
        public string PatientCity { get; set; }
        public string PatientState { get; set; }
        public string PatientZip { get; set; }
        public bool HospitalBill { get; set; }
        public bool ERCharges { get; set; }
        public string IC9Code1 { get; set; }
        public string IC9Code2 { get; set; }
        public string IC9Code3 { get; set; }
        public string IC9Code4 { get; set; }
        public int InvoiceLineCount { get; set; }
        public string ProviderName { get; set; }
        public string ProviderStreet { get; set; }
        public string ProviderCity { get; set; }
        public string ProviderState { get; set; }
        public string ProviderZip { get; set; }
        public int InvoiceTerritory { get; set; }
        public string ImageFileName { get; set; }
        public string PIPBillKey { get; set; }
        public string UPIN { get; set; }
        public string PhyName { get; set; }
        public int IndexID { get; set; }
    }

    [Persistent("ClaimsPIPInvoice_old")]
    public class XpoClaimsPIPInvoice_old : XPLiteObject, IClaimsPIPInvoice_old
    {
        int fPALMSInvNo;
        public int PALMSInvNo
        {
            get { return fPALMSInvNo; }
            set { SetPropertyValue<int>("PALMSInvNo", ref fPALMSInvNo, value); }
        }
        string fInvoiceNo;
        [Size(30)]
        public string InvoiceNo
        {
            get { return fInvoiceNo; }
            set { SetPropertyValue<string>("InvoiceNo", ref fInvoiceNo, value); }
        }
        string fProviderID;
        public string ProviderID
        {
            get { return fProviderID; }
            set { SetPropertyValue<string>("ProviderID", ref fProviderID, value); }
        }
        DateTime fInvoiceDate;
        public DateTime InvoiceDate
        {
            get { return fInvoiceDate; }
            set { SetPropertyValue<DateTime>("InvoiceDate", ref fInvoiceDate, value); }
        }
        DateTime fDateReceived;
        public DateTime DateReceived
        {
            get { return fDateReceived; }
            set { SetPropertyValue<DateTime>("DateReceived", ref fDateReceived, value); }
        }
        string fClaimNo;
        [Size(20)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPatientID;
        [Size(20)]
        public string PatientID
        {
            get { return fPatientID; }
            set { SetPropertyValue<string>("PatientID", ref fPatientID, value); }
        }
        string fPatient;
        [Size(40)]
        public string Patient
        {
            get { return fPatient; }
            set { SetPropertyValue<string>("Patient", ref fPatient, value); }
        }
        string fPatientAddress;
        [Size(40)]
        public string PatientAddress
        {
            get { return fPatientAddress; }
            set { SetPropertyValue<string>("PatientAddress", ref fPatientAddress, value); }
        }
        string fPatientCity;
        [Size(30)]
        public string PatientCity
        {
            get { return fPatientCity; }
            set { SetPropertyValue<string>("PatientCity", ref fPatientCity, value); }
        }
        string fPatientState;
        [Size(2)]
        public string PatientState
        {
            get { return fPatientState; }
            set { SetPropertyValue<string>("PatientState", ref fPatientState, value); }
        }
        string fPatientZip;
        [Size(5)]
        public string PatientZip
        {
            get { return fPatientZip; }
            set { SetPropertyValue<string>("PatientZip", ref fPatientZip, value); }
        }
        bool fHospitalBill;
        public bool HospitalBill
        {
            get { return fHospitalBill; }
            set { SetPropertyValue<bool>("HospitalBill", ref fHospitalBill, value); }
        }
        bool fERCharges;
        public bool ERCharges
        {
            get { return fERCharges; }
            set { SetPropertyValue<bool>("ERCharges", ref fERCharges, value); }
        }
        string fIC9Code1;
        [Size(15)]
        public string IC9Code1
        {
            get { return fIC9Code1; }
            set { SetPropertyValue<string>("IC9Code1", ref fIC9Code1, value); }
        }
        string fIC9Code2;
        [Size(15)]
        public string IC9Code2
        {
            get { return fIC9Code2; }
            set { SetPropertyValue<string>("IC9Code2", ref fIC9Code2, value); }
        }
        string fIC9Code3;
        [Size(15)]
        public string IC9Code3
        {
            get { return fIC9Code3; }
            set { SetPropertyValue<string>("IC9Code3", ref fIC9Code3, value); }
        }
        string fIC9Code4;
        [Size(15)]
        public string IC9Code4
        {
            get { return fIC9Code4; }
            set { SetPropertyValue<string>("IC9Code4", ref fIC9Code4, value); }
        }
        int fInvoiceLineCount;
        public int InvoiceLineCount
        {
            get { return fInvoiceLineCount; }
            set { SetPropertyValue<int>("InvoiceLineCount", ref fInvoiceLineCount, value); }
        }
        string fProviderName;
        [Size(40)]
        public string ProviderName
        {
            get { return fProviderName; }
            set { SetPropertyValue<string>("ProviderName", ref fProviderName, value); }
        }
        string fProviderStreet;
        [Size(40)]
        public string ProviderStreet
        {
            get { return fProviderStreet; }
            set { SetPropertyValue<string>("ProviderStreet", ref fProviderStreet, value); }
        }
        string fProviderCity;
        [Size(40)]
        public string ProviderCity
        {
            get { return fProviderCity; }
            set { SetPropertyValue<string>("ProviderCity", ref fProviderCity, value); }
        }
        string fProviderState;
        [Size(2)]
        public string ProviderState
        {
            get { return fProviderState; }
            set { SetPropertyValue<string>("ProviderState", ref fProviderState, value); }
        }
        string fProviderZip;
        [Size(5)]
        public string ProviderZip
        {
            get { return fProviderZip; }
            set { SetPropertyValue<string>("ProviderZip", ref fProviderZip, value); }
        }
        int fInvoiceTerritory;
        public int InvoiceTerritory
        {
            get { return fInvoiceTerritory; }
            set { SetPropertyValue<int>("InvoiceTerritory", ref fInvoiceTerritory, value); }
        }
        string fImageFileName;
        [Size(40)]
        public string ImageFileName
        {
            get { return fImageFileName; }
            set { SetPropertyValue<string>("ImageFileName", ref fImageFileName, value); }
        }
        string fPIPBillKey;
        [Size(20)]
        public string PIPBillKey
        {
            get { return fPIPBillKey; }
            set { SetPropertyValue<string>("PIPBillKey", ref fPIPBillKey, value); }
        }
        string fUPIN;
        [Size(20)]
        public string UPIN
        {
            get { return fUPIN; }
            set { SetPropertyValue<string>("UPIN", ref fUPIN, value); }
        }
        string fPhyName;
        [Size(50)]
        public string PhyName
        {
            get { return fPhyName; }
            set { SetPropertyValue<string>("PhyName", ref fPhyName, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public XpoClaimsPIPInvoice_old(Session session) : base(session) { }
        public XpoClaimsPIPInvoice_old() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IAuditPolicy
    {
        int HistoryIndexID { get; set; }
        string PolicyNo { get; set; }
        string PriorPolicyNo { get; set; }
        string NextPolicyNo { get; set; }
        int RenCount { get; set; }
        string State { get; set; }
        string LOB { get; set; }
        DateTime EffDate { get; set; }
        DateTime ExpDate { get; set; }
        string PolicyStatus { get; set; }
        string RenewalStatus { get; set; }
        DateTime DateBound { get; set; }
        DateTime DateIssued { get; set; }
        DateTime CancelDate { get; set; }
        string CancelType { get; set; }
        string Ins1First { get; set; }
        string Ins1Last { get; set; }
        string Ins1MI { get; set; }
        string Ins1Suffix { get; set; }
        string Ins1FullName { get; set; }
        string Ins2First { get; set; }
        string Ins2Last { get; set; }
        string Ins2MI { get; set; }
        string Ins2Suffix { get; set; }
        string Ins2FullName { get; set; }
        string Territory { get; set; }
        double PolicyWritten { get; set; }
        double PolicyAnnualized { get; set; }
        int CommPrem { get; set; }
        int DBSetupFee { get; set; }
        int PolicyFee { get; set; }
        int SR22Fee { get; set; }
        double FHCFFee { get; set; }
        string RateCycle { get; set; }
        bool SixMonth { get; set; }
        int PayPlan { get; set; }
        bool PaidInFullDisc { get; set; }
        bool EFT { get; set; }
        string MailStreet { get; set; }
        string MailCity { get; set; }
        string MailState { get; set; }
        string MailZip { get; set; }
        bool MailGarageSame { get; set; }
        string GarageStreet { get; set; }
        string GarageCity { get; set; }
        string GarageState { get; set; }
        string GarageZip { get; set; }
        string GarageTerritory { get; set; }
        string GarageCounty { get; set; }
        string HomePhone { get; set; }
        string WorkPhone { get; set; }
        string MainEmail { get; set; }
        string AltEmail { get; set; }
        bool Paperless { get; set; }
        string AgentCode { get; set; }
        double CommAtIssue { get; set; }
        double LOUCommAtIssue { get; set; }
        double ADNDCommAtIssue { get; set; }
        bool AgentGross { get; set; }
        string PIPDed { get; set; }
        bool NIO { get; set; }
        bool NIRR { get; set; }
        bool UMStacked { get; set; }
        bool HasBI { get; set; }
        bool HasMP { get; set; }
        bool HasUM { get; set; }
        bool HasLOU { get; set; }
        string BILimit { get; set; }
        string PDLimit { get; set; }
        string UMLimit { get; set; }
        string MedPayLimit { get; set; }
        bool Homeowner { get; set; }
        bool RenDisc { get; set; }
        int TransDisc { get; set; }
        bool HasPPO { get; set; }
        string PreviousCompany { get; set; }
        DateTime PreviousExpDate { get; set; }
        bool PreviousBI { get; set; }
        bool PreviousBalance { get; set; }
        bool DirectRepairDisc { get; set; }
        string UndTier { get; set; }
        bool OOSEnd { get; set; }
        int LOUCost { get; set; }
        int LOUAnnlPrem { get; set; }
        int ADNDLimit { get; set; }
        int ADNDCost { get; set; }
        int ADNDAnnlPrem { get; set; }
        bool HoldRtn { get; set; }
        bool NonOwners { get; set; }
        string ChangeGUID { get; set; }
        int policyCars { get; set; }
        int policyDrivers { get; set; }
        bool Unacceptable { get; set; }
        int HousholdIndex { get; set; }
        int RatingID { get; set; }
        int OID { get; set; }
        int Cars { get; set; }
        int Drivers { get; set; }
        bool IsReturnedMail { get; set; }
        bool IsOnHold { get; set; }
        bool IsClaimMisRep { get; set; }
        bool IsNoREI { get; set; }
        bool IsSuspense { get; set; }
        bool isAnnual { get; set; }
        string Carrier { get; set; }
        bool HasPriorCoverage { get; set; }
        bool HasPriorBalance { get; set; }
        bool HasLapseNone { get; set; }
        bool HasPD { get; set; }
        bool HasTOW { get; set; }
        string PIPLimit { get; set; }
        bool HasADND { get; set; }
        int MVRFee { get; set; }
        int WorkLoss { get; set; }
        double RentalAnnlPrem { get; set; }
        double RentalCost { get; set; }
        bool HasLapse110 { get; set; }
        bool HasLapse1131 { get; set; }
        double QuotedAmount { get; set; }
        double RewriteFee { get; set; }
    }

    public class AuditPolicy : IAuditPolicy
    {
        public int HistoryIndexID { get; set; }
        public string PolicyNo { get; set; }
        public string PriorPolicyNo { get; set; }
        public string NextPolicyNo { get; set; }
        public int RenCount { get; set; }
        public string State { get; set; }
        public string LOB { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public string PolicyStatus { get; set; }
        public string RenewalStatus { get; set; }
        public DateTime DateBound { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime CancelDate { get; set; }
        public string CancelType { get; set; }
        public string Ins1First { get; set; }
        public string Ins1Last { get; set; }
        public string Ins1MI { get; set; }
        public string Ins1Suffix { get; set; }
        public string Ins1FullName { get; set; }
        public string Ins2First { get; set; }
        public string Ins2Last { get; set; }
        public string Ins2MI { get; set; }
        public string Ins2Suffix { get; set; }
        public string Ins2FullName { get; set; }
        public string Territory { get; set; }
        public double PolicyWritten { get; set; }
        public double PolicyAnnualized { get; set; }
        public int CommPrem { get; set; }
        public int DBSetupFee { get; set; }
        public int PolicyFee { get; set; }
        public int SR22Fee { get; set; }
        public double FHCFFee { get; set; }
        public string RateCycle { get; set; }
        public bool SixMonth { get; set; }
        public int PayPlan { get; set; }
        public bool PaidInFullDisc { get; set; }
        public bool EFT { get; set; }
        public string MailStreet { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailZip { get; set; }
        public bool MailGarageSame { get; set; }
        public string GarageStreet { get; set; }
        public string GarageCity { get; set; }
        public string GarageState { get; set; }
        public string GarageZip { get; set; }
        public string GarageTerritory { get; set; }
        public string GarageCounty { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MainEmail { get; set; }
        public string AltEmail { get; set; }
        public bool Paperless { get; set; }
        public string AgentCode { get; set; }
        public double CommAtIssue { get; set; }
        public double LOUCommAtIssue { get; set; }
        public double ADNDCommAtIssue { get; set; }
        public bool AgentGross { get; set; }
        public string PIPDed { get; set; }
        public bool NIO { get; set; }
        public bool NIRR { get; set; }
        public bool UMStacked { get; set; }
        public bool HasBI { get; set; }
        public bool HasMP { get; set; }
        public bool HasUM { get; set; }
        public bool HasLOU { get; set; }
        public string BILimit { get; set; }
        public string PDLimit { get; set; }
        public string UMLimit { get; set; }
        public string MedPayLimit { get; set; }
        public bool Homeowner { get; set; }
        public bool RenDisc { get; set; }
        public int TransDisc { get; set; }
        public bool HasPPO { get; set; }
        public string PreviousCompany { get; set; }
        public DateTime PreviousExpDate { get; set; }
        public bool PreviousBI { get; set; }
        public bool PreviousBalance { get; set; }
        public bool DirectRepairDisc { get; set; }
        public string UndTier { get; set; }
        public bool OOSEnd { get; set; }
        public int LOUCost { get; set; }
        public int LOUAnnlPrem { get; set; }
        public int ADNDLimit { get; set; }
        public int ADNDCost { get; set; }
        public int ADNDAnnlPrem { get; set; }
        public bool HoldRtn { get; set; }
        public bool NonOwners { get; set; }
        public string ChangeGUID { get; set; }
        public int policyCars { get; set; }
        public int policyDrivers { get; set; }
        public bool Unacceptable { get; set; }
        public int HousholdIndex { get; set; }
        public int RatingID { get; set; }
        public int OID { get; set; }
        public int Cars { get; set; }
        public int Drivers { get; set; }
        public bool IsReturnedMail { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsClaimMisRep { get; set; }
        public bool IsNoREI { get; set; }
        public bool IsSuspense { get; set; }
        public bool isAnnual { get; set; }
        public string Carrier { get; set; }
        public bool HasPriorCoverage { get; set; }
        public bool HasPriorBalance { get; set; }
        public bool HasLapseNone { get; set; }
        public bool HasPD { get; set; }
        public bool HasTOW { get; set; }
        public string PIPLimit { get; set; }
        public bool HasADND { get; set; }
        public int MVRFee { get; set; }
        public int WorkLoss { get; set; }
        public double RentalAnnlPrem { get; set; }
        public double RentalCost { get; set; }
        public bool HasLapse110 { get; set; }
        public bool HasLapse1131 { get; set; }
        public double QuotedAmount { get; set; }
        public double RewriteFee { get; set; }
    }

    [Persistent("AuditPolicy")]
    public class XpoAuditPolicy : XPCustomObject, IAuditPolicy
    {
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPriorPolicyNo;
        [Size(15)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        [Size(5)]
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fpolicyCars;
        public int policyCars
        {
            get { return fpolicyCars; }
            set { SetPropertyValue<int>("policyCars", ref fpolicyCars, value); }
        }
        int fpolicyDrivers;
        public int policyDrivers
        {
            get { return fpolicyDrivers; }
            set { SetPropertyValue<int>("policyDrivers", ref fpolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }
        public XpoAuditPolicy(Session session) : base(session) { }
        public XpoAuditPolicy() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IPaymentsStaging
    {
        int IndexID { get; set; }
        string BatchNo { get; set; }
        string BatchID { get; set; }
        string PolicyNo { get; set; }
        string PaymentType { get; set; }
        string CheckNo { get; set; }
        string SerialNo { get; set; }
        DateTime PostmarkDate { get; set; }
        DateTime AcctDate { get; set; }
        double CheckAmt { get; set; }
        double DraftAmt { get; set; }
        double TotalAmt { get; set; }
        double CommissionAmt { get; set; }
        string PFCCode { get; set; }
        string UserName { get; set; }
        double FeeAmt { get; set; }
        double Interest { get; set; }
        bool InsuredCheck { get; set; }
        bool EFTDraft { get; set; }
        string EFTRefNo { get; set; }
        double AmtPrinciple { get; set; }
        DateTime DateVoided { get; set; }
        DateTime DateCleared { get; set; }
        DateTime ReportedToBankDate { get; set; }
        string BatchGUID { get; set; }
        string CheckNotes { get; set; }
        string Notes { get; set; }
        string PayeeOne { get; set; }
        string PayeeOneType { get; set; }
        string PayeeTwo { get; set; }
        string PayeeTwoType { get; set; }
        string ACHTransID { get; set; }
        DateTime CreateDate { get; set; }
        string TokenID { get; set; }
        string PortalRespCode { get; set; }
    }

    public class PaymentsStaging : IPaymentsStaging
    {
        public int IndexID { get; set; }
        public string BatchNo { get; set; }
        public string BatchID { get; set; }
        public string PolicyNo { get; set; }
        public string PaymentType { get; set; }
        public string CheckNo { get; set; }
        public string SerialNo { get; set; }
        public DateTime PostmarkDate { get; set; }
        public DateTime AcctDate { get; set; }
        public double CheckAmt { get; set; }
        public double DraftAmt { get; set; }
        public double TotalAmt { get; set; }
        public double CommissionAmt { get; set; }
        public string PFCCode { get; set; }
        public string UserName { get; set; }
        public double FeeAmt { get; set; }
        public double Interest { get; set; }
        public bool InsuredCheck { get; set; }
        public bool EFTDraft { get; set; }
        public string EFTRefNo { get; set; }
        public double AmtPrinciple { get; set; }
        public DateTime DateVoided { get; set; }
        public DateTime DateCleared { get; set; }
        public DateTime ReportedToBankDate { get; set; }
        public string BatchGUID { get; set; }
        public string CheckNotes { get; set; }
        public string Notes { get; set; }
        public string PayeeOne { get; set; }
        public string PayeeOneType { get; set; }
        public string PayeeTwo { get; set; }
        public string PayeeTwoType { get; set; }
        public string ACHTransID { get; set; }
        public DateTime CreateDate { get; set; }
        public string TokenID { get; set; }
        public string PortalRespCode { get; set; }
    }

    [Persistent("PaymentsStaging")]

    public class XpoPaymentsStaging : XPLiteObject, IPaymentsStaging
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fBatchNo;
        [Size(10)]
        public string BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<string>("BatchNo", ref fBatchNo, value); }
        }
        string fBatchID;
        [Size(50)]
        public string BatchID
        {
            get { return fBatchID; }
            set { SetPropertyValue<string>("BatchID", ref fBatchID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPaymentType;
        [Size(10)]
        public string PaymentType
        {
            get { return fPaymentType; }
            set { SetPropertyValue<string>("PaymentType", ref fPaymentType, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        string fSerialNo;
        [Size(10)]
        public string SerialNo
        {
            get { return fSerialNo; }
            set { SetPropertyValue<string>("SerialNo", ref fSerialNo, value); }
        }
        DateTime fPostmarkDate;
        public DateTime PostmarkDate
        {
            get { return fPostmarkDate; }
            set { SetPropertyValue<DateTime>("PostmarkDate", ref fPostmarkDate, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        double fCheckAmt;
        public double CheckAmt
        {
            get { return fCheckAmt; }
            set { SetPropertyValue<double>("CheckAmt", ref fCheckAmt, value); }
        }
        double fDraftAmt;
        public double DraftAmt
        {
            get { return fDraftAmt; }
            set { SetPropertyValue<double>("DraftAmt", ref fDraftAmt, value); }
        }
        double fTotalAmt;
        public double TotalAmt
        {
            get { return fTotalAmt; }
            set { SetPropertyValue<double>("TotalAmt", ref fTotalAmt, value); }
        }
        double fCommissionAmt;
        public double CommissionAmt
        {
            get { return fCommissionAmt; }
            set { SetPropertyValue<double>("CommissionAmt", ref fCommissionAmt, value); }
        }
        string fPFCCode;
        [Size(5)]
        public string PFCCode
        {
            get { return fPFCCode; }
            set { SetPropertyValue<string>("PFCCode", ref fPFCCode, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        double fFeeAmt;
        public double FeeAmt
        {
            get { return fFeeAmt; }
            set { SetPropertyValue<double>("FeeAmt", ref fFeeAmt, value); }
        }
        double fInterest;
        public double Interest
        {
            get { return fInterest; }
            set { SetPropertyValue<double>("Interest", ref fInterest, value); }
        }
        bool fInsuredCheck;
        public bool InsuredCheck
        {
            get { return fInsuredCheck; }
            set { SetPropertyValue<bool>("InsuredCheck", ref fInsuredCheck, value); }
        }
        bool fEFTDraft;
        public bool EFTDraft
        {
            get { return fEFTDraft; }
            set { SetPropertyValue<bool>("EFTDraft", ref fEFTDraft, value); }
        }
        string fEFTRefNo;
        [Size(35)]
        public string EFTRefNo
        {
            get { return fEFTRefNo; }
            set { SetPropertyValue<string>("EFTRefNo", ref fEFTRefNo, value); }
        }
        double fAmtPrinciple;
        public double AmtPrinciple
        {
            get { return fAmtPrinciple; }
            set { SetPropertyValue<double>("AmtPrinciple", ref fAmtPrinciple, value); }
        }
        DateTime fDateVoided;
        public DateTime DateVoided
        {
            get { return fDateVoided; }
            set { SetPropertyValue<DateTime>("DateVoided", ref fDateVoided, value); }
        }
        DateTime fDateCleared;
        public DateTime DateCleared
        {
            get { return fDateCleared; }
            set { SetPropertyValue<DateTime>("DateCleared", ref fDateCleared, value); }
        }
        DateTime fReportedToBankDate;
        public DateTime ReportedToBankDate
        {
            get { return fReportedToBankDate; }
            set { SetPropertyValue<DateTime>("ReportedToBankDate", ref fReportedToBankDate, value); }
        }
        string fBatchGUID;
        public string BatchGUID
        {
            get { return fBatchGUID; }
            set { SetPropertyValue<string>("BatchGUID", ref fBatchGUID, value); }
        }
        string fCheckNotes;
        public string CheckNotes
        {
            get { return fCheckNotes; }
            set { SetPropertyValue<string>("CheckNotes", ref fCheckNotes, value); }
        }
        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        string fPayeeOne;
        [Size(150)]
        public string PayeeOne
        {
            get { return fPayeeOne; }
            set { SetPropertyValue<string>("PayeeOne", ref fPayeeOne, value); }
        }
        string fPayeeOneType;
        [Size(50)]
        public string PayeeOneType
        {
            get { return fPayeeOneType; }
            set { SetPropertyValue<string>("PayeeOneType", ref fPayeeOneType, value); }
        }
        string fPayeeTwo;
        public string PayeeTwo
        {
            get { return fPayeeTwo; }
            set { SetPropertyValue<string>("PayeeTwo", ref fPayeeTwo, value); }
        }
        string fPayeeTwoType;
        [Size(50)]
        public string PayeeTwoType
        {
            get { return fPayeeTwoType; }
            set { SetPropertyValue<string>("PayeeTwoType", ref fPayeeTwoType, value); }
        }
        string fACHTransID;
        [Size(20)]
        public string ACHTransID
        {
            get { return fACHTransID; }
            set { SetPropertyValue<string>("ACHTransID", ref fACHTransID, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        string fTokenID;
        [Size(2000)]
        public string TokenID
        {
            get { return fTokenID; }
            set { SetPropertyValue<string>("TokenID", ref fTokenID, value); }
        }
        string fPortalRespCode;
        [Size(200)]
        public string PortalRespCode
        {
            get { return fPortalRespCode; }
            set { SetPropertyValue<string>("PortalRespCode", ref fPortalRespCode, value); }
        }
        public XpoPaymentsStaging(Session session) : base(session) { }
        public XpoPaymentsStaging() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IAuditPolicyDriversViolations
    {
        int HistoryIndexID { get; set; }
        string PolicyNo { get; set; }
        int DriverIndex { get; set; }
        int ViolationNo { get; set; }
        int ViolationIndex { get; set; }
        string ViolationCode { get; set; }
        string ViolationGroup { get; set; }
        string ViolationDesc { get; set; }
        DateTime ViolationDate { get; set; }
        int ViolationMonths { get; set; }
        int ViolationPoints { get; set; }
        bool Chargeable { get; set; }
        int OID { get; set; }
        int PTSDriverIndex { get; set; }
    }

    public class AuditPolicyDriversViolations : IAuditPolicyDriversViolations
    {
        public int HistoryIndexID { get; set; }
        public string PolicyNo { get; set; }
        public int DriverIndex { get; set; }
        public int ViolationNo { get; set; }
        public int ViolationIndex { get; set; }
        public string ViolationCode { get; set; }
        public string ViolationGroup { get; set; }
        public string ViolationDesc { get; set; }
        public DateTime ViolationDate { get; set; }
        public int ViolationMonths { get; set; }
        public int ViolationPoints { get; set; }
        public bool Chargeable { get; set; }
        public int OID { get; set; }
        public int PTSDriverIndex { get; set; }
    }

    [Persistent("AuditPolicyDriversViolations")]
    public class XpoAuditPolicyDriversViolations : XPCustomObject, IAuditPolicyDriversViolations
    {
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public XpoAuditPolicyDriversViolations(Session session) : base(session) { }
        public XpoAuditPolicyDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimPIPInvNumbers
    {
        int PALMSInvNo { get; set; }
        string UniqueKey { get; set; }
    }

    public class ClaimPIPInvNumbers : IClaimPIPInvNumbers
    {
        public int PALMSInvNo { get; set; }
        public string UniqueKey { get; set; }
    }

    [Persistent("ClaimPIPInvNumbers")]
    public class XpoClaimPIPInvNumbers : XPLiteObject, IClaimPIPInvNumbers
    {
        int fPALMSInvNo;
        [Key(true)]
        public int PALMSInvNo
        {
            get { return fPALMSInvNo; }
            set { SetPropertyValue<int>("PALMSInvNo", ref fPALMSInvNo, value); }
        }
        string fUniqueKey;
        [Size(20)]
        public string UniqueKey
        {
            get { return fUniqueKey; }
            set { SetPropertyValue<string>("UniqueKey", ref fUniqueKey, value); }
        }
        public XpoClaimPIPInvNumbers(Session session) : base(session) { }
        public XpoClaimPIPInvNumbers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimsChecksPending
    {
        string ClaimNo { get; set; }
        int Claimant { get; set; }
        string Type { get; set; }
        string CheckNo { get; set; }
        string SerialNo { get; set; }
        DateTime CheckDate { get; set; }
        string Note1 { get; set; }
        string Note2 { get; set; }
        string NewClmStatus { get; set; }
        int Type1099 { get; set; }
        string TaxName { get; set; }
        string TaxIDNo { get; set; }
        string PayeeOne { get; set; }
        bool UseAddr1 { get; set; }
        string PayeeTwo { get; set; }
        bool UseAddr2 { get; set; }
        string Street { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zip { get; set; }
        bool PickupChk { get; set; }
        double PayAmt { get; set; }
        int Peril { get; set; }
        string xxUser { get; set; }
        bool Allocated { get; set; }
        bool PIPPayment { get; set; }
        string PIPBillKey { get; set; }
        double PIPLAEAmt { get; set; }
        bool xxBulk { get; set; }
        int IndexID { get; set; }
        string PerilDesc { get; set; }
        string PayAmtInTxt { get; set; }
        string BatchNo { get; set; }
        string BillKey { get; set; }
        string Transmittal { get; set; }
    }

    public class ClaimsChecksPending :  IClaimsChecksPending
    {
        public string ClaimNo { get; set; }
        public int Claimant { get; set; }
        public string Type { get; set; }
        public string CheckNo { get; set; }
        public string SerialNo { get; set; }
        public DateTime CheckDate { get; set; }
        public string Note1 { get; set; }
        public string Note2 { get; set; }
        public string NewClmStatus { get; set; }
        public int Type1099 { get; set; }
        public string TaxName { get; set; }
        public string TaxIDNo { get; set; }
        public string PayeeOne { get; set; }
        public bool UseAddr1 { get; set; }
        public string PayeeTwo { get; set; }
        public bool UseAddr2 { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool PickupChk { get; set; }
        public double PayAmt { get; set; }
        public int Peril { get; set; }
        public string xxUser { get; set; }
        public bool Allocated { get; set; }
        public bool PIPPayment { get; set; }
        public string PIPBillKey { get; set; }
        public double PIPLAEAmt { get; set; }
        public bool xxBulk { get; set; }
        public int IndexID { get; set; }
        public string PerilDesc { get; set; }
        public string PayAmtInTxt { get; set; }
        public string BatchNo { get; set; }
        public string BillKey { get; set; }
        public string Transmittal { get; set; }
    }

    [Persistent("ClaimsChecksPending")]
    public class XpoClaimsChecksPending : XPLiteObject, IClaimsChecksPending
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fType;
        [Size(4)]
        public string Type
        {
            get { return fType; }
            set { SetPropertyValue<string>("Type", ref fType, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        string fSerialNo;
        [Size(10)]
        public string SerialNo
        {
            get { return fSerialNo; }
            set { SetPropertyValue<string>("SerialNo", ref fSerialNo, value); }
        }
        DateTime fCheckDate;
        public DateTime CheckDate
        {
            get { return fCheckDate; }
            set { SetPropertyValue<DateTime>("CheckDate", ref fCheckDate, value); }
        }
        string fNote1;
        [Size(50)]
        public string Note1
        {
            get { return fNote1; }
            set { SetPropertyValue<string>("Note1", ref fNote1, value); }
        }
        string fNote2;
        [Size(50)]
        public string Note2
        {
            get { return fNote2; }
            set { SetPropertyValue<string>("Note2", ref fNote2, value); }
        }
        string fNewClmStatus;
        [Size(15)]
        public string NewClmStatus
        {
            get { return fNewClmStatus; }
            set { SetPropertyValue<string>("NewClmStatus", ref fNewClmStatus, value); }
        }
        int fType1099;
        public int Type1099
        {
            get { return fType1099; }
            set { SetPropertyValue<int>("Type1099", ref fType1099, value); }
        }
        string fTaxName;
        [Size(150)]
        public string TaxName
        {
            get { return fTaxName; }
            set { SetPropertyValue<string>("TaxName", ref fTaxName, value); }
        }
        string fTaxIDNo;
        [Size(150)]
        public string TaxIDNo
        {
            get { return fTaxIDNo; }
            set { SetPropertyValue<string>("TaxIDNo", ref fTaxIDNo, value); }
        }
        string fPayeeOne;
        [Size(150)]
        public string PayeeOne
        {
            get { return fPayeeOne; }
            set { SetPropertyValue<string>("PayeeOne", ref fPayeeOne, value); }
        }
        bool fUseAddr1;
        public bool UseAddr1
        {
            get { return fUseAddr1; }
            set { SetPropertyValue<bool>("UseAddr1", ref fUseAddr1, value); }
        }
        string fPayeeTwo;
        [Size(150)]
        public string PayeeTwo
        {
            get { return fPayeeTwo; }
            set { SetPropertyValue<string>("PayeeTwo", ref fPayeeTwo, value); }
        }
        bool fUseAddr2;
        public bool UseAddr2
        {
            get { return fUseAddr2; }
            set { SetPropertyValue<bool>("UseAddr2", ref fUseAddr2, value); }
        }
        string fStreet;
        [Size(35)]
        public string Street
        {
            get { return fStreet; }
            set { SetPropertyValue<string>("Street", ref fStreet, value); }
        }
        string fCity;
        [Size(25)]
        public string City
        {
            get { return fCity; }
            set { SetPropertyValue<string>("City", ref fCity, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fZip;
        [Size(5)]
        public string Zip
        {
            get { return fZip; }
            set { SetPropertyValue<string>("Zip", ref fZip, value); }
        }
        bool fPickupChk;
        public bool PickupChk
        {
            get { return fPickupChk; }
            set { SetPropertyValue<bool>("PickupChk", ref fPickupChk, value); }
        }
        double fPayAmt;
        public double PayAmt
        {
            get { return fPayAmt; }
            set { SetPropertyValue<double>("PayAmt", ref fPayAmt, value); }
        }
        int fPeril;
        public int Peril
        {
            get { return fPeril; }
            set { SetPropertyValue<int>("Peril", ref fPeril, value); }
        }
        string fxxUser;
        [Size(30)]
        public string xxUser
        {
            get { return fxxUser; }
            set { SetPropertyValue<string>("xxUser", ref fxxUser, value); }
        }
        bool fAllocated;
        public bool Allocated
        {
            get { return fAllocated; }
            set { SetPropertyValue<bool>("Allocated", ref fAllocated, value); }
        }
        bool fPIPPayment;
        public bool PIPPayment
        {
            get { return fPIPPayment; }
            set { SetPropertyValue<bool>("PIPPayment", ref fPIPPayment, value); }
        }
        string fPIPBillKey;
        [Size(20)]
        public string PIPBillKey
        {
            get { return fPIPBillKey; }
            set { SetPropertyValue<string>("PIPBillKey", ref fPIPBillKey, value); }
        }
        double fPIPLAEAmt;
        public double PIPLAEAmt
        {
            get { return fPIPLAEAmt; }
            set { SetPropertyValue<double>("PIPLAEAmt", ref fPIPLAEAmt, value); }
        }
        bool fxxBulk;
        public bool xxBulk
        {
            get { return fxxBulk; }
            set { SetPropertyValue<bool>("xxBulk", ref fxxBulk, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPerilDesc;
        [Size(10)]
        public string PerilDesc
        {
            get { return fPerilDesc; }
            set { SetPropertyValue<string>("PerilDesc", ref fPerilDesc, value); }
        }
        string fPayAmtInTxt;
        [Size(400)]
        public string PayAmtInTxt
        {
            get { return fPayAmtInTxt; }
            set { SetPropertyValue<string>("PayAmtInTxt", ref fPayAmtInTxt, value); }
        }
        string fBatchNo;
        [Size(20)]
        public string BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<string>("BatchNo", ref fBatchNo, value); }
        }
        string fBillKey;
        [Size(20)]
        public string BillKey
        {
            get { return fBillKey; }
            set { SetPropertyValue<string>("BillKey", ref fBillKey, value); }
        }
        string fTransmittal;
        [Size(8000)]
        public string Transmittal
        {
            get { return fTransmittal; }
            set { SetPropertyValue<string>("Transmittal", ref fTransmittal, value); }
        }
        public XpoClaimsChecksPending(Session session) : base(session) { }
        public XpoClaimsChecksPending() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimsDocumentRequest
    {
        int IndexID { get; set; }
        string ClaimNo { get; set; }
        string EmailAddress { get; set; }
        bool IsProcessed { get; set; }
        DateTime DateRequested { get; set; }
        DateTime DateProcessed { get; set; }
    }

    public class ClaimsDocumentRequest : IClaimsDocumentRequest
    {
        public int IndexID { get; set; }
        public string ClaimNo { get; set; }
        public string EmailAddress { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime DateProcessed { get; set; }
    }

    [Persistent("ClaimsDocumentRequest")]
    public class XpoClaimsDocumentRequest : XPLiteObject, IClaimsDocumentRequest
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        string fEmailAddress;
        [Size(500)]
        public string EmailAddress
        {
            get { return fEmailAddress; }
            set { SetPropertyValue<string>("EmailAddress", ref fEmailAddress, value); }
        }
        bool fIsProcessed;
        public bool IsProcessed
        {
            get { return fIsProcessed; }
            set { SetPropertyValue<bool>("IsProcessed", ref fIsProcessed, value); }
        }
        DateTime fDateRequested;
        public DateTime DateRequested
        {
            get { return fDateRequested; }
            set { SetPropertyValue<DateTime>("DateRequested", ref fDateRequested, value); }
        }
        DateTime fDateProcessed;
        public DateTime DateProcessed
        {
            get { return fDateProcessed; }
            set { SetPropertyValue<DateTime>("DateProcessed", ref fDateProcessed, value); }
        }
        public XpoClaimsDocumentRequest(Session session) : base(session) { }
        public XpoClaimsDocumentRequest() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimsOnlyDriver
    {
        string PolicyNo { get; set; }
        int DriverIndex { get; set; }
        int IndexID { get; set; }
        string Firstname { get; set; }
        string MI { get; set; }
        string Lastname { get; set; }
        DateTime DOB { get; set; }
        DateTime DateLicensed { get; set; }
        string Gender { get; set; }
        string LicenseSt { get; set; }
        string LicenseNo { get; set; }
        bool Exclude { get; set; }
    }

    public class ClaimsOnlyDriver : IClaimsOnlyDriver
    {
        public string PolicyNo { get; set; }
        public int DriverIndex { get; set; }
        public int IndexID { get; set; }
        public string Firstname { get; set; }
        public string MI { get; set; }
        public string Lastname { get; set; }
        public DateTime DOB { get; set; }
        public DateTime DateLicensed { get; set; }
        public string Gender { get; set; }
        public string LicenseSt { get; set; }
        public string LicenseNo { get; set; }
        public bool Exclude { get; set; }
    }

    [Persistent("ClaimsOnlyDriver")]
    public class XpoClaimsOnlyDriver : XPLiteObject, IClaimsOnlyDriver
    {
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fFirstname;
        [Size(75)]
        public string Firstname
        {
            get { return fFirstname; }
            set { SetPropertyValue<string>("Firstname", ref fFirstname, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fLastname;
        [Size(75)]
        public string Lastname
        {
            get { return fLastname; }
            set { SetPropertyValue<string>("Lastname", ref fLastname, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        string fGender;
        [Size(6)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        public XpoClaimsOnlyDriver(Session session) : base(session) { }
        public XpoClaimsOnlyDriver() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IAuditHistoryActivity
    {
        int IndexID { get; set; }
        string PolicyNo { get; set; }
        int HistoryID { get; set; }
        string TableName { get; set; }
        int IndexNo { get; set; }
        int SubIndexNo { get; set; }
        string ColumnName { get; set; }
        string ColumnDesc { get; set; }
        string FieldType { get; set; }
        string Value1 { get; set; }
        string Value2 { get; set; }
    }

    public class AuditHistoryActivity :  IAuditHistoryActivity
    {
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public int HistoryID { get; set; }
        public string TableName { get; set; }
        public int IndexNo { get; set; }
        public int SubIndexNo { get; set; }
        public string ColumnName { get; set; }
        public string ColumnDesc { get; set; }
        public string FieldType { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
    }

    [Persistent("AuditHistoryActivity")]
    public class XpoAuditHistoryActivity : XPLiteObject, IAuditHistoryActivity
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fHistoryID;
        public int HistoryID
        {
            get { return fHistoryID; }
            set { SetPropertyValue<int>("HistoryID", ref fHistoryID, value); }
        }
        string fTableName;
        public string TableName
        {
            get { return fTableName; }
            set { SetPropertyValue<string>("TableName", ref fTableName, value); }
        }
        int fIndexNo;
        public int IndexNo
        {
            get { return fIndexNo; }
            set { SetPropertyValue<int>("IndexNo", ref fIndexNo, value); }
        }
        int fSubIndexNo;
        public int SubIndexNo
        {
            get { return fSubIndexNo; }
            set { SetPropertyValue<int>("SubIndexNo", ref fSubIndexNo, value); }
        }
        string fColumnName;
        public string ColumnName
        {
            get { return fColumnName; }
            set { SetPropertyValue<string>("ColumnName", ref fColumnName, value); }
        }
        string fColumnDesc;
        public string ColumnDesc
        {
            get { return fColumnDesc; }
            set { SetPropertyValue<string>("ColumnDesc", ref fColumnDesc, value); }
        }
        string fFieldType;
        [Size(20)]
        public string FieldType
        {
            get { return fFieldType; }
            set { SetPropertyValue<string>("FieldType", ref fFieldType, value); }
        }
        string fValue1;
        [Size(8000)]
        public string Value1
        {
            get { return fValue1; }
            set { SetPropertyValue<string>("Value1", ref fValue1, value); }
        }
        string fValue2;
        [Size(8000)]
        public string Value2
        {
            get { return fValue2; }
            set { SetPropertyValue<string>("Value2", ref fValue2, value); }
        }
        public XpoAuditHistoryActivity(Session session) : base(session) { }
        public XpoAuditHistoryActivity() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public interface IClaimantsStaging
    {
        string ClaimNo { get; set; }
        int Claimant { get; set; }
        string ClaimantName { get; set; }
        string PolicyNo { get; set; }
        int Peril { get; set; }
        string PerilDesc { get; set; }
        int CompExt { get; set; }
        string CompExtDesc { get; set; }
        string CatNo { get; set; }
        DateTime DOL { get; set; }
        DateTime RptDate { get; set; }
        DateTime EntryDate { get; set; }
        string ClmntAddress { get; set; }
        string ClmntCity { get; set; }
        string ClmntState { get; set; }
        string ClmntZip { get; set; }
        string ClmntHome { get; set; }
        string ClmntWork { get; set; }
        string YearMake { get; set; }
        string BodyStyle { get; set; }
        string PriorDamage { get; set; }
        string TagInfo { get; set; }
        string Color { get; set; }
        string Location { get; set; }
        string BodyShop { get; set; }
        string SalvageYard { get; set; }
        bool Driveable { get; set; }
        bool TotalLoss { get; set; }
        string ProvCode { get; set; }
        string ProvName { get; set; }
        string ProvAddr { get; set; }
        string ProvCity { get; set; }
        string ProvSt { get; set; }
        string ProvZip { get; set; }
        string ProvPhone { get; set; }
        string ProvFax { get; set; }
        string AttyCode { get; set; }
        string AttyName { get; set; }
        string Status { get; set; }
        string ReserveType { get; set; }
        int ReserveAmt { get; set; }
        DateTime OrigResDate { get; set; }
        string FileLoc { get; set; }
        string Adjuster { get; set; }
        string DescOfLoss { get; set; }
        int ALAE { get; set; }
        int ULAE { get; set; }
        string Supervisor { get; set; }
        string SIU { get; set; }
        DateTime StatusDate { get; set; }
        int ClaimantIndex { get; set; }
        double PDClaimantOverride { get; set; }
        double PIPClaimantOverride { get; set; }
        bool ADNDInsured { get; set; }
        bool ADNDSeatBelt { get; set; }
        int ADNDInjuryLevel { get; set; }
        string PropertyDesc { get; set; }
        string PropertyDamage { get; set; }
        string PropertyLocation { get; set; }
        string PropertyPOC { get; set; }
        string PropertyAddr { get; set; }
        string PropertyCity { get; set; }
        string PropertyState { get; set; }
        string PropertyZip { get; set; }
        string PropertyPhone { get; set; }
        string PropertyAltPhone { get; set; }
        string RatingState { get; set; }
        string HICN { get; set; }
        string SSN { get; set; }
        DateTime DOB { get; set; }
        string Gender { get; set; }
        string CMSLastName { get; set; }
        string CMSFirstName { get; set; }
        string CMSMiddleInital { get; set; }
        bool PropertyIsVeh { get; set; }
        bool TheftRecovery { get; set; }
        bool OwnerRetSalvage { get; set; }
        DateTime RecoveryDate { get; set; }
        string RecoveryAgency { get; set; }
        string RecoveryState { get; set; }
        string RecoveryCondition { get; set; }
        string VehVIN { get; set; }
        bool ClmtIsBusiness { get; set; }
        string BusinessName { get; set; }
        bool ForceSubmit { get; set; }
        string FirstName { get; set; }
        string MI { get; set; }
        string LastName { get; set; }
        string Suffix { get; set; }
        string ClmtLanguage { get; set; }
        bool FastTrack { get; set; }
        int ID { get; set; }
        string ClaimantFirstName { get; set; }
        string ClaimantLastName { get; set; }
        string ClaimantMI { get; set; }
        string PhyDamAreaDamaged { get; set; }
        string PropDamAreaDamaged { get; set; }
        int VehYear { get; set; }
        int PaidAmt { get; set; }
        int SalvRecAmt { get; set; }
        int SubroAmt { get; set; }
        bool IsInsured { get; set; }
        bool IsCovInvestigation { get; set; }
        string WitnessesStatements { get; set; }
        string MedAttorneyTaxID { get; set; }
        bool IsPropDamSubroClaim { get; set; }
        bool SuitDemandRec { get; set; }
        DateTime SuitDateRec { get; set; }
        bool IsInSuit { get; set; }
        DateTime InSuitDate { get; set; }
        string DefenseTaxID { get; set; }
        string InsuredRepTaxID { get; set; }
        string SuitNotes { get; set; }
        bool IsPreSuit { get; set; }
        DateTime PreSuitDate { get; set; }
    }

    public class ClaimantsStaging : IClaimantsStaging
    {
        public string ClaimNo { get; set; }
        public int Claimant { get; set; }
        public string ClaimantName { get; set; }
        public string PolicyNo { get; set; }
        public int Peril { get; set; }
        public string PerilDesc { get; set; }
        public int CompExt { get; set; }
        public string CompExtDesc { get; set; }
        public string CatNo { get; set; }
        public DateTime DOL { get; set; }
        public DateTime RptDate { get; set; }
        public DateTime EntryDate { get; set; }
        public string ClmntAddress { get; set; }
        public string ClmntCity { get; set; }
        public string ClmntState { get; set; }
        public string ClmntZip { get; set; }
        public string ClmntHome { get; set; }
        public string ClmntWork { get; set; }
        public string YearMake { get; set; }
        public string BodyStyle { get; set; }
        public string PriorDamage { get; set; }
        public string TagInfo { get; set; }
        public string Color { get; set; }
        public string Location { get; set; }
        public string BodyShop { get; set; }
        public string SalvageYard { get; set; }
        public bool Driveable { get; set; }
        public bool TotalLoss { get; set; }
        public string ProvCode { get; set; }
        public string ProvName { get; set; }
        public string ProvAddr { get; set; }
        public string ProvCity { get; set; }
        public string ProvSt { get; set; }
        public string ProvZip { get; set; }
        public string ProvPhone { get; set; }
        public string ProvFax { get; set; }
        public string AttyCode { get; set; }
        public string AttyName { get; set; }
        public string Status { get; set; }
        public string ReserveType { get; set; }
        public int ReserveAmt { get; set; }
        public DateTime OrigResDate { get; set; }
        public string FileLoc { get; set; }
        public string Adjuster { get; set; }
        public string DescOfLoss { get; set; }
        public int ALAE { get; set; }
        public int ULAE { get; set; }
        public string Supervisor { get; set; }
        public string SIU { get; set; }
        public DateTime StatusDate { get; set; }
        public int ClaimantIndex { get; set; }
        public double PDClaimantOverride { get; set; }
        public double PIPClaimantOverride { get; set; }
        public bool ADNDInsured { get; set; }
        public bool ADNDSeatBelt { get; set; }
        public int ADNDInjuryLevel { get; set; }
        public string PropertyDesc { get; set; }
        public string PropertyDamage { get; set; }
        public string PropertyLocation { get; set; }
        public string PropertyPOC { get; set; }
        public string PropertyAddr { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public string PropertyZip { get; set; }
        public string PropertyPhone { get; set; }
        public string PropertyAltPhone { get; set; }
        public string RatingState { get; set; }
        public string HICN { get; set; }
        public string SSN { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public string CMSLastName { get; set; }
        public string CMSFirstName { get; set; }
        public string CMSMiddleInital { get; set; }
        public bool PropertyIsVeh { get; set; }
        public bool TheftRecovery { get; set; }
        public bool OwnerRetSalvage { get; set; }
        public DateTime RecoveryDate { get; set; }
        public string RecoveryAgency { get; set; }
        public string RecoveryState { get; set; }
        public string RecoveryCondition { get; set; }
        public string VehVIN { get; set; }
        public bool ClmtIsBusiness { get; set; }
        public string BusinessName { get; set; }
        public bool ForceSubmit { get; set; }
        public string FirstName { get; set; }
        public string MI { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string ClmtLanguage { get; set; }
        public bool FastTrack { get; set; }
        public int ID { get; set; }
        public string ClaimantFirstName { get; set; }
        public string ClaimantLastName { get; set; }
        public string ClaimantMI { get; set; }
        public string PhyDamAreaDamaged { get; set; }
        public string PropDamAreaDamaged { get; set; }
        public int VehYear { get; set; }
        public int PaidAmt { get; set; }
        public int SalvRecAmt { get; set; }
        public int SubroAmt { get; set; }
        public bool IsInsured { get; set; }
        public bool IsCovInvestigation { get; set; }
        public string WitnessesStatements { get; set; }
        public string MedAttorneyTaxID { get; set; }
        public bool IsPropDamSubroClaim { get; set; }
        public bool SuitDemandRec { get; set; }
        public DateTime SuitDateRec { get; set; }
        public bool IsInSuit { get; set; }
        public DateTime InSuitDate { get; set; }
        public string DefenseTaxID { get; set; }
        public string InsuredRepTaxID { get; set; }
        public string SuitNotes { get; set; }
        public bool IsPreSuit { get; set; }
        public DateTime PreSuitDate { get; set; }
    }

    [Persistent("ClaimantsStaging")]
    public class XpoClaimantsStaging : XPLiteObject, IClaimantsStaging
    {
        string fClaimNo;
        [Size(75)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fClaimantName;
        [Size(105)]
        public string ClaimantName
        {
            get { return fClaimantName; }
            set { SetPropertyValue<string>("ClaimantName", ref fClaimantName, value); }
        }
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fPeril;
        public int Peril
        {
            get { return fPeril; }
            set { SetPropertyValue<int>("Peril", ref fPeril, value); }
        }
        string fPerilDesc;
        public string PerilDesc
        {
            get { return fPerilDesc; }
            set { SetPropertyValue<string>("PerilDesc", ref fPerilDesc, value); }
        }
        int fCompExt;
        public int CompExt
        {
            get { return fCompExt; }
            set { SetPropertyValue<int>("CompExt", ref fCompExt, value); }
        }
        string fCompExtDesc;
        [Size(25)]
        public string CompExtDesc
        {
            get { return fCompExtDesc; }
            set { SetPropertyValue<string>("CompExtDesc", ref fCompExtDesc, value); }
        }
        string fCatNo;
        [Size(8)]
        public string CatNo
        {
            get { return fCatNo; }
            set { SetPropertyValue<string>("CatNo", ref fCatNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fRptDate;
        public DateTime RptDate
        {
            get { return fRptDate; }
            set { SetPropertyValue<DateTime>("RptDate", ref fRptDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fClmntAddress;
        [Size(200)]
        public string ClmntAddress
        {
            get { return fClmntAddress; }
            set { SetPropertyValue<string>("ClmntAddress", ref fClmntAddress, value); }
        }
        string fClmntCity;
        [Size(30)]
        public string ClmntCity
        {
            get { return fClmntCity; }
            set { SetPropertyValue<string>("ClmntCity", ref fClmntCity, value); }
        }
        string fClmntState;
        [Size(2)]
        public string ClmntState
        {
            get { return fClmntState; }
            set { SetPropertyValue<string>("ClmntState", ref fClmntState, value); }
        }
        string fClmntZip;
        [Size(10)]
        public string ClmntZip
        {
            get { return fClmntZip; }
            set { SetPropertyValue<string>("ClmntZip", ref fClmntZip, value); }
        }
        string fClmntHome;
        [Size(14)]
        public string ClmntHome
        {
            get { return fClmntHome; }
            set { SetPropertyValue<string>("ClmntHome", ref fClmntHome, value); }
        }
        string fClmntWork;
        [Size(14)]
        public string ClmntWork
        {
            get { return fClmntWork; }
            set { SetPropertyValue<string>("ClmntWork", ref fClmntWork, value); }
        }
        string fYearMake;
        [Size(25)]
        public string YearMake
        {
            get { return fYearMake; }
            set { SetPropertyValue<string>("YearMake", ref fYearMake, value); }
        }
        string fBodyStyle;
        [Size(25)]
        public string BodyStyle
        {
            get { return fBodyStyle; }
            set { SetPropertyValue<string>("BodyStyle", ref fBodyStyle, value); }
        }
        string fPriorDamage;
        [Size(25)]
        public string PriorDamage
        {
            get { return fPriorDamage; }
            set { SetPropertyValue<string>("PriorDamage", ref fPriorDamage, value); }
        }
        string fTagInfo;
        [Size(10)]
        public string TagInfo
        {
            get { return fTagInfo; }
            set { SetPropertyValue<string>("TagInfo", ref fTagInfo, value); }
        }
        string fColor;
        [Size(10)]
        public string Color
        {
            get { return fColor; }
            set { SetPropertyValue<string>("Color", ref fColor, value); }
        }
        string fLocation;
        [Size(25)]
        public string Location
        {
            get { return fLocation; }
            set { SetPropertyValue<string>("Location", ref fLocation, value); }
        }
        string fBodyShop;
        [Size(25)]
        public string BodyShop
        {
            get { return fBodyShop; }
            set { SetPropertyValue<string>("BodyShop", ref fBodyShop, value); }
        }
        string fSalvageYard;
        [Size(25)]
        public string SalvageYard
        {
            get { return fSalvageYard; }
            set { SetPropertyValue<string>("SalvageYard", ref fSalvageYard, value); }
        }
        bool fDriveable;
        public bool Driveable
        {
            get { return fDriveable; }
            set { SetPropertyValue<bool>("Driveable", ref fDriveable, value); }
        }
        bool fTotalLoss;
        public bool TotalLoss
        {
            get { return fTotalLoss; }
            set { SetPropertyValue<bool>("TotalLoss", ref fTotalLoss, value); }
        }
        string fProvCode;
        public string ProvCode
        {
            get { return fProvCode; }
            set { SetPropertyValue<string>("ProvCode", ref fProvCode, value); }
        }
        string fProvName;
        public string ProvName
        {
            get { return fProvName; }
            set { SetPropertyValue<string>("ProvName", ref fProvName, value); }
        }
        string fProvAddr;
        public string ProvAddr
        {
            get { return fProvAddr; }
            set { SetPropertyValue<string>("ProvAddr", ref fProvAddr, value); }
        }
        string fProvCity;
        [Size(25)]
        public string ProvCity
        {
            get { return fProvCity; }
            set { SetPropertyValue<string>("ProvCity", ref fProvCity, value); }
        }
        string fProvSt;
        [Size(2)]
        public string ProvSt
        {
            get { return fProvSt; }
            set { SetPropertyValue<string>("ProvSt", ref fProvSt, value); }
        }
        string fProvZip;
        [Size(10)]
        public string ProvZip
        {
            get { return fProvZip; }
            set { SetPropertyValue<string>("ProvZip", ref fProvZip, value); }
        }
        string fProvPhone;
        [Size(14)]
        public string ProvPhone
        {
            get { return fProvPhone; }
            set { SetPropertyValue<string>("ProvPhone", ref fProvPhone, value); }
        }
        string fProvFax;
        [Size(14)]
        public string ProvFax
        {
            get { return fProvFax; }
            set { SetPropertyValue<string>("ProvFax", ref fProvFax, value); }
        }
        string fAttyCode;
        public string AttyCode
        {
            get { return fAttyCode; }
            set { SetPropertyValue<string>("AttyCode", ref fAttyCode, value); }
        }
        string fAttyName;
        public string AttyName
        {
            get { return fAttyName; }
            set { SetPropertyValue<string>("AttyName", ref fAttyName, value); }
        }
        string fStatus;
        [Size(15)]
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        string fReserveType;
        [Size(1)]
        public string ReserveType
        {
            get { return fReserveType; }
            set { SetPropertyValue<string>("ReserveType", ref fReserveType, value); }
        }
        int fReserveAmt;
        public int ReserveAmt
        {
            get { return fReserveAmt; }
            set { SetPropertyValue<int>("ReserveAmt", ref fReserveAmt, value); }
        }
        DateTime fOrigResDate;
        public DateTime OrigResDate
        {
            get { return fOrigResDate; }
            set { SetPropertyValue<DateTime>("OrigResDate", ref fOrigResDate, value); }
        }
        string fFileLoc;
        [Size(20)]
        public string FileLoc
        {
            get { return fFileLoc; }
            set { SetPropertyValue<string>("FileLoc", ref fFileLoc, value); }
        }
        string fAdjuster;
        [Size(20)]
        public string Adjuster
        {
            get { return fAdjuster; }
            set { SetPropertyValue<string>("Adjuster", ref fAdjuster, value); }
        }
        string fDescOfLoss;
        [Size(200)]
        public string DescOfLoss
        {
            get { return fDescOfLoss; }
            set { SetPropertyValue<string>("DescOfLoss", ref fDescOfLoss, value); }
        }
        int fALAE;
        public int ALAE
        {
            get { return fALAE; }
            set { SetPropertyValue<int>("ALAE", ref fALAE, value); }
        }
        int fULAE;
        public int ULAE
        {
            get { return fULAE; }
            set { SetPropertyValue<int>("ULAE", ref fULAE, value); }
        }
        string fSupervisor;
        [Size(250)]
        public string Supervisor
        {
            get { return fSupervisor; }
            set { SetPropertyValue<string>("Supervisor", ref fSupervisor, value); }
        }
        string fSIU;
        [Size(10)]
        public string SIU
        {
            get { return fSIU; }
            set { SetPropertyValue<string>("SIU", ref fSIU, value); }
        }
        DateTime fStatusDate;
        public DateTime StatusDate
        {
            get { return fStatusDate; }
            set { SetPropertyValue<DateTime>("StatusDate", ref fStatusDate, value); }
        }
        int fClaimantIndex;
        public int ClaimantIndex
        {
            get { return fClaimantIndex; }
            set { SetPropertyValue<int>("ClaimantIndex", ref fClaimantIndex, value); }
        }
        double fPDClaimantOverride;
        public double PDClaimantOverride
        {
            get { return fPDClaimantOverride; }
            set { SetPropertyValue<double>("PDClaimantOverride", ref fPDClaimantOverride, value); }
        }
        double fPIPClaimantOverride;
        public double PIPClaimantOverride
        {
            get { return fPIPClaimantOverride; }
            set { SetPropertyValue<double>("PIPClaimantOverride", ref fPIPClaimantOverride, value); }
        }
        bool fADNDInsured;
        public bool ADNDInsured
        {
            get { return fADNDInsured; }
            set { SetPropertyValue<bool>("ADNDInsured", ref fADNDInsured, value); }
        }
        bool fADNDSeatBelt;
        public bool ADNDSeatBelt
        {
            get { return fADNDSeatBelt; }
            set { SetPropertyValue<bool>("ADNDSeatBelt", ref fADNDSeatBelt, value); }
        }
        int fADNDInjuryLevel;
        public int ADNDInjuryLevel
        {
            get { return fADNDInjuryLevel; }
            set { SetPropertyValue<int>("ADNDInjuryLevel", ref fADNDInjuryLevel, value); }
        }
        string fPropertyDesc;
        [Size(30)]
        public string PropertyDesc
        {
            get { return fPropertyDesc; }
            set { SetPropertyValue<string>("PropertyDesc", ref fPropertyDesc, value); }
        }
        string fPropertyDamage;
        [Size(30)]
        public string PropertyDamage
        {
            get { return fPropertyDamage; }
            set { SetPropertyValue<string>("PropertyDamage", ref fPropertyDamage, value); }
        }
        string fPropertyLocation;
        [Size(30)]
        public string PropertyLocation
        {
            get { return fPropertyLocation; }
            set { SetPropertyValue<string>("PropertyLocation", ref fPropertyLocation, value); }
        }
        string fPropertyPOC;
        [Size(30)]
        public string PropertyPOC
        {
            get { return fPropertyPOC; }
            set { SetPropertyValue<string>("PropertyPOC", ref fPropertyPOC, value); }
        }
        string fPropertyAddr;
        [Size(30)]
        public string PropertyAddr
        {
            get { return fPropertyAddr; }
            set { SetPropertyValue<string>("PropertyAddr", ref fPropertyAddr, value); }
        }
        string fPropertyCity;
        [Size(20)]
        public string PropertyCity
        {
            get { return fPropertyCity; }
            set { SetPropertyValue<string>("PropertyCity", ref fPropertyCity, value); }
        }
        string fPropertyState;
        [Size(2)]
        public string PropertyState
        {
            get { return fPropertyState; }
            set { SetPropertyValue<string>("PropertyState", ref fPropertyState, value); }
        }
        string fPropertyZip;
        [Size(5)]
        public string PropertyZip
        {
            get { return fPropertyZip; }
            set { SetPropertyValue<string>("PropertyZip", ref fPropertyZip, value); }
        }
        string fPropertyPhone;
        [Size(14)]
        public string PropertyPhone
        {
            get { return fPropertyPhone; }
            set { SetPropertyValue<string>("PropertyPhone", ref fPropertyPhone, value); }
        }
        string fPropertyAltPhone;
        [Size(14)]
        public string PropertyAltPhone
        {
            get { return fPropertyAltPhone; }
            set { SetPropertyValue<string>("PropertyAltPhone", ref fPropertyAltPhone, value); }
        }
        string fRatingState;
        [Size(2)]
        public string RatingState
        {
            get { return fRatingState; }
            set { SetPropertyValue<string>("RatingState", ref fRatingState, value); }
        }
        string fHICN;
        [Size(15)]
        public string HICN
        {
            get { return fHICN; }
            set { SetPropertyValue<string>("HICN", ref fHICN, value); }
        }
        string fSSN;
        [Size(200)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        string fGender;
        [Size(1)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        string fCMSLastName;
        [Size(25)]
        public string CMSLastName
        {
            get { return fCMSLastName; }
            set { SetPropertyValue<string>("CMSLastName", ref fCMSLastName, value); }
        }
        string fCMSFirstName;
        [Size(25)]
        public string CMSFirstName
        {
            get { return fCMSFirstName; }
            set { SetPropertyValue<string>("CMSFirstName", ref fCMSFirstName, value); }
        }
        string fCMSMiddleInital;
        [Size(1)]
        public string CMSMiddleInital
        {
            get { return fCMSMiddleInital; }
            set { SetPropertyValue<string>("CMSMiddleInital", ref fCMSMiddleInital, value); }
        }
        bool fPropertyIsVeh;
        public bool PropertyIsVeh
        {
            get { return fPropertyIsVeh; }
            set { SetPropertyValue<bool>("PropertyIsVeh", ref fPropertyIsVeh, value); }
        }
        bool fTheftRecovery;
        public bool TheftRecovery
        {
            get { return fTheftRecovery; }
            set { SetPropertyValue<bool>("TheftRecovery", ref fTheftRecovery, value); }
        }
        bool fOwnerRetSalvage;
        public bool OwnerRetSalvage
        {
            get { return fOwnerRetSalvage; }
            set { SetPropertyValue<bool>("OwnerRetSalvage", ref fOwnerRetSalvage, value); }
        }
        DateTime fRecoveryDate;
        public DateTime RecoveryDate
        {
            get { return fRecoveryDate; }
            set { SetPropertyValue<DateTime>("RecoveryDate", ref fRecoveryDate, value); }
        }
        string fRecoveryAgency;
        [Size(30)]
        public string RecoveryAgency
        {
            get { return fRecoveryAgency; }
            set { SetPropertyValue<string>("RecoveryAgency", ref fRecoveryAgency, value); }
        }
        string fRecoveryState;
        [Size(2)]
        public string RecoveryState
        {
            get { return fRecoveryState; }
            set { SetPropertyValue<string>("RecoveryState", ref fRecoveryState, value); }
        }
        string fRecoveryCondition;
        [Size(20)]
        public string RecoveryCondition
        {
            get { return fRecoveryCondition; }
            set { SetPropertyValue<string>("RecoveryCondition", ref fRecoveryCondition, value); }
        }
        string fVehVIN;
        [Size(20)]
        public string VehVIN
        {
            get { return fVehVIN; }
            set { SetPropertyValue<string>("VehVIN", ref fVehVIN, value); }
        }
        bool fClmtIsBusiness;
        public bool ClmtIsBusiness
        {
            get { return fClmtIsBusiness; }
            set { SetPropertyValue<bool>("ClmtIsBusiness", ref fClmtIsBusiness, value); }
        }
        string fBusinessName;
        [Size(35)]
        public string BusinessName
        {
            get { return fBusinessName; }
            set { SetPropertyValue<string>("BusinessName", ref fBusinessName, value); }
        }
        bool fForceSubmit;
        public bool ForceSubmit
        {
            get { return fForceSubmit; }
            set { SetPropertyValue<bool>("ForceSubmit", ref fForceSubmit, value); }
        }
        string fFirstName;
        [Size(50)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fLastName;
        [Size(50)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fSuffix;
        [Size(5)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClmtLanguage;
        [Size(20)]
        public string ClmtLanguage
        {
            get { return fClmtLanguage; }
            set { SetPropertyValue<string>("ClmtLanguage", ref fClmtLanguage, value); }
        }
        bool fFastTrack;
        public bool FastTrack
        {
            get { return fFastTrack; }
            set { SetPropertyValue<bool>("FastTrack", ref fFastTrack, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fClaimantFirstName;
        [Size(50)]
        public string ClaimantFirstName
        {
            get { return fClaimantFirstName; }
            set { SetPropertyValue<string>("ClaimantFirstName", ref fClaimantFirstName, value); }
        }
        string fClaimantLastName;
        [Size(50)]
        public string ClaimantLastName
        {
            get { return fClaimantLastName; }
            set { SetPropertyValue<string>("ClaimantLastName", ref fClaimantLastName, value); }
        }
        string fClaimantMI;
        [Size(1)]
        public string ClaimantMI
        {
            get { return fClaimantMI; }
            set { SetPropertyValue<string>("ClaimantMI", ref fClaimantMI, value); }
        }
        string fPhyDamAreaDamaged;
        [Size(12)]
        public string PhyDamAreaDamaged
        {
            get { return fPhyDamAreaDamaged; }
            set { SetPropertyValue<string>("PhyDamAreaDamaged", ref fPhyDamAreaDamaged, value); }
        }
        string fPropDamAreaDamaged;
        [Size(12)]
        public string PropDamAreaDamaged
        {
            get { return fPropDamAreaDamaged; }
            set { SetPropertyValue<string>("PropDamAreaDamaged", ref fPropDamAreaDamaged, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        int fPaidAmt;
        public int PaidAmt
        {
            get { return fPaidAmt; }
            set { SetPropertyValue<int>("PaidAmt", ref fPaidAmt, value); }
        }
        int fSalvRecAmt;
        public int SalvRecAmt
        {
            get { return fSalvRecAmt; }
            set { SetPropertyValue<int>("SalvRecAmt", ref fSalvRecAmt, value); }
        }
        int fSubroAmt;
        public int SubroAmt
        {
            get { return fSubroAmt; }
            set { SetPropertyValue<int>("SubroAmt", ref fSubroAmt, value); }
        }
        bool fIsInsured;
        public bool IsInsured
        {
            get { return fIsInsured; }
            set { SetPropertyValue<bool>("IsInsured", ref fIsInsured, value); }
        }
        bool fIsCovInvestigation;
        public bool IsCovInvestigation
        {
            get { return fIsCovInvestigation; }
            set { SetPropertyValue<bool>("IsCovInvestigation", ref fIsCovInvestigation, value); }
        }
        string fWitnessesStatements;
        [Size(8000)]
        public string WitnessesStatements
        {
            get { return fWitnessesStatements; }
            set { SetPropertyValue<string>("WitnessesStatements", ref fWitnessesStatements, value); }
        }
        string fMedAttorneyTaxID;
        [Size(150)]
        public string MedAttorneyTaxID
        {
            get { return fMedAttorneyTaxID; }
            set { SetPropertyValue<string>("MedAttorneyTaxID", ref fMedAttorneyTaxID, value); }
        }
        bool fIsPropDamSubroClaim;
        public bool IsPropDamSubroClaim
        {
            get { return fIsPropDamSubroClaim; }
            set { SetPropertyValue<bool>("IsPropDamSubroClaim", ref fIsPropDamSubroClaim, value); }
        }
        bool fSuitDemandRec;
        public bool SuitDemandRec
        {
            get { return fSuitDemandRec; }
            set { SetPropertyValue<bool>("SuitDemandRec", ref fSuitDemandRec, value); }
        }
        DateTime fSuitDateRec;
        public DateTime SuitDateRec
        {
            get { return fSuitDateRec; }
            set { SetPropertyValue<DateTime>("SuitDateRec", ref fSuitDateRec, value); }
        }
        bool fIsInSuit;
        public bool IsInSuit
        {
            get { return fIsInSuit; }
            set { SetPropertyValue<bool>("IsInSuit", ref fIsInSuit, value); }
        }
        DateTime fInSuitDate;
        public DateTime InSuitDate
        {
            get { return fInSuitDate; }
            set { SetPropertyValue<DateTime>("InSuitDate", ref fInSuitDate, value); }
        }
        string fDefenseTaxID;
        public string DefenseTaxID
        {
            get { return fDefenseTaxID; }
            set { SetPropertyValue<string>("DefenseTaxID", ref fDefenseTaxID, value); }
        }
        string fInsuredRepTaxID;
        public string InsuredRepTaxID
        {
            get { return fInsuredRepTaxID; }
            set { SetPropertyValue<string>("InsuredRepTaxID", ref fInsuredRepTaxID, value); }
        }
        string fSuitNotes;
        [Size(8000)]
        public string SuitNotes
        {
            get { return fSuitNotes; }
            set { SetPropertyValue<string>("SuitNotes", ref fSuitNotes, value); }
        }
        bool fIsPreSuit;
        public bool IsPreSuit
        {
            get { return fIsPreSuit; }
            set { SetPropertyValue<bool>("IsPreSuit", ref fIsPreSuit, value); }
        }
        DateTime fPreSuitDate;
        public DateTime PreSuitDate
        {
            get { return fPreSuitDate; }
            set { SetPropertyValue<DateTime>("PreSuitDate", ref fPreSuitDate, value); }
        }
        public XpoClaimantsStaging(Session session) : base(session) { }
        public XpoClaimantsStaging() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyDrivers : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public PolicyDrivers(Session session) : base(session) { }
        public PolicyDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Holidays : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        short fCalendarYear;
        public short CalendarYear
        {
            get { return fCalendarYear; }
            set { SetPropertyValue<short>("CalendarYear", ref fCalendarYear, value); }
        }
        string fHolidayDesc;
        public string HolidayDesc
        {
            get { return fHolidayDesc; }
            set { SetPropertyValue<string>("HolidayDesc", ref fHolidayDesc, value); }
        }
        DateTime fDateHoliday;
        public DateTime DateHoliday
        {
            get { return fDateHoliday; }
            set { SetPropertyValue<DateTime>("DateHoliday", ref fDateHoliday, value); }
        }
        public Holidays(Session session) : base(session) { }
        public Holidays() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Rights : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fShortDesc;
        [Size(200)]
        public string ShortDesc
        {
            get { return fShortDesc; }
            set { SetPropertyValue<string>("ShortDesc", ref fShortDesc, value); }
        }
        string fLongDesc;
        [Size(500)]
        public string LongDesc
        {
            get { return fLongDesc; }
            set { SetPropertyValue<string>("LongDesc", ref fLongDesc, value); }
        }
        string fDepartment;
        public string Department
        {
            get { return fDepartment; }
            set { SetPropertyValue<string>("Department", ref fDepartment, value); }
        }
        public Rights(Session session) : base(session) { }
        public Rights() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Reasons : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fAppID;
        public int AppID
        {
            get { return fAppID; }
            set { SetPropertyValue<int>("AppID", ref fAppID, value); }
        }
        string fCodeType;
        [Size(50)]
        public string CodeType
        {
            get { return fCodeType; }
            set { SetPropertyValue<string>("CodeType", ref fCodeType, value); }
        }
        string fCode;
        [Size(25)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>("Code", ref fCode, value); }
        }
        string fDescription;
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }
        int fLineOfBusiness;
        public int LineOfBusiness
        {
            get { return fLineOfBusiness; }
            set { SetPropertyValue<int>("LineOfBusiness", ref fLineOfBusiness, value); }
        }
        string fReasonType;
        [Size(10)]
        public string ReasonType
        {
            get { return fReasonType; }
            set { SetPropertyValue<string>("ReasonType", ref fReasonType, value); }
        }
        public Reasons(Session session) : base(session) { }
        public Reasons() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class SystemParameters : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fCompanyID;
        [Size(10)]
        public string CompanyID
        {
            get { return fCompanyID; }
            set { SetPropertyValue<string>("CompanyID", ref fCompanyID, value); }
        }
        string fStateCode;
        [Size(2)]
        public string StateCode
        {
            get { return fStateCode; }
            set { SetPropertyValue<string>("StateCode", ref fStateCode, value); }
        }
        string fLOB;
        [Size(25)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        int fDaysPastDueForLateFee;
        public int DaysPastDueForLateFee
        {
            get { return fDaysPastDueForLateFee; }
            set { SetPropertyValue<int>("DaysPastDueForLateFee", ref fDaysPastDueForLateFee, value); }
        }
        int fTotalMailDays;
        public int TotalMailDays
        {
            get { return fTotalMailDays; }
            set { SetPropertyValue<int>("TotalMailDays", ref fTotalMailDays, value); }
        }
        int fDaysForMailing;
        public int DaysForMailing
        {
            get { return fDaysForMailing; }
            set { SetPropertyValue<int>("DaysForMailing", ref fDaysForMailing, value); }
        }
        int fMinimumDaysForBill;
        public int MinimumDaysForBill
        {
            get { return fMinimumDaysForBill; }
            set { SetPropertyValue<int>("MinimumDaysForBill", ref fMinimumDaysForBill, value); }
        }
        string fCompanyName;
        [Size(80)]
        public string CompanyName
        {
            get { return fCompanyName; }
            set { SetPropertyValue<string>("CompanyName", ref fCompanyName, value); }
        }
        string fCompanyStreetAddress1;
        [Size(80)]
        public string CompanyStreetAddress1
        {
            get { return fCompanyStreetAddress1; }
            set { SetPropertyValue<string>("CompanyStreetAddress1", ref fCompanyStreetAddress1, value); }
        }
        string fCompanyStreetAddress2;
        [Size(80)]
        public string CompanyStreetAddress2
        {
            get { return fCompanyStreetAddress2; }
            set { SetPropertyValue<string>("CompanyStreetAddress2", ref fCompanyStreetAddress2, value); }
        }
        string fCompanyCity;
        [Size(40)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fCompanyPhone;
        [Size(25)]
        public string CompanyPhone
        {
            get { return fCompanyPhone; }
            set { SetPropertyValue<string>("CompanyPhone", ref fCompanyPhone, value); }
        }
        string fCompanyPhoneTollFree;
        [Size(25)]
        public string CompanyPhoneTollFree
        {
            get { return fCompanyPhoneTollFree; }
            set { SetPropertyValue<string>("CompanyPhoneTollFree", ref fCompanyPhoneTollFree, value); }
        }
        string fCompanyFax;
        [Size(25)]
        public string CompanyFax
        {
            get { return fCompanyFax; }
            set { SetPropertyValue<string>("CompanyFax", ref fCompanyFax, value); }
        }
        string fCompanyMailingAddress1;
        [Size(80)]
        public string CompanyMailingAddress1
        {
            get { return fCompanyMailingAddress1; }
            set { SetPropertyValue<string>("CompanyMailingAddress1", ref fCompanyMailingAddress1, value); }
        }
        string fCompanyMailingAddress2;
        [Size(80)]
        public string CompanyMailingAddress2
        {
            get { return fCompanyMailingAddress2; }
            set { SetPropertyValue<string>("CompanyMailingAddress2", ref fCompanyMailingAddress2, value); }
        }
        string fCompanyMailingCity;
        [Size(40)]
        public string CompanyMailingCity
        {
            get { return fCompanyMailingCity; }
            set { SetPropertyValue<string>("CompanyMailingCity", ref fCompanyMailingCity, value); }
        }
        string fCompanyMailingState;
        [Size(2)]
        public string CompanyMailingState
        {
            get { return fCompanyMailingState; }
            set { SetPropertyValue<string>("CompanyMailingState", ref fCompanyMailingState, value); }
        }
        string fCompanyMailingZip;
        [Size(15)]
        public string CompanyMailingZip
        {
            get { return fCompanyMailingZip; }
            set { SetPropertyValue<string>("CompanyMailingZip", ref fCompanyMailingZip, value); }
        }
        string fMGA;
        public string MGA
        {
            get { return fMGA; }
            set { SetPropertyValue<string>("MGA", ref fMGA, value); }
        }
        string fCompanyEmail;
        [Size(40)]
        public string CompanyEmail
        {
            get { return fCompanyEmail; }
            set { SetPropertyValue<string>("CompanyEmail", ref fCompanyEmail, value); }
        }
        string fWebsite;
        public string Website
        {
            get { return fWebsite; }
            set { SetPropertyValue<string>("Website", ref fWebsite, value); }
        }
        string fClaimsMailingAddress;
        [Size(80)]
        public string ClaimsMailingAddress
        {
            get { return fClaimsMailingAddress; }
            set { SetPropertyValue<string>("ClaimsMailingAddress", ref fClaimsMailingAddress, value); }
        }
        string fClaimsMailingCity;
        [Size(40)]
        public string ClaimsMailingCity
        {
            get { return fClaimsMailingCity; }
            set { SetPropertyValue<string>("ClaimsMailingCity", ref fClaimsMailingCity, value); }
        }
        string fClaimsMailingZip;
        [Size(15)]
        public string ClaimsMailingZip
        {
            get { return fClaimsMailingZip; }
            set { SetPropertyValue<string>("ClaimsMailingZip", ref fClaimsMailingZip, value); }
        }
        string fClaimsMailingState;
        [Size(2)]
        public string ClaimsMailingState
        {
            get { return fClaimsMailingState; }
            set { SetPropertyValue<string>("ClaimsMailingState", ref fClaimsMailingState, value); }
        }
        string fClaimsPhoneNumber;
        [Size(20)]
        public string ClaimsPhoneNumber
        {
            get { return fClaimsPhoneNumber; }
            set { SetPropertyValue<string>("ClaimsPhoneNumber", ref fClaimsPhoneNumber, value); }
        }
        string fPIPPhoneNumber;
        [Size(20)]
        public string PIPPhoneNumber
        {
            get { return fPIPPhoneNumber; }
            set { SetPropertyValue<string>("PIPPhoneNumber", ref fPIPPhoneNumber, value); }
        }
        string fClaimsFaxNumber;
        [Size(20)]
        public string ClaimsFaxNumber
        {
            get { return fClaimsFaxNumber; }
            set { SetPropertyValue<string>("ClaimsFaxNumber", ref fClaimsFaxNumber, value); }
        }
        string fUnderwritingFaxNumber;
        [Size(20)]
        public string UnderwritingFaxNumber
        {
            get { return fUnderwritingFaxNumber; }
            set { SetPropertyValue<string>("UnderwritingFaxNumber", ref fUnderwritingFaxNumber, value); }
        }
        string fMarketingFaxNumber;
        [Size(20)]
        public string MarketingFaxNumber
        {
            get { return fMarketingFaxNumber; }
            set { SetPropertyValue<string>("MarketingFaxNumber", ref fMarketingFaxNumber, value); }
        }
        string fCompanyCode;
        [Size(20)]
        public string CompanyCode
        {
            get { return fCompanyCode; }
            set { SetPropertyValue<string>("CompanyCode", ref fCompanyCode, value); }
        }
        string fClaimsAcctRoutingNo;
        [Size(2000)]
        public string ClaimsAcctRoutingNo
        {
            get { return fClaimsAcctRoutingNo; }
            set { SetPropertyValue<string>("ClaimsAcctRoutingNo", ref fClaimsAcctRoutingNo, value); }
        }
        string fClaimsAcctNo;
        [Size(2000)]
        public string ClaimsAcctNo
        {
            get { return fClaimsAcctNo; }
            set { SetPropertyValue<string>("ClaimsAcctNo", ref fClaimsAcctNo, value); }
        }
        string fClaimsAcctFractionalNo;
        [Size(2000)]
        public string ClaimsAcctFractionalNo
        {
            get { return fClaimsAcctFractionalNo; }
            set { SetPropertyValue<string>("ClaimsAcctFractionalNo", ref fClaimsAcctFractionalNo, value); }
        }
        string fUnderwritingMailingAddress;
        [Size(80)]
        public string UnderwritingMailingAddress
        {
            get { return fUnderwritingMailingAddress; }
            set { SetPropertyValue<string>("UnderwritingMailingAddress", ref fUnderwritingMailingAddress, value); }
        }
        string fUnderwritingMailingState;
        [Size(2)]
        public string UnderwritingMailingState
        {
            get { return fUnderwritingMailingState; }
            set { SetPropertyValue<string>("UnderwritingMailingState", ref fUnderwritingMailingState, value); }
        }
        string fUnderwritingMailingZip;
        [Size(15)]
        public string UnderwritingMailingZip
        {
            get { return fUnderwritingMailingZip; }
            set { SetPropertyValue<string>("UnderwritingMailingZip", ref fUnderwritingMailingZip, value); }
        }
        string fUnderwritingMailingCity;
        [Size(40)]
        public string UnderwritingMailingCity
        {
            get { return fUnderwritingMailingCity; }
            set { SetPropertyValue<string>("UnderwritingMailingCity", ref fUnderwritingMailingCity, value); }
        }
        public SystemParameters(Session session) : base(session) { }
        public SystemParameters() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyBindersCars : XPCustomObject
    {
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public PolicyBindersCars(Session session) : base(session) { }
        public PolicyBindersCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AgentLogins : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fPassword;
        [Size(2000)]
        public string Password
        {
            get { return fPassword; }
            set { SetPropertyValue<string>("Password", ref fPassword, value); }
        }
        string fStatus;
        [Size(250)]
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        DateTime fDate;
        public DateTime Date
        {
            get { return fDate; }
            set { SetPropertyValue<DateTime>("Date", ref fDate, value); }
        }
        string fAgentCode;
        [Size(20)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }

        bool fisActive;
        public bool IsActive
        {
            get { return fisActive; }
            set { SetPropertyValue<bool>("IsActive", ref fisActive, value); }
        }

        public AgentLogins(Session session) : base(session) { }
        public AgentLogins() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ProviderNotes : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fInternalCode;
        [Size(50)]
        public string InternalCode
        {
            get { return fInternalCode; }
            set { SetPropertyValue<string>("InternalCode", ref fInternalCode, value); }
        }
        int fIndexNo;
        public int IndexNo
        {
            get { return fIndexNo; }
            set { SetPropertyValue<int>("IndexNo", ref fIndexNo, value); }
        }
        string fUserBy;
        [Size(50)]
        public string UserBy
        {
            get { return fUserBy; }
            set { SetPropertyValue<string>("UserBy", ref fUserBy, value); }
        }
        string fNotes;
        [Size(8000)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        DateTime fDate;
        public DateTime Date
        {
            get { return fDate; }
            set { SetPropertyValue<DateTime>("Date", ref fDate, value); }
        }
        bool fisDiary;
        public bool isDiary
        {
            get { return fisDiary; }
            set { SetPropertyValue<bool>("isDiary", ref fisDiary, value); }
        }
        public ProviderNotes(Session session) : base(session) { }
        public ProviderNotes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class FLtRateCycles : XPLiteObject
    {
        public static FLtRateCycles GetCurrentCycle(Session session)
        {
            return new XPQuery<FLtRateCycles>(session)
                .Where(c => c.EffDate_NewBusiness <= DateTime.Now)
                .OrderByDescending(c => c.EffDate_NewBusiness)
                .FirstOrDefault();
        }

        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        DateTime fEffDate_NewBusiness;
        public DateTime EffDate_NewBusiness
        {
            get { return fEffDate_NewBusiness; }
            set { SetPropertyValue<DateTime>("EffDate_NewBusiness", ref fEffDate_NewBusiness, value); }
        }
        DateTime fEffDate_Renewals;
        public DateTime EffDate_Renewals
        {
            get { return fEffDate_Renewals; }
            set { SetPropertyValue<DateTime>("EffDate_Renewals", ref fEffDate_Renewals, value); }
        }
        public FLtRateCycles(Session session) : base(session) { }
        public FLtRateCycles() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyBinders : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPriorPolicyNo;
        [Size(15)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        [Size(5)]
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fpolicyCars;
        public int policyCars
        {
            get { return fpolicyCars; }
            set { SetPropertyValue<int>("policyCars", ref fpolicyCars, value); }
        }
        int fpolicyDrivers;
        public int policyDrivers
        {
            get { return fpolicyDrivers; }
            set { SetPropertyValue<int>("policyDrivers", ref fpolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        public PolicyBinders(Session session) : base(session) { }
        public PolicyBinders() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


    public class Claims : XPLiteObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fRptDate;
        public DateTime RptDate
        {
            get { return fRptDate; }
            set { SetPropertyValue<DateTime>("RptDate", ref fRptDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fPolicyRateCycle;
        [Size(50)]
        public string PolicyRateCycle
        {
            get { return fPolicyRateCycle; }
            set { SetPropertyValue<string>("PolicyRateCycle", ref fPolicyRateCycle, value); }
        }
        string fReinsuranceTreaty;
        [Size(50)]
        public string ReinsuranceTreaty
        {
            get { return fReinsuranceTreaty; }
            set { SetPropertyValue<string>("ReinsuranceTreaty", ref fReinsuranceTreaty, value); }
        }
        bool fMaterialMisrep;
        public bool MaterialMisrep
        {
            get { return fMaterialMisrep; }
            set { SetPropertyValue<bool>("MaterialMisrep", ref fMaterialMisrep, value); }
        }
        bool fAtFault;
        public bool AtFault
        {
            get { return fAtFault; }
            set { SetPropertyValue<bool>("AtFault", ref fAtFault, value); }
        }
        bool fInSuit;
        public bool InSuit
        {
            get { return fInSuit; }
            set { SetPropertyValue<bool>("InSuit", ref fInSuit, value); }
        }
        bool fSubro;
        public bool Subro
        {
            get { return fSubro; }
            set { SetPropertyValue<bool>("Subro", ref fSubro, value); }
        }
        bool fSalvage;
        public bool Salvage
        {
            get { return fSalvage; }
            set { SetPropertyValue<bool>("Salvage", ref fSalvage, value); }
        }
        bool fVehListed;
        public bool VehListed
        {
            get { return fVehListed; }
            set { SetPropertyValue<bool>("VehListed", ref fVehListed, value); }
        }
        int fVehNo;
        public int VehNo
        {
            get { return fVehNo; }
            set { SetPropertyValue<int>("VehNo", ref fVehNo, value); }
        }
        string fVehYear;
        [Size(4)]
        public string VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<string>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(75)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel;
        public string VehModel
        {
            get { return fVehModel; }
            set { SetPropertyValue<string>("VehModel", ref fVehModel, value); }
        }
        string fPriorDamage;
        public string PriorDamage
        {
            get { return fPriorDamage; }
            set { SetPropertyValue<string>("PriorDamage", ref fPriorDamage, value); }
        }
        bool fDrivListed;
        public bool DrivListed
        {
            get { return fDrivListed; }
            set { SetPropertyValue<bool>("DrivListed", ref fDrivListed, value); }
        }
        int fDrivNo;
        public int DrivNo
        {
            get { return fDrivNo; }
            set { SetPropertyValue<int>("DrivNo", ref fDrivNo, value); }
        }
        bool fResident;
        public bool Resident
        {
            get { return fResident; }
            set { SetPropertyValue<bool>("Resident", ref fResident, value); }
        }
        string fDrivName;
        public string DrivName
        {
            get { return fDrivName; }
            set { SetPropertyValue<string>("DrivName", ref fDrivName, value); }
        }
        int fDrivAge;
        public int DrivAge
        {
            get { return fDrivAge; }
            set { SetPropertyValue<int>("DrivAge", ref fDrivAge, value); }
        }
        string fDrivSex;
        [Size(1)]
        public string DrivSex
        {
            get { return fDrivSex; }
            set { SetPropertyValue<string>("DrivSex", ref fDrivSex, value); }
        }
        string fDrivRelation;
        [Size(50)]
        public string DrivRelation
        {
            get { return fDrivRelation; }
            set { SetPropertyValue<string>("DrivRelation", ref fDrivRelation, value); }
        }
        bool fCCForms;
        public bool CCForms
        {
            get { return fCCForms; }
            set { SetPropertyValue<bool>("CCForms", ref fCCForms, value); }
        }
        string fCCAddressee;
        [Size(25)]
        public string CCAddressee
        {
            get { return fCCAddressee; }
            set { SetPropertyValue<string>("CCAddressee", ref fCCAddressee, value); }
        }
        string fCCAddress;
        [Size(25)]
        public string CCAddress
        {
            get { return fCCAddress; }
            set { SetPropertyValue<string>("CCAddress", ref fCCAddress, value); }
        }
        string fCCCity;
        [Size(15)]
        public string CCCity
        {
            get { return fCCCity; }
            set { SetPropertyValue<string>("CCCity", ref fCCCity, value); }
        }
        string fCCState;
        [Size(2)]
        public string CCState
        {
            get { return fCCState; }
            set { SetPropertyValue<string>("CCState", ref fCCState, value); }
        }
        string fCCZip;
        [Size(10)]
        public string CCZip
        {
            get { return fCCZip; }
            set { SetPropertyValue<string>("CCZip", ref fCCZip, value); }
        }
        int fPolTerritory;
        public int PolTerritory
        {
            get { return fPolTerritory; }
            set { SetPropertyValue<int>("PolTerritory", ref fPolTerritory, value); }
        }
        string fPolInsured;
        public string PolInsured
        {
            get { return fPolInsured; }
            set { SetPropertyValue<string>("PolInsured", ref fPolInsured, value); }
        }
        string fPolZip;
        [Size(10)]
        public string PolZip
        {
            get { return fPolZip; }
            set { SetPropertyValue<string>("PolZip", ref fPolZip, value); }
        }
        string fPolPhone;
        public string PolPhone
        {
            get { return fPolPhone; }
            set { SetPropertyValue<string>("PolPhone", ref fPolPhone, value); }
        }
        bool fNRPolicy;
        public bool NRPolicy
        {
            get { return fNRPolicy; }
            set { SetPropertyValue<bool>("NRPolicy", ref fNRPolicy, value); }
        }
        int fNoClaimants;
        public int NoClaimants
        {
            get { return fNoClaimants; }
            set { SetPropertyValue<int>("NoClaimants", ref fNoClaimants, value); }
        }
        string fLossDesc;
        [Size(3000)]
        public string LossDesc
        {
            get { return fLossDesc; }
            set { SetPropertyValue<string>("LossDesc", ref fLossDesc, value); }
        }
        float fPerClaimOverride;
        public float PerClaimOverride
        {
            get { return fPerClaimOverride; }
            set { SetPropertyValue<float>("PerClaimOverride", ref fPerClaimOverride, value); }
        }
        float fPerClaimantOverride;
        public float PerClaimantOverride
        {
            get { return fPerClaimantOverride; }
            set { SetPropertyValue<float>("PerClaimantOverride", ref fPerClaimantOverride, value); }
        }
        string fLossLoc;
        [Size(40)]
        public string LossLoc
        {
            get { return fLossLoc; }
            set { SetPropertyValue<string>("LossLoc", ref fLossLoc, value); }
        }
        string fVin;
        public string Vin
        {
            get { return fVin; }
            set { SetPropertyValue<string>("Vin", ref fVin, value); }
        }
        int fLineOfBusiness;
        public int LineOfBusiness
        {
            get { return fLineOfBusiness; }
            set { SetPropertyValue<int>("LineOfBusiness", ref fLineOfBusiness, value); }
        }
        string fRatingState;
        public string RatingState
        {
            get { return fRatingState; }
            set { SetPropertyValue<string>("RatingState", ref fRatingState, value); }
        }
        string fReportedBy;
        public string ReportedBy
        {
            get { return fReportedBy; }
            set { SetPropertyValue<string>("ReportedBy", ref fReportedBy, value); }
        }
        string fLossCity;
        public string LossCity
        {
            get { return fLossCity; }
            set { SetPropertyValue<string>("LossCity", ref fLossCity, value); }
        }
        string fLossState;
        public string LossState
        {
            get { return fLossState; }
            set { SetPropertyValue<string>("LossState", ref fLossState, value); }
        }
        int fSeverity;
        public int Severity
        {
            get { return fSeverity; }
            set { SetPropertyValue<int>("Severity", ref fSeverity, value); }
        }
        string fCMSInjuryCause;
        [Size(50)]
        public string CMSInjuryCause
        {
            get { return fCMSInjuryCause; }
            set { SetPropertyValue<string>("CMSInjuryCause", ref fCMSInjuryCause, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        int fClaimants;
        public int Claimants
        {
            get { return fClaimants; }
            set { SetPropertyValue<int>("Claimants", ref fClaimants, value); }
        }
        public Claims(Session session) : base(session) { }
        public Claims() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class FLtRateViolations : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fLongDesc;
        public string LongDesc
        {
            get { return fLongDesc; }
            set { SetPropertyValue<string>("LongDesc", ref fLongDesc, value); }
        }
        string fViolationGroup;
        [Size(10)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fGroupDesc;
        public string GroupDesc
        {
            get { return fGroupDesc; }
            set { SetPropertyValue<string>("GroupDesc", ref fGroupDesc, value); }
        }
        int fPointsFirst;
        public int PointsFirst
        {
            get { return fPointsFirst; }
            set { SetPropertyValue<int>("PointsFirst", ref fPointsFirst, value); }
        }
        int fPointsAddl;
        public int PointsAddl
        {
            get { return fPointsAddl; }
            set { SetPropertyValue<int>("PointsAddl", ref fPointsAddl, value); }
        }
        string fRateCycle;
        [Size(25)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        
        string fQQViolCode;  //LF  11/5/18  Added this field, QQViolCode
        [Size(10)]
        public string QQViolCode
        {
            get { return fQQViolCode; }
            set { SetPropertyValue<string>("QQViolCode", ref fQQViolCode, value); }
        }  
        public FLtRateViolations(Session session) : base(session) { }
        public FLtRateViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyBindersDrivers : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public PolicyBindersDrivers(Session session) : base(session) { }
        public PolicyBindersDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ZipLookup : XPLiteObject
    {
        string fZipCode;
        [Size(5)]
        public string ZipCode
        {
            get { return fZipCode; }
            set { SetPropertyValue<string>("ZipCode", ref fZipCode, value); }
        }
        string fCityName;
        [Size(35)]
        public string CityName
        {
            get { return fCityName; }
            set { SetPropertyValue<string>("CityName", ref fCityName, value); }
        }
        string fPostalName;
        [Size(35)]
        public string PostalName
        {
            get { return fPostalName; }
            set { SetPropertyValue<string>("PostalName", ref fPostalName, value); }
        }
        string fCounty;
        [Size(35)]
        public string County
        {
            get { return fCounty; }
            set { SetPropertyValue<string>("County", ref fCounty, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        int fCommAutoTerritory;
        public int CommAutoTerritory
        {
            get { return fCommAutoTerritory; }
            set { SetPropertyValue<int>("CommAutoTerritory", ref fCommAutoTerritory, value); }
        }
        bool fSplit;
        public bool Split
        {
            get { return fSplit; }
            set { SetPropertyValue<bool>("Split", ref fSplit, value); }
        }
        int fCASplit1Terr;
        public int CASplit1Terr
        {
            get { return fCASplit1Terr; }
            set { SetPropertyValue<int>("CASplit1Terr", ref fCASplit1Terr, value); }
        }
        string fCASplit1Desc;
        [Size(15)]
        public string CASplit1Desc
        {
            get { return fCASplit1Desc; }
            set { SetPropertyValue<string>("CASplit1Desc", ref fCASplit1Desc, value); }
        }
        int fCASplit2Terr;
        public int CASplit2Terr
        {
            get { return fCASplit2Terr; }
            set { SetPropertyValue<int>("CASplit2Terr", ref fCASplit2Terr, value); }
        }
        string fCASplit2Desc;
        [Size(15)]
        public string CASplit2Desc
        {
            get { return fCASplit2Desc; }
            set { SetPropertyValue<string>("CASplit2Desc", ref fCASplit2Desc, value); }
        }
        int fGATerritory;
        public int GATerritory
        {
            get { return fGATerritory; }
            set { SetPropertyValue<int>("GATerritory", ref fGATerritory, value); }
        }
        int fGLTerritory;
        public int GLTerritory
        {
            get { return fGLTerritory; }
            set { SetPropertyValue<int>("GLTerritory", ref fGLTerritory, value); }
        }
        string fUnacceptable;
        [Size(3)]
        public string Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<string>("Unacceptable", ref fUnacceptable, value); }
        }
        int fPATerrAfter112010;
        public int PATerrAfter112010
        {
            get { return fPATerrAfter112010; }
            set { SetPropertyValue<int>("PATerrAfter112010", ref fPATerrAfter112010, value); }
        }
        int fPATerrAfter012011;
        public int PATerrAfter012011
        {
            get { return fPATerrAfter012011; }
            set { SetPropertyValue<int>("PATerrAfter012011", ref fPATerrAfter012011, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public ZipLookup(Session session) : base(session) { }
        public ZipLookup() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class SystemACH : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fType;
        public string Type
        {
            get { return fType; }
            set { SetPropertyValue<string>("Type", ref fType, value); }
        }
        string fName;
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        string fValue;
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }
        public SystemACH(Session session) : base(session) { }
        public SystemACH() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class RatingMain : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1FullName;
        [Size(255)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        int fHousholdFactorIndex;
        public int HousholdFactorIndex
        {
            get { return fHousholdFactorIndex; }
            set { SetPropertyValue<int>("HousholdFactorIndex", ref fHousholdFactorIndex, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(30)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        int fBIBase;
        public int BIBase
        {
            get { return fBIBase; }
            set { SetPropertyValue<int>("BIBase", ref fBIBase, value); }
        }
        int fPIPBase;
        public int PIPBase
        {
            get { return fPIPBase; }
            set { SetPropertyValue<int>("PIPBase", ref fPIPBase, value); }
        }
        int fPDBase;
        public int PDBase
        {
            get { return fPDBase; }
            set { SetPropertyValue<int>("PDBase", ref fPDBase, value); }
        }
        int fMPBase;
        public int MPBase
        {
            get { return fMPBase; }
            set { SetPropertyValue<int>("MPBase", ref fMPBase, value); }
        }
        int fUMBase;
        public int UMBase
        {
            get { return fUMBase; }
            set { SetPropertyValue<int>("UMBase", ref fUMBase, value); }
        }
        int fCOMPBase;
        public int COMPBase
        {
            get { return fCOMPBase; }
            set { SetPropertyValue<int>("COMPBase", ref fCOMPBase, value); }
        }
        int fCOLLBase;
        public int COLLBase
        {
            get { return fCOLLBase; }
            set { SetPropertyValue<int>("COLLBase", ref fCOLLBase, value); }
        }
        double fBIFactor;
        public double BIFactor
        {
            get { return fBIFactor; }
            set { SetPropertyValue<double>("BIFactor", ref fBIFactor, value); }
        }
        double fPIPFactor;
        public double PIPFactor
        {
            get { return fPIPFactor; }
            set { SetPropertyValue<double>("PIPFactor", ref fPIPFactor, value); }
        }
        double fPDFactor;
        public double PDFactor
        {
            get { return fPDFactor; }
            set { SetPropertyValue<double>("PDFactor", ref fPDFactor, value); }
        }
        double fMPFactor;
        public double MPFactor
        {
            get { return fMPFactor; }
            set { SetPropertyValue<double>("MPFactor", ref fMPFactor, value); }
        }
        double fUMFactor;
        public double UMFactor
        {
            get { return fUMFactor; }
            set { SetPropertyValue<double>("UMFactor", ref fUMFactor, value); }
        }
        double fCOMPFactor;
        public double COMPFactor
        {
            get { return fCOMPFactor; }
            set { SetPropertyValue<double>("COMPFactor", ref fCOMPFactor, value); }
        }
        double fCOLLFactor;
        public double COLLFactor
        {
            get { return fCOLLFactor; }
            set { SetPropertyValue<double>("COLLFactor", ref fCOLLFactor, value); }
        }
        int fNoCars;
        public int NoCars
        {
            get { return fNoCars; }
            set { SetPropertyValue<int>("NoCars", ref fNoCars, value); }
        }
        int fNoDrivers;
        public int NoDrivers
        {
            get { return fNoDrivers; }
            set { SetPropertyValue<int>("NoDrivers", ref fNoDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }
        double fLOUAnnlPremRaw;
        public double LOUAnnlPremRaw
        {
            get { return fLOUAnnlPremRaw; }
            set { SetPropertyValue<double>("LOUAnnlPremRaw", ref fLOUAnnlPremRaw, value); }
        }
        double fADNDAnnlPremRaw;
        public double ADNDAnnlPremRaw
        {
            get { return fADNDAnnlPremRaw; }
            set { SetPropertyValue<double>("ADNDAnnlPremRaw", ref fADNDAnnlPremRaw, value); }
        }
        double fRentalAnnlPremRaw;
        public double RentalAnnlPremRaw
        {
            get { return fRentalAnnlPremRaw; }
            set { SetPropertyValue<double>("RentalAnnlPremRaw", ref fRentalAnnlPremRaw, value); }
        }


        int fNoDaysLapse;
        public int NoDaysLapse
        {
            get { return fNoDaysLapse; }
            set { SetPropertyValue<int>("NoDaysLapse", ref fNoDaysLapse, value); }
        }

        string fPriorBILimit;
        [Size(10)]
        public string PriorBILimit
        {
            get { return fPriorBILimit; }
            set { SetPropertyValue<string>("PriorBILimit", ref fPriorBILimit, value); }
        }

        int fCreditScore;
        public int CreditScore
        {
            get { return fCreditScore; }
            set { SetPropertyValue<int>("CreditScore", ref fCreditScore, value); }
        }

        [NonPersistent]
        public string PriorPolicyNo;
        [NonPersistent]
        public string NextPolicyNo;
        [NonPersistent]
        public string PolicyStatus;
        [NonPersistent]
        public string RenewalStatus;
        [NonPersistent]
        public string ateIssued;
        [NonPersistent]
        public string Ins1First;
        [NonPersistent]
        public string Ins1Last;
        [NonPersistent]
        public string Ins1MI;
        [NonPersistent]
        public string Ins1Suffix;
        [NonPersistent]
        public string Ins2First;
        [NonPersistent]
        public string Ins2Last;
        [NonPersistent]
        public string Ins2MI;
        [NonPersistent]
        public string Ins2Suffix;
        [NonPersistent]
        public string Ins2FullName;
        [NonPersistent]
        public string MailStreet;
        [NonPersistent]
        public string MailCity;
        [NonPersistent]
        public string MailState;
        [NonPersistent]
        public string MailZip;
        [NonPersistent]
        public Boolean MailGarageSame;
        [NonPersistent]
        public string GarageStreet;
        [NonPersistent]
        public string HomePhone;
        [NonPersistent]
        public string WorkPhone;
        [NonPersistent]
        public string MainEmail;
        [NonPersistent]
        public string AltEmail;
        [NonPersistent]
        public Boolean Paperless;
        [NonPersistent]
        public Boolean AgentGross;
        [NonPersistent]
        public Boolean OOSEnd;
        [NonPersistent]
        public Boolean HoldRtn;
        [NonPersistent]
        public string ChangeGUID;
        [NonPersistent]
        public int policyCars;
        [NonPersistent]
        public int policyDrivers;
        [NonPersistent]
        public int HousholdIndex;
        [NonPersistent]
        public int RatingID;
        [NonPersistent]
        public int Cars;
        [NonPersistent]
        public int Drivers;
        [NonPersistent]
        public Boolean IsReturnedMail;
        [NonPersistent]
        public Boolean IsOnHold;
        [NonPersistent]
        public Boolean IsClaimMisRep;
        [NonPersistent]
        public Boolean IsNoREI;
        [NonPersistent]
        public Boolean IsSuspense;

        public RatingMain(Session session) : base(session) { }
        public RatingMain() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimHistoryActivity : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fClaimNo;
        [Size(20)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        int fHistoryID;
        public int HistoryID
        {
            get { return fHistoryID; }
            set { SetPropertyValue<int>("HistoryID", ref fHistoryID, value); }
        }
        string fTableName;
        public string TableName
        {
            get { return fTableName; }
            set { SetPropertyValue<string>("TableName", ref fTableName, value); }
        }
        int fIndexNo;
        public int IndexNo
        {
            get { return fIndexNo; }
            set { SetPropertyValue<int>("IndexNo", ref fIndexNo, value); }
        }
        string fColumnName;
        public string ColumnName
        {
            get { return fColumnName; }
            set { SetPropertyValue<string>("ColumnName", ref fColumnName, value); }
        }
        string fColumnDesc;
        public string ColumnDesc
        {
            get { return fColumnDesc; }
            set { SetPropertyValue<string>("ColumnDesc", ref fColumnDesc, value); }
        }
        string fFieldType;
        [Size(20)]
        public string FieldType
        {
            get { return fFieldType; }
            set { SetPropertyValue<string>("FieldType", ref fFieldType, value); }
        }
        string fValue1;
        [Size(8000)]
        public string Value1
        {
            get { return fValue1; }
            set { SetPropertyValue<string>("Value1", ref fValue1, value); }
        }
        string fValue2;
        [Size(8000)]
        public string Value2
        {
            get { return fValue2; }
            set { SetPropertyValue<string>("Value2", ref fValue2, value); }
        }
        public ClaimHistoryActivity(Session session) : base(session) { }
        public ClaimHistoryActivity() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsNumberSource : XPLiteObject
    {
        int fxxYear;
        public int xxYear
        {
            get { return fxxYear; }
            set { SetPropertyValue<int>("xxYear", ref fxxYear, value); }
        }
        int fxxMonth;
        public int xxMonth
        {
            get { return fxxMonth; }
            set { SetPropertyValue<int>("xxMonth", ref fxxMonth, value); }
        }
        int fCurrentCount;
        public int CurrentCount
        {
            get { return fCurrentCount; }
            set { SetPropertyValue<int>("CurrentCount", ref fCurrentCount, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        public ClaimsNumberSource(Session session) : base(session) { }
        public ClaimsNumberSource() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class System_Prefixes : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fState;
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fStateAbbr;
        [Size(2)]
        public string StateAbbr
        {
            get { return fStateAbbr; }
            set { SetPropertyValue<string>("StateAbbr", ref fStateAbbr, value); }
        }
        string fPolicyPrefix;
        [Size(50)]
        public string PolicyPrefix
        {
            get { return fPolicyPrefix; }
            set { SetPropertyValue<string>("PolicyPrefix", ref fPolicyPrefix, value); }
        }
        string fClaimPrefix;
        [Size(50)]
        public string ClaimPrefix
        {
            get { return fClaimPrefix; }
            set { SetPropertyValue<string>("ClaimPrefix", ref fClaimPrefix, value); }
        }
        int fIsActive;
        public int IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<int>("IsActive", ref fIsActive, value); }
        }
        public System_Prefixes(Session session) : base(session) { }
        public System_Prefixes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyDOLDrivers : XPCustomObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fDateTimeCreated;
        public DateTime DateTimeCreated
        {
            get { return fDateTimeCreated; }
            set { SetPropertyValue<DateTime>("DateTimeCreated", ref fDateTimeCreated, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public PolicyDOLDrivers(Session session) : base(session) { }
        public PolicyDOLDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PositivePayOutput : XPLiteObject
    {
        int fIndexID;
        [Key]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        bool fIsClaims;
        public bool IsClaims
        {
            get { return fIsClaims; }
            set { SetPropertyValue<bool>("IsClaims", ref fIsClaims, value); }
        }
        string fDataRow;
        [Size(160)]
        public string DataRow
        {
            get { return fDataRow; }
            set { SetPropertyValue<string>("DataRow", ref fDataRow, value); }
        }
        public PositivePayOutput(Session session) : base(session) { }
        public PositivePayOutput() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class GroupMemberships : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        int fUserID;
        public int UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<int>("UserID", ref fUserID, value); }
        }
        string fRights;
        [Size(8000)]
        public string Rights
        {
            get { return fRights; }
            set { SetPropertyValue<string>("Rights", ref fRights, value); }
        }
        public GroupMemberships(Session session) : base(session) { }
        public GroupMemberships() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PaymentsCheckSequence : XPLiteObject
    {
        int fCheckNo;
        [Key(true)]
        public int CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<int>("CheckNo", ref fCheckNo, value); }
        }
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fIndexID_PaymentsChecksPending;
        public int IndexID_PaymentsChecksPending
        {
            get { return fIndexID_PaymentsChecksPending; }
            set { SetPropertyValue<int>("IndexID_PaymentsChecksPending", ref fIndexID_PaymentsChecksPending, value); }
        }
        public PaymentsCheckSequence(Session session) : base(session) { }
        public PaymentsCheckSequence() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class GroupRights : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        int fUserID;
        public int UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<int>("UserID", ref fUserID, value); }
        }
        string fRights;
        [Size(8000)]
        public string Rights
        {
            get { return fRights; }
            set { SetPropertyValue<string>("Rights", ref fRights, value); }
        }
        public GroupRights(Session session) : base(session) { }
        public GroupRights() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsOnly_original_2012_0621 : XPLiteObject
    {
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fIns1Firstname;
        [Size(75)]
        public string Ins1Firstname
        {
            get { return fIns1Firstname; }
            set { SetPropertyValue<string>("Ins1Firstname", ref fIns1Firstname, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Lastname;
        [Size(75)]
        public string Ins1Lastname
        {
            get { return fIns1Lastname; }
            set { SetPropertyValue<string>("Ins1Lastname", ref fIns1Lastname, value); }
        }
        string fIns2Firstname;
        [Size(75)]
        public string Ins2Firstname
        {
            get { return fIns2Firstname; }
            set { SetPropertyValue<string>("Ins2Firstname", ref fIns2Firstname, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Lastname;
        [Size(75)]
        public string Ins2Lastname
        {
            get { return fIns2Lastname; }
            set { SetPropertyValue<string>("Ins2Lastname", ref fIns2Lastname, value); }
        }
        string fMailStreet;
        [Size(75)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(75)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(75)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(75)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        [Size(75)]
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(75)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(75)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAgentCode;
        [Size(50)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        string fUMLimit;
        [Size(20)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(20)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        string fADNDLimit;
        [Size(20)]
        public string ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<string>("ADNDLimit", ref fADNDLimit, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        string fPIPLimit;
        [Size(20)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        string fBILimit;
        [Size(20)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        string fClaimNo;
        [Size(25)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasPIP;
        public bool HasPIP
        {
            get { return fHasPIP; }
            set { SetPropertyValue<bool>("HasPIP", ref fHasPIP, value); }
        }
        public ClaimsOnly_original_2012_0621(Session session) : base(session) { }
        public ClaimsOnly_original_2012_0621() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ErrorLog : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fGUID_;
        [Size(36)]
        public string GUID_
        {
            get { return fGUID_; }
            set { SetPropertyValue<string>("GUID_", ref fGUID_, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fUserName;
        [Size(20)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        DateTime fDateTimeError;
        public DateTime DateTimeError
        {
            get { return fDateTimeError; }
            set { SetPropertyValue<DateTime>("DateTimeError", ref fDateTimeError, value); }
        }
        string fErrorProcedure;
        public string ErrorProcedure
        {
            get { return fErrorProcedure; }
            set { SetPropertyValue<string>("ErrorProcedure", ref fErrorProcedure, value); }
        }
        string fErrorCode;
        [Size(10)]
        public string ErrorCode
        {
            get { return fErrorCode; }
            set { SetPropertyValue<string>("ErrorCode", ref fErrorCode, value); }
        }
        string fErrorDescr;
        [Size(2000)]
        public string ErrorDescr
        {
            get { return fErrorDescr; }
            set { SetPropertyValue<string>("ErrorDescr", ref fErrorDescr, value); }
        }
        public ErrorLog(Session session) : base(session) { }
        public ErrorLog() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Groups : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fShortDesc;
        [Size(200)]
        public string ShortDesc
        {
            get { return fShortDesc; }
            set { SetPropertyValue<string>("ShortDesc", ref fShortDesc, value); }
        }
        string fLongDesc;
        [Size(500)]
        public string LongDesc
        {
            get { return fLongDesc; }
            set { SetPropertyValue<string>("LongDesc", ref fLongDesc, value); }
        }
        public Groups(Session session) : base(session) { }
        public Groups() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimantsFirstReport : XPLiteObject
    {
        string fClaimNo;
        [Size(75)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fClaimantName;
        [Size(105)]
        public string ClaimantName
        {
            get { return fClaimantName; }
            set { SetPropertyValue<string>("ClaimantName", ref fClaimantName, value); }
        }
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fPeril;
        public int Peril
        {
            get { return fPeril; }
            set { SetPropertyValue<int>("Peril", ref fPeril, value); }
        }
        string fPerilDesc;
        public string PerilDesc
        {
            get { return fPerilDesc; }
            set { SetPropertyValue<string>("PerilDesc", ref fPerilDesc, value); }
        }
        int fCompExt;
        public int CompExt
        {
            get { return fCompExt; }
            set { SetPropertyValue<int>("CompExt", ref fCompExt, value); }
        }
        string fCompExtDesc;
        [Size(25)]
        public string CompExtDesc
        {
            get { return fCompExtDesc; }
            set { SetPropertyValue<string>("CompExtDesc", ref fCompExtDesc, value); }
        }
        string fCatNo;
        [Size(8)]
        public string CatNo
        {
            get { return fCatNo; }
            set { SetPropertyValue<string>("CatNo", ref fCatNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fRptDate;
        public DateTime RptDate
        {
            get { return fRptDate; }
            set { SetPropertyValue<DateTime>("RptDate", ref fRptDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fClmntAddress;
        [Size(200)]
        public string ClmntAddress
        {
            get { return fClmntAddress; }
            set { SetPropertyValue<string>("ClmntAddress", ref fClmntAddress, value); }
        }
        string fClmntCity;
        [Size(30)]
        public string ClmntCity
        {
            get { return fClmntCity; }
            set { SetPropertyValue<string>("ClmntCity", ref fClmntCity, value); }
        }
        string fClmntState;
        [Size(2)]
        public string ClmntState
        {
            get { return fClmntState; }
            set { SetPropertyValue<string>("ClmntState", ref fClmntState, value); }
        }
        string fClmntZip;
        [Size(10)]
        public string ClmntZip
        {
            get { return fClmntZip; }
            set { SetPropertyValue<string>("ClmntZip", ref fClmntZip, value); }
        }
        string fClmntHome;
        [Size(14)]
        public string ClmntHome
        {
            get { return fClmntHome; }
            set { SetPropertyValue<string>("ClmntHome", ref fClmntHome, value); }
        }
        string fClmntWork;
        [Size(14)]
        public string ClmntWork
        {
            get { return fClmntWork; }
            set { SetPropertyValue<string>("ClmntWork", ref fClmntWork, value); }
        }
        string fYearMake;
        [Size(25)]
        public string YearMake
        {
            get { return fYearMake; }
            set { SetPropertyValue<string>("YearMake", ref fYearMake, value); }
        }
        string fBodyStyle;
        [Size(25)]
        public string BodyStyle
        {
            get { return fBodyStyle; }
            set { SetPropertyValue<string>("BodyStyle", ref fBodyStyle, value); }
        }
        string fPriorDamage;
        [Size(25)]
        public string PriorDamage
        {
            get { return fPriorDamage; }
            set { SetPropertyValue<string>("PriorDamage", ref fPriorDamage, value); }
        }
        string fTagInfo;
        [Size(10)]
        public string TagInfo
        {
            get { return fTagInfo; }
            set { SetPropertyValue<string>("TagInfo", ref fTagInfo, value); }
        }
        string fColor;
        [Size(10)]
        public string Color
        {
            get { return fColor; }
            set { SetPropertyValue<string>("Color", ref fColor, value); }
        }
        string fLocation;
        [Size(25)]
        public string Location
        {
            get { return fLocation; }
            set { SetPropertyValue<string>("Location", ref fLocation, value); }
        }
        string fBodyShop;
        [Size(25)]
        public string BodyShop
        {
            get { return fBodyShop; }
            set { SetPropertyValue<string>("BodyShop", ref fBodyShop, value); }
        }
        string fSalvageYard;
        [Size(25)]
        public string SalvageYard
        {
            get { return fSalvageYard; }
            set { SetPropertyValue<string>("SalvageYard", ref fSalvageYard, value); }
        }
        bool fDriveable;
        public bool Driveable
        {
            get { return fDriveable; }
            set { SetPropertyValue<bool>("Driveable", ref fDriveable, value); }
        }
        bool fTotalLoss;
        public bool TotalLoss
        {
            get { return fTotalLoss; }
            set { SetPropertyValue<bool>("TotalLoss", ref fTotalLoss, value); }
        }
        string fProvCode;
        public string ProvCode
        {
            get { return fProvCode; }
            set { SetPropertyValue<string>("ProvCode", ref fProvCode, value); }
        }
        string fProvName;
        public string ProvName
        {
            get { return fProvName; }
            set { SetPropertyValue<string>("ProvName", ref fProvName, value); }
        }
        string fProvAddr;
        public string ProvAddr
        {
            get { return fProvAddr; }
            set { SetPropertyValue<string>("ProvAddr", ref fProvAddr, value); }
        }
        string fProvCity;
        [Size(25)]
        public string ProvCity
        {
            get { return fProvCity; }
            set { SetPropertyValue<string>("ProvCity", ref fProvCity, value); }
        }
        string fProvSt;
        [Size(2)]
        public string ProvSt
        {
            get { return fProvSt; }
            set { SetPropertyValue<string>("ProvSt", ref fProvSt, value); }
        }
        string fProvZip;
        [Size(10)]
        public string ProvZip
        {
            get { return fProvZip; }
            set { SetPropertyValue<string>("ProvZip", ref fProvZip, value); }
        }
        string fProvPhone;
        [Size(14)]
        public string ProvPhone
        {
            get { return fProvPhone; }
            set { SetPropertyValue<string>("ProvPhone", ref fProvPhone, value); }
        }
        string fProvFax;
        [Size(14)]
        public string ProvFax
        {
            get { return fProvFax; }
            set { SetPropertyValue<string>("ProvFax", ref fProvFax, value); }
        }
        string fAttyCode;
        public string AttyCode
        {
            get { return fAttyCode; }
            set { SetPropertyValue<string>("AttyCode", ref fAttyCode, value); }
        }
        string fAttyName;
        public string AttyName
        {
            get { return fAttyName; }
            set { SetPropertyValue<string>("AttyName", ref fAttyName, value); }
        }
        string fStatus;
        [Size(15)]
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        string fReserveType;
        [Size(1)]
        public string ReserveType
        {
            get { return fReserveType; }
            set { SetPropertyValue<string>("ReserveType", ref fReserveType, value); }
        }
        int fReserveAmt;
        public int ReserveAmt
        {
            get { return fReserveAmt; }
            set { SetPropertyValue<int>("ReserveAmt", ref fReserveAmt, value); }
        }
        DateTime fOrigResDate;
        public DateTime OrigResDate
        {
            get { return fOrigResDate; }
            set { SetPropertyValue<DateTime>("OrigResDate", ref fOrigResDate, value); }
        }
        string fFileLoc;
        [Size(20)]
        public string FileLoc
        {
            get { return fFileLoc; }
            set { SetPropertyValue<string>("FileLoc", ref fFileLoc, value); }
        }
        string fAdjuster;
        [Size(20)]
        public string Adjuster
        {
            get { return fAdjuster; }
            set { SetPropertyValue<string>("Adjuster", ref fAdjuster, value); }
        }
        string fDescOfLoss;
        [Size(200)]
        public string DescOfLoss
        {
            get { return fDescOfLoss; }
            set { SetPropertyValue<string>("DescOfLoss", ref fDescOfLoss, value); }
        }
        int fALAE;
        public int ALAE
        {
            get { return fALAE; }
            set { SetPropertyValue<int>("ALAE", ref fALAE, value); }
        }
        int fULAE;
        public int ULAE
        {
            get { return fULAE; }
            set { SetPropertyValue<int>("ULAE", ref fULAE, value); }
        }
        string fSupervisor;
        [Size(250)]
        public string Supervisor
        {
            get { return fSupervisor; }
            set { SetPropertyValue<string>("Supervisor", ref fSupervisor, value); }
        }
        string fSIU;
        [Size(10)]
        public string SIU
        {
            get { return fSIU; }
            set { SetPropertyValue<string>("SIU", ref fSIU, value); }
        }
        DateTime fStatusDate;
        public DateTime StatusDate
        {
            get { return fStatusDate; }
            set { SetPropertyValue<DateTime>("StatusDate", ref fStatusDate, value); }
        }
        int fClaimantIndex;
        public int ClaimantIndex
        {
            get { return fClaimantIndex; }
            set { SetPropertyValue<int>("ClaimantIndex", ref fClaimantIndex, value); }
        }
        double fPDClaimantOverride;
        public double PDClaimantOverride
        {
            get { return fPDClaimantOverride; }
            set { SetPropertyValue<double>("PDClaimantOverride", ref fPDClaimantOverride, value); }
        }
        double fPIPClaimantOverride;
        public double PIPClaimantOverride
        {
            get { return fPIPClaimantOverride; }
            set { SetPropertyValue<double>("PIPClaimantOverride", ref fPIPClaimantOverride, value); }
        }
        bool fADNDInsured;
        public bool ADNDInsured
        {
            get { return fADNDInsured; }
            set { SetPropertyValue<bool>("ADNDInsured", ref fADNDInsured, value); }
        }
        bool fADNDSeatBelt;
        public bool ADNDSeatBelt
        {
            get { return fADNDSeatBelt; }
            set { SetPropertyValue<bool>("ADNDSeatBelt", ref fADNDSeatBelt, value); }
        }
        int fADNDInjuryLevel;
        public int ADNDInjuryLevel
        {
            get { return fADNDInjuryLevel; }
            set { SetPropertyValue<int>("ADNDInjuryLevel", ref fADNDInjuryLevel, value); }
        }
        string fPropertyDesc;
        [Size(30)]
        public string PropertyDesc
        {
            get { return fPropertyDesc; }
            set { SetPropertyValue<string>("PropertyDesc", ref fPropertyDesc, value); }
        }
        string fPropertyDamage;
        [Size(30)]
        public string PropertyDamage
        {
            get { return fPropertyDamage; }
            set { SetPropertyValue<string>("PropertyDamage", ref fPropertyDamage, value); }
        }
        string fPropertyLocation;
        [Size(30)]
        public string PropertyLocation
        {
            get { return fPropertyLocation; }
            set { SetPropertyValue<string>("PropertyLocation", ref fPropertyLocation, value); }
        }
        string fPropertyPOC;
        [Size(30)]
        public string PropertyPOC
        {
            get { return fPropertyPOC; }
            set { SetPropertyValue<string>("PropertyPOC", ref fPropertyPOC, value); }
        }
        string fPropertyAddr;
        [Size(30)]
        public string PropertyAddr
        {
            get { return fPropertyAddr; }
            set { SetPropertyValue<string>("PropertyAddr", ref fPropertyAddr, value); }
        }
        string fPropertyCity;
        [Size(20)]
        public string PropertyCity
        {
            get { return fPropertyCity; }
            set { SetPropertyValue<string>("PropertyCity", ref fPropertyCity, value); }
        }
        string fPropertyState;
        [Size(2)]
        public string PropertyState
        {
            get { return fPropertyState; }
            set { SetPropertyValue<string>("PropertyState", ref fPropertyState, value); }
        }
        string fPropertyZip;
        [Size(5)]
        public string PropertyZip
        {
            get { return fPropertyZip; }
            set { SetPropertyValue<string>("PropertyZip", ref fPropertyZip, value); }
        }
        string fPropertyPhone;
        [Size(14)]
        public string PropertyPhone
        {
            get { return fPropertyPhone; }
            set { SetPropertyValue<string>("PropertyPhone", ref fPropertyPhone, value); }
        }
        string fPropertyAltPhone;
        [Size(14)]
        public string PropertyAltPhone
        {
            get { return fPropertyAltPhone; }
            set { SetPropertyValue<string>("PropertyAltPhone", ref fPropertyAltPhone, value); }
        }
        string fRatingState;
        [Size(2)]
        public string RatingState
        {
            get { return fRatingState; }
            set { SetPropertyValue<string>("RatingState", ref fRatingState, value); }
        }
        string fHICN;
        [Size(15)]
        public string HICN
        {
            get { return fHICN; }
            set { SetPropertyValue<string>("HICN", ref fHICN, value); }
        }
        string fSSN;
        [Size(200)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        string fGender;
        [Size(1)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        string fCMSLastName;
        [Size(25)]
        public string CMSLastName
        {
            get { return fCMSLastName; }
            set { SetPropertyValue<string>("CMSLastName", ref fCMSLastName, value); }
        }
        string fCMSFirstName;
        [Size(25)]
        public string CMSFirstName
        {
            get { return fCMSFirstName; }
            set { SetPropertyValue<string>("CMSFirstName", ref fCMSFirstName, value); }
        }
        string fCMSMiddleInital;
        [Size(1)]
        public string CMSMiddleInital
        {
            get { return fCMSMiddleInital; }
            set { SetPropertyValue<string>("CMSMiddleInital", ref fCMSMiddleInital, value); }
        }
        bool fPropertyIsVeh;
        public bool PropertyIsVeh
        {
            get { return fPropertyIsVeh; }
            set { SetPropertyValue<bool>("PropertyIsVeh", ref fPropertyIsVeh, value); }
        }
        bool fTheftRecovery;
        public bool TheftRecovery
        {
            get { return fTheftRecovery; }
            set { SetPropertyValue<bool>("TheftRecovery", ref fTheftRecovery, value); }
        }
        bool fOwnerRetSalvage;
        public bool OwnerRetSalvage
        {
            get { return fOwnerRetSalvage; }
            set { SetPropertyValue<bool>("OwnerRetSalvage", ref fOwnerRetSalvage, value); }
        }
        DateTime fRecoveryDate;
        public DateTime RecoveryDate
        {
            get { return fRecoveryDate; }
            set { SetPropertyValue<DateTime>("RecoveryDate", ref fRecoveryDate, value); }
        }
        string fRecoveryAgency;
        [Size(30)]
        public string RecoveryAgency
        {
            get { return fRecoveryAgency; }
            set { SetPropertyValue<string>("RecoveryAgency", ref fRecoveryAgency, value); }
        }
        string fRecoveryState;
        [Size(2)]
        public string RecoveryState
        {
            get { return fRecoveryState; }
            set { SetPropertyValue<string>("RecoveryState", ref fRecoveryState, value); }
        }
        string fRecoveryCondition;
        [Size(20)]
        public string RecoveryCondition
        {
            get { return fRecoveryCondition; }
            set { SetPropertyValue<string>("RecoveryCondition", ref fRecoveryCondition, value); }
        }
        string fVehVIN;
        [Size(20)]
        public string VehVIN
        {
            get { return fVehVIN; }
            set { SetPropertyValue<string>("VehVIN", ref fVehVIN, value); }
        }
        bool fClmtIsBusiness;
        public bool ClmtIsBusiness
        {
            get { return fClmtIsBusiness; }
            set { SetPropertyValue<bool>("ClmtIsBusiness", ref fClmtIsBusiness, value); }
        }
        string fBusinessName;
        [Size(35)]
        public string BusinessName
        {
            get { return fBusinessName; }
            set { SetPropertyValue<string>("BusinessName", ref fBusinessName, value); }
        }
        bool fForceSubmit;
        public bool ForceSubmit
        {
            get { return fForceSubmit; }
            set { SetPropertyValue<bool>("ForceSubmit", ref fForceSubmit, value); }
        }
        string fFirstName;
        [Size(50)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fLastName;
        [Size(50)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fSuffix;
        [Size(5)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClmtLanguage;
        [Size(20)]
        public string ClmtLanguage
        {
            get { return fClmtLanguage; }
            set { SetPropertyValue<string>("ClmtLanguage", ref fClmtLanguage, value); }
        }
        bool fFastTrack;
        public bool FastTrack
        {
            get { return fFastTrack; }
            set { SetPropertyValue<bool>("FastTrack", ref fFastTrack, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fClaimantFirstName;
        [Size(50)]
        public string ClaimantFirstName
        {
            get { return fClaimantFirstName; }
            set { SetPropertyValue<string>("ClaimantFirstName", ref fClaimantFirstName, value); }
        }
        string fClaimantLastName;
        [Size(50)]
        public string ClaimantLastName
        {
            get { return fClaimantLastName; }
            set { SetPropertyValue<string>("ClaimantLastName", ref fClaimantLastName, value); }
        }
        string fClaimantMI;
        [Size(5)]
        public string ClaimantMI
        {
            get { return fClaimantMI; }
            set { SetPropertyValue<string>("ClaimantMI", ref fClaimantMI, value); }
        }
        string fPhyDamAreaDamaged;
        [Size(12)]
        public string PhyDamAreaDamaged
        {
            get { return fPhyDamAreaDamaged; }
            set { SetPropertyValue<string>("PhyDamAreaDamaged", ref fPhyDamAreaDamaged, value); }
        }
        string fPropDamAreaDamaged;
        [Size(12)]
        public string PropDamAreaDamaged
        {
            get { return fPropDamAreaDamaged; }
            set { SetPropertyValue<string>("PropDamAreaDamaged", ref fPropDamAreaDamaged, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        int fPaidAmt;
        public int PaidAmt
        {
            get { return fPaidAmt; }
            set { SetPropertyValue<int>("PaidAmt", ref fPaidAmt, value); }
        }
        int fSalvRecAmt;
        public int SalvRecAmt
        {
            get { return fSalvRecAmt; }
            set { SetPropertyValue<int>("SalvRecAmt", ref fSalvRecAmt, value); }
        }
        int fSubroAmt;
        public int SubroAmt
        {
            get { return fSubroAmt; }
            set { SetPropertyValue<int>("SubroAmt", ref fSubroAmt, value); }
        }
        bool fIsInsured;
        public bool IsInsured
        {
            get { return fIsInsured; }
            set { SetPropertyValue<bool>("IsInsured", ref fIsInsured, value); }
        }
        bool fIsCovInvestigation;
        public bool IsCovInvestigation
        {
            get { return fIsCovInvestigation; }
            set { SetPropertyValue<bool>("IsCovInvestigation", ref fIsCovInvestigation, value); }
        }
        string fWitnessesStatements;
        [Size(8000)]
        public string WitnessesStatements
        {
            get { return fWitnessesStatements; }
            set { SetPropertyValue<string>("WitnessesStatements", ref fWitnessesStatements, value); }
        }
        string fMedAttorneyTaxID;
        [Size(150)]
        public string MedAttorneyTaxID
        {
            get { return fMedAttorneyTaxID; }
            set { SetPropertyValue<string>("MedAttorneyTaxID", ref fMedAttorneyTaxID, value); }
        }
        bool fIsPropDamSubroClaim;
        public bool IsPropDamSubroClaim
        {
            get { return fIsPropDamSubroClaim; }
            set { SetPropertyValue<bool>("IsPropDamSubroClaim", ref fIsPropDamSubroClaim, value); }
        }
        bool fSuitDemandRec;
        public bool SuitDemandRec
        {
            get { return fSuitDemandRec; }
            set { SetPropertyValue<bool>("SuitDemandRec", ref fSuitDemandRec, value); }
        }
        DateTime fSuitDateRec;
        public DateTime SuitDateRec
        {
            get { return fSuitDateRec; }
            set { SetPropertyValue<DateTime>("SuitDateRec", ref fSuitDateRec, value); }
        }
        bool fIsInSuit;
        public bool IsInSuit
        {
            get { return fIsInSuit; }
            set { SetPropertyValue<bool>("IsInSuit", ref fIsInSuit, value); }
        }
        DateTime fInSuitDate;
        public DateTime InSuitDate
        {
            get { return fInSuitDate; }
            set { SetPropertyValue<DateTime>("InSuitDate", ref fInSuitDate, value); }
        }
        string fDefenseTaxID;
        public string DefenseTaxID
        {
            get { return fDefenseTaxID; }
            set { SetPropertyValue<string>("DefenseTaxID", ref fDefenseTaxID, value); }
        }
        string fInsuredRepTaxID;
        public string InsuredRepTaxID
        {
            get { return fInsuredRepTaxID; }
            set { SetPropertyValue<string>("InsuredRepTaxID", ref fInsuredRepTaxID, value); }
        }
        string fSuitNotes;
        [Size(8000)]
        public string SuitNotes
        {
            get { return fSuitNotes; }
            set { SetPropertyValue<string>("SuitNotes", ref fSuitNotes, value); }
        }
        bool fIsPreSuit;
        public bool IsPreSuit
        {
            get { return fIsPreSuit; }
            set { SetPropertyValue<bool>("IsPreSuit", ref fIsPreSuit, value); }
        }
        DateTime fPreSuitDate;
        public DateTime PreSuitDate
        {
            get { return fPreSuitDate; }
            set { SetPropertyValue<DateTime>("PreSuitDate", ref fPreSuitDate, value); }
        }
        public ClaimantsFirstReport(Session session) : base(session) { }
        public ClaimantsFirstReport() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class sysdiagrams : XPLiteObject
    {
        string fname;
        [Size(128)]
        public string name
        {
            get { return fname; }
            set { SetPropertyValue<string>("name", ref fname, value); }
        }
        int fprincipal_id;
        public int principal_id
        {
            get { return fprincipal_id; }
            set { SetPropertyValue<int>("principal_id", ref fprincipal_id, value); }
        }
        int fdiagram_id;
        [Key(true)]
        public int diagram_id
        {
            get { return fdiagram_id; }
            set { SetPropertyValue<int>("diagram_id", ref fdiagram_id, value); }
        }
        int fversion;
        public int version
        {
            get { return fversion; }
            set { SetPropertyValue<int>("version", ref fversion, value); }
        }
        byte[] fdefinition;
        public byte[] definition
        {
            get { return fdefinition; }
            set { SetPropertyValue<byte[]>("definition", ref fdefinition, value); }
        }
        public sysdiagrams(Session session) : base(session) { }
        public sysdiagrams() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimPIPInvAmbulanceZips : XPLiteObject
    {
        int fZipCode;
        [Key]
        public int ZipCode
        {
            get { return fZipCode; }
            set { SetPropertyValue<int>("ZipCode", ref fZipCode, value); }
        }
        int fCarrier;
        public int Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<int>("Carrier", ref fCarrier, value); }
        }
        int fLocality;
        public int Locality
        {
            get { return fLocality; }
            set { SetPropertyValue<int>("Locality", ref fLocality, value); }
        }
        string fAreaType;
        [Size(1)]
        public string AreaType
        {
            get { return fAreaType; }
            set { SetPropertyValue<string>("AreaType", ref fAreaType, value); }
        }
        public ClaimPIPInvAmbulanceZips(Session session) : base(session) { }
        public ClaimPIPInvAmbulanceZips() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyEndorseQuoteCars : XPCustomObject
    {
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fGrossVehicleWeight;
        public string GrossVehicleWeight
        {
            get { return fGrossVehicleWeight; }
            set { SetPropertyValue<string>("GrossVehicleWeight", ref fGrossVehicleWeight, value); }
        }
        string fClassCode;
        [Size(30)]
        public string ClassCode
        {
            get { return fClassCode; }
            set { SetPropertyValue<string>("ClassCode", ref fClassCode, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public PolicyEndorseQuoteCars(Session session) : base(session) { }
        public PolicyEndorseQuoteCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class RatingDriversViolations : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fMainIndexID;
        public int MainIndexID
        {
            get { return fMainIndexID; }
            set { SetPropertyValue<int>("MainIndexID", ref fMainIndexID, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        bool fViolationTooOld;
        public bool ViolationTooOld
        {
            get { return fViolationTooOld; }
            set { SetPropertyValue<bool>("ViolationTooOld", ref fViolationTooOld, value); }
        }
        bool fMinorForgiven;
        public bool MinorForgiven
        {
            get { return fMinorForgiven; }
            set { SetPropertyValue<bool>("MinorForgiven", ref fMinorForgiven, value); }
        }
        string fQQViolCode;
        [Size(5000)]
        public string QQViolCode
        {
            get { return fQQViolCode; }
            set { SetPropertyValue<string>("QQViolCode", ref fQQViolCode, value); }
        }

        [NonPersistent]
        public string PolicyNo;
        public RatingDriversViolations(Session session) : base(session) { }
        public RatingDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyEndorseQuote : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPriorPolicyNo;
        [Size(15)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }

        bool isCurrentCustomer;
        public bool IsCurrentCustomer
        {
            get { return isCurrentCustomer; }
            set { SetPropertyValue<bool>("IsCurrentCustomer", ref isCurrentCustomer, value); }
        }

        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        [Size(5)]
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        string fFromRater;
        public string FromRater
        {
            get { return fFromRater; }
            set { SetPropertyValue<string>("FromRater", ref fFromRater, value); }
        }
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }
        bool fAdvancedQuote;
        public bool AdvancedQuote
        {
            get { return fAdvancedQuote; }
            set { SetPropertyValue<bool>("AdvancedQuote", ref fAdvancedQuote, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fPolicyCars;
        public int PolicyCars
        {
            get { return fPolicyCars; }
            set { SetPropertyValue<int>("PolicyCars", ref fPolicyCars, value); }
        }
        int fPolicyDrivers;
        public int PolicyDrivers
        {
            get { return fPolicyDrivers; }
            set { SetPropertyValue<int>("PolicyDrivers", ref fPolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }

        int fCreditScore;
        public int CreditScore
        {
            get { return fCreditScore; }
            set { SetPropertyValue<int>("CreditScore", ref fCreditScore, value); }
        }
        int fEstimatedCreditScore;
        public int EstimatedCreditScore
        {
            get { return fEstimatedCreditScore; }
            set { SetPropertyValue<int>("EstimatedCreditScore", ref fEstimatedCreditScore, value); }
        }
        double fMaintenanceFee;
        public double MaintenanceFee
        {
            get { return fMaintenanceFee; }
            set { SetPropertyValue<double>("MaintenanceFee", ref fMaintenanceFee, value); }
        }
        double fPIPPDOnlyFee;
        public double PIPPDOnlyFee
        {
            get { return fPIPPDOnlyFee; }
            set { SetPropertyValue<double>("PIPPDOnlyFee", ref fPIPPDOnlyFee, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        bool fIsSaved;
        public bool IsSaved
        {
            get { return fIsSaved; }
            set { SetPropertyValue<bool>("IsSaved", ref fIsSaved, value); }
        }

        bool fIsPremiumPaymentPaid;
        public bool IsPremiumPaymentPaid
        {
            get { return fIsPremiumPaymentPaid; }
            set { SetPropertyValue<bool>("IsPremiumPaymentPaid", ref fIsPremiumPaymentPaid, value); }
        }

        bool fAgentPremiumPaymentDueEmailSent;
        public bool AgentPremiumPaymentDueEmailSent
        {
            get { return fAgentPremiumPaymentDueEmailSent; }
            set { SetPropertyValue<bool>("AgentPremiumPaymentDueEmailSent", ref fAgentPremiumPaymentDueEmailSent, value); }
        }

        // returns the first error found that is against the endorsement issuance rules. 
        public bool IsValidateForIssuance()
        {
            bool isValid = true;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    XpoPolicy policyForEndorsementQuote =
                        uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                    if(policyForEndorsementQuote != null)
                    {
                        if(PIPDed != null && !PIPDed.Equals(policyForEndorsementQuote.PIPDed))
                        {
                            isValid = false;
                        }
                        if (PDLimit != null && !PDLimit.Equals(policyForEndorsementQuote.PDLimit))
                        {
                            isValid = false;
                        }
                        if (!WorkLoss.Equals(policyForEndorsementQuote.WorkLoss))
                        {
                            isValid = false;
                        }
                        if (!NIO.Equals(policyForEndorsementQuote.NIO))
                        {
                            isValid = false;
                        }
                        if (!NIRR.Equals(policyForEndorsementQuote.NIRR))
                        {
                            isValid = false;
                        }
                        if (!HasPPO.Equals(policyForEndorsementQuote.HasPPO))
                        {
                            isValid = false;
                        }
                        if (!TransDisc.Equals(policyForEndorsementQuote.TransDisc))
                        {
                            isValid = false;
                        }
                        if (!Homeowner.Equals(policyForEndorsementQuote.Homeowner))
                        {
                            isValid = false;
                        }
                        if (!Paperless.Equals(policyForEndorsementQuote.Paperless))
                        {
                            isValid = false;
                        }
                        if (!AdvancedQuote.Equals(policyForEndorsementQuote.AdvancedQuote))
                        {
                            isValid = false;
                        }

                        //Check for Deleted drivers
                        IList<PolicyEndorseQuoteDrivers> driversForEndorsement =
                            new XPCollection<PolicyEndorseQuoteDrivers>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        IList<PolicyDrivers> driversForPolicy =
                            new XPCollection<PolicyDrivers>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        if (!driversForEndorsement.Any())
                        {
                            isValid = false;
                        }
                        if (driversForEndorsement?.Count < driversForPolicy?.Count)
                        {
                            isValid = false;
                        }
                        else
                        {
                            var isDriversDeleted = driversForPolicy.Any(x => !driversForEndorsement.Any(e => e.LicenseNo?.ToLower()?.Trim() == x.LicenseNo?.ToLower()?.Trim()));
                            isValid = isDriversDeleted ? false : isValid;
                        }

                        //Check for deleted vehicles
                        IList<PolicyEndorseQuoteCars> carsForEndorsement =
                            new XPCollection<PolicyEndorseQuoteCars>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        IList<PolicyCars> carsForPolicy =
                            new XPCollection<PolicyCars>(uow,
                                CriteriaOperator.Parse("PolicyNo = ?", PolicyNo));
                        if (!carsForEndorsement.Any())
                        {
                            isValid = false;
                        }
                        if (carsForEndorsement?.Count < carsForPolicy?.Count)
                        {
                            isValid = false;
                        }
                        else
                        {
                            var isCarsDeleted = carsForPolicy.Any(x => !carsForEndorsement.Any(e => e.VIN?.ToLower()?.Trim() == x.VIN?.ToLower()?.Trim()));
                            isValid = isCarsDeleted ? false : isValid;
                        }
                        //Check for new or updated vechiles with phy dam / comp/coll
                        foreach (PolicyEndorseQuoteCars endorsementCar in carsForEndorsement)
                        {
                            PolicyCars carOnPolicy =
                                carsForPolicy.FirstOrDefault(c => c.VIN?.ToLower() == endorsementCar.VIN?.ToLower());
                            if(carOnPolicy != null)
                            {
                                if (!carOnPolicy.HasPhyDam.Equals(endorsementCar.HasPhyDam))
                                {
                                    isValid = false;
                                }
                                if (carOnPolicy.CompDed != null && !carOnPolicy.CompDed.Equals(endorsementCar.CompDed))
                                {
                                    isValid = false;
                                }
                                if (carOnPolicy.CollDed != null && !carOnPolicy.CollDed.Equals(endorsementCar.CollDed))
                                {
                                    isValid = false;
                                }
                            }
                            else // new vehicle
                            {
                                if (endorsementCar.HasPhyDam)
                                {
                                    isValid = false;
                                }
                                if(!string.IsNullOrEmpty(endorsementCar.CompDed))
                                {
                                    double compDed;
                                    if (double.TryParse(endorsementCar.CompDed, out compDed))
                                    {
                                        if(compDed > 0)
                                        {
                                            isValid = false;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(endorsementCar.CollDed))
                                {
                                    double collDed;
                                    if (double.TryParse(endorsementCar.CollDed, out collDed))
                                    {
                                        if (collDed > 0)
                                        {
                                            isValid = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                catch
                {
                    isValid = false;
                }
            }

            return isValid;
        }

        public PolicyEndorseQuote(Session session) : base(session) { }
        public PolicyEndorseQuote() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsOnly : XPLiteObject
    {
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fIns1Firstname;
        [Size(75)]
        public string Ins1Firstname
        {
            get { return fIns1Firstname; }
            set { SetPropertyValue<string>("Ins1Firstname", ref fIns1Firstname, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Lastname;
        [Size(75)]
        public string Ins1Lastname
        {
            get { return fIns1Lastname; }
            set { SetPropertyValue<string>("Ins1Lastname", ref fIns1Lastname, value); }
        }
        string fIns2Firstname;
        [Size(75)]
        public string Ins2Firstname
        {
            get { return fIns2Firstname; }
            set { SetPropertyValue<string>("Ins2Firstname", ref fIns2Firstname, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Lastname;
        [Size(75)]
        public string Ins2Lastname
        {
            get { return fIns2Lastname; }
            set { SetPropertyValue<string>("Ins2Lastname", ref fIns2Lastname, value); }
        }
        string fMailStreet;
        [Size(75)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(75)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(75)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(75)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        [Size(75)]
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(75)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(75)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAgentCode;
        [Size(50)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        string fUMLimit;
        [Size(20)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(20)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        string fADNDLimit;
        [Size(20)]
        public string ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<string>("ADNDLimit", ref fADNDLimit, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        string fPIPLimit;
        [Size(20)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        string fBILimit;
        [Size(20)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        string fClaimNo;
        [Size(25)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasPIP;
        public bool HasPIP
        {
            get { return fHasPIP; }
            set { SetPropertyValue<bool>("HasPIP", ref fHasPIP, value); }
        }
        public ClaimsOnly(Session session) : base(session) { }
        public ClaimsOnly() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditPolicyCars : XPCustomObject
    {
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public AuditPolicyCars(Session session) : base(session) { }
        public AuditPolicyCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditPolicyHistory : XPLiteObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fAction;
        [Size(25)]
        public string Action
        {
            get { return fAction; }
            set { SetPropertyValue<string>("Action", ref fAction, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fProcessDate;
        public DateTime ProcessDate
        {
            get { return fProcessDate; }
            set { SetPropertyValue<DateTime>("ProcessDate", ref fProcessDate, value); }
        }
        DateTime fMailDate;
        public DateTime MailDate
        {
            get { return fMailDate; }
            set { SetPropertyValue<DateTime>("MailDate", ref fMailDate, value); }
        }
        int fEndCount;
        public int EndCount
        {
            get { return fEndCount; }
            set { SetPropertyValue<int>("EndCount", ref fEndCount, value); }
        }
        int fActivityCount;
        public int ActivityCount
        {
            get { return fActivityCount; }
            set { SetPropertyValue<int>("ActivityCount", ref fActivityCount, value); }
        }
        string fUserName;
        [Size(25)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        double fNoticeAmount;
        public double NoticeAmount
        {
            get { return fNoticeAmount; }
            set { SetPropertyValue<double>("NoticeAmount", ref fNoticeAmount, value); }
        }
        double fNoticeFeeAmount;
        public double NoticeFeeAmount
        {
            get { return fNoticeFeeAmount; }
            set { SetPropertyValue<double>("NoticeFeeAmount", ref fNoticeFeeAmount, value); }
        }
        double fNSFFeeAmount;
        public double NSFFeeAmount
        {
            get { return fNSFFeeAmount; }
            set { SetPropertyValue<double>("NSFFeeAmount", ref fNSFFeeAmount, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        double fOrigBaseCommission;
        public double OrigBaseCommission
        {
            get { return fOrigBaseCommission; }
            set { SetPropertyValue<double>("OrigBaseCommission", ref fOrigBaseCommission, value); }
        }
        double fNewBaseCommission;
        public double NewBaseCommission
        {
            get { return fNewBaseCommission; }
            set { SetPropertyValue<double>("NewBaseCommission", ref fNewBaseCommission, value); }
        }
        double fOrigOtherComm;
        public double OrigOtherComm
        {
            get { return fOrigOtherComm; }
            set { SetPropertyValue<double>("OrigOtherComm", ref fOrigOtherComm, value); }
        }
        double fNewOtherComm;
        public double NewOtherComm
        {
            get { return fNewOtherComm; }
            set { SetPropertyValue<double>("NewOtherComm", ref fNewOtherComm, value); }
        }
        double fOrigTotalFees;
        public double OrigTotalFees
        {
            get { return fOrigTotalFees; }
            set { SetPropertyValue<double>("OrigTotalFees", ref fOrigTotalFees, value); }
        }
        double fNewTotalFees;
        public double NewTotalFees
        {
            get { return fNewTotalFees; }
            set { SetPropertyValue<double>("NewTotalFees", ref fNewTotalFees, value); }
        }
        double fOrigWritten;
        public double OrigWritten
        {
            get { return fOrigWritten; }
            set { SetPropertyValue<double>("OrigWritten", ref fOrigWritten, value); }
        }
        double fNewWritten;
        public double NewWritten
        {
            get { return fNewWritten; }
            set { SetPropertyValue<double>("NewWritten", ref fNewWritten, value); }
        }
        string fGUID_;
        [Size(40)]
        public string GUID_
        {
            get { return fGUID_; }
            set { SetPropertyValue<string>("GUID_", ref fGUID_, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        bool fReportedToCVExchange;
        public bool ReportedToCVExchange
        {
            get { return fReportedToCVExchange; }
            set { SetPropertyValue<bool>("ReportedToCVExchange", ref fReportedToCVExchange, value); }
        }
        DateTime fReportedToCVExchangeDate;
        public DateTime ReportedToCVExchangeDate
        {
            get { return fReportedToCVExchangeDate; }
            set { SetPropertyValue<DateTime>("ReportedToCVExchangeDate", ref fReportedToCVExchangeDate, value); }
        }
        private bool fIsNoticeActive;
        public bool IsNoticeActive
        {
            get { return fIsNoticeActive; }
            set { SetPropertyValue<bool>("IsNoticeActive", ref fIsNoticeActive, value); }
        }

        public AuditPolicyHistory(Session session) : base(session) { }
        public AuditPolicyHistory() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AgentNotes : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        int fIndexNo;
        public int IndexNo
        {
            get { return fIndexNo; }
            set { SetPropertyValue<int>("IndexNo", ref fIndexNo, value); }
        }
        string fUserBy;
        [Size(50)]
        public string UserBy
        {
            get { return fUserBy; }
            set { SetPropertyValue<string>("UserBy", ref fUserBy, value); }
        }
        string fNotes;
        [Size(8000)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        DateTime fDate;
        public DateTime Date
        {
            get { return fDate; }
            set { SetPropertyValue<DateTime>("Date", ref fDate, value); }
        }
        bool fisDiary;
        public bool isDiary
        {
            get { return fisDiary; }
            set { SetPropertyValue<bool>("isDiary", ref fisDiary, value); }
        }
        public AgentNotes(Session session) : base(session) { }
        public AgentNotes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsChecks : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fTKey;
        [Size(20)]
        public string TKey
        {
            get { return fTKey; }
            set { SetPropertyValue<string>("TKey", ref fTKey, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        string fSerialNo;
        [Size(10)]
        public string SerialNo
        {
            get { return fSerialNo; }
            set { SetPropertyValue<string>("SerialNo", ref fSerialNo, value); }
        }
        DateTime fCheckDate;
        public DateTime CheckDate
        {
            get { return fCheckDate; }
            set { SetPropertyValue<DateTime>("CheckDate", ref fCheckDate, value); }
        }
        string fNote1;
        [Size(50)]
        public string Note1
        {
            get { return fNote1; }
            set { SetPropertyValue<string>("Note1", ref fNote1, value); }
        }
        string fNote2;
        [Size(50)]
        public string Note2
        {
            get { return fNote2; }
            set { SetPropertyValue<string>("Note2", ref fNote2, value); }
        }
        string fNewClmStatus;
        [Size(15)]
        public string NewClmStatus
        {
            get { return fNewClmStatus; }
            set { SetPropertyValue<string>("NewClmStatus", ref fNewClmStatus, value); }
        }
        int fType1099;
        public int Type1099
        {
            get { return fType1099; }
            set { SetPropertyValue<int>("Type1099", ref fType1099, value); }
        }
        string fTaxName;
        [Size(150)]
        public string TaxName
        {
            get { return fTaxName; }
            set { SetPropertyValue<string>("TaxName", ref fTaxName, value); }
        }
        string fTaxIDNo;
        [Size(150)]
        public string TaxIDNo
        {
            get { return fTaxIDNo; }
            set { SetPropertyValue<string>("TaxIDNo", ref fTaxIDNo, value); }
        }
        string fPayeeOne;
        [Size(150)]
        public string PayeeOne
        {
            get { return fPayeeOne; }
            set { SetPropertyValue<string>("PayeeOne", ref fPayeeOne, value); }
        }
        bool fUseAddr1;
        public bool UseAddr1
        {
            get { return fUseAddr1; }
            set { SetPropertyValue<bool>("UseAddr1", ref fUseAddr1, value); }
        }
        string fPayeeTwo;
        [Size(150)]
        public string PayeeTwo
        {
            get { return fPayeeTwo; }
            set { SetPropertyValue<string>("PayeeTwo", ref fPayeeTwo, value); }
        }
        bool fUseAddr2;
        public bool UseAddr2
        {
            get { return fUseAddr2; }
            set { SetPropertyValue<bool>("UseAddr2", ref fUseAddr2, value); }
        }
        string fStreet;
        [Size(35)]
        public string Street
        {
            get { return fStreet; }
            set { SetPropertyValue<string>("Street", ref fStreet, value); }
        }
        string fCity;
        [Size(25)]
        public string City
        {
            get { return fCity; }
            set { SetPropertyValue<string>("City", ref fCity, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fZip;
        [Size(5)]
        public string Zip
        {
            get { return fZip; }
            set { SetPropertyValue<string>("Zip", ref fZip, value); }
        }
        bool fPickupChk;
        public bool PickupChk
        {
            get { return fPickupChk; }
            set { SetPropertyValue<bool>("PickupChk", ref fPickupChk, value); }
        }
        double fPayAmt;
        public double PayAmt
        {
            get { return fPayAmt; }
            set { SetPropertyValue<double>("PayAmt", ref fPayAmt, value); }
        }
        int fPeril;
        public int Peril
        {
            get { return fPeril; }
            set { SetPropertyValue<int>("Peril", ref fPeril, value); }
        }
        string fxxUser;
        [Size(30)]
        public string xxUser
        {
            get { return fxxUser; }
            set { SetPropertyValue<string>("xxUser", ref fxxUser, value); }
        }
        bool fAllocated;
        public bool Allocated
        {
            get { return fAllocated; }
            set { SetPropertyValue<bool>("Allocated", ref fAllocated, value); }
        }
        bool fPIPPayment;
        public bool PIPPayment
        {
            get { return fPIPPayment; }
            set { SetPropertyValue<bool>("PIPPayment", ref fPIPPayment, value); }
        }
        string fPIPBillKey;
        [Size(20)]
        public string PIPBillKey
        {
            get { return fPIPBillKey; }
            set { SetPropertyValue<string>("PIPBillKey", ref fPIPBillKey, value); }
        }
        double fPIPLAEAmt;
        public double PIPLAEAmt
        {
            get { return fPIPLAEAmt; }
            set { SetPropertyValue<double>("PIPLAEAmt", ref fPIPLAEAmt, value); }
        }
        bool fReportedToBank;
        public bool ReportedToBank
        {
            get { return fReportedToBank; }
            set { SetPropertyValue<bool>("ReportedToBank", ref fReportedToBank, value); }
        }
        int fCheckStatus;
        public int CheckStatus
        {
            get { return fCheckStatus; }
            set { SetPropertyValue<int>("CheckStatus", ref fCheckStatus, value); }
        }
        DateTime fCheckStatusDate;
        public DateTime CheckStatusDate
        {
            get { return fCheckStatusDate; }
            set { SetPropertyValue<DateTime>("CheckStatusDate", ref fCheckStatusDate, value); }
        }
        string fPayType;
        [Size(4)]
        public string PayType
        {
            get { return fPayType; }
            set { SetPropertyValue<string>("PayType", ref fPayType, value); }
        }
        bool fReportVoidToBank;
        public bool ReportVoidToBank
        {
            get { return fReportVoidToBank; }
            set { SetPropertyValue<bool>("ReportVoidToBank", ref fReportVoidToBank, value); }
        }
        string fBatchNo;
        [Size(20)]
        public string BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<string>("BatchNo", ref fBatchNo, value); }
        }
        string fPerilDesc;
        [Size(10)]
        public string PerilDesc
        {
            get { return fPerilDesc; }
            set { SetPropertyValue<string>("PerilDesc", ref fPerilDesc, value); }
        }
        DateTime fReportedToBankDate;
        public DateTime ReportedToBankDate
        {
            get { return fReportedToBankDate; }
            set { SetPropertyValue<DateTime>("ReportedToBankDate", ref fReportedToBankDate, value); }
        }
        string fBillKey;
        [Size(20)]
        public string BillKey
        {
            get { return fBillKey; }
            set { SetPropertyValue<string>("BillKey", ref fBillKey, value); }
        }
        string fTransmittal;
        [Size(8000)]
        public string Transmittal
        {
            get { return fTransmittal; }
            set { SetPropertyValue<string>("Transmittal", ref fTransmittal, value); }
        }
        public ClaimsChecks(Session session) : base(session) { }
        public ClaimsChecks() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsPIPInvoiceLines : XPLiteObject
    {
        int fPALMSInvNo;
        public int PALMSInvNo
        {
            get { return fPALMSInvNo; }
            set { SetPropertyValue<int>("PALMSInvNo", ref fPALMSInvNo, value); }
        }
        int fInvLineNo;
        public int InvLineNo
        {
            get { return fInvLineNo; }
            set { SetPropertyValue<int>("InvLineNo", ref fInvLineNo, value); }
        }
        DateTime fDateFrom;
        public DateTime DateFrom
        {
            get { return fDateFrom; }
            set { SetPropertyValue<DateTime>("DateFrom", ref fDateFrom, value); }
        }
        DateTime fDateTo;
        public DateTime DateTo
        {
            get { return fDateTo; }
            set { SetPropertyValue<DateTime>("DateTo", ref fDateTo, value); }
        }
        string fCPTCode;
        [Size(15)]
        public string CPTCode
        {
            get { return fCPTCode; }
            set { SetPropertyValue<string>("CPTCode", ref fCPTCode, value); }
        }
        string fCPTDesc;
        [Size(50)]
        public string CPTDesc
        {
            get { return fCPTDesc; }
            set { SetPropertyValue<string>("CPTDesc", ref fCPTDesc, value); }
        }
        string fOrigCPTCode;
        [Size(15)]
        public string OrigCPTCode
        {
            get { return fOrigCPTCode; }
            set { SetPropertyValue<string>("OrigCPTCode", ref fOrigCPTCode, value); }
        }
        string fOrigCPTDesc;
        [Size(50)]
        public string OrigCPTDesc
        {
            get { return fOrigCPTDesc; }
            set { SetPropertyValue<string>("OrigCPTDesc", ref fOrigCPTDesc, value); }
        }
        int fUnits;
        public int Units
        {
            get { return fUnits; }
            set { SetPropertyValue<int>("Units", ref fUnits, value); }
        }
        double fAmtBilled;
        public double AmtBilled
        {
            get { return fAmtBilled; }
            set { SetPropertyValue<double>("AmtBilled", ref fAmtBilled, value); }
        }
        double fAmountAllowedPerUnit;
        public double AmountAllowedPerUnit
        {
            get { return fAmountAllowedPerUnit; }
            set { SetPropertyValue<double>("AmountAllowedPerUnit", ref fAmountAllowedPerUnit, value); }
        }
        double fAmountAllowed;
        public double AmountAllowed
        {
            get { return fAmountAllowed; }
            set { SetPropertyValue<double>("AmountAllowed", ref fAmountAllowed, value); }
        }
        double fAnesthesiaMinutes;
        public double AnesthesiaMinutes
        {
            get { return fAnesthesiaMinutes; }
            set { SetPropertyValue<double>("AnesthesiaMinutes", ref fAnesthesiaMinutes, value); }
        }
        bool fAnesthesiaBilledByDoctor;
        public bool AnesthesiaBilledByDoctor
        {
            get { return fAnesthesiaBilledByDoctor; }
            set { SetPropertyValue<bool>("AnesthesiaBilledByDoctor", ref fAnesthesiaBilledByDoctor, value); }
        }
        double fAnesthesiaBase;
        public double AnesthesiaBase
        {
            get { return fAnesthesiaBase; }
            set { SetPropertyValue<double>("AnesthesiaBase", ref fAnesthesiaBase, value); }
        }
        string fAmbulanceZip;
        [Size(5)]
        public string AmbulanceZip
        {
            get { return fAmbulanceZip; }
            set { SetPropertyValue<string>("AmbulanceZip", ref fAmbulanceZip, value); }
        }
        int fAmbulanceLocality;
        public int AmbulanceLocality
        {
            get { return fAmbulanceLocality; }
            set { SetPropertyValue<int>("AmbulanceLocality", ref fAmbulanceLocality, value); }
        }
        string fAmbulanceAreaType;
        [Size(1)]
        public string AmbulanceAreaType
        {
            get { return fAmbulanceAreaType; }
            set { SetPropertyValue<string>("AmbulanceAreaType", ref fAmbulanceAreaType, value); }
        }
        bool fDenied;
        public bool Denied
        {
            get { return fDenied; }
            set { SetPropertyValue<bool>("Denied", ref fDenied, value); }
        }
        bool fBundled;
        public bool Bundled
        {
            get { return fBundled; }
            set { SetPropertyValue<bool>("Bundled", ref fBundled, value); }
        }
        string fBundledCPTCode;
        [Size(15)]
        public string BundledCPTCode
        {
            get { return fBundledCPTCode; }
            set { SetPropertyValue<string>("BundledCPTCode", ref fBundledCPTCode, value); }
        }
        string fReasonCode;
        [Size(10)]
        public string ReasonCode
        {
            get { return fReasonCode; }
            set { SetPropertyValue<string>("ReasonCode", ref fReasonCode, value); }
        }
        string fReasonCodeDesc;
        public string ReasonCodeDesc
        {
            get { return fReasonCodeDesc; }
            set { SetPropertyValue<string>("ReasonCodeDesc", ref fReasonCodeDesc, value); }
        }
        string fDuplicateInvoiceNo;
        [Size(20)]
        public string DuplicateInvoiceNo
        {
            get { return fDuplicateInvoiceNo; }
            set { SetPropertyValue<string>("DuplicateInvoiceNo", ref fDuplicateInvoiceNo, value); }
        }
        int fIndexID;
        [Key]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public ClaimsPIPInvoiceLines(Session session) : base(session) { }
        public ClaimsPIPInvoiceLines() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class RatingCars : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fMainIndexID;
        public int MainIndexID
        {
            get { return fMainIndexID; }
            set { SetPropertyValue<int>("MainIndexID", ref fMainIndexID, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        int fRatedOprIndex;
        public int RatedOprIndex
        {
            get { return fRatedOprIndex; }
            set { SetPropertyValue<int>("RatedOprIndex", ref fRatedOprIndex, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        bool fABS;
        public bool ABS
        {
            get { return fABS; }
            set { SetPropertyValue<bool>("ABS", ref fABS, value); }
        }
        bool fARB;
        public bool ARB
        {
            get { return fARB; }
            set { SetPropertyValue<bool>("ARB", ref fARB, value); }
        }
        double fAPCDiscAmt;
        public double APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<double>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        double fABSDiscAmt;
        public double ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<double>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        double fARBDiscAmt;
        public double ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<double>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        double fMCDiscAmt;
        public double MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<double>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        double fRenDiscAmt;
        public double RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<double>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        double fTransferDiscAmt;
        public double TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<double>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        double fSSDDiscAmt;
        public double SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<double>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        double fATDDiscAmt;
        public double ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<double>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        double fDirectRepairDiscAmt;
        public double DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<double>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        double fWLDiscAmt;
        public double WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<double>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        double fSDDiscAmt;
        public double SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<double>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        double fPointSurgAmt;
        public double PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<double>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        double fOOSLicSurgAmt;
        public double OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<double>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        double fInexpSurgAmt;
        public double InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<double>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        double fInternationalSurgAmt;
        public double InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<double>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        double fPriorBalanceSurgAmt;
        public double PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<double>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        double fNoHitSurgAmt;
        public double NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<double>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        double fUnacceptSurgAmt;
        public double UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<double>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        double fMeritDUIAmt;
        public double MeritDUIAmt
        {
            get { return fMeritDUIAmt; }
            set { SetPropertyValue<double>("MeritDUIAmt", ref fMeritDUIAmt, value); }
        }
        double fMeritACCAmt;
        public double MeritACCAmt
        {
            get { return fMeritACCAmt; }
            set { SetPropertyValue<double>("MeritACCAmt", ref fMeritACCAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        double fBIAnnlPrem;
        public double BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<double>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        double fPDAnnlPrem;
        public double PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<double>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        double fPIPAnnlPrem;
        public double PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<double>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        double fMPAnnlPrem;
        public double MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<double>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        double fUMAnnlPrem;
        public double UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<double>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        double fCompAnnlPrem;
        public double CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<double>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        double fCollAnnlPrem;
        public double CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<double>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        double fCarRatingValue;
        public double CarRatingValue
        {
            get { return fCarRatingValue; }
            set { SetPropertyValue<double>("CarRatingValue", ref fCarRatingValue, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        double fBIAnnlPremRaw;
        public double BIAnnlPremRaw
        {
            get { return fBIAnnlPremRaw; }
            set { SetPropertyValue<double>("BIAnnlPremRaw", ref fBIAnnlPremRaw, value); }
        }
        double fPIPAnnlPremRaw;
        public double PIPAnnlPremRaw
        {
            get { return fPIPAnnlPremRaw; }
            set { SetPropertyValue<double>("PIPAnnlPremRaw", ref fPIPAnnlPremRaw, value); }
        }
        double fPDAnnlPremRaw;
        public double PDAnnlPremRaw
        {
            get { return fPDAnnlPremRaw; }
            set { SetPropertyValue<double>("PDAnnlPremRaw", ref fPDAnnlPremRaw, value); }
        }
        double fUMAnnlPremRaw;
        public double UMAnnlPremRaw
        {
            get { return fUMAnnlPremRaw; }
            set { SetPropertyValue<double>("UMAnnlPremRaw", ref fUMAnnlPremRaw, value); }
        }
        double fMPAnnlPremRaw;
        public double MPAnnlPremRaw
        {
            get { return fMPAnnlPremRaw; }
            set { SetPropertyValue<double>("MPAnnlPremRaw", ref fMPAnnlPremRaw, value); }
        }
        double fCompAnnlPremRaw;
        public double CompAnnlPremRaw
        {
            get { return fCompAnnlPremRaw; }
            set { SetPropertyValue<double>("CompAnnlPremRaw", ref fCompAnnlPremRaw, value); }
        }
        double fCollAnnlPremRaw;
        public double CollAnnlPremRaw
        {
            get { return fCollAnnlPremRaw; }
            set { SetPropertyValue<double>("CollAnnlPremRaw", ref fCollAnnlPremRaw, value); }
        }

        [NonPersistent]
        public string PolicyNo;
        [NonPersistent]
        public DateTime EffDate;
        [NonPersistent]
        public DateTime ExpDate;
        [NonPersistent]
        public Boolean Deleted;
        [NonPersistent]
        public string VehDoors;
        [NonPersistent]
        public string ISOSymbol;
        [NonPersistent]
        public string ISOPhyDamSymbol;
        [NonPersistent]
        public Boolean Is4wd;
        [NonPersistent]
        public Boolean IsDRL;
        [NonPersistent]
        public Boolean IsIneligible;
        [NonPersistent]
        public Boolean IsHybrid;

        public RatingCars(Session session) : base(session) { }
        public RatingCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyCars : XPCustomObject
    {
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public PolicyCars(Session session) : base(session) { }
        public PolicyCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyPrintSuspense : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fFormType;
        [Size(50)]
        public string FormType
        {
            get { return fFormType; }
            set { SetPropertyValue<string>("FormType", ref fFormType, value); }
        }
        string fFormDesc;
        [Size(150)]
        public string FormDesc
        {
            get { return fFormDesc; }
            set { SetPropertyValue<string>("FormDesc", ref fFormDesc, value); }
        }
        int fHistoryID;
        public int HistoryID
        {
            get { return fHistoryID; }
            set { SetPropertyValue<int>("HistoryID", ref fHistoryID, value); }
        }
        DateTime fDateRequested;
        public DateTime DateRequested
        {
            get { return fDateRequested; }
            set { SetPropertyValue<DateTime>("DateRequested", ref fDateRequested, value); }
        }
        string fUserName;
        [Size(50)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        bool fPrinted;
        public bool Printed
        {
            get { return fPrinted; }
            set { SetPropertyValue<bool>("Printed", ref fPrinted, value); }
        }
        bool fIgnore;
        public bool Ignore
        {
            get { return fIgnore; }
            set { SetPropertyValue<bool>("Ignore", ref fIgnore, value); }
        }
        bool fError;
        public bool Error
        {
            get { return fError; }
            set { SetPropertyValue<bool>("Error", ref fError, value); }
        }
        string fErrorMsg;
        [Size(50)]
        public string ErrorMsg
        {
            get { return fErrorMsg; }
            set { SetPropertyValue<string>("ErrorMsg", ref fErrorMsg, value); }
        }
        public PolicyPrintSuspense(Session session) : base(session) { }
        public PolicyPrintSuspense() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsFirstReport : XPLiteObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fRptDate;
        public DateTime RptDate
        {
            get { return fRptDate; }
            set { SetPropertyValue<DateTime>("RptDate", ref fRptDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fPolicyRateCycle;
        [Size(50)]
        public string PolicyRateCycle
        {
            get { return fPolicyRateCycle; }
            set { SetPropertyValue<string>("PolicyRateCycle", ref fPolicyRateCycle, value); }
        }
        string fReinsuranceTreaty;
        [Size(50)]
        public string ReinsuranceTreaty
        {
            get { return fReinsuranceTreaty; }
            set { SetPropertyValue<string>("ReinsuranceTreaty", ref fReinsuranceTreaty, value); }
        }
        bool fMaterialMisrep;
        public bool MaterialMisrep
        {
            get { return fMaterialMisrep; }
            set { SetPropertyValue<bool>("MaterialMisrep", ref fMaterialMisrep, value); }
        }
        bool fAtFault;
        public bool AtFault
        {
            get { return fAtFault; }
            set { SetPropertyValue<bool>("AtFault", ref fAtFault, value); }
        }
        bool fInSuit;
        public bool InSuit
        {
            get { return fInSuit; }
            set { SetPropertyValue<bool>("InSuit", ref fInSuit, value); }
        }
        bool fSubro;
        public bool Subro
        {
            get { return fSubro; }
            set { SetPropertyValue<bool>("Subro", ref fSubro, value); }
        }
        bool fSalvage;
        public bool Salvage
        {
            get { return fSalvage; }
            set { SetPropertyValue<bool>("Salvage", ref fSalvage, value); }
        }
        bool fVehListed;
        public bool VehListed
        {
            get { return fVehListed; }
            set { SetPropertyValue<bool>("VehListed", ref fVehListed, value); }
        }
        int fVehNo;
        public int VehNo
        {
            get { return fVehNo; }
            set { SetPropertyValue<int>("VehNo", ref fVehNo, value); }
        }
        string fVehYear;
        [Size(4)]
        public string VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<string>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(75)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel;
        public string VehModel
        {
            get { return fVehModel; }
            set { SetPropertyValue<string>("VehModel", ref fVehModel, value); }
        }
        string fPriorDamage;
        public string PriorDamage
        {
            get { return fPriorDamage; }
            set { SetPropertyValue<string>("PriorDamage", ref fPriorDamage, value); }
        }
        bool fDrivListed;
        public bool DrivListed
        {
            get { return fDrivListed; }
            set { SetPropertyValue<bool>("DrivListed", ref fDrivListed, value); }
        }
        int fDrivNo;
        public int DrivNo
        {
            get { return fDrivNo; }
            set { SetPropertyValue<int>("DrivNo", ref fDrivNo, value); }
        }
        bool fResident;
        public bool Resident
        {
            get { return fResident; }
            set { SetPropertyValue<bool>("Resident", ref fResident, value); }
        }
        string fDrivName;
        public string DrivName
        {
            get { return fDrivName; }
            set { SetPropertyValue<string>("DrivName", ref fDrivName, value); }
        }
        int fDrivAge;
        public int DrivAge
        {
            get { return fDrivAge; }
            set { SetPropertyValue<int>("DrivAge", ref fDrivAge, value); }
        }
        string fDrivSex;
        [Size(1)]
        public string DrivSex
        {
            get { return fDrivSex; }
            set { SetPropertyValue<string>("DrivSex", ref fDrivSex, value); }
        }
        string fDrivRelation;
        [Size(50)]
        public string DrivRelation
        {
            get { return fDrivRelation; }
            set { SetPropertyValue<string>("DrivRelation", ref fDrivRelation, value); }
        }
        bool fCCForms;
        public bool CCForms
        {
            get { return fCCForms; }
            set { SetPropertyValue<bool>("CCForms", ref fCCForms, value); }
        }
        string fCCAddressee;
        [Size(25)]
        public string CCAddressee
        {
            get { return fCCAddressee; }
            set { SetPropertyValue<string>("CCAddressee", ref fCCAddressee, value); }
        }
        string fCCAddress;
        [Size(25)]
        public string CCAddress
        {
            get { return fCCAddress; }
            set { SetPropertyValue<string>("CCAddress", ref fCCAddress, value); }
        }
        string fCCCity;
        [Size(15)]
        public string CCCity
        {
            get { return fCCCity; }
            set { SetPropertyValue<string>("CCCity", ref fCCCity, value); }
        }
        string fCCState;
        [Size(2)]
        public string CCState
        {
            get { return fCCState; }
            set { SetPropertyValue<string>("CCState", ref fCCState, value); }
        }
        string fCCZip;
        [Size(10)]
        public string CCZip
        {
            get { return fCCZip; }
            set { SetPropertyValue<string>("CCZip", ref fCCZip, value); }
        }
        int fPolTerritory;
        public int PolTerritory
        {
            get { return fPolTerritory; }
            set { SetPropertyValue<int>("PolTerritory", ref fPolTerritory, value); }
        }
        string fPolInsured;
        public string PolInsured
        {
            get { return fPolInsured; }
            set { SetPropertyValue<string>("PolInsured", ref fPolInsured, value); }
        }
        string fPolZip;
        [Size(10)]
        public string PolZip
        {
            get { return fPolZip; }
            set { SetPropertyValue<string>("PolZip", ref fPolZip, value); }
        }
        string fPolPhone;
        public string PolPhone
        {
            get { return fPolPhone; }
            set { SetPropertyValue<string>("PolPhone", ref fPolPhone, value); }
        }
        bool fNRPolicy;
        public bool NRPolicy
        {
            get { return fNRPolicy; }
            set { SetPropertyValue<bool>("NRPolicy", ref fNRPolicy, value); }
        }
        int fNoClaimants;
        public int NoClaimants
        {
            get { return fNoClaimants; }
            set { SetPropertyValue<int>("NoClaimants", ref fNoClaimants, value); }
        }
        string fLossDesc;
        [Size(3000)]
        public string LossDesc
        {
            get { return fLossDesc; }
            set { SetPropertyValue<string>("LossDesc", ref fLossDesc, value); }
        }
        float fPerClaimOverride;
        public float PerClaimOverride
        {
            get { return fPerClaimOverride; }
            set { SetPropertyValue<float>("PerClaimOverride", ref fPerClaimOverride, value); }
        }
        float fPerClaimantOverride;
        public float PerClaimantOverride
        {
            get { return fPerClaimantOverride; }
            set { SetPropertyValue<float>("PerClaimantOverride", ref fPerClaimantOverride, value); }
        }
        string fLossLoc;
        [Size(40)]
        public string LossLoc
        {
            get { return fLossLoc; }
            set { SetPropertyValue<string>("LossLoc", ref fLossLoc, value); }
        }
        string fVin;
        public string Vin
        {
            get { return fVin; }
            set { SetPropertyValue<string>("Vin", ref fVin, value); }
        }
        int fLineOfBusiness;
        public int LineOfBusiness
        {
            get { return fLineOfBusiness; }
            set { SetPropertyValue<int>("LineOfBusiness", ref fLineOfBusiness, value); }
        }
        string fRatingState;
        public string RatingState
        {
            get { return fRatingState; }
            set { SetPropertyValue<string>("RatingState", ref fRatingState, value); }
        }
        string fReportedBy;
        public string ReportedBy
        {
            get { return fReportedBy; }
            set { SetPropertyValue<string>("ReportedBy", ref fReportedBy, value); }
        }
        string fLossCity;
        public string LossCity
        {
            get { return fLossCity; }
            set { SetPropertyValue<string>("LossCity", ref fLossCity, value); }
        }
        string fLossState;
        public string LossState
        {
            get { return fLossState; }
            set { SetPropertyValue<string>("LossState", ref fLossState, value); }
        }
        int fSeverity;
        public int Severity
        {
            get { return fSeverity; }
            set { SetPropertyValue<int>("Severity", ref fSeverity, value); }
        }
        string fCMSInjuryCause;
        [Size(50)]
        public string CMSInjuryCause
        {
            get { return fCMSInjuryCause; }
            set { SetPropertyValue<string>("CMSInjuryCause", ref fCMSInjuryCause, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        public ClaimsFirstReport(Session session) : base(session) { }
        public ClaimsFirstReport() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditCoverageData : XPLiteObject
    {
        string fPolicyNo;
        [Size(15)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fVehIndex;
        public int VehIndex
        {
            get { return fVehIndex; }
            set { SetPropertyValue<int>("VehIndex", ref fVehIndex, value); }
        }
        int fCompany;
        public int Company
        {
            get { return fCompany; }
            set { SetPropertyValue<int>("Company", ref fCompany, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(10)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        int fActionIndexID;
        public int ActionIndexID
        {
            get { return fActionIndexID; }
            set { SetPropertyValue<int>("ActionIndexID", ref fActionIndexID, value); }
        }
        int fTermIndexID;
        public int TermIndexID
        {
            get { return fTermIndexID; }
            set { SetPropertyValue<int>("TermIndexID", ref fTermIndexID, value); }
        }
        string fCoverage;
        [Size(15)]
        public string Coverage
        {
            get { return fCoverage; }
            set { SetPropertyValue<string>("Coverage", ref fCoverage, value); }
        }
        bool fIsFee;
        public bool IsFee
        {
            get { return fIsFee; }
            set { SetPropertyValue<bool>("IsFee", ref fIsFee, value); }
        }
        bool fNegate;
        public bool Negate
        {
            get { return fNegate; }
            set { SetPropertyValue<bool>("Negate", ref fNegate, value); }
        }
        bool fIgnoreRecord;
        public bool IgnoreRecord
        {
            get { return fIgnoreRecord; }
            set { SetPropertyValue<bool>("IgnoreRecord", ref fIgnoreRecord, value); }
        }
        int fIgnoreIndexID;
        public int IgnoreIndexID
        {
            get { return fIgnoreIndexID; }
            set { SetPropertyValue<int>("IgnoreIndexID", ref fIgnoreIndexID, value); }
        }
        string fAction;
        [Size(25)]
        public string Action
        {
            get { return fAction; }
            set { SetPropertyValue<string>("Action", ref fAction, value); }
        }
        string fOrigAction;
        [Size(25)]
        public string OrigAction
        {
            get { return fOrigAction; }
            set { SetPropertyValue<string>("OrigAction", ref fOrigAction, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        double fProRata;
        public double ProRata
        {
            get { return fProRata; }
            set { SetPropertyValue<double>("ProRata", ref fProRata, value); }
        }
        DateTime fEarnStart;
        public DateTime EarnStart
        {
            get { return fEarnStart; }
            set { SetPropertyValue<DateTime>("EarnStart", ref fEarnStart, value); }
        }
        DateTime fEarnStop;
        public DateTime EarnStop
        {
            get { return fEarnStop; }
            set { SetPropertyValue<DateTime>("EarnStop", ref fEarnStop, value); }
        }
        int fEarnNoDays;
        public int EarnNoDays
        {
            get { return fEarnNoDays; }
            set { SetPropertyValue<int>("EarnNoDays", ref fEarnNoDays, value); }
        }
        int fAnnualized;
        public int Annualized
        {
            get { return fAnnualized; }
            set { SetPropertyValue<int>("Annualized", ref fAnnualized, value); }
        }
        double fWritten;
        public double Written
        {
            get { return fWritten; }
            set { SetPropertyValue<double>("Written", ref fWritten, value); }
        }
        double fOneDayEarn;
        public double OneDayEarn
        {
            get { return fOneDayEarn; }
            set { SetPropertyValue<double>("OneDayEarn", ref fOneDayEarn, value); }
        }
        double fLastDayEarn;
        public double LastDayEarn
        {
            get { return fLastDayEarn; }
            set { SetPropertyValue<double>("LastDayEarn", ref fLastDayEarn, value); }
        }
        double fEarnedSum;
        public double EarnedSum
        {
            get { return fEarnedSum; }
            set { SetPropertyValue<double>("EarnedSum", ref fEarnedSum, value); }
        }
        bool fChangeDateEnd;
        public bool ChangeDateEnd
        {
            get { return fChangeDateEnd; }
            set { SetPropertyValue<bool>("ChangeDateEnd", ref fChangeDateEnd, value); }
        }
        public AuditCoverageData(Session session) : base(session) { }
        public AuditCoverageData() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyNotes : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fIndexNo;
        public int IndexNo
        {
            get { return fIndexNo; }
            set { SetPropertyValue<int>("IndexNo", ref fIndexNo, value); }
        }
        string fUserBy;
        [Size(50)]
        public string UserBy
        {
            get { return fUserBy; }
            set { SetPropertyValue<string>("UserBy", ref fUserBy, value); }
        }
        string fNotes;
        [Size(8000)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        DateTime fDate;
        public DateTime Date
        {
            get { return fDate; }
            set { SetPropertyValue<DateTime>("Date", ref fDate, value); }
        }
        bool fisDiary;
        public bool isDiary
        {
            get { return fisDiary; }
            set { SetPropertyValue<bool>("isDiary", ref fisDiary, value); }
        }
        public PolicyNotes(Session session) : base(session) { }
        public PolicyNotes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class CoverageReasons : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        string fFileName;
        [Size(255)]
        public string FileName
        {
            get { return fFileName; }
            set { SetPropertyValue<string>("FileName", ref fFileName, value); }
        }
        string fCoverageReason;
        [Size(2000)]
        public string CoverageReason
        {
            get { return fCoverageReason; }
            set { SetPropertyValue<string>("CoverageReason", ref fCoverageReason, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        public CoverageReasons(Session session) : base(session) { }
        public CoverageReasons() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsStaging : XPLiteObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fRptDate;
        public DateTime RptDate
        {
            get { return fRptDate; }
            set { SetPropertyValue<DateTime>("RptDate", ref fRptDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fPolicyRateCycle;
        [Size(50)]
        public string PolicyRateCycle
        {
            get { return fPolicyRateCycle; }
            set { SetPropertyValue<string>("PolicyRateCycle", ref fPolicyRateCycle, value); }
        }
        string fReinsuranceTreaty;
        [Size(50)]
        public string ReinsuranceTreaty
        {
            get { return fReinsuranceTreaty; }
            set { SetPropertyValue<string>("ReinsuranceTreaty", ref fReinsuranceTreaty, value); }
        }
        bool fMaterialMisrep;
        public bool MaterialMisrep
        {
            get { return fMaterialMisrep; }
            set { SetPropertyValue<bool>("MaterialMisrep", ref fMaterialMisrep, value); }
        }
        bool fAtFault;
        public bool AtFault
        {
            get { return fAtFault; }
            set { SetPropertyValue<bool>("AtFault", ref fAtFault, value); }
        }
        bool fInSuit;
        public bool InSuit
        {
            get { return fInSuit; }
            set { SetPropertyValue<bool>("InSuit", ref fInSuit, value); }
        }
        bool fSubro;
        public bool Subro
        {
            get { return fSubro; }
            set { SetPropertyValue<bool>("Subro", ref fSubro, value); }
        }
        bool fSalvage;
        public bool Salvage
        {
            get { return fSalvage; }
            set { SetPropertyValue<bool>("Salvage", ref fSalvage, value); }
        }
        bool fVehListed;
        public bool VehListed
        {
            get { return fVehListed; }
            set { SetPropertyValue<bool>("VehListed", ref fVehListed, value); }
        }
        int fVehNo;
        public int VehNo
        {
            get { return fVehNo; }
            set { SetPropertyValue<int>("VehNo", ref fVehNo, value); }
        }
        string fVehYear;
        [Size(4)]
        public string VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<string>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(75)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel;
        public string VehModel
        {
            get { return fVehModel; }
            set { SetPropertyValue<string>("VehModel", ref fVehModel, value); }
        }
        string fPriorDamage;
        public string PriorDamage
        {
            get { return fPriorDamage; }
            set { SetPropertyValue<string>("PriorDamage", ref fPriorDamage, value); }
        }
        bool fDrivListed;
        public bool DrivListed
        {
            get { return fDrivListed; }
            set { SetPropertyValue<bool>("DrivListed", ref fDrivListed, value); }
        }
        int fDrivNo;
        public int DrivNo
        {
            get { return fDrivNo; }
            set { SetPropertyValue<int>("DrivNo", ref fDrivNo, value); }
        }
        bool fResident;
        public bool Resident
        {
            get { return fResident; }
            set { SetPropertyValue<bool>("Resident", ref fResident, value); }
        }
        string fDrivName;
        public string DrivName
        {
            get { return fDrivName; }
            set { SetPropertyValue<string>("DrivName", ref fDrivName, value); }
        }
        int fDrivAge;
        public int DrivAge
        {
            get { return fDrivAge; }
            set { SetPropertyValue<int>("DrivAge", ref fDrivAge, value); }
        }
        string fDrivSex;
        [Size(1)]
        public string DrivSex
        {
            get { return fDrivSex; }
            set { SetPropertyValue<string>("DrivSex", ref fDrivSex, value); }
        }
        string fDrivRelation;
        [Size(50)]
        public string DrivRelation
        {
            get { return fDrivRelation; }
            set { SetPropertyValue<string>("DrivRelation", ref fDrivRelation, value); }
        }
        bool fCCForms;
        public bool CCForms
        {
            get { return fCCForms; }
            set { SetPropertyValue<bool>("CCForms", ref fCCForms, value); }
        }
        string fCCAddressee;
        [Size(25)]
        public string CCAddressee
        {
            get { return fCCAddressee; }
            set { SetPropertyValue<string>("CCAddressee", ref fCCAddressee, value); }
        }
        string fCCAddress;
        [Size(25)]
        public string CCAddress
        {
            get { return fCCAddress; }
            set { SetPropertyValue<string>("CCAddress", ref fCCAddress, value); }
        }
        string fCCCity;
        [Size(15)]
        public string CCCity
        {
            get { return fCCCity; }
            set { SetPropertyValue<string>("CCCity", ref fCCCity, value); }
        }
        string fCCState;
        [Size(2)]
        public string CCState
        {
            get { return fCCState; }
            set { SetPropertyValue<string>("CCState", ref fCCState, value); }
        }
        string fCCZip;
        [Size(10)]
        public string CCZip
        {
            get { return fCCZip; }
            set { SetPropertyValue<string>("CCZip", ref fCCZip, value); }
        }
        int fPolTerritory;
        public int PolTerritory
        {
            get { return fPolTerritory; }
            set { SetPropertyValue<int>("PolTerritory", ref fPolTerritory, value); }
        }
        string fPolInsured;
        public string PolInsured
        {
            get { return fPolInsured; }
            set { SetPropertyValue<string>("PolInsured", ref fPolInsured, value); }
        }
        string fPolZip;
        [Size(10)]
        public string PolZip
        {
            get { return fPolZip; }
            set { SetPropertyValue<string>("PolZip", ref fPolZip, value); }
        }
        string fPolPhone;
        public string PolPhone
        {
            get { return fPolPhone; }
            set { SetPropertyValue<string>("PolPhone", ref fPolPhone, value); }
        }
        bool fNRPolicy;
        public bool NRPolicy
        {
            get { return fNRPolicy; }
            set { SetPropertyValue<bool>("NRPolicy", ref fNRPolicy, value); }
        }
        int fNoClaimants;
        public int NoClaimants
        {
            get { return fNoClaimants; }
            set { SetPropertyValue<int>("NoClaimants", ref fNoClaimants, value); }
        }
        string fLossDesc;
        [Size(3000)]
        public string LossDesc
        {
            get { return fLossDesc; }
            set { SetPropertyValue<string>("LossDesc", ref fLossDesc, value); }
        }
        float fPerClaimOverride;
        public float PerClaimOverride
        {
            get { return fPerClaimOverride; }
            set { SetPropertyValue<float>("PerClaimOverride", ref fPerClaimOverride, value); }
        }
        float fPerClaimantOverride;
        public float PerClaimantOverride
        {
            get { return fPerClaimantOverride; }
            set { SetPropertyValue<float>("PerClaimantOverride", ref fPerClaimantOverride, value); }
        }
        string fLossLoc;
        [Size(40)]
        public string LossLoc
        {
            get { return fLossLoc; }
            set { SetPropertyValue<string>("LossLoc", ref fLossLoc, value); }
        }
        string fVin;
        public string Vin
        {
            get { return fVin; }
            set { SetPropertyValue<string>("Vin", ref fVin, value); }
        }
        int fLineOfBusiness;
        public int LineOfBusiness
        {
            get { return fLineOfBusiness; }
            set { SetPropertyValue<int>("LineOfBusiness", ref fLineOfBusiness, value); }
        }
        string fRatingState;
        public string RatingState
        {
            get { return fRatingState; }
            set { SetPropertyValue<string>("RatingState", ref fRatingState, value); }
        }
        string fReportedBy;
        public string ReportedBy
        {
            get { return fReportedBy; }
            set { SetPropertyValue<string>("ReportedBy", ref fReportedBy, value); }
        }
        string fLossCity;
        public string LossCity
        {
            get { return fLossCity; }
            set { SetPropertyValue<string>("LossCity", ref fLossCity, value); }
        }
        string fLossState;
        public string LossState
        {
            get { return fLossState; }
            set { SetPropertyValue<string>("LossState", ref fLossState, value); }
        }
        int fSeverity;
        public int Severity
        {
            get { return fSeverity; }
            set { SetPropertyValue<int>("Severity", ref fSeverity, value); }
        }
        string fCMSInjuryCause;
        [Size(50)]
        public string CMSInjuryCause
        {
            get { return fCMSInjuryCause; }
            set { SetPropertyValue<string>("CMSInjuryCause", ref fCMSInjuryCause, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        public ClaimsStaging(Session session) : base(session) { }
        public ClaimsStaging() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyFutureActions : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fAction;
        [Size(25)]
        public string Action
        {
            get { return fAction; }
            set { SetPropertyValue<string>("Action", ref fAction, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        DateTime fDueDate;
        public DateTime DueDate
        {
            get { return fDueDate; }
            set { SetPropertyValue<DateTime>("DueDate", ref fDueDate, value); }
        }
        DateTime fMailDate;
        public DateTime MailDate
        {
            get { return fMailDate; }
            set { SetPropertyValue<DateTime>("MailDate", ref fMailDate, value); }
        }
        bool fProcess;
        public bool Process
        {
            get { return fProcess; }
            set { SetPropertyValue<bool>("Process", ref fProcess, value); }
        }
        DateTime fProcessDate;
        public DateTime ProcessDate
        {
            get { return fProcessDate; }
            set { SetPropertyValue<DateTime>("ProcessDate", ref fProcessDate, value); }
        }
        bool fCancel;
        public bool Cancel
        {
            get { return fCancel; }
            set { SetPropertyValue<bool>("Cancel", ref fCancel, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        bool fPrinted;
        public bool Printed
        {
            get { return fPrinted; }
            set { SetPropertyValue<bool>("Printed", ref fPrinted, value); }
        }
        public PolicyFutureActions(Session session) : base(session) { }
        public PolicyFutureActions() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyBankInfo : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNumber;
        [Size(50)]
        public string PolicyNumber
        {
            get { return fPolicyNumber; }
            set { SetPropertyValue<string>("PolicyNumber", ref fPolicyNumber, value); }
        }
        string fBankAccountName;
        [Size(300)]
        public string BankAccountName
        {
            get { return fBankAccountName; }
            set { SetPropertyValue<string>("BankAccountName", ref fBankAccountName, value); }
        }
        string fAccount;
        [Size(400)]
        public string Account
        {
            get { return fAccount; }
            set { SetPropertyValue<string>("Account", ref fAccount, value); }
        }
        string fAccountType;
        [Size(300)]
        public string AccountType
        {
            get { return fAccountType; }
            set { SetPropertyValue<string>("AccountType", ref fAccountType, value); }
        }
        string fABA;
        [Size(300)]
        public string ABA
        {
            get { return fABA; }
            set { SetPropertyValue<string>("ABA", ref fABA, value); }
        }
        string fAccountNumber;
        [Size(400)]
        public string AccountNumber
        {
            get { return fAccountNumber; }
            set { SetPropertyValue<string>("AccountNumber", ref fAccountNumber, value); }
        }
        public PolicyBankInfo(Session session) : base(session) { }
        public PolicyBankInfo() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsPIPInvoiceLines_old : XPLiteObject
    {
        int fPALMSInvNo;
        public int PALMSInvNo
        {
            get { return fPALMSInvNo; }
            set { SetPropertyValue<int>("PALMSInvNo", ref fPALMSInvNo, value); }
        }
        int fInvLineNo;
        public int InvLineNo
        {
            get { return fInvLineNo; }
            set { SetPropertyValue<int>("InvLineNo", ref fInvLineNo, value); }
        }
        DateTime fDateFrom;
        public DateTime DateFrom
        {
            get { return fDateFrom; }
            set { SetPropertyValue<DateTime>("DateFrom", ref fDateFrom, value); }
        }
        DateTime fDateTo;
        public DateTime DateTo
        {
            get { return fDateTo; }
            set { SetPropertyValue<DateTime>("DateTo", ref fDateTo, value); }
        }
        string fCPTCode;
        [Size(15)]
        public string CPTCode
        {
            get { return fCPTCode; }
            set { SetPropertyValue<string>("CPTCode", ref fCPTCode, value); }
        }
        string fCPTDesc;
        [Size(50)]
        public string CPTDesc
        {
            get { return fCPTDesc; }
            set { SetPropertyValue<string>("CPTDesc", ref fCPTDesc, value); }
        }
        string fOrigCPTCode;
        [Size(15)]
        public string OrigCPTCode
        {
            get { return fOrigCPTCode; }
            set { SetPropertyValue<string>("OrigCPTCode", ref fOrigCPTCode, value); }
        }
        string fOrigCPTDesc;
        [Size(50)]
        public string OrigCPTDesc
        {
            get { return fOrigCPTDesc; }
            set { SetPropertyValue<string>("OrigCPTDesc", ref fOrigCPTDesc, value); }
        }
        int fUnits;
        public int Units
        {
            get { return fUnits; }
            set { SetPropertyValue<int>("Units", ref fUnits, value); }
        }
        double fAmtBilled;
        public double AmtBilled
        {
            get { return fAmtBilled; }
            set { SetPropertyValue<double>("AmtBilled", ref fAmtBilled, value); }
        }
        double fAmountAllowedPerUnit;
        public double AmountAllowedPerUnit
        {
            get { return fAmountAllowedPerUnit; }
            set { SetPropertyValue<double>("AmountAllowedPerUnit", ref fAmountAllowedPerUnit, value); }
        }
        double fAmountAllowed;
        public double AmountAllowed
        {
            get { return fAmountAllowed; }
            set { SetPropertyValue<double>("AmountAllowed", ref fAmountAllowed, value); }
        }
        double fAnesthesiaMinutes;
        public double AnesthesiaMinutes
        {
            get { return fAnesthesiaMinutes; }
            set { SetPropertyValue<double>("AnesthesiaMinutes", ref fAnesthesiaMinutes, value); }
        }
        bool fAnesthesiaBilledByDoctor;
        public bool AnesthesiaBilledByDoctor
        {
            get { return fAnesthesiaBilledByDoctor; }
            set { SetPropertyValue<bool>("AnesthesiaBilledByDoctor", ref fAnesthesiaBilledByDoctor, value); }
        }
        double fAnesthesiaBase;
        public double AnesthesiaBase
        {
            get { return fAnesthesiaBase; }
            set { SetPropertyValue<double>("AnesthesiaBase", ref fAnesthesiaBase, value); }
        }
        string fAmbulanceZip;
        [Size(5)]
        public string AmbulanceZip
        {
            get { return fAmbulanceZip; }
            set { SetPropertyValue<string>("AmbulanceZip", ref fAmbulanceZip, value); }
        }
        int fAmbulanceLocality;
        public int AmbulanceLocality
        {
            get { return fAmbulanceLocality; }
            set { SetPropertyValue<int>("AmbulanceLocality", ref fAmbulanceLocality, value); }
        }
        string fAmbulanceAreaType;
        [Size(1)]
        public string AmbulanceAreaType
        {
            get { return fAmbulanceAreaType; }
            set { SetPropertyValue<string>("AmbulanceAreaType", ref fAmbulanceAreaType, value); }
        }
        bool fDenied;
        public bool Denied
        {
            get { return fDenied; }
            set { SetPropertyValue<bool>("Denied", ref fDenied, value); }
        }
        bool fBundled;
        public bool Bundled
        {
            get { return fBundled; }
            set { SetPropertyValue<bool>("Bundled", ref fBundled, value); }
        }
        string fBundledCPTCode;
        [Size(15)]
        public string BundledCPTCode
        {
            get { return fBundledCPTCode; }
            set { SetPropertyValue<string>("BundledCPTCode", ref fBundledCPTCode, value); }
        }
        string fReasonCode;
        [Size(10)]
        public string ReasonCode
        {
            get { return fReasonCode; }
            set { SetPropertyValue<string>("ReasonCode", ref fReasonCode, value); }
        }
        string fReasonCodeDesc;
        public string ReasonCodeDesc
        {
            get { return fReasonCodeDesc; }
            set { SetPropertyValue<string>("ReasonCodeDesc", ref fReasonCodeDesc, value); }
        }
        string fDuplicateInvoiceNo;
        [Size(20)]
        public string DuplicateInvoiceNo
        {
            get { return fDuplicateInvoiceNo; }
            set { SetPropertyValue<string>("DuplicateInvoiceNo", ref fDuplicateInvoiceNo, value); }
        }
        int fIndexID;
        [Key]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public ClaimsPIPInvoiceLines_old(Session session) : base(session) { }
        public ClaimsPIPInvoiceLines_old() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class System_SubCode : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fCode;
        [Size(4)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>("Code", ref fCode, value); }
        }
        string fDescription;
        [Size(75)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }
        public System_SubCode(Session session) : base(session) { }
        public System_SubCode() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Claimants : XPLiteObject
    {
        string fClaimNo;
        [Size(75)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fClaimantName;
        [Size(105)]
        public string ClaimantName
        {
            get { return fClaimantName; }
            set { SetPropertyValue<string>("ClaimantName", ref fClaimantName, value); }
        }
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fPeril;
        public int Peril
        {
            get { return fPeril; }
            set { SetPropertyValue<int>("Peril", ref fPeril, value); }
        }
        string fPerilDesc;
        public string PerilDesc
        {
            get { return fPerilDesc; }
            set { SetPropertyValue<string>("PerilDesc", ref fPerilDesc, value); }
        }
        int fCompExt;
        public int CompExt
        {
            get { return fCompExt; }
            set { SetPropertyValue<int>("CompExt", ref fCompExt, value); }
        }
        string fCompExtDesc;
        [Size(25)]
        public string CompExtDesc
        {
            get { return fCompExtDesc; }
            set { SetPropertyValue<string>("CompExtDesc", ref fCompExtDesc, value); }
        }
        string fCatNo;
        [Size(8)]
        public string CatNo
        {
            get { return fCatNo; }
            set { SetPropertyValue<string>("CatNo", ref fCatNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fRptDate;
        public DateTime RptDate
        {
            get { return fRptDate; }
            set { SetPropertyValue<DateTime>("RptDate", ref fRptDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fClmntAddress;
        [Size(200)]
        public string ClmntAddress
        {
            get { return fClmntAddress; }
            set { SetPropertyValue<string>("ClmntAddress", ref fClmntAddress, value); }
        }
        string fClmntCity;
        [Size(30)]
        public string ClmntCity
        {
            get { return fClmntCity; }
            set { SetPropertyValue<string>("ClmntCity", ref fClmntCity, value); }
        }
        string fClmntState;
        [Size(2)]
        public string ClmntState
        {
            get { return fClmntState; }
            set { SetPropertyValue<string>("ClmntState", ref fClmntState, value); }
        }
        string fClmntZip;
        [Size(10)]
        public string ClmntZip
        {
            get { return fClmntZip; }
            set { SetPropertyValue<string>("ClmntZip", ref fClmntZip, value); }
        }
        string fClmntHome;
        [Size(14)]
        public string ClmntHome
        {
            get { return fClmntHome; }
            set { SetPropertyValue<string>("ClmntHome", ref fClmntHome, value); }
        }
        string fClmntWork;
        [Size(14)]
        public string ClmntWork
        {
            get { return fClmntWork; }
            set { SetPropertyValue<string>("ClmntWork", ref fClmntWork, value); }
        }
        string fYearMake;
        [Size(25)]
        public string YearMake
        {
            get { return fYearMake; }
            set { SetPropertyValue<string>("YearMake", ref fYearMake, value); }
        }
        string fBodyStyle;
        [Size(25)]
        public string BodyStyle
        {
            get { return fBodyStyle; }
            set { SetPropertyValue<string>("BodyStyle", ref fBodyStyle, value); }
        }
        string fPriorDamage;
        [Size(25)]
        public string PriorDamage
        {
            get { return fPriorDamage; }
            set { SetPropertyValue<string>("PriorDamage", ref fPriorDamage, value); }
        }
        string fTagInfo;
        [Size(10)]
        public string TagInfo
        {
            get { return fTagInfo; }
            set { SetPropertyValue<string>("TagInfo", ref fTagInfo, value); }
        }
        string fColor;
        [Size(10)]
        public string Color
        {
            get { return fColor; }
            set { SetPropertyValue<string>("Color", ref fColor, value); }
        }
        string fLocation;
        [Size(25)]
        public string Location
        {
            get { return fLocation; }
            set { SetPropertyValue<string>("Location", ref fLocation, value); }
        }
        string fBodyShop;
        [Size(25)]
        public string BodyShop
        {
            get { return fBodyShop; }
            set { SetPropertyValue<string>("BodyShop", ref fBodyShop, value); }
        }
        string fSalvageYard;
        [Size(25)]
        public string SalvageYard
        {
            get { return fSalvageYard; }
            set { SetPropertyValue<string>("SalvageYard", ref fSalvageYard, value); }
        }
        bool fDriveable;
        public bool Driveable
        {
            get { return fDriveable; }
            set { SetPropertyValue<bool>("Driveable", ref fDriveable, value); }
        }
        bool fTotalLoss;
        public bool TotalLoss
        {
            get { return fTotalLoss; }
            set { SetPropertyValue<bool>("TotalLoss", ref fTotalLoss, value); }
        }
        string fProvCode;
        public string ProvCode
        {
            get { return fProvCode; }
            set { SetPropertyValue<string>("ProvCode", ref fProvCode, value); }
        }
        string fProvName;
        public string ProvName
        {
            get { return fProvName; }
            set { SetPropertyValue<string>("ProvName", ref fProvName, value); }
        }
        string fProvAddr;
        public string ProvAddr
        {
            get { return fProvAddr; }
            set { SetPropertyValue<string>("ProvAddr", ref fProvAddr, value); }
        }
        string fProvCity;
        [Size(25)]
        public string ProvCity
        {
            get { return fProvCity; }
            set { SetPropertyValue<string>("ProvCity", ref fProvCity, value); }
        }
        string fProvSt;
        [Size(2)]
        public string ProvSt
        {
            get { return fProvSt; }
            set { SetPropertyValue<string>("ProvSt", ref fProvSt, value); }
        }
        string fProvZip;
        [Size(10)]
        public string ProvZip
        {
            get { return fProvZip; }
            set { SetPropertyValue<string>("ProvZip", ref fProvZip, value); }
        }
        string fProvPhone;
        [Size(14)]
        public string ProvPhone
        {
            get { return fProvPhone; }
            set { SetPropertyValue<string>("ProvPhone", ref fProvPhone, value); }
        }
        string fProvFax;
        [Size(14)]
        public string ProvFax
        {
            get { return fProvFax; }
            set { SetPropertyValue<string>("ProvFax", ref fProvFax, value); }
        }
        string fAttyCode;
        public string AttyCode
        {
            get { return fAttyCode; }
            set { SetPropertyValue<string>("AttyCode", ref fAttyCode, value); }
        }
        string fAttyName;
        public string AttyName
        {
            get { return fAttyName; }
            set { SetPropertyValue<string>("AttyName", ref fAttyName, value); }
        }
        string fStatus;
        [Size(15)]
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        string fReserveType;
        [Size(1)]
        public string ReserveType
        {
            get { return fReserveType; }
            set { SetPropertyValue<string>("ReserveType", ref fReserveType, value); }
        }
        int fReserveAmt;
        public int ReserveAmt
        {
            get { return fReserveAmt; }
            set { SetPropertyValue<int>("ReserveAmt", ref fReserveAmt, value); }
        }
        DateTime fOrigResDate;
        public DateTime OrigResDate
        {
            get { return fOrigResDate; }
            set { SetPropertyValue<DateTime>("OrigResDate", ref fOrigResDate, value); }
        }
        string fFileLoc;
        [Size(20)]
        public string FileLoc
        {
            get { return fFileLoc; }
            set { SetPropertyValue<string>("FileLoc", ref fFileLoc, value); }
        }
        string fAdjuster;
        [Size(20)]
        public string Adjuster
        {
            get { return fAdjuster; }
            set { SetPropertyValue<string>("Adjuster", ref fAdjuster, value); }
        }
        string fDescOfLoss;
        [Size(200)]
        public string DescOfLoss
        {
            get { return fDescOfLoss; }
            set { SetPropertyValue<string>("DescOfLoss", ref fDescOfLoss, value); }
        }
        int fALAE;
        public int ALAE
        {
            get { return fALAE; }
            set { SetPropertyValue<int>("ALAE", ref fALAE, value); }
        }
        int fULAE;
        public int ULAE
        {
            get { return fULAE; }
            set { SetPropertyValue<int>("ULAE", ref fULAE, value); }
        }
        string fSupervisor;
        [Size(250)]
        public string Supervisor
        {
            get { return fSupervisor; }
            set { SetPropertyValue<string>("Supervisor", ref fSupervisor, value); }
        }
        string fSIU;
        [Size(10)]
        public string SIU
        {
            get { return fSIU; }
            set { SetPropertyValue<string>("SIU", ref fSIU, value); }
        }
        DateTime fStatusDate;
        public DateTime StatusDate
        {
            get { return fStatusDate; }
            set { SetPropertyValue<DateTime>("StatusDate", ref fStatusDate, value); }
        }
        int fClaimantIndex;
        public int ClaimantIndex
        {
            get { return fClaimantIndex; }
            set { SetPropertyValue<int>("ClaimantIndex", ref fClaimantIndex, value); }
        }
        double fPDClaimantOverride;
        public double PDClaimantOverride
        {
            get { return fPDClaimantOverride; }
            set { SetPropertyValue<double>("PDClaimantOverride", ref fPDClaimantOverride, value); }
        }
        double fPIPClaimantOverride;
        public double PIPClaimantOverride
        {
            get { return fPIPClaimantOverride; }
            set { SetPropertyValue<double>("PIPClaimantOverride", ref fPIPClaimantOverride, value); }
        }
        bool fADNDInsured;
        public bool ADNDInsured
        {
            get { return fADNDInsured; }
            set { SetPropertyValue<bool>("ADNDInsured", ref fADNDInsured, value); }
        }
        bool fADNDSeatBelt;
        public bool ADNDSeatBelt
        {
            get { return fADNDSeatBelt; }
            set { SetPropertyValue<bool>("ADNDSeatBelt", ref fADNDSeatBelt, value); }
        }
        int fADNDInjuryLevel;
        public int ADNDInjuryLevel
        {
            get { return fADNDInjuryLevel; }
            set { SetPropertyValue<int>("ADNDInjuryLevel", ref fADNDInjuryLevel, value); }
        }
        string fPropertyDesc;
        [Size(30)]
        public string PropertyDesc
        {
            get { return fPropertyDesc; }
            set { SetPropertyValue<string>("PropertyDesc", ref fPropertyDesc, value); }
        }
        string fPropertyDamage;
        [Size(30)]
        public string PropertyDamage
        {
            get { return fPropertyDamage; }
            set { SetPropertyValue<string>("PropertyDamage", ref fPropertyDamage, value); }
        }
        string fPropertyLocation;
        [Size(30)]
        public string PropertyLocation
        {
            get { return fPropertyLocation; }
            set { SetPropertyValue<string>("PropertyLocation", ref fPropertyLocation, value); }
        }
        string fPropertyPOC;
        [Size(30)]
        public string PropertyPOC
        {
            get { return fPropertyPOC; }
            set { SetPropertyValue<string>("PropertyPOC", ref fPropertyPOC, value); }
        }
        string fPropertyAddr;
        [Size(30)]
        public string PropertyAddr
        {
            get { return fPropertyAddr; }
            set { SetPropertyValue<string>("PropertyAddr", ref fPropertyAddr, value); }
        }
        string fPropertyCity;
        [Size(20)]
        public string PropertyCity
        {
            get { return fPropertyCity; }
            set { SetPropertyValue<string>("PropertyCity", ref fPropertyCity, value); }
        }
        string fPropertyState;
        [Size(2)]
        public string PropertyState
        {
            get { return fPropertyState; }
            set { SetPropertyValue<string>("PropertyState", ref fPropertyState, value); }
        }
        string fPropertyZip;
        [Size(5)]
        public string PropertyZip
        {
            get { return fPropertyZip; }
            set { SetPropertyValue<string>("PropertyZip", ref fPropertyZip, value); }
        }
        string fPropertyPhone;
        [Size(14)]
        public string PropertyPhone
        {
            get { return fPropertyPhone; }
            set { SetPropertyValue<string>("PropertyPhone", ref fPropertyPhone, value); }
        }
        string fPropertyAltPhone;
        [Size(14)]
        public string PropertyAltPhone
        {
            get { return fPropertyAltPhone; }
            set { SetPropertyValue<string>("PropertyAltPhone", ref fPropertyAltPhone, value); }
        }
        string fRatingState;
        [Size(2)]
        public string RatingState
        {
            get { return fRatingState; }
            set { SetPropertyValue<string>("RatingState", ref fRatingState, value); }
        }
        string fHICN;
        [Size(15)]
        public string HICN
        {
            get { return fHICN; }
            set { SetPropertyValue<string>("HICN", ref fHICN, value); }
        }
        string fSSN;
        [Size(200)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        string fGender;
        [Size(1)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        string fCMSLastName;
        [Size(25)]
        public string CMSLastName
        {
            get { return fCMSLastName; }
            set { SetPropertyValue<string>("CMSLastName", ref fCMSLastName, value); }
        }
        string fCMSFirstName;
        [Size(25)]
        public string CMSFirstName
        {
            get { return fCMSFirstName; }
            set { SetPropertyValue<string>("CMSFirstName", ref fCMSFirstName, value); }
        }
        string fCMSMiddleInital;
        [Size(1)]
        public string CMSMiddleInital
        {
            get { return fCMSMiddleInital; }
            set { SetPropertyValue<string>("CMSMiddleInital", ref fCMSMiddleInital, value); }
        }
        bool fPropertyIsVeh;
        public bool PropertyIsVeh
        {
            get { return fPropertyIsVeh; }
            set { SetPropertyValue<bool>("PropertyIsVeh", ref fPropertyIsVeh, value); }
        }
        bool fTheftRecovery;
        public bool TheftRecovery
        {
            get { return fTheftRecovery; }
            set { SetPropertyValue<bool>("TheftRecovery", ref fTheftRecovery, value); }
        }
        bool fOwnerRetSalvage;
        public bool OwnerRetSalvage
        {
            get { return fOwnerRetSalvage; }
            set { SetPropertyValue<bool>("OwnerRetSalvage", ref fOwnerRetSalvage, value); }
        }
        DateTime fRecoveryDate;
        public DateTime RecoveryDate
        {
            get { return fRecoveryDate; }
            set { SetPropertyValue<DateTime>("RecoveryDate", ref fRecoveryDate, value); }
        }
        string fRecoveryAgency;
        [Size(30)]
        public string RecoveryAgency
        {
            get { return fRecoveryAgency; }
            set { SetPropertyValue<string>("RecoveryAgency", ref fRecoveryAgency, value); }
        }
        string fRecoveryState;
        [Size(2)]
        public string RecoveryState
        {
            get { return fRecoveryState; }
            set { SetPropertyValue<string>("RecoveryState", ref fRecoveryState, value); }
        }
        string fRecoveryCondition;
        [Size(20)]
        public string RecoveryCondition
        {
            get { return fRecoveryCondition; }
            set { SetPropertyValue<string>("RecoveryCondition", ref fRecoveryCondition, value); }
        }
        string fVehVIN;
        [Size(20)]
        public string VehVIN
        {
            get { return fVehVIN; }
            set { SetPropertyValue<string>("VehVIN", ref fVehVIN, value); }
        }
        bool fClmtIsBusiness;
        public bool ClmtIsBusiness
        {
            get { return fClmtIsBusiness; }
            set { SetPropertyValue<bool>("ClmtIsBusiness", ref fClmtIsBusiness, value); }
        }
        string fBusinessName;
        [Size(35)]
        public string BusinessName
        {
            get { return fBusinessName; }
            set { SetPropertyValue<string>("BusinessName", ref fBusinessName, value); }
        }
        bool fForceSubmit;
        public bool ForceSubmit
        {
            get { return fForceSubmit; }
            set { SetPropertyValue<bool>("ForceSubmit", ref fForceSubmit, value); }
        }
        string fFirstName;
        [Size(50)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fLastName;
        [Size(50)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fSuffix;
        [Size(5)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClmtLanguage;
        [Size(20)]
        public string ClmtLanguage
        {
            get { return fClmtLanguage; }
            set { SetPropertyValue<string>("ClmtLanguage", ref fClmtLanguage, value); }
        }
        bool fFastTrack;
        public bool FastTrack
        {
            get { return fFastTrack; }
            set { SetPropertyValue<bool>("FastTrack", ref fFastTrack, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fClaimantFirstName;
        [Size(50)]
        public string ClaimantFirstName
        {
            get { return fClaimantFirstName; }
            set { SetPropertyValue<string>("ClaimantFirstName", ref fClaimantFirstName, value); }
        }
        string fClaimantLastName;
        [Size(50)]
        public string ClaimantLastName
        {
            get { return fClaimantLastName; }
            set { SetPropertyValue<string>("ClaimantLastName", ref fClaimantLastName, value); }
        }
        string fClaimantMI;
        [Size(5)]
        public string ClaimantMI
        {
            get { return fClaimantMI; }
            set { SetPropertyValue<string>("ClaimantMI", ref fClaimantMI, value); }
        }
        string fPhyDamAreaDamaged;
        [Size(12)]
        public string PhyDamAreaDamaged
        {
            get { return fPhyDamAreaDamaged; }
            set { SetPropertyValue<string>("PhyDamAreaDamaged", ref fPhyDamAreaDamaged, value); }
        }
        string fPropDamAreaDamaged;
        [Size(12)]
        public string PropDamAreaDamaged
        {
            get { return fPropDamAreaDamaged; }
            set { SetPropertyValue<string>("PropDamAreaDamaged", ref fPropDamAreaDamaged, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        int fPaidAmt;
        public int PaidAmt
        {
            get { return fPaidAmt; }
            set { SetPropertyValue<int>("PaidAmt", ref fPaidAmt, value); }
        }
        int fSalvRecAmt;
        public int SalvRecAmt
        {
            get { return fSalvRecAmt; }
            set { SetPropertyValue<int>("SalvRecAmt", ref fSalvRecAmt, value); }
        }
        int fSubroAmt;
        public int SubroAmt
        {
            get { return fSubroAmt; }
            set { SetPropertyValue<int>("SubroAmt", ref fSubroAmt, value); }
        }
        bool fIsInsured;
        public bool IsInsured
        {
            get { return fIsInsured; }
            set { SetPropertyValue<bool>("IsInsured", ref fIsInsured, value); }
        }
        bool fIsCovInvestigation;
        public bool IsCovInvestigation
        {
            get { return fIsCovInvestigation; }
            set { SetPropertyValue<bool>("IsCovInvestigation", ref fIsCovInvestigation, value); }
        }
        string fWitnessesStatements;
        [Size(8000)]
        public string WitnessesStatements
        {
            get { return fWitnessesStatements; }
            set { SetPropertyValue<string>("WitnessesStatements", ref fWitnessesStatements, value); }
        }
        string fMedAttorneyTaxID;
        [Size(150)]
        public string MedAttorneyTaxID
        {
            get { return fMedAttorneyTaxID; }
            set { SetPropertyValue<string>("MedAttorneyTaxID", ref fMedAttorneyTaxID, value); }
        }
        bool fIsPropDamSubroClaim;
        public bool IsPropDamSubroClaim
        {
            get { return fIsPropDamSubroClaim; }
            set { SetPropertyValue<bool>("IsPropDamSubroClaim", ref fIsPropDamSubroClaim, value); }
        }
        bool fSuitDemandRec;
        public bool SuitDemandRec
        {
            get { return fSuitDemandRec; }
            set { SetPropertyValue<bool>("SuitDemandRec", ref fSuitDemandRec, value); }
        }
        DateTime fSuitDateRec;
        public DateTime SuitDateRec
        {
            get { return fSuitDateRec; }
            set { SetPropertyValue<DateTime>("SuitDateRec", ref fSuitDateRec, value); }
        }
        bool fIsInSuit;
        public bool IsInSuit
        {
            get { return fIsInSuit; }
            set { SetPropertyValue<bool>("IsInSuit", ref fIsInSuit, value); }
        }
        DateTime fInSuitDate;
        public DateTime InSuitDate
        {
            get { return fInSuitDate; }
            set { SetPropertyValue<DateTime>("InSuitDate", ref fInSuitDate, value); }
        }
        string fDefenseTaxID;
        public string DefenseTaxID
        {
            get { return fDefenseTaxID; }
            set { SetPropertyValue<string>("DefenseTaxID", ref fDefenseTaxID, value); }
        }
        string fInsuredRepTaxID;
        public string InsuredRepTaxID
        {
            get { return fInsuredRepTaxID; }
            set { SetPropertyValue<string>("InsuredRepTaxID", ref fInsuredRepTaxID, value); }
        }
        string fSuitNotes;
        [Size(8000)]
        public string SuitNotes
        {
            get { return fSuitNotes; }
            set { SetPropertyValue<string>("SuitNotes", ref fSuitNotes, value); }
        }
        bool fIsPreSuit;
        public bool IsPreSuit
        {
            get { return fIsPreSuit; }
            set { SetPropertyValue<bool>("IsPreSuit", ref fIsPreSuit, value); }
        }
        DateTime fPreSuitDate;
        public DateTime PreSuitDate
        {
            get { return fPreSuitDate; }
            set { SetPropertyValue<DateTime>("PreSuitDate", ref fPreSuitDate, value); }
        }
        public Claimants(Session session) : base(session) { }
        public Claimants() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyBindersDriversViolations : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public PolicyBindersDriversViolations(Session session) : base(session) { }
        public PolicyBindersDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditImageIndex : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fFileNo;
        [Size(20)]
        public string FileNo
        {
            get { return fFileNo; }
            set { SetPropertyValue<string>("FileNo", ref fFileNo, value); }
        }
        int fHistoryID;
        public int HistoryID
        {
            get { return fHistoryID; }
            set { SetPropertyValue<int>("HistoryID", ref fHistoryID, value); }
        }
        string fDept;
        [Size(25)]
        public string Dept
        {
            get { return fDept; }
            set { SetPropertyValue<string>("Dept", ref fDept, value); }
        }
        string fFormType;
        [Size(10)]
        public string FormType
        {
            get { return fFormType; }
            set { SetPropertyValue<string>("FormType", ref fFormType, value); }
        }
        string fFormDesc;
        [Size(2000)]
        public string FormDesc
        {
            get { return fFormDesc; }
            set { SetPropertyValue<string>("FormDesc", ref fFormDesc, value); }
        }
        string fFileFolder;
        public string FileFolder
        {
            get { return fFileFolder; }
            set { SetPropertyValue<string>("FileFolder", ref fFileFolder, value); }
        }
        string fFileName;
        [Size(8000)]
        public string FileName
        {
            get { return fFileName; }
            set { SetPropertyValue<string>("FileName", ref fFileName, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        bool fPrinted;
        public bool Printed
        {
            get { return fPrinted; }
            set { SetPropertyValue<bool>("Printed", ref fPrinted, value); }
        }
        string fUserName;
        [Size(25)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        DateTime fDateLastModified;
        public DateTime DateLastModified
        {
            get { return fDateLastModified; }
            set { SetPropertyValue<DateTime>("DateLastModified", ref fDateLastModified, value); }
        }
        string fThumbnail;
        [Size(8000)]
        public string Thumbnail
        {
            get { return fThumbnail; }
            set { SetPropertyValue<string>("Thumbnail", ref fThumbnail, value); }
        }
        bool fIsDiary;
        public bool IsDiary
        {
            get { return fIsDiary; }
            set { SetPropertyValue<bool>("IsDiary", ref fIsDiary, value); }
        }
        string fCategory;
        [Size(500)]
        public string Category
        {
            get { return fCategory; }
            set { SetPropertyValue<string>("Category", ref fCategory, value); }
        }
        string fPortalCode;
        [Size(150)]
        public string PortalCode
        {
            get { return fPortalCode; }
            set { SetPropertyValue<string>("PortalCode", ref fPortalCode, value); }
        }
        string fPerilDesc;
        [Size(20)]
        public string PerilDesc
        {
            get { return fPerilDesc; }
            set { SetPropertyValue<string>("PerilDesc", ref fPerilDesc, value); }
        }
        string fIndexNo;
        [Size(50)]
        public string IndexNo
        {
            get { return fIndexNo; }
            set { SetPropertyValue<string>("IndexNo", ref fIndexNo, value); }
        }
        bool fIsProcessed;
        public bool IsProcessed
        {
            get { return fIsProcessed; }
            set { SetPropertyValue<bool>("IsProcessed", ref fIsProcessed, value); }
        }

        int fCatalog;
        public int Catalog
        {
            get { return fCatalog; }
            set { SetPropertyValue<int>("Catalog", ref fCatalog, value); }
        }
        bool fIsBurnedToDisk;
        public bool IsBurnedToDisk
        {
            get { return fIsBurnedToDisk; }
            set { SetPropertyValue<bool>("IsBurnedToDisk", ref fIsBurnedToDisk, value); }
        }
        bool fIsIndexed;
        public bool IsIndexed
        {
            get { return fIsIndexed; }
            set { SetPropertyValue<bool>("IsIndexed", ref fIsIndexed, value); }
        }
        bool fFlagEmc;
        public bool FlagEmc
        {
            get { return fFlagEmc; }
            set { SetPropertyValue<bool>("FlagEmc", ref fFlagEmc, value); }
        }
        bool fFlagImage;
        public bool FlagImage
        {
            get { return fFlagImage; }
            set { SetPropertyValue<bool>("FlagImage", ref fFlagImage, value); }
        }
        DateTime fDateProcessed;
        public DateTime DateProcessed
        {
            get { return fDateProcessed; }
            set { SetPropertyValue<DateTime>("DateProcessed", ref fDateProcessed, value); }
        }
        string fAssignedTo;
        [Size(200)]
        public string AssignedTo
        {
            get { return fAssignedTo; }
            set { SetPropertyValue<string>("AssignedTo", ref fAssignedTo, value); }
        }
        string fWFClassification;
        [Size(500)]
        public string WFClassification
        {
            get { return fWFClassification; }
            set { SetPropertyValue<string>("WFClassification", ref fWFClassification, value); }
        }

        bool fIsPhysicalDamageImage;
        public bool IsPhysicalDamageImage
        {
            get { return fIsPhysicalDamageImage; }
            set { SetPropertyValue<bool>("IsPhysicalDamageImage", ref fIsPhysicalDamageImage, value); }
        }

        bool fIsNewPolicyDocument;
        public bool IsNewPolicyDocument
        {
            get { return fIsNewPolicyDocument; }
            set { SetPropertyValue<bool>("IsNewPolicyDocument", ref fIsNewPolicyDocument, value); }
        }

        public AuditImageIndex(Session session) : base(session) { }
        public AuditImageIndex() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsCheckSequence : XPLiteObject
    {
        int fCheckNo;
        [Key(true)]
        public int CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<int>("CheckNo", ref fCheckNo, value); }
        }
        string fClaimNo;
        [Size(75)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fIndexID_ClaimsChecksPending;
        public int IndexID_ClaimsChecksPending
        {
            get { return fIndexID_ClaimsChecksPending; }
            set { SetPropertyValue<int>("IndexID_ClaimsChecksPending", ref fIndexID_ClaimsChecksPending, value); }
        }
        public ClaimsCheckSequence(Session session) : base(session) { }
        public ClaimsCheckSequence() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditPolicyDrivers : XPCustomObject
    {
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(255)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public AuditPolicyDrivers(Session session) : base(session) { }
        public AuditPolicyDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyEndorseQuoteDriversViolations : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fFromMvr;
        public bool FromMVR
        {
            get { return fFromMvr; }
            set { SetPropertyValue<bool>("FromMVR", ref fFromMvr, value); }
        }

        bool fFromAPlus;
        public bool FromAPlus
        {
            get { return fFromAPlus; }
            set { SetPropertyValue<bool>("FromAPlus", ref fFromAPlus, value); }
        }

        bool fFromCV;
        public bool FromCV
        {
            get { return fFromCV; }
            set { SetPropertyValue<bool>("FromCV", ref fFromCV, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public PolicyEndorseQuoteDriversViolations(Session session) : base(session) { }
        public PolicyEndorseQuoteDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PaymentBatches : IPaymentBatches
    {
        public int IndexID { get; set; }
        public string BatchNo { get; set; }
        public string BatchType { get; set; }
        public DateTime BatchDate { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime AcctDate { get; set; }
        public decimal HashTotal { get; set; }
        public decimal CheckTotal { get; set; }
        public decimal DraftTotal { get; set; }
        public decimal TotalAmt { get; set; }
        public string UserName { get; set; }
    }

    public interface IPaymentBatches
    {
        int IndexID { get; set; }
        string BatchNo { get; set; }
        string BatchType { get; set; }
        DateTime BatchDate { get; set; }
        DateTime DateCreated { get; set; }
        DateTime AcctDate { get; set; }
        decimal HashTotal { get; set; }
        decimal CheckTotal { get; set; }
        decimal DraftTotal { get; set; }
        decimal TotalAmt { get; set; }
        string UserName { get; set; }
    }

	[Persistent("PaymentBatches")]
    public class XpoPaymentBatches : XPLiteObject, IPaymentBatches
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fBatchNo;
        [Size(25)]
        public string BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<string>("BatchNo", ref fBatchNo, value); }
        }
        string fBatchType;
        [Size(30)]
        public string BatchType
        {
            get { return fBatchType; }
            set { SetPropertyValue<string>("BatchType", ref fBatchType, value); }
        }
        DateTime fBatchDate;
        public DateTime BatchDate
        {
            get { return fBatchDate; }
            set { SetPropertyValue<DateTime>("BatchDate", ref fBatchDate, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        decimal fHashTotal;
        public decimal HashTotal
        {
            get { return fHashTotal; }
            set { SetPropertyValue<decimal>("HashTotal", ref fHashTotal, value); }
        }
        decimal fCheckTotal;
        public decimal CheckTotal
        {
            get { return fCheckTotal; }
            set { SetPropertyValue<decimal>("CheckTotal", ref fCheckTotal, value); }
        }
        decimal fDraftTotal;
        public decimal DraftTotal
        {
            get { return fDraftTotal; }
            set { SetPropertyValue<decimal>("DraftTotal", ref fDraftTotal, value); }
        }
        decimal fTotalAmt;
        public decimal TotalAmt
        {
            get { return fTotalAmt; }
            set { SetPropertyValue<decimal>("TotalAmt", ref fTotalAmt, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        public XpoPaymentBatches(Session session) : base(session) { }
        public XpoPaymentBatches() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyDOLDriversViolations : XPCustomObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fDateTimeCreated;
        public DateTime DateTimeCreated
        {
            get { return fDateTimeCreated; }
            set { SetPropertyValue<DateTime>("DateTimeCreated", ref fDateTimeCreated, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        public PolicyDOLDriversViolations(Session session) : base(session) { }
        public PolicyDOLDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimNotes : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fClaimNo;
        [Size(60)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fNoteIndexID;
        public int NoteIndexID
        {
            get { return fNoteIndexID; }
            set { SetPropertyValue<int>("NoteIndexID", ref fNoteIndexID, value); }
        }
        string fNotes;
        [Size(8000)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fUsername;
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        DateTime fDateModified;
        public DateTime DateModified
        {
            get { return fDateModified; }
            set { SetPropertyValue<DateTime>("DateModified", ref fDateModified, value); }
        }
        bool fIsDiary;
        public bool IsDiary
        {
            get { return fIsDiary; }
            set { SetPropertyValue<bool>("IsDiary", ref fIsDiary, value); }
        }
        bool fInternalOnly;
        public bool InternalOnly
        {
            get { return fInternalOnly; }
            set { SetPropertyValue<bool>("InternalOnly", ref fInternalOnly, value); }
        }
        public ClaimNotes(Session session) : base(session) { }
        public ClaimNotes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Providers : XPCustomObject
    {
        string fFEIN;
        public string FEIN
        {
            get { return fFEIN; }
            set { SetPropertyValue<string>("FEIN", ref fFEIN, value); }
        }
        int fType;
        public int Type
        {
            get { return fType; }
            set { SetPropertyValue<int>("Type", ref fType, value); }
        }
        string fInternalCode;
        public string InternalCode
        {
            get { return fInternalCode; }
            set { SetPropertyValue<string>("InternalCode", ref fInternalCode, value); }
        }
        string fName;
        [Size(150)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        string fPrimaryFirstName;
        [Size(150)]
        public string PrimaryFirstName
        {
            get { return fPrimaryFirstName; }
            set { SetPropertyValue<string>("PrimaryFirstName", ref fPrimaryFirstName, value); }
        }
        string fPrimaryLastname;
        [Size(150)]
        public string PrimaryLastname
        {
            get { return fPrimaryLastname; }
            set { SetPropertyValue<string>("PrimaryLastname", ref fPrimaryLastname, value); }
        }
        string fDBA;
        [Size(200)]
        public string DBA
        {
            get { return fDBA; }
            set { SetPropertyValue<string>("DBA", ref fDBA, value); }
        }
        string fMailAddress;
        [Size(150)]
        public string MailAddress
        {
            get { return fMailAddress; }
            set { SetPropertyValue<string>("MailAddress", ref fMailAddress, value); }
        }
        string fMailCity;
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZIP;
        [Size(20)]
        public string MailZIP
        {
            get { return fMailZIP; }
            set { SetPropertyValue<string>("MailZIP", ref fMailZIP, value); }
        }
        string fPhysicalAddress;
        [Size(150)]
        public string PhysicalAddress
        {
            get { return fPhysicalAddress; }
            set { SetPropertyValue<string>("PhysicalAddress", ref fPhysicalAddress, value); }
        }
        string fPhysicalCity;
        public string PhysicalCity
        {
            get { return fPhysicalCity; }
            set { SetPropertyValue<string>("PhysicalCity", ref fPhysicalCity, value); }
        }
        string fPhysicalState;
        [Size(2)]
        public string PhysicalState
        {
            get { return fPhysicalState; }
            set { SetPropertyValue<string>("PhysicalState", ref fPhysicalState, value); }
        }
        string fPhysicalZIP;
        [Size(20)]
        public string PhysicalZIP
        {
            get { return fPhysicalZIP; }
            set { SetPropertyValue<string>("PhysicalZIP", ref fPhysicalZIP, value); }
        }
        string fCounty;
        [Size(50)]
        public string County
        {
            get { return fCounty; }
            set { SetPropertyValue<string>("County", ref fCounty, value); }
        }
        bool fIsPGLLC;
        public bool IsPGLLC
        {
            get { return fIsPGLLC; }
            set { SetPropertyValue<bool>("IsPGLLC", ref fIsPGLLC, value); }
        }
        string fPhone;
        [Size(20)]
        public string Phone
        {
            get { return fPhone; }
            set { SetPropertyValue<string>("Phone", ref fPhone, value); }
        }
        string fFax;
        [Size(20)]
        public string Fax
        {
            get { return fFax; }
            set { SetPropertyValue<string>("Fax", ref fFax, value); }
        }
        string fEmailAddress;
        public string EmailAddress
        {
            get { return fEmailAddress; }
            set { SetPropertyValue<string>("EmailAddress", ref fEmailAddress, value); }
        }
        bool fIsW9Required;
        public bool IsW9Required
        {
            get { return fIsW9Required; }
            set { SetPropertyValue<bool>("IsW9Required", ref fIsW9Required, value); }
        }
        bool fIsW9Received;
        public bool IsW9Received
        {
            get { return fIsW9Received; }
            set { SetPropertyValue<bool>("IsW9Received", ref fIsW9Received, value); }
        }
        string fStatus;
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        DateTime fDateReceived;
        public DateTime DateReceived
        {
            get { return fDateReceived; }
            set { SetPropertyValue<DateTime>("DateReceived", ref fDateReceived, value); }
        }
        string fProviderType;
        public string ProviderType
        {
            get { return fProviderType; }
            set { SetPropertyValue<string>("ProviderType", ref fProviderType, value); }
        }
        bool fIsInvestigated;
        public bool IsInvestigated
        {
            get { return fIsInvestigated; }
            set { SetPropertyValue<bool>("IsInvestigated", ref fIsInvestigated, value); }
        }
        bool fIsFlagClaimants;
        public bool IsFlagClaimants
        {
            get { return fIsFlagClaimants; }
            set { SetPropertyValue<bool>("IsFlagClaimants", ref fIsFlagClaimants, value); }
        }
        bool fIsMDOnPremises;
        public bool IsMDOnPremises
        {
            get { return fIsMDOnPremises; }
            set { SetPropertyValue<bool>("IsMDOnPremises", ref fIsMDOnPremises, value); }
        }
        bool fIsRestrictedTreatments;
        public bool IsRestrictedTreatments
        {
            get { return fIsRestrictedTreatments; }
            set { SetPropertyValue<bool>("IsRestrictedTreatments", ref fIsRestrictedTreatments, value); }
        }
        bool fIsMandatoryIME;
        public bool IsMandatoryIME
        {
            get { return fIsMandatoryIME; }
            set { SetPropertyValue<bool>("IsMandatoryIME", ref fIsMandatoryIME, value); }
        }
        string fNotifications;
        [Size(8000)]
        public string Notifications
        {
            get { return fNotifications; }
            set { SetPropertyValue<string>("Notifications", ref fNotifications, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fSameAddress;
        public bool SameAddress
        {
            get { return fSameAddress; }
            set { SetPropertyValue<bool>("SameAddress", ref fSameAddress, value); }
        }
        public Providers(Session session) : base(session) { }
        public Providers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ICD9Codes : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fCode;
        [Size(255)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>("Code", ref fCode, value); }
        }
        string fShortDesc;
        [Size(255)]
        public string ShortDesc
        {
            get { return fShortDesc; }
            set { SetPropertyValue<string>("ShortDesc", ref fShortDesc, value); }
        }
        string fMedDesc;
        [Size(255)]
        public string MedDesc
        {
            get { return fMedDesc; }
            set { SetPropertyValue<string>("MedDesc", ref fMedDesc, value); }
        }
        string fLongDesc;
        [Size(255)]
        public string LongDesc
        {
            get { return fLongDesc; }
            set { SetPropertyValue<string>("LongDesc", ref fLongDesc, value); }
        }
        public ICD9Codes(Session session) : base(session) { }
        public ICD9Codes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditPolicyHistoryNotes : XPLiteObject
    {
        int fIndexID;
        [Key]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fNotes;
        [Size(SizeAttribute.Unlimited)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        public AuditPolicyHistoryNotes(Session session) : base(session) { }
        public AuditPolicyHistoryNotes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class SystemVars : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fName;
        [Size(500)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        string fValue;
        [Size(8000)]
        public string Value
        {
            get { return fValue; }
            set { SetPropertyValue<string>("Value", ref fValue, value); }
        }
        public SystemVars(Session session) : base(session) { }
        public SystemVars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditAgentCommission : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fAction;
        [Size(25)]
        public string Action
        {
            get { return fAction; }
            set { SetPropertyValue<string>("Action", ref fAction, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        DateTime fProcessDate;
        public DateTime ProcessDate
        {
            get { return fProcessDate; }
            set { SetPropertyValue<DateTime>("ProcessDate", ref fProcessDate, value); }
        }
        string fUserName;
        [Size(25)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        double fPolicyPremium;
        public double PolicyPremium
        {
            get { return fPolicyPremium; }
            set { SetPropertyValue<double>("PolicyPremium", ref fPolicyPremium, value); }
        }
        double fCommRate;
        public double CommRate
        {
            get { return fCommRate; }
            set { SetPropertyValue<double>("CommRate", ref fCommRate, value); }
        }
        double fCommEarned;
        public double CommEarned
        {
            get { return fCommEarned; }
            set { SetPropertyValue<double>("CommEarned", ref fCommEarned, value); }
        }
        bool fAgentPaid;
        public bool AgentPaid
        {
            get { return fAgentPaid; }
            set { SetPropertyValue<bool>("AgentPaid", ref fAgentPaid, value); }
        }
        double fCommPaidAmtCurrent;
        public double CommPaidAmtCurrent
        {
            get { return fCommPaidAmtCurrent; }
            set { SetPropertyValue<double>("CommPaidAmtCurrent", ref fCommPaidAmtCurrent, value); }
        }
        double fCommPaidAmtPrior;
        public double CommPaidAmtPrior
        {
            get { return fCommPaidAmtPrior; }
            set { SetPropertyValue<double>("CommPaidAmtPrior", ref fCommPaidAmtPrior, value); }
        }
        DateTime fStatementDate;
        public DateTime StatementDate
        {
            get { return fStatementDate; }
            set { SetPropertyValue<DateTime>("StatementDate", ref fStatementDate, value); }
        }
        double fStatementBalanceCurrent;
        public double StatementBalanceCurrent
        {
            get { return fStatementBalanceCurrent; }
            set { SetPropertyValue<double>("StatementBalanceCurrent", ref fStatementBalanceCurrent, value); }
        }
        double fStatementBalancePrior;
        public double StatementBalancePrior
        {
            get { return fStatementBalancePrior; }
            set { SetPropertyValue<double>("StatementBalancePrior", ref fStatementBalancePrior, value); }
        }
        public AuditAgentCommission(Session session) : base(session) { }
        public AuditAgentCommission() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyDOL : XPCustomObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fDateTimeCreated;
        public DateTime DateTimeCreated
        {
            get { return fDateTimeCreated; }
            set { SetPropertyValue<DateTime>("DateTimeCreated", ref fDateTimeCreated, value); }
        }
        string fPriorPolicyNo;
        [Size(15)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        [Size(5)]
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fpolicyCars;
        public int policyCars
        {
            get { return fpolicyCars; }
            set { SetPropertyValue<int>("policyCars", ref fpolicyCars, value); }
        }
        int fpolicyDrivers;
        public int policyDrivers
        {
            get { return fpolicyDrivers; }
            set { SetPropertyValue<int>("policyDrivers", ref fpolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        bool fHasPIP;
        public bool HasPIP
        {
            get { return fHasPIP; }
            set { SetPropertyValue<bool>("HasPIP", ref fHasPIP, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        public PolicyDOL(Session session) : base(session) { }
        public PolicyDOL() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class UserRights : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        int fUserID;
        public int UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<int>("UserID", ref fUserID, value); }
        }
        string fRights;
        [Size(8000)]
        public string Rights
        {
            get { return fRights; }
            set { SetPropertyValue<string>("Rights", ref fRights, value); }
        }
        public UserRights(Session session) : base(session) { }
        public UserRights() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsPIPBills : XPLiteObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fBillKey;
        [Size(20)]
        public string BillKey
        {
            get { return fBillKey; }
            set { SetPropertyValue<string>("BillKey", ref fBillKey, value); }
        }
        string fInvoiceNo;
        [Size(20)]
        public string InvoiceNo
        {
            get { return fInvoiceNo; }
            set { SetPropertyValue<string>("InvoiceNo", ref fInvoiceNo, value); }
        }
        DateTime fDateRec;
        public DateTime DateRec
        {
            get { return fDateRec; }
            set { SetPropertyValue<DateTime>("DateRec", ref fDateRec, value); }
        }
        string fClmPayKey;
        [Size(20)]
        public string ClmPayKey
        {
            get { return fClmPayKey; }
            set { SetPropertyValue<string>("ClmPayKey", ref fClmPayKey, value); }
        }
        DateTime fDatePaid;
        public DateTime DatePaid
        {
            get { return fDatePaid; }
            set { SetPropertyValue<DateTime>("DatePaid", ref fDatePaid, value); }
        }
        string fProviderID;
        public string ProviderID
        {
            get { return fProviderID; }
            set { SetPropertyValue<string>("ProviderID", ref fProviderID, value); }
        }
        string fProviderName;
        [Size(250)]
        public string ProviderName
        {
            get { return fProviderName; }
            set { SetPropertyValue<string>("ProviderName", ref fProviderName, value); }
        }
        DateTime fDateSvrcFrom;
        public DateTime DateSvrcFrom
        {
            get { return fDateSvrcFrom; }
            set { SetPropertyValue<DateTime>("DateSvrcFrom", ref fDateSvrcFrom, value); }
        }
        DateTime fDateSrvcTo;
        public DateTime DateSrvcTo
        {
            get { return fDateSrvcTo; }
            set { SetPropertyValue<DateTime>("DateSrvcTo", ref fDateSrvcTo, value); }
        }
        string fBillDesc;
        [Size(42)]
        public string BillDesc
        {
            get { return fBillDesc; }
            set { SetPropertyValue<string>("BillDesc", ref fBillDesc, value); }
        }
        string fSubCode;
        [Size(30)]
        public string SubCode
        {
            get { return fSubCode; }
            set { SetPropertyValue<string>("SubCode", ref fSubCode, value); }
        }
        string fStatus;
        [Size(10)]
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        double fAmtBilled;
        public double AmtBilled
        {
            get { return fAmtBilled; }
            set { SetPropertyValue<double>("AmtBilled", ref fAmtBilled, value); }
        }
        double fAmtAllowed;
        public double AmtAllowed
        {
            get { return fAmtAllowed; }
            set { SetPropertyValue<double>("AmtAllowed", ref fAmtAllowed, value); }
        }
        int fCoPay;
        public int CoPay
        {
            get { return fCoPay; }
            set { SetPropertyValue<int>("CoPay", ref fCoPay, value); }
        }
        double fAmtResult;
        public double AmtResult
        {
            get { return fAmtResult; }
            set { SetPropertyValue<double>("AmtResult", ref fAmtResult, value); }
        }
        double fDeductRemaining;
        public double DeductRemaining
        {
            get { return fDeductRemaining; }
            set { SetPropertyValue<double>("DeductRemaining", ref fDeductRemaining, value); }
        }
        double fDeductApplied;
        public double DeductApplied
        {
            get { return fDeductApplied; }
            set { SetPropertyValue<double>("DeductApplied", ref fDeductApplied, value); }
        }
        bool fIncludeInterest;
        public bool IncludeInterest
        {
            get { return fIncludeInterest; }
            set { SetPropertyValue<bool>("IncludeInterest", ref fIncludeInterest, value); }
        }
        double fIntFee;
        public double IntFee
        {
            get { return fIntFee; }
            set { SetPropertyValue<double>("IntFee", ref fIntFee, value); }
        }
        double fAttyFee;
        public double AttyFee
        {
            get { return fAttyFee; }
            set { SetPropertyValue<double>("AttyFee", ref fAttyFee, value); }
        }
        double fAmtPaid;
        public double AmtPaid
        {
            get { return fAmtPaid; }
            set { SetPropertyValue<double>("AmtPaid", ref fAmtPaid, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        bool fVoided;
        public bool Voided
        {
            get { return fVoided; }
            set { SetPropertyValue<bool>("Voided", ref fVoided, value); }
        }
        DateTime fDateVoided;
        public DateTime DateVoided
        {
            get { return fDateVoided; }
            set { SetPropertyValue<DateTime>("DateVoided", ref fDateVoided, value); }
        }
        string fPALMSInvNo;
        [Size(25)]
        public string PALMSInvNo
        {
            get { return fPALMSInvNo; }
            set { SetPropertyValue<string>("PALMSInvNo", ref fPALMSInvNo, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPatientID;
        [Size(30)]
        public string PatientID
        {
            get { return fPatientID; }
            set { SetPropertyValue<string>("PatientID", ref fPatientID, value); }
        }
        public ClaimsPIPBills(Session session) : base(session) { }
        public ClaimsPIPBills() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PaymentsChecks : IPaymentsChecks
    {
        public int IndexID { get; set; }
        public string BatchID { get; set; }
        public string BatchNo { get; set; }
        public string PolicyNo { get; set; }
        public string PaymentType { get; set; }
        public string CheckNo { get; set; }
        public string SerialNo { get; set; }
        public string Code { get; set; }
        public DateTime AcctDate { get; set; }
        public string Payee { get; set; }
        public string PayeeStreet { get; set; }
        public string PayeeCityStZip { get; set; }
        public string UserName { get; set; }
        public double CheckAmt { get; set; }
        public bool IsCleared { get; set; }
        public DateTime DateCleared { get; set; }
        public bool IsVoided { get; set; }
        public DateTime DateVoided { get; set; }
        public string ReplaceOf { get; set; }
        public bool ReportedToBank { get; set; }
        public DateTime ReportedToBankDate { get; set; }
        public bool ReportVoidToBank { get; set; }
        public DateTime CreateDate { get; set; }
    }

    public interface IPaymentsChecks
    {
        int IndexID { get; set; }
        string BatchID { get; set; }
        string BatchNo { get; set; }
        string PolicyNo { get; set; }
        string PaymentType { get; set; }
        string CheckNo { get; set; }
        string SerialNo { get; set; }
        string Code { get; set; }
        DateTime AcctDate { get; set; }
        string Payee { get; set; }
        string PayeeStreet { get; set; }
        string PayeeCityStZip { get; set; }
        string UserName { get; set; }
        double CheckAmt { get; set; }
        bool IsCleared { get; set; }
        DateTime DateCleared { get; set; }
        bool IsVoided { get; set; }
        DateTime DateVoided { get; set; }
        string ReplaceOf { get; set; }
        bool ReportedToBank { get; set; }
        DateTime ReportedToBankDate { get; set; }
        bool ReportVoidToBank { get; set; }
        DateTime CreateDate { get; set; }
    }

	[Persistent("PaymentsChecks")]
    public class XpoPaymentsChecks : XPLiteObject, IPaymentsChecks
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fBatchID;
        [Size(50)]
        public string BatchID
        {
            get { return fBatchID; }
            set { SetPropertyValue<string>("BatchID", ref fBatchID, value); }
        }
        string fBatchNo;
        [Size(50)]
        public string BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<string>("BatchNo", ref fBatchNo, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPaymentType;
        [Size(10)]
        public string PaymentType
        {
            get { return fPaymentType; }
            set { SetPropertyValue<string>("PaymentType", ref fPaymentType, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        string fSerialNo;
        [Size(10)]
        public string SerialNo
        {
            get { return fSerialNo; }
            set { SetPropertyValue<string>("SerialNo", ref fSerialNo, value); }
        }
        string fCode;
        [Size(3)]
        public string Code
        {
            get { return fCode; }
            set { SetPropertyValue<string>("Code", ref fCode, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        string fPayee;
        [Size(255)]
        public string Payee
        {
            get { return fPayee; }
            set { SetPropertyValue<string>("Payee", ref fPayee, value); }
        }
        string fPayeeStreet;
        public string PayeeStreet
        {
            get { return fPayeeStreet; }
            set { SetPropertyValue<string>("PayeeStreet", ref fPayeeStreet, value); }
        }
        string fPayeeCityStZip;
        public string PayeeCityStZip
        {
            get { return fPayeeCityStZip; }
            set { SetPropertyValue<string>("PayeeCityStZip", ref fPayeeCityStZip, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        double fCheckAmt;
        public double CheckAmt
        {
            get { return fCheckAmt; }
            set { SetPropertyValue<double>("CheckAmt", ref fCheckAmt, value); }
        }
        bool fIsCleared;
        public bool IsCleared
        {
            get { return fIsCleared; }
            set { SetPropertyValue<bool>("IsCleared", ref fIsCleared, value); }
        }
        DateTime fDateCleared;
        public DateTime DateCleared
        {
            get { return fDateCleared; }
            set { SetPropertyValue<DateTime>("DateCleared", ref fDateCleared, value); }
        }
        bool fIsVoided;
        public bool IsVoided
        {
            get { return fIsVoided; }
            set { SetPropertyValue<bool>("IsVoided", ref fIsVoided, value); }
        }
        DateTime fDateVoided;
        public DateTime DateVoided
        {
            get { return fDateVoided; }
            set { SetPropertyValue<DateTime>("DateVoided", ref fDateVoided, value); }
        }
        string fReplaceOf;
        [Size(8)]
        public string ReplaceOf
        {
            get { return fReplaceOf; }
            set { SetPropertyValue<string>("ReplaceOf", ref fReplaceOf, value); }
        }
        bool fReportedToBank;
        public bool ReportedToBank
        {
            get { return fReportedToBank; }
            set { SetPropertyValue<bool>("ReportedToBank", ref fReportedToBank, value); }
        }
        DateTime fReportedToBankDate;
        public DateTime ReportedToBankDate
        {
            get { return fReportedToBankDate; }
            set { SetPropertyValue<DateTime>("ReportedToBankDate", ref fReportedToBankDate, value); }
        }
        bool fReportVoidToBank;
        public bool ReportVoidToBank
        {
            get { return fReportVoidToBank; }
            set { SetPropertyValue<bool>("ReportVoidToBank", ref fReportVoidToBank, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        public XpoPaymentsChecks(Session session) : base(session) { }
        public XpoPaymentsChecks() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsOnlyCars : XPLiteObject
    {
        string fPolicyNo;
        [Size(75)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fVIN;
        [Size(75)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(75)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel;
        [Size(75)]
        public string VehModel
        {
            get { return fVehModel; }
            set { SetPropertyValue<string>("VehModel", ref fVehModel, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        public ClaimsOnlyCars(Session session) : base(session) { }
        public ClaimsOnlyCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditUserActivity : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fActivityType;
        public string ActivityType
        {
            get { return fActivityType; }
            set { SetPropertyValue<string>("ActivityType", ref fActivityType, value); }
        }
        DateTime fActivityDate;
        public DateTime ActivityDate
        {
            get { return fActivityDate; }
            set { SetPropertyValue<DateTime>("ActivityDate", ref fActivityDate, value); }
        }
        public AuditUserActivity(Session session) : base(session) { }
        public AuditUserActivity() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class ClaimsPIPLog : XPLiteObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        double fDeductible;
        public double Deductible
        {
            get { return fDeductible; }
            set { SetPropertyValue<double>("Deductible", ref fDeductible, value); }
        }
        double fDeductRemaining;
        public double DeductRemaining
        {
            get { return fDeductRemaining; }
            set { SetPropertyValue<double>("DeductRemaining", ref fDeductRemaining, value); }
        }
        bool fPIPApp;
        public bool PIPApp
        {
            get { return fPIPApp; }
            set { SetPropertyValue<bool>("PIPApp", ref fPIPApp, value); }
        }
        bool fSwornAff;
        public bool SwornAff
        {
            get { return fSwornAff; }
            set { SetPropertyValue<bool>("SwornAff", ref fSwornAff, value); }
        }
        bool fWageVerif;
        public bool WageVerif
        {
            get { return fWageVerif; }
            set { SetPropertyValue<bool>("WageVerif", ref fWageVerif, value); }
        }
        bool fCrimeVic;
        public bool CrimeVic
        {
            get { return fCrimeVic; }
            set { SetPropertyValue<bool>("CrimeVic", ref fCrimeVic, value); }
        }
        bool fWageOnly;
        public bool WageOnly
        {
            get { return fWageOnly; }
            set { SetPropertyValue<bool>("WageOnly", ref fWageOnly, value); }
        }
        bool fResRelative;
        public bool ResRelative
        {
            get { return fResRelative; }
            set { SetPropertyValue<bool>("ResRelative", ref fResRelative, value); }
        }
        string fAttyID;
        [Size(20)]
        public string AttyID
        {
            get { return fAttyID; }
            set { SetPropertyValue<string>("AttyID", ref fAttyID, value); }
        }
        double fInterestRate;
        public double InterestRate
        {
            get { return fInterestRate; }
            set { SetPropertyValue<double>("InterestRate", ref fInterestRate, value); }
        }
        string fLastTrans;
        [Size(15)]
        public string LastTrans
        {
            get { return fLastTrans; }
            set { SetPropertyValue<string>("LastTrans", ref fLastTrans, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fMode;
        public string Mode
        {
            get { return fMode; }
            set { SetPropertyValue<string>("Mode", ref fMode, value); }
        }
        public ClaimsPIPLog(Session session) : base(session) { }
        public ClaimsPIPLog() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class RatingDrivers : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fMainIndexID;
        public int MainIndexID
        {
            get { return fMainIndexID; }
            set { SetPropertyValue<int>("MainIndexID", ref fMainIndexID, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(20)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriversAge;
        public int DriversAge
        {
            get { return fDriversAge; }
            set { SetPropertyValue<int>("DriversAge", ref fDriversAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        double fDriverRateValue;
        public double DriverRateValue
        {
            get { return fDriverRateValue; }
            set { SetPropertyValue<double>("DriverRateValue", ref fDriverRateValue, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        int fCountMAJDateRange1;
        public int CountMAJDateRange1
        {
            get { return fCountMAJDateRange1; }
            set { SetPropertyValue<int>("CountMAJDateRange1", ref fCountMAJDateRange1, value); }
        }
        int fCountMajDateRange2;
        public int CountMajDateRange2
        {
            get { return fCountMajDateRange2; }
            set { SetPropertyValue<int>("CountMajDateRange2", ref fCountMajDateRange2, value); }
        }
        int fCountMajDateRange3;
        public int CountMajDateRange3
        {
            get { return fCountMajDateRange3; }
            set { SetPropertyValue<int>("CountMajDateRange3", ref fCountMajDateRange3, value); }
        }
        int fCountDUIDateRange1;
        public int CountDUIDateRange1
        {
            get { return fCountDUIDateRange1; }
            set { SetPropertyValue<int>("CountDUIDateRange1", ref fCountDUIDateRange1, value); }
        }
        int fCountDUIDateRange2;
        public int CountDUIDateRange2
        {
            get { return fCountDUIDateRange2; }
            set { SetPropertyValue<int>("CountDUIDateRange2", ref fCountDUIDateRange2, value); }
        }
        int fCountDUIDateRange3;
        public int CountDUIDateRange3
        {
            get { return fCountDUIDateRange3; }
            set { SetPropertyValue<int>("CountDUIDateRange3", ref fCountDUIDateRange3, value); }
        }
        bool fAssigned;
        public bool Assigned
        {
            get { return fAssigned; }
            set { SetPropertyValue<bool>("Assigned", ref fAssigned, value); }
        }
        string fQQViolCode;
        [Size(5000)]
        public string QQViolCode
        {
            get { return fQQViolCode; }
            set { SetPropertyValue<string>("QQViolCode", ref fQQViolCode, value); }
        }
        bool fChargeMVR;
        public bool ChargeMVR
        {
            get { return fChargeMVR; }
            set { SetPropertyValue<bool>("ChargeMVR", ref fChargeMVR, value); }
        }
        [NonPersistent]
        public string PolicyNo;
        [NonPersistent]
        public bool Deleted;
        [NonPersistent]
        public string FirstName;
        [NonPersistent]
        public string LastName;
        [NonPersistent]
        public string MI;
        [NonPersistent]
        public string Suffix;
        [NonPersistent]
        public string JobTitle;
        [NonPersistent]
        public string Company;
        [NonPersistent]
        public string CompanyAddress;
        [NonPersistent]
        public string CompanyCity;
        [NonPersistent]
        public string CompanyState;
        [NonPersistent]
        public string CompanyZip;
        [NonPersistent]
        public string WorkPhone;       
        

        public RatingDrivers(Session session) : base(session) { }
        public RatingDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AgentAppointments : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fName;
        [Size(200)]
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        string fLicenseNum;
        public string LicenseNum
        {
            get { return fLicenseNum; }
            set { SetPropertyValue<string>("LicenseNum", ref fLicenseNum, value); }
        }
        DateTime fDateRegistered;
        public DateTime DateRegistered
        {
            get { return fDateRegistered; }
            set { SetPropertyValue<DateTime>("DateRegistered", ref fDateRegistered, value); }
        }
        string fStatus;
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        string fAgentCode;
        [Size(20)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        public AgentAppointments(Session session) : base(session) { }
        public AgentAppointments() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditDiary : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fSetFor;
        [Size(30)]
        public string SetFor
        {
            get { return fSetFor; }
            set { SetPropertyValue<string>("SetFor", ref fSetFor, value); }
        }
        string fSetBy;
        [Size(30)]
        public string SetBy
        {
            get { return fSetBy; }
            set { SetPropertyValue<string>("SetBy", ref fSetBy, value); }
        }
        DateTime fRemindDate;
        public DateTime RemindDate
        {
            get { return fRemindDate; }
            set { SetPropertyValue<DateTime>("RemindDate", ref fRemindDate, value); }
        }
        DateTime fEntryDate;
        public DateTime EntryDate
        {
            get { return fEntryDate; }
            set { SetPropertyValue<DateTime>("EntryDate", ref fEntryDate, value); }
        }
        string fRemindType;
        [Size(25)]
        public string RemindType
        {
            get { return fRemindType; }
            set { SetPropertyValue<string>("RemindType", ref fRemindType, value); }
        }
        string fRemindID;
        [Size(50)]
        public string RemindID
        {
            get { return fRemindID; }
            set { SetPropertyValue<string>("RemindID", ref fRemindID, value); }
        }
        string fRemindText;
        [Size(8000)]
        public string RemindText
        {
            get { return fRemindText; }
            set { SetPropertyValue<string>("RemindText", ref fRemindText, value); }
        }
        bool fCleared;
        public bool Cleared
        {
            get { return fCleared; }
            set { SetPropertyValue<bool>("Cleared", ref fCleared, value); }
        }
        DateTime fClearDate;
        public DateTime ClearDate
        {
            get { return fClearDate; }
            set { SetPropertyValue<DateTime>("ClearDate", ref fClearDate, value); }
        }
        bool fViewed;
        public bool Viewed
        {
            get { return fViewed; }
            set { SetPropertyValue<bool>("Viewed", ref fViewed, value); }
        }
        int fBatchNo;
        public int BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<int>("BatchNo", ref fBatchNo, value); }
        }
        public AuditDiary(Session session) : base(session) { }
        public AuditDiary() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Users : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fUserID;
        [Size(250)]
        public string UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<string>("UserID", ref fUserID, value); }
        }
        string fFirstName;
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fTitle;
        public string Title
        {
            get { return fTitle; }
            set { SetPropertyValue<string>("Title", ref fTitle, value); }
        }
        string fPhone;
        [Size(15)]
        public string Phone
        {
            get { return fPhone; }
            set { SetPropertyValue<string>("Phone", ref fPhone, value); }
        }
        int fExtension;
        public int Extension
        {
            get { return fExtension; }
            set { SetPropertyValue<int>("Extension", ref fExtension, value); }
        }
        string fEmailAddress;
        [Size(250)]
        public string EmailAddress
        {
            get { return fEmailAddress; }
            set { SetPropertyValue<string>("EmailAddress", ref fEmailAddress, value); }
        }
        int fStatus;
        public int Status
        {
            get { return fStatus; }
            set { SetPropertyValue<int>("Status", ref fStatus, value); }
        }
        bool fIsAdjuster;
        public bool IsAdjuster
        {
            get { return fIsAdjuster; }
            set { SetPropertyValue<bool>("IsAdjuster", ref fIsAdjuster, value); }
        }
        bool fIsCatchAll;
        public bool IsCatchAll
        {
            get { return fIsCatchAll; }
            set { SetPropertyValue<bool>("IsCatchAll", ref fIsCatchAll, value); }
        }
        string fDepartment;
        public string Department
        {
            get { return fDepartment; }
            set { SetPropertyValue<string>("Department", ref fDepartment, value); }
        }
        string fCompany;
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        public Users(Session session) : base(session) { }
        public Users() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class PolicyDOLCars : XPCustomObject
    {
        string fClaimNo;
        [Size(50)]
        public string ClaimNo
        {
            get { return fClaimNo; }
            set { SetPropertyValue<string>("ClaimNo", ref fClaimNo, value); }
        }
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fDOL;
        public DateTime DOL
        {
            get { return fDOL; }
            set { SetPropertyValue<DateTime>("DOL", ref fDOL, value); }
        }
        DateTime fDateTimeCreated;
        public DateTime DateTimeCreated
        {
            get { return fDateTimeCreated; }
            set { SetPropertyValue<DateTime>("DateTimeCreated", ref fDateTimeCreated, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        public PolicyDOLCars(Session session) : base(session) { }
        public PolicyDOLCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class RightsMask : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        int fUserID;
        public int UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<int>("UserID", ref fUserID, value); }
        }
        string fRights;
        [Size(8000)]
        public string Rights
        {
            get { return fRights; }
            set { SetPropertyValue<string>("Rights", ref fRights, value); }
        }
        public RightsMask(Session session) : base(session) { }
        public RightsMask() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class Diary : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        [Size(50)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        int fStatus;
        public int Status
        {
            get { return fStatus; }
            set { SetPropertyValue<int>("Status", ref fStatus, value); }
        }
        string fSubject;
        [Size(2000)]
        public string Subject
        {
            get { return fSubject; }
            set { SetPropertyValue<string>("Subject", ref fSubject, value); }
        }
        string fDescription;
        [Size(8000)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }
        int fLabel;
        public int Label
        {
            get { return fLabel; }
            set { SetPropertyValue<int>("Label", ref fLabel, value); }
        }
        DateTime fStartTime;
        public DateTime StartTime
        {
            get { return fStartTime; }
            set { SetPropertyValue<DateTime>("StartTime", ref fStartTime, value); }
        }
        DateTime fEndTime;
        public DateTime EndTime
        {
            get { return fEndTime; }
            set { SetPropertyValue<DateTime>("EndTime", ref fEndTime, value); }
        }
        string fLocation;
        [Size(2000)]
        public string Location
        {
            get { return fLocation; }
            set { SetPropertyValue<string>("Location", ref fLocation, value); }
        }
        bool fAllDay;
        public bool AllDay
        {
            get { return fAllDay; }
            set { SetPropertyValue<bool>("AllDay", ref fAllDay, value); }
        }
        int fEventType;
        public int EventType
        {
            get { return fEventType; }
            set { SetPropertyValue<int>("EventType", ref fEventType, value); }
        }
        string fRecurrenceInfo;
        [Size(2000)]
        public string RecurrenceInfo
        {
            get { return fRecurrenceInfo; }
            set { SetPropertyValue<string>("RecurrenceInfo", ref fRecurrenceInfo, value); }
        }
        string fReminderInfo;
        [Size(2000)]
        public string ReminderInfo
        {
            get { return fReminderInfo; }
            set { SetPropertyValue<string>("ReminderInfo", ref fReminderInfo, value); }
        }
        string fContactInfo;
        [Size(2000)]
        public string ContactInfo
        {
            get { return fContactInfo; }
            set { SetPropertyValue<string>("ContactInfo", ref fContactInfo, value); }
        }
        string fAddedFrom;
        [Size(200)]
        public string AddedFrom
        {
            get { return fAddedFrom; }
            set { SetPropertyValue<string>("AddedFrom", ref fAddedFrom, value); }
        }
        string fSetBy;
        public string SetBy
        {
            get { return fSetBy; }
            set { SetPropertyValue<string>("SetBy", ref fSetBy, value); }
        }
        string fNo;
        [Size(10)]
        public string No
        {
            get { return fNo; }
            set { SetPropertyValue<string>("No", ref fNo, value); }
        }
        int fBatchNo;
        public int BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<int>("BatchNo", ref fBatchNo, value); }
        }
        public Diary(Session session) : base(session) { }
        public Diary() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class System_PIPBillsDesc : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fPercentage;
        [Size(10)]
        public string Percentage
        {
            get { return fPercentage; }
            set { SetPropertyValue<string>("Percentage", ref fPercentage, value); }
        }
        string fDescription;
        [Size(50)]
        public string Description
        {
            get { return fDescription; }
            set { SetPropertyValue<string>("Description", ref fDescription, value); }
        }
        public System_PIPBillsDesc(Session session) : base(session) { }
        public System_PIPBillsDesc() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditAgentCommissionActivity : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fHistoryIndexID;
        public int HistoryIndexID
        {
            get { return fHistoryIndexID; }
            set { SetPropertyValue<int>("HistoryIndexID", ref fHistoryIndexID, value); }
        }
        string fAction;
        [Size(25)]
        public string Action
        {
            get { return fAction; }
            set { SetPropertyValue<string>("Action", ref fAction, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        DateTime fProcessDate;
        public DateTime ProcessDate
        {
            get { return fProcessDate; }
            set { SetPropertyValue<DateTime>("ProcessDate", ref fProcessDate, value); }
        }
        string fUserName;
        [Size(25)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        double fPolicyPremium;
        public double PolicyPremium
        {
            get { return fPolicyPremium; }
            set { SetPropertyValue<double>("PolicyPremium", ref fPolicyPremium, value); }
        }
        double fCommRate;
        public double CommRate
        {
            get { return fCommRate; }
            set { SetPropertyValue<double>("CommRate", ref fCommRate, value); }
        }
        double fCommAmt;
        public double CommAmt
        {
            get { return fCommAmt; }
            set { SetPropertyValue<double>("CommAmt", ref fCommAmt, value); }
        }
        bool fAgentPaid;
        public bool AgentPaid
        {
            get { return fAgentPaid; }
            set { SetPropertyValue<bool>("AgentPaid", ref fAgentPaid, value); }
        }
        DateTime fStatementDate;
        public DateTime StatementDate
        {
            get { return fStatementDate; }
            set { SetPropertyValue<DateTime>("StatementDate", ref fStatementDate, value); }
        }
        double fPaidByInsured;
        public double PaidByInsured
        {
            get { return fPaidByInsured; }
            set { SetPropertyValue<double>("PaidByInsured", ref fPaidByInsured, value); }
        }
        double fCommPaidPrior;
        public double CommPaidPrior
        {
            get { return fCommPaidPrior; }
            set { SetPropertyValue<double>("CommPaidPrior", ref fCommPaidPrior, value); }
        }
        double fCommEarned;
        public double CommEarned
        {
            get { return fCommEarned; }
            set { SetPropertyValue<double>("CommEarned", ref fCommEarned, value); }
        }
        double fCommPaidCurrent;
        public double CommPaidCurrent
        {
            get { return fCommPaidCurrent; }
            set { SetPropertyValue<double>("CommPaidCurrent", ref fCommPaidCurrent, value); }
        }
        double fCommPaidAmtPrior;
        public double CommPaidAmtPrior
        {
            get { return fCommPaidAmtPrior; }
            set { SetPropertyValue<double>("CommPaidAmtPrior", ref fCommPaidAmtPrior, value); }
        }
        double fCommNetAmt;
        public double CommNetAmt
        {
            get { return fCommNetAmt; }
            set { SetPropertyValue<double>("CommNetAmt", ref fCommNetAmt, value); }
        }
        public AuditAgentCommissionActivity(Session session) : base(session) { }
        public AuditAgentCommissionActivity() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class AuditACHTransactions : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        DateTime fDate;
        public DateTime Date
        {
            get { return fDate; }
            set { SetPropertyValue<DateTime>("Date", ref fDate, value); }
        }
        string fRecordType;
        [Size(150)]
        public string RecordType
        {
            get { return fRecordType; }
            set { SetPropertyValue<string>("RecordType", ref fRecordType, value); }
        }
        string fRecordKey;
        [Size(200)]
        public string RecordKey
        {
            get { return fRecordKey; }
            set { SetPropertyValue<string>("RecordKey", ref fRecordKey, value); }
        }
        double fAmt;
        public double Amt
        {
            get { return fAmt; }
            set { SetPropertyValue<double>("Amt", ref fAmt, value); }
        }
        int fSettlementDays;
        public int SettlementDays
        {
            get { return fSettlementDays; }
            set { SetPropertyValue<int>("SettlementDays", ref fSettlementDays, value); }
        }
        DateTime fSettlementDate;
        public DateTime SettlementDate
        {
            get { return fSettlementDate; }
            set { SetPropertyValue<DateTime>("SettlementDate", ref fSettlementDate, value); }
        }
        DateTime fSentDate;
        public DateTime SentDate
        {
            get { return fSentDate; }
            set { SetPropertyValue<DateTime>("SentDate", ref fSentDate, value); }
        }
        string fConfirmationNo;
        [Size(30)]
        public string ConfirmationNo
        {
            get { return fConfirmationNo; }
            set { SetPropertyValue<string>("ConfirmationNo", ref fConfirmationNo, value); }
        }
        bool fIgnoreRec;
        public bool IgnoreRec
        {
            get { return fIgnoreRec; }
            set { SetPropertyValue<bool>("IgnoreRec", ref fIgnoreRec, value); }
        }
        bool fStopped;
        public bool Stopped
        {
            get { return fStopped; }
            set { SetPropertyValue<bool>("Stopped", ref fStopped, value); }
        }
        DateTime fStopDate;
        public DateTime StopDate
        {
            get { return fStopDate; }
            set { SetPropertyValue<DateTime>("StopDate", ref fStopDate, value); }
        }
        string fAgentCode;
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        bool fIsReported;
        public bool IsReported
        {
            get { return fIsReported; }
            set { SetPropertyValue<bool>("IsReported", ref fIsReported, value); }
        }
        DateTime fReportedDate;
        public DateTime ReportedDate
        {
            get { return fReportedDate; }
            set { SetPropertyValue<DateTime>("ReportedDate", ref fReportedDate, value); }
        }
        double fInstallNo;
        public double InstallNo
        {
            get { return fInstallNo; }
            set { SetPropertyValue<double>("InstallNo", ref fInstallNo, value); }
        }
        string fSourceLocation;
        [Size(250)]
        public string SourceLocation
        {
            get { return fSourceLocation; }
            set { SetPropertyValue<string>("SourceLocation", ref fSourceLocation, value); }
        }
        string fUsername;
        [Size(250)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }

        private bool _fUseTempAcct;
        public bool UseTempAcct
        {
            get { return _fUseTempAcct; }
            set { SetPropertyValue<bool>("UseTempAcct", ref _fUseTempAcct, value); }
        }
        public AuditACHTransactions(Session session) : base(session) { }
        public AuditACHTransactions() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class PolicyEndorseQuoteDrivers : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fDriverMsg;
        public string DriverMsg
        {
            get { return fDriverMsg; }
            set { SetPropertyValue<string>("DriverMsg", ref fDriverMsg, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }

        bool hasMvr;
        public bool HasMvr
        {
            get { return hasMvr; }
            set { SetPropertyValue<bool>("HasMvr", ref hasMvr, value); }
        }
        public PolicyEndorseQuoteDrivers(Session session) : base(session) { }
        public PolicyEndorseQuoteDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    public class RatingCoverageFactors : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fMainIndexID;
        public int MainIndexID
        {
            get { return fMainIndexID; }
            set { SetPropertyValue<int>("MainIndexID", ref fMainIndexID, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        string fCoverage;
        [Size(10)]
        public string Coverage
        {
            get { return fCoverage; }
            set { SetPropertyValue<string>("Coverage", ref fCoverage, value); }
        }
        int fBaseRate;
        public int BaseRate
        {
            get { return fBaseRate; }
            set { SetPropertyValue<int>("BaseRate", ref fBaseRate, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        double fClassFactor;
        public double ClassFactor
        {
            get { return fClassFactor; }
            set { SetPropertyValue<double>("ClassFactor", ref fClassFactor, value); }
        }
        double fPointFactor;
        public double PointFactor
        {
            get { return fPointFactor; }
            set { SetPropertyValue<double>("PointFactor", ref fPointFactor, value); }
        }
        double fMeritDUIFactor;
        public double MeritDUIFactor
        {
            get { return fMeritDUIFactor; }
            set { SetPropertyValue<double>("MeritDUIFactor", ref fMeritDUIFactor, value); }
        }
        double fMeritACCFactor;
        public double MeritACCFactor
        {
            get { return fMeritACCFactor; }
            set { SetPropertyValue<double>("MeritACCFactor", ref fMeritACCFactor, value); }
        }
        double fPointsAgeFactor;
        public double PointsAgeFactor
        {
            get { return fPointsAgeFactor; }
            set { SetPropertyValue<double>("PointsAgeFactor", ref fPointsAgeFactor, value); }
        }
        double fInexpFactor;
        public double InexpFactor
        {
            get { return fInexpFactor; }
            set { SetPropertyValue<double>("InexpFactor", ref fInexpFactor, value); }
        }
        double fOOSLicFactor;
        public double OOSLicFactor
        {
            get { return fOOSLicFactor; }
            set { SetPropertyValue<double>("OOSLicFactor", ref fOOSLicFactor, value); }
        }
        double fInternationalFactor;
        public double InternationalFactor
        {
            get { return fInternationalFactor; }
            set { SetPropertyValue<double>("InternationalFactor", ref fInternationalFactor, value); }
        }
        double fNoHitMVRFactor;
        public double NoHitMVRFactor
        {
            get { return fNoHitMVRFactor; }
            set { SetPropertyValue<double>("NoHitMVRFactor", ref fNoHitMVRFactor, value); }
        }
        double fSSDDiscFactor;
        public double SSDDiscFactor
        {
            get { return fSSDDiscFactor; }
            set { SetPropertyValue<double>("SSDDiscFactor", ref fSSDDiscFactor, value); }
        }
        double fAPCDiscFactor;
        public double APCDiscFactor
        {
            get { return fAPCDiscFactor; }
            set { SetPropertyValue<double>("APCDiscFactor", ref fAPCDiscFactor, value); }
        }
        double fModelYearFactor;
        public double ModelYearFactor
        {
            get { return fModelYearFactor; }
            set { SetPropertyValue<double>("ModelYearFactor", ref fModelYearFactor, value); }
        }
        double fSymbolFactor;
        public double SymbolFactor
        {
            get { return fSymbolFactor; }
            set { SetPropertyValue<double>("SymbolFactor", ref fSymbolFactor, value); }
        }
        double fBusinessUseFactor;
        public double BusinessUseFactor
        {
            get { return fBusinessUseFactor; }
            set { SetPropertyValue<double>("BusinessUseFactor", ref fBusinessUseFactor, value); }
        }
        double fArtisanFactor;
        public double ArtisanFactor
        {
            get { return fArtisanFactor; }
            set { SetPropertyValue<double>("ArtisanFactor", ref fArtisanFactor, value); }
        }
        double fABSDiscFactor;
        public double ABSDiscFactor
        {
            get { return fABSDiscFactor; }
            set { SetPropertyValue<double>("ABSDiscFactor", ref fABSDiscFactor, value); }
        }
        double fATDDiscFactor;
        public double ATDDiscFactor
        {
            get { return fATDDiscFactor; }
            set { SetPropertyValue<double>("ATDDiscFactor", ref fATDDiscFactor, value); }
        }
        double fARBDiscFactor;
        public double ARBDiscFactor
        {
            get { return fARBDiscFactor; }
            set { SetPropertyValue<double>("ARBDiscFactor", ref fARBDiscFactor, value); }
        }
        double fLimitFactor;
        public double LimitFactor
        {
            get { return fLimitFactor; }
            set { SetPropertyValue<double>("LimitFactor", ref fLimitFactor, value); }
        }
        double fDeductFactor;
        public double DeductFactor
        {
            get { return fDeductFactor; }
            set { SetPropertyValue<double>("DeductFactor", ref fDeductFactor, value); }
        }
        double fUWTierFactor;
        public double UWTierFactor
        {
            get { return fUWTierFactor; }
            set { SetPropertyValue<double>("UWTierFactor", ref fUWTierFactor, value); }
        }
        double fHousholdFactor;
        public double HousholdFactor
        {
            get { return fHousholdFactor; }
            set { SetPropertyValue<double>("HousholdFactor", ref fHousholdFactor, value); }
        }
        double fMatrixFactor;
        public double MatrixFactor
        {
            get { return fMatrixFactor; }
            set { SetPropertyValue<double>("MatrixFactor", ref fMatrixFactor, value); }
        }
        double fPriorBalanceFactor;
        public double PriorBalanceFactor
        {
            get { return fPriorBalanceFactor; }
            set { SetPropertyValue<double>("PriorBalanceFactor", ref fPriorBalanceFactor, value); }
        }
        double fUnacceptFactor;
        public double UnacceptFactor
        {
            get { return fUnacceptFactor; }
            set { SetPropertyValue<double>("UnacceptFactor", ref fUnacceptFactor, value); }
        }
        double fSDDiscFactor;
        public double SDDiscFactor
        {
            get { return fSDDiscFactor; }
            set { SetPropertyValue<double>("SDDiscFactor", ref fSDDiscFactor, value); }
        }
        double fTransferDiscFactor;
        public double TransferDiscFactor
        {
            get { return fTransferDiscFactor; }
            set { SetPropertyValue<double>("TransferDiscFactor", ref fTransferDiscFactor, value); }
        }
        double fDirectRepairDiscFactor;
        public double DirectRepairDiscFactor
        {
            get { return fDirectRepairDiscFactor; }
            set { SetPropertyValue<double>("DirectRepairDiscFactor", ref fDirectRepairDiscFactor, value); }
        }
        double fWLE_NIFactor;
        public double WLE_NIFactor
        {
            get { return fWLE_NIFactor; }
            set { SetPropertyValue<double>("WLE_NIFactor", ref fWLE_NIFactor, value); }
        }
        double fWLE_NIRRFactor;
        public double WLE_NIRRFactor
        {
            get { return fWLE_NIRRFactor; }
            set { SetPropertyValue<double>("WLE_NIRRFactor", ref fWLE_NIRRFactor, value); }
        }
        double fPaidInFullDiscFactor;
        public double PaidInFullDiscFactor
        {
            get { return fPaidInFullDiscFactor; }
            set { SetPropertyValue<double>("PaidInFullDiscFactor", ref fPaidInFullDiscFactor, value); }
        }
        double fEFTDiscFactor;
        public double EFTDiscFactor
        {
            get { return fEFTDiscFactor; }
            set { SetPropertyValue<double>("EFTDiscFactor", ref fEFTDiscFactor, value); }
        }
        double fRenDiscFactor;
        public double RenDiscFactor
        {
            get { return fRenDiscFactor; }
            set { SetPropertyValue<double>("RenDiscFactor", ref fRenDiscFactor, value); }
        }
        double fPayPlanFactor;
        public double PayPlanFactor
        {
            get { return fPayPlanFactor; }
            set { SetPropertyValue<double>("PayPlanFactor", ref fPayPlanFactor, value); }
        }
        double fMCDiscFactor;
        public double MCDiscFactor
        {
            get { return fMCDiscFactor; }
            set { SetPropertyValue<double>("MCDiscFactor", ref fMCDiscFactor, value); }
        }
        double fExpenseFeeAmt;
        public double ExpenseFeeAmt
        {
            get { return fExpenseFeeAmt; }
            set { SetPropertyValue<double>("ExpenseFeeAmt", ref fExpenseFeeAmt, value); }
        }
        double fZipCodeFactor;
        public double ZipCodeFactor
        {
            get { return fZipCodeFactor; }
            set { SetPropertyValue<double>("ZipCodeFactor", ref fZipCodeFactor, value); }
        }
        double fProRataFactor;
        public double ProRataFactor
        {
            get { return fProRataFactor; }
            set { SetPropertyValue<double>("ProRataFactor", ref fProRataFactor, value); }
        }
        double fPreFactorAnnlPrem;
        public double PreFactorAnnlPrem
        {
            get { return fPreFactorAnnlPrem; }
            set { SetPropertyValue<double>("PreFactorAnnlPrem", ref fPreFactorAnnlPrem, value); }
        }
        public RatingCoverageFactors(Session session) : base(session) { }
        public RatingCoverageFactors() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
