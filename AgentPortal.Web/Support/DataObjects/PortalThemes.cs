﻿using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PortalThemes :XPCustomObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }

        string fCompanyName;
        public string CompanyName
        {
            get { return fCompanyName; }
            set { SetPropertyValue<string>("CompanyName", ref fCompanyName, value); }
        }

        string fLogoName;
        public string LogoName
        {
            get { return fLogoName; }
            set { SetPropertyValue<string>("LogoName", ref fLogoName, value); }
        }
        string fPrimaryColor;
        public string PrimaryColor
        {
            get { return fPrimaryColor; }
            set { SetPropertyValue<string>("PrimaryColor", ref fPrimaryColor, value); }
        }

        string fSecondaryColor;
        public string SecondaryColor
        {
            get { return fSecondaryColor; }
            set { SetPropertyValue<string>("SecondaryColor", ref fSecondaryColor, value); }
        }
        string fWhoWeAre;
        public string WhoWeAre
        {
            get { return fWhoWeAre; }
            set { SetPropertyValue<string>("WhoWeAre", ref fWhoWeAre, value); }
        }
        string fFullServiceCo;
        public string FullServiceCo
        {
            get { return fFullServiceCo; }
            set { SetPropertyValue<string>("FullServiceCo", ref fFullServiceCo, value); }
        }
        string fWhoWeRepresent;
        public string WhoWeRepresent
        {
            get { return fWhoWeRepresent; }
            set { SetPropertyValue<string>("WhoWeRepresent", ref fWhoWeRepresent, value); }
        }
        string fWhyThisCompany;
        public string WhyThisCompany
        {
            get { return fWhyThisCompany; }
            set { SetPropertyValue<string>("WhyThisCompany", ref fWhyThisCompany, value); }
        }
        string fAboutUs;
        public string AboutUs
        {
            get { return fAboutUs; }
            set { SetPropertyValue<string>("AboutUs", ref fAboutUs, value); }
        }
        string fWelcome;
        public string Welcome
        {
            get { return fWelcome; }
            set { SetPropertyValue<string>("Welcome", ref fWelcome, value); }
        }
        string fOurStaff;
        public string OurStaff
        {
            get { return fOurStaff; }
            set { SetPropertyValue<string>("OurStaff", ref fOurStaff, value); }
        }
        string fUnderwritingAndAccounting;
        public string UnderwritingAndAccounting
        {
            get { return fUnderwritingAndAccounting; }
            set { SetPropertyValue<string>("UnderwritingAndAccounting", ref fUnderwritingAndAccounting, value); }
        }
        string fClaimsManagement;
        public string ClaimsManagement
        {
            get { return fClaimsManagement; }
            set { SetPropertyValue<string>("ClaimsManagement", ref fClaimsManagement, value); }
        }
        string fSpecialUnit;
        public string SpecialUnit
        {
            get { return fSpecialUnit; }
            set { SetPropertyValue<string>("SpecialUnit", ref fSpecialUnit, value); }
        }
        string fInformationTechnology;
        public string InformationTechnology
        {
            get { return fInformationTechnology; }
            set { SetPropertyValue<string>("InformationTechnology", ref fInformationTechnology, value); }
        }
        string fCustomerService;
        public string CustomerService
        {
            get { return fCustomerService; }
            set { SetPropertyValue<string>("CustomerService", ref fCustomerService, value); }
        }
        string fAboutImageName;
        public string AboutImageName
        {
            get { return fAboutImageName; }
            set { SetPropertyValue<string>("AboutImageName", ref fAboutImageName, value); }
        }
        string fSliderImageName1;
        public string SliderImageName1
        {
            get { return fSliderImageName1; }
            set { SetPropertyValue<string>("SliderImageName1", ref fSliderImageName1, value); }
        }
        string fSliderImageName2;
        public string SliderImageName2
        {
            get { return fSliderImageName2; }
            set { SetPropertyValue<string>("SliderImageName2", ref fSliderImageName2, value); }
        }
        string fSliderImageName3;
        public string SliderImageName3
        {
            get { return fSliderImageName3; }
            set { SetPropertyValue<string>("SliderImageName3", ref fSliderImageName3, value); }
        }
        string fContactPhone;
        public string ContactPhone
        {
            get { return fContactPhone; }
            set { SetPropertyValue<string>("ContactPhone", ref fContactPhone, value); }
        }
        string fContactEmail;
        public string ContactEmail
        {
            get { return fContactEmail; }
            set { SetPropertyValue<string>("ContactEmail", ref fContactEmail, value); }
        }
        string fFooterMiddle;
        public string FooterMiddle
        {
            get { return fFooterMiddle; }
            set { SetPropertyValue<string>("FooterMiddle", ref fFooterMiddle, value); }
        }
        string fFooterAboutUs;
        public string FooterAboutUs
        {
            get { return fFooterAboutUs; }
            set { SetPropertyValue<string>("FooterAboutUs", ref fFooterAboutUs, value); }
        }

        public PortalThemes(Session session)
            : base(session)
        {
            
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}