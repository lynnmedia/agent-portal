﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditLexisCreditRequest
    {
        int IndexID { get; set; }
        string GuidCredit { get; set; }
        string Request { get; set; }
        DateTime DateCreated { get; set; }
    }

    public class AuditLexisCreditRequest : IAuditLexisCreditRequest
    {
        public int IndexID { get; set; }
        public string GuidCredit { get; set; }
        public string Request { get; set; }
        public DateTime DateCreated { get; set; }
    }

    [Persistent("AuditLexisCreditRequest")]
    public partial class XpoAuditLexisCreditRequest : XPLiteObject, IAuditLexisCreditRequest
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fGuidCredit;
        [Size(60)]
        public string GuidCredit
        {
            get { return fGuidCredit; }
            set { SetPropertyValue<string>("GuidCredit", ref fGuidCredit, value); }
        }
        string fRequest;
        [Size(SizeAttribute.Unlimited)]
        public string Request
        {
            get { return fRequest; }
            set { SetPropertyValue<string>("Request", ref fRequest, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }

        public XpoAuditLexisCreditRequest(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
