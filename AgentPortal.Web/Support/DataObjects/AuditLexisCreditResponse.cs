﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditLexisCreditResponse
    {
        int IndexID { get; set; }
        string GuidCredit { get; set; }
        string Response { get; set; }
        DateTime DateCreated { get; set; }
    }

    public class AuditLexisCreditResponse : IAuditLexisCreditResponse
    {
        public int IndexID { get; set; }
        public string GuidCredit { get; set; }
        public string Response { get; set; }
        public DateTime DateCreated { get; set; }
    }

	[Persistent("AuditLexisCreditResponse")]
    public partial class XpoAuditLexisCreditResponse : XPLiteObject, IAuditLexisCreditResponse
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fGuidCredit;
        [Size(60)]
        public string GuidCredit
        {
            get { return fGuidCredit; }
            set { SetPropertyValue<string>("GuidCredit", ref fGuidCredit, value); }
        }
        string fResponse;
        [Size(SizeAttribute.Unlimited)]
        public string Response
        {
            get { return fResponse; }
            set { SetPropertyValue<string>("Response", ref fResponse, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }

        public XpoAuditLexisCreditResponse(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
