using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditPolicyHistoryReasons
    {
        string PolicyNo { get; set; }
        int HistoryKey { get; set; }
        string ReasonType { get; set; }
        int ReasonNo { get; set; }
        string HistoryAction { get; set; }
        string ReasonCode { get; set; }
        string Status { get; set; }
        bool Cancel { get; set; }
        string Reason { get; set; }
        string ReasonIndex { get; set; }
        string IndexDesc { get; set; }
        string UserName { get; set; }
        DateTime LastUpdated { get; set; }
        int IndexID { get; set; }
        string TempGUID { get; set; }
    }

    public class AuditPolicyHistoryReasons : IAuditPolicyHistoryReasons
    {
        public string PolicyNo { get; set; }
        public int HistoryKey { get; set; }
        public string ReasonType { get; set; }
        public int ReasonNo { get; set; }
        public string HistoryAction { get; set; }
        public string ReasonCode { get; set; }
        public string Status { get; set; }
        public bool Cancel { get; set; }
        public string Reason { get; set; }
        public string ReasonIndex { get; set; }
        public string IndexDesc { get; set; }
        public string UserName { get; set; }
        public DateTime LastUpdated { get; set; }
        public int IndexID { get; set; }
        public string TempGUID { get; set; }
    }

	[Persistent("AuditPolicyHistoryReasons")]
    public class XpoAuditPolicyHistoryReasons : XPLiteObject, IAuditPolicyHistoryReasons
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fHistoryKey;
        public int HistoryKey
        {
            get { return fHistoryKey; }
            set { SetPropertyValue<int>("HistoryKey", ref fHistoryKey, value); }
        }
        string fReasonType;
        [Size(10)]
        public string ReasonType
        {
            get { return fReasonType; }
            set { SetPropertyValue<string>("ReasonType", ref fReasonType, value); }
        }
        int fReasonNo;
        public int ReasonNo
        {
            get { return fReasonNo; }
            set { SetPropertyValue<int>("ReasonNo", ref fReasonNo, value); }
        }
        string fHistoryAction;
        [Size(10)]
        public string HistoryAction
        {
            get { return fHistoryAction; }
            set { SetPropertyValue<string>("HistoryAction", ref fHistoryAction, value); }
        }
        string fReasonCode;
        [Size(5)]
        public string ReasonCode
        {
            get { return fReasonCode; }
            set { SetPropertyValue<string>("ReasonCode", ref fReasonCode, value); }
        }
        string fStatus;
        [Size(7)]
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        bool fCancel;
        public bool Cancel
        {
            get { return fCancel; }
            set { SetPropertyValue<bool>("Cancel", ref fCancel, value); }
        }
        string fReason;
        [Size(8000)]
        public string Reason
        {
            get { return fReason; }
            set { SetPropertyValue<string>("Reason", ref fReason, value); }
        }
        string fReasonIndex;
        [Size(7)]
        public string ReasonIndex
        {
            get { return fReasonIndex; }
            set { SetPropertyValue<string>("ReasonIndex", ref fReasonIndex, value); }
        }
        string fIndexDesc;
        [Size(2000)]
        public string IndexDesc
        {
            get { return fIndexDesc; }
            set { SetPropertyValue<string>("IndexDesc", ref fIndexDesc, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        DateTime fLastUpdated;
        public DateTime LastUpdated
        {
            get { return fLastUpdated; }
            set { SetPropertyValue<DateTime>("LastUpdated", ref fLastUpdated, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fTempGUID;
        [Size(50)]
        public string TempGUID
        {
            get { return fTempGUID; }
            set { SetPropertyValue<string>("TempGUID", ref fTempGUID, value); }
        }
        public XpoAuditPolicyHistoryReasons(Session session) : base(session) { }
        public XpoAuditPolicyHistoryReasons() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
