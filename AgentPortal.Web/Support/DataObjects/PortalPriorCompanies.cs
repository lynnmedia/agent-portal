using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PortalPriorCompanies : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fCompanyName;
        [Size(500)]
        public string CompanyName
        {
            get { return fCompanyName; }
            set { SetPropertyValue<string>("CompanyName", ref fCompanyName, value); }
        }
        bool fActive;
        public bool Active
        {
            get { return fActive; }
            set { SetPropertyValue<bool>("Active", ref fActive, value); }
        }
        public PortalPriorCompanies(Session session) : base(session) { }
        public PortalPriorCompanies() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
