using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class RenQuoteInstallments : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(15)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        decimal fInstallNo;
        public decimal InstallNo
        {
            get { return fInstallNo; }
            set { SetPropertyValue<decimal>("InstallNo", ref fInstallNo, value); }
        }
        short fInstallCount;
        public short InstallCount
        {
            get { return fInstallCount; }
            set { SetPropertyValue<short>("InstallCount", ref fInstallCount, value); }
        }
        string fDescr;
        [Size(40)]
        public string Descr
        {
            get { return fDescr; }
            set { SetPropertyValue<string>("Descr", ref fDescr, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        DateTime fDueDate;
        public DateTime DueDate
        {
            get { return fDueDate; }
            set { SetPropertyValue<DateTime>("DueDate", ref fDueDate, value); }
        }
        decimal fPolicyWritten;
        public decimal PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<decimal>("PolicyWritten", ref fPolicyWritten, value); }
        }
        decimal fInstallmentFee;
        public decimal InstallmentFee
        {
            get { return fInstallmentFee; }
            set { SetPropertyValue<decimal>("InstallmentFee", ref fInstallmentFee, value); }
        }
        decimal fPolicyFee;
        public decimal PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<decimal>("PolicyFee", ref fPolicyFee, value); }
        }
        decimal fSR22Fee;
        public decimal SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<decimal>("SR22Fee", ref fSR22Fee, value); }
        }
        decimal fFHCF_Fee;
        public decimal FHCF_Fee
        {
            get { return fFHCF_Fee; }
            set { SetPropertyValue<decimal>("FHCF_Fee", ref fFHCF_Fee, value); }
        }
        decimal fDBSetupFee;
        public decimal DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<decimal>("DBSetupFee", ref fDBSetupFee, value); }
        }
        decimal fLateFee;
        public decimal LateFee
        {
            get { return fLateFee; }
            set { SetPropertyValue<decimal>("LateFee", ref fLateFee, value); }
        }
        decimal fFeeTotal;
        public decimal FeeTotal
        {
            get { return fFeeTotal; }
            set { SetPropertyValue<decimal>("FeeTotal", ref fFeeTotal, value); }
        }
        decimal fDownPymtPercent;
        public decimal DownPymtPercent
        {
            get { return fDownPymtPercent; }
            set { SetPropertyValue<decimal>("DownPymtPercent", ref fDownPymtPercent, value); }
        }
        decimal fDownPymtAmount;
        public decimal DownPymtAmount
        {
            get { return fDownPymtAmount; }
            set { SetPropertyValue<decimal>("DownPymtAmount", ref fDownPymtAmount, value); }
        }
        decimal fInstallmentPymt;
        public decimal InstallmentPymt
        {
            get { return fInstallmentPymt; }
            set { SetPropertyValue<decimal>("InstallmentPymt", ref fInstallmentPymt, value); }
        }
        decimal fPremiumBalance;
        public decimal PremiumBalance
        {
            get { return fPremiumBalance; }
            set { SetPropertyValue<decimal>("PremiumBalance", ref fPremiumBalance, value); }
        }
        decimal fPymtApplied;
        public decimal PymtApplied
        {
            get { return fPymtApplied; }
            set { SetPropertyValue<decimal>("PymtApplied", ref fPymtApplied, value); }
        }
        decimal fBalanceInstallment;
        public decimal BalanceInstallment
        {
            get { return fBalanceInstallment; }
            set { SetPropertyValue<decimal>("BalanceInstallment", ref fBalanceInstallment, value); }
        }
        bool fLocked;
        public bool Locked
        {
            get { return fLocked; }
            set { SetPropertyValue<bool>("Locked", ref fLocked, value); }
        }
        bool fIgnoreRec;
        public bool IgnoreRec
        {
            get { return fIgnoreRec; }
            set { SetPropertyValue<bool>("IgnoreRec", ref fIgnoreRec, value); }
        }
        DateTime fDateIgnored;
        public DateTime DateIgnored
        {
            get { return fDateIgnored; }
            set { SetPropertyValue<DateTime>("DateIgnored", ref fDateIgnored, value); }
        }
        DateTime fLastPayDate;
        public DateTime LastPayDate
        {
            get { return fLastPayDate; }
            set { SetPropertyValue<DateTime>("LastPayDate", ref fLastPayDate, value); }
        }
        double fLastPayAmt;
        public double LastPayAmt
        {
            get { return fLastPayAmt; }
            set { SetPropertyValue<double>("LastPayAmt", ref fLastPayAmt, value); }
        }
        int fWritten;
        public int Written
        {
            get { return fWritten; }
            set { SetPropertyValue<int>("Written", ref fWritten, value); }
        }
        DateTime fPrintDate;
        public DateTime PrintDate
        {
            get { return fPrintDate; }
            set { SetPropertyValue<DateTime>("PrintDate", ref fPrintDate, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        decimal fNSF_Fee;
        public decimal NSF_Fee
        {
            get { return fNSF_Fee; }
            set { SetPropertyValue<decimal>("NSF_Fee", ref fNSF_Fee, value); }
        }
        int fCancIDSetToIgnore;
        public int CancIDSetToIgnore
        {
            get { return fCancIDSetToIgnore; }
            set { SetPropertyValue<int>("CancIDSetToIgnore", ref fCancIDSetToIgnore, value); }
        }
        int fCancIDCreatedInstall;
        public int CancIDCreatedInstall
        {
            get { return fCancIDCreatedInstall; }
            set { SetPropertyValue<int>("CancIDCreatedInstall", ref fCancIDCreatedInstall, value); }
        }
        public RenQuoteInstallments(Session session) : base(session) { }
        public RenQuoteInstallments() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

    [DeferredDeletion(false)]
    public class RenQuoteInstallmentsTemp : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(15)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        decimal fInstallNo;
        public decimal InstallNo
        {
            get { return fInstallNo; }
            set { SetPropertyValue<decimal>("InstallNo", ref fInstallNo, value); }
        }
        short fInstallCount;
        public short InstallCount
        {
            get { return fInstallCount; }
            set { SetPropertyValue<short>("InstallCount", ref fInstallCount, value); }
        }
        string fDescr;
        [Size(40)]
        public string Descr
        {
            get { return fDescr; }
            set { SetPropertyValue<string>("Descr", ref fDescr, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        DateTime fDueDate;
        public DateTime DueDate
        {
            get { return fDueDate; }
            set { SetPropertyValue<DateTime>("DueDate", ref fDueDate, value); }
        }
        decimal fPolicyWritten;
        public decimal PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<decimal>("PolicyWritten", ref fPolicyWritten, value); }
        }
        decimal fInstallmentFee;
        public decimal InstallmentFee
        {
            get { return fInstallmentFee; }
            set { SetPropertyValue<decimal>("InstallmentFee", ref fInstallmentFee, value); }
        }
        decimal fPolicyFee;
        public decimal PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<decimal>("PolicyFee", ref fPolicyFee, value); }
        }
        decimal fSR22Fee;
        public decimal SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<decimal>("SR22Fee", ref fSR22Fee, value); }
        }
        decimal fFHCF_Fee;
        public decimal FHCF_Fee
        {
            get { return fFHCF_Fee; }
            set { SetPropertyValue<decimal>("FHCF_Fee", ref fFHCF_Fee, value); }
        }
        decimal fDBSetupFee;
        public decimal DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<decimal>("DBSetupFee", ref fDBSetupFee, value); }
        }
        decimal fLateFee;
        public decimal LateFee
        {
            get { return fLateFee; }
            set { SetPropertyValue<decimal>("LateFee", ref fLateFee, value); }
        }
        decimal fFeeTotal;
        public decimal FeeTotal
        {
            get { return fFeeTotal; }
            set { SetPropertyValue<decimal>("FeeTotal", ref fFeeTotal, value); }
        }
        decimal fDownPymtPercent;
        public decimal DownPymtPercent
        {
            get { return fDownPymtPercent; }
            set { SetPropertyValue<decimal>("DownPymtPercent", ref fDownPymtPercent, value); }
        }
        decimal fDownPymtAmount;
        public decimal DownPymtAmount
        {
            get { return fDownPymtAmount; }
            set { SetPropertyValue<decimal>("DownPymtAmount", ref fDownPymtAmount, value); }
        }
        decimal fInstallmentPymt;
        public decimal InstallmentPymt
        {
            get { return fInstallmentPymt; }
            set { SetPropertyValue<decimal>("InstallmentPymt", ref fInstallmentPymt, value); }
        }
        decimal fPremiumBalance;
        public decimal PremiumBalance
        {
            get { return fPremiumBalance; }
            set { SetPropertyValue<decimal>("PremiumBalance", ref fPremiumBalance, value); }
        }
        decimal fPymtApplied;
        public decimal PymtApplied
        {
            get { return fPymtApplied; }
            set { SetPropertyValue<decimal>("PymtApplied", ref fPymtApplied, value); }
        }
        decimal fBalanceInstallment;
        public decimal BalanceInstallment
        {
            get { return fBalanceInstallment; }
            set { SetPropertyValue<decimal>("BalanceInstallment", ref fBalanceInstallment, value); }
        }
        bool fLocked;
        public bool Locked
        {
            get { return fLocked; }
            set { SetPropertyValue<bool>("Locked", ref fLocked, value); }
        }
        bool fIgnoreRec;
        public bool IgnoreRec
        {
            get { return fIgnoreRec; }
            set { SetPropertyValue<bool>("IgnoreRec", ref fIgnoreRec, value); }
        }
        DateTime fDateIgnored;
        public DateTime DateIgnored
        {
            get { return fDateIgnored; }
            set { SetPropertyValue<DateTime>("DateIgnored", ref fDateIgnored, value); }
        }
        DateTime fLastPayDate;
        public DateTime LastPayDate
        {
            get { return fLastPayDate; }
            set { SetPropertyValue<DateTime>("LastPayDate", ref fLastPayDate, value); }
        }
        double fLastPayAmt;
        public double LastPayAmt
        {
            get { return fLastPayAmt; }
            set { SetPropertyValue<double>("LastPayAmt", ref fLastPayAmt, value); }
        }
        int fWritten;
        public int Written
        {
            get { return fWritten; }
            set { SetPropertyValue<int>("Written", ref fWritten, value); }
        }
        DateTime fPrintDate;
        public DateTime PrintDate
        {
            get { return fPrintDate; }
            set { SetPropertyValue<DateTime>("PrintDate", ref fPrintDate, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        decimal fNSF_Fee;
        public decimal NSF_Fee
        {
            get { return fNSF_Fee; }
            set { SetPropertyValue<decimal>("NSF_Fee", ref fNSF_Fee, value); }
        }
        int fCancIDSetToIgnore;
        public int CancIDSetToIgnore
        {
            get { return fCancIDSetToIgnore; }
            set { SetPropertyValue<int>("CancIDSetToIgnore", ref fCancIDSetToIgnore, value); }
        }
        int fCancIDCreatedInstall;
        public int CancIDCreatedInstall
        {
            get { return fCancIDCreatedInstall; }
            set { SetPropertyValue<int>("CancIDCreatedInstall", ref fCancIDCreatedInstall, value); }
        }
        public RenQuoteInstallmentsTemp(Session session) : base(session) { }
        public RenQuoteInstallmentsTemp() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
