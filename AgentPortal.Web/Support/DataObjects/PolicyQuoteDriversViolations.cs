﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyQuoteDriversViolations : XPCustomObject
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        int fViolationNo;
        public int ViolationNo
        {
            get { return fViolationNo; }
            set { SetPropertyValue<int>("ViolationNo", ref fViolationNo, value); }
        }
        int fViolationIndex;
        public int ViolationIndex
        {
            get { return fViolationIndex; }
            set { SetPropertyValue<int>("ViolationIndex", ref fViolationIndex, value); }
        }
        string fViolationCode;
        [Size(20)]
        public string ViolationCode
        {
            get { return fViolationCode; }
            set { SetPropertyValue<string>("ViolationCode", ref fViolationCode, value); }
        }
        string fViolationGroup;
        [Size(20)]
        public string ViolationGroup
        {
            get { return fViolationGroup; }
            set { SetPropertyValue<string>("ViolationGroup", ref fViolationGroup, value); }
        }
        string fViolationDesc;
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }
        DateTime fViolationDate;
        public DateTime ViolationDate
        {
            get { return fViolationDate; }
            set { SetPropertyValue<DateTime>("ViolationDate", ref fViolationDate, value); }
        }
        int fViolationMonths;
        public int ViolationMonths
        {
            get { return fViolationMonths; }
            set { SetPropertyValue<int>("ViolationMonths", ref fViolationMonths, value); }
        }
        int fViolationPoints;
        public int ViolationPoints
        {
            get { return fViolationPoints; }
            set { SetPropertyValue<int>("ViolationPoints", ref fViolationPoints, value); }
        }
        bool fChargeable;
        public bool Chargeable
        {
            get { return fChargeable; }
            set { SetPropertyValue<bool>("Chargeable", ref fChargeable, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }

        bool fFromMVR;
        public bool FromMVR
        {
            get { return fFromMVR; }
            set { SetPropertyValue<bool>("FromMVR", ref fFromMVR, value); }
        }

        bool fFromAPlus;
        public bool FromAPlus
        {
            get { return fFromAPlus; }
            set { SetPropertyValue<bool>("FromAPlus", ref fFromAPlus, value); }
        }

        bool fromCV;
        public bool FromCV
        {
            get { return fromCV; }
            set { SetPropertyValue<bool>("FromCV", ref fromCV, value); }
        }
        
        [NonPersistent]
        public int AAPoints { get; set; }

        public PolicyQuoteDriversViolations(Session session) : base(session) { }
        public PolicyQuoteDriversViolations() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}