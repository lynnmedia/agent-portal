using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAgents
    {
        string AgentCode { get; set; }
        string AgencyName { get; set; }
        string PrimaryFirstName { get; set; }
        string PrimaryLastName { get; set; }
        bool IsOwnerOperator { get; set; }
        string DBA { get; set; }
        string MailAddress { get; set; }
        string MailCity { get; set; }
        string MailState { get; set; }
        string MailZIP { get; set; }
        string PhysicalAddress { get; set; }
        string PhysicalCity { get; set; }
        string PhysicalState { get; set; }
        string PhysicalZIP { get; set; }
        string County { get; set; }
        string FEIN { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        string EmailAddress { get; set; }
        string Website { get; set; }
        string MarketingRep { get; set; }
        string Group { get; set; }
        DateTime DateOpened { get; set; }
        DateTime DateLicensed { get; set; }
        string InitialContact { get; set; }
        bool IsAllowedHigherBI { get; set; }
        bool IsPaperless { get; set; }
        string Status { get; set; }
        DateTime StatusDate { get; set; }
        bool IsProbation { get; set; }
        string TerminationCause { get; set; }
        string LastReview { get; set; }
        int OID { get; set; }
        double CommissionRate { get; set; }
        bool MailGarageSame { get; set; }
        string MarketingRepUserID { get; set; }
        string Password { get; set; }
        string BankAccountName { get; set; }
        string Account { get; set; }
        string AccountType { get; set; }
        string ABA { get; set; }
        string AccountNumber { get; set; }
        string Carrier { get; set; }
        string EOMailingAddress { get; set; }
        string EOCity { get; set; }
        string EOState { get; set; }
        string EOZip { get; set; }
        string EOPolicyNo { get; set; }
        DateTime EOEffDate { get; set; }
        DateTime EOExpDate { get; set; }
        string EONotes { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        string ComparativeRater { get; set; }
        string AltEmailAddress { get; set; }
        string AltPassword { get; set; }
        string AgentGuid { get; set; }
        bool AllowLowDp { get; set; }
        bool IsRequireCall { get; set; }
    }

    public class Agents : IAgents
    {
        public string AgentCode { get; set; }
        public string AgencyName { get; set; }
        public string PrimaryFirstName { get; set; }
        public string PrimaryLastName { get; set; }
        public bool IsOwnerOperator { get; set; }
        public string DBA { get; set; }
        public string MailAddress { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailZIP { get; set; }
        public string PhysicalAddress { get; set; }
        public string PhysicalCity { get; set; }
        public string PhysicalState { get; set; }
        public string PhysicalZIP { get; set; }
        public string County { get; set; }
        public string FEIN { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string EmailAddress { get; set; }
        public string Website { get; set; }
        public string MarketingRep { get; set; }
        public string Group { get; set; }
        public DateTime DateOpened { get; set; }
        public DateTime DateLicensed { get; set; }
        public string InitialContact { get; set; }
        public bool IsAllowedHigherBI { get; set; }
        public bool IsPaperless { get; set; }
        public string Status { get; set; }
        public DateTime StatusDate { get; set; }
        public bool IsProbation { get; set; }
        public string TerminationCause { get; set; }
        public string LastReview { get; set; }
        public int OID { get; set; }
        public double CommissionRate { get; set; }
        public bool MailGarageSame { get; set; }
        public string MarketingRepUserID { get; set; }
        public string Password { get; set; }
        public string BankAccountName { get; set; }
        public string Account { get; set; }
        public string AccountType { get; set; }
        public string ABA { get; set; }
        public string AccountNumber { get; set; }
        public string Carrier { get; set; }
        public string EOMailingAddress { get; set; }
        public string EOCity { get; set; }
        public string EOState { get; set; }
        public string EOZip { get; set; }
        public string EOPolicyNo { get; set; }
        public DateTime EOEffDate { get; set; }
        public DateTime EOExpDate { get; set; }
        public string EONotes { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string ComparativeRater { get; set; }
        public string AltEmailAddress { get; set; }
        public string AltPassword { get; set; }
        public string AgentGuid { get; set; }
        public bool AllowLowDp { get; set; }
        public bool IsRequireCall { get; set; }
    }

	[Persistent("Agents")]
    public class XpoAgents : XPCustomObject, IAgents
    {
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        string fAgencyName;
        public string AgencyName
        {
            get { return fAgencyName; }
            set { SetPropertyValue<string>("AgencyName", ref fAgencyName, value); }
        }
        string fPrimaryFirstName;
        [Size(50)]
        public string PrimaryFirstName
        {
            get { return fPrimaryFirstName; }
            set { SetPropertyValue<string>("PrimaryFirstName", ref fPrimaryFirstName, value); }
        }
        string fPrimaryLastName;
        [Size(50)]
        public string PrimaryLastName
        {
            get { return fPrimaryLastName; }
            set { SetPropertyValue<string>("PrimaryLastName", ref fPrimaryLastName, value); }
        }
        bool fIsOwnerOperator;
        public bool IsOwnerOperator
        {
            get { return fIsOwnerOperator; }
            set { SetPropertyValue<bool>("IsOwnerOperator", ref fIsOwnerOperator, value); }
        }
        string fDBA;
        public string DBA
        {
            get { return fDBA; }
            set { SetPropertyValue<string>("DBA", ref fDBA, value); }
        }
        string fMailAddress;
        [Size(250)]
        public string MailAddress
        {
            get { return fMailAddress; }
            set { SetPropertyValue<string>("MailAddress", ref fMailAddress, value); }
        }
        string fMailCity;
        [Size(150)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZIP;
        [Size(15)]
        public string MailZIP
        {
            get { return fMailZIP; }
            set { SetPropertyValue<string>("MailZIP", ref fMailZIP, value); }
        }
        string fPhysicalAddress;
        [Size(250)]
        public string PhysicalAddress
        {
            get { return fPhysicalAddress; }
            set { SetPropertyValue<string>("PhysicalAddress", ref fPhysicalAddress, value); }
        }
        string fPhysicalCity;
        [Size(150)]
        public string PhysicalCity
        {
            get { return fPhysicalCity; }
            set { SetPropertyValue<string>("PhysicalCity", ref fPhysicalCity, value); }
        }
        string fPhysicalState;
        [Size(2)]
        public string PhysicalState
        {
            get { return fPhysicalState; }
            set { SetPropertyValue<string>("PhysicalState", ref fPhysicalState, value); }
        }
        string fPhysicalZIP;
        [Size(15)]
        public string PhysicalZIP
        {
            get { return fPhysicalZIP; }
            set { SetPropertyValue<string>("PhysicalZIP", ref fPhysicalZIP, value); }
        }
        string fCounty;
        [Size(50)]
        public string County
        {
            get { return fCounty; }
            set { SetPropertyValue<string>("County", ref fCounty, value); }
        }
        string fFEIN;
        [Size(50)]
        public string FEIN
        {
            get { return fFEIN; }
            set { SetPropertyValue<string>("FEIN", ref fFEIN, value); }
        }
        string fPhone;
        [Size(20)]
        public string Phone
        {
            get { return fPhone; }
            set { SetPropertyValue<string>("Phone", ref fPhone, value); }
        }
        string fFax;
        [Size(20)]
        public string Fax
        {
            get { return fFax; }
            set { SetPropertyValue<string>("Fax", ref fFax, value); }
        }
        string fEmailAddress;
        [Size(200)]
        public string EmailAddress
        {
            get { return fEmailAddress; }
            set { SetPropertyValue<string>("EmailAddress", ref fEmailAddress, value); }
        }
        string fWebsite;
        [Size(150)]
        public string Website
        {
            get { return fWebsite; }
            set { SetPropertyValue<string>("Website", ref fWebsite, value); }
        }
        string fMarketingRep;
        public string MarketingRep
        {
            get { return fMarketingRep; }
            set { SetPropertyValue<string>("MarketingRep", ref fMarketingRep, value); }
        }
        string fGroup;
        public string Group
        {
            get { return fGroup; }
            set { SetPropertyValue<string>("Group", ref fGroup, value); }
        }
        DateTime fDateOpened;
        public DateTime DateOpened
        {
            get { return fDateOpened; }
            set { SetPropertyValue<DateTime>("DateOpened", ref fDateOpened, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        string fInitialContact;
        [Size(150)]
        public string InitialContact
        {
            get { return fInitialContact; }
            set { SetPropertyValue<string>("InitialContact", ref fInitialContact, value); }
        }
        bool fIsAllowedHigherBI;
        public bool IsAllowedHigherBI
        {
            get { return fIsAllowedHigherBI; }
            set { SetPropertyValue<bool>("IsAllowedHigherBI", ref fIsAllowedHigherBI, value); }
        }
        bool fIsPaperless;
        public bool IsPaperless
        {
            get { return fIsPaperless; }
            set { SetPropertyValue<bool>("IsPaperless", ref fIsPaperless, value); }
        }
        string fStatus;
        public string Status
        {
            get { return fStatus; }
            set { SetPropertyValue<string>("Status", ref fStatus, value); }
        }
        DateTime fStatusDate;
        public DateTime StatusDate
        {
            get { return fStatusDate; }
            set { SetPropertyValue<DateTime>("StatusDate", ref fStatusDate, value); }
        }
        bool fIsProbation;
        public bool IsProbation
        {
            get { return fIsProbation; }
            set { SetPropertyValue<bool>("IsProbation", ref fIsProbation, value); }
        }
        string fTerminationCause;
        [Size(300)]
        public string TerminationCause
        {
            get { return fTerminationCause; }
            set { SetPropertyValue<string>("TerminationCause", ref fTerminationCause, value); }
        }
        string fLastReview;
        [Size(150)]
        public string LastReview
        {
            get { return fLastReview; }
            set { SetPropertyValue<string>("LastReview", ref fLastReview, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        double fCommissionRate;
        public double CommissionRate
        {
            get { return fCommissionRate; }
            set { SetPropertyValue<double>("CommissionRate", ref fCommissionRate, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fMarketingRepUserID;
        public string MarketingRepUserID
        {
            get { return fMarketingRepUserID; }
            set { SetPropertyValue<string>("MarketingRepUserID", ref fMarketingRepUserID, value); }
        }
        string fPassword;
        [Size(2000)]
        public string Password
        {
            get { return fPassword; }
            set { SetPropertyValue<string>("Password", ref fPassword, value); }
        }
        string fBankAccountName;
        [Size(500)]
        public string BankAccountName
        {
            get { return fBankAccountName; }
            set { SetPropertyValue<string>("BankAccountName", ref fBankAccountName, value); }
        }
        string fAccount;
        public string Account
        {
            get { return fAccount; }
            set { SetPropertyValue<string>("Account", ref fAccount, value); }
        }
        string fAccountType;
        public string AccountType
        {
            get { return fAccountType; }
            set { SetPropertyValue<string>("AccountType", ref fAccountType, value); }
        }
        string fABA;
        [Size(3000)]
        public string ABA
        {
            get { return fABA; }
            set { SetPropertyValue<string>("ABA", ref fABA, value); }
        }
        string fAccountNumber;
        [Size(3000)]
        public string AccountNumber
        {
            get { return fAccountNumber; }
            set { SetPropertyValue<string>("AccountNumber", ref fAccountNumber, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        string fEOMailingAddress;
        [Size(200)]
        public string EOMailingAddress
        {
            get { return fEOMailingAddress; }
            set { SetPropertyValue<string>("EOMailingAddress", ref fEOMailingAddress, value); }
        }
        string fEOCity;
        public string EOCity
        {
            get { return fEOCity; }
            set { SetPropertyValue<string>("EOCity", ref fEOCity, value); }
        }
        string fEOState;
        [Size(2)]
        public string EOState
        {
            get { return fEOState; }
            set { SetPropertyValue<string>("EOState", ref fEOState, value); }
        }
        string fEOZip;
        [Size(5)]
        public string EOZip
        {
            get { return fEOZip; }
            set { SetPropertyValue<string>("EOZip", ref fEOZip, value); }
        }
        string fEOPolicyNo;
        [Size(25)]
        public string EOPolicyNo
        {
            get { return fEOPolicyNo; }
            set { SetPropertyValue<string>("EOPolicyNo", ref fEOPolicyNo, value); }
        }
        DateTime fEOEffDate;
        public DateTime EOEffDate
        {
            get { return fEOEffDate; }
            set { SetPropertyValue<DateTime>("EOEffDate", ref fEOEffDate, value); }
        }
        DateTime fEOExpDate;
        public DateTime EOExpDate
        {
            get { return fEOExpDate; }
            set { SetPropertyValue<DateTime>("EOExpDate", ref fEOExpDate, value); }
        }
        string fEONotes;
        [Size(8000)]
        public string EONotes
        {
            get { return fEONotes; }
            set { SetPropertyValue<string>("EONotes", ref fEONotes, value); }
        }
        double fLatitude;
        public double Latitude
        {
            get { return fLatitude; }
            set { SetPropertyValue<double>("Latitude", ref fLatitude, value); }
        }
        double fLongitude;
        public double Longitude
        {
            get { return fLongitude; }
            set { SetPropertyValue<double>("Longitude", ref fLongitude, value); }
        }
        string fComparativeRater;
        [Size(50)]
        public string ComparativeRater
        {
            get { return fComparativeRater; }
            set { SetPropertyValue<string>("ComparativeRater", ref fComparativeRater, value); }
        }
        string fAltEmailAddress;
        [Size(200)]
        public string AltEmailAddress
        {
            get { return fAltEmailAddress; }
            set { SetPropertyValue<string>("AltEmailAddress", ref fAltEmailAddress, value); }
        }
        string fAltPassword;
        [Size(2000)]
        public string AltPassword
        {
            get { return fAltPassword; }
            set { SetPropertyValue<string>("AltPassword", ref fAltPassword, value); }
        }

        string fAgentGuid;
        public string AgentGuid
        {
            get { return fAgentGuid; }
            set { SetPropertyValue<string>("AgentGuid", ref fAgentGuid, value); }
        }

        bool _fAllowLowDp;
        public bool AllowLowDp
        {
            get { return _fAllowLowDp; }
            set { SetPropertyValue<bool>("AllowLowDp", ref _fAllowLowDp, value); }
        }
        private bool _fIsRequireCall;
        public bool IsRequireCall
        {
            get { return _fIsRequireCall; }
            set { SetPropertyValue<bool>("IsRequireCall", ref _fIsRequireCall, value); }
        }
        public XpoAgents(Session session) : base(session) { }
        public XpoAgents() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
