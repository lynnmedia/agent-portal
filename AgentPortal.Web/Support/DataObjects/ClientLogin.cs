using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IClientLogin
    {
        int IndexID { get; set; }
        string EmailAddress { get; set; }
        string HashedPassword { get; set; }
        string ValidationCode { get; set; }
        bool IsValidated { get; set; }
        bool IsActive { get; set; }
    }

    public class ClientLogin : IClientLogin
    {
        public int IndexID { get; set; }
        public string EmailAddress { get; set; }
        public string HashedPassword { get; set; }
        public string ValidationCode { get; set; }
        public bool IsValidated { get; set; }
        public bool IsActive { get; set; }
    }

    [Persistent("ClientLogin")]
    public class XpoClientLogin : XPLiteObject, IClientLogin
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fEmailAddress;
        [Size(150)]
        public string EmailAddress
        {
            get { return fEmailAddress; }
            set { SetPropertyValue<string>("EmailAddress", ref fEmailAddress, value); }
        }
        string fHashedPassword;
        [Size(8000)]
        public string HashedPassword
        {
            get { return fHashedPassword; }
            set { SetPropertyValue<string>("HashedPassword", ref fHashedPassword, value); }
        }
        string fValidationCode;
        [Size(36)]
        public string ValidationCode
        {
            get { return fValidationCode; }
            set { SetPropertyValue<string>("ValidationCode", ref fValidationCode, value); }
        }
        bool fIsValidated;
        public bool IsValidated
        {
            get { return fIsValidated; }
            set { SetPropertyValue<bool>("IsValidated", ref fIsValidated, value); }
        }
        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }
        public XpoClientLogin(Session session) : base(session) { }
        public XpoClientLogin() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
