﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAARateSupUW
    {
        int IndexID { get; set; }
        string SupportArea { get; set; }
        string UWArea { get; set; }
        int DownPay { get; set; }
        int PIPCount { get; set; }
        string RateCycle { get; set; }
        string PDFFileName { get; set; }
        DateTime DateCreated { get; set; }
        float PageNo { get; set; }
        bool AllowNewBusiness { get; set; }
    }

    public class AARateSupUW : IAARateSupUW
    {
        public int IndexID { get; set; }
        public string SupportArea { get; set; }
        public string UWArea { get; set; }
        public int DownPay { get; set; }
        public int PIPCount { get; set; }
        public string RateCycle { get; set; }
        public string PDFFileName { get; set; }
        public DateTime DateCreated { get; set; }
        public float PageNo { get; set; }
        public bool AllowNewBusiness { get; set; }
    }

	[Persistent("AARateSupUW")]
    public class XpoAARateSupUW : XPLiteObject, IAARateSupUW
    {

        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>(nameof(IndexID), ref fIndexID, value); }
        }

        string fSupportArea;
        public string SupportArea
        {
            get { return fSupportArea; }
            set { SetPropertyValue<string>(nameof(SupportArea), ref fSupportArea, value); }
        }

        string fUWArea;
        public string UWArea
        {
            get { return fUWArea; }
            set { SetPropertyValue<string>(nameof(UWArea), ref fUWArea, value); }
        }

        int fDownPay;
        public int DownPay
        {
            get { return fDownPay; }
            set { SetPropertyValue<int>(nameof(DownPay), ref fDownPay, value); }
        }

        int fPIPCount;
        public int PIPCount
        {
            get { return fPIPCount; }
            set { SetPropertyValue<int>(nameof(PIPCount), ref fPIPCount, value); }
        }

        string fRateCycle;
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>(nameof(RateCycle), ref fRateCycle, value); }
        }

        string fPDFFileName;
        public string PDFFileName
        {
            get { return fPDFFileName; }
            set { SetPropertyValue<string>(nameof(PDFFileName), ref fPDFFileName, value); }
        }

        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>(nameof(DateCreated), ref fDateCreated, value); }
        }

        float fPageNo;
        public float PageNo
        {
            get { return fPageNo; }
            set { SetPropertyValue<float>(nameof(PageNo), ref fPageNo, value); }
        }

        bool fAllowNewBusiness;
        public bool AllowNewBusiness
        {
            get { return fAllowNewBusiness; }
            set { SetPropertyValue<bool>(nameof(AllowNewBusiness), ref fAllowNewBusiness, value); }
        }

        public XpoAARateSupUW(Session session) : base(session) { }
        public XpoAARateSupUW() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}