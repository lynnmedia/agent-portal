﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("RiskCheckNonOwnedVehicles")]
    public class RiskCheckNonOwnedVehicles : XPLiteObject
    {
        public RiskCheckNonOwnedVehicles(Session session) : base(session) { }
        public RiskCheckNonOwnedVehicles() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        int fId;
        [Key(true)]
        public int Id
        {
            get { return fId; }
            set { SetPropertyValue<int>("Id", ref fId, value); }
        }

        string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        string fVehicleIndex;
        public string VehicleIndex
        {
            get { return fVehicleIndex; }
            set { SetPropertyValue<string>("VehicleIndex", ref fVehicleIndex, value); }
        }

        string fVIN;
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }

        string fYear;
        public string Year
        {
            get { return fYear; }
            set { SetPropertyValue<string>("Year", ref fYear, value); }
        }

        string fMake;
        public string Make
        {
            get { return fMake; }
            set { SetPropertyValue<string>("Make", ref fMake, value); }
        }

        string fModel;
        public string Model
        {
            get { return fModel; }
            set { SetPropertyValue<string>("Model", ref fModel, value); }
        }

        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }

        string fRegisteredOwnerName;
        public string RegisteredOwnerName
        {
            get { return fRegisteredOwnerName; }
            set { SetPropertyValue<string>("RegisteredOwnerName", ref fRegisteredOwnerName, value); }
        }

    }
}