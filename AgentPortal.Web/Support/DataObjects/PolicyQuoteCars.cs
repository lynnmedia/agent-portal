﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyQuoteCars : XPCustomObject
    {
        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fVIN;
        [Size(20)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        int fVehYear;
        public int VehYear
        {
            get { return fVehYear; }
            set { SetPropertyValue<int>("VehYear", ref fVehYear, value); }
        }
        string fVehMake;
        [Size(30)]
        public string VehMake
        {
            get { return fVehMake; }
            set { SetPropertyValue<string>("VehMake", ref fVehMake, value); }
        }
        string fVehModel1;
        [Size(30)]
        public string VehModel1
        {
            get { return fVehModel1; }
            set { SetPropertyValue<string>("VehModel1", ref fVehModel1, value); }
        }
        string fVehModel2;
        [Size(30)]
        public string VehModel2
        {
            get { return fVehModel2; }
            set { SetPropertyValue<string>("VehModel2", ref fVehModel2, value); }
        }
        string fVehDoors;
        [Size(30)]
        public string VehDoors
        {
            get { return fVehDoors; }
            set { SetPropertyValue<string>("VehDoors", ref fVehDoors, value); }
        }
        string fVehCylinders;
        [Size(30)]
        public string VehCylinders
        {
            get { return fVehCylinders; }
            set { SetPropertyValue<string>("VehCylinders", ref fVehCylinders, value); }
        }
        string fVehDriveType;
        [Size(30)]
        public string VehDriveType
        {
            get { return fVehDriveType; }
            set { SetPropertyValue<string>("VehDriveType", ref fVehDriveType, value); }
        }
        int fVINIndex;
        public int VINIndex
        {
            get { return fVINIndex; }
            set { SetPropertyValue<int>("VINIndex", ref fVINIndex, value); }
        }
        int fPrincOpr;
        public int PrincOpr
        {
            get { return fPrincOpr; }
            set { SetPropertyValue<int>("PrincOpr", ref fPrincOpr, value); }
        }
        int fRatedOpr;
        public int RatedOpr
        {
            get { return fRatedOpr; }
            set { SetPropertyValue<int>("RatedOpr", ref fRatedOpr, value); }
        }
        string fRatingClass;
        [Size(20)]
        public string RatingClass
        {
            get { return fRatingClass; }
            set { SetPropertyValue<string>("RatingClass", ref fRatingClass, value); }
        }
        int fTotalPoints;
        public int TotalPoints
        {
            get { return fTotalPoints; }
            set { SetPropertyValue<int>("TotalPoints", ref fTotalPoints, value); }
        }
        string fISOSymbol;
        [Size(5)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fISOPhyDamSymbol;
        [Size(5)]
        public string ISOPhyDamSymbol
        {
            get { return fISOPhyDamSymbol; }
            set { SetPropertyValue<string>("ISOPhyDamSymbol", ref fISOPhyDamSymbol, value); }
        }
        string fBISymbol;
        [Size(5)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(5)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fPIPSymbol;
        [Size(5)]
        public string PIPSymbol
        {
            get { return fPIPSymbol; }
            set { SetPropertyValue<string>("PIPSymbol", ref fPIPSymbol, value); }
        }
        string fUMSymbol;
        [Size(5)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(5)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(5)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(5)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        double fCostNew;
        public double CostNew
        {
            get { return fCostNew; }
            set { SetPropertyValue<double>("CostNew", ref fCostNew, value); }
        }
        int fMilesToWork;
        public int MilesToWork
        {
            get { return fMilesToWork; }
            set { SetPropertyValue<int>("MilesToWork", ref fMilesToWork, value); }
        }
        bool fBusinessUse;
        public bool BusinessUse
        {
            get { return fBusinessUse; }
            set { SetPropertyValue<bool>("BusinessUse", ref fBusinessUse, value); }
        }
        bool fArtisan;
        public bool Artisan
        {
            get { return fArtisan; }
            set { SetPropertyValue<bool>("Artisan", ref fArtisan, value); }
        }
        double fAmountCustom;
        public double AmountCustom
        {
            get { return fAmountCustom; }
            set { SetPropertyValue<double>("AmountCustom", ref fAmountCustom, value); }
        }
        bool fFarmUse;
        public bool FarmUse
        {
            get { return fFarmUse; }
            set { SetPropertyValue<bool>("FarmUse", ref fFarmUse, value); }
        }
        int fHomeDiscAmt;
        public int HomeDiscAmt
        {
            get { return fHomeDiscAmt; }
            set { SetPropertyValue<int>("HomeDiscAmt", ref fHomeDiscAmt, value); }
        }
        string fATD;
        [Size(75)]
        public string ATD
        {
            get { return fATD; }
            set { SetPropertyValue<string>("ATD", ref fATD, value); }
        }
        int fABS;
        public int ABS
        {
            get { return fABS; }
            set { SetPropertyValue<int>("ABS", ref fABS, value); }
        }
        int fARB;
        public int ARB
        {
            get { return fARB; }
            set { SetPropertyValue<int>("ARB", ref fARB, value); }
        }
        int fAPCDiscAmt;
        public int APCDiscAmt
        {
            get { return fAPCDiscAmt; }
            set { SetPropertyValue<int>("APCDiscAmt", ref fAPCDiscAmt, value); }
        }
        int fABSDiscAmt;
        public int ABSDiscAmt
        {
            get { return fABSDiscAmt; }
            set { SetPropertyValue<int>("ABSDiscAmt", ref fABSDiscAmt, value); }
        }
        int fARBDiscAmt;
        public int ARBDiscAmt
        {
            get { return fARBDiscAmt; }
            set { SetPropertyValue<int>("ARBDiscAmt", ref fARBDiscAmt, value); }
        }
        int fMCDiscAmt;
        public int MCDiscAmt
        {
            get { return fMCDiscAmt; }
            set { SetPropertyValue<int>("MCDiscAmt", ref fMCDiscAmt, value); }
        }
        int fRenDiscAmt;
        public int RenDiscAmt
        {
            get { return fRenDiscAmt; }
            set { SetPropertyValue<int>("RenDiscAmt", ref fRenDiscAmt, value); }
        }
        int fTransferDiscAmt;
        public int TransferDiscAmt
        {
            get { return fTransferDiscAmt; }
            set { SetPropertyValue<int>("TransferDiscAmt", ref fTransferDiscAmt, value); }
        }
        int fSSDDiscAmt;
        public int SSDDiscAmt
        {
            get { return fSSDDiscAmt; }
            set { SetPropertyValue<int>("SSDDiscAmt", ref fSSDDiscAmt, value); }
        }
        int fATDDiscAmt;
        public int ATDDiscAmt
        {
            get { return fATDDiscAmt; }
            set { SetPropertyValue<int>("ATDDiscAmt", ref fATDDiscAmt, value); }
        }
        int fDirectRepairDiscAmt;
        public int DirectRepairDiscAmt
        {
            get { return fDirectRepairDiscAmt; }
            set { SetPropertyValue<int>("DirectRepairDiscAmt", ref fDirectRepairDiscAmt, value); }
        }
        int fWLDiscAmt;
        public int WLDiscAmt
        {
            get { return fWLDiscAmt; }
            set { SetPropertyValue<int>("WLDiscAmt", ref fWLDiscAmt, value); }
        }
        int fSDDiscAmt;
        public int SDDiscAmt
        {
            get { return fSDDiscAmt; }
            set { SetPropertyValue<int>("SDDiscAmt", ref fSDDiscAmt, value); }
        }
        int fPointSurgAmt;
        public int PointSurgAmt
        {
            get { return fPointSurgAmt; }
            set { SetPropertyValue<int>("PointSurgAmt", ref fPointSurgAmt, value); }
        }
        int fOOSLicSurgAmt;
        public int OOSLicSurgAmt
        {
            get { return fOOSLicSurgAmt; }
            set { SetPropertyValue<int>("OOSLicSurgAmt", ref fOOSLicSurgAmt, value); }
        }
        int fInexpSurgAmt;
        public int InexpSurgAmt
        {
            get { return fInexpSurgAmt; }
            set { SetPropertyValue<int>("InexpSurgAmt", ref fInexpSurgAmt, value); }
        }
        int fInternationalSurgAmt;
        public int InternationalSurgAmt
        {
            get { return fInternationalSurgAmt; }
            set { SetPropertyValue<int>("InternationalSurgAmt", ref fInternationalSurgAmt, value); }
        }
        int fPriorBalanceSurgAmt;
        public int PriorBalanceSurgAmt
        {
            get { return fPriorBalanceSurgAmt; }
            set { SetPropertyValue<int>("PriorBalanceSurgAmt", ref fPriorBalanceSurgAmt, value); }
        }
        int fNoHitSurgAmt;
        public int NoHitSurgAmt
        {
            get { return fNoHitSurgAmt; }
            set { SetPropertyValue<int>("NoHitSurgAmt", ref fNoHitSurgAmt, value); }
        }
        int fUnacceptSurgAmt;
        public int UnacceptSurgAmt
        {
            get { return fUnacceptSurgAmt; }
            set { SetPropertyValue<int>("UnacceptSurgAmt", ref fUnacceptSurgAmt, value); }
        }
        int fBIWritten;
        public int BIWritten
        {
            get { return fBIWritten; }
            set { SetPropertyValue<int>("BIWritten", ref fBIWritten, value); }
        }
        int fPDWritten;
        public int PDWritten
        {
            get { return fPDWritten; }
            set { SetPropertyValue<int>("PDWritten", ref fPDWritten, value); }
        }
        int fPIPWritten;
        public int PIPWritten
        {
            get { return fPIPWritten; }
            set { SetPropertyValue<int>("PIPWritten", ref fPIPWritten, value); }
        }
        int fMPWritten;
        public int MPWritten
        {
            get { return fMPWritten; }
            set { SetPropertyValue<int>("MPWritten", ref fMPWritten, value); }
        }
        int fUMWritten;
        public int UMWritten
        {
            get { return fUMWritten; }
            set { SetPropertyValue<int>("UMWritten", ref fUMWritten, value); }
        }
        int fCompWritten;
        public int CompWritten
        {
            get { return fCompWritten; }
            set { SetPropertyValue<int>("CompWritten", ref fCompWritten, value); }
        }
        int fCollWritten;
        public int CollWritten
        {
            get { return fCollWritten; }
            set { SetPropertyValue<int>("CollWritten", ref fCollWritten, value); }
        }
        int fBIAnnlPrem;
        public int BIAnnlPrem
        {
            get { return fBIAnnlPrem; }
            set { SetPropertyValue<int>("BIAnnlPrem", ref fBIAnnlPrem, value); }
        }
        int fPDAnnlPrem;
        public int PDAnnlPrem
        {
            get { return fPDAnnlPrem; }
            set { SetPropertyValue<int>("PDAnnlPrem", ref fPDAnnlPrem, value); }
        }
        int fPIPAnnlPrem;
        public int PIPAnnlPrem
        {
            get { return fPIPAnnlPrem; }
            set { SetPropertyValue<int>("PIPAnnlPrem", ref fPIPAnnlPrem, value); }
        }
        int fMPAnnlPrem;
        public int MPAnnlPrem
        {
            get { return fMPAnnlPrem; }
            set { SetPropertyValue<int>("MPAnnlPrem", ref fMPAnnlPrem, value); }
        }
        int fUMAnnlPrem;
        public int UMAnnlPrem
        {
            get { return fUMAnnlPrem; }
            set { SetPropertyValue<int>("UMAnnlPrem", ref fUMAnnlPrem, value); }
        }
        int fCompAnnlPrem;
        public int CompAnnlPrem
        {
            get { return fCompAnnlPrem; }
            set { SetPropertyValue<int>("CompAnnlPrem", ref fCompAnnlPrem, value); }
        }
        int fCollAnnlPrem;
        public int CollAnnlPrem
        {
            get { return fCollAnnlPrem; }
            set { SetPropertyValue<int>("CollAnnlPrem", ref fCollAnnlPrem, value); }
        }
        string fCompDed;
        [Size(10)]
        public string CompDed
        {
            get { return fCompDed; }
            set { SetPropertyValue<string>("CompDed", ref fCompDed, value); }
        }
        string fCollDed;
        [Size(10)]
        public string CollDed
        {
            get { return fCollDed; }
            set { SetPropertyValue<string>("CollDed", ref fCollDed, value); }
        }
        bool fConvTT;
        public bool ConvTT
        {
            get { return fConvTT; }
            set { SetPropertyValue<bool>("ConvTT", ref fConvTT, value); }
        }
        bool fPurchNew;
        public bool PurchNew
        {
            get { return fPurchNew; }
            set { SetPropertyValue<bool>("PurchNew", ref fPurchNew, value); }
        }
        string fDamage;
        public string Damage
        {
            get { return fDamage; }
            set { SetPropertyValue<string>("Damage", ref fDamage, value); }
        }
        bool fHasComp;
        public bool HasComp
        {
            get { return fHasComp; }
            set { SetPropertyValue<bool>("HasComp", ref fHasComp, value); }
        }
        bool fHasColl;
        public bool HasColl
        {
            get { return fHasColl; }
            set { SetPropertyValue<bool>("HasColl", ref fHasColl, value); }
        }
        bool fMultiCar;
        public bool MultiCar
        {
            get { return fMultiCar; }
            set { SetPropertyValue<bool>("MultiCar", ref fMultiCar, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        bool fIs4wd;
        public bool Is4wd
        {
            get { return fIs4wd; }
            set { SetPropertyValue<bool>("Is4wd", ref fIs4wd, value); }
        }
        bool fIsDRL;
        public bool IsDRL
        {
            get { return fIsDRL; }
            set { SetPropertyValue<bool>("IsDRL", ref fIsDRL, value); }
        }
        bool fIsIneligible;
        public bool IsIneligible
        {
            get { return fIsIneligible; }
            set { SetPropertyValue<bool>("IsIneligible", ref fIsIneligible, value); }
        }
        bool fIsHybrid;
        public bool IsHybrid
        {
            get { return fIsHybrid; }
            set { SetPropertyValue<bool>("IsHybrid", ref fIsHybrid, value); }
        }
        bool fisValid;
        public bool isValid
        {
            get { return fisValid; }
            set { SetPropertyValue<bool>("isValid", ref fisValid, value); }
        }
        bool fHasPhyDam;
        public bool HasPhyDam
        {
            get { return fHasPhyDam; }
            set { SetPropertyValue<bool>("HasPhyDam", ref fHasPhyDam, value); }
        }
        string fVSRCollSymbol;
        [Size(5)]
        public string VSRCollSymbol
        {
            get { return fVSRCollSymbol; }
            set { SetPropertyValue<string>("VSRCollSymbol", ref fVSRCollSymbol, value); }
        }
        double fStatedAmtCost;
        public double StatedAmtCost
        {
            get { return fStatedAmtCost; }
            set { SetPropertyValue<double>("StatedAmtCost", ref fStatedAmtCost, value); }
        }
        double fStatedAmtAnnlPrem;
        public double StatedAmtAnnlPrem
        {
            get { return fStatedAmtAnnlPrem; }
            set { SetPropertyValue<double>("StatedAmtAnnlPrem", ref fStatedAmtAnnlPrem, value); }
        }
        bool fIsForceSymbols;
        public bool IsForceSymbols
        {
            get { return fIsForceSymbols; }
            set { SetPropertyValue<bool>("IsForceSymbols", ref fIsForceSymbols, value); }
        }
        string engineType;
        [Size(5)]
        public string EngineType
        {
            get { return engineType; }
            set { SetPropertyValue<string>("EngineType", ref engineType, value); }
        }
        string fGrossVehicleWeight;
        public string GrossVehicleWeight
        {
            get { return fGrossVehicleWeight; }
            set { SetPropertyValue<string>("GrossVehicleWeight", ref fGrossVehicleWeight, value); }
        }

        string fClassCode;
        public string ClassCode
        {
            get { return fClassCode; }
            set { SetPropertyValue<string>("TonnageIndicator", ref fClassCode, value); }
        }

        string msrpUpperBound;
        public string MSRPUpperBound
        {
            get { return msrpUpperBound; }
            set { SetPropertyValue<string>("TonnageIMSRPUpperBoundndicator", ref msrpUpperBound, value); }
        }

        DateTime? fDatePurchased;
        public DateTime? DatePurchased
        {
            get { return fDatePurchased; }
            set { SetPropertyValue<DateTime?>("DatePurchased", ref fDatePurchased, value); }
        }

        public PolicyQuoteCars(Session session) : base(session) { }
        public PolicyQuoteCars() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}