using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("VinLookup")]
    public class VinLookupDO : XPLiteObject
    {
        string fVinNo;
        [Size(10)]
        public string VinNo
        {
            get { return fVinNo; }
            set { SetPropertyValue<string>("VinNo", ref fVinNo, value); }
        }
        string fIncrement;
        [Size(15)]
        public string Increment
        {
            get { return fIncrement; }
            set { SetPropertyValue<string>("Increment", ref fIncrement, value); }
        }
        int fVinID;
        public int VinID
        {
            get { return fVinID; }
            set { SetPropertyValue<int>("VinID", ref fVinID, value); }
        }
        string fxxYear;
        [Size(5)]
        public string xxYear
        {
            get { return fxxYear; }
            set { SetPropertyValue<string>("xxYear", ref fxxYear, value); }
        }
        string fChangeIndicator;
        [Size(1)]
        public string ChangeIndicator
        {
            get { return fChangeIndicator; }
            set { SetPropertyValue<string>("ChangeIndicator", ref fChangeIndicator, value); }
        }
        string fEffective;
        [Size(4)]
        public string Effective
        {
            get { return fEffective; }
            set { SetPropertyValue<string>("Effective", ref fEffective, value); }
        }
        string fStateException;
        [Size(2)]
        public string StateException
        {
            get { return fStateException; }
            set { SetPropertyValue<string>("StateException", ref fStateException, value); }
        }
        string fManufacturer;
        [Size(4)]
        public string Manufacturer
        {
            get { return fManufacturer; }
            set { SetPropertyValue<string>("Manufacturer", ref fManufacturer, value); }
        }
        string fFullName;
        [Size(15)]
        public string FullName
        {
            get { return fFullName; }
            set { SetPropertyValue<string>("FullName", ref fFullName, value); }
        }
        string fAbbrModelName;
        [Size(10)]
        public string AbbrModelName
        {
            get { return fAbbrModelName; }
            set { SetPropertyValue<string>("AbbrModelName", ref fAbbrModelName, value); }
        }
        string fBodyStyle;
        [Size(8)]
        public string BodyStyle
        {
            get { return fBodyStyle; }
            set { SetPropertyValue<string>("BodyStyle", ref fBodyStyle, value); }
        }
        string fEngineSize;
        [Size(3)]
        public string EngineSize
        {
            get { return fEngineSize; }
            set { SetPropertyValue<string>("EngineSize", ref fEngineSize, value); }
        }
        string fNoCylinders;
        [Size(2)]
        public string NoCylinders
        {
            get { return fNoCylinders; }
            set { SetPropertyValue<string>("NoCylinders", ref fNoCylinders, value); }
        }
        string fFourWheelDrive;
        [Size(1)]
        public string FourWheelDrive
        {
            get { return fFourWheelDrive; }
            set { SetPropertyValue<string>("FourWheelDrive", ref fFourWheelDrive, value); }
        }
        string fRestraintInfo;
        [Size(4)]
        public string RestraintInfo
        {
            get { return fRestraintInfo; }
            set { SetPropertyValue<string>("RestraintInfo", ref fRestraintInfo, value); }
        }
        string fTransmission;
        [Size(4)]
        public string Transmission
        {
            get { return fTransmission; }
            set { SetPropertyValue<string>("Transmission", ref fTransmission, value); }
        }
        string fAntiLockBrake;
        [Size(1)]
        public string AntiLockBrake
        {
            get { return fAntiLockBrake; }
            set { SetPropertyValue<string>("AntiLockBrake", ref fAntiLockBrake, value); }
        }
        string fDRL;
        [Size(1)]
        public string DRL
        {
            get { return fDRL; }
            set { SetPropertyValue<string>("DRL", ref fDRL, value); }
        }
        string fEngineType;
        [Size(1)]
        public string EngineType
        {
            get { return fEngineType; }
            set { SetPropertyValue<string>("EngineType", ref fEngineType, value); }
        }
        string fISOSymbol;
        [Size(2)]
        public string ISOSymbol
        {
            get { return fISOSymbol; }
            set { SetPropertyValue<string>("ISOSymbol", ref fISOSymbol, value); }
        }
        string fNonISOSymbol;
        [Size(2)]
        public string NonISOSymbol
        {
            get { return fNonISOSymbol; }
            set { SetPropertyValue<string>("NonISOSymbol", ref fNonISOSymbol, value); }
        }
        string fMiscBodyInfo;
        [Size(4)]
        public string MiscBodyInfo
        {
            get { return fMiscBodyInfo; }
            set { SetPropertyValue<string>("MiscBodyInfo", ref fMiscBodyInfo, value); }
        }
        string fMiscEngineInfo;
        [Size(4)]
        public string MiscEngineInfo
        {
            get { return fMiscEngineInfo; }
            set { SetPropertyValue<string>("MiscEngineInfo", ref fMiscEngineInfo, value); }
        }
        string fMiscRestraintInfo;
        [Size(4)]
        public string MiscRestraintInfo
        {
            get { return fMiscRestraintInfo; }
            set { SetPropertyValue<string>("MiscRestraintInfo", ref fMiscRestraintInfo, value); }
        }
        string fFullModelName;
        [Size(19)]
        public string FullModelName
        {
            get { return fFullModelName; }
            set { SetPropertyValue<string>("FullModelName", ref fFullModelName, value); }
        }
        string fAntiTheft;
        [Size(1)]
        public string AntiTheft
        {
            get { return fAntiTheft; }
            set { SetPropertyValue<string>("AntiTheft", ref fAntiTheft, value); }
        }
        string fEligible;
        [Size(1)]
        public string Eligible
        {
            get { return fEligible; }
            set { SetPropertyValue<string>("Eligible", ref fEligible, value); }
        }
        int fVSRSymbol;
        public int VSRSymbol
        {
            get { return fVSRSymbol; }
            set { SetPropertyValue<int>("VSRSymbol", ref fVSRSymbol, value); }
        }
        int fPriceNewSymbol;
        public int PriceNewSymbol
        {
            get { return fPriceNewSymbol; }
            set { SetPropertyValue<int>("PriceNewSymbol", ref fPriceNewSymbol, value); }
        }
        string fBISymbol;
        [Size(2)]
        public string BISymbol
        {
            get { return fBISymbol; }
            set { SetPropertyValue<string>("BISymbol", ref fBISymbol, value); }
        }
        string fPDSymbol;
        [Size(2)]
        public string PDSymbol
        {
            get { return fPDSymbol; }
            set { SetPropertyValue<string>("PDSymbol", ref fPDSymbol, value); }
        }
        string fCOMPSymbol;
        [Size(2)]
        public string COMPSymbol
        {
            get { return fCOMPSymbol; }
            set { SetPropertyValue<string>("COMPSymbol", ref fCOMPSymbol, value); }
        }
        string fCOLLSymbol;
        [Size(2)]
        public string COLLSymbol
        {
            get { return fCOLLSymbol; }
            set { SetPropertyValue<string>("COLLSymbol", ref fCOLLSymbol, value); }
        }
        string fUMSymbol;
        [Size(2)]
        public string UMSymbol
        {
            get { return fUMSymbol; }
            set { SetPropertyValue<string>("UMSymbol", ref fUMSymbol, value); }
        }
        string fMPSymbol;
        [Size(2)]
        public string MPSymbol
        {
            get { return fMPSymbol; }
            set { SetPropertyValue<string>("MPSymbol", ref fMPSymbol, value); }
        }
        int fIDMIVehNo;
        public int IDMIVehNo
        {
            get { return fIDMIVehNo; }
            set { SetPropertyValue<int>("IDMIVehNo", ref fIDMIVehNo, value); }
        }
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        public VinLookupDO(Session session) : base(session) { }
        public VinLookupDO() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
