﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("UnacceptableRisk")]
    public class UnacceptableRiskDO : XPLiteObject
    {

        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }

        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        string fFirstName;
        [Size(30)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }

        string fLastName;
        [Size(30)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }

        string fNameSuffix;
        [Size(10)]
        public string NameSuffix
        {
            get { return fNameSuffix; }
            set { SetPropertyValue<string>("NameSuffix", ref fNameSuffix, value); }
        }

        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }

        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }

        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }

        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }

        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }

        string fLicenseState;
        [Size(2)]
        public string LicenseState
        {
            get { return fLicenseState; }
            set { SetPropertyValue<string>("LicenseState", ref fLicenseState, value); }
        }

        bool fInternationalLicense;
        public bool InternationalLicense
        {
            get { return fInternationalLicense; }
            set { SetPropertyValue<bool>("InternationalLicense", ref fInternationalLicense, value); }
        }

        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }

        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }

        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }

        public UnacceptableRiskDO(Session session) : base(session) { }
        public UnacceptableRiskDO() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }
}