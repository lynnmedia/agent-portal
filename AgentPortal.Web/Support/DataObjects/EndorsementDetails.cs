using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IEndorsementDetails
    {
        int IndexID { get; set; }
        string PolicyNo { get; set; }
        DateTime EndorsementDate { get; set; }
        string EndorsementReason { get; set; }
        string AgentEndorsementNotes { get; set; }
        int DisallowedEndorseQuoteId { get; set; }
    }

    public class EndorsementDetails : IEndorsementDetails
    {
        public int IndexID { get; set; }
        public string PolicyNo { get; set; }
        public DateTime EndorsementDate { get; set; }
        public string EndorsementReason { get; set; }
        public string AgentEndorsementNotes { get; set; }
        public int DisallowedEndorseQuoteId { get; set; }
    }

    [Persistent("EndorsementDetails")]
    public class XpoEndorsementDetails : XPLiteObject, IEndorsementDetails
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
		DateTime fEndorsementDate;
        public DateTime EndorsementDate
        {
            get { return fEndorsementDate; }
            set { SetPropertyValue<DateTime>("EndorsementDate", ref fEndorsementDate, value); }
        }
        string fEndorsementReason;
        [Size(1000)]
        public string EndorsementReason
        {
            get { return fEndorsementReason; }
            set { SetPropertyValue<string>("EndorsementReason", ref fEndorsementReason, value); }
        }
		string fAgentEndorsementNotes;
        [Size(1001)]
        public string AgentEndorsementNotes
        {
            get { return fAgentEndorsementNotes; }
            set { SetPropertyValue<string>("AgentEndorsementNotes", ref fAgentEndorsementNotes, value); }
        }
        int fDisallowedEndorseQuoteId;
        public int DisallowedEndorseQuoteId
        {
            get { return fDisallowedEndorseQuoteId; }
            set { SetPropertyValue<int>("DisallowedEndorseQuoteId", ref fDisallowedEndorseQuoteId, value); }
        }

        public XpoEndorsementDetails(Session session) : base(session) { }
        public XpoEndorsementDetails() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


    public class AuditAutoREIForPostACHTrans : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        DateTime fPaidDate;
        public DateTime PaidDate
        {
            get { return fPaidDate; }
            set { SetPropertyValue<DateTime>("PaidDate", ref fPaidDate, value); }
        }
        string fPaymentType;
        [Size(10)]
        public string PaymentType
        {
            get { return fPaymentType; }
            set { SetPropertyValue<string>("PaymentType", ref fPaymentType, value); }
        }
        double fPayAmt;
        public double PayAmt
        {
            get { return fPayAmt; }
            set { SetPropertyValue<double>("PayAmt", ref fPayAmt, value); }
        }
        string fPymntBatchGuid;
        [Size(50)]
        public string PymntBatchGuid
        {
            get { return fPymntBatchGuid; }
            set { SetPropertyValue<string>("PymntBatchGuid", ref fPymntBatchGuid, value); }
        }
        string fUserName;
        [Size(50)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        public AuditAutoREIForPostACHTrans(Session session) : base(session) { }
        public AuditAutoREIForPostACHTrans() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
