﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditUndAlerts
    {
        int IndexID { get; set; }
        string AlertLocation { get; set; }
        string Msg { get; set; }
        string ClearedBy { get; set; }
        DateTime DateModified { get; set; }
        DateTime DateCreated { get; set; }
        bool IsActive { get; set; }
    }

    public class AuditUndAlerts : IAuditUndAlerts
    {
        public int IndexID { get; set; }
        public string AlertLocation { get; set; }
        public string Msg { get; set; }
        public string ClearedBy { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsActive { get; set; }
    }

    [Persistent("AuditUndAlerts")]
    public class XpoAuditUndAlerts : XPLiteObject, IAuditUndAlerts
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fAlertLocation;
        [Size(200)]
        public string AlertLocation
        {
            get { return fAlertLocation; }
            set { SetPropertyValue<string>("AlertLocation", ref fAlertLocation, value); }
        }
        string fMsg;
        [Size(1000)]
        public string Msg
        {
            get { return fMsg; }
            set { SetPropertyValue<string>("Msg", ref fMsg, value); }
        }
        string fClearedBy;
        public string ClearedBy
        {
            get { return fClearedBy; }
            set { SetPropertyValue<string>("ClearedBy", ref fClearedBy, value); }
        }
        DateTime fDateModified;
        public DateTime DateModified
        {
            get { return fDateModified; }
            set { SetPropertyValue<DateTime>("DateModified", ref fDateModified, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }
        public XpoAuditUndAlerts(Session session) : base(session) { }
        public XpoAuditUndAlerts() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
