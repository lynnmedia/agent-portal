using DevExpress.Xpo;
using System;

namespace AgentPortal.Support.DataObjects
{

    public class PolicyQuoteCarLienHolders : XPLiteObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fVIN;
        [Size(25)]
        public string VIN
        {
            get { return fVIN; }
            set { SetPropertyValue<string>("VIN", ref fVIN, value); }
        }
        string fName;
        public string Name
        {
            get { return fName; }
            set { SetPropertyValue<string>("Name", ref fName, value); }
        }
        string fAddress;
        public string Address
        {
            get { return fAddress; }
            set { SetPropertyValue<string>("Address", ref fAddress, value); }
        }
        string fCity;
        public string City
        {
            get { return fCity; }
            set { SetPropertyValue<string>("City", ref fCity, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fZip;
        [Size(25)]
        public string Zip
        {
            get { return fZip; }
            set { SetPropertyValue<string>("Zip", ref fZip, value); }
        }
        string fPhone;
        [Size(20)]
        public string Phone
        {
            get { return fPhone; }
            set { SetPropertyValue<string>("Phone", ref fPhone, value); }
        }
        bool fisAddInterest;
        public bool isAddInterest
        {
            get { return fisAddInterest; }
            set { SetPropertyValue<bool>("isAddInterest", ref fisAddInterest, value); }
        }

        bool fIsNonInstitutional;
        public bool IsNonInstitutional
        {
            get { return fIsNonInstitutional; }
            set { SetPropertyValue<bool>("IsNonInstitutional", ref fIsNonInstitutional, value); }
        }

        int fLienHolderIndex;
        public int LienHolderIndex
        {
            get { return fLienHolderIndex; }
            set { SetPropertyValue<int>("LienHolderIndex", ref fLienHolderIndex, value); }
        }
        int fCarIndex;
        public int CarIndex
        {
            get { return fCarIndex; }
            set { SetPropertyValue<int>("CarIndex", ref fCarIndex, value); }
        }
        DateTime? fDateCreated;
        public DateTime? DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime?>("DateCreated", ref fDateCreated, value); }
        }
        public PolicyQuoteCarLienHolders(Session session) : base(session) { }
        public PolicyQuoteCarLienHolders() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

   }
