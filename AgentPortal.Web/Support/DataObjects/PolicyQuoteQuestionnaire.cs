﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyQuoteQuestionnaire : XPCustomObject
    {
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }

        string fPolicyNo;
        [Size(30)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        bool fhasDependent;
        public bool hasDependent
        {
            get { return fhasDependent; }
            set { SetPropertyValue<bool>("hasDependent", ref fhasDependent, value); }
        }

        bool fhasFloridaResident;
        public bool hasFloridaResident
        {
            get { return fhasFloridaResident; }
            set { SetPropertyValue<bool>("hasFloridaResident", ref fhasFloridaResident, value); }
        }


        bool fhasDrivingImpairments;
        public bool hasDrivingImpairments
        {
            get { return fhasDrivingImpairments; }
            set { SetPropertyValue<bool>("hasDrivingImpairments", ref fhasDrivingImpairments, value); }
        }



        bool fhasVehicleOwned;
        public bool hasVehicleOwned
        {
            get { return fhasVehicleOwned; }
            set { SetPropertyValue<bool>("hasVehicleOwned", ref fhasVehicleOwned, value); }
        }

        string fhasVehicleOwnedRemarkOne;
        [Size(8000)]
        public string hasVehicleOwnedRemarkOne
        {
            get { return fhasVehicleOwnedRemarkOne; }
            set { SetPropertyValue<string>("hasVehicleOwnedRemarkOne", ref fhasVehicleOwnedRemarkOne, value); }
        }

        bool fhasCoownerResident;
        public bool hasCoownerResident
        {
            get { return fhasCoownerResident; }
            set { SetPropertyValue<bool>("hasCoownerResident", ref fhasCoownerResident, value); }
        }

        string fhasCoownerResidentRemarkOne;
        [Size(8000)]
        public string hasCoownerResidentRemarkOne
        {
            get { return fhasCoownerResidentRemarkOne; }
            set { SetPropertyValue<string>("hasCoownerResidentRemarkOne", ref fhasCoownerResidentRemarkOne, value); }
        }

        //string fhasCoownerResidentRemarkTwo;
        //[Size(8000)]
        //public string hasCoownerResidentRemarkTwo
        //{
        //    get { return fhasCoownerResidentRemarkTwo; }
        //    set { SetPropertyValue<string>("hasCoownerResidentRemarkTwo", ref fhasCoownerResidentRemarkTwo, value); }
        //}

        bool fhasVehicleNotGaraged;
        public bool hasVehicleNotGaraged
        {
            get { return fhasVehicleNotGaraged; }
            set { SetPropertyValue<bool>("hasVehicleNotGaraged", ref fhasVehicleNotGaraged, value); }
        }

        bool fhasExistingDamage;
        public bool hasExistingDamage
        {
            get { return fhasExistingDamage; }
            set { SetPropertyValue<bool>("hasExistingDamage", ref fhasExistingDamage, value); }
        }

        string fhasExistingDamageRemarkOne;
        [Size(8000)]
        public string hasExistingDamageRemarkOne
        {
            get { return fhasExistingDamageRemarkOne; }
            set { SetPropertyValue<string>("hasExistingDamageRemarkOne", ref fhasExistingDamageRemarkOne, value); }
        }

        //string fhasExistingDamageRemarkTwo;
        //[Size(8000)]
        //public string hasExistingDamageRemarkTwo
        //{
        //    get { return fhasExistingDamageRemarkTwo; }
        //    set { SetPropertyValue<string>("hasExistingDamageRemarkTwo", ref fhasExistingDamageRemarkTwo, value); }
        //}


        bool fhasGrayMarketVehicle;
        public bool hasGrayMarketVehicle
        {
            get { return fhasGrayMarketVehicle; }
            set { SetPropertyValue<bool>("hasGrayMarketVehicle", ref fhasGrayMarketVehicle, value); }
        }

        bool fhasAutoInsuranceCancelledorPIPClaim;
        public bool hasAutoInsuranceCancelledorPIPClaim
        {
            get { return fhasAutoInsuranceCancelledorPIPClaim; }
            set { SetPropertyValue<bool>("hasAutoInsuranceCancelledorPIPClaim", ref fhasAutoInsuranceCancelledorPIPClaim, value); }
        }

        string fhasAutoInsuranceCancelledorPIPClaimRemarkOne;
        [Size(8000)]
        public string hasAutoInsuranceCancelledorPIPClaimRemarkOne
        {
            get { return fhasAutoInsuranceCancelledorPIPClaimRemarkOne; }
            set { SetPropertyValue<string>("hasAutoInsuranceCancelledorPIPClaimRemarkOne", ref fhasAutoInsuranceCancelledorPIPClaimRemarkOne, value); }
        }

        bool fhasCriminalActivity;
        public bool hasCriminalActivity
        {
            get { return fhasCriminalActivity; }
            set { SetPropertyValue<bool>("hasCriminalActivity", ref fhasCriminalActivity, value); }
        }

        string fhasCriminalActivityRemarkOne;
        [Size(8000)]
        public string hasCriminalActivityRemarkOne
        {
            get { return fhasCriminalActivityRemarkOne; }
            set { SetPropertyValue<string>("hasCriminalActivityRemarkOne", ref fhasCriminalActivityRemarkOne, value); }
        }

        string fhasCriminalActivityRemarkTwo;
        [Size(8000)]
        public string hasCriminalActivityRemarkTwo
        {
            get { return fhasCriminalActivityRemarkTwo; }
            set { SetPropertyValue<string>("hasCriminalActivityRemarkTwo", ref fhasCriminalActivityRemarkTwo, value); }
        }

        string fhasCriminalActivityRemarkThree;
        [Size(8000)]
        public string hasCriminalActivityRemarkThree
        {
            get { return fhasCriminalActivityRemarkThree; }
            set { SetPropertyValue<string>("hasCriminalActivityRemarkThree", ref fhasCriminalActivityRemarkThree, value); }
        }
        bool fhasVehicleForBusinessPurpose;
        public bool hasVehicleForBusinessPurpose
        {
            get { return fhasVehicleForBusinessPurpose; }
            set { SetPropertyValue<bool>("hasVehicleForBusinessPurpose", ref fhasVehicleForBusinessPurpose, value); }
        }

        bool fhasReportedBusinessUse;
        public bool hasReportedBusinessUse
        {
            get { return fhasReportedBusinessUse; }
            set { SetPropertyValue<bool>("hasReportedBusinessUse", ref fhasReportedBusinessUse, value); }
        }



        public PolicyQuoteQuestionnaire(Session session) : base(session) { }
        public PolicyQuoteQuestionnaire() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}