﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAAPolScorePolDetail
    {
        int IndexID { get; set; }
        string QuoteNo { get; set; }
        string PolicyNo { get; set; }
        string PolScoreStep { get; set; }
        string PolScoreBand { get; set; }
        int PolicyScore { get; set; }
        bool AllowNewBusiness { get; set; }
        DateTime ChangeDate { get; set; }
        DateTime AddDate { get; set; }
    }

    public class AAPolScorePolDetail : IAAPolScorePolDetail
    {
        public int IndexID { get; set; }
        public string QuoteNo { get; set; }
        public string PolicyNo { get; set; }
        public string PolScoreStep { get; set; }
        public string PolScoreBand { get; set; }
        public int PolicyScore { get; set; }
        public bool AllowNewBusiness { get; set; }
        public DateTime ChangeDate { get; set; }
        public DateTime AddDate { get; set; }
    }

    [Persistent("AAPolScorePolDetail")]
    public class XpoAAPolScorePolDetail : XPLiteObject, IAAPolScorePolDetail
    {
        public XpoAAPolScorePolDetail(Session session) : base(session)
        {
        }

        private int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get => fIndexID;
            set => SetPropertyValue<int>(nameof(IndexID), ref fIndexID, value);
        }

        private string fQuoteNo;
        public string QuoteNo
        {
            get { return fQuoteNo; }
            set { SetPropertyValue<string>(nameof(QuoteNo), ref fQuoteNo, value); }
        }

        private string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>(nameof(PolicyNo), ref fPolicyNo, value); }
        }

        private string fPolScoreStep;
        public string PolScoreStep
        {
            get { return fPolScoreStep; }
            set { SetPropertyValue<string>(nameof(PolScoreStep), ref fPolScoreStep, value); }
        }

        private string fPolScoreBand;
        public string PolScoreBand
        {
            get { return fPolScoreBand; }
            set { SetPropertyValue<string>(nameof(PolScoreBand), ref fPolScoreBand, value); }
        }

        private int fPolicyScore;
        public int PolicyScore
        {
            get { return fPolicyScore; }
            set { SetPropertyValue<int>(nameof(PolicyScore), ref fPolicyScore, value); }
        }

        private bool fAllowNewBusiness;
        public bool AllowNewBusiness
        {
            get { return fAllowNewBusiness; }
            set { SetPropertyValue<bool>(nameof(AllowNewBusiness), ref fAllowNewBusiness, value); }
        }

        private DateTime fChangeDate;
        public DateTime ChangeDate
        {
            get { return fChangeDate; }
            set { SetPropertyValue<DateTime>(nameof(ChangeDate), ref fChangeDate, value); }
        }

        private DateTime fAddDate;
        public DateTime AddDate
        {
            get { return fAddDate; }
            set { SetPropertyValue<DateTime>(nameof(AddDate), ref fAddDate, value); }
        }

    }
}