﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyQuoteNotAssociatedDrivers : XPCustomObject
    {
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }

        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }

        DateTime fDateTimeCreated;
        public DateTime DateTimeCreated
        {
            get { return fDateTimeCreated; }
            set { SetPropertyValue<DateTime>("DateTimeCreated", ref fDateTimeCreated, value); }
        }

        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }

        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }

        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }

        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }

        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }

        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }

        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }

        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }

        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }

        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }

        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
      
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }

        string fEducationLevel;
        [Size(500)]
        public string EducationLevel
        {
            get { return fEducationLevel; }
            set { SetPropertyValue<string>("EducationLevel", ref fEducationLevel, value); }
        }

        bool fNotAssociatedDriver;
        public bool NotAssociatedDriver
        {
            get { return fNotAssociatedDriver; }
            set { SetPropertyValue<bool>("NotAssociatedDriver", ref fNotAssociatedDriver, value); }
        }

        string fAgentCodeCreatedBy;
        [Size(500)]
        public string AgentCodeCreatedBy
        {
            get { return fAgentCodeCreatedBy; }
            set { SetPropertyValue<string>("AgentIdCreatedBy", ref fAgentCodeCreatedBy, value); }
        }

        string fNADConfirmedAgentId;
        [Size(500)]
        public string NADConfirmedAgentId
        {
            get { return fNADConfirmedAgentId; }
            set { SetPropertyValue<string>("NADConfirmedAgentId", ref fNADConfirmedAgentId, value); }
        }

        DateTime fNADConfirmedDateTime;
        public DateTime NADConfirmedDateTime
        {
            get { return fNADConfirmedDateTime; }
            set { SetPropertyValue<DateTime>("NADConfirmedDateTime", ref fNADConfirmedDateTime, value); }
        }

        string fViolationDesc;
        [Size(3000)]
        public string ViolationDesc
        {
            get { return fViolationDesc; }
            set { SetPropertyValue<string>("ViolationDesc", ref fViolationDesc, value); }
        }

        string fActionMessage;
        [Size(3000)]
        public string ActionMessage
        {
            get { return fActionMessage; }
            set { SetPropertyValue<string>("ActionMessage", ref fActionMessage, value); }
        }

        bool fFromRCPOS;
        public bool FromRCPOS
        {
            get { return fFromRCPOS; }
            set { SetPropertyValue<bool>("FromRCPOS", ref fFromRCPOS, value); }
        }

        string fNADReason;
        [Size(100)]
        public string NADReason
        {
            get { return fNADReason; }
            set { SetPropertyValue<string>("NADReason", ref fNADReason, value); }
        }
        public PolicyQuoteNotAssociatedDrivers(Session session) : base(session) { }
        public PolicyQuoteNotAssociatedDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}