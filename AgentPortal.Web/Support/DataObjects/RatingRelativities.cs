﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("RatingRelativities")]
    public class RatingRelativitiesDo : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fMainIndexID;
        public int MainIndexID
        {
            get { return fMainIndexID; }
            set { SetPropertyValue<int>("MainIndexID", ref fMainIndexID, value); }
        }
        int fVehIndexID;
        public int VehIndexID
        {
            get { return fVehIndexID; }
            set { SetPropertyValue<int>("VehIndexID", ref fVehIndexID, value); }
        }
        int fOprIndexID;
        public int OprIndexID
        {
            get { return fOprIndexID; }
            set { SetPropertyValue<int>("OprIndexID", ref fOprIndexID, value); }
        }
        string fPolicyNo;
        [Size(25)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fVehicleNo;
        public int VehicleNo
        {
            get { return fVehicleNo; }
            set { SetPropertyValue<int>("VehicleNo", ref fVehicleNo, value); }
        }
        int _fRatedOperatorNo;
        public int RatedOperatorNo
        {
            get { return _fRatedOperatorNo; }
            set { SetPropertyValue<int>("RatedOperatorNo", ref _fRatedOperatorNo, value); }
        }
        string fTableName;
        [Size(50)]
        public string TableName
        {
            get { return fTableName; }
            set { SetPropertyValue<string>("TableName", ref fTableName, value); }
        }
        int fTableIndexID;
        public int TableIndexID
        {
            get { return fTableIndexID; }
            set { SetPropertyValue<int>("TableIndexID", ref fTableIndexID, value); }
        }
        int fRateOrderNo;
        public int RateOrderNo
        {
            get { return fRateOrderNo; }
            set { SetPropertyValue<int>("RateOrderNo", ref fRateOrderNo, value); }
        }
        string fMathAction;
        [Size(5)]
        public string MathAction
        {
            get { return fMathAction; }
            set { SetPropertyValue<string>("MathAction", ref fMathAction, value); }
        }
        bool fRoundStep;
        public bool RoundStep
        {
            get { return fRoundStep; }
            set { SetPropertyValue<bool>("RoundStep", ref fRoundStep, value); }
        }
        bool fIncludeHousehold;
        public bool IncludeHousehold
        {
            get { return fIncludeHousehold; }
            set { SetPropertyValue<bool>("IncludeHousehold", ref fIncludeHousehold, value); }
        }
        string fRelativity;
        [Size(50)]
        public string Relativity
        {
            get { return fRelativity; }
            set { SetPropertyValue<string>("Relativity", ref fRelativity, value); }
        }
        double fBI;
        public double BI
        {
            get { return fBI; }
            set { SetPropertyValue<double>("BI", ref fBI, value); }
        }
        double fPIPNI;
        public double PIPNI
        {
            get { return fPIPNI; }
            set { SetPropertyValue<double>("PIPNI", ref fPIPNI, value); }
        }
        double fPIPNR;
        public double PIPNR
        {
            get { return fPIPNR; }
            set { SetPropertyValue<double>("PIPNR", ref fPIPNR, value); }
        }
        double fPD;
        public double PD
        {
            get { return fPD; }
            set { SetPropertyValue<double>("PD", ref fPD, value); }
        }
        double fUM;
        public double UM
        {
            get { return fUM; }
            set { SetPropertyValue<double>("UM", ref fUM, value); }
        }
        double fUMS;
        public double UMS
        {
            get { return fUMS; }
            set { SetPropertyValue<double>("UMS", ref fUMS, value); }
        }
        double fMP;
        public double MP
        {
            get { return fMP; }
            set { SetPropertyValue<double>("MP", ref fMP, value); }
        }
        double fCOMP;
        public double COMP
        {
            get { return fCOMP; }
            set { SetPropertyValue<double>("COMP", ref fCOMP, value); }
        }
        double fCOLL;
        public double COLL
        {
            get { return fCOLL; }
            set { SetPropertyValue<double>("COLL", ref fCOLL, value); }
        }
        double fSE;
        public double SE
        {
            get { return fSE; }
            set { SetPropertyValue<double>("SE", ref fSE, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        int fPageNo;
        public int PageNo
        {
            get { return fPageNo; }
            set { SetPropertyValue<int>("PageNo", ref fPageNo, value); }
        }

        public RatingRelativitiesDo()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public RatingRelativitiesDo(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }

}