﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditWorkFlowDO
    {
        int IndexID { get; set; }
        int ImageIndexID { get; set; }
        string FileNo { get; set; }
        int Claimant { get; set; }
        string Dept { get; set; }
        string FormType { get; set; }
        string FormDesc { get; set; }
        DateTime DateCreated { get; set; }
        string CreatedBy { get; set; }
        string AssignedTo { get; set; }
        DateTime DateProcessed { get; set; }
        string WFClassification { get; set; }
        bool WFProcessed { get; set; }
    }

    public class AuditWorkFlowDO : IAuditWorkFlowDO
    {
        public int IndexID { get; set; }
        public int ImageIndexID { get; set; }
        public string FileNo { get; set; }
        public int Claimant { get; set; }
        public string Dept { get; set; }
        public string FormType { get; set; }
        public string FormDesc { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }
        public DateTime DateProcessed { get; set; }
        public string WFClassification { get; set; }
        public bool WFProcessed { get; set; }
    }

    [Persistent("AuditWorkFlowDO")]
    public class XpoAuditWorkFlowDO : XPLiteObject, IAuditWorkFlowDO
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        int fImageIndexID;
        public int ImageIndexID
        {
            get { return fImageIndexID; }
            set { SetPropertyValue<int>("ImageIndexID", ref fImageIndexID, value); }
        }
        string fFileNo;
        [Size(20)]
        public string FileNo
        {
            get { return fFileNo; }
            set { SetPropertyValue<string>("FileNo", ref fFileNo, value); }
        }
        int fClaimant;
        public int Claimant
        {
            get { return fClaimant; }
            set { SetPropertyValue<int>("Claimant", ref fClaimant, value); }
        }
        string fDept;
        [Size(25)]
        public string Dept
        {
            get { return fDept; }
            set { SetPropertyValue<string>("Dept", ref fDept, value); }
        }
        string fFormType;
        [Size(10)]
        public string FormType
        {
            get { return fFormType; }
            set { SetPropertyValue<string>("FormType", ref fFormType, value); }
        }
        string fFormDesc;
        [Size(2000)]
        public string FormDesc
        {
            get { return fFormDesc; }
            set { SetPropertyValue<string>("FormDesc", ref fFormDesc, value); }
        }        
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        string fCreatedBy;
        [Size(50)]
        public string CreatedBy
        {
            get { return fCreatedBy; }
            set { SetPropertyValue<string>("CreatedBy", ref fCreatedBy, value); }
        }
        string fAssignedTo;
        [Size(200)]
        public string AssignedTo
        {
            get { return fAssignedTo; }
            set { SetPropertyValue<string>("AssignedTo", ref fAssignedTo, value); }
        }
        DateTime fDateProcessed;
        public DateTime DateProcessed
        {
            get { return fDateProcessed; }
            set { SetPropertyValue<DateTime>("DateProcessed", ref fDateProcessed, value); }
        }
        string fWFClassification;
        [Size(500)]
        public string WFClassification
        {
            get { return fWFClassification; }
            set { SetPropertyValue<string>("WFClassification", ref fWFClassification, value); }
        }
        bool fWFProcessed;
        public bool WFProcessed
        {
            get { return fWFProcessed; }
            set { SetPropertyValue<bool>("WFProcessed", ref fWFProcessed, value); }
        }
        public XpoAuditWorkFlowDO(Session session) : base(session) { }
        public XpoAuditWorkFlowDO() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
