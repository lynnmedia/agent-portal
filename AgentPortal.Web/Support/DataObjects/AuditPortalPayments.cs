﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditPortalPaymentsDo
    {
        int IndexID { get; set; }
        string Username { get; set; }
        string PymntLocation { get; set; }
        string PymntType { get; set; }
        double TotalAmount { get; set; }
        double RenAmt { get; set; }
        double PriorAmt { get; set; }
        string PymntBatchGuid { get; set; }
        bool IsSweep { get; set; }
        bool IsCyberSource { get; set; }
        bool IsRenewal { get; set; }
        DateTime DateCreated { get; set; }
        double ModelAmt { get; set; }
        bool IsFirstData { get; set; }
    }

    public class AuditPortalPaymentsDo : IAuditPortalPaymentsDo
    {
        public int IndexID { get; set; }
        public string Username { get; set; }
        public string PymntLocation { get; set; }
        public string PymntType { get; set; }
        public double TotalAmount { get; set; }
        public double RenAmt { get; set; }
        public double PriorAmt { get; set; }
        public string PymntBatchGuid { get; set; }
        public bool IsSweep { get; set; }
        public bool IsCyberSource { get; set; }
        public bool IsRenewal { get; set; }
        public DateTime DateCreated { get; set; }
        public double ModelAmt { get; set; }
        public bool IsFirstData { get; set; }
    }

    [Persistent("AuditPortalPayments")]
    public class XpoAuditPortalPaymentsDo : XPLiteObject, IAuditPortalPaymentsDo
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        [Size(200)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fPymntLocation;
        [Size(200)]
        public string PymntLocation
        {
            get { return fPymntLocation; }
            set { SetPropertyValue<string>("PymntLocation", ref fPymntLocation, value); }
        }
        string fPymntType;
        public string PymntType
        {
            get { return fPymntType; }
            set { SetPropertyValue<string>("PymntType", ref fPymntType, value); }
        }
        double fTotalAmount;
        public double TotalAmount
        {
            get { return fTotalAmount; }
            set { SetPropertyValue<double>("TotalAmount", ref fTotalAmount, value); }
        }
        double fRenAmt;
        public double RenAmt
        {
            get { return fRenAmt; }
            set { SetPropertyValue<double>("RenAmt", ref fRenAmt, value); }
        }
        double fPriorAmt;
        public double PriorAmt
        {
            get { return fPriorAmt; }
            set { SetPropertyValue<double>("PriorAmt", ref fPriorAmt, value); }
        }
        string fPymntBatchGuid;
        [Size(50)]
        public string PymntBatchGuid
        {
            get { return fPymntBatchGuid; }
            set { SetPropertyValue<string>("PymntBatchGuid", ref fPymntBatchGuid, value); }
        }
        bool fIsSweep;
        public bool IsSweep
        {
            get { return fIsSweep; }
            set { SetPropertyValue<bool>("IsSweep", ref fIsSweep, value); }
        }
        bool fIsCyberSource;
        public bool IsCyberSource
        {
            get { return fIsCyberSource; }
            set { SetPropertyValue<bool>("IsCyberSource", ref fIsCyberSource, value); }
        }
        bool fIsRenewal;
        public bool IsRenewal
        {
            get { return fIsRenewal; }
            set { SetPropertyValue<bool>("IsRenewal", ref fIsRenewal, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }

        private double _fModelAmt;
        public double ModelAmt
        {
            get { return _fModelAmt; }
            set { SetPropertyValue<double>("ModelAmt", ref _fModelAmt, value); }
        }

        private bool _fIsFirstData;
        public bool IsFirstData
        {
            get { return _fIsFirstData; }
            set { SetPropertyValue<bool>("IsFirstData", ref _fIsFirstData, value); }
        }

        public XpoAuditPortalPaymentsDo(Session session) : base(session) { }
        public XpoAuditPortalPaymentsDo() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
