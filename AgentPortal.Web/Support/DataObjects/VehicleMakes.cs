﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;
//using DevExpress.XtraCharts.Native;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("VehicleMakes")]
    public class VehicleMakes : XPLiteObject
    {
        public VehicleMakes(Session session) : base(session) { }
        public VehicleMakes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }

        string fMake;
        [Size(500)]
        public string Make
        {
            get { return fMake; }
            set { SetPropertyValue<string>("Make", ref fMake, value); }
        }

        string fISOCode;
        [Size(500)]
        public string ISOCode
        {
            get { return fISOCode; }
            set { SetPropertyValue<string>("ISOCode", ref fISOCode, value); }
        }

        string fNCICCode;
        [Size(500)]
        public string NCICCode
        {
            get { return fNCICCode; }
            set { SetPropertyValue<string>("NCICCode", ref fNCICCode, value); }
        }

        bool fIsActive;
        public bool IsActive
        {
            get { return fIsActive; }
            set { SetPropertyValue<bool>("IsActive", ref fIsActive, value); }
        }
    }
}