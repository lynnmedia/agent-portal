﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyESignatureRequests : XPLiteObject
    {

        int fIndexID;
        [Key(true)]
        public int IndexId
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexId", ref fIndexID, value); }
        }

        string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }

        public PolicyESignatureRequests(Session session) : base(session) { }
        public PolicyESignatureRequests() : base(Session.DefaultSession) { }

        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}