using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IDemoPortalLogins
    {
        int IndexID { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int LoginCount { get; set; }
    }

    public class DemoPortalLogins : IDemoPortalLogins
    {
        public int IndexID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int LoginCount { get; set; }
    }

    [Persistent("DemoPortalLogins")]
    public class XpoDemoPortalLogins : XPCustomObject, IDemoPortalLogins
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUserName;
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }

        string fPassword;
        public string Password
        {
            get { return fPassword; }
            set { SetPropertyValue<string>("Password", ref fPassword, value); }
        }
        int fLoginCount;
        public int LoginCount
        {
            get { return fLoginCount; }
            set { SetPropertyValue<int>("LoginCount", ref fLoginCount, value); }
        }

        public XpoDemoPortalLogins()
            : base()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public XpoDemoPortalLogins(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }

}