using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IActivityLog
    {
        int IndexID { get; set; }
        string Application { get; set; }
        string Username { get; set; }
        string ActivityType { get; set; }
        string ActivityDesc { get; set; }
        DateTime ActivityDate { get; set; }
    }
    public class ActivityLog : IActivityLog
    {
        public int IndexID { get; set; }
        public string Application { get; set; }
        public string Username { get; set; }
        public string ActivityType { get; set; }
        public string ActivityDesc { get; set; }
        public DateTime ActivityDate { get; set; }
    }

    [Persistent("ActivityLog")]
    public class XpoActivityLog : XPLiteObject, IActivityLog
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fApplication;
        [Size(50)]
        public string Application
        {
            get { return fApplication; }
            set { SetPropertyValue<string>("Application", ref fApplication, value); }
        }
        string fUsername;
        [Size(200)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fActivityType;
        [Size(200)]
        public string ActivityType
        {
            get { return fActivityType; }
            set { SetPropertyValue<string>("ActivityType", ref fActivityType, value); }
        }
        string fActivityDesc;
        [Size(8000)]
        public string ActivityDesc
        {
            get { return fActivityDesc; }
            set { SetPropertyValue<string>("ActivityDesc", ref fActivityDesc, value); }
        }
        DateTime fActivityDate;
        public DateTime ActivityDate
        {
            get { return fActivityDate; }
            set { SetPropertyValue<DateTime>("ActivityDate", ref fActivityDate, value); }
        }
        public XpoActivityLog(Session session) : base(session) { }
        public XpoActivityLog() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
