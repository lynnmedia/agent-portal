using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IPolicy
    {
        string PolicyNo { get; set; }
        string PriorPolicyNo { get; set; }
        string NextPolicyNo { get; set; }
        int RenCount { get; set; }
        string State { get; set; }
        string LOB { get; set; }
        DateTime EffDate { get; set; }
        DateTime ExpDate { get; set; }
        string PolicyStatus { get; set; }
        string RenewalStatus { get; set; }
        DateTime DateBound { get; set; }
        DateTime DateIssued { get; set; }
        DateTime CancelDate { get; set; }
        string CancelType { get; set; }
        string Ins1First { get; set; }
        string Ins1Last { get; set; }
        string Ins1MI { get; set; }
        string Ins1Suffix { get; set; }
        string Ins1FullName { get; set; }
        string Ins2First { get; set; }
        string Ins2Last { get; set; }
        string Ins2MI { get; set; }
        string Ins2Suffix { get; set; }
        string Ins2FullName { get; set; }
        string Territory { get; set; }
        double PolicyWritten { get; set; }
        double PolicyAnnualized { get; set; }
        int CommPrem { get; set; }
        int DBSetupFee { get; set; }
        int PolicyFee { get; set; }
        int SR22Fee { get; set; }
        double FHCFFee { get; set; }
        string RateCycle { get; set; }
        bool SixMonth { get; set; }
        int PayPlan { get; set; }
        bool PaidInFullDisc { get; set; }
        bool EFT { get; set; }
        string MailStreet { get; set; }
        string MailCity { get; set; }
        string MailState { get; set; }
        string MailZip { get; set; }
        bool MailGarageSame { get; set; }
        string GarageStreet { get; set; }
        string GarageCity { get; set; }
        string GarageState { get; set; }
        string GarageZip { get; set; }
        string GarageTerritory { get; set; }
        string GarageCounty { get; set; }
        string HomePhone { get; set; }
        string WorkPhone { get; set; }
        string MainEmail { get; set; }
        string AltEmail { get; set; }
        bool Paperless { get; set; }
        bool AdvancedQuote { get; set; }
        string AgentCode { get; set; }
        double CommAtIssue { get; set; }
        double LOUCommAtIssue { get; set; }
        double ADNDCommAtIssue { get; set; }
        bool AgentGross { get; set; }
        string PIPDed { get; set; }
        bool NIO { get; set; }
        bool NIRR { get; set; }
        bool UMStacked { get; set; }
        bool HasBI { get; set; }
        bool HasMP { get; set; }
        bool HasUM { get; set; }
        bool HasLOU { get; set; }
        string BILimit { get; set; }
        string PDLimit { get; set; }
        string UMLimit { get; set; }
        string MedPayLimit { get; set; }
        bool Homeowner { get; set; }
        bool RenDisc { get; set; }
        int TransDisc { get; set; }
        bool HasPPO { get; set; }
        string PreviousCompany { get; set; }
        DateTime PreviousExpDate { get; set; }
        bool PreviousBI { get; set; }
        bool PreviousBalance { get; set; }
        bool DirectRepairDisc { get; set; }
        string UndTier { get; set; }
        bool OOSEnd { get; set; }
        int LOUCost { get; set; }
        int LOUAnnlPrem { get; set; }
        int ADNDLimit { get; set; }
        int ADNDCost { get; set; }
        int ADNDAnnlPrem { get; set; }
        bool HoldRtn { get; set; }
        bool NonOwners { get; set; }
        string ChangeGUID { get; set; }
        int policyCars { get; set; }
        int policyDrivers { get; set; }
        bool Unacceptable { get; set; }
        int HousholdIndex { get; set; }
        int RatingID { get; set; }
        int OID { get; set; }
        int Cars { get; set; }
        int Drivers { get; set; }
        bool IsReturnedMail { get; set; }
        bool IsOnHold { get; set; }
        bool IsClaimMisRep { get; set; }
        bool IsNoREI { get; set; }
        bool IsSuspense { get; set; }
        bool isAnnual { get; set; }
        string Carrier { get; set; }
        bool HasPriorCoverage { get; set; }
        bool HasPriorBalance { get; set; }
        bool HasLapseNone { get; set; }
        bool HasPD { get; set; }
        bool HasTOW { get; set; }
        string PIPLimit { get; set; }
        bool HasADND { get; set; }
        int MVRFee { get; set; }
        int WorkLoss { get; set; }
        double RentalAnnlPrem { get; set; }
        double RentalCost { get; set; }
        bool HasLapse110 { get; set; }
        bool HasLapse1131 { get; set; }
        double QuotedAmount { get; set; }
        string AppQuestionAnswers { get; set; }
        string AppQuestionExplainOne { get; set; }
        string AppQuestionExplainTwo { get; set; }
        bool FromPortal { get; set; }
        string FromRater { get; set; }
        string QuoteNo { get; set; }
        string AgencyRep { get; set; }
        DateTime QuotedDate { get; set; }
        bool Bridged { get; set; }
        DateTime MinDuePayDate { get; set; }
        bool IsAllowRenPastExp { get; set; }
        bool? IsShow { get; set; }
        bool AllowPymntWithin15Days { get; set; }
        double MaintenanceFee { get; set; }
        double PIPPDOnlyFee { get; set; }
        double RewriteFee { get; set; }
        string AgentESignEmailAddress { get; set; }
    }

    public class Policy : IPolicy
    {
        public string PolicyNo { get; set; }
        public string PriorPolicyNo { get; set; }
        public string NextPolicyNo { get; set; }
        public int RenCount { get; set; }
        public string State { get; set; }
        public string LOB { get; set; }
        public DateTime EffDate { get; set; }
        public DateTime ExpDate { get; set; }
        public string PolicyStatus { get; set; }
        public string RenewalStatus { get; set; }
        public DateTime DateBound { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime CancelDate { get; set; }
        public string CancelType { get; set; }
        public string Ins1First { get; set; }
        public string Ins1Last { get; set; }
        public string Ins1MI { get; set; }
        public string Ins1Suffix { get; set; }
        public string Ins1FullName { get; set; }
        public string Ins2First { get; set; }
        public string Ins2Last { get; set; }
        public string Ins2MI { get; set; }
        public string Ins2Suffix { get; set; }
        public string Ins2FullName { get; set; }
        public string Territory { get; set; }
        public double PolicyWritten { get; set; }
        public double PolicyAnnualized { get; set; }
        public int CommPrem { get; set; }
        public int DBSetupFee { get; set; }
        public int PolicyFee { get; set; }
        public int SR22Fee { get; set; }
        public double FHCFFee { get; set; }
        public string RateCycle { get; set; }
        public bool SixMonth { get; set; }
        public int PayPlan { get; set; }
        public bool PaidInFullDisc { get; set; }
        public bool EFT { get; set; }
        public string MailStreet { get; set; }
        public string MailCity { get; set; }
        public string MailState { get; set; }
        public string MailZip { get; set; }
        public bool MailGarageSame { get; set; }
        public string GarageStreet { get; set; }
        public string GarageCity { get; set; }
        public string GarageState { get; set; }
        public string GarageZip { get; set; }
        public string GarageTerritory { get; set; }
        public string GarageCounty { get; set; }
        public string HomePhone { get; set; }
        public string WorkPhone { get; set; }
        public string MainEmail { get; set; }
        public string AltEmail { get; set; }
        public bool Paperless { get; set; }
        public bool AdvancedQuote { get; set; }
        public string AgentCode { get; set; }
        public double CommAtIssue { get; set; }
        public double LOUCommAtIssue { get; set; }
        public double ADNDCommAtIssue { get; set; }
        public bool AgentGross { get; set; }
        public string PIPDed { get; set; }
        public bool NIO { get; set; }
        public bool NIRR { get; set; }
        public bool UMStacked { get; set; }
        public bool HasBI { get; set; }
        public bool HasMP { get; set; }
        public bool HasUM { get; set; }
        public bool HasLOU { get; set; }
        public string BILimit { get; set; }
        public string PDLimit { get; set; }
        public string UMLimit { get; set; }
        public string MedPayLimit { get; set; }
        public bool Homeowner { get; set; }
        public bool RenDisc { get; set; }
        public int TransDisc { get; set; }
        public bool HasPPO { get; set; }
        public string PreviousCompany { get; set; }
        public DateTime PreviousExpDate { get; set; }
        public bool PreviousBI { get; set; }
        public bool PreviousBalance { get; set; }
        public bool DirectRepairDisc { get; set; }
        public string UndTier { get; set; }
        public bool OOSEnd { get; set; }
        public int LOUCost { get; set; }
        public int LOUAnnlPrem { get; set; }
        public int ADNDLimit { get; set; }
        public int ADNDCost { get; set; }
        public int ADNDAnnlPrem { get; set; }
        public bool HoldRtn { get; set; }
        public bool NonOwners { get; set; }
        public string ChangeGUID { get; set; }
        public int policyCars { get; set; }
        public int policyDrivers { get; set; }
        public bool Unacceptable { get; set; }
        public int HousholdIndex { get; set; }
        public int RatingID { get; set; }
        public int OID { get; set; }
        public int Cars { get; set; }
        public int Drivers { get; set; }
        public bool IsReturnedMail { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsClaimMisRep { get; set; }
        public bool IsNoREI { get; set; }
        public bool IsSuspense { get; set; }
        public bool isAnnual { get; set; }
        public string Carrier { get; set; }
        public bool HasPriorCoverage { get; set; }
        public bool HasPriorBalance { get; set; }
        public bool HasLapseNone { get; set; }
        public bool HasPD { get; set; }
        public bool HasTOW { get; set; }
        public string PIPLimit { get; set; }
        public bool HasADND { get; set; }
        public int MVRFee { get; set; }
        public int WorkLoss { get; set; }
        public double RentalAnnlPrem { get; set; }
        public double RentalCost { get; set; }
        public bool HasLapse110 { get; set; }
        public bool HasLapse1131 { get; set; }
        public double QuotedAmount { get; set; }
        public string AppQuestionAnswers { get; set; }
        public string AppQuestionExplainOne { get; set; }
        public string AppQuestionExplainTwo { get; set; }
        public bool FromPortal { get; set; }
        public string FromRater { get; set; }
        public string QuoteNo { get; set; }
        public string AgencyRep { get; set; }
        public DateTime QuotedDate { get; set; }
        public bool Bridged { get; set; }
        public DateTime MinDuePayDate { get; set; }
        public bool IsAllowRenPastExp { get; set; }
        public bool? IsShow { get; set; }
        public bool AllowPymntWithin15Days { get; set; }
        public double MaintenanceFee { get; set; }
        public double PIPPDOnlyFee { get; set; }
        public double RewriteFee { get; set; }
        public string AgentESignEmailAddress { get; set; }
    }

    [Persistent("Policy")]
    public class XpoPolicy : XPCustomObject, IPolicy
    {
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPriorPolicyNo;
        [Size(150)]
        public string PriorPolicyNo
        {
            get { return fPriorPolicyNo; }
            set { SetPropertyValue<string>("PriorPolicyNo", ref fPriorPolicyNo, value); }
        }
        string fNextPolicyNo;
        [Size(15)]
        public string NextPolicyNo
        {
            get { return fNextPolicyNo; }
            set { SetPropertyValue<string>("NextPolicyNo", ref fNextPolicyNo, value); }
        }
        int fRenCount;
        public int RenCount
        {
            get { return fRenCount; }
            set { SetPropertyValue<int>("RenCount", ref fRenCount, value); }
        }
        string fState;
        [Size(5)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fLOB;
        [Size(5)]
        public string LOB
        {
            get { return fLOB; }
            set { SetPropertyValue<string>("LOB", ref fLOB, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        string fPolicyStatus;
        public string PolicyStatus
        {
            get { return fPolicyStatus; }
            set { SetPropertyValue<string>("PolicyStatus", ref fPolicyStatus, value); }
        }
        string fRenewalStatus;
        [Size(5)]
        public string RenewalStatus
        {
            get { return fRenewalStatus; }
            set { SetPropertyValue<string>("RenewalStatus", ref fRenewalStatus, value); }
        }
        DateTime fDateBound;
        public DateTime DateBound
        {
            get { return fDateBound; }
            set { SetPropertyValue<DateTime>("DateBound", ref fDateBound, value); }
        }
        DateTime fDateIssued;
        public DateTime DateIssued
        {
            get { return fDateIssued; }
            set { SetPropertyValue<DateTime>("DateIssued", ref fDateIssued, value); }
        }
        DateTime fCancelDate;
        public DateTime CancelDate
        {
            get { return fCancelDate; }
            set { SetPropertyValue<DateTime>("CancelDate", ref fCancelDate, value); }
        }
        string fCancelType;
        [Size(30)]
        public string CancelType
        {
            get { return fCancelType; }
            set { SetPropertyValue<string>("CancelType", ref fCancelType, value); }
        }
        string fIns1First;
        [Size(30)]
        public string Ins1First
        {
            get { return fIns1First; }
            set { SetPropertyValue<string>("Ins1First", ref fIns1First, value); }
        }
        string fIns1Last;
        [Size(30)]
        public string Ins1Last
        {
            get { return fIns1Last; }
            set { SetPropertyValue<string>("Ins1Last", ref fIns1Last, value); }
        }
        string fIns1MI;
        [Size(1)]
        public string Ins1MI
        {
            get { return fIns1MI; }
            set { SetPropertyValue<string>("Ins1MI", ref fIns1MI, value); }
        }
        string fIns1Suffix;
        [Size(10)]
        public string Ins1Suffix
        {
            get { return fIns1Suffix; }
            set { SetPropertyValue<string>("Ins1Suffix", ref fIns1Suffix, value); }
        }
        string fIns1FullName;
        [Size(80)]
        public string Ins1FullName
        {
            get { return fIns1FullName; }
            set { SetPropertyValue<string>("Ins1FullName", ref fIns1FullName, value); }
        }
        string fIns2First;
        [Size(30)]
        public string Ins2First
        {
            get { return fIns2First; }
            set { SetPropertyValue<string>("Ins2First", ref fIns2First, value); }
        }
        string fIns2Last;
        [Size(30)]
        public string Ins2Last
        {
            get { return fIns2Last; }
            set { SetPropertyValue<string>("Ins2Last", ref fIns2Last, value); }
        }
        string fIns2MI;
        [Size(1)]
        public string Ins2MI
        {
            get { return fIns2MI; }
            set { SetPropertyValue<string>("Ins2MI", ref fIns2MI, value); }
        }
        string fIns2Suffix;
        [Size(10)]
        public string Ins2Suffix
        {
            get { return fIns2Suffix; }
            set { SetPropertyValue<string>("Ins2Suffix", ref fIns2Suffix, value); }
        }
        string fIns2FullName;
        [Size(80)]
        public string Ins2FullName
        {
            get { return fIns2FullName; }
            set { SetPropertyValue<string>("Ins2FullName", ref fIns2FullName, value); }
        }
        string fTerritory;
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>("Territory", ref fTerritory, value); }
        }
        double fPolicyWritten;
        public double PolicyWritten
        {
            get { return fPolicyWritten; }
            set { SetPropertyValue<double>("PolicyWritten", ref fPolicyWritten, value); }
        }
        double fPolicyAnnualized;
        public double PolicyAnnualized
        {
            get { return fPolicyAnnualized; }
            set { SetPropertyValue<double>("PolicyAnnualized", ref fPolicyAnnualized, value); }
        }
        int fCommPrem;
        public int CommPrem
        {
            get { return fCommPrem; }
            set { SetPropertyValue<int>("CommPrem", ref fCommPrem, value); }
        }
        int fDBSetupFee;
        public int DBSetupFee
        {
            get { return fDBSetupFee; }
            set { SetPropertyValue<int>("DBSetupFee", ref fDBSetupFee, value); }
        }
        int fPolicyFee;
        public int PolicyFee
        {
            get { return fPolicyFee; }
            set { SetPropertyValue<int>("PolicyFee", ref fPolicyFee, value); }
        }
        int fSR22Fee;
        public int SR22Fee
        {
            get { return fSR22Fee; }
            set { SetPropertyValue<int>("SR22Fee", ref fSR22Fee, value); }
        }
        double fFHCFFee;
        public double FHCFFee
        {
            get { return fFHCFFee; }
            set { SetPropertyValue<double>("FHCFFee", ref fFHCFFee, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        bool fSixMonth;
        public bool SixMonth
        {
            get { return fSixMonth; }
            set { SetPropertyValue<bool>("SixMonth", ref fSixMonth, value); }
        }
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        string fMailStreet;
        [Size(35)]
        public string MailStreet
        {
            get { return fMailStreet; }
            set { SetPropertyValue<string>("MailStreet", ref fMailStreet, value); }
        }
        string fMailCity;
        [Size(20)]
        public string MailCity
        {
            get { return fMailCity; }
            set { SetPropertyValue<string>("MailCity", ref fMailCity, value); }
        }
        string fMailState;
        [Size(2)]
        public string MailState
        {
            get { return fMailState; }
            set { SetPropertyValue<string>("MailState", ref fMailState, value); }
        }
        string fMailZip;
        [Size(10)]
        public string MailZip
        {
            get { return fMailZip; }
            set { SetPropertyValue<string>("MailZip", ref fMailZip, value); }
        }
        bool fMailGarageSame;
        public bool MailGarageSame
        {
            get { return fMailGarageSame; }
            set { SetPropertyValue<bool>("MailGarageSame", ref fMailGarageSame, value); }
        }
        string fGarageStreet;
        [Size(35)]
        public string GarageStreet
        {
            get { return fGarageStreet; }
            set { SetPropertyValue<string>("GarageStreet", ref fGarageStreet, value); }
        }
        string fGarageCity;
        [Size(20)]
        public string GarageCity
        {
            get { return fGarageCity; }
            set { SetPropertyValue<string>("GarageCity", ref fGarageCity, value); }
        }
        string fGarageState;
        [Size(2)]
        public string GarageState
        {
            get { return fGarageState; }
            set { SetPropertyValue<string>("GarageState", ref fGarageState, value); }
        }
        string fGarageZip;
        [Size(10)]
        public string GarageZip
        {
            get { return fGarageZip; }
            set { SetPropertyValue<string>("GarageZip", ref fGarageZip, value); }
        }
        string fGarageTerritory;
        public string GarageTerritory
        {
            get { return fGarageTerritory; }
            set { SetPropertyValue<string>("GarageTerritory", ref fGarageTerritory, value); }
        }
        string fGarageCounty;
        [Size(20)]
        public string GarageCounty
        {
            get { return fGarageCounty; }
            set { SetPropertyValue<string>("GarageCounty", ref fGarageCounty, value); }
        }
        string fHomePhone;
        [Size(14)]
        public string HomePhone
        {
            get { return fHomePhone; }
            set { SetPropertyValue<string>("HomePhone", ref fHomePhone, value); }
        }
        string fWorkPhone;
        [Size(14)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        string fMainEmail;
        [Size(150)]
        public string MainEmail
        {
            get { return fMainEmail; }
            set { SetPropertyValue<string>("MainEmail", ref fMainEmail, value); }
        }
        string fAltEmail;
        [Size(150)]
        public string AltEmail
        {
            get { return fAltEmail; }
            set { SetPropertyValue<string>("AltEmail", ref fAltEmail, value); }
        }
        bool fPaperless;
        public bool Paperless
        {
            get { return fPaperless; }
            set { SetPropertyValue<bool>("Paperless", ref fPaperless, value); }
        }

        private bool fAdvancedQuote;
        public bool AdvancedQuote
        {
            get { return fAdvancedQuote; }
            set { SetPropertyValue<bool>("AdvancedQuote", ref fAdvancedQuote, value); }
        }
        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }
        double fCommAtIssue;
        public double CommAtIssue
        {
            get { return fCommAtIssue; }
            set { SetPropertyValue<double>("CommAtIssue", ref fCommAtIssue, value); }
        }
        double fLOUCommAtIssue;
        public double LOUCommAtIssue
        {
            get { return fLOUCommAtIssue; }
            set { SetPropertyValue<double>("LOUCommAtIssue", ref fLOUCommAtIssue, value); }
        }
        double fADNDCommAtIssue;
        public double ADNDCommAtIssue
        {
            get { return fADNDCommAtIssue; }
            set { SetPropertyValue<double>("ADNDCommAtIssue", ref fADNDCommAtIssue, value); }
        }
        bool fAgentGross;
        public bool AgentGross
        {
            get { return fAgentGross; }
            set { SetPropertyValue<bool>("AgentGross", ref fAgentGross, value); }
        }
        string fPIPDed;
        [Size(4)]
        public string PIPDed
        {
            get { return fPIPDed; }
            set { SetPropertyValue<string>("PIPDed", ref fPIPDed, value); }
        }
        bool fNIO;
        public bool NIO
        {
            get { return fNIO; }
            set { SetPropertyValue<bool>("NIO", ref fNIO, value); }
        }
        bool fNIRR;
        public bool NIRR
        {
            get { return fNIRR; }
            set { SetPropertyValue<bool>("NIRR", ref fNIRR, value); }
        }
        bool fUMStacked;
        public bool UMStacked
        {
            get { return fUMStacked; }
            set { SetPropertyValue<bool>("UMStacked", ref fUMStacked, value); }
        }
        bool fHasBI;
        public bool HasBI
        {
            get { return fHasBI; }
            set { SetPropertyValue<bool>("HasBI", ref fHasBI, value); }
        }
        bool fHasMP;
        public bool HasMP
        {
            get { return fHasMP; }
            set { SetPropertyValue<bool>("HasMP", ref fHasMP, value); }
        }
        bool fHasUM;
        public bool HasUM
        {
            get { return fHasUM; }
            set { SetPropertyValue<bool>("HasUM", ref fHasUM, value); }
        }
        bool fHasLOU;
        public bool HasLOU
        {
            get { return fHasLOU; }
            set { SetPropertyValue<bool>("HasLOU", ref fHasLOU, value); }
        }
        string fBILimit;
        [Size(10)]
        public string BILimit
        {
            get { return fBILimit; }
            set { SetPropertyValue<string>("BILimit", ref fBILimit, value); }
        }
        string fPDLimit;
        [Size(10)]
        public string PDLimit
        {
            get { return fPDLimit; }
            set { SetPropertyValue<string>("PDLimit", ref fPDLimit, value); }
        }
        string fUMLimit;
        [Size(10)]
        public string UMLimit
        {
            get { return fUMLimit; }
            set { SetPropertyValue<string>("UMLimit", ref fUMLimit, value); }
        }
        string fMedPayLimit;
        [Size(10)]
        public string MedPayLimit
        {
            get { return fMedPayLimit; }
            set { SetPropertyValue<string>("MedPayLimit", ref fMedPayLimit, value); }
        }
        bool fHomeowner;
        public bool Homeowner
        {
            get { return fHomeowner; }
            set { SetPropertyValue<bool>("Homeowner", ref fHomeowner, value); }
        }
        bool fRenDisc;
        public bool RenDisc
        {
            get { return fRenDisc; }
            set { SetPropertyValue<bool>("RenDisc", ref fRenDisc, value); }
        }
        int fTransDisc;
        public int TransDisc
        {
            get { return fTransDisc; }
            set { SetPropertyValue<int>("TransDisc", ref fTransDisc, value); }
        }
        bool hasPpo;
        public bool HasPPO
        {
            get { return hasPpo; }
            set { SetPropertyValue<bool>("HasPPO", ref hasPpo, value); }
        }
        string fPreviousCompany;
        [Size(500)]
        public string PreviousCompany
        {
            get { return fPreviousCompany; }
            set { SetPropertyValue<string>("PreviousCompany", ref fPreviousCompany, value); }
        }
        DateTime fPreviousExpDate;
        public DateTime PreviousExpDate
        {
            get { return fPreviousExpDate; }
            set { SetPropertyValue<DateTime>("PreviousExpDate", ref fPreviousExpDate, value); }
        }
        bool fPreviousBI;
        public bool PreviousBI
        {
            get { return fPreviousBI; }
            set { SetPropertyValue<bool>("PreviousBI", ref fPreviousBI, value); }
        }
        bool fPreviousBalance;
        public bool PreviousBalance
        {
            get { return fPreviousBalance; }
            set { SetPropertyValue<bool>("PreviousBalance", ref fPreviousBalance, value); }
        }
        bool fDirectRepairDisc;
        public bool DirectRepairDisc
        {
            get { return fDirectRepairDisc; }
            set { SetPropertyValue<bool>("DirectRepairDisc", ref fDirectRepairDisc, value); }
        }
        string fUndTier;
        [Size(5)]
        public string UndTier
        {
            get { return fUndTier; }
            set { SetPropertyValue<string>("UndTier", ref fUndTier, value); }
        }
        bool fOOSEnd;
        public bool OOSEnd
        {
            get { return fOOSEnd; }
            set { SetPropertyValue<bool>("OOSEnd", ref fOOSEnd, value); }
        }
        int fLOUCost;
        public int LOUCost
        {
            get { return fLOUCost; }
            set { SetPropertyValue<int>("LOUCost", ref fLOUCost, value); }
        }
        int fLOUAnnlPrem;
        public int LOUAnnlPrem
        {
            get { return fLOUAnnlPrem; }
            set { SetPropertyValue<int>("LOUAnnlPrem", ref fLOUAnnlPrem, value); }
        }
        int fADNDLimit;
        public int ADNDLimit
        {
            get { return fADNDLimit; }
            set { SetPropertyValue<int>("ADNDLimit", ref fADNDLimit, value); }
        }
        int fADNDCost;
        public int ADNDCost
        {
            get { return fADNDCost; }
            set { SetPropertyValue<int>("ADNDCost", ref fADNDCost, value); }
        }
        int fADNDAnnlPrem;
        public int ADNDAnnlPrem
        {
            get { return fADNDAnnlPrem; }
            set { SetPropertyValue<int>("ADNDAnnlPrem", ref fADNDAnnlPrem, value); }
        }
        bool fHoldRtn;
        public bool HoldRtn
        {
            get { return fHoldRtn; }
            set { SetPropertyValue<bool>("HoldRtn", ref fHoldRtn, value); }
        }
        bool fNonOwners;
        public bool NonOwners
        {
            get { return fNonOwners; }
            set { SetPropertyValue<bool>("NonOwners", ref fNonOwners, value); }
        }
        string fChangeGUID;
        [Size(50)]
        public string ChangeGUID
        {
            get { return fChangeGUID; }
            set { SetPropertyValue<string>("ChangeGUID", ref fChangeGUID, value); }
        }
        int fpolicyCars;
        public int policyCars
        {
            get { return fpolicyCars; }
            set { SetPropertyValue<int>("policyCars", ref fpolicyCars, value); }
        }
        int fpolicyDrivers;
        public int policyDrivers
        {
            get { return fpolicyDrivers; }
            set { SetPropertyValue<int>("policyDrivers", ref fpolicyDrivers, value); }
        }
        bool fUnacceptable;
        public bool Unacceptable
        {
            get { return fUnacceptable; }
            set { SetPropertyValue<bool>("Unacceptable", ref fUnacceptable, value); }
        }
        int fHousholdIndex;
        public int HousholdIndex
        {
            get { return fHousholdIndex; }
            set { SetPropertyValue<int>("HousholdIndex", ref fHousholdIndex, value); }
        }
        int fRatingID;
        public int RatingID
        {
            get { return fRatingID; }
            set { SetPropertyValue<int>("RatingID", ref fRatingID, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        int fCars;
        public int Cars
        {
            get { return fCars; }
            set { SetPropertyValue<int>("Cars", ref fCars, value); }
        }
        int fDrivers;
        public int Drivers
        {
            get { return fDrivers; }
            set { SetPropertyValue<int>("Drivers", ref fDrivers, value); }
        }
        bool fIsReturnedMail;
        public bool IsReturnedMail
        {
            get { return fIsReturnedMail; }
            set { SetPropertyValue<bool>("IsReturnedMail", ref fIsReturnedMail, value); }
        }
        bool fIsOnHold;
        public bool IsOnHold
        {
            get { return fIsOnHold; }
            set { SetPropertyValue<bool>("IsOnHold", ref fIsOnHold, value); }
        }
        bool fIsClaimMisRep;
        public bool IsClaimMisRep
        {
            get { return fIsClaimMisRep; }
            set { SetPropertyValue<bool>("IsClaimMisRep", ref fIsClaimMisRep, value); }
        }
        bool fIsNoREI;
        public bool IsNoREI
        {
            get { return fIsNoREI; }
            set { SetPropertyValue<bool>("IsNoREI", ref fIsNoREI, value); }
        }
        bool fIsSuspense;
        public bool IsSuspense
        {
            get { return fIsSuspense; }
            set { SetPropertyValue<bool>("IsSuspense", ref fIsSuspense, value); }
        }
        bool fisAnnual;
        public bool isAnnual
        {
            get { return fisAnnual; }
            set { SetPropertyValue<bool>("isAnnual", ref fisAnnual, value); }
        }
        string fCarrier;
        [Size(200)]
        public string Carrier
        {
            get { return fCarrier; }
            set { SetPropertyValue<string>("Carrier", ref fCarrier, value); }
        }
        bool fHasPriorCoverage;
        public bool HasPriorCoverage
        {
            get { return fHasPriorCoverage; }
            set { SetPropertyValue<bool>("HasPriorCoverage", ref fHasPriorCoverage, value); }
        }
        bool fHasPriorBalance;
        public bool HasPriorBalance
        {
            get { return fHasPriorBalance; }
            set { SetPropertyValue<bool>("HasPriorBalance", ref fHasPriorBalance, value); }
        }
        bool fHasLapseNone;
        public bool HasLapseNone
        {
            get { return fHasLapseNone; }
            set { SetPropertyValue<bool>("HasLapseNone", ref fHasLapseNone, value); }
        }
        bool fHasPD;
        public bool HasPD
        {
            get { return fHasPD; }
            set { SetPropertyValue<bool>("HasPD", ref fHasPD, value); }
        }
        bool fHasTOW;
        public bool HasTOW
        {
            get { return fHasTOW; }
            set { SetPropertyValue<bool>("HasTOW", ref fHasTOW, value); }
        }
        string fPIPLimit;
        [Size(50)]
        public string PIPLimit
        {
            get { return fPIPLimit; }
            set { SetPropertyValue<string>("PIPLimit", ref fPIPLimit, value); }
        }
        bool fHasADND;
        public bool HasADND
        {
            get { return fHasADND; }
            set { SetPropertyValue<bool>("HasADND", ref fHasADND, value); }
        }
        int fMVRFee;
        public int MVRFee
        {
            get { return fMVRFee; }
            set { SetPropertyValue<int>("MVRFee", ref fMVRFee, value); }
        }
        int fWorkLoss;
        public int WorkLoss
        {
            get { return fWorkLoss; }
            set { SetPropertyValue<int>("WorkLoss", ref fWorkLoss, value); }
        }
        double fRentalAnnlPrem;
        public double RentalAnnlPrem
        {
            get { return fRentalAnnlPrem; }
            set { SetPropertyValue<double>("RentalAnnlPrem", ref fRentalAnnlPrem, value); }
        }
        double fRentalCost;
        public double RentalCost
        {
            get { return fRentalCost; }
            set { SetPropertyValue<double>("RentalCost", ref fRentalCost, value); }
        }
        bool fHasLapse110;
        public bool HasLapse110
        {
            get { return fHasLapse110; }
            set { SetPropertyValue<bool>("HasLapse110", ref fHasLapse110, value); }
        }
        bool fHasLapse1131;
        public bool HasLapse1131
        {
            get { return fHasLapse1131; }
            set { SetPropertyValue<bool>("HasLapse1131", ref fHasLapse1131, value); }
        }
        double fQuotedAmount;
        public double QuotedAmount
        {
            get { return fQuotedAmount; }
            set { SetPropertyValue<double>("QuotedAmount", ref fQuotedAmount, value); }
        }
        string fAppQuestionAnswers;
        [Size(500)]
        public string AppQuestionAnswers
        {
            get { return fAppQuestionAnswers; }
            set { SetPropertyValue<string>("AppQuestionAnswers", ref fAppQuestionAnswers, value); }
        }
        string fAppQuestionExplainOne;
        [Size(8000)]
        public string AppQuestionExplainOne
        {
            get { return fAppQuestionExplainOne; }
            set { SetPropertyValue<string>("AppQuestionExplainOne", ref fAppQuestionExplainOne, value); }
        }
        string fAppQuestionExplainTwo;
        [Size(8000)]
        public string AppQuestionExplainTwo
        {
            get { return fAppQuestionExplainTwo; }
            set { SetPropertyValue<string>("AppQuestionExplainTwo", ref fAppQuestionExplainTwo, value); }
        }
        bool fFromPortal;
        public bool FromPortal
        {
            get { return fFromPortal; }
            set { SetPropertyValue<bool>("FromPortal", ref fFromPortal, value); }
        }
        string fFromRater;
        [Size(20)]
        public string FromRater
        {
            get { return fFromRater; }
            set { SetPropertyValue<string>("FromRater", ref fFromRater, value); }
        }
        string fQuoteNo;
        [Size(50)]
        public string QuoteNo
        {
            get { return fQuoteNo; }
            set { SetPropertyValue<string>("QuoteNo", ref fQuoteNo, value); }
        }
        string fAgencyRep;
        [Size(500)]
        public string AgencyRep
        {
            get { return fAgencyRep; }
            set { SetPropertyValue<string>("AgencyRep", ref fAgencyRep, value); }
        }
        DateTime fQuotedDate;
        public DateTime QuotedDate
        {
            get { return fQuotedDate; }
            set { SetPropertyValue<DateTime>("QuotedDate", ref fQuotedDate, value); }
        }
        bool fBridged;
        public bool Bridged
        {
            get { return fBridged; }
            set { SetPropertyValue<bool>("Bridged", ref fBridged, value); }
        }
        DateTime fMinDuePayDate;
        public DateTime MinDuePayDate
        {
            get { return fMinDuePayDate; }
            set { SetPropertyValue<DateTime>("MinDuePayDate", ref fMinDuePayDate, value); }
        }
        bool fIsAllowRenPastExp;
        public bool IsAllowRenPastExp
        {
            get { return fIsAllowRenPastExp; }
            set { SetPropertyValue<bool>("IsAllowRenPastExp", ref fIsAllowRenPastExp, value); }
        }
        bool? fIsShow;
        public bool? IsShow
        {
            get { return fIsShow; }
            set { SetPropertyValue<bool?>("IsShow", ref fIsShow, value); }
        }
        bool fAllowPymntWithin15Days;
        public bool AllowPymntWithin15Days
        {
            get { return fAllowPymntWithin15Days; }
            set { SetPropertyValue<bool>("AllowPymntWithin15Days", ref fAllowPymntWithin15Days, value); }
        }
        double fMaintenanceFee;
        public double MaintenanceFee
        {
            get { return fMaintenanceFee; }
            set { SetPropertyValue<double>("MaintenanceFee", ref fMaintenanceFee, value); }
        }
        double fPIPPDOnlyFee;
        public double PIPPDOnlyFee
        {
            get { return fPIPPDOnlyFee; }
            set { SetPropertyValue<double>("PIPPDOnlyFee", ref fPIPPDOnlyFee, value); }
        }

        double rewriteFee;
        public double RewriteFee
        {
            get { return rewriteFee; }
            set { SetPropertyValue<double>("RewriteFee", ref rewriteFee, value); }
        }

        string fAgentESignEmailAddress;
        public string AgentESignEmailAddress
        {
            get { return fAgentESignEmailAddress; }
            set { SetPropertyValue<string>("AgentESignEmailAddress", ref fAgentESignEmailAddress, value); }
        }

        public XpoPolicy(Session session) : base(session) { }
        public XpoPolicy() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
