using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class Payments : IPayments
    {
        public int IndexID { get; set; }
        public string BatchID { get; set; }
        public string BatchNo { get; set; }
        public string PolicyNo { get; set; }
        public string PaymentType { get; set; }
        public string CheckNo { get; set; }
        public string SerialNo { get; set; }
        public DateTime PostmarkDate { get; set; }
        public DateTime AcctDate { get; set; }
        public double CheckAmt { get; set; }
        public double DraftAmt { get; set; }
        public double TotalAmt { get; set; }
        public double CommissionAmt { get; set; }
        public string PFCCode { get; set; }
        public string UserName { get; set; }
        public double FeeAmt { get; set; }
        public double Interest { get; set; }
        public bool InsuredCheck { get; set; }
        public bool EFTDraft { get; set; }
        public string EFTRefNo { get; set; }
        public double AmtPrinciple { get; set; }
        public DateTime DateVoided { get; set; }
        public DateTime DateCleared { get; set; }
        public DateTime ReportedToBankDate { get; set; }
        public string BatchGUID { get; set; }
        public string CheckNotes { get; set; }
        public string Notes { get; set; }
        public string PayeeOne { get; set; }
        public string PayeeOneType { get; set; }
        public string PayeeTwo { get; set; }
        public string PayeeTwoType { get; set; }
        public DateTime CreateDate { get; set; }
        public string ACHTransID { get; set; }
        public string TokenID { get; set; }
        public string PortalRespCode { get; set; }
        public bool IsBounced { get; set; }
        public bool IsEftReverse { get; set; }
        public string PortalReplyMsg { get; set; }
        public bool IsCheck { get; set; }
        public bool FromCardConnect { get; set; }
        public string Last4OfCard { get; set; }
    }

    public interface IPayments
    {
        int IndexID { get; set; }
        string BatchID { get; set; }
        string BatchNo { get; set; }
        string PolicyNo { get; set; }
        string PaymentType { get; set; }
        string CheckNo { get; set; }
        string SerialNo { get; set; }
        DateTime PostmarkDate { get; set; }
        DateTime AcctDate { get; set; }
        double CheckAmt { get; set; }
        double DraftAmt { get; set; }
        double TotalAmt { get; set; }
        double CommissionAmt { get; set; }
        string PFCCode { get; set; }
        string UserName { get; set; }
        double FeeAmt { get; set; }
        double Interest { get; set; }
        bool InsuredCheck { get; set; }
        bool EFTDraft { get; set; }
        string EFTRefNo { get; set; }
        double AmtPrinciple { get; set; }
        DateTime DateVoided { get; set; }
        DateTime DateCleared { get; set; }
        DateTime ReportedToBankDate { get; set; }
        string BatchGUID { get; set; }
        string CheckNotes { get; set; }
        string Notes { get; set; }
        string PayeeOne { get; set; }
        string PayeeOneType { get; set; }
        string PayeeTwo { get; set; }
        string PayeeTwoType { get; set; }
        DateTime CreateDate { get; set; }
        string ACHTransID { get; set; }
        string TokenID { get; set; }
        string PortalRespCode { get; set; }
        bool IsBounced { get; set; }
        bool IsEftReverse { get; set; }
        string PortalReplyMsg { get; set; }
        bool IsCheck { get; set; }
        bool FromCardConnect { get; set; }
        string Last4OfCard { get; set; }
    }


	[Persistent("Payments")]
    public class XpoPayments : XPLiteObject, IPayments
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fBatchID;
        [Size(50)]
        public string BatchID
        {
            get { return fBatchID; }
            set { SetPropertyValue<string>("BatchID", ref fBatchID, value); }
        }
        string fBatchNo;
        [Size(50)]
        public string BatchNo
        {
            get { return fBatchNo; }
            set { SetPropertyValue<string>("BatchNo", ref fBatchNo, value); }
        }
        string fPolicyNo;
        [Size(20)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fPaymentType;
        [Size(10)]
        public string PaymentType
        {
            get { return fPaymentType; }
            set { SetPropertyValue<string>("PaymentType", ref fPaymentType, value); }
        }
        string fCheckNo;
        [Size(10)]
        public string CheckNo
        {
            get { return fCheckNo; }
            set { SetPropertyValue<string>("CheckNo", ref fCheckNo, value); }
        }
        string fSerialNo;
        [Size(10)]
        public string SerialNo
        {
            get { return fSerialNo; }
            set { SetPropertyValue<string>("SerialNo", ref fSerialNo, value); }
        }
        DateTime fPostmarkDate;
        public DateTime PostmarkDate
        {
            get { return fPostmarkDate; }
            set { SetPropertyValue<DateTime>("PostmarkDate", ref fPostmarkDate, value); }
        }
        DateTime fAcctDate;
        public DateTime AcctDate
        {
            get { return fAcctDate; }
            set { SetPropertyValue<DateTime>("AcctDate", ref fAcctDate, value); }
        }
        double fCheckAmt;
        public double CheckAmt
        {
            get { return fCheckAmt; }
            set { SetPropertyValue<double>("CheckAmt", ref fCheckAmt, value); }
        }
        double fDraftAmt;
        public double DraftAmt
        {
            get { return fDraftAmt; }
            set { SetPropertyValue<double>("DraftAmt", ref fDraftAmt, value); }
        }
        double fTotalAmt;
        public double TotalAmt
        {
            get { return fTotalAmt; }
            set { SetPropertyValue<double>("TotalAmt", ref fTotalAmt, value); }
        }
        double fCommissionAmt;
        public double CommissionAmt
        {
            get { return fCommissionAmt; }
            set { SetPropertyValue<double>("CommissionAmt", ref fCommissionAmt, value); }
        }
        string fPFCCode;
        [Size(5)]
        public string PFCCode
        {
            get { return fPFCCode; }
            set { SetPropertyValue<string>("PFCCode", ref fPFCCode, value); }
        }
        string fUserName;
        [Size(15)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        double fFeeAmt;
        public double FeeAmt
        {
            get { return fFeeAmt; }
            set { SetPropertyValue<double>("FeeAmt", ref fFeeAmt, value); }
        }
        double fInterest;
        public double Interest
        {
            get { return fInterest; }
            set { SetPropertyValue<double>("Interest", ref fInterest, value); }
        }
        bool fInsuredCheck;
        public bool InsuredCheck
        {
            get { return fInsuredCheck; }
            set { SetPropertyValue<bool>("InsuredCheck", ref fInsuredCheck, value); }
        }
        bool fEFTDraft;
        public bool EFTDraft
        {
            get { return fEFTDraft; }
            set { SetPropertyValue<bool>("EFTDraft", ref fEFTDraft, value); }
        }
        string fEFTRefNo;
        [Size(35)]
        public string EFTRefNo
        {
            get { return fEFTRefNo; }
            set { SetPropertyValue<string>("EFTRefNo", ref fEFTRefNo, value); }
        }
        double fAmtPrinciple;
        public double AmtPrinciple
        {
            get { return fAmtPrinciple; }
            set { SetPropertyValue<double>("AmtPrinciple", ref fAmtPrinciple, value); }
        }
        DateTime fDateVoided;
        public DateTime DateVoided
        {
            get { return fDateVoided; }
            set { SetPropertyValue<DateTime>("DateVoided", ref fDateVoided, value); }
        }
        DateTime fDateCleared;
        public DateTime DateCleared
        {
            get { return fDateCleared; }
            set { SetPropertyValue<DateTime>("DateCleared", ref fDateCleared, value); }
        }
        DateTime fReportedToBankDate;
        public DateTime ReportedToBankDate
        {
            get { return fReportedToBankDate; }
            set { SetPropertyValue<DateTime>("ReportedToBankDate", ref fReportedToBankDate, value); }
        }
        string fBatchGUID;
        public string BatchGUID
        {
            get { return fBatchGUID; }
            set { SetPropertyValue<string>("BatchGUID", ref fBatchGUID, value); }
        }
        string fCheckNotes;
        public string CheckNotes
        {
            get { return fCheckNotes; }
            set { SetPropertyValue<string>("CheckNotes", ref fCheckNotes, value); }
        }
        string fNotes;
        [Size(255)]
        public string Notes
        {
            get { return fNotes; }
            set { SetPropertyValue<string>("Notes", ref fNotes, value); }
        }
        string fPayeeOne;
        [Size(150)]
        public string PayeeOne
        {
            get { return fPayeeOne; }
            set { SetPropertyValue<string>("PayeeOne", ref fPayeeOne, value); }
        }
        string fPayeeOneType;
        [Size(50)]
        public string PayeeOneType
        {
            get { return fPayeeOneType; }
            set { SetPropertyValue<string>("PayeeOneType", ref fPayeeOneType, value); }
        }
        string fPayeeTwo;
        public string PayeeTwo
        {
            get { return fPayeeTwo; }
            set { SetPropertyValue<string>("PayeeTwo", ref fPayeeTwo, value); }
        }
        string fPayeeTwoType;
        [Size(50)]
        public string PayeeTwoType
        {
            get { return fPayeeTwoType; }
            set { SetPropertyValue<string>("PayeeTwoType", ref fPayeeTwoType, value); }
        }
        DateTime fCreateDate;
        public DateTime CreateDate
        {
            get { return fCreateDate; }
            set { SetPropertyValue<DateTime>("CreateDate", ref fCreateDate, value); }
        }
        string fACHTransID;
        [Size(20)]
        public string ACHTransID
        {
            get { return fACHTransID; }
            set { SetPropertyValue<string>("ACHTransID", ref fACHTransID, value); }
        }
        string fTokenID;
        [Size(2000)]
        public string TokenID
        {
            get { return fTokenID; }
            set { SetPropertyValue<string>("TokenID", ref fTokenID, value); }
        }
        string fPortalRespCode;
        [Size(200)]
        public string PortalRespCode
        {
            get { return fPortalRespCode; }
            set { SetPropertyValue<string>("PortalRespCode", ref fPortalRespCode, value); }
        }
        bool fIsBounced;
        public bool IsBounced
        {
            get { return fIsBounced; }
            set { SetPropertyValue<bool>("IsBounced", ref fIsBounced, value); }
        }
        bool fIsEftReverse;
        public bool IsEftReverse
        {
            get { return fIsEftReverse; }
            set { SetPropertyValue<bool>("IsEftReverse", ref fIsEftReverse, value); }
        }
        string fPortalReplyMsg;
        [Size(1000)]
        public string PortalReplyMsg
        {
            get { return fPortalReplyMsg; }
            set { SetPropertyValue<string>("PortalReplyMsg", ref fPortalReplyMsg, value); }
        }

        bool fIsCheck;
        public bool IsCheck
        {
            get { return fIsCheck; }
            set { SetPropertyValue<bool>("IsCheck", ref fIsCheck, value); }
        }

        bool fFromCardConnect;
        public bool FromCardConnect
        {
            get { return fFromCardConnect; }
            set { SetPropertyValue<bool>("FromCardConnect", ref fFromCardConnect, value); }
        }

        string fLast4OfCard;
        [Size(4)]
        public string Last4OfCard
        {
            get { return fLast4OfCard; }
            set { SetPropertyValue<string>("Last4OfCard", ref fLast4OfCard, value); }
        }
        public XpoPayments(Session session) : base(session) { }
        public XpoPayments() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
