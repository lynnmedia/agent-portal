using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PortalRights : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fShortDesc;
        [Size(200)]
        public string ShortDesc
        {
            get { return fShortDesc; }
            set { SetPropertyValue<string>("ShortDesc", ref fShortDesc, value); }
        }
        string fLongDesc;
        [Size(2000)]
        public string LongDesc
        {
            get { return fLongDesc; }
            set { SetPropertyValue<string>("LongDesc", ref fLongDesc, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        public PortalRights(Session session) : base(session) { }
        public PortalRights() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}
