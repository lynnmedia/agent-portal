﻿using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public partial class FLRatesAACreditScore : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fState;
        [Size(2)]
        public string State
        {
            get { return fState; }
            set { SetPropertyValue<string>("State", ref fState, value); }
        }
        string fCreditRating;
        [Size(2)]
        public string CreditRating
        {
            get { return fCreditRating; }
            set { SetPropertyValue<string>("CreditRating", ref fCreditRating, value); }
        }
        int fMinScore;
        public int MinScore
        {
            get { return fMinScore; }
            set { SetPropertyValue<int>("MinScore", ref fMinScore, value); }
        }
        int fMaxScore;
        public int MaxScore
        {
            get { return fMaxScore; }
            set { SetPropertyValue<int>("MaxScore", ref fMaxScore, value); }
        }
        string fRateCycle;
        [Size(25)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        double fPageNo;
        public double PageNo
        {
            get { return fPageNo; }
            set { SetPropertyValue<double>("PageNo", ref fPageNo, value); }
        }

        public FLRatesAACreditScore(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }

}
