using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class SystemErrorCodes : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fErrorMsg;
        public string ErrorMsg
        {
            get { return fErrorMsg; }
            set { SetPropertyValue<string>("ErrorMsg", ref fErrorMsg, value); }
        }
        string fShortErrMsg;
        public string ShortErrMsg
        {
            get { return fShortErrMsg; }
            set { SetPropertyValue<string>("ShortErrMsg", ref fShortErrMsg, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref fDateCreated, value); }
        }
        public SystemErrorCodes(Session session) : base(session) { }
        public SystemErrorCodes() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
