﻿using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class PolicyQuoteDrivers : XPCustomObject
    {
        string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        int fDriverIndex;
        public int DriverIndex
        {
            get { return fDriverIndex; }
            set { SetPropertyValue<int>("DriverIndex", ref fDriverIndex, value); }
        }
        DateTime fEffDate;
        public DateTime EffDate
        {
            get { return fEffDate; }
            set { SetPropertyValue<DateTime>("EffDate", ref fEffDate, value); }
        }
        DateTime fExpDate;
        public DateTime ExpDate
        {
            get { return fExpDate; }
            set { SetPropertyValue<DateTime>("ExpDate", ref fExpDate, value); }
        }
        bool fDeleted;
        public bool Deleted
        {
            get { return fDeleted; }
            set { SetPropertyValue<bool>("Deleted", ref fDeleted, value); }
        }
        string fFirstName;
        [Size(255)]
        public string FirstName
        {
            get { return fFirstName; }
            set { SetPropertyValue<string>("FirstName", ref fFirstName, value); }
        }
        string fLastName;
        [Size(500)]
        public string LastName
        {
            get { return fLastName; }
            set { SetPropertyValue<string>("LastName", ref fLastName, value); }
        }
        string fMI;
        [Size(1)]
        public string MI
        {
            get { return fMI; }
            set { SetPropertyValue<string>("MI", ref fMI, value); }
        }
        string fSuffix;
        [Size(10)]
        public string Suffix
        {
            get { return fSuffix; }
            set { SetPropertyValue<string>("Suffix", ref fSuffix, value); }
        }
        string fClass;
        [Size(20)]
        public string Class
        {
            get { return fClass; }
            set { SetPropertyValue<string>("Class", ref fClass, value); }
        }
        string fGender;
        [Size(10)]
        public string Gender
        {
            get { return fGender; }
            set { SetPropertyValue<string>("Gender", ref fGender, value); }
        }
        bool fMarried;
        public bool Married
        {
            get { return fMarried; }
            set { SetPropertyValue<bool>("Married", ref fMarried, value); }
        }
        DateTime fDOB;
        public DateTime DOB
        {
            get { return fDOB; }
            set { SetPropertyValue<DateTime>("DOB", ref fDOB, value); }
        }
        int fDriverAge;
        public int DriverAge
        {
            get { return fDriverAge; }
            set { SetPropertyValue<int>("DriverAge", ref fDriverAge, value); }
        }
        bool fYouthful;
        public bool Youthful
        {
            get { return fYouthful; }
            set { SetPropertyValue<bool>("Youthful", ref fYouthful, value); }
        }
        int fPoints;
        public int Points
        {
            get { return fPoints; }
            set { SetPropertyValue<int>("Points", ref fPoints, value); }
        }
        int fPrimaryCar;
        public int PrimaryCar
        {
            get { return fPrimaryCar; }
            set { SetPropertyValue<int>("PrimaryCar", ref fPrimaryCar, value); }
        }
        bool fSafeDriver;
        public bool SafeDriver
        {
            get { return fSafeDriver; }
            set { SetPropertyValue<bool>("SafeDriver", ref fSafeDriver, value); }
        }
        bool fSeniorDefDrvDisc;
        public bool SeniorDefDrvDisc
        {
            get { return fSeniorDefDrvDisc; }
            set { SetPropertyValue<bool>("SeniorDefDrvDisc", ref fSeniorDefDrvDisc, value); }
        }
        DateTime fSeniorDefDrvDate;
        public DateTime SeniorDefDrvDate
        {
            get { return fSeniorDefDrvDate; }
            set { SetPropertyValue<DateTime>("SeniorDefDrvDate", ref fSeniorDefDrvDate, value); }
        }
        string fLicenseNo;
        [Size(20)]
        public string LicenseNo
        {
            get { return fLicenseNo; }
            set { SetPropertyValue<string>("LicenseNo", ref fLicenseNo, value); }
        }
        string fLicenseSt;
        [Size(2)]
        public string LicenseSt
        {
            get { return fLicenseSt; }
            set { SetPropertyValue<string>("LicenseSt", ref fLicenseSt, value); }
        }
        DateTime fDateLicensed;
        public DateTime DateLicensed
        {
            get { return fDateLicensed; }
            set { SetPropertyValue<DateTime>("DateLicensed", ref fDateLicensed, value); }
        }
        bool fInternationalLic;
        public bool InternationalLic
        {
            get { return fInternationalLic; }
            set { SetPropertyValue<bool>("InternationalLic", ref fInternationalLic, value); }
        }
        bool fUnverifiable;
        public bool Unverifiable
        {
            get { return fUnverifiable; }
            set { SetPropertyValue<bool>("Unverifiable", ref fUnverifiable, value); }
        }
        bool fInexp;
        public bool Inexp
        {
            get { return fInexp; }
            set { SetPropertyValue<bool>("Inexp", ref fInexp, value); }
        }
        bool fSR22;
        public bool SR22
        {
            get { return fSR22; }
            set { SetPropertyValue<bool>("SR22", ref fSR22, value); }
        }
        bool fSuspLic;
        public bool SuspLic
        {
            get { return fSuspLic; }
            set { SetPropertyValue<bool>("SuspLic", ref fSuspLic, value); }
        }
        string fSSN;
        [Size(11)]
        public string SSN
        {
            get { return fSSN; }
            set { SetPropertyValue<string>("SSN", ref fSSN, value); }
        }
        string fSR22CaseNo;
        [Size(15)]
        public string SR22CaseNo
        {
            get { return fSR22CaseNo; }
            set { SetPropertyValue<string>("SR22CaseNo", ref fSR22CaseNo, value); }
        }
        DateTime fSR22PrintDate;
        public DateTime SR22PrintDate
        {
            get { return fSR22PrintDate; }
            set { SetPropertyValue<DateTime>("SR22PrintDate", ref fSR22PrintDate, value); }
        }
        DateTime fSR26PrintDate;
        public DateTime SR26PrintDate
        {
            get { return fSR26PrintDate; }
            set { SetPropertyValue<DateTime>("SR26PrintDate", ref fSR26PrintDate, value); }
        }
        bool fExclude;
        public bool Exclude
        {
            get { return fExclude; }
            set { SetPropertyValue<bool>("Exclude", ref fExclude, value); }
        }
        string fRelationToInsured;
        [Size(15)]
        public string RelationToInsured
        {
            get { return fRelationToInsured; }
            set { SetPropertyValue<string>("RelationToInsured", ref fRelationToInsured, value); }
        }
        string fOccupation;
        [Size(25)]
        public string Occupation
        {
            get { return fOccupation; }
            set { SetPropertyValue<string>("Occupation", ref fOccupation, value); }
        }
        int fOID;
        [Key(true)]
        public int OID
        {
            get { return fOID; }
            set { SetPropertyValue<int>("OID", ref fOID, value); }
        }
        DateTime fInceptionDate;
        public DateTime InceptionDate
        {
            get { return fInceptionDate; }
            set { SetPropertyValue<DateTime>("InceptionDate", ref fInceptionDate, value); }
        }
        string fJobTitle;
        [Size(300)]
        public string JobTitle
        {
            get { return fJobTitle; }
            set { SetPropertyValue<string>("JobTitle", ref fJobTitle, value); }
        }
        string fCompany;
        [Size(200)]
        public string Company
        {
            get { return fCompany; }
            set { SetPropertyValue<string>("Company", ref fCompany, value); }
        }
        string fCompanyAddress;
        [Size(200)]
        public string CompanyAddress
        {
            get { return fCompanyAddress; }
            set { SetPropertyValue<string>("CompanyAddress", ref fCompanyAddress, value); }
        }
        string fCompanyCity;
        [Size(50)]
        public string CompanyCity
        {
            get { return fCompanyCity; }
            set { SetPropertyValue<string>("CompanyCity", ref fCompanyCity, value); }
        }
        string fCompanyState;
        [Size(2)]
        public string CompanyState
        {
            get { return fCompanyState; }
            set { SetPropertyValue<string>("CompanyState", ref fCompanyState, value); }
        }
        string fCompanyZip;
        [Size(15)]
        public string CompanyZip
        {
            get { return fCompanyZip; }
            set { SetPropertyValue<string>("CompanyZip", ref fCompanyZip, value); }
        }
        string fWorkPhone;
        [Size(25)]
        public string WorkPhone
        {
            get { return fWorkPhone; }
            set { SetPropertyValue<string>("WorkPhone", ref fWorkPhone, value); }
        }
        int fViolations;
        public int Violations
        {
            get { return fViolations; }
            set { SetPropertyValue<int>("Violations", ref fViolations, value); }
        }
        int fPTSDriverIndex;
        public int PTSDriverIndex
        {
            get { return fPTSDriverIndex; }
            set { SetPropertyValue<int>("PTSDriverIndex", ref fPTSDriverIndex, value); }
        }
        string fDriverMsg;
        [Size(200)]
        public string DriverMsg
        {
            get { return fDriverMsg; }
            set { SetPropertyValue<string>("DriverMsg", ref fDriverMsg, value); }
        }
        bool fChargeMVR;
        public bool ChargeMVR
        {
            get { return fChargeMVR; }
            set { SetPropertyValue<bool>("ChargeMVR", ref fChargeMVR, value); }
        }
        private bool fIsLearners;
        public bool IsLearners
        {
            get { return fIsLearners; }
            set { SetPropertyValue<bool>("IsLearners", ref fIsLearners, value); }
        }

        private bool hasMvr;
        public bool HasMvr
        {
            get { return hasMvr; }
            set { SetPropertyValue<bool>("HasMvr", ref hasMvr, value); }
        }


        private DateTime mvrDate;
        public DateTime MvrDate
        {
            get { return mvrDate; }
            set { SetPropertyValue<DateTime>("MvrDate", ref mvrDate, value); }
        }


        private string veriskMvrOrderNumber;
        public string VeriskMvrOrderNumber
        {
            get { return veriskMvrOrderNumber; }
            set { SetPropertyValue<string>("VeriskMvrOrderNumber", ref veriskMvrOrderNumber, value); }
        }

        private string licenseStatus;
        public string LicenseStatus
        {
            get { return licenseStatus; }
            set { SetPropertyValue<string>("LicenseStatus", ref licenseStatus, value); }
        }

        private DateTime licenseExpiration;
        public DateTime LicenseExpiration
        {
            get { return licenseExpiration; }
            set { SetPropertyValue<DateTime>("LicenseExpiration", ref licenseExpiration, value); }
        }
        public PolicyQuoteDrivers(Session session) : base(session) { }
        public PolicyQuoteDrivers() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}