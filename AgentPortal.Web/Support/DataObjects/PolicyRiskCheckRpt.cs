﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class PolicyRiskCheckRpt : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fTransactionId;
        [Size(40)]
        public string TransactionId
        {
            get { return fTransactionId; }
            set { SetPropertyValue<string>("TransactionId", ref fTransactionId, value); }
        }
        string fPolicyNo;
        [Size(50)]
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fRiskGroup;
        [Size(15)]
        public string RiskGroup
        {
            get { return fRiskGroup; }
            set { SetPropertyValue<string>("RiskGroup", ref fRiskGroup, value); }
        }
        string fTotalScore;
        [Size(10)]
        public string TotalScore
        {
            get { return fTotalScore; }
            set { SetPropertyValue<string>("TotalScore", ref fTotalScore, value); }
        }
        string fScoreColor;
        [Size(10)]
        public string ScoreColor
        {
            get { return fScoreColor; }
            set { SetPropertyValue<string>("ScoreColor", ref fScoreColor, value); }
        }
        bool fIsCRAWarning;
        public bool IsCRAWarning
        {
            get { return fIsCRAWarning; }
            set { SetPropertyValue<bool>("IsCRAWarning", ref fIsCRAWarning, value); }
        }
        bool fIsFraudIndicator;
        public bool IsFraudIndicator
        {
            get { return fIsFraudIndicator; }
            set { SetPropertyValue<bool>("IsFraudIndicator", ref fIsFraudIndicator, value); }
        }
        string fOrgId;
        [Size(10)]
        public string OrgId
        {
            get { return fOrgId; }
            set { SetPropertyValue<string>("OrgId", ref fOrgId, value); }
        }
        string fShipId;
        [Size(10)]
        public string ShipId
        {
            get { return fShipId; }
            set { SetPropertyValue<string>("ShipId", ref fShipId, value); }
        }
        DateTime fTransactionDate;
        public DateTime TransactionDate
        {
            get { return fTransactionDate; }
            set { SetPropertyValue<DateTime>("TransactionDate", ref fTransactionDate, value); }
        }

        public PolicyRiskCheckRpt(Session session) : base(session) { }
        public PolicyRiskCheckRpt() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }




}