using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class PortalLogonActivity : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fUsername;
        [Size(500)]
        public string Username
        {
            get { return fUsername; }
            set { SetPropertyValue<string>("Username", ref fUsername, value); }
        }
        string fUserIp;
        [Size(20)]
        public string UserIp
        {
            get { return fUserIp; }
            set { SetPropertyValue<string>("UserIp", ref fUserIp, value); }
        }
        DateTime fDateLoggedIn;
        public DateTime DateLoggedIn
        {
            get { return fDateLoggedIn; }
            set { SetPropertyValue<DateTime>("DateLoggedIn", ref fDateLoggedIn, value); }
        }
        public PortalLogonActivity(Session session) : base(session) { }
        public PortalLogonActivity() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
