using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAuditCyberSourcePayments
    {
        int IndexID { get; set; }
        string requestID { get; set; }
        string pymntPolicyNumber { get; set; }
        string portalController { get; set; }
        string paymentOption { get; set; }
        double orderAmount { get; set; }
        double ccAuthReply_amount { get; set; }
        string card_cardType { get; set; }
        string reasonCode { get; set; }
        string decision { get; set; }
        string billTo_firstName { get; set; }
        string billTo_lastName { get; set; }
        string billTo_street1 { get; set; }
        string billTo_city { get; set; }
        string billTo_state { get; set; }
        string billTo_postalCode { get; set; }
        string billTo_country { get; set; }
        string card_expirationYear { get; set; }
        string card_expirationMonth { get; set; }
        string ccAuthReply_authorizedDateTime { get; set; }
        string ccAuthReply_authorizationCode { get; set; }
        string ccAuthReply_avsCodeRaw { get; set; }
        string ccAuthReply_reasonCode { get; set; }
        string ccAuthReply_cvCode { get; set; }
        string ccAuthReply_avsCode { get; set; }
        string ccAuthReply_processorResponse { get; set; }
        string ccAuthReply_cvCodeRaw { get; set; }
        string orderAmount_publicSignature { get; set; }
        string orderPage_serialNumber { get; set; }
        string orderCurrency { get; set; }
        string orderPage_requestToken { get; set; }
        string orderCurrency_publicSignature { get; set; }
        string orderPage_transactionType { get; set; }
        string orderNumber_publicSignature { get; set; }
        string orderPage_environment { get; set; }
        string reconciliationID { get; set; }
        string signedDataPublicSignature { get; set; }
        string transactionSignature { get; set; }
        string decision_publicSignature { get; set; }
        string merchantID { get; set; }
        string orderNumber { get; set; }
    }

    public class AuditCyberSourcePayments : IAuditCyberSourcePayments
    {
        public int IndexID { get; set; }
        public string requestID { get; set; }
        public string pymntPolicyNumber { get; set; }
        public string portalController { get; set; }
        public string paymentOption { get; set; }
        public double orderAmount { get; set; }
        public double ccAuthReply_amount { get; set; }
        public string card_cardType { get; set; }
        public string reasonCode { get; set; }
        public string decision { get; set; }
        public string billTo_firstName { get; set; }
        public string billTo_lastName { get; set; }
        public string billTo_street1 { get; set; }
        public string billTo_city { get; set; }
        public string billTo_state { get; set; }
        public string billTo_postalCode { get; set; }
        public string billTo_country { get; set; }
        public string card_expirationYear { get; set; }
        public string card_expirationMonth { get; set; }
        public string ccAuthReply_authorizedDateTime { get; set; }
        public string ccAuthReply_authorizationCode { get; set; }
        public string ccAuthReply_avsCodeRaw { get; set; }
        public string ccAuthReply_reasonCode { get; set; }
        public string ccAuthReply_cvCode { get; set; }
        public string ccAuthReply_avsCode { get; set; }
        public string ccAuthReply_processorResponse { get; set; }
        public string ccAuthReply_cvCodeRaw { get; set; }
        public string orderAmount_publicSignature { get; set; }
        public string orderPage_serialNumber { get; set; }
        public string orderCurrency { get; set; }
        public string orderPage_requestToken { get; set; }
        public string orderCurrency_publicSignature { get; set; }
        public string orderPage_transactionType { get; set; }
        public string orderNumber_publicSignature { get; set; }
        public string orderPage_environment { get; set; }
        public string reconciliationID { get; set; }
        public string signedDataPublicSignature { get; set; }
        public string transactionSignature { get; set; }
        public string decision_publicSignature { get; set; }
        public string merchantID { get; set; }
        public string orderNumber { get; set; }
    }

	[Persistent("AuditCyberSourcePayments")]
    public class XpoAuditCyberSourcePayments : XPLiteObject, IAuditCyberSourcePayments
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string frequestID;
        [Size(1000)]
        public string requestID
        {
            get { return frequestID; }
            set { SetPropertyValue<string>("requestID", ref frequestID, value); }
        }
        string fpymntPolicyNumber;
        [Size(400)]
        public string pymntPolicyNumber
        {
            get { return fpymntPolicyNumber; }
            set { SetPropertyValue<string>("pymntPolicyNumber", ref fpymntPolicyNumber, value); }
        }
        string fportalController;
        [Size(400)]
        public string portalController
        {
            get { return fportalController; }
            set { SetPropertyValue<string>("portalController", ref fportalController, value); }
        }
        string fpaymentOption;
        [Size(400)]
        public string paymentOption
        {
            get { return fpaymentOption; }
            set { SetPropertyValue<string>("paymentOption", ref fpaymentOption, value); }
        }
        double forderAmount;
        public double orderAmount
        {
            get { return forderAmount; }
            set { SetPropertyValue<double>("orderAmount", ref forderAmount, value); }
        }
        double fccAuthReply_amount;
        public double ccAuthReply_amount
        {
            get { return fccAuthReply_amount; }
            set { SetPropertyValue<double>("ccAuthReply_amount", ref fccAuthReply_amount, value); }
        }
        string fcard_cardType;
        [Size(1000)]
        public string card_cardType
        {
            get { return fcard_cardType; }
            set { SetPropertyValue<string>("card_cardType", ref fcard_cardType, value); }
        }
        string freasonCode;
        [Size(1000)]
        public string reasonCode
        {
            get { return freasonCode; }
            set { SetPropertyValue<string>("reasonCode", ref freasonCode, value); }
        }
        string fdecision;
        [Size(400)]
        public string decision
        {
            get { return fdecision; }
            set { SetPropertyValue<string>("decision", ref fdecision, value); }
        }
        string fbillTo_firstName;
        [Size(400)]
        public string billTo_firstName
        {
            get { return fbillTo_firstName; }
            set { SetPropertyValue<string>("billTo_firstName", ref fbillTo_firstName, value); }
        }
        string fbillTo_lastName;
        [Size(400)]
        public string billTo_lastName
        {
            get { return fbillTo_lastName; }
            set { SetPropertyValue<string>("billTo_lastName", ref fbillTo_lastName, value); }
        }
        string fbillTo_street1;
        [Size(400)]
        public string billTo_street1
        {
            get { return fbillTo_street1; }
            set { SetPropertyValue<string>("billTo_street1", ref fbillTo_street1, value); }
        }
        string fbillTo_city;
        [Size(400)]
        public string billTo_city
        {
            get { return fbillTo_city; }
            set { SetPropertyValue<string>("billTo_city", ref fbillTo_city, value); }
        }
        string fbillTo_state;
        [Size(400)]
        public string billTo_state
        {
            get { return fbillTo_state; }
            set { SetPropertyValue<string>("billTo_state", ref fbillTo_state, value); }
        }
        string fbillTo_postalCode;
        [Size(400)]
        public string billTo_postalCode
        {
            get { return fbillTo_postalCode; }
            set { SetPropertyValue<string>("billTo_postalCode", ref fbillTo_postalCode, value); }
        }
        string fbillTo_country;
        [Size(1000)]
        public string billTo_country
        {
            get { return fbillTo_country; }
            set { SetPropertyValue<string>("billTo_country", ref fbillTo_country, value); }
        }
        string fcard_expirationYear;
        [Size(400)]
        public string card_expirationYear
        {
            get { return fcard_expirationYear; }
            set { SetPropertyValue<string>("card_expirationYear", ref fcard_expirationYear, value); }
        }
        string fcard_expirationMonth;
        [Size(1000)]
        public string card_expirationMonth
        {
            get { return fcard_expirationMonth; }
            set { SetPropertyValue<string>("card_expirationMonth", ref fcard_expirationMonth, value); }
        }
        string fccAuthReply_authorizedDateTime;
        [Size(1000)]
        public string ccAuthReply_authorizedDateTime
        {
            get { return fccAuthReply_authorizedDateTime; }
            set { SetPropertyValue<string>("ccAuthReply_authorizedDateTime", ref fccAuthReply_authorizedDateTime, value); }
        }
        string fccAuthReply_authorizationCode;
        [Size(1000)]
        public string ccAuthReply_authorizationCode
        {
            get { return fccAuthReply_authorizationCode; }
            set { SetPropertyValue<string>("ccAuthReply_authorizationCode", ref fccAuthReply_authorizationCode, value); }
        }
        string fccAuthReply_avsCodeRaw;
        [Size(1000)]
        public string ccAuthReply_avsCodeRaw
        {
            get { return fccAuthReply_avsCodeRaw; }
            set { SetPropertyValue<string>("ccAuthReply_avsCodeRaw", ref fccAuthReply_avsCodeRaw, value); }
        }
        string fccAuthReply_reasonCode;
        [Size(1000)]
        public string ccAuthReply_reasonCode
        {
            get { return fccAuthReply_reasonCode; }
            set { SetPropertyValue<string>("ccAuthReply_reasonCode", ref fccAuthReply_reasonCode, value); }
        }
        string fccAuthReply_cvCode;
        [Size(400)]
        public string ccAuthReply_cvCode
        {
            get { return fccAuthReply_cvCode; }
            set { SetPropertyValue<string>("ccAuthReply_cvCode", ref fccAuthReply_cvCode, value); }
        }
        string fccAuthReply_avsCode;
        [Size(1000)]
        public string ccAuthReply_avsCode
        {
            get { return fccAuthReply_avsCode; }
            set { SetPropertyValue<string>("ccAuthReply_avsCode", ref fccAuthReply_avsCode, value); }
        }
        string fccAuthReply_processorResponse;
        [Size(400)]
        public string ccAuthReply_processorResponse
        {
            get { return fccAuthReply_processorResponse; }
            set { SetPropertyValue<string>("ccAuthReply_processorResponse", ref fccAuthReply_processorResponse, value); }
        }
        string fccAuthReply_cvCodeRaw;
        [Size(400)]
        public string ccAuthReply_cvCodeRaw
        {
            get { return fccAuthReply_cvCodeRaw; }
            set { SetPropertyValue<string>("ccAuthReply_cvCodeRaw", ref fccAuthReply_cvCodeRaw, value); }
        }
        string forderAmount_publicSignature;
        [Size(1000)]
        public string orderAmount_publicSignature
        {
            get { return forderAmount_publicSignature; }
            set { SetPropertyValue<string>("orderAmount_publicSignature", ref forderAmount_publicSignature, value); }
        }
        string forderPage_serialNumber;
        [Size(1000)]
        public string orderPage_serialNumber
        {
            get { return forderPage_serialNumber; }
            set { SetPropertyValue<string>("orderPage_serialNumber", ref forderPage_serialNumber, value); }
        }
        string forderCurrency;
        [Size(40)]
        public string orderCurrency
        {
            get { return forderCurrency; }
            set { SetPropertyValue<string>("orderCurrency", ref forderCurrency, value); }
        }
        string forderPage_requestToken;
        [Size(1000)]
        public string orderPage_requestToken
        {
            get { return forderPage_requestToken; }
            set { SetPropertyValue<string>("orderPage_requestToken", ref forderPage_requestToken, value); }
        }
        string forderCurrency_publicSignature;
        [Size(1000)]
        public string orderCurrency_publicSignature
        {
            get { return forderCurrency_publicSignature; }
            set { SetPropertyValue<string>("orderCurrency_publicSignature", ref forderCurrency_publicSignature, value); }
        }
        string forderPage_transactionType;
        [Size(400)]
        public string orderPage_transactionType
        {
            get { return forderPage_transactionType; }
            set { SetPropertyValue<string>("orderPage_transactionType", ref forderPage_transactionType, value); }
        }
        string forderNumber_publicSignature;
        [Size(1000)]
        public string orderNumber_publicSignature
        {
            get { return forderNumber_publicSignature; }
            set { SetPropertyValue<string>("orderNumber_publicSignature", ref forderNumber_publicSignature, value); }
        }
        string forderPage_environment;
        [Size(1000)]
        public string orderPage_environment
        {
            get { return forderPage_environment; }
            set { SetPropertyValue<string>("orderPage_environment", ref forderPage_environment, value); }
        }
        string freconciliationID;
        public string reconciliationID
        {
            get { return freconciliationID; }
            set { SetPropertyValue<string>("reconciliationID", ref freconciliationID, value); }
        }
        string fsignedDataPublicSignature;
        [Size(400)]
        public string signedDataPublicSignature
        {
            get { return fsignedDataPublicSignature; }
            set { SetPropertyValue<string>("signedDataPublicSignature", ref fsignedDataPublicSignature, value); }
        }
        string ftransactionSignature;
        [Size(1000)]
        public string transactionSignature
        {
            get { return ftransactionSignature; }
            set { SetPropertyValue<string>("transactionSignature", ref ftransactionSignature, value); }
        }
        string fdecision_publicSignature;
        [Size(1000)]
        public string decision_publicSignature
        {
            get { return fdecision_publicSignature; }
            set { SetPropertyValue<string>("decision_publicSignature", ref fdecision_publicSignature, value); }
        }
        string fmerchantID;
        [Size(1000)]
        public string merchantID
        {
            get { return fmerchantID; }
            set { SetPropertyValue<string>("merchantID", ref fmerchantID, value); }
        }
        string forderNumber;
        [Size(1000)]
        public string orderNumber
        {
            get { return forderNumber; }
            set { SetPropertyValue<string>("orderNumber", ref forderNumber, value); }
        }
        public XpoAuditCyberSourcePayments(Session session) : base(session) { }
        public XpoAuditCyberSourcePayments() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
