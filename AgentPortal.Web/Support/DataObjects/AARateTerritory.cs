﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAARateTerritory
    {
        int IndexID { get; set; }
        string ZIP { get; set; }
        string County { get; set; }
        string OHCTerritory { get; set; }
        string RateCycle { get; set; }
        string PDFFileName { get; set; }
        double PageNo { get; set; }
        DateTime DateCreated { get; set; }
        string SupportArea { get; set; }
        string UWArea { get; set; }
        bool HasLowerDownPayment { get; set; }
    }

    public class AARateTerritory : IAARateTerritory
    {
        public int IndexID { get; set; }
        public string ZIP { get; set; }
        public string County { get; set; }
        public string OHCTerritory { get; set; }
        public string RateCycle { get; set; }
        public string PDFFileName { get; set; }
        public double PageNo { get; set; }
        public DateTime DateCreated { get; set; }
        public string SupportArea { get; set; }
        public string UWArea { get; set; }
        public bool HasLowerDownPayment { get; set; }
    }

	[Persistent("AARateTerritory")]
    public class XpoAARateTerritory : XPLiteObject, IAARateTerritory
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>(nameof(IndexID), ref fIndexID, value); }
        }
        string fZIP;
        [Size(10)]
        public string ZIP
        {
            get { return fZIP; }
            set { SetPropertyValue<string>(nameof(ZIP), ref fZIP, value); }
        }
        string fCounty;
        [Size(255)]
        public string County
        {
            get { return fCounty; }
            set { SetPropertyValue<string>(nameof(County), ref fCounty, value); }
        }
        string fOHCTerritory;
        [Size(5)]
        public string OHCTerritory
        {
            get { return fOHCTerritory; }
            set { SetPropertyValue<string>(nameof(OHCTerritory), ref fOHCTerritory, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>(nameof(RateCycle), ref fRateCycle, value); }
        }
        string fPDFFileName;
        [Size(500)]
        public string PDFFileName
        {
            get { return fPDFFileName; }
            set { SetPropertyValue<string>(nameof(PDFFileName), ref fPDFFileName, value); }
        }
        double fPageNo;
        public double PageNo
        {
            get { return fPageNo; }
            set { SetPropertyValue<double>(nameof(PageNo), ref fPageNo, value); }
        }
        DateTime fDateCreated;
        public DateTime DateCreated
        {
            get { return fDateCreated; }
            set { SetPropertyValue<DateTime>(nameof(DateCreated), ref fDateCreated, value); }
        }

        string fSupportArea;
        public string SupportArea
        {
            get { return fSupportArea; }
            set { SetPropertyValue<string>("SupportArea", ref fSupportArea, value); }
        }

        string fUWArea;
        public string UWArea
        {
            get { return fUWArea; }
            set { SetPropertyValue<string>("UWArea", ref fUWArea, value); }
        }

        bool hasLowerDownPayment;
        public bool HasLowerDownPayment
        {
            get { return hasLowerDownPayment; }
            set { SetPropertyValue<bool>("HasLowerDownPayment", ref hasLowerDownPayment, value); }
        }

        public XpoAARateTerritory(Session session) : base(session) { }
        public XpoAARateTerritory() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }
}