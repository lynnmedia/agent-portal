using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class PolicyAltBankInfo : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fPolicyNo;
        public string PolicyNo
        {
            get { return fPolicyNo; }
            set { SetPropertyValue<string>("PolicyNo", ref fPolicyNo, value); }
        }
        string fBankName;
        [Size(300)]
        public string BankName
        {
            get { return fBankName; }
            set { SetPropertyValue<string>("BankName", ref fBankName, value); }
        }
        string fAcctHolderName;
        [Size(200)]
        public string AcctHolderName
        {
            get { return fAcctHolderName; }
            set { SetPropertyValue<string>("AcctHolderName", ref fAcctHolderName, value); }
        }
        string fAccount;
        [Size(400)]
        public string Account
        {
            get { return fAccount; }
            set { SetPropertyValue<string>("Account", ref fAccount, value); }
        }
        string fAccountType;
        [Size(300)]
        public string AccountType
        {
            get { return fAccountType; }
            set { SetPropertyValue<string>("AccountType", ref fAccountType, value); }
        }
        string fABA;
        [Size(300)]
        public string ABA
        {
            get { return fABA; }
            set { SetPropertyValue<string>("ABA", ref fABA, value); }
        }
        string fAccountNumber;
        [Size(400)]
        public string AccountNumber
        {
            get { return fAccountNumber; }
            set { SetPropertyValue<string>("AccountNumber", ref fAccountNumber, value); }
        }
        public PolicyAltBankInfo(Session session) : base(session) { }
        public PolicyAltBankInfo() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
