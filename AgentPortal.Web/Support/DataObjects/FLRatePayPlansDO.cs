﻿using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    [Persistent("FLRatePayPlans")]
    public class FLRatePayPlansDO : XPLiteObject
    {
        int fPayPlan;
        public int PayPlan
        {
            get { return fPayPlan; }
            set { SetPropertyValue<int>("PayPlan", ref fPayPlan, value); }
        }
        string fRateCycle;
        [Size(25)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        int fPolicyTerm;
        public int PolicyTerm
        {
            get { return fPolicyTerm; }
            set { SetPropertyValue<int>("PolicyTerm", ref fPolicyTerm, value); }
        }
        bool fRenewal;
        public bool Renewal
        {
            get { return fRenewal; }
            set { SetPropertyValue<bool>("Renewal", ref fRenewal, value); }
        }
        double fDownPayment;
        public double DownPayment
        {
            get { return fDownPayment; }
            set { SetPropertyValue<double>("DownPayment", ref fDownPayment, value); }
        }
        int fInstallCount;
        public int InstallCount
        {
            get { return fInstallCount; }
            set { SetPropertyValue<int>("InstallCount", ref fInstallCount, value); }
        }
        int fDaysFor1st;
        public int DaysFor1st
        {
            get { return fDaysFor1st; }
            set { SetPropertyValue<int>("DaysFor1st", ref fDaysFor1st, value); }
        }
        int fDaysOther;
        public int DaysOther
        {
            get { return fDaysOther; }
            set { SetPropertyValue<int>("DaysOther", ref fDaysOther, value); }
        }
        int fRenewalPlan;
        public int RenewalPlan
        {
            get { return fRenewalPlan; }
            set { SetPropertyValue<int>("RenewalPlan", ref fRenewalPlan, value); }
        }
        bool fEFT;
        public bool EFT
        {
            get { return fEFT; }
            set { SetPropertyValue<bool>("EFT", ref fEFT, value); }
        }
        bool fDirectBill;
        public bool DirectBill
        {
            get { return fDirectBill; }
            set { SetPropertyValue<bool>("DirectBill", ref fDirectBill, value); }
        }
        bool fReqPriorBI;
        public bool ReqPriorBI
        {
            get { return fReqPriorBI; }
            set { SetPropertyValue<bool>("ReqPriorBI", ref fReqPriorBI, value); }
        }
        bool fReqEFT;
        public bool ReqEFT
        {
            get { return fReqEFT; }
            set { SetPropertyValue<bool>("ReqEFT", ref fReqEFT, value); }
        }
        bool fReqEFTorBI;
        public bool ReqEFTorBI
        {
            get { return fReqEFTorBI; }
            set { SetPropertyValue<bool>("ReqEFTorBI", ref fReqEFTorBI, value); }
        }
        bool fPaidInFullDisc;
        public bool PaidInFullDisc
        {
            get { return fPaidInFullDisc; }
            set { SetPropertyValue<bool>("PaidInFullDisc", ref fPaidInFullDisc, value); }
        }
        bool fRenewalOnly;
        public bool RenewalOnly
        {
            get { return fRenewalOnly; }
            set { SetPropertyValue<bool>("RenewalOnly", ref fRenewalOnly, value); }
        }
        double fPayPlanFactor;
        public double PayPlanFactor
        {
            get { return fPayPlanFactor; }
            set { SetPropertyValue<double>("PayPlanFactor", ref fPayPlanFactor, value); }
        }
        int fID;
        [Key(true)]
        public int ID
        {
            get { return fID; }
            set { SetPropertyValue<int>("ID", ref fID, value); }
        }
        bool fActive;
        public bool Active
        {
            get { return fActive; }
            set { SetPropertyValue<bool>("Active", ref fActive, value); }
        }
        public FLRatePayPlansDO(Session session) : base(session) { }
        public FLRatePayPlansDO() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); FLRatePayPlansDO.AutoSaveOnEndEdit = false; }
    }
}