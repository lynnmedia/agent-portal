﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public class ViolationCodes_SVC : XPLiteObject
    {

        private int fIndexID;
        private double svc_cat;
        private string svc_summary;
        private string svc_additional;
        public string svc_lob;
        public string svc;
        public string description;
        public string state;
        public string code;
        public string points;
        public string duration;
        private double sampleScore;
        private double sampleDuration;
        private string aaiccode;
        private string rateCycle;
        private DateTime dateCreated;

        public ViolationCodes_SVC(Session session) : base(session) { }
        public ViolationCodes_SVC() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }

        public double SVC_CAT
        {
            get { return svc_cat; }
            set { SetPropertyValue<double>("SVC_CAT", ref svc_cat, value); }
        }

        public string SVC_SUMMARY
        {
            get { return svc_summary; }
            set { SetPropertyValue<string>("SVC_SUMMARY", ref svc_summary, value); }
        }

        public string SVC_ADDITIONAL
        {
            get { return svc_additional; }
            set { SetPropertyValue<string>("SVC_ADDITIONAL", ref svc_additional, value); }
        }

        public string SVC_LOB
        {
            get { return svc_lob; }
            set { SetPropertyValue<string>("SVC_LOB", ref svc_lob, value); }
        }

        public string SVC
        {
            get { return svc; }
            set { SetPropertyValue<string>("SVC", ref svc, value); }
        }

        public string DESCRIPTION
        {
            get { return description; }
            set { SetPropertyValue<string>("DESCRIPTION", ref description, value); }
        }

        public string State
        {
            get { return state; }
            set { SetPropertyValue<string>("State", ref state, value); }
        }

        public string Code
        {
            get { return code; }
            set { SetPropertyValue<string>("Code", ref code, value); }
        }

        public string Points
        {
            get { return points; }
            set { SetPropertyValue<string>("Points", ref points, value); }
        }

        public string Duration
        {
            get { return duration; }
            set { SetPropertyValue<string>("Duration", ref duration, value); }
        }

        public double SampleScore
        {
            get { return sampleScore; }
            set { SetPropertyValue<double>("SampleScore", ref sampleScore, value); }
        }

        public double SampleDuration
        {
            get { return sampleDuration; }
            set { SetPropertyValue<double>("SampleDuration", ref sampleDuration, value); }
        }

        public string AAICCode
        {
            get { return aaiccode; }
            set { SetPropertyValue<string>("AAICCode", ref aaiccode, value); }
        }

        public string RateCycle
        {
            get { return rateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref rateCycle, value); }
        }

        public DateTime DateCreated
        {
            get { return dateCreated; }
            set { SetPropertyValue<DateTime>("DateCreated", ref dateCreated, value); }
        }
    }
}