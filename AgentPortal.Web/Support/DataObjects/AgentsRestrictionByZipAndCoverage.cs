﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IAgentsRestrictionByZipAndCoverage
    {
        int IndexID { get; set; }
        string AgentCode { get; set; }
        string Zip { get; set; }
        string County { get; set; }
        string Territory { get; set; }
        string RestrictedCoverageName { get; set; }
        bool IsRestricted { get; set; }
        DateTime DateAdded { get; set; }
    }

    public class AgentsRestrictionByZipAndCoverage : IAgentsRestrictionByZipAndCoverage
    {
        public int IndexID { get; set; }
        public string AgentCode { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
        public string Territory { get; set; }
        public string RestrictedCoverageName { get; set; }
        public bool IsRestricted { get; set; }
        public DateTime DateAdded { get; set; }
    }

	[Persistent("AgentsRestrictionByZipAndCoverage")]
    public class XpoAgentsRestrictionByZipAndCoverage : XPLiteObject, IAgentsRestrictionByZipAndCoverage
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>(nameof(IndexID), ref fIndexID, value); }
        }

        string fAgentCode;
        [Size(15)]
        public string AgentCode
        {
            get { return fAgentCode; }
            set { SetPropertyValue<string>("AgentCode", ref fAgentCode, value); }
        }

        string fZip;
        [Size(10)]
        public string Zip
        {
            get { return fZip; }
            set { SetPropertyValue<string>(nameof(Zip), ref fZip, value); }
        }
        string fCounty;
        [Size(255)]
        public string County
        {
            get { return fCounty; }
            set { SetPropertyValue<string>(nameof(County), ref fCounty, value); }
        }
        string fTerritory;
        [Size(5)]
        public string Territory
        {
            get { return fTerritory; }
            set { SetPropertyValue<string>(nameof(Territory), ref fTerritory, value); }
        }
        string fRestrictedCoverageName;
        [Size(100)]
        public string RestrictedCoverageName
        {
            get { return fRestrictedCoverageName; }
            set { SetPropertyValue<string>(nameof(RestrictedCoverageName), ref fRestrictedCoverageName, value); }
        }
        bool fIsRestricted;
        public bool IsRestricted
        {
            get { return fIsRestricted; }
            set { SetPropertyValue<bool>("IsRestricted", ref fIsRestricted, value); }
        }
        DateTime fDateAdded;
        public DateTime DateAdded
        {
            get { return fDateAdded; }
            set { SetPropertyValue<DateTime>(nameof(DateAdded), ref fDateAdded, value); }
        }

        public XpoAgentsRestrictionByZipAndCoverage(Session session) : base(session) { }
        public XpoAgentsRestrictionByZipAndCoverage() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

    }
}