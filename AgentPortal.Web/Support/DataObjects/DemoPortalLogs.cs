using System;
using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{
    public interface IDemoPortalLogs
    {
        int IndexID { get; set; }
        string IPAddress { get; set; }
        DateTime DateOfLog { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
    }

    public class DemoPortalLogs : IDemoPortalLogs
    {
        public int IndexID { get; set; }
        public string IPAddress { get; set; }
        public DateTime DateOfLog { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    [Persistent("DemoPortalLogs")]
    public class XpoDemoPortalLogs : XPCustomObject, IDemoPortalLogs
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }

        string fIPAddress;
        public string IPAddress
        {
            get { return fIPAddress; }
            set { SetPropertyValue<string>("IPAddress", ref fIPAddress, value); }
        }
        DateTime fDateOfLog;
        public DateTime DateOfLog
        {
            get { return fDateOfLog; }
            set { SetPropertyValue<DateTime>("DateOfLog", ref fDateOfLog, value); }
        }
        string fUserName;
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>("UserName", ref fUserName, value); }
        }
        string fPassword;
        public string Password
        {
            get { return fPassword; }
            set { SetPropertyValue<string>("Password", ref fPassword, value); }
        }
        public XpoDemoPortalLogs()
            : base()
        {
           
        }

        public XpoDemoPortalLogs(Session session)
            : base(session)
        {
            
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }

}