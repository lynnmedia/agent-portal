using DevExpress.Xpo;

namespace AgentPortal.Support.DataObjects
{

    public class FLRateBaseRates : XPLiteObject
    {
        int fIndexID;
        [Key(true)]
        public int IndexID
        {
            get { return fIndexID; }
            set { SetPropertyValue<int>("IndexID", ref fIndexID, value); }
        }
        string fZip;
        [Size(5)]
        public string Zip
        {
            get { return fZip; }
            set { SetPropertyValue<string>("Zip", ref fZip, value); }
        }
        string fRateCycle;
        [Size(20)]
        public string RateCycle
        {
            get { return fRateCycle; }
            set { SetPropertyValue<string>("RateCycle", ref fRateCycle, value); }
        }
        string fCity;
        [Size(35)]
        public string City
        {
            get { return fCity; }
            set { SetPropertyValue<string>("City", ref fCity, value); }
        }
        string fCounty;
        [Size(35)]
        public string County
        {
            get { return fCounty; }
            set { SetPropertyValue<string>("County", ref fCounty, value); }
        }
        double fBIFactor;
        public double BIFactor
        {
            get { return fBIFactor; }
            set { SetPropertyValue<double>("BIFactor", ref fBIFactor, value); }
        }
        double fPDFactor;
        public double PDFactor
        {
            get { return fPDFactor; }
            set { SetPropertyValue<double>("PDFactor", ref fPDFactor, value); }
        }
        double fPIPFactor;
        public double PIPFactor
        {
            get { return fPIPFactor; }
            set { SetPropertyValue<double>("PIPFactor", ref fPIPFactor, value); }
        }
        double fMPFactor;
        public double MPFactor
        {
            get { return fMPFactor; }
            set { SetPropertyValue<double>("MPFactor", ref fMPFactor, value); }
        }
        double fUMFactor;
        public double UMFactor
        {
            get { return fUMFactor; }
            set { SetPropertyValue<double>("UMFactor", ref fUMFactor, value); }
        }
        double fCompFactor;
        public double CompFactor
        {
            get { return fCompFactor; }
            set { SetPropertyValue<double>("CompFactor", ref fCompFactor, value); }
        }
        double fCollFactor;
        public double CollFactor
        {
            get { return fCollFactor; }
            set { SetPropertyValue<double>("CollFactor", ref fCollFactor, value); }
        }
        double fBITerr;
        public double BITerr
        {
            get { return fBITerr; }
            set { SetPropertyValue<double>("BITerr", ref fBITerr, value); }
        }
        double fPDTerr;
        public double PDTerr
        {
            get { return fPDTerr; }
            set { SetPropertyValue<double>("PDTerr", ref fPDTerr, value); }
        }
        double fPIPTerr;
        public double PIPTerr
        {
            get { return fPIPTerr; }
            set { SetPropertyValue<double>("PIPTerr", ref fPIPTerr, value); }
        }
        double fMPTerr;
        public double MPTerr
        {
            get { return fMPTerr; }
            set { SetPropertyValue<double>("MPTerr", ref fMPTerr, value); }
        }
        double fUMTerr;
        public double UMTerr
        {
            get { return fUMTerr; }
            set { SetPropertyValue<double>("UMTerr", ref fUMTerr, value); }
        }
        double fCOMPTerr;
        public double COMPTerr
        {
            get { return fCOMPTerr; }
            set { SetPropertyValue<double>("COMPTerr", ref fCOMPTerr, value); }
        }
        double fCOLLTerr;
        public double COLLTerr
        {
            get { return fCOLLTerr; }
            set { SetPropertyValue<double>("COLLTerr", ref fCOLLTerr, value); }
        }
        public FLRateBaseRates(Session session) : base(session) { }
        public FLRateBaseRates() : base(Session.DefaultSession) { }
        public override void AfterConstruction() { base.AfterConstruction(); }
    }


}
