﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Support.Enums
{
    public enum TransferDiscountOptions
    {
        None = 0,
        SomeDriversLessThan3Years,
        AllDriversMoreThan3Years,
        AgentAndDriversMoreThan3Years
    }
}