﻿namespace AgentPortal.Support.Enums
{
  public enum DErrors
  {
      NoRenQuote = 1,
      NoRenInstalls = 2,
      NoPolicy = 3,
      NoPolicyQuote = 4,
      NoDrivers = 5,
      NoVehicles = 6,
      NoQuoteDrivers = 7,
      NoQuoteVehicles = 8,
      LostSession = 9,
      SweepMadeSameDay = 10,
      VendorPymntErrCode = 11
  }
}