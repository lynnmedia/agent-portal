﻿namespace AgentPortal.Support.Enums

{
    public static class WorkFlowEnums
    {
        public enum WorkFlowConditions
        {
            DoesPolicyExists = 1,
            IsPolicyExpired = 2,
            IsPolicyCancelled = 3,
            DoesClaimExists = 4,
            DoesPerilExists = 5,
            IsClaimOpen = 6,
            DoesAgentExists = 7
        }
        public enum WorkFlowActions
        {
            AssignDiary = 1           
        }
    }
}
