﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgentPortal.Support.Enums
{
    public enum AllowedAgentEndorsements
    {
        NAME = 0,
        ADDRESS,
        HOME_PHONE,
        WORK_PHONE,
        INSURED_PREFERRED_LANGUAGE,
        ADD_VEHICLE_NO_COMP_NO_COLL,
        ADD_UPDATE_DRIVER,
        EMAIL
    }
}