﻿using System;

namespace AgentPortal.Support
{
    public class Mapping
    {
        /// <summary>
        /// Copies properties from source object to destination object
        /// 
        /// For each property in the source which has a corresponding
        /// property in the destination -- same name and type -- the
        /// value is copied from source to destination
        /// 
        /// Inspired from similar methods from people online, rewritten
        /// for the need here.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TDest"></typeparam>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        public static TDest MapProperties<TSource, TDest>(TSource source, TDest dest)
                                    where TSource : class
                                    where TDest : class
        {
            if (source == null)
            {
                return null;
            }

            var sourceType = source.GetType();
            var destType = dest.GetType();
            foreach (var sourceProperty in sourceType.GetProperties(System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.Public|System.Reflection.BindingFlags.DeclaredOnly))
            {
                var destProperty = destType.GetProperty(sourceProperty.Name);
                if (destProperty != null && destProperty.GetType() == sourceProperty.GetType())
                {
                    destProperty.SetValue(dest, sourceProperty.GetValue(source));
                }

            }

            return dest;
        }
    }
}