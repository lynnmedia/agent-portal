﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AgentPortal.Models;
using PalmInsure.Palms.Utilities;

namespace AgentPortal.Support.Attributes
{
    public class ValidDriverDobAttribute : ValidationAttribute, IClientValidatable
    {
        public int driverIndex;
        public ValidDriverDobAttribute()
        {
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                Console.WriteLine("Value: " + value.ToString());
                string dateValue = (string) value;
                DateTime dtV = dateValue.Parse<DateTime>();
                long lTicks = DateTime.Now.Ticks - dtV.Ticks;
                DateTime dtAge = new DateTime(lTicks);
                string sErrorMessage = "";
                //Removed as per PBI 2582
                //if (driverIndex == 1 && !(dtAge.Year >= 18))
                //{
                //    sErrorMessage = "Minimum age for Named Insured is 18.";
                //    return new ValidationResult(sErrorMessage);
                //}

                //if (!(dtAge.Year >= 14))
                //{
                //    sErrorMessage = "Minimum age for a driver is 14.";
                //    return new ValidationResult(sErrorMessage);
                //}
            }


            return ValidationResult.Success;
        } 

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            int drvIndex = 0;
            try
            {
                DriverModel driver = (DriverModel)context.Controller.ViewData.Model;
                drvIndex = driver.DriverIndex;
            }
            catch
            {
                EndorsementDriverModel enddriver = (EndorsementDriverModel)context.Controller.ViewData.Model;
                //TODO: determine why the driver would be null for a validation. Probably a model issue
                if (enddriver == null)
                    drvIndex = 0;
                else
                    drvIndex = enddriver.DriverIndex;
            }

            if (drvIndex == 0)
            {
                if (context?.Controller?.ControllerContext?.Controller.TempData["DriverCount"] != null)
                {
                    drvIndex = (int)context.Controller.ControllerContext.Controller.TempData["DriverCount"];
                }
            }
            driverIndex = drvIndex;

            ModelClientValidationRule mcvrTwo = new ModelClientValidationRule();
            mcvrTwo.ValidationType            = "checkage";
            ////Updated as per PBI 2582
            //mcvrTwo.ErrorMessage              = "Minimum age for a driver is 14.";
            mcvrTwo.ValidationParameters.Add("param", DateTime.Now.ToString("dd/MM/yyyy") + "/" + drvIndex.ToString());
            return new List<ModelClientValidationRule> { mcvrTwo };
        }
    }//END Class
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}//END Namespace