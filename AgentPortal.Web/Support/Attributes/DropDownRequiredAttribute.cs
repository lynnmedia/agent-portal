﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Web.Mvc;
//using System.Globalization;
//using System.ComponentModel;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Web.Mvc;
//using PalmInsure.Palms.Utilities;
//using System.Linq;
//using System.Globalization;

//namespace DemoWebPortal.Support.Attributes
//{
//    public class DropDownRequiredAttribute : ValidationAttribute
//    {
//        private const string _defaultErrorMessage = "{0} is not a valid {1}";
//        private string CompareValue { get; set; }
//        private string CompareField { get; set; }

//        public DropDownRequiredAttribute(string tcompareValue, string tcompareField)
//            : base(_defaultErrorMessage)
//        {
//            CompareValue = tcompareValue;
//            CompareField = tcompareField;
//        }

//        public override string FormatErrorMessage(string name)
//        {
//            return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString, CompareValue, CompareField);
//        }

//        public override bool IsValid(object value)
//        {
//            if (value == null || !(value is String))
//            {
//                return false;
//            }
//            string gender = (string)value;

//            bool returnVal = gender.ToUpper() == "SELECT" ? false : true;
//            return returnVal;
//        }

//        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
//        {
//            return new[]
//            {
//                new ModelClientValidationDropDownRequiredRule(  FormatErrorMessage(metadata.GetDisplayName()), CompareValue, CompareField)
//            };
//        }
//    }

//    public class ModelClientValidationDropDownRequiredRule : ModelClientValidationRule
//    {
//        public ModelClientValidationDropDownRequiredRule(string errorMessage, string tcompareValue, string tcompareField)
//        {
//            ErrorMessage = errorMessage;
//            ValidationType = "dropDownRequired";

//            ValidationParameters["compareVal"] = tcompareValue;
//            ValidationParameters["compareField"] = tcompareField;
//        }
//    }
//}