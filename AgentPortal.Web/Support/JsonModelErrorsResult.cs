﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgentPortal.Support
{
    public struct FieldError
    {
        public string Key;
        public string Message;
    }//END FieldError
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public class JsonModelErrorsResult : JsonResult
    {
        public JsonModelErrorsResult(ModelStateDictionary modelState)
        {
            var errors = modelState.Where(x => x.Value.Errors.Count > 0);

            List<FieldError> output = new List<FieldError>();

            foreach (var field in errors)
            {
                foreach (var error in field.Value.Errors)
                {
                    string emsg = error.ErrorMessage;

                    if (String.IsNullOrEmpty(emsg))
                    {
                        emsg = error.Exception.Message;
                    }

                    output.Add(new FieldError()
                    {
                        Key = field.Key,
                        Message = emsg
                    });
                }
            }//end loop through errors

            if (output.Count > 0) 
            {
                HttpContext.Current.Response.StatusCode = 400;
            }

            Data = new { errors = output };
        }
    }//END JsonModelErrorsResult
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}//END Namespace