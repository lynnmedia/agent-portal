﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities.Extensions;

namespace AgentPortal.Support
{
    public class FirstData : IPaymentProcessor
    {
        public string CardNo { get; set; }
        public string Cvv { get; set; }
        public DateTime CardExpDate { get; set; }
        public string CardExpYear { get; set; }
        public string CardExpMonth { get; set; }
        public string CheckAcctNo { get; set; }
        public string CheckRoutingNo { get; set; }
        public double Amount { get; set; }
        public string ResponseCode { get; set; }
        public bool HasErrors { get; set; }
        public string ErrMsg { get; set; }
        public string PayorName { get; set; }
        public string TransId { get; set; }
        public string OrderId { get; set; }
        public string PolicyNo { get; set; }

        public void PostCredit()
        {
            var string_builder = new StringBuilder();
            using (var string_writer = new StringWriter(string_builder))
            {
                using (var xml_writer = new XmlTextWriter(string_writer))
                {   //build XML string 
                    xml_writer.Formatting = Formatting.Indented;
                    xml_writer.WriteStartElement("Transaction");
                    xml_writer.WriteElementString("ExactID", ConfigSettings.ReadSetting("ExactID"));//Gateway ID
                    xml_writer.WriteElementString("Password", ConfigSettings.ReadSetting("FDPass"));//Password
                    xml_writer.WriteElementString("Transaction_Type", "00");
                    xml_writer.WriteElementString("DollarAmount", Amount.ToString(CultureInfo.CurrentCulture));
                    xml_writer.WriteElementString("Expiry_Date", CardExpMonth + CardExpYear);
                    xml_writer.WriteElementString("CardHoldersName", PayorName);
                    xml_writer.WriteElementString("Card_Number", CardNo);
                    xml_writer.WriteEndElement();
                }
            }
            string xml_string = string_builder.ToString();

            //SHA1 hash on XML string
            var encoder = new ASCIIEncoding();
            byte[] xml_byte = encoder.GetBytes(xml_string);
            var sha1_crypto = new SHA1CryptoServiceProvider();
            string hash = BitConverter.ToString(sha1_crypto.ComputeHash(xml_byte)).Replace("-", "");
            string hashed_content = hash.ToLower();

            //assign values to hashing and header variables
            string keyID = ConfigSettings.ReadSetting("KeyID");//key ID
            string key = ConfigSettings.ReadSetting("HMACKey");//Hmac key
            string method = "POST\n";
            string type = "application/xml";//REST XML
            string time = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
            string uri = "/transaction/v13";
            string hash_data = method + type + "\n" + hashed_content + "\n" + time + "\n" + uri;
            //hmac sha1 hash with key + hash_data
            HMAC hmac_sha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key)); //key
            byte[] hmac_data = hmac_sha1.ComputeHash(Encoding.UTF8.GetBytes(hash_data)); //data
            //base64 encode on hmac_data
            string base64_hash = Convert.ToBase64String(hmac_data);
            string url = ConfigSettings.ReadSetting("FDEndPointURL") + uri; // End Point

            //begin HttpWebRequest 
            var web_request = (HttpWebRequest)WebRequest.Create(url);
            web_request.Method = "POST";
            web_request.ContentType = type;
            web_request.Accept = "*/*";
            web_request.Headers.Add("x-gge4-date", time);
            web_request.Headers.Add("x-gge4-content-sha1", hashed_content);
            web_request.Headers.Add("Authorization", "GGE4_API " + keyID + ":" + base64_hash);
            web_request.ContentLength = xml_string.Length;

            // write and send request data 
            using (var stream_writer = new StreamWriter(web_request.GetRequestStream()))
            {
                stream_writer.Write(xml_string);
            }

            //get response and read into string
            string response_string;
            try
            {
                using (var web_response = (HttpWebResponse)web_request.GetResponse())
                {
                    using (var response_stream = new StreamReader(web_response.GetResponseStream()))
                    {
                        response_string = response_stream.ReadToEnd();
                    }
                    //load xml
                    var xmldoc = new XmlDocument();
                    xmldoc.LoadXml(response_string);
                    XmlNodeList nodelist = xmldoc.SelectNodes("TransactionResult");
                    if (nodelist != null)
                    {
                        foreach (XmlNode node in nodelist)
                        {
                            ResponseCode = node.SelectSingleNode("Bank_Message").InnerText;
                        }
                    }
                    if (ResponseCode.ToUpper().CompareTo("APPROVED") == 0)
                    {
                        HasErrors = false;
                        ErrMsg = "APPROVED";
                        AuditCreditPayment(nodelist, CardNo, PayorName, PolicyNo);
                    }
                    else
                    {
                        AuditFailedCreditPayment(ResponseCode, CardNo, PayorName, PolicyNo);
                    }
                }
            }
            //read stream for remote error response
            catch (WebException ex)
            {
                if (ex.Response != null)
                {
                    using (var error_response = (HttpWebResponse)ex.Response)
                    {
                        using (var reader = new StreamReader(error_response.GetResponseStream()))
                        {
                            HasErrors = true;
                            ResponseCode = "FAILURE";
                            string remote_ex = reader.ReadToEnd();
                            ErrMsg = remote_ex;
                            AuditFailedCreditPayment(ErrMsg, CardNo, PayorName, PolicyNo);
                        }
                    }
                }
            }
        }

        public void PostChecking()
        {
            throw new NotImplementedException();
        }

        public void VoidPayment()
        {
            throw new NotImplementedException();
        }

        public void AuditCreditPayment()
        {
            throw new NotImplementedException();
        }


        public void AuditFailedCreditPayment(string error, string lastFour, string nameOnCard, string policyNo)
        {

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var audit = new XpoAuditFirstDataPaymentsDo();
                HasErrors = true;
                audit.CommercialServiceProvider = "FirstData";
                audit.ErrorMessage = error;
                audit.PolicyNo = policyNo;
                PolicyNo = policyNo;
                int index = error.IndexOf("-");
                string transResult = "";
                if (index != -1)
                {
                    transResult = error.Substring(index + 1);
                }
                else
                {
                    transResult = error;
                }
                ErrMsg = transResult;
                audit.TransactionResult = transResult;
                audit.NameOnCard = nameOnCard;
                audit.TDate = DateTime.Now.ToShortDateString();
                audit.TransactionTime = DateTime.Now.ToShortTimeString();
                audit.LastFourOfCard = lastFour.Last(4);
                audit.DateCreated = DateTime.Now;
                audit.Save();
                uow.CommitChanges();
            }
        }
        public void AuditCreditPayment(XmlNodeList nodelist, string lastFour, string nameOnCard, string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var audit = new XpoAuditFirstDataPaymentsDo(uow);
                if (nodelist != null)
                {
                    foreach (XmlNode node in nodelist)
                    {
                        audit.TransactionID = node.SelectSingleNode("Transaction_Tag").InnerText;
                        audit.ProcessorApprovalCode = node.SelectSingleNode("Authorization_Num").InnerText;
                        audit.CommercialServiceProvider = "First Data";
                        audit.ProcessorResponseCode = node.SelectSingleNode("Bank_Resp_Code").InnerText;
                        audit.ProcessorResponseMessage = node.SelectSingleNode("Bank_Message").InnerText;
                        audit.FraudAction = node.SelectSingleNode("FraudSuspected").InnerText;
                    }
                }
                OrderId = audit.ProcessorApprovalCode;
                audit.ProcessorReferenceNumber = audit.TransactionID;
                audit.ApprovalCode = audit.ProcessorApprovalCode;
                audit.TDate = DateTime.Now.ToShortDateString();
                audit.TransactionTime = DateTime.Now.ToShortTimeString();
                audit.TransactionResult = audit.ProcessorResponseMessage;
                audit.LastFourOfCard = lastFour.Last(4);
                audit.NameOnCard = nameOnCard;
                audit.DateCreated = DateTime.Now;
                audit.PolicyNo = policyno;
                PolicyNo = policyno;
                audit.Save();
                uow.CommitChanges();
            }
        }
    }//END Class

    public class CyberSource : IPaymentProcessor
    {
        public string CardNo { get; set; }
        public string Cvv { get; set; }
        public DateTime CardExpDate { get; set; }
        public string CardExpYear { get; set; }
        public string CardExpMonth { get; set; }
        public string CheckAcctNo { get; set; }
        public string CheckRoutingNo { get; set; }
        public double Amount { get; set; }
        public string ResponseCode { get; set; }
        public bool HasErrors { get; set; }
        public string ErrMsg { get; set; }
        public string PayorName { get; set; }
        public string TransId { get; set; }
        public string OrderId { get; set; }
        public string PolicyNo { get; set; }

        public void PostCredit()
        {
            throw new NotImplementedException();
        }

        public void AuditCreditPayment(XmlNodeList nodeList, string lastFour, string nameOnCard, string policyno)
        {
            throw new NotImplementedException();
        }

        public void AuditCreditPayment(XmlNodeList nodeList, string lastFour, string nameOnCard)
        {
            throw new NotImplementedException();
        }

        public void PostChecking()
        {
            throw new NotImplementedException();
        }

        public void VoidPayment()
        {
            throw new NotImplementedException();
        }

        public void AuditCreditPayment()
        {
            throw new NotImplementedException();
        }
    }

    public class AuthorizeNet : IPaymentProcessor
    {
        public string CardNo { get; set; }
        public string Cvv { get; set; }
        public DateTime CardExpDate { get; set; }
        public string CardExpYear { get; set; }
        public string CardExpMonth { get; set; }
        public string CheckAcctNo { get; set; }
        public string CheckRoutingNo { get; set; }
        public double Amount { get; set; }
        public string ResponseCode { get; set; }
        public bool HasErrors { get; set; }
        public string ErrMsg { get; set; }
        public string PayorName { get; set; }
        public string TransId { get; set; }
        public string OrderId { get; set; }
        public string PolicyNo { get; set; }
        public void PostCredit()
        {
            throw new NotImplementedException();
        }

        public void AuditCreditPayment(XmlNodeList nodeList, string lastFour, string nameOnCard, string policyno)
        {
            throw new NotImplementedException();
        }

        public void PostChecking()
        {
            throw new NotImplementedException();
        }

        public void VoidPayment()
        {
            throw new NotImplementedException();
        }

        public void AuditCreditPayment()
        {
            throw new NotImplementedException();
        }
    }


}//END Namespace