﻿using System;
using System.Xml;

namespace AgentPortal.Support
{
    public interface IPaymentProcessor
    {
        //Credit Card Specific
        string CardNo { get; set; }
        string Cvv { get; set; }
        DateTime CardExpDate { get; set; }
        string CardExpYear { get; set; }
        string CardExpMonth { get; set; }
        //------------------
        //Checking Specific
        string CheckAcctNo { get; set; }
        string CheckRoutingNo { get; set; }
        //------------------
        //Generic
        double Amount { get; set; }
        string ResponseCode { get; set; }
        bool HasErrors { get; set; }
        string ErrMsg { get; set; }
        string PayorName { get; set; }
        string TransId { get; set; }
        string OrderId { get; set; }
        string PolicyNo { get; set; }
        //------------------
        void PostCredit();
        void AuditCreditPayment(XmlNodeList nodeList, string lastFour, string nameOnCard, string policyno);
        void PostChecking();
        void VoidPayment();
        void AuditCreditPayment();
    }
}