﻿using System;
using System.Collections.Generic;
using AgentPortal.Models;

namespace AgentPortal.Support
{
    [Obsolete("Save things to the database instead")]
    public class DataMock
    {
        #region Singleton
        [Obsolete("Save things to the database instead")]
        public static DataMock Instance
        {
            get
            {
                return null;
            }
        }
        #endregion

        public List<QuoteModel> Quotes { get; set; }
        public List<DriverModel> Drivers { get; set; }
        public List<ViolationModel> Violations { get; set; }
        public List<VehicleModel> Vehicles { get; set; }
        public List<LienHolderModel> LienHolders { get; set; }

        private DataMock()
        {
            Quotes = new List<QuoteModel>();
            Drivers = new List<DriverModel>();
            Violations = new List<ViolationModel>();
            Vehicles = new List<VehicleModel>();
            LienHolders = new List<LienHolderModel>();
        }
    }
}