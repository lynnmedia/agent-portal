﻿using AgentPortal.sqlssrsdev;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace AgentPortal.Support
{
    public class SSRSHelpers
    {
        //TODO: add generation of renewal policy docs

        public void GeneratePolicyDocs(string policyNo, int historyIndex, string userName, UnitOfWork uow)
        {
            List<string> listPDFs = new List<string>();
            List<ParameterValue> ssrsParams = new List<ParameterValue>();

            AuditPolicyHistory getHistory =
                new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("IndexID = ?", historyIndex))
                    .FirstOrDefault();
            if (getHistory == null)
            {
                return;
            }

            XpoPolicy policyInfo =
                new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .FirstOrDefault();
            if (policyInfo == null)
            {
                return;
            }

            bool isEft = false;
            isEft = (policyInfo != null && policyInfo.EFT == true);
            bool isPPO = policyInfo.HasPPO;
            List<string> forms = new List<string>();
            forms.Add("Application Cover");
            forms.Add("AAIC FLQQ (0120) Application");
            forms.Add("AAIC FLUM (1018) UM Coverage Rejection");
            forms.Add("AAIC FLBI (1018) BI Coverage Rejection");
            forms.Add("AAIC 002 (1018) Policy Declarations");
            forms.Add("ID Cards");
            forms.Add("Payment Schedule");
            forms.Add("Payment Receipt");
            forms.Add("AAIC FL27(0719) Driver Disclosure");
            forms.Add("Amendatory Endorsement - E-2");
            forms.Add("Fraud Letter");
            if (isPPO)
            {
                forms.Add("E1 (0619) Preferred Medical Provider Endorsement");
            }

            if (isEft)
            {
                forms.Add("EFT");
            }

            for (int i = 0; i < forms.Count; i++)
            {
                try
                {
                    //Init Report Params
                    string reportName = null;
                    ssrsParams.Clear();
                    SetReportParams(forms[i], policyNo, historyIndex, ssrsParams, out reportName);
                    string newFileLocation = "";
                    bool isProd = ConfigSettings.ReadSetting("DB_Server").Equals("VM-AAINS-SQL");
                    if (isProd)
                    {
                        newFileLocation = GenerateSSRSReport(policyNo, reportName, ssrsParams, "UNDERWRITING", userName, uow);
                    }
                    else
                    {
                        newFileLocation = GenerateSSRSReport(policyNo, reportName, ssrsParams, "UNDERWRITING", userName, uow);
                    }
                    if (File.Exists(newFileLocation))
                    {
                        listPDFs.Add(newFileLocation);
                    }
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.Log();
                }
            }
            if (listPDFs.Count > 1)
            {
                Guid gID = Guid.NewGuid();
                string destinationConcatPDFFile = string.Format(@"\\{0}\pdfs\{1}_NewPolicyDocs.pdf",
                    ConfigSettings.ReadSetting("IIS_Server"), policyNo);
                ConcatPDFs(listPDFs, destinationConcatPDFFile);
            }
        }

        public string GenerateSSRSReport(string policyNo, string reportName, IList<ParameterValue> ssrsParams,
            string departmentName, string userName, UnitOfWork uow)
        {
            string reportPath = string.Empty;
            try
            {
                ReportExecutionService rs = new ReportExecutionService();
                string deviceInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
                string mimeType;
                string encoding;
                string[] streamId;
                Warning[] warning;
                string aaadminUserName = ConfigSettings.ReadSetting("Report_Admin_UserName");
                string password = ConfigSettings.ReadSetting("Report_Admin_UserPassword");
                string domain = ConfigSettings.ReadSetting("Domain");
                rs.Credentials = new NetworkCredential(aaadminUserName, password, domain);
                rs.ExecutionHeaderValue = new ExecutionHeader();
                var executionInfo = new ExecutionInfo();
                executionInfo = rs.LoadReport(reportName, null);
                rs.SetExecutionParameters(ssrsParams.ToArray(), "en-US");

                var result = rs.Render("PDF", deviceInfo, out mimeType, out encoding, out encoding, out warning, out streamId);

                string nameWithOutPath = reportName;
                int lastIndexOfForwardSlash = reportName.LastIndexOf("/");
                if (lastIndexOfForwardSlash > -1)
                {
                    nameWithOutPath = nameWithOutPath.Substring(lastIndexOfForwardSlash + 1);
                }
                string destinationFilePath = string.Format(@"\\{0}\pdfs\{1}_{2}_{3}.pdf",
                    ConfigSettings.ReadSetting("IIS_Server"), policyNo, nameWithOutPath, DateTime.Now.Ticks);
                File.WriteAllBytes(destinationFilePath, result);
                reportPath = destinationFilePath;
                string portalcode = null;
                if (reportName.Contains("Policy Declarations"))
                {
                    portalcode = "DEC";
                }
                else if (reportName.Contains("Cards"))
                {
                    portalcode = "IDCARD";
                }
                else if (reportName.Contains("Invoice"))
                {
                    portalcode = "INV";
                }
                else if (reportName.Contains("Schedule"))
                {
                    portalcode = "PYMNTSCHEDULE";
                }
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyNo;
                Images.Dept = departmentName;
                Images.FormType = "PDF";
                Images.FormDesc = nameWithOutPath;
                Images.PortalCode = portalcode;
                Images.FileFolder = Path.GetDirectoryName(reportPath);
                Images.FileName = @"\" + Path.GetFileName(reportPath);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = userName;
                Images.IsDiary = true;
                Images.Category = nameWithOutPath;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyNo;
                Index.historyID = null;
                Index.portalcode = portalcode;
                Index.department = departmentName;
                Index.formType = "PDF";
                Index.formDesc = nameWithOutPath;
                Index.fileFolder = Path.GetDirectoryName(reportPath); ;
                Index.fileName = @"\" + Path.GetFileName(reportPath); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = userName;
                Index.isDiary = true;
                Index.category = "UNCATEGORIZED";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();

                return reportPath;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return reportPath;
            }
        }

        public string GeneratePaymentReceipt(string policyNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Payment Receipt";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(policyNo);
                Ssrs.ParamNames.Add("Show");
                Ssrs.ParamValues.Add("LAST");
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Payment Receipt";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyNo;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = "Payment Receipt";
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = "SYSTEM";
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyNo;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = "SYSTEM";
                Index.isDiary = true;
                Index.category = "Payment Receipt";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }


        public void GenerateDriversNotAssociatedReport(string policyNo, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
              
                    Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                        ConfigSettings.ReadSetting("DB_Catalog"));

                    Ssrs.RecordKey = policyNo;
                    Ssrs.ListPdfs.Clear();
                    Ssrs.ListFailedPdfs.Clear();
                    Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                    Ssrs.FtpUser = "ftpadmin";
                    Ssrs.FtpPass = "Ds3@dmin";
                    Ssrs.FromPortal = true;
                    Ssrs.FileFormat = "PDF";
                    Ssrs.Setup();
                    Ssrs.ReportName = @"/Underwriting/Policy Not Associated Drivers";
                    Ssrs.ParamNames.Add("PolicyNo");
                    Ssrs.ParamValues.Add(policyNo);
                    Ssrs.GenerateSsrsReport();
                    Ssrs.Department = "UNDERWRITING";
                    Ssrs.Category = "Policy Not Associated Drivers";
                    AuditImageIndex Images = new AuditImageIndex(uow);
                    Images.FileNo = policyNo;
                    Images.Dept = Ssrs.Department;
                    Images.FormType = Ssrs.FileFormat;
                    Images.FormDesc = Ssrs.Category;
                    Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                    Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                    Images.DateCreated = DateTime.Now;
                    Images.Printed = false;
                    Images.UserName = userName;
                    Images.IsDiary = true;
                    Images.Category = Ssrs.Category;
                    Images.Save();

                    IndexInfo Index = new IndexInfo();
                    Index.tablename = "AuditImageIndex";
                    Index.indexID = null;
                    Index.fileNo = policyNo;
                    Index.historyID = null;
                    Index.department = Ssrs.Department;
                    Index.formType = Ssrs.FileFormat;
                    Index.formDesc = Ssrs.Category;
                    Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                    Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                    Index.dateCreated = DateTime.Now;
                    Index.isPrinted = false;
                    Index.username = userName;
                    Index.isDiary = true;
                    Index.category = "UNCATEGORIZED";
                    Index.Create();
                    Index.Dispose(Index);
                    uow.CommitChanges();
                
            }
            catch (Exception ex)
            {
                ex.Message.Log();
            }

        }

        public void GenerateRiskCheckReport(string policyNo, string userName, string quoteNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            string policyOrQuoteNo = String.Empty;
            uow.ExecuteSproc("spMAINT_RCPOSConversion", quoteNo);
            PolicyRiskCheckRpt riskcheckForSubject;

            riskcheckForSubject = new XPCollection<PolicyRiskCheckRpt>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).OrderByDescending(i => i.IndexID).FirstOrDefault();
            if (riskcheckForSubject == null)
            {
                XpoPolicy pol = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
                policyOrQuoteNo = pol.QuoteNo;
                riskcheckForSubject = new XPCollection<PolicyRiskCheckRpt>(uow, CriteriaOperator.Parse("PolicyNo = ?", pol.QuoteNo)).OrderByDescending(i => i.IndexID).FirstOrDefault();
            }


            try
            {
                if (riskcheckForSubject != null && !String.IsNullOrEmpty(riskcheckForSubject.TransactionId))
                {
                    Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                        ConfigSettings.ReadSetting("DB_Catalog"));

                    Ssrs.RecordKey = policyNo;
                    Ssrs.ListPdfs.Clear();
                    Ssrs.ListFailedPdfs.Clear();
                    Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                    Ssrs.FtpUser = "ftpadmin";
                    Ssrs.FtpPass = "Ds3@dmin";
                    Ssrs.FromPortal = true;
                    Ssrs.FileFormat = "PDF";
                    Ssrs.Setup();
                    Ssrs.ReportName = @"/Underwriting/Risk Check Report";
                    Ssrs.ParamNames.Add("TransactionId");
                    Ssrs.ParamNames.Add("PolicyNo");
                    Ssrs.ParamValues.Add(riskcheckForSubject.TransactionId);
                    Ssrs.ParamValues.Add(riskcheckForSubject.PolicyNo);
                    Ssrs.GenerateSsrsReport();
                    Ssrs.Department = "UNDERWRITING";
                    Ssrs.Category = "RISKCHECK";
                    AuditImageIndex Images = new AuditImageIndex(uow);
                    Images.FileNo = policyNo;
                    Images.Dept = Ssrs.Department;
                    Images.FormType = Ssrs.FileFormat;
                    Images.FormDesc = Ssrs.Category;
                    Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                    Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                    Images.DateCreated = DateTime.Now;
                    Images.Printed = false;
                    Images.UserName = userName;
                    Images.IsDiary = true;
                    Images.Category = Ssrs.Category;
                    Images.Save();

                    IndexInfo Index = new IndexInfo();
                    Index.tablename = "AuditImageIndex";
                    Index.indexID = null;
                    Index.fileNo = policyNo;
                    Index.historyID = null;
                    Index.department = Ssrs.Department;
                    Index.formType = Ssrs.FileFormat;
                    Index.formDesc = Ssrs.Category;
                    Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                    Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                    Index.dateCreated = DateTime.Now;
                    Index.isPrinted = false;
                    Index.username = userName;
                    Index.isDiary = true;
                    Index.category = "UNCATEGORIZED";
                    Index.Create();
                    Index.Dispose(Index);
                    uow.CommitChanges();
                }
            }
            catch (Exception ex)
            {
                ex.Message.Log();
            }

        }

        public void GenerateAplusReport(string policyNo, string userName, string quoteNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;

            XPCollection<PolicyDrivers> driversForPolicy =
                new XPCollection<PolicyDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?  AND LicenseNo IS NOT NULL",
                    policyNo));
            uow.ExecuteSproc("spMAINT_APlusWithCapReport", quoteNo);
            foreach (var driver in driversForPolicy)
            {
                try
                {
                    Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                        ConfigSettings.ReadSetting("DB_Catalog"));

                    Ssrs.RecordKey = policyNo;
                    Ssrs.ListPdfs.Clear();
                    Ssrs.ListFailedPdfs.Clear();
                    Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                    Ssrs.FtpUser = "ftpadmin";
                    Ssrs.FtpPass = "Ds3@dmin";
                    Ssrs.FromPortal = true;
                    Ssrs.FileFormat = "PDF";
                    Ssrs.Setup();
                    Ssrs.ReportName = @"/Underwriting/A Plus with Cap Report";
                    Ssrs.ParamNames.Add("ListDriverLicense");
                    Ssrs.ParamValues.Add(driver.LicenseNo);
                    Ssrs.GenerateSsrsReport();
                    Ssrs.Department = "UNDERWRITING";
                    Ssrs.Category = "APLUS";
                    AuditImageIndex Images = new AuditImageIndex(uow);
                    Images.FileNo = policyNo;
                    Images.Dept = Ssrs.Department;
                    Images.FormType = Ssrs.FileFormat;
                    Images.FormDesc = Ssrs.Category;
                    Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                    Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                    Images.DateCreated = DateTime.Now;
                    Images.Printed = false;
                    Images.UserName = userName;
                    Images.IsDiary = true;
                    Images.Category = Ssrs.Category;
                    Images.Save();

                    IndexInfo Index = new IndexInfo();
                    Index.tablename = "AuditImageIndex";
                    Index.indexID = null;
                    Index.fileNo = policyNo;
                    Index.historyID = null;
                    Index.department = Ssrs.Department;
                    Index.formType = Ssrs.FileFormat;
                    Index.formDesc = Ssrs.Category;
                    Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                    Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                    Index.dateCreated = DateTime.Now;
                    Index.isPrinted = false;
                    Index.username = userName;
                    Index.isDiary = true;
                    Index.category = "UNCATEGORIZED";
                    Index.Create();
                    Index.Dispose(Index);
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.Log();
                }
            }
        }

        public void GenerateMVRs(string policyno, string userName, string quoteNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            XPCollection<PolicyDrivers> driversForPolicy =
                new XPCollection<PolicyDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ? AND LicenseNo IS NOT NULL",
                    policyno));
            uow.ExecuteSproc("spMAINT_MVRConversion", quoteNo);
            foreach (var driver in driversForPolicy)
            {
                try
                {
                    Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                        ConfigSettings.ReadSetting("DB_Catalog"));

                    Ssrs.RecordKey = policyno;
                    Ssrs.ListPdfs.Clear();
                    Ssrs.ListFailedPdfs.Clear();
                    Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                    Ssrs.FtpUser = "ftpadmin";
                    Ssrs.FtpPass = "Ds3@dmin";
                    Ssrs.FromPortal = true;
                    Ssrs.FileFormat = "PDF";
                    Ssrs.Setup();
                    Ssrs.ReportName = @"/Underwriting/MVR";
                    Ssrs.ParamNames.Add("ListDriverLicense");
                    Ssrs.ParamValues.Add(driver.LicenseNo);
                    Ssrs.GenerateSsrsReport();
                    Ssrs.Department = "UNDERWRITING";
                    Ssrs.Category = "MVR";
                    AuditImageIndex Images = new AuditImageIndex(uow);
                    Images.FileNo = policyno;
                    Images.Dept = Ssrs.Department;
                    Images.FormType = Ssrs.FileFormat;
                    Images.FormDesc = Ssrs.Category;
                    Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                    Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                    Images.DateCreated = DateTime.Now;
                    Images.Printed = false;
                    Images.UserName = userName;
                    Images.IsDiary = true;
                    Images.Category = Ssrs.Category;
                    Images.Save();

                    IndexInfo Index = new IndexInfo();
                    Index.tablename = "AuditImageIndex";
                    Index.indexID = null;
                    Index.fileNo = policyno;
                    Index.historyID = null;
                    Index.department = Ssrs.Department;
                    Index.formType = Ssrs.FileFormat;
                    Index.formDesc = Ssrs.Category;
                    Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                    Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                    Index.dateCreated = DateTime.Now;
                    Index.isPrinted = false;
                    Index.username = userName;
                    Index.isDiary = true;
                    Index.category = "UNCATEGORIZED";
                    Index.Create();
                    Index.Dispose(Index);
                    uow.CommitChanges();
                }
                catch (Exception ex)
                {
                    ex.Message.Log();
                }
            }
        }

        public string GenerateDriverDisclosure(string policyno, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC FL27 (0719) Driver Disclosure";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "AAIC FL27 (0719) Driver Disclosure";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyno;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = Ssrs.Category;
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = userName;
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyno;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = userName;
                Index.isDiary = true;
                Index.category = "UNCATEGORIZED";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string GenerateEFT(string policyno, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/EFT";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamNames.Add("AccountType");
                Ssrs.ParamNames.Add("AccountNo");
                Ssrs.ParamNames.Add("ABA");

                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.ParamValues.Add("AAIC");
                string accountType =
                    DbHelperUtils.getScalarValue("PolicyBankInfo", "AccountType", "PolicyNumber", policyno);
                string accountNumber =
                    DbHelperUtils.getScalarValue("PolicyBankInfo", "AccountNumber", "PolicyNumber", policyno);
                //Routing number not being used until pci compliance resolved in ui
                if (!string.IsNullOrEmpty(accountType))
                {
                    Ssrs.ParamValues.Add(accountType.ToUpper());
                }
                else
                {
                    Ssrs.ParamValues.Add("");
                }

                if (!string.IsNullOrEmpty(accountNumber))
                {
                    Ssrs.ParamValues.Add(accountNumber.ToUpper());
                }
                else
                {
                    Ssrs.ParamValues.Add("");
                }

                string aba =
                    DbHelperUtils.getScalarValue("PolicyBankInfo", "ABA", "PolicyNumber", policyno);
                if (!string.IsNullOrEmpty(aba))
                {
                    Ssrs.ParamValues.Add(aba.ToUpper());
                }
                else
                {
                    Ssrs.ParamValues.Add("");
                }

                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "EFT";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyno;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = Ssrs.Category;
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = userName;
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyno;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = userName;
                Index.isDiary = true;
                Index.category = "UNCATEGORIZED";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string GenerateBIRejection(string policyno, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC FLBI (1018) BI Coverage Rejection";
                Ssrs.ParamNames.Add("ApplicationReportLevel");
                Ssrs.ParamValues.Add("Policy");
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "AAIC FLBI (1018) BI Coverage Rejection";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyno;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = Ssrs.Category;
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = userName;
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyno;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = userName;
                Index.isDiary = true;
                Index.category = "UNCATEGORIZED";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string GenerateUMRejection(string policyno, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC FLUM (1018) UM Coverage Rejection";
                Ssrs.ParamNames.Add("ApplicationReportLevel");
                Ssrs.ParamValues.Add("Policy");
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "AAIC FLUM (1018) UM Coverage Rejection";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyno;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = Ssrs.Category;
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = userName;
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyno;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = userName;
                Index.isDiary = true;
                Index.category = "UNCATEGORIZED";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string GenerateApplication(string policyno, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC FLQQ (0120) Application";
                Ssrs.ParamNames.Add("ApplicationReportLevel");
                Ssrs.ParamValues.Add("Policy");
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "AAIC FLQQ (0120) Application";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyno;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = Ssrs.Category;
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = userName;
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyno;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = userName;
                Index.isDiary = true;
                Index.category = "UNCATEGORIZED";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }
        public string GenerateEndorsementRequest(string policyno, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC FL02 (1019) Endorsement Request";
                //Ssrs.ParamNames.Add("ApplicationReportLevel");
                //Ssrs.ParamValues.Add("Policy");
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "AAIC FL02 (1019) Endorsement Request";

                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadAgentInforceReport(string agentCode, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = agentCode;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Reports/Inforce Report for Agents";
                Ssrs.ParamNames.Add("AgentCode");
                Ssrs.ParamValues.Add(String.Format("{0}", agentCode));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "AGENTS";
                Ssrs.Category = "Inforce Report for Agents";

                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadAgentSuspenseReport(string agentCode, string userName, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = agentCode;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Reports/Agent Suspense Report";
                Ssrs.ParamNames.Add("AgentCode");
                Ssrs.ParamValues.Add(String.Format("{0}", agentCode));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "AGENTS";
                Ssrs.Category = "Agent Suspense Report";

                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadPolicyDec(string policyNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                IList<AuditPolicyHistory> auditPolicyHistoryRecordsForPolicy =
                    new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                        .OrderByDescending(aph => aph.IndexID).ToList();
                if (auditPolicyHistoryRecordsForPolicy == null || !auditPolicyHistoryRecordsForPolicy.Any())
                {
                    return string.Empty;
                }

                AuditPolicyHistory auditPolicyHistoryRecord = null;
                string reportType = string.Empty;
                if (policyNo.Contains("-"))
                {
                    AuditPolicyHistory renewalRecordForRenwedPolicy = auditPolicyHistoryRecordsForPolicy
                        .Where(aph => aph.Action.Equals("REN")).OrderByDescending(aph => aph.IndexID).FirstOrDefault();
                    AuditPolicyHistory endorseRecordForRenewedPolicy =
                        auditPolicyHistoryRecordsForPolicy.FirstOrDefault(aph =>
                            aph.Action.Equals("ENDORSE") && aph.IndexID > renewalRecordForRenwedPolicy.IndexID);
                    if (endorseRecordForRenewedPolicy != null)
                    {
                        reportType = "ENDORSE";
                        auditPolicyHistoryRecord = endorseRecordForRenewedPolicy;
                    }
                    else
                    {
                        reportType = "REN";
                        auditPolicyHistoryRecord = renewalRecordForRenwedPolicy;
                    }

                }
                else
                {
                    AuditPolicyHistory newRecordForNonRenwedPolicy = auditPolicyHistoryRecordsForPolicy
                        .Where(aph => aph.Action.Equals("NEW")).OrderByDescending(aph => aph.IndexID).FirstOrDefault();
                    if (newRecordForNonRenwedPolicy == null)
                    {
                        return string.Empty;
                    }
                    AuditPolicyHistory endorseRecordForNonRenewedPolicy =
                        auditPolicyHistoryRecordsForPolicy.Where(aph =>
                            aph.Action.Equals("ENDORSE") && aph.IndexID > newRecordForNonRenwedPolicy.IndexID).OrderByDescending(aph => aph.IndexID).FirstOrDefault();
                    if (endorseRecordForNonRenewedPolicy != null)
                    {
                        reportType = "ENDORSE";
                        auditPolicyHistoryRecord = endorseRecordForNonRenewedPolicy;
                    }
                    else
                    {
                        reportType = "NEW";
                        auditPolicyHistoryRecord = newRecordForNonRenwedPolicy;
                    }

                }
                if (auditPolicyHistoryRecord == null || reportType.Equals(string.Empty))
                {
                    return string.Empty;
                }

                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC 002 (1018) Policy Declarations";
                Ssrs.ParamNames.Add("ReportType");
                Ssrs.ParamValues.Add(reportType);
                Ssrs.ParamNames.Add("PolicyHistoryIndex");
                Ssrs.ParamValues.Add(String.Format("{0}_{1}", policyNo, auditPolicyHistoryRecord.IndexID));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Policy Dec";
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadPolicyIDCards(string policyNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/ID Cards";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(policyNo);
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "ID Cards";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyNo;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = $"ID Cards";
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = "SYSTEM";
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyNo;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = "SYSTEM";
                Index.isDiary = true;
                Index.category = "ID Cards";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadAgentStatusReport(string agentCode)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = agentCode;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Reports/Agent Status Report";
                Ssrs.ParamNames.Add("AgentCode");
                Ssrs.ParamValues.Add(agentCode);
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Agent Status Report";

                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadAgentLossRatioReport(string agentCode, DateTime date)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = agentCode;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Reports/Agent Loss Ratio Report";
                Ssrs.ParamNames.Add("AgentCode");
                Ssrs.ParamValues.Add(agentCode);
                Ssrs.ParamNames.Add("Date");
                Ssrs.ParamValues.Add(date.ToString("d"));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Loss Ratio Report";

                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadPaymentSchedule(string policyNo)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Payment Schedule";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(policyNo);
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Payment Schedule";

                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }
        public string DownloadPolicyInvoice(string policyNo, DateTime mailDate, UnitOfWork uow, bool insuredCopyOnly = false)
        {
            var filePath = DownloadPolicyInvoice(policyNo, mailDate, uow);
            if (insuredCopyOnly)
            {
                SSRSUtils.RemovePDFPages(filePath, new List<int> { 1 });
            }
            return filePath;
        }

        public string DownloadPolicyInvoice(string policyNo, DateTime mailDate, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Invoice";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(policyNo);
                Ssrs.ParamNames.Add("MailDate");
                Ssrs.ParamValues.Add(mailDate.ToString("MM-dd-yyyy"));
                Ssrs.ParamNames.Add("CancelDate");
                Ssrs.ParamValues.Add(mailDate.ToString("MM-dd-yyyy"));
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Invoice";
                AuditImageIndex Images = new AuditImageIndex(uow);
                Images.FileNo = policyNo;
                Images.Dept = Ssrs.Department;
                Images.FormType = Ssrs.FileFormat;
                Images.FormDesc = $"Invoice";
                Images.FileFolder = Path.GetDirectoryName(Ssrs.FilePathOut);
                Images.FileName = @"\" + Path.GetFileName(Ssrs.FilePathOut);
                Images.DateCreated = DateTime.Now;
                Images.Printed = false;
                Images.UserName = "SYSTEM";
                Images.IsDiary = true;
                Images.Category = Ssrs.Category;
                Images.Save();

                IndexInfo Index = new IndexInfo();
                Index.tablename = "AuditImageIndex";
                Index.indexID = null;
                Index.fileNo = policyNo;
                Index.historyID = null;
                Index.department = Ssrs.Department;
                Index.formType = Ssrs.FileFormat;
                Index.formDesc = Ssrs.Category;
                Index.fileFolder = Path.GetDirectoryName(Ssrs.FilePathOut); ;
                Index.fileName = @"\" + Path.GetFileName(Ssrs.FilePathOut); ;
                Index.dateCreated = DateTime.Now;
                Index.isPrinted = false;
                Index.username = "SYSTEM";
                Index.isDiary = true;
                Index.category = "Invoice";
                Index.Create();
                Index.Dispose(Index);
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadRenewalPolicyDec(string policyNo, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                AuditPolicyHistory auditPolicyHistoryRenewalRecord =
                    new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Action = 'QTE'", policyNo)).OrderByDescending(aph => aph.IndexID).FirstOrDefault();
                if (auditPolicyHistoryRenewalRecord == null)
                {
                    return string.Empty;
                }
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC 002 (1018) Policy Declarations";
                Ssrs.ParamNames.Add("ReportType");
                Ssrs.ParamValues.Add("QTE");
                Ssrs.ParamNames.Add("PolicyHistoryIndex");
                Ssrs.ParamValues.Add(String.Format("{0}_{1}", policyNo, auditPolicyHistoryRenewalRecord.IndexID));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Renewal Policy Dec";
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadRenewalQuotePaymentSchedule(string renewalPolicyNo)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = renewalPolicyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Renewal Quote Payment Schedule";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(renewalPolicyNo);
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Renewal Quote Payment Schedule";
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string DownloadRenewalInvoice(string renewalPolicyNo, string userName)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = renewalPolicyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Forms/Renewal Invoice";
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(renewalPolicyNo);
                Ssrs.ParamNames.Add("UserName");
                Ssrs.ParamValues.Add(userName);
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "Renewal Invoice";
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        private void SetReportParams(string formdesc, string policyno, int historyid,
            List<ParameterValue> ssrsParams, out string reportName)
        {
            switch (formdesc)
            {
                case "Application Cover":
                    reportName = @"/Underwriting/Application Cover";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "Fraud Letter":
                    reportName = @"/Underwriting/Fraud Letter";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "AAIC FLQQ (0120) Application":
                    reportName = @"/Underwriting/AAIC FLQQ (0120) Application";
                    ssrsParams.Add(new ParameterValue { Name = "ApplicationReportLevel", Value = "Policy" });
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "E1 (0619) Preferred Medical Provider Endorsement":
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    reportName = @"/Underwriting/E1 (0619) Preferred Medical Provider Endorsement";
                    break;
                case "AAIC FL27(0719) Driver Disclosure":
                    reportName = @"/Underwriting/AAIC FL27 (0719) Driver Disclosure";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;

                case "NAMED DRIVER EXCLUSION":
                    reportName = @"/Underwriting/AAIC FL13 (1018) Driver Exclusion";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "Amendatory Endorsement - E-2":
                    reportName = @"/Underwriting/Amendatory Endorsement - E-2";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "AAIC 002 (1018) Policy Declarations":
                    reportName = @"/Underwriting/AAIC 002 (1018) Policy Declarations";
                    ssrsParams.Add(new ParameterValue { Name = "ReportType", Value = "NEW" });
                    ssrsParams.Add(new ParameterValue { Name = "PolicyHistoryIndex", Value = String.Format("{0}_{1}", policyno, historyid) });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "EFT":
                    reportName = @"/Underwriting/EFT";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    string accountType =
                        DbHelperUtils.getScalarValue("PolicyBankInfo", "AccountType", "PolicyNumber", policyno);
                    string accountNumber =
                        DbHelperUtils.getScalarValue("PolicyBankInfo", "AccountNumber", "PolicyNumber", policyno);
                    //Routing number not being used until pci compliance resolved in ui
                    if (!string.IsNullOrEmpty(accountType))
                    {
                        ssrsParams.Add(new ParameterValue { Name = "AccountType", Value = accountType.ToUpper() });
                    }
                    else
                    {
                        ssrsParams.Add(new ParameterValue { Name = "AccountType", Value = string.Empty });
                    }

                    if (!string.IsNullOrEmpty(accountNumber))
                    {
                        ssrsParams.Add(new ParameterValue { Name = "AccountNo", Value = accountNumber.ToUpper() });
                    }
                    else
                    {
                        ssrsParams.Add(new ParameterValue { Name = "AccountNo", Value = string.Empty });
                    }

                    string aba =
                        DbHelperUtils.getScalarValue("PolicyBankInfo", "ABA", "PolicyNumber", policyno);
                    if (!string.IsNullOrEmpty(aba))
                    {
                        ssrsParams.Add(new ParameterValue { Name = "ABA", Value = aba.ToUpper() });
                    }
                    else
                    {
                        ssrsParams.Add(new ParameterValue { Name = "ABA", Value = string.Empty });
                    }
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "ID Cards":
                    reportName = @"/Underwriting/ID Cards";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;

                case "AAIC FLBI (1018) BI Coverage Rejection":
                    reportName = @"/Underwriting/AAIC FLBI (1018) BI Coverage Rejection";
                    if (policyno.StartsWith("Q-") || policyno.StartsWith("D-"))
                    {
                        ssrsParams.Add(new ParameterValue { Name = "ApplicationReportLevel", Value = "Quote" });
                    }
                    else
                    {
                        ssrsParams.Add(new ParameterValue { Name = "ApplicationReportLevel", Value = "Policy" });
                    }
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "AAIC FLUM (1018) UM Coverage Rejection":
                    reportName = @"/Underwriting/AAIC FLUM (1018) UM Coverage Rejection";
                    if (policyno.StartsWith("Q-") || policyno.StartsWith("D-"))
                    {
                        ssrsParams.Add(new ParameterValue { Name = "ApplicationReportLevel", Value = "Quote" });
                    }
                    else
                    {
                        ssrsParams.Add(new ParameterValue { Name = "ApplicationReportLevel", Value = "Policy" });
                    }
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "Payment Receipt":
                    reportName = @"/Underwriting/Payment Receipt";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "Show", Value = "ALL" });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                case "Payment Schedule":
                    reportName = @"/Underwriting/Payment Schedule";
                    ssrsParams.Add(new ParameterValue { Name = "PolicyNo", Value = policyno });
                    ssrsParams.Add(new ParameterValue { Name = "CompanyID", Value = "AAIC" });
                    break;
                default:
                    reportName = string.Empty;
                    break;
            }
        }

        public static bool ConcatPDFs(List<string> listPDFs, string pdfFileName)
        {
            try
            {
                PdfDocument outputDocument = new PdfDocument();
                for (int i = 0; i < listPDFs.Count; i++)
                {
                    PdfDocument inputDocument = PdfReader.Open(listPDFs[i], PdfDocumentOpenMode.Import);

                    int count = inputDocument.PageCount;
                    for (int idx = 0; idx < count; idx++)
                    {
                        PdfPage page = inputDocument.Pages[idx];
                        outputDocument.AddPage(page);
                    }
                }
                outputDocument.Save(pdfFileName);
                return true;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return false;
            }
        }

        public string GenerateReinstatementNoLossRequest(string policyno, UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyno;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/AAIC FL09 (1018) No Loss Statement";
                //Ssrs.ParamNames.Add("ApplicationReportLevel");
                //Ssrs.ParamValues.Add("Policy");
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyno));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "AAIC FL09 (1018) No Loss Statement";

                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

        public string GenerateNoticeOfReinstatementDoc(string policyNo, DateTime cancelDate ,UnitOfWork uow)
        {
            SSRSUtils Ssrs = null;
            try
            {
                Ssrs = new SSRSUtils(ConfigSettings.ReadSetting("REPORT_Server"),
                    ConfigSettings.ReadSetting("DB_Catalog"));
                Ssrs.RecordKey = policyNo;
                Ssrs.ListPdfs.Clear();
                Ssrs.ListFailedPdfs.Clear();
                Ssrs.FtpServer = $@"ftp://{ConfigSettings.ReadSetting("IIS_Server")}";
                Ssrs.FtpUser = "ftpadmin";
                Ssrs.FtpPass = "Ds3@dmin";
                Ssrs.FromPortal = true;
                Ssrs.FileFormat = "PDF";
                Ssrs.Setup();
                Ssrs.ReportName = @"/Underwriting/Notice Of Reinstatement";
                //Ssrs.ParamNames.Add("ApplicationReportLevel");
                //Ssrs.ParamValues.Add("Policy");
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(String.Format("{0}", policyNo));
                Ssrs.ParamNames.Add("CompanyID");
                Ssrs.ParamValues.Add("AAIC");

                ////////////////////////////////
                Ssrs.ParamNames.Add("PolicyNo");
                Ssrs.ParamValues.Add(policyNo);
                ////////////////////////////////
                Ssrs.ParamNames.Add("MailDate");
                Ssrs.ParamValues.Add(DateTime.Now.ToString("MM-dd-yyyy"));
                ////////////////////////////////
                Ssrs.ParamNames.Add("CancelDate");
                Ssrs.ParamValues.Add(String.Format("{0}", cancelDate));
                Ssrs.GenerateSsrsReport();
                Ssrs.Department = "UNDERWRITING";
                Ssrs.Category = "POLICY REINSTATEMENT";
                uow.CommitChanges();
                return Ssrs.FilePathOut;
            }
            catch (Exception ex)
            {
                ex.Message.Log();
                return Ssrs?.FilePathOut;
            }
        }

    }
}