﻿using System;
using System.IO;
using System.Web.Hosting;

namespace AgentPortal.Support
{
    public static class EmailRenderer
    {
        #region Implementation details

        static readonly string EmailViewFolder = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "Views/EmailRenderer");

        static string RenderFromTemplate(string view)
        {
            string templateData = File.ReadAllText(Path.Combine(EmailViewFolder, "template.html"));
            string viewData     = File.ReadAllText(Path.Combine(EmailViewFolder, view + ".html"));

            return templateData.Replace("@RenderBody()", viewData);
        }//END RenderFromTemplate
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        #endregion

        #region Client specific

        public static string RegistrationValidation(string name, string code, string requestUrl)
        {
            return RenderFromTemplate("RegistrationValidation")
                .Replace("{% title %}", "Activate Web Access")
                .Replace("{% blurb %}", "Before you get started, there's one thing to do.")
                .Replace("{% link %}", string.Format("<a href='http://" + requestUrl + "/Client/SetPassword?code={0}'>Activation Link</a>", code))
                .Replace("{% name %}", name);
        }//END RegistrationValidation
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string InitiatePasswordReset(string name, string code, string requestUrl)
        {
            return RenderFromTemplate("InitiatePasswordReset")
                .Replace("{% title %}", "Reset your password")
                .Replace("{% blurb %}", "Click the link to set a new password")
                .Replace("{% link %}", string.Format("<a href='http://{0}/Client/SetPassword?code={1}'>Reset Password</a>", requestUrl, code))
                .Replace("{% name %}", name);
        }//END InitiatePasswordReset
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        #endregion

        public static string PaymentSuccess(string insuredName,
                                            string policyNo,
                                            string agentcode,
                                            string paymentType,
                                            DateTime date,
                                            double amount,
                                            string notes)
        {
            return RenderFromTemplate("PaymentSuccess")
                .Replace("{% title %}", "Payment Received")
                .Replace("{% blurb %}", "A Payment has been made on policy " + policyNo)
                .Replace("{% insuredName %}", insuredName)
                .Replace("{% policy %}", policyNo)
                .Replace("{% agentcode %}", agentcode)
                .Replace("{% paymentType %}", paymentType)
                .Replace("{% date %}", date.ToLongDateString() + " at " + date.ToShortTimeString())
                .Replace("{% amount %}", String.Format("{0:C}", Math.Round(amount, 2)))
                .Replace("{% notes %}", notes)
                .ToString();
        }//END PaymentSuccess
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string PolicyIssued(string title,
                                            string policyno,
                                            string insuredname,
                                            string agentcode,
                                            string agentname,
                                            string policytotal,
                                            string ratersource)
        {
            return RenderFromTemplate("IssuePolicy")
                .Replace("{% title %}", title)
                .Replace("{% blurb %}", string.Format("Policy {0} was issued through the portal on {1}", policyno, DateTime.Now.ToString("D")))
                .Replace("{% policynumber %}", policyno)
                .Replace("{% insuredname %}", insuredname)
                .Replace("{% agentcode %}", agentcode)
                .Replace("{% agentname %}", agentname)
                .Replace("{% policytotal %}", policytotal)
                .Replace("{% ratersource %}", ratersource)
                .ToString();
        }//END PaymentSuccess
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string IssueRenewal(string title,
                                            string policyno,
                                            string priorpolicyno,
                                            string insuredname,
                                            string agentcode,
                                            string agentname,
                                            string policytotal)
        {
            return RenderFromTemplate("IssueRenewal")
                .Replace("{% title %}", title)
                .Replace("{% blurb %}", string.Format("Renewal for {0} was issued through the portal on {1}", policyno, DateTime.Now.ToString("D")))
                .Replace("{% policynumber %}", policyno)
                .Replace("{% priorpolicyno %}", priorpolicyno)
                .Replace("{% insuredname %}", insuredname)
                .Replace("{% agentcode %}", agentcode)
                .Replace("{% agentname %}", agentname)
                .Replace("{% policytotal %}", policytotal)
                .ToString();
        }//END PaymentSuccess
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string OrderSupplies(string agentname,
                                            string order)
        {
            return RenderFromTemplate("OrderSupplies")
                .Replace("{% title %}", "Supply Order")
                .Replace("{% blurb %}", string.Format("Agency {0} has placed an order", agentname))
                .Replace("{% AgentName %}", agentname)
                .Replace("{% order %}", order)
                .ToString();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static string UploadConfirmation(string uploadMsg, string policyno)
        {
            return RenderFromTemplate("UploadConfirmation")
                .Replace("{% title %}", "Document Upload Successful")
                .Replace("{% blurb %}", string.Format("Upload For {0}", policyno))
                .Replace("{% UploadMsg %}", uploadMsg)
                .ToString();
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
    }//END Class
}//END Namespace