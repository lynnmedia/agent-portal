﻿using System;
using System.Configuration;
using System.Reflection;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;

namespace AgentPortal.Support
{
    public static class XpoHelper
    {
        public static Session GetNewSession()
        {
            return new Session(DataLayer);
        }

        public static UnitOfWork GetNewUnitOfWork()
        {
            return new UnitOfWork(DataLayer);
        }

        private readonly static object lockObject = new object();

        static volatile IDataLayer fDataLayer;
        static IDataLayer DataLayer
        {
            get
            {
                if (fDataLayer == null)
                {
                    lock (lockObject)
                    {
                        if (fDataLayer == null)
                        {
                            fDataLayer = GetDataLayer();
                        }
                    }
                }
                return fDataLayer;
            }
        }

        private static IDataLayer GetDataLayer()
        {
            XpoDefault.Session = null;
            string conn = ConfigurationManager.ConnectionStrings["AlertAutoConnectionString"].ConnectionString;
            conn = XpoDefault.GetConnectionPoolString(conn);
            XPDictionary dict = new ReflectionDictionary();
            IDataStore store = XpoDefault.GetConnectionProvider(conn, AutoCreateOption.SchemaAlreadyExists);
            dict.GetDataStoreSchema(Assembly.GetExecutingAssembly());
            IDataLayer dl = new ThreadSafeDataLayer(dict, store);
            XpoDefault.DataLayer = dl;
            return dl;
        }

        /// <summary>
        /// Executes a query with the given parameters in a nested unit of work
        ///
        /// Queries that return a result set
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameterNames"></param>
        /// <param name="parameterValues"></param>
        /// <param name="parentUow"></param>
        /// <returns>result set</returns>
        public static SelectStatementResult[] ExecuteQuery(string query, string[] parameterNames,
            object[] parameterValues, UnitOfWork parentUow)
        {
            using (var uow = parentUow.BeginNestedUnitOfWork())
            {
                var result = uow.ExecuteQuery(query, parameterNames, parameterValues);
                uow.CommitChanges();
                return result.ResultSet;
            }
        }

        /// <summary>
        /// Executes a query with the given parameters in a nested unit of work
        ///
        /// Queries that return a scalar value
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameterNames"></param>
        /// <param name="parameterValues"></param>
        /// <param name="parentUow"></param>
        /// <returns>scalar result as object</returns>
        public static object ExecuteScalar(string query, string[] parameterNames,
            object[] parameterValues, UnitOfWork parentUow)
        {
            using (var uow = parentUow.BeginNestedUnitOfWork())
            {
                var result = uow.ExecuteScalar(query, parameterNames, parameterValues);
                uow.CommitChanges();
                return result;
            }
        }

        /// <summary>
        /// Executes a query with the given parameters in a nested unit of work
        ///
        /// INSERT, UPDATE, DELETE
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameterNames"></param>
        /// <param name="parameterValues"></param>
        /// <param name="parentUow"></param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQuery(string query, string[] parameterNames, object[] parameterValues, UnitOfWork parentUow)
        {
            using (var uow = parentUow.BeginNestedUnitOfWork())
            {
                var nRows = uow.ExecuteNonQuery(query, parameterNames, parameterValues);
                uow.CommitChanges();
                return nRows;
            }
        }
    }
}