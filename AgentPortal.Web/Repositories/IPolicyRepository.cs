﻿using System.Collections.Generic;
using AgentPortal.Support.DataObjects;
using DevExpress.Xpo.DB;

namespace AgentPortal.Repositories
{
    public interface IPolicyRepository
    {
        IPolicy GetPolicy(string policyNo);
        void ApplyPaymentsToInstallments(string policyNo);
        void SavePolicyPreference(string policyNo, string type, string value);
        PolicyPreferences GetPolicyPreference(string policyNo, string key);
        SelectedData GetPolicyScore(string policyNo, string username, bool isNewBusinessAllowed);
        void RatePolicy(string username, string ratingGuid, string policyNo);
        IList<AuditImageIndex> GetDamageImages(string policyNo);
        int GetDamagedVehicleCount(string policyNo);
        IList<PolicyCarLienHolders> GetLienHoldersForCar(string quoteId, int carIndex);
        int GetLienHolderCount(string policyNo);
        void SaveLienHolder(PolicyCarLienHolders lienHolder);
        PolicyCarLienHolders GetLienHolderById(string id);
    }
}