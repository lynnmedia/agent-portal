﻿using System;
using System.Threading.Tasks;
using AgentPortal.Repositories.Xpo;

namespace AgentPortal.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IAgentRepository AgentRepository { get;}
        IQuoteRepository QuoteRepository { get;}
        IPolicyRepository PolicyRepository { get;}
        IEndorsementRepository EndorsementRepository { get;}
        IPaymentRepository PaymentRepository { get;}
        IRateCycleRepository RateCycleRepository { get; }
        IDocumentRepository DocumentRepository { get; }
        void Save();
        Task SaveAsync();
        ITransaction BeginTransaction();
        Task<ITransaction> BeginTransactionAsync();
    }
}