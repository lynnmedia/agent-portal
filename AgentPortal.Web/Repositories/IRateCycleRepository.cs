﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentPortal.Support.DataObjects;

namespace AgentPortal.Repositories
{
    public interface IRateCycleRepository
    {
        XpoAARateCycles GetCurrentCycle();
    }
}
