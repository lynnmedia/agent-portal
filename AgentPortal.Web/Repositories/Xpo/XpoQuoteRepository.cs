﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoQuoteRepository : IQuoteRepository
    {
        private UnitOfWork uow;

        public XpoQuoteRepository(UnitOfWork unitOfWork)
        {
            uow = unitOfWork;
        }

        public XpoPolicyQuote GetQuote(string policyNo)
        {
            return new XPCollection<XpoPolicyQuote>(uow,CriteriaOperator.Parse("PolicyNo = ?", policyNo)).First();
        }

        public void SaveQuote(IPolicyQuote updateQuote)
        {
            ((XpoPolicyQuote)updateQuote).Save();
        }

        public void CreateQuote(QuoteModel model)
        {
            XpoPolicyQuote newquote = new XpoPolicyQuote(uow);
            newquote.PolicyNo = model.QuoteId;
            UtilitiesRating.MapModelToXpo(model, newquote, true);
            newquote.SixMonth = true;
            newquote.isAnnual = false;
            newquote.NIRR = true; //LF 3/15/19 changed to true
            newquote.NIO = false; //added 9/28/2018 LF
            newquote.PIPDed = "1000";
            newquote.PDLimit = "10000";
            newquote.WorkLoss = 1;
            newquote.Save();
        }

        public IList<PolicyQuoteDrivers> GetQuoteDrivers(string quoteId)
        {
            return new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteId));
        }

        public IList<PolicyQuoteCars> GetQuoteCars(string quoteId)
        {
            return new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteId));
        }

        public PolicyRenQuote GetRenewalQuote(string policyNo)
        {
            return new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", policyNo)).FirstOrDefault();
        }

        public PolicyEndorseQuote GetEndorsementQuote(string policyNo)
        {
            return uow.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
        }

        public PolicyQuoteQuestionnaire GetQuoteQuestionnaire(string policyNo)
        {
            return uow.FindObject<PolicyQuoteQuestionnaire>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
        }

        public void SaveQuoteQuestionnaire(PolicyQuoteQuestionnaire questionnaire)
        {
            questionnaire.Save();
        }

        public IList<PolicyQuoteDriversViolations> GetViolations(string policyNo)
        {
            return new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo));
        }

        public IList<PolicyQuoteDriversViolations> GetPipClaims(string policyNo)
        {
            return new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse("PolicyNo = ? AND UPPER(ViolationDesc) LIKE'%PIP%'", policyNo));
        }

        public IList<PolicyQuoteDrivers> GetQuoteDriversByIndex(string policyNo, int driverIndex)
        {
            return new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", policyNo, driverIndex));
        }

        public void SaveDriver(PolicyQuoteDrivers driver)
        {
            driver.Save();
        }

        public PolicyQuoteDrivers GetQuoteDriverByOID(string oid)
        {
            return uow.FindObject<PolicyQuoteDrivers>(CriteriaOperator.Parse("OID = ?", oid));
        }

        public IList<PolicyQuoteDriversViolations> GetViolationsByDriver(string policyNo, int driverIndex)
        {
            return new XPCollection<PolicyQuoteDriversViolations>(uow, CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", policyNo, driverIndex));
        }

        public int GetDriverViolationsCount(string policyNo, int driverIndex)
        {
            return new XPCollection<PolicyQuoteDriversViolations>(uow,
                CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", policyNo, driverIndex)).Count;
        }

        public PolicyQuoteCars GetQuoteCarByOID(string oid)
        {
            return uow.FindObject<PolicyQuoteCars>(CriteriaOperator.Parse("OID = ?", oid));
        }
    }
}