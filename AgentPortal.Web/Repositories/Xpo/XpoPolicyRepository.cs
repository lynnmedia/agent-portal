﻿using System;
using System.Collections.Generic;
using System.Linq;
using AgentPortal.Repositories.StoredProcedures;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoPolicyRepository : IPolicyRepository
    {
        private readonly UnitOfWork uow;
        private StoredProcedures.StoredProcedures _storedProcedures;
		
		public XpoPolicyRepository(UnitOfWork unitOfWork)
        {
            uow = unitOfWork;
            _storedProcedures = new StoredProcedures.StoredProcedures(new StoredProcedureComponentsSql());
        }
		
        public IPolicy GetPolicy(string policyNo)
        {
            var policy = new XPCollection<XpoPolicy>(uow,CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
            return policy;
        }

        public void ApplyPaymentsToInstallments(string policyNo)
        {
            _storedProcedures.InstallmentPaymentApply(policyNo, DateTime.Now, "SYSTEM",Guid.NewGuid().ToString());
            //uow.ExecuteSproc("spUW_InstallmentPaymentApply", policyNo, DateTime.Now, "SYSTEM",Guid.NewGuid().ToString());
        }

        public void SavePolicyPreference(string policyNo, string type, string value)
        {
            var preferences = new PolicyPreferences(uow)
            {
                PolicyNo = Sessions.Instance.PolicyNo,
                Type = type,
                Value = value
            };
            preferences.Save();
        }

        public PolicyPreferences GetPolicyPreference(string policyNo, string key)
        {
            return new XPCollection<PolicyPreferences>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Type = ? ", Sessions.Instance.PolicyNo, key)).FirstOrDefault();
        }

        public SelectedData GetPolicyScore(string policyNo, string username, bool isNewBusinessAllowed)
        {
            var newBusinessAllowedParam = new OperandValue(isNewBusinessAllowed);
            return uow.ExecuteSproc("spUW_GetPolicyScore", policyNo, username, newBusinessAllowedParam);
        }

        public void RatePolicy(string username, string ratingGuid, string policyNo)
        {
            uow.ExecuteSproc("spUW_RatePolicyAlert", //LF 3/12/19
                new OperandValue(0),
                new OperandValue(username),
                new OperandValue(ratingGuid),
                new OperandValue(policyNo),
                new OperandValue(0));
        }

        public IList<AuditImageIndex> GetDamageImages(string policyNo)
        {
            return new XPCollection<AuditImageIndex>(uow, CriteriaOperator.Parse("FileNo = ? AND IsPhysicalDamageImage = 1", policyNo));
        }

        public int GetDamagedVehicleCount(string policyNo)
        {
            return new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ? AND HasPhyDam = 1", policyNo)).Count;
        }

        public IList<PolicyCarLienHolders> GetLienHoldersForCar(string quoteId, int carIndex)
        {
            return new XPCollection<PolicyCarLienHolders>(uow, CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", quoteId, carIndex));
        }

        public int GetLienHolderCount(string policyNo)
        {
            return new XPCollection<PolicyCarLienHolders>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .Count;
        }

        public void SaveLienHolder(PolicyCarLienHolders lienHolder)
        {
            lienHolder.Save();
        }

        public PolicyCarLienHolders GetLienHolderById(string id)
        {
            return new XPCollection<PolicyCarLienHolders>(uow, CriteriaOperator.Parse("ID = ?", id)).FirstOrDefault();
        }
    }
}