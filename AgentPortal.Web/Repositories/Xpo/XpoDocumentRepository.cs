﻿using AgentPortal.Support.DataObjects;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.Data.Filtering;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoDocumentRepository : IDocumentRepository
    {
        private UnitOfWork uow;

        public XpoDocumentRepository(UnitOfWork unitOfWork)
        {
            uow = unitOfWork;
        }

        public PortalDocumentRequest GetDocumentRequest(string policyNo)
        {
            return uow.FindObject<PortalDocumentRequest>(CriteriaOperator.Parse("RecordNo = ?", policyNo));
        }

        public void SaveDocumentRequest(PortalDocumentRequest documentRequest)
        {
            documentRequest.Save();
        }
    }
}