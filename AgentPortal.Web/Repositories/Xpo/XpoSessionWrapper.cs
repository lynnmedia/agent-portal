﻿using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoSessionWrapper : ITransaction
    {
        public XpoSessionWrapper(Session session)
        {
            _session = session;
        }

        private readonly Session _session;

        public void Commit()
        {
            _session.CommitTransaction();
        }

        public void Rollback()
        {
            _session.RollbackTransaction();
        }
    }
}