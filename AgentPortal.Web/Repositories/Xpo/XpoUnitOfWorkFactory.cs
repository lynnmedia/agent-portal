﻿namespace AgentPortal.Repositories.Xpo
{
    public class XpoUnitOfWorkFactory : IUnitOfWorkFactory{

        public IUnitOfWork GetUnitOfWork()
        {
            return new XpoUnitOfWorkWrapper();
        }
    }
}