﻿using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoEndorsementRepository : IEndorsementRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public XpoEndorsementRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void MarkEndorsementAsPaid(string policyNo)
        {
            var endorsementQuoteForPolicy = _unitOfWork.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
            if (endorsementQuoteForPolicy == null) return;
            endorsementQuoteForPolicy.IsPremiumPaymentPaid = true;
            endorsementQuoteForPolicy.Save();
        }
    }
}