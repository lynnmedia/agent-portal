﻿using System;
using System.Threading.Tasks;
using AgentPortal.Support;
using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoUnitOfWorkWrapper : IUnitOfWork
    {
        private readonly UnitOfWork _unitOfWork;
        private bool _disposed;

        public XpoUnitOfWorkWrapper()
        {
            _unitOfWork = XpoHelper.GetNewUnitOfWork();
            XpoDefault.DataLayer = _unitOfWork.DataLayer;//TODO: this is a work around to keep the default constructors for XPO objects using the same context
        }

        private IAgentRepository _agentRepository;
        private IQuoteRepository _quoteRepository;
        private IPolicyRepository _policyRepository;
        private IPaymentRepository _paymentRepository;
        private IEndorsementRepository _endorsementRepository;
        private IRateCycleRepository _rateCycleRepository;
        private IDocumentRepository _documentRepository;

        public IPolicyRepository PolicyRepository => _policyRepository ?? (_policyRepository = new XpoPolicyRepository(_unitOfWork));
        public IEndorsementRepository EndorsementRepository => _endorsementRepository ?? (_endorsementRepository = new XpoEndorsementRepository(_unitOfWork));
        public IPaymentRepository PaymentRepository => _paymentRepository ?? (_paymentRepository = new XpoPaymentRepository(_unitOfWork));
        public IRateCycleRepository RateCycleRepository => _rateCycleRepository ?? (_rateCycleRepository = new XpoRateCycleRepository(_unitOfWork));
        public IAgentRepository AgentRepository => _agentRepository ?? (_agentRepository = new XpoAgentRepository(_unitOfWork));
        public IQuoteRepository QuoteRepository => _quoteRepository ?? (_quoteRepository = new XpoQuoteRepository(_unitOfWork));
        public IDocumentRepository DocumentRepository => _documentRepository ?? (_documentRepository = new XpoDocumentRepository(_unitOfWork));

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            _unitOfWork.CommitChanges();
        }

        public async Task SaveAsync()
        {
            await Task.Run(() => _unitOfWork.CommitChanges());
        }

        public ITransaction BeginTransaction()
        {
            _unitOfWork.ExplicitBeginTransaction();
            return new XpoSessionWrapper(_unitOfWork);
        }

        public Task<ITransaction> BeginTransactionAsync()
        {
            _unitOfWork.ExplicitBeginTransaction();
            return Task.FromResult<ITransaction>(new XpoSessionWrapper(_unitOfWork));
        }
    }
}