﻿using System.Linq;
using AgentPortal.Support.DataObjects;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoAgentRepository : IAgentRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public XpoAgentRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IAgents GetAgentByEmail(string emailAddress)
        {
            return new XPCollection<XpoAgents>(_unitOfWork, CriteriaOperator.Parse("EmailAddress = ? OR AltEmailAddress = ?", emailAddress)).FirstOrDefault();
        }

        public IAgents GetAgentByAlternateEmail(string emailAddress)
        {
            return new XPCollection<XpoAgents>(_unitOfWork, CriteriaOperator.Parse("AltEmailAddress = ?", emailAddress)).FirstOrDefault();
        }
    }
}