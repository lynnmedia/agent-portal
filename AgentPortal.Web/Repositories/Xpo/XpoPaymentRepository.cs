using System;
using System.Collections.Generic;
using System.Linq;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using AlertAuto.Integrations.Contracts.CardConnect;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoPaymentRepository : IPaymentRepository
    {
        private UnitOfWork uow;

        public XpoPaymentRepository(UnitOfWork uow)
        {
            this.uow = uow;
        }

        public void WriteCardConnectResponseToPaymentTables(string policyNo, CardConnectRequest requestData,
            CardConnectResponse response, CardConnectPaymentModel paymentInfo)
        {
            if (response == null || requestData == null || response == null || paymentInfo == null ||
                string.IsNullOrEmpty(policyNo))
            {
                return;
            }

            string paymentType = "INS";
            double amount = Math.Round(Convert.ToDouble(requestData.amount), 2);
            XpoPayments newPaymentDetail = new XpoPayments(uow);
            newPaymentDetail.PolicyNo = policyNo;
            newPaymentDetail.PaymentType = paymentType;
            newPaymentDetail.PostmarkDate = DateTime.Now;
            newPaymentDetail.AcctDate = DateTime.Now;
            newPaymentDetail.TotalAmt = amount;
            newPaymentDetail.UserName = Sessions.Instance.AgentCode;
            newPaymentDetail.BatchNo = response.retref;
            newPaymentDetail.BatchID = response.batchid;
            newPaymentDetail.CreateDate = DateTime.Now;
            newPaymentDetail.TokenID = response.token;
            newPaymentDetail.PortalRespCode = response.respstat;
            newPaymentDetail.PortalReplyMsg = response.resptext;
            newPaymentDetail.FromCardConnect = true;
            newPaymentDetail.Last4OfCard = paymentInfo.ExtractLast4OfAccount();
            if (paymentInfo.IsAch)
            {
                newPaymentDetail.PaymentType = "EFT";
                newPaymentDetail.CheckAmt = amount;
                newPaymentDetail.IsCheck = true;
            }
            else
            {
                double creditCardFeeAmount = Math.Round(Convert.ToDouble(paymentInfo.CreditCardFee), 2);
                newPaymentDetail.FeeAmt = creditCardFeeAmount;
            }

            newPaymentDetail.Save();
            uow.CommitChanges();
        }

        public IList<AuditACHTransactions> GetAchTransactions(string policyNo)
        {
           return new XPCollection<AuditACHTransactions>(uow, CriteriaOperator.Parse("RecordKey = ? AND ConfirmationNo IS NULL", policyNo));
        }

        public IList<Payments> GetPayments(string policyNo)
        {
            return new XPCollection<Payments>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo));
        }

        public RenQuoteInstallments GetRenewalInstallments(string policyNo)
        {
            return new XPCollection<RenQuoteInstallments>(uow, 
                CriteriaOperator.Parse("PolicyNo = ? AND Descr = 'Deposit' AND IgnoreRec = '0'", policyNo)).FirstOrDefault();
        }
    }


    //public List<IPayments> ListPayments(PaymentResponseModel paymentToMake)
    //{
    //    using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
    //    {
    //        var SaveToRep = new XpoPayments(uow);
    //        SaveToRep.PolicyNo = "polnum1";
    //        SaveToRep.PaymentType = "MOV";
    //        SaveToRep.PostmarkDate = DateTime.Now;
    //        SaveToRep.AcctDate = DateTime.Today;
    //        SaveToRep.CheckAmt = 12.77;
    //        SaveToRep.TotalAmt = 500.54;
    //        SaveToRep.UserName = "mrman";
    //        SaveToRep.BatchGUID = "pymntBatchGUID";
    //        SaveToRep.CreateDate = DateTime.Now;
    //        SaveToRep.TokenID = paymentToMake.csrequestID;
    //        SaveToRep.PortalRespCode = paymentToMake.csreasonCode;
    //        SaveToRep.PortalReplyMsg = paymentToMake.decision;
    //        SaveToRep.CheckNotes = "MOVED FROM" + paymentToMake.pymntPolicyNumber;
    //        SaveToRep.Save();
    //        uow.CommitChanges();
    //        // Get all the items
    //        var allPayments = uow.Query<XpoPayments>().ToList();
            
    //        return (List<IPayments>) allPayments;
    //    }

    //    #region Get XPO UOW data Examples
    //    //var p = uow.GetObjectByKey<Person>(key);
    //    // Get all the items whose Person.Position property value is "Employee"
    //    //var employees = uow.Query<Person>().Where(x => x.Position == "Employee").ToList();
    //    #endregion

    //    #region Stored Proc Model Example
    //    //sd = uow.ExecuteSproc("spUW_InstallmentPaymentApply", 1/*policyno*/, DateTime.Now,
    //    /*"SYSTEM", Guid.NewGuid().ToString());

    //    foreach (var result in uow)
    //    {
    //        foreach (var row in result.Rows)
    //        {
    //            PaymentResponseModel dataRow = null;
    //            dataRow.csbillTo_city = (string)row.Values[0];
    //            dataRow.csbillTo_country = (string)row.Values[1];
    //            dataRow.csbillTo_firstName = (string)row.Values[2];
    //            dataRow.csbillTo_lastName = (string)row.Values[4];
    //            dataRow.csbillTo_phoneNumber = (string)row.Values[5];
    //            payments.Add(dataRow);
    //        }
    //    }*/
    //    #endregion
    //}

    //public bool CreatePayment(PaymentResponseModel paymentToMake)
    //{
    //    try
    //    {
    //        ListPayments(paymentToMake);
    //        return true;
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //}
}