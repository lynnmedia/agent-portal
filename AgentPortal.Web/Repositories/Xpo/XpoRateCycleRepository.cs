﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgentPortal.Support.DataObjects;
using DevExpress.Xpo;

namespace AgentPortal.Repositories.Xpo
{
    public class XpoRateCycleRepository : IRateCycleRepository
    {
        private readonly UnitOfWork _uow;

        public XpoRateCycleRepository(UnitOfWork uow)
        {
            this._uow = uow;
        }
        public XpoAARateCycles GetCurrentCycle()
        {
            return new XPQuery<XpoAARateCycles>(_uow)
                .Where(c => c.EffDate_NewBusiness <= DateTime.Now)
                .OrderByDescending(c => c.EffDate_NewBusiness)
                .FirstOrDefault();
        }
    }
}