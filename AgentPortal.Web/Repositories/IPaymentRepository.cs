using System.Collections;
using System.Collections.Generic;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;
using AlertAuto.Integrations.Contracts.CardConnect;

namespace AgentPortal.Repositories
{
    public interface IPaymentRepository
    {
        void WriteCardConnectResponseToPaymentTables(string policyNo, CardConnectRequest requestData, CardConnectResponse response, CardConnectPaymentModel paymentInfo);
        IList<AuditACHTransactions> GetAchTransactions(string policyNo);
        IList<Payments> GetPayments(string policyNo);
        RenQuoteInstallments GetRenewalInstallments(string policyNo);
    }
}