﻿using System.Collections.Generic;
using AgentPortal.Models;
using AgentPortal.Support.DataObjects;

namespace AgentPortal.Repositories
{
    public interface IQuoteRepository
    {
        XpoPolicyQuote GetQuote(string policyNo);
        void SaveQuote(IPolicyQuote updateQuote);
        void CreateQuote(QuoteModel model);
        IList<PolicyQuoteDrivers> GetQuoteDrivers(string quoteId);
        IList<PolicyQuoteCars> GetQuoteCars(string quoteId);
        PolicyRenQuote GetRenewalQuote(string policyNo);
        PolicyEndorseQuote GetEndorsementQuote(string policyNo);
        PolicyQuoteQuestionnaire GetQuoteQuestionnaire(string policyNo);
        void SaveQuoteQuestionnaire(PolicyQuoteQuestionnaire questionnaire);
        IList<PolicyQuoteDriversViolations> GetViolations(string policyNo);
        IList<PolicyQuoteDriversViolations> GetPipClaims(string policyNo);
        IList<PolicyQuoteDrivers> GetQuoteDriversByIndex(string policyNo, int driverIndex);
        void SaveDriver(PolicyQuoteDrivers driver);
        PolicyQuoteDrivers GetQuoteDriverByOID(string oid);
        IList<PolicyQuoteDriversViolations> GetViolationsByDriver(string policyNo, int driverIndex);
        int GetDriverViolationsCount(string policyNo, int driverIndex);
        PolicyQuoteCars GetQuoteCarByOID(string oid);
    }
}