﻿namespace AgentPortal.Repositories
{
    public interface IEndorsementRepository
    {
        void MarkEndorsementAsPaid(string policyNo);
    }
}