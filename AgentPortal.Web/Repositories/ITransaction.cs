﻿namespace AgentPortal.Repositories
{
    public interface ITransaction
    {
        void Commit();
        void Rollback();
    }
}