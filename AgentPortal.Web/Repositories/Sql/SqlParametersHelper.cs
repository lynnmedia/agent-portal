﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace AgentPortal.Repositories.Sql
{
    public static class SqlParametersHelper
    {
        public static SqlParameter CreateParamBigInt(string paramName, long? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.BigInt)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamBit(string paramName, bool? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.Bit)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamDateTime(string paramName, DateTime? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.DateTime2)
            {
                Scale = 3,
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamDecimal(string paramName, decimal? paramValue, byte precision = 18,
            byte scale = 4)
        {
            return new SqlParameter(paramName, SqlDbType.Decimal)
            {
                Precision = precision,
                Scale = scale,
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamFloat(string paramName, double? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.Float)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamInt(string paramName, int? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.Int)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamMoney(string paramName, decimal? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.Money)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamNVarchar(string paramName, string paramValue, int size)
        {
            return new SqlParameter(paramName, SqlDbType.NVarChar, size)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamSmallMoney(string paramName, decimal? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.SmallMoney)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamSmallInt(string paramName, int? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.SmallInt)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamTinyInt(string paramName, byte? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.TinyInt)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamUniqueIdentifier(string paramName, Guid? paramValue)
        {
            return new SqlParameter(paramName, SqlDbType.UniqueIdentifier)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }

        public static SqlParameter CreateParamVarchar(string paramName, string paramValue, int size)
        {
            return new SqlParameter(paramName, SqlDbType.VarChar, size)
            {
                Value = paramValue ?? (object)DBNull.Value
            };
        }
    }
}