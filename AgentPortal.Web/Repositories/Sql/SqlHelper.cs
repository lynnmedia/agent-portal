using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using AgentPortal.Repositories.StoredProcedures;
using static AgentPortal.Repositories.Sql.SqlParametersHelper;

namespace AgentPortal.Repositories.Sql
{
    public static class SqlHelper
    {
        public static readonly double FloatTolerance = 0.001D;


        #region Conversions

        /// <summary>
        /// Converts a value retrieved from the database
        /// to the specified type or that type's default if null
        /// 
        /// </summary>
        /// <typeparam name="T">C# type of returned value</typeparam>
        /// <param name="obj">The value retrieved as object type</param>
        /// <returns></returns>
        public static T ConvertFromDbVal<T>(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return default;
            }

            return (T)obj;
        }

        /// <summary>
        ///  Converts a value retrieved from the database
        ///  to the specified type or to a specified value if null
        /// 
        /// </summary>
        /// <typeparam name="T">C# type of returned value</typeparam>
        /// <param name="obj">The value retrieved as object type</param>
        /// <param name="defaultValue">value to return if the retrieved value is null</param>
        /// <returns></returns>
        public static T ConvertFromDbVal<T>(object obj, T defaultValue )
        {
            if (obj == null || obj == DBNull.Value)
            {
                return defaultValue;
            }

            return (T)obj;
        }

        /// <summary>
        ///  Converts a value retrieved from the database
        ///  to the specified type or throws exception if null
        /// 
        /// </summary>
        /// <typeparam name="T">C# type of returned value</typeparam>
        /// <param name="obj">The value retrieved as object type</param>
        /// <param name="exceptionMessage">message to include in exception</param>
        /// <returns></returns>
        public static T ConvertFromDbValOrException<T>(object obj, string exceptionMessage)
        {
            if (obj == null || obj == DBNull.Value)
                throw new StoredProcedureException(exceptionMessage);

            return (T)obj;
        }

        /// <summary>
        /// Converts a returned SCOPE_IDENTITY value to int
        ///
        /// Must first convert to decimal, then to int.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>identity value as nullable int</returns>
        public static int? ConvertScopeIdentity(object obj)
        {
            return (int?)GetDbValOrNull<decimal>(obj);
        }

        /// <summary>
        /// Rounds a nullable decimal value to the given scale
        /// or returns null if null
        /// </summary>
        /// <param name="value">nullable decimal value to round</param>
        /// <param name="scale">number of decimal places to round to</param>
        /// <returns></returns>
        public static decimal? DecimalRoundNull(decimal? value, int scale)
        {
            return value != null ? decimal.Round((decimal)value, scale) : (decimal?)null;
        }

        /// <summary>
        /// Converts a retrieved value from the database to the
        /// specified nullable non-reference type or null if null
        ///
        /// This is used to match the database behavior with value types
        /// that can be null
        /// </summary>
        /// <typeparam name="T">C# type of returned value (pre-nullable)</typeparam>
        /// <param name="obj">The value retrieved as object type</param>
        /// <returns></returns>
        public static T? GetDbValOrNull<T>(object obj) where T : struct
        {

            if (obj is DBNull || obj is null)
                return null;

            return (T)obj;
        }

        /// <summary>
        /// Converts a retrieved value from the database to the
        /// specified reference type or null if null
        ///
        /// This is used to match the database behavior nullable types
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T GetDbRefValOrNull<T>(object obj) where T : class
        {
            if (obj == null || obj == DBNull.Value)
            {
                return null;
            }

            return (T)obj;
        }

        /// <summary>
        /// Returns value contained in specified result set
        /// as returned by ExecuteQueryWithParametersMulti
        ///
        /// Works when result set contains only a single value
        ///
        /// Result set example where the first set contains an
        /// int value of 1831, and the second set contains a set of
        /// rows returned from a query of the Policy table:
        ///
        /// resultsets					List of object
        ///    [0]                      object      List of object
        ///          [0]                object      object[]
        ///                [0]          object      int             1831
        ///
        ///    [1]                      object      List of object
        ///          [0]                object      object[] (Policy record)
        ///                [0]          object      int             23
        ///                [1]          object      string          "AA..."
        ///                [2]          object      datetime        2019-01-...
        ///
        /// </summary>
        /// <typeparam name="T">Type to cast returned value</typeparam>
        /// <param name="resultSets"></param>
        /// <param name="index">index of set containing the value</param>
        /// <returns>the value cast to the specified type</returns>
        public static T GetResultSetVal<T>(IList<object> resultSets, int index)
        {
            return (T)((object[])((List<object>)resultSets[index])[0])[0];
        }

        #endregion

        #region Query Executers

        /// <summary>
        /// Execute a non-query that requires no arguments
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQueryNoArgs(string query, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                return cmd.ExecuteNonQuery();
            }
        }


        /// <summary>
        /// Execute a non-query using a parameter collection already created
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="sqlParams">parameter collection</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQueryWithParameters(string query, SqlParameter[] sqlParams, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.AddRange(sqlParams);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Execute a non-query that requires only a policyNo argument
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="policyNo">Policy # as string</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQueryWithPolicyNo(string query, string policyNo, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.Add("@PolicyNo", SqlDbType.VarChar, 30).Value = policyNo ?? (object)DBNull.Value;
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Execute a non-query that requires exactly a policyNo and an int
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="policyNo">Policy # as string</param>
        /// <param name="intName">Column name of int field</param>
        /// <param name="intVal">value of int field</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQueryWithPolicyNoPlusInt(string query, string policyNo, string intName, int? intVal,
            SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo,30));
                cmd.Parameters.Add(CreateParamInt(intName, intVal));
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Execute a query that requires no arguments
        /// </summary>
        /// <param name="query"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static List<object> ExecuteQueryNoArgs(string query, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                var resultList = new List<object>();
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var values = new object[reader.FieldCount];
                            reader.GetValues(values);
                            resultList.Add(values);
                        }
                    }
                }
                return resultList;
            }
        }

        /// <summary>
        /// Execute a query using a parameter collection already created
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="sqlParams">parameter collection</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>a list of objects each representing a row of the result set</returns>
        public static List<object> ExecuteQueryWithParameters(string query, SqlParameter[] sqlParams, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.AddRange(sqlParams);
                var resultList = new List<object>();
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var values = new object[reader.FieldCount];
                            reader.GetValues(values);
                            resultList.Add(values);
                        }
                    }
                }
                return resultList;
            }
        }

        /// <summary>
        /// Execute a query using a parameter collection already created
        ///
        /// Returns multiple result sets
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="sqlParams">parameter collection</param>
        /// <param name="conn">existing active sql connection</param>
        /// <param name="timeout">optional command timeout</param>
        /// <returns>a list of objects each representing a row of the result set</returns>
        public static List<object> ExecuteQueryWithParametersMulti(string query, SqlParameter[] sqlParams, SqlConnection conn, int? timeout = null)
        {
            var resultList = new List<object>();

            using (var cmd = new SqlCommand(query, conn))
            {
                if (timeout != null)
                {
                    cmd.CommandTimeout = (int)timeout;
                }
                cmd.Parameters.AddRange(sqlParams);

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.HasRows)
                    {
                        var currentSet = new List<object>();
                        while (reader.Read())
                        {
                            var values = new object[reader.FieldCount];
                            reader.GetValues(values);
                            currentSet.Add(values);
                        }
                        resultList.Add(currentSet);
                        reader.NextResult();
                    }

                }

                return resultList;
            }
        }

        /// <summary>
        /// Execute a query requiring only a policyNo argument
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="policyNo">Policy # as string</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>a list of objects each representing a row of the result set</returns>
        public static List<object> ExecuteQueryWithPolicyNo(string query, string policyNo, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 30));
                var resultList = new List<object>();
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var values = new object[reader.FieldCount];
                            reader.GetValues(values);
                            resultList.Add(values);
                        }
                    }
                    reader.Close();
                }

                return resultList;
            }
        }

        /// <summary>
        /// Execute a query which returns exactly one value
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>the value returned as object</returns>
        public static object ExecuteScalarNoArgs(string query, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                return cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Execute a query returning exactly one value
        /// and using a parameter collection already created
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="sqlParams">parameter collection</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>the value returned as object</returns>
        public static object ExecuteScalarWithParameters(string query, SqlParameter[] sqlParams, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.AddRange(sqlParams);
                return cmd.ExecuteScalar();
            }
        }

        /// <summary>
        /// Execute a query which returns exactly one value
        /// and which requires only a policyNo argument
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="policyNo">Policy # as string</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns>the value returned as object</returns>
        public static object ExecuteScalarWithPolicyNo(string query, string policyNo, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 30));
                return cmd.ExecuteScalar();
            }
        }

        /// <summary>
        ///  Execute a query which returns exactly one value
        ///  and which requires only a policyNo argument and an int
        /// </summary>
        /// <param name="query">query to execute</param>
        /// <param name="policyNo">Policy # as string</param>
        /// <param name="intName">Column name of int field</param>
        /// <param name="intVal">value of int field</param>
        /// <param name="conn">existing active sql connection</param>
        /// <returns></returns>
        public static object ExecuteScalarWithPolicyNoPlusInt(string query, string policyNo, string intName, int? intVal,
            SqlConnection conn)
        {
            using (var cmd = new SqlCommand(query, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 30));
                cmd.Parameters.Add(CreateParamInt(intName, intVal));
                return cmd.ExecuteScalar();
            }
        }

        #endregion

        /// <summary>
        /// Calculate the difference in days between two dates
        ///
        /// This method matches the behavior of the DATEDIFF
        /// database function
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static int DateDiffDays(DateTime date1, DateTime date2)
        {
            return (date2 - date1).Days;
        }

        /// <summary>
        /// Calculate the difference in days between two dates
        ///
        /// This method matches the behavior of the DATEDIFF
        /// database function
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static int? DateDiffDaysNull(DateTime? date1, DateTime? date2)
        {
            if (date1 == null || date2 == null) return null;
            return DateDiffDays((DateTime)date1, (DateTime)date2);
        }

        /// <summary>
        /// Returns the first length characters of string src
        ///
        /// If null or empty, returns src string as is.
        /// If length is greater than the length of src, no
        /// exception is thrown and 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Left(string src, int length)
        {
            if (length < 0) throw new ArgumentException("Must be >= 0", nameof(length));
            return string.IsNullOrEmpty(src) || length > src.Length ? src : src.Substring(0, Math.Min(length, src.Length));
        }

        /// <summary>
        /// Truncates a string to the specified size
        /// </summary>
        /// <param name="str">String to truncate</param>
        /// <param name="maxlength">Length to which string is truncated if it is longer</param>
        /// <returns>a new string with the excess truncated</returns>
        public static string Truncate(string str, int maxlength)
        {
            if (string.IsNullOrEmpty(str)) return str;
            return str.Length <= maxlength ? str : str.Substring(0, maxlength);
        }

    }
}
