﻿using System;
using System.Runtime.Serialization;

namespace AgentPortal.Repositories.StoredProcedures
{
    [Serializable]
    public class StoredProcedureException : Exception
    {
        public string ErrorSection { get; set; }

        public StoredProcedureException()
        {
        }

        public StoredProcedureException(string message) : base(message)
        {
        }

        public StoredProcedureException(string message, Exception inner) : base(message, inner)
        {
        }

        protected StoredProcedureException(
            SerializationInfo info,
            StreamingContext context) : base(info, context)
        {
        }

        public StoredProcedureException(string message, string errorAction) : base(message)
        { 
            ErrorSection = errorAction;
        }

        public StoredProcedureException(string message, string errorAction, Exception inner) : base(message, inner)
        {
            ErrorSection = errorAction;
        }
    }

}