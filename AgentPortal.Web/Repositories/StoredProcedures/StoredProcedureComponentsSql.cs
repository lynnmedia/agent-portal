﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Transactions;
using AgentPortal.Repositories.Sql;
using AgentPortal.Repositories.StoredProcedures.StoredProcedureQueries;
using Newtonsoft.Json;
using static AgentPortal.Repositories.Sql.SqlHelper;
using static AgentPortal.Repositories.Sql.SqlParametersHelper;
using static AgentPortal.Repositories.StoredProcedures.NonDbFunctions;

// ReSharper disable UnusedMethodReturnValue.Local

namespace AgentPortal.Repositories.StoredProcedures
{
    /// <summary>
    /// Implements the IDataAccess interface for direct SQL querying
    /// of the database
    ///
    /// The methods are grouped into five regions
    ///    - publicly accessible
    ///    - private internal versions of the publicly accessible
    ///    - transactions
    ///    - extracted methods called by transactions
    ///    - support
    ///
    /// Publicly Accessible
    ///    These methods have essentially a one-to-one relationship
    ///    with a query used in the portal code. Initially, the queries
    ///    they execute are those found in the stored procedures, but
    ///    a method could be created for any query.
    ///
    ///    Each method creates and disposes of its own connection.
    ///    Generally, the code to execute the query is not in these
    ///    methods, but in a private internal version.
    ///
    /// Private internal versions
    ///    These methods are where the queries associated with the
    ///    methods are executed. They require an open connection to
    ///    be passed to them.  This allows for them to be called
    ///    within transactions using the same connection opened
    ///    in the transaction. If this is not necessary, we could
    ///    remove these internal versions and just move their code
    ///    into the public versions, but so far it appears to be
    ///    necessary.
    ///
    ///    Each of these methods have a corresponding public method.
    ///    The method's name is the same as the public version
    ///    with "InTx" prefixed to indicate that these methods
    ///    are suitable for calling inside transactions.
    ///
    ///    The methods had previously been overloads of the
    ///    public methods with the only difference being that
    ///    they were private and had one additional parameter,
    ///    the connection. Unfortunately, this made it all
    ///    too easy to unintentionally call one of the public
    ///    methods from the transaction code. Again, if it turns
    ///    out this is not an issue, than the whole public/private
    ///    approach will have been moot.
    ///
    /// Transactions
    ///    These methods contain a single transaction that
    ///    is committed at the end.  If an exception occurs
    ///    in the transaction, it is not handled in the method
    ///    and the changes made to the database up to the point
    ///    of the exception are rolled back automatically.
    ///
    ///    A transaction method calls only internal methods
    ///    passing its connection to each.
    ///
    /// Extracted methods
    ///    These methods are logical extractions of code from
    ///    the transaction methods to provide for more
    ///    readable and maintainable code.
    ///
    /// Support
    ///    These methods provide support for the other methods
    ///    in the class.  Most of the queries have only a policy
    ///    number as a parameter, so methods exists here to
    ///    execute queries with a policy number for scalar,
    ///    non-query, and select types.
    ///
    ///    Additional support methods not necessarily query-related
    ///    are also found here.
    ///
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class StoredProcedureComponentsSql : IStoredProcedureComponents
    {
        private readonly string _connectionString;

        public StoredProcedureComponentsSql(string connectionString)
        {
            _connectionString = connectionString ??
                                ConfigurationManager.ConnectionStrings["AlertAutoConnectionString"].ConnectionString;
        }

        public StoredProcedureComponentsSql() : this(null)
        {

        }

        public int TxAllowAutoDraft(string policyNo)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                if (GetDbValOrNull<int>(ExecuteScalarWithPolicyNo(AllowAuditDraft.HistoryCount, policyNo, conn)) == 0)
                    return 1;

                return (ExecuteQueryWithPolicyNo(AllowAuditDraft.AuditPolicy, policyNo, conn).Count > 0) ? 0 : 1;
            }
        }

        public void TxAuditSaveDifference(string policyNo, string table1, string table2, int? historyId,
             string userName,
             bool? ignoreSpecialFields, string guid = "")
        {
            var errorSection = "0: Startup";
            userName = userName ?? "ApplicationUser";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();
                    
                        //bool parametersValid = true;

                        // ------------------------------------------------------------------
                        errorSection = "1:Get Policy values. ";

                        // ------------------------------------------------------------------
                        errorSection = "2: Insert into #Columns. ";

                        var tempColumns = new TempColumns();

                        var columnList = ExecuteQueryWithParameters(AuditSaveDifference.IntersectTables, new[]
                        {
                            CreateParamVarchar("@Table1", table1, 100),
                            CreateParamVarchar("@Table2", table2, 100)
                        }, conn);

                        foreach (var rec in columnList)
                        {
                            var values = (object[])rec;
                            tempColumns.AddRecord(new TempColumnsRecord()
                            {
                                ColumnName = GetDbRefValOrNull<string>(values[0])
                            });
                        }

                        // ------------------------------------------------------------------
                        errorSection = "3: Delete ignored fields from #Columns. ";

                        // Remove fields that don't need different values stored, for all comparisons

                        tempColumns.Table.RemoveAll(c =>
                            (new List<string>() { "POLICYNO", "INDEXID", "OID", "ID" }).Contains(c.ColumnName.ToUpper()));

                        // Remove fields that don't need different values stored, if  @IgnoreSpecialFields = 1.

                        if (ignoreSpecialFields != null && (bool)ignoreSpecialFields)
                        {
                            tempColumns.Table.RemoveAll(c =>
                                (new List<string>() { "POLICYSTATUS", "CANCELDATE", "CANCELTYPE" }).Contains(
                                    c.ColumnName.ToUpper()));
                        }

                        // ------------------------------------------------------------------
                        errorSection = "4: set Index variables.";

                        //  find out if tables have fields CarIndex, DriverIndex, etc.

                        var hasCarIndex = tempColumns.Table.Count(c => string.Equals(c.ColumnName, "CarIndex"));
                        var hasDriverIndex = tempColumns.Table.Count(c => string.Equals(c.ColumnName, "DriverIndex"));
                        var hasViolationIndex = tempColumns.Table.Count(c => string.Equals(c.ColumnName, "ViolationIndex"));

                        // ------------------------------------------------------------------
                        errorSection = "5: While @RowNbr <= @MaxRows.";

                        // Loop through each column that exists in both tables.  Store values that are different between the two tables.

                        foreach (var rec in tempColumns.Table)
                        {
                            var columnName = rec.ColumnName;
                            var sb = new StringBuilder();
                            sb.Append(
                                "INSERT INTO AuditHistoryActivity (PolicyNo,HistoryID,TableName,IndexNo,SubIndexNo,ColumnName,ColumnDesc,FieldType,Value2,Value1) ");
                            sb.Append($"SELECT ISNULL({table1}.PolicyNo, {table2}.PolicyNo) as PolicyNo ");
                            sb.Append($", {historyId} as HistoryID ");
                            sb.Append($",'{table1}'");

                            // -------------------------- IndexNo ---------------------------	
                            if (hasCarIndex > 0)
                                sb.Append($", {table1}.CarIndex as IndexNo ");
                            if (hasDriverIndex > 0)
                                sb.Append($", {table1}.DriverIndex as IndexNo ");

                            if (hasCarIndex == 0 && hasDriverIndex == 0)
                                sb.Append(",0 as IndexNo ");

                            // ------------------- SubIndexNo: only used for Violations --------------------	

                            sb.Append(hasViolationIndex > 0
                                ? $", {table1}.ViolationIndex as SubIndexNo "
                                : ",0 as SubIndexNo ");

                            // ---------------------------------------------------------------		

                            sb.Append($",'{columnName}' as ColumnName ");
                            sb.Append(",cast(ISNULL((select top 1 ep.value from sys.columns ");
                            sb.Append(
                                $"  CROSS APPLY fn_listextendedproperty(default, 'SCHEMA', 'dbo', 'TABLE', '{table1}', 'COLUMN','{columnName}') ep where sys.columns.name='{columnName}')");
                            sb.Append(" ,'') as varchar(100)) as ColumnDesc ");
                            sb.Append(
                                $",IsNull((select DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{table1}' and COLUMN_NAME = '{columnName}'), '') as FieldType ");
                            sb.Append($",{table1}.{columnName} as Value1 ");
                            sb.Append($",{table2}.{columnName} as Value2 ");
                            sb.Append($" FROM dbo.{table1} LEFT OUTER JOIN dbo.{table2}");
                            sb.Append($" ON {table1}.PolicyNo = {table2}.PolicyNo ");

                            if (hasCarIndex > 0)
                                sb.Append($" and {table1}.CarIndex = {table2}.CarIndex");
                            if (hasDriverIndex > 0)
                                sb.Append($" and {table1}.DriverIndex = {table2}.DriverIndex");
                            if (hasViolationIndex > 0)
                                sb.Append($" and {table1}.ViolationIndex = {table2}.ViolationIndex");

                            sb.Append($" WHERE ISNULL({table1}.PolicyNo, {table2}.PolicyNo) = '{policyNo}'");
                            sb.Append($" AND (({table1}.{columnName} IS NULL AND {table2}.{columnName} IS NOT NULL) ");
                            sb.Append($" OR ({table1}.{columnName} IS NOT NULL AND {table2}.{columnName} IS NULL) ");
                            sb.Append($" OR ({table1}.{columnName} <> {table2}.{columnName}))");

                            ExecuteNonQueryNoArgs(sb.ToString(), conn);
                        }
                    }
                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "AuditSaveDifference", ex.HResult.ToString(),
                    $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Calls stored proc spUW_AutomaticActionStep1 as is
        /// 
        /// </summary>
        /// <param name="requestedReportDate"></param>
        /// <returns>Policy list</returns>
        public List<PolicyList> TxAutomaticActionStep1(DateTime? requestedReportDate)
        {
            const string storedProcName = "spUW_AutomaticActionsStep1";
            const int timeout = 240;

            var policyList = new List<PolicyList>();

            using (var conn = GetConnection())
            {
                conn.Open();
                var resultSets = ExecuteQueryWithParametersMulti(storedProcName, new[] { CreateParamDateTime("@RequestedReportDate", requestedReportDate) }, conn, timeout);
                var resultSetCount = resultSets.Count;
                if (resultSetCount >= 1)
                {
                    // num = (int)((object[])((List<object>)resultSets[0])[0])[0];
                    // ReSharper disable once UnusedVariable
                    int? num = GetResultSetVal<int>(resultSets, 0);
                }
                if (resultSetCount >= 2)
                {
                    var objList = (List<object>)resultSets[1];
                    foreach (var obj in objList)
                    {
                        var values = (object[])obj;
                        policyList.Add(new PolicyList
                        {
                            Id_num = GetDbValOrNull<int>(values[0]),
                            PolicyNo = GetDbRefValOrNull<string>(values[1]),
                            AgentCode = GetDbRefValOrNull<string>(values[2]),
                            AgentStatus = GetDbRefValOrNull<string>(values[3]),
                            EffDate = GetDbValOrNull<DateTime>(values[4]),
                            ExpDate = GetDbValOrNull<DateTime>(values[5]),
                            CancDate = GetDbValOrNull<DateTime>(values[6]),
                            EquityDate = GetDbValOrNull<DateTime>(values[7]),
                            Annualized = GetDbValOrNull<double>(values[8]),
                            PerDiem = GetDbValOrNull<double>(values[9]),
                            Written = GetDbValOrNull<double>(values[10]),
                            TotalPaid = GetDbValOrNull<double>(values[11]),
                            TotalNSFFees = GetDbValOrNull<double>(values[12]),
                            PolicyStatus = GetDbRefValOrNull<string>(values[13]),
                            RenewalStatus = GetDbRefValOrNull<string>(values[14]),
                            CurrMinDue = GetDbValOrNull<double>(values[15]),
                            CurrDueDate = GetDbValOrNull<DateTime>(values[16]),
                            MinDueDateNotPrinted = GetDbValOrNull<DateTime>(values[17]),
                            CurrTotalDue = GetDbValOrNull<double>(values[18]),
                            NoticeAmt = GetDbValOrNull<double>(values[19]),
                            NoticeProcessDate = GetDbValOrNull<DateTime>(values[20]),
                            PaidSinceNotice = GetDbValOrNull<double>(values[21]),
                            DaysInForce = GetDbValOrNull<int>(values[22]),
                            DaysFromExpiration = GetDbValOrNull<int>(values[23]),
                            IsNSF = GetDbValOrNull<bool>(values[24]),
                            IsFNR = GetDbValOrNull<bool>(values[25]),
                            IsFNRClaims = GetDbValOrNull<bool>(values[26]),
                            OpenSuspense = GetDbValOrNull<bool>(values[27]),
                            OpenSuspenseSetToCancel = GetDbValOrNull<bool>(values[28]),
                            FirstSuspDate = GetDbValOrNull<DateTime>(values[29]),
                            SecondSuspDate = GetDbValOrNull<DateTime>(values[30]),
                            IsDirectBill = GetDbValOrNull<bool>(values[31]),
                            IsPIF = GetDbValOrNull<bool>(values[32]),
                            IsEFT = GetDbValOrNull<bool>(values[33]),
                            IsRenewal = GetDbValOrNull<bool>(values[34]),
                            _Send2ndSusp = GetDbValOrNull<bool>(values[35]),
                            _Reinstate = GetDbValOrNull<bool>(values[36]),
                            _NoticeNonPay = GetDbValOrNull<bool>(values[37]),
                            _NoticeUnd = GetDbValOrNull<bool>(values[38]),
                            _FinalCancel = GetDbValOrNull<bool>(values[39]),
                            _PrintInvoice = GetDbValOrNull<bool>(values[40]),
                            _SetZeroDueInvoice = GetDbValOrNull<bool>(values[41]),
                            _IssueNonRenewal = GetDbValOrNull<bool>(values[42]),
                            _IssueNonRenewalAgent = GetDbValOrNull<bool>(values[43]),
                            _IssueRenQuote = GetDbValOrNull<bool>(values[44]),
                            _IssueRenewal = GetDbValOrNull<bool>(values[45]),
                            _AlertPossibleUndReinstatement = GetDbValOrNull<bool>(values[46]),
                            _IssueNSFDepositNotice = GetDbValOrNull<bool>(values[47]),
                            _IssueVoidPolicy = GetDbValOrNull<bool>(values[48]),
                            _IssueNonRenewalClaimsMsg = GetDbValOrNull<bool>(values[49]),
                            _AlertPossibleUndNonRenewalReinstatement = GetDbValOrNull<bool>(values[50]),
                            _AutomaticRenewalInvoice = GetDbValOrNull<bool>(values[51]),
                            _DraftRenewalDownPayment = GetDbValOrNull<bool>(values[52]),
                            DoneProcessing = GetDbValOrNull<bool>(values[53]),
                            ActionDate = GetDbValOrNull<DateTime>(values[54]),
                            RefID = GetDbValOrNull<int>(values[55]),
                            CreateDate = GetDbValOrNull<DateTime>(values[56])
                        });
                    }
                }

                return policyList;
            }
        }

        public void TxAutomaticActionsStep3(DateTime? requestedReportDate = null)
        {
            const string userName = "SYSTEM";
            var errorSection = string.Empty;

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        /*Log Installment Fees*/
                        TxUpdateInstallmentAudit();
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(string.Empty, string.Empty, userName, DateTime.Now, "AutomaticActionsStep3", ex.HResult.ToString(),
                    $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }


        /// <summary>
        /// Delete records from Policy Endorse Quote tables for given policy #
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// ClearPolicyEndorseQuote.
        /// 
        /// Migration of stored Procedure spUW_ClearPolicyEndorseQuote
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxClearPolicyEndorseQuote(string policyNo, string userName, string guid)
        {
            var errorSection = "0: Startup";
            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        ClearPolicyEndorseQuote(policyNo, conn);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "ClearPolicyEndorseQuote",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Delete records from Policy Ren Quote Temp tables for given policy #
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// ClearPolicyRenQuoteTemp.
        /// 
        /// Migration of stored Procedure spUW_ClearPolicyRenQuoteTemp
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        public void TxClearPolicyRenQuoteTemp(string policyNo, string guid, string userName)
        {
            var errorSection = "0: Startup";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Policy Record";
    
                     ExecuteNonQueryWithPolicyNo(ClearPolicyRenQuoteTemp.DeletePolicyRenQuoteTempByPolicyNo, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: Policy Cars Record";

                        ExecuteNonQueryWithPolicyNo(ClearPolicyRenQuoteTemp.DeletePolicyRenQuoteCarsTempByPolicyNo, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "3: Policy Drivers Records";

                        ExecuteNonQueryWithPolicyNo(ClearPolicyRenQuoteTemp.DeletePolicyRenQuoteDriversTempByPolicyNo, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "4: Policy Violations Records";

                        ExecuteNonQueryWithPolicyNo(ClearPolicyRenQuoteTemp.DeletePolicyRenQuoteDriversViolationsTempByPolicyNo, policyNo,
                            conn);

                        // ------------------------------------------------------------------
                        errorSection = "5: RenQuoteInstallments";

                        ExecuteNonQueryWithPolicyNo(ClearPolicyRenQuoteTemp.DeleteRenQuoteInstallmentsTempByPolicyNo, policyNo, conn);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "ClearPolicyRenQuoteTemp", ex.HResult.ToString(),
                    $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Copy from Policy tables into AuditPolicy tables
        ///       for a given PolicyNo and History ID.
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// CopyPolicyToAuditTables.
        /// 
        /// Migration of stored Procedure spLOG_CopyPolicyToAuditTables
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="historyId"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxCopyPolicyToAuditTables(string policyNo, int historyId, string userName, string guid)
        {
            var errorSection = "0: Startup";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Insert Record into AuditPolicy. ";

                        ExecuteNonQueryWithPolicyNoPlusInt(CopyPolicyToAuditTables.CopyPolicyToAudit, policyNo, "@HistoryID", historyId, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: Insert Record into AuditPolicyCars. ";

                        ExecuteNonQueryWithPolicyNoPlusInt(CopyPolicyToAuditTables.CopyPolicyToAudit_Cars, policyNo, "@HistoryID", historyId, conn);

                        // ------------------------------------------------------------------
                        errorSection = "3: Insert Record into AuditPolicyDrivers. ";

                        ExecuteNonQueryWithPolicyNoPlusInt(CopyPolicyToAuditTables.CopyPolicyToAudit_Drivers, policyNo, "@HistoryID", historyId, conn);

                        // ------------------------------------------------------------------
                        errorSection = "4: Insert Record into AuditPolicyDriversViolations. ";

                        ExecuteNonQueryWithPolicyNoPlusInt(CopyPolicyToAuditTables.CopyPolicyToAudit_DriversViolations, policyNo, "@HistoryID",
                            historyId, conn);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "CopyPolicyToAuditTables", ex.HResult.ToString(),
                    $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Copy from PolicyEndorseQuote tables into Policy tables for a given PolicyNo
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// CopyPolicyEndorseQuoteToPolicyTables.
        /// 
        /// Migration of stored Procedure spUW_CopyPolicyEndorseQuoteToPolicyTables
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="fromWeb"></param>
        public void TxCopyPolicyEndorseQuoteToPolicyTables(string policyNo, string userName, string guid,
            bool fromWeb = false)
        {
            var errorSection = "0: Startup";
            var policyValid = true;

            if (string.IsNullOrEmpty(userName))
            {
                userName = Environment.UserName;
            }

            if (GetCountOfPolicyOidRecordsByPolicyNo(policyNo) == 0)
            {
                policyValid = false;
                TxLogError(guid, policyNo, userName, DateTime.Now, "CopyPolicyEndorseQuoteToPolicyTables", string.Empty,
                    $"Policy {policyNo} does not exist in table PolicyEndorseQuote.");
            }

            if (!policyValid) return;

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Delete Policy from Policy tables.";

                        // Don't delete from Policy tables if PolicyNo not in PolicyEndorseQuote tables.

                        DeleteIfAny(Queries.PolicyEndorseQuote_CountByPolicyNo,policyNo, Queries.Policy_DeleteByPolicyNo,conn);
                        DeleteIfAny(Queries.PolicyEndorseQuoteCars_CountByPolicyNo, policyNo, Queries.PolicyCars_DeleteByPolicyNo, conn);
                        DeleteIfAny(Queries.PolicyEndorseQuoteDrivers_CountByPolicyNo, policyNo, Queries.PolicyDrivers_DeleteByPolicyNo, conn);

                        // Driver Violations are exceptions.
                        ExecuteNonQueryWithPolicyNo(Queries.PolicyDriversViolations_DeleteByPolicyNo, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: INSERT INTO Policy.";

                        ExecuteNonQueryWithPolicyNo(CopyPolicyEndorseQuoteToPolicyTables.InsertIntoPolicy, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "3: INSERT INTO PolicyCars.";

                        ExecuteNonQueryWithPolicyNo(CopyPolicyEndorseQuoteToPolicyTables.InsertIntoPolicyCars, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "4: INSERT INTO PolicyDrivers.";

                        ExecuteNonQueryWithPolicyNo(CopyPolicyEndorseQuoteToPolicyTables.InsertIntoPolicyDrivers, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "5: INSERT INTO PolicyDriversViolations.";

                        ExecuteNonQueryWithPolicyNo(CopyPolicyEndorseQuoteToPolicyTables.InsertIntoPolicyDriversViolations, policyNo,
                            conn);

                        // ------------------------------------------------------------------
                        errorSection = "6: delete from PolicyEndorseQuote tables";

                        if (!fromWeb)
                        {
                            ExecuteNonQueryWithPolicyNo(Queries.PolicyEndorseQuote_DeleteByPolicyNo, policyNo, conn);
                            ExecuteNonQueryWithPolicyNo(Queries.PolicyEndorseQuoteCars_DeleteByPolicyNo, policyNo, conn);
                            ExecuteNonQueryWithPolicyNo(Queries.PolicyEndorseQuoteDrivers_DeleteByPolicyNo, policyNo, conn);
                            ExecuteNonQueryWithPolicyNo(Queries.PolicyEndorseQuoteDriversViolations_DeleteByPolicyNo, policyNo, conn);
                        }

                        txScope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "CopyPolicyEndorseQuoteToPolicyTables",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Copy from Policy tables into PolicyEndorseQuote tables for a given PolicyNo
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// CopyPolicyToPolicyEndorseQuoteTables.
        /// 
        /// Migration of stored Procedure spUW_CopyPolicyToPolicyEndorseQuoteTables
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxCopyPolicyToPolicyEndorseQuoteTables(string policyNo, string userName, string guid)
        {
            var errorSection = "0: Startup";

            if (string.IsNullOrEmpty(userName))
            {
                userName = Environment.UserName;
            }

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Delete PolicyNo from PolicyEndorseQuote tables.";

                        ClearPolicyEndorseQuote(policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: INSERT INTO PolicyEndorseQuote.";

                        var policyEndorseQuoteId = InTxCopyPolicyToPolicyEndorseQuoteByPolicyNo(policyNo,conn);

                        ExecuteNonQueryWithPolicyNo(CopyPolicyToPolicyEndorseQuoteTables.InsertCars, policyNo, conn);

                        ExecuteNonQueryWithPolicyNo(CopyPolicyToPolicyEndorseQuoteTables.InsertDrivers, policyNo, conn);

                        ExecuteNonQueryWithPolicyNo(CopyPolicyToPolicyEndorseQuoteTables.InsertDriversViolations, policyNo,
                            conn);

                        // ------------------------------------------------------------------
                        // Store retrieved key from first insert

                        using (var idCmd = new SqlCommand(CopyPolicyToPolicyEndorseQuoteTables.IdsCreated, conn))
                        {
                            idCmd.Parameters.Add(CreateParamInt("@PolicyEndorseQuoteId", decimal.ToInt32(policyEndorseQuoteId)));
                            idCmd.ExecuteNonQuery();
                        }

                        txScope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "CopyPolicyToPolicyEndorseQuoteTables",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }

        }

        public void TxCreateInstallmentsPolicy(string policyNo, decimal? policyWritten, int? installCount, decimal? installNo,
            DateTime? dueDate, decimal? installmentPymt, bool? nullableCreateDownPayment, bool? forceDepositToAmountPaid,
            string userName, string guid, int? historyIndexId, double? amountForForceDeposit)
        {

            var errorSection = "0: Startup";
            installmentPymt = null;
            installCount = null;
            userName = Left(userName ?? Environment.UserName,25);

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        // ---------------- Local Methods ----------------

                        int CreateVars(string name, double? val) => CreateVarsBase(userName, name, val);

                        int CreateVars2(string name, double? val) => CreateVarsBase("@UserName2", name, val);

                        int CreateVarsBase(string userName1, string name, double? val) =>
                            ExecuteNonQueryWithParameters(CreateInstallmentsPolicy.InsertIntoCreateVarsNameAndValue,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 50),
                                    CreateParamVarchar("@UserName", userName1, 50),
                                    CreateParamVarchar("@VariableName", name, 50),
                                    CreateParamFloat("@VariableValue", val),
                                }, conn);

                        int GetCountAlreadyBilled() =>
                            GetDbValOrNull<int>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetCountAlreadyBilled, policyNo, conn)) ?? 0;

                        decimal? GetDiff() =>
                            (GetDbValOrNull<decimal>(ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetDiff,
                                policyNo, conn)) - policyWritten) * (-1);

                        DateTime? GetDueDateFromInstallments() =>
                            GetDbValOrNull<DateTime>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetDueDateFromInstallments, policyNo,
                                    conn));

                        int GetFirstDue(int? pp) =>
                            (int?)GetDbValOrNull<double>(ExecuteScalarWithParameters(CreateInstallmentsPolicy.GetFirstDue,
                                new[] { CreateParamInt("@PayPlan", pp) }, conn)) ?? 30;

                        int? GetInstallCount() =>
                            GetDbValOrNull<int>(ExecuteScalarWithPolicyNo(
                                CreateInstallmentsPolicy.GetInstallCount,
                                policyNo, conn));

                        double? GetInstallCountUsingRateCycle() =>
                            GetDbValOrNull<double>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetInstallCountUsingRateCycle,
                                    policyNo, conn)) ?? 0;

                        DateTime? GetMaxDueDate() => GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetMaxDueDate, policyNo, conn));

                        DateTime? GetMinEffDateForPolicy() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetMinEffDateForPolicy, policyNo,
                                conn));

                        DateTime? GetMinEffDateFromPolicyBinders() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(
                                CreateInstallmentsPolicy.GetMinEffDateFromPolicyBinders, policyNo, conn));

                        double? GetPaidSoFar() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetPaidSoFar,
                                policyNo, conn));

                        int GetPolicyCount() => (int)ExecuteScalarWithPolicyNo(Queries.Policy_CountByPolicyNo, policyNo, conn);

                        DateTime? GetPolicyEffDate() =>
                            GetDbValOrNull<DateTime>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetPolicyEffDate, policyNo, conn));

                        decimal GetTotalAmt() =>
                            GetDbValOrNull<decimal>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.GetTotalAmt,
                                    policyNo, conn)) ?? 0M;

                        void SetIgnores() => ExecuteNonQueryWithParameters(CreateInstallmentsPolicy.SetIgnores, new []
                        {
                            CreateParamVarchar("@PolicyNo", policyNo, 50),
                            CreateParamInt("@HistoryIndexID", historyIndexId)
                        }, conn);
                        
                        void SetBalanceInstallments() => ExecuteNonQueryWithPolicyNo(CreateInstallmentsPolicy.SetBalanceInstallments, policyNo, conn);

                        void SetInstallmentPymtToBalanceInstallment() =>
                            ExecuteNonQueryWithPolicyNo(CreateInstallmentsPolicy.SetInstallmentPymtToBalanceInstallment,
                                policyNo, conn);

                        void SetPremiumBalanceZeroIfNeg() =>
                            ExecuteNonQueryWithPolicyNo(CreateInstallmentsPolicy.SetPremiumBalanceZeroIfNeg, policyNo,
                                conn);

                        decimal? SumDownPymtAmount() =>
                            GetDbValOrNull<decimal>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.SumDownPymtAmount, policyNo, conn));

                        decimal? SumFeePayments() =>
                            GetDbValOrNull<decimal>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.SumFeePayments, policyNo, conn)) ?? 0M;

                        double SumTotalAmt() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(CreateInstallmentsPolicy.SumTotalAmt,
                                policyNo, conn)) ?? 0D;

                        void UpdateBalanceInstallmentForAllInstallmentsForPolicy(string rateCycle1) =>
                            ExecuteNonQueryWithParameters(
                                CreateInstallmentsPolicy.UpdateBalanceInstallmentForAllInstallmentsForPolicy, new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamVarchar("@RateCycle", rateCycle1, 99)
                                }, conn);

                        void UpdateBalanceInstallmentsUsingDiff(decimal? diff2) =>
                            ExecuteNonQueryWithParameters(CreateInstallmentsPolicy.UpdateBalanceInstallmentsUsingDiff,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamFloat("@Diff", (double?)diff2)
                                }, conn);

                        void UpdateInstallmentsIgnore() =>
                            ExecuteNonQueryWithPolicyNoPlusInt(CreateInstallmentsPolicy.UpdateInstallmentsIgnore, policyNo,
                                "@HistoryIndexID", historyIndexId, conn);

                        void UpdatePreDueDateInstallments() =>
                            ExecuteNonQueryWithPolicyNoPlusInt(CreateInstallmentsPolicy.UpdatePreDueDateInstallments,
                                policyNo, "@HistoryIndexID", historyIndexId, conn);

                        void ZeroOutDepositFieldsForNonDepositRecord() =>
                            ExecuteNonQueryWithPolicyNo(
                                CreateInstallmentsPolicy.ZeroOutDepositFieldsForNonDepositRecord, policyNo, conn);

                        void ZeroOutInstallmentPymtAndFeesForDepositRecord() =>
                            ExecuteNonQueryWithPolicyNo(
                                CreateInstallmentsPolicy.ZeroOutInstallmentPymtAndFeesForDepositRecord, policyNo, conn);


                        // --------------End  Local Methods --------------

                        conn.Open();

                        SetIgnores();

                        CreateVars($"var @PolicyWritten {policyWritten ?? 0M}",1D);
                        CreateVars($"var @InstallCount {installCount ?? 0}", 1D);
                        CreateVars($"var @InstallNo {installNo ?? 0M}", 1D);
                        CreateVars($"var @DueDate {dueDate}", 1D);
                        CreateVars($"var @InstallmentPymt {installmentPymt ?? 0M}", 1D);
                        CreateVars($"var @CreateDownPayment {nullableCreateDownPayment}", 1D);
                        CreateVars($"var @ForceDepositToAmountPaid {forceDepositToAmountPaid ?? false}", 1D);
                        CreateVars($"var @HistoryIndexID {historyIndexId ?? 0}", 1D);
                        CreateVars($"var @AmountForForceDeposit {amountForForceDeposit ?? 0D}", 1D);

                        // ------------------------------------------------------------------
                        errorSection = "1: Delete remaining valid installments for policy. ";

                        // update all Installments whose Due Dates are not yet came. mark Ignore flag to 1

                        UpdatePreDueDateInstallments();

                        //  This Flag needs to be 0 when endorsement to be made. As Deposit will
                        //  be made only during Policy Issue Workflow

                        var createDownPayment = nullableCreateDownPayment ?? true;

                        CreateVars("1: CreateDownPayment", createDownPayment ? 1D : 0D);

                        // ------------------------------------------------------------------
                        errorSection = "3: Get parameter @InstallNo";

                        if (installNo is null)
                        {
                            installNo = createDownPayment ? 0 : 1;
                        }

                        CreateVars("3: InstallNo", (double?)installNo);

                        // ------------------------------------------------------------------
                        errorSection = "4: Get parameter @InstallCount";

                        if (installCount is null)
                        {
                            installCount = (int)GetInstallCountUsingRateCycle();
                            if (installCount == 0)
                            {
                                installCount = GetInstallCount();
                            }
                        }

                        CreateVars("4: InstallCount", (double?)installCount);

                        // ------------------------------------------------------------------
                        errorSection = "5:  Get parameter @DueDate";

                        // (from orig)
                        // Get DueDate from next valid installment. 
                        // get the next due date which is valid. this could be remaining
                        // Installments date which are more than current date
                        // and minimum from that due date.

                        if (dueDate is null)
                        {
                            dueDate = ((GetMaxDueDate()
                                        ?? GetMinEffDateForPolicy())
                                       ?? GetMinEffDateFromPolicyBinders())
                                      ?? (DateTime?)DateTime.Now.AddMonths(1);
                        }

                        CreateVars($"5: maxDueDate of Ins {dueDate}", 1D);

                        // ------------------------------------------------------------------
                        errorSection = "6: Declare variables";

                        DateTime? dateIgnored = null;
                        double? lastPayAmt = null;
                        DateTime? lastPayDate = null;

                        // Payments Include CC fees also.Like totalAmt = Actual Amount + CC Fee.This should only have Actual amount. 
                        double? paidSoFar = GetPaidSoFar();

                        CreateVars($"6: PaidSoFar {paidSoFar}", 1D);

                        // ------------------------------------------------------------------
                        errorSection = "7: Create first installment";

                        // --Create Deposit Record if @CreateDownPayment = 1
                        // --Otherwise, create first Intallment record.

                        var result = ExecuteQueryWithPolicyNo(CreateInstallmentsPolicy.CreateFirstInstallment,
                            policyNo, conn);
                        var values = (object[])result.First();

                        var dayOfMonthStr = GetDbRefValOrNull<string>(values[0]);
                        policyWritten = (decimal?)GetDbValOrNull<double>(values[1]);
                        var descr = GetDbRefValOrNull<string>(values[2]);
                        var payPlan = GetDbValOrNull<int>(values[3]);
                        var installmentFee = (decimal?)GetDbValOrNull<int>(values[4]);
                        var policyFee = (decimal?)GetDbValOrNull<double>(values[5]);
                        var sr22Fee = (decimal?)GetDbValOrNull<double>(values[6]);
                        var fhcfFee = (decimal?)GetDbValOrNull<double>(values[7]);
                        var dbSetupFee = (decimal?)GetDbValOrNull<double>(values[8]);
                        var mvrFee = (decimal?)GetDbValOrNull<double>(values[9]);
                        var maintenanceFee = (decimal?)GetDbValOrNull<double>(values[10]);
                        var pippdOnlyFee = (decimal?)GetDbValOrNull<double>(values[11]);
                        var rewriteFee = (decimal?)GetDbValOrNull<double>(values[12]);
                        var downPymtPercent = (decimal?)GetDbValOrNull<double>(values[13]);
                        var lateFee = (decimal?)GetDbValOrNull<int>(values[14]);
                        var pymtApplied = (decimal?)GetDbValOrNull<int>(values[15]);
                        var balanceInstallment = (decimal?)GetDbValOrNull<int>(values[16]);
                        var lockedInt = GetDbValOrNull<int>(values[17]);
                        var ignoreRecInt = GetDbValOrNull<int>(values[18]);
                        var effDate = GetDbValOrNull<DateTime>(values[19]);
                        var rateCycle = GetDbRefValOrNull<string>(values[20]);
                        var sixMonth = GetDbValOrNull<bool>(values[21]);

                        if (!int.TryParse(dayOfMonthStr, out var dayOfMonth))
                            throw new StoredProcedureException($"Unable to parse day of month \"{dayOfMonthStr}\"");
                        var locked = lockedInt != null ? lockedInt != 0 : null as bool?;
                        var ignoreRec = ignoreRecInt != null ? ignoreRecInt != 0 : null as bool?;

                        // -- No use of this below code as Policy Binder is of no more exist . Could be used
                        // -- during Policy Quote case and should be called when 
                        // -- CreateDownPayment Flag is 1.which should come from Policy Issue flow.

                        if (GetPolicyCount() == 0)
                        {
                            result = ExecuteQueryWithParameters(CreateInstallmentsPolicy.CreateFirstInstallment,
                            new []
                            {
                                CreateParamVarchar("@PolicyNo",policyNo,50),
                                CreateParamDecimal("@InstallNo",installNo)
                            }, conn);
                            values = (object[])result.First();

                            dayOfMonthStr = GetDbRefValOrNull<string>(values[0]);
                            policyWritten = (decimal?)GetDbValOrNull<double>(values[1]);
                            descr = GetDbRefValOrNull<string>(values[2]);
                            payPlan = GetDbValOrNull<int>(values[3]);
                            installmentFee = (decimal?)GetDbValOrNull<int>(values[4]);
                            policyFee = (decimal?)GetDbValOrNull<double>(values[5]);
                            sr22Fee = (decimal?)GetDbValOrNull<double>(values[6]);
                            fhcfFee = (decimal?)GetDbValOrNull<int>(values[7]);
                            dbSetupFee = (decimal?)GetDbValOrNull<int>(values[8]);
                            mvrFee = (decimal?)GetDbValOrNull<double>(values[9]);
                            maintenanceFee = (decimal?)GetDbValOrNull<double>(values[10]);
                            pippdOnlyFee = (decimal?)GetDbValOrNull<double>(values[11]);
                            rewriteFee = (decimal?)GetDbValOrNull<double>(values[12]);
                            downPymtPercent = (decimal?)GetDbValOrNull<double>(values[13]);
                            lateFee = (decimal?)GetDbValOrNull<int>(values[14]);
                            pymtApplied = (decimal?)GetDbValOrNull<int>(values[15]);
                            balanceInstallment = (decimal?)GetDbValOrNull<int>(values[16]);
                            lockedInt = GetDbValOrNull<int>(values[17]);
                            ignoreRecInt = GetDbValOrNull<int>(values[18]);
                            effDate = GetDbValOrNull<DateTime>(values[19]);
                            rateCycle = GetDbRefValOrNull<string>(values[20]);
                            sixMonth = GetDbValOrNull<bool>(values[21]);

                            if (!int.TryParse(dayOfMonthStr, out dayOfMonth))
                                throw new StoredProcedureException($"Unable to parse day of month \"{dayOfMonthStr}\"");
                            locked = lockedInt != null ? lockedInt != 0 : null as bool?;
                            ignoreRec = ignoreRecInt != null ? ignoreRecInt != 0 : null as bool?;
                        }

                        // -----------------------------------------------------------------------     
                        // --New Step - Reduce Policy written by any payments marked premium adjustments
                        // --@amit when will the entry goes in payments table with Premiumadjusment. 

                        var adjustmentTotal = SumTotalAmt();
                        CreateVars("7: Adjustment Total", adjustmentTotal);

                        policyWritten -= (decimal?)adjustmentTotal;
                        var written = (double?)policyWritten;

                        CreateVars("7: Adjustment Written", written);

                        // ------------------------------------------------------------------
                        errorSection = "8: Set @FeeTotal";

                        decimal? feeTotal = (policyFee ?? 0M) + (sr22Fee ?? 0M) + (fhcfFee ?? 0M) + (mvrFee ?? 0M) +
                                            (maintenanceFee ?? 0M) + (pippdOnlyFee ?? 0M) + (rewriteFee ?? 0M);

                        CreateVars("8: FeeTotal", (double?)feeTotal);

                        // ------------------------------------------------------------------
                        errorSection = "9: Set @DownPymtAmount ";

                        // --this @DownPymtAmount will always return 0. for Endorsement case this record
                        // --set to IgnoreRec = 0 and for Policy Quote , 
                        // --there will not be any record in installment table. 

                        decimal? downPymtAmount = SumDownPymtAmount();

                        CreateVars("@DownPayment", (double?)downPymtAmount);

                        // remove the check of @DownPymtAmount = 0 , This has to be created during @CreateDownPayment = 1. During Policy Issue case. 

                        if (createDownPayment || downPymtAmount == 0M)
                        {
                            downPymtAmount =
                                DecimalRoundNull(
                                    ((policyWritten ?? 0M) - (feeTotal ?? 0M)) * (downPymtPercent ?? 0M) +
                                    (feeTotal ?? 0M), 2);
                            balanceInstallment = downPymtAmount;

                            CreateVars("@DownPayment", (double?)downPymtAmount);
                        }

                        // -- this flag can be passed as 0 for Endorsement process. This is creating an
                        // -- issue during Effective date change step Endorsement. 
                        // -- Check the Installment call from Agent Portal.

                        if (forceDepositToAmountPaid != null && (bool)forceDepositToAmountPaid)
                        {
                            downPymtAmount = GetTotalAmt();

                            if (amountForForceDeposit > 50D)
                            {
                                downPymtAmount = (decimal?)amountForForceDeposit;
                            }

                        }

                        CreateVars("9: Down Payment Amount", (double?)downPymtAmount);

                        // ------------------------------------------------------------------
                        errorSection = "10:  set @InstallmentPymt ";

                        // -- @amit this will never be > 0 as all records are IgnoreRec = 1 which is made at the begining. 
                        // -- no use of the below code.
                        // -- @amit count of already billed installments are wrong. 

                        var countAlreadyBilled = GetCountAlreadyBilled();

                        var amountAlreadyBilled = SumFeePayments();

                        CreateVars("10: Count Billed", (double?)countAlreadyBilled);
                        CreateVars("10: Amount Already Billed", (double?)amountAlreadyBilled);

                        // ------------------------------------------------------------------
                        errorSection = "11: set @PremiumBalance";

                        var maxInstallFees = GetMaxBillingFee(policyWritten, downPymtAmount, installCount,
                            sixMonth ?? false ? 6 : 12);

                        decimal? tempDecimal;
                        if ((tempDecimal = (installmentFee ?? 0M) * (installCount ?? 0)) < maxInstallFees)
                        {
                            maxInstallFees = tempDecimal;
                        }

                        CreateVars("11: Max Install Fees", (double?)maxInstallFees);

                        // ------------------------------------------------------------------
                        errorSection = "11a: set @PremiumBalance";

                        // -- Handle issue where there are no installments left to bill
                        // -- @amit @AmountAlreadyBilled to be calculated correctly.It should be picked up from the payment table.
                        // -- @amit not sure when the premium balance is more than 5 $ and all the installments are paid.

                        if (installCount == 0)
                        {
                            amountAlreadyBilled = downPymtAmount ?? 0M;
                        }

                        decimal? premiumBalance = (policyWritten ?? 0M) - (amountAlreadyBilled ?? 0M);

                        var origInstallCount = installCount;

                        if (installCount <= countAlreadyBilled && Math.Abs((decimal)premiumBalance) > 5)
                        {
                            installCount += (countAlreadyBilled - installCount) + 1;
                        } // No remaining installments to bill

                        CreateVars("'11a: Original Install Count",(double?)origInstallCount);
                        CreateVars("11a: Actual Install Count",(double?)installCount);

                        // -- Calculate Installment amount based on balance and installments remaining

                        CreateVars("'11a: Create Down Payment", createDownPayment ? 1D : 0D);
                        CreateVars("Force Deposit to Paid", forceDepositToAmountPaid != null ? ((bool)forceDepositToAmountPaid ? 1D : 0D) : (double?)null);

                        // ------------------------------------------------------------------
                        errorSection = "11b: set @PremiumBalance";

                        // -- @amit logic of Installment payment is here. based on how many Installments are done and how many pending. 

                        // Calculate Installment Pymt if not passed in as parameter
                        if (installmentPymt is null)
                        {
                            var policyLessDown = (policyWritten ?? 0M) - (downPymtAmount ?? 0M);

                            if (installCount == 0) // If there are no installments - Policy is paid in full on deposit/down payment record
                            {
                                installmentPymt = DecimalRoundNull(policyLessDown, 2);
                            }
                            else
                            {
                                //  Policy has installments

                                if (createDownPayment)
                                {
                                    installmentPymt = DecimalRoundNull(policyLessDown / installCount, 2);

                                    if (installmentPymt * installCount < policyLessDown)
                                    {
                                        installmentPymt += 0.01M;
                                    }
                                    premiumBalance = policyLessDown;
                                }
                                else
                                {
                                    installmentPymt = installCount - countAlreadyBilled > 0 ? DecimalRoundNull(policyLessDown / (installCount - countAlreadyBilled), 2) : DecimalRoundNull(premiumBalance, 2);

                                    if (installmentPymt * (installCount - countAlreadyBilled) <
                                        (policyWritten ?? 0) - (decimal)(amountAlreadyBilled ?? 0))
                                    {
                                        installmentPymt += 0.01M;
                                    }
                                    premiumBalance = (policyWritten ?? 0) - (decimal)(amountAlreadyBilled ?? 0);
                                }
                            }
                        }

                        var depositDueDate = GetDueDateFromInstallments() ?? dueDate;

                        UpdateInstallmentsIgnore();

                        CreateVars($"11b.0: depDueDate {depositDueDate}", 1D);
                        CreateVars($"11b.0: DueDate {dueDate}", 1D);

                        CreateVars("11b: Installment Payment", (double?)installmentPymt);
                        CreateVars("Premium Balance", (double?)premiumBalance);
                        CreateVars("Installs Remaining", (double?)(installCount - countAlreadyBilled));


                        // ------------------------------------------------------------------
                        errorSection = "12: INSERT INTO Installments";

                        ExecuteNonQueryWithParameters(CreateInstallmentsPolicy.InsertIntoInstallments,
                            new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 99), CreateParamDecimal("@InstallNo", 0M),
                                CreateParamInt("@InstallCount", installCount ?? 0),
                                CreateParamVarchar("@Descr", descr ?? string.Empty, 99),
                                CreateParamInt("@PayPlan", payPlan),
                                CreateParamDateTime("@DueDate", depositDueDate),
                                CreateParamDecimal("@PolicyWritten", decimal.Round(policyWritten ?? 0M, 2)),
                                CreateParamDecimal("@InstallmentFee", decimal.Round(installmentFee ?? 0M, 2)),
                                CreateParamDecimal("@PolicyFee", decimal.Round(policyFee ?? 0M, 2)),
                                CreateParamDecimal("@SR22Fee", decimal.Round(sr22Fee ?? 0M, 2)),
                                CreateParamDecimal("@FHCF_Fee", decimal.Round(fhcfFee ?? 0M, 2)),
                                CreateParamDecimal("@DBSetupFee", decimal.Round(dbSetupFee ?? 0M, 2)),
                                CreateParamDecimal("@MVRFee", decimal.Round(mvrFee ?? 0M, 2)),
                                CreateParamDecimal("@MaintenanceFee", decimal.Round(maintenanceFee ?? 0M, 2)),
                                CreateParamDecimal("@PIPPDOnlyFee", decimal.Round(pippdOnlyFee ?? 0M, 2)),
                                CreateParamDecimal("@RewriteFee", decimal.Round(rewriteFee ?? 0M, 2)),
                                CreateParamDecimal("@LateFee", decimal.Round(lateFee ?? 0M, 2)),
                                CreateParamDecimal("@FeeTotal", decimal.Round(feeTotal ?? 0M, 2)),
                                CreateParamDecimal("@DownPymtPercent", downPymtPercent ?? 0M),
                                CreateParamDecimal("@DownPymtAmount", decimal.Round(downPymtAmount ?? 0M, 2)),
                                CreateParamDecimal("@InstallmentPymt", decimal.Round(installmentPymt ?? 0M, 2)),
                                CreateParamDecimal("@PremiumBalance", decimal.Round(premiumBalance ?? 0M, 2)),
                                CreateParamDecimal("@PymtApplied", decimal.Round(pymtApplied ?? 0M, 2)),
                                CreateParamDecimal("@BalanceInstallment",
                                    decimal.Round(balanceInstallment ?? 0M, 2)),
                                CreateParamBit("@Locked", locked ?? false),
                                CreateParamBit("@IgnoreRec", ignoreRec ?? false),
                                CreateParamVarchar("@UserName", userName, 99),
                                CreateParamInt("@CancIDCreatedInstall", historyIndexId)
                            }, conn);


                        // ------------------------------------------------------------------
                        errorSection = "13: While @InstallNo <  @InstallCount";

                        // Create remaining Installment records
                        decimal? runningInstallFees = 0M;
                        decimal? currInstallFee = 0M;
                        var installmentNo = 1;

                        if (countAlreadyBilled >= 1)
                        {
                            installmentNo = countAlreadyBilled + 1;
                        }
                        else
                        {
                            dueDate = GetPolicyEffDate();
                        }

                        // Loop through policy's installments
                        while (installmentNo < installCount)
                        {
                            // Set current installment fee based on maximum installment fees allowed
                            currInstallFee = 0M;

                            if (maxInstallFees - runningInstallFees > installmentFee)
                            {
                                currInstallFee = installmentFee;
                            }
                            else if (runningInstallFees < maxInstallFees)
                            {
                                currInstallFee = maxInstallFees = runningInstallFees;
                            }

                            runningInstallFees += currInstallFee;

                            CreateVars("13: Install No", (double?)installmentNo);
                            CreateVars("13: Pre Premium Balance", (double?)premiumBalance);
                            CreateVars("13: Installment Payment", (double?)installmentPymt);

                            // -- @amit not sure when this condition satisfied. i,e 6 InstallmentNo is more than 5 actual Installment.
                            // -- @removed the condition as Increment is added later.

                            premiumBalance = DecimalRoundNull(premiumBalance - installmentPymt, 2);

                            if (premiumBalance < 0M && installmentPymt > 0M)
                            {
                                installmentPymt += premiumBalance;
                            }

                            // -- Set payment date of installment to Policy.EffDate + 1 month. 
                            // -- Use same day of month of Effective date, if it is a valid date.
                            // -- Otherwise, just add 1 month.

                            CreateVars("Post Premium Balance", (double?)premiumBalance);

                            var noDays = GetFirstDue(payPlan);

                            if (installmentNo == 1)
                            {
                                dueDate = ((DateTime)dueDate).AddDays(noDays);
                                // ReSharper disable once RedundantAssignment
                                dayOfMonth = ((DateTime)dueDate).Day;
                            }
                            else
                            {
                                if (dueDate != null)
                                {
                                    if (installNo == 1)
                                    {
                                        dueDate = ((DateTime)dueDate).AddDays(noDays);
                                        // ReSharper disable once RedundantAssignment
                                        dayOfMonth = ((DateTime)dueDate).Day;
                                    }
                                    else
                                    {
                                        var nextMonth = ((DateTime)dueDate).AddMonths(1);
                                        dueDate = dayOfMonth <=
                                                  DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month)
                                            ? new DateTime(nextMonth.Year, nextMonth.Month, dayOfMonth)
                                            : nextMonth;
                                    }
                                }
                            }

                            var installDesc = $"Installment {installmentNo}";
                            if (installNo > origInstallCount)
                            {
                                installDesc = (installmentPymt + currInstallFee > 0) ? "Add Premium" : "Return Premium";
                            }

                            ExecuteNonQueryWithParameters(CreateInstallmentsPolicy.InsertIntoInstallments2,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamDecimal("@InstallNo", installmentNo),
                                    CreateParamInt("@InstallCount", installCount),
                                    CreateParamVarchar("@Desc", installDesc, 99),
                                    CreateParamInt("@PayPlan", payPlan), CreateParamDateTime("@DueDate", dueDate),
                                    CreateParamDecimal("@PolicyWritten", policyWritten),
                                    CreateParamDecimal("@InstallmentFee", currInstallFee),
                                    CreateParamDecimal("@PolicyFee", policyFee),
                                    CreateParamDecimal("@SR22Fee", sr22Fee),
                                    CreateParamDecimal("@FHCF_Fee", fhcfFee),
                                    CreateParamDecimal("@DBSetupFee", dbSetupFee),
                                    CreateParamDecimal("@MVRFee", mvrFee),
                                    CreateParamDecimal("@MaintenanceFee", maintenanceFee),
                                    CreateParamDecimal("@PIPPDOnlyFee", pippdOnlyFee),
                                    CreateParamDecimal("@RewriteFee", rewriteFee),
                                    CreateParamDecimal("@LateFee", lateFee),
                                    CreateParamDecimal("@FeeTotal", feeTotal),
                                    CreateParamDecimal("@DownPymtPercent", downPymtPercent),
                                    CreateParamDecimal("@DownPymtAmount",
                                        string.Equals(installDesc, "Add Premium") ||
                                        string.Equals(installDesc, "Return Premium")
                                            ? 0M
                                            : downPymtAmount),
                                    CreateParamDecimal("@InstallmentPymt", installmentPymt + currInstallFee),
                                    CreateParamDecimal("@PremiumBalance",
                                        premiumBalance < 0M ? 0M : premiumBalance),
                                    CreateParamDecimal("@PymtApplied", pymtApplied),
                                    CreateParamDecimal("@BalanceInstallment",
                                        string.Equals(installDesc, "Add Premium") ||
                                        string.Equals(installDesc, "Return Premium")
                                            ? installmentPymt
                                            : balanceInstallment),
                                    CreateParamBit("@Locked", locked), CreateParamBit("@IgnoreRec", ignoreRec),
                                    CreateParamDateTime("@DateIgnored", dateIgnored),
                                    CreateParamDateTime("@LastPayDate", lastPayDate),
                                    CreateParamFloat("@LastPayAmt", lastPayAmt),
                                    CreateParamVarchar("@UserName", userName, 99),
                                    CreateParamInt("@CancIDCreatedInstall", historyIndexId)
                                }, conn);

                            installmentNo += 1;
                        }

                        // ------------------------------------------------------------------
                        errorSection = "14: Zero out InstallmentPymt, Installment Fees for Deposit record.";

                        ZeroOutInstallmentPymtAndFeesForDepositRecord();

                        // ------------------------------------------------------------------
                        errorSection = "15: Zero out deposit fields for non-Deposit Installment record.";

                        ZeroOutDepositFieldsForNonDepositRecord();

                        // ------------------------------------------------------------------
                        errorSection = "16: Update BalanceInstallment for all installments for policy.";

                        UpdateBalanceInstallmentForAllInstallmentsForPolicy(rateCycle);

                        SetBalanceInstallments();

                        var diff = GetDiff();
                        CreateVars("16: Diff", (double?)diff);

                        if (diff != 0M)
                        {
                            UpdateBalanceInstallmentsUsingDiff(diff);
                            SetInstallmentPymtToBalanceInstallment();
                        }

                        if (premiumBalance < 0M)
                        {
                            SetPremiumBalanceZeroIfNeg();
                        }

                        // ------------------------------------------------------------------
                        errorSection = "12: EXEC spUW_InstallmentPaymentApply.";

                        // -- Now re-apply existing payments to the Installments for the policy.
                        // -- Re - write nightly job to update Installments.LastPayDate with Payments.PostmarkDate

                        DateTime? postmarkDate = null;

                        CreateVars2("@PolicyWritten", (double?)policyWritten);
                        CreateVars2("@InstallCount", (double?)installCount);
                        CreateVars2("@InstallNo", (double?)installNo);
                        CreateVars2("@dueDate", null);
                        CreateVars2("@InstallmentPymt", (double?)installmentPymt);
                        CreateVars2("@CreateDownPayment", createDownPayment ? 1D : 0D);
                        CreateVars2("@ForceDepositToAmountPaid", forceDepositToAmountPaid != null ? ((bool)forceDepositToAmountPaid ? 1D : 0D) : (double?)null);
                        CreateVars2("@HistoryIndexID", (double?)historyIndexId);
                        CreateVars2("@AmountForForceDeposit", (double?)amountForForceDeposit);

                        (new StoredProcedures(this)).InstallmentPaymentApply(policyNo, postmarkDate, userName, null);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "CreateInstallmentsPolicy",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        ///  Populate Installments table for a single Policy
        ///
        /// Transaction part of migration of spUW_CreateInstallmentsRenQuote
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="policyWritten"></param>
        /// <param name="installCount"></param>
        /// <param name="installNo"></param>
        /// <param name="dueDate"></param>
        /// <param name="installmentPymt"></param>
        /// <param name="nullableCreateDownPayment"></param>
        /// <param name="forceDepositToAmountPaid"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="historyIndexId"></param>
        /// <param name="amountForForceDeposit"></param>
        // ReSharper disable once RedundantAssignment
        public void TxCreateInstallmentsRenQuote(string policyNo, decimal? policyWritten, int? installCount, decimal? installNo,
            // ReSharper disable once RedundantAssignment
            DateTime? dueDate, decimal? installmentPymt, bool? nullableCreateDownPayment, bool? forceDepositToAmountPaid,
            string userName, string guid, int? historyIndexId, double? amountForForceDeposit)
        {
            var errorSection = "0: Startup";

            userName = Left(userName ?? Environment.UserName, 25);

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        // ---------------- Local Methods ----------------

                        void DeleteDebugInfo() => ExecuteNonQueryWithPolicyNo(CreateInstallmentsRenQuote.DeleteDebugInfo, policyNo, conn);

                        int GetCountRenquoteinstallments() =>
                            GetDbValOrNull<int>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetCountRenquoteinstallments, policyNo,
                                    conn)) ?? 0;

                        decimal? GetDiff()
                        {
                            return (GetDbValOrNull<decimal>(ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetDiff,
                                policyNo, conn)) - policyWritten) * (-1);
                        }

                        DateTime? GetDueDate() =>
                            GetDbValOrNull<DateTime>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetDueDate, policyNo, conn));

                        int GetFirstDue(int? pp) =>
                            (int?)GetDbValOrNull<double>(ExecuteScalarWithParameters(CreateInstallmentsRenQuote.GetFirstDue,
                                new[] { CreateParamInt("@PayPlan", pp) }, conn)) ?? 30;

                        int? GetInstallCount() =>
                            GetDbValOrNull<int>(ExecuteScalarWithPolicyNo(
                                CreateInstallmentsRenQuote.GetInstallCount,
                                policyNo, conn));

                        double? GetInstallCountUsingRateCycle() =>
                            GetDbValOrNull<double>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetInstallCountUsingRateCycle,
                                    policyNo, conn)) ?? 0;

                        decimal GetMaintenanceFeeByRatingID(int? i2) =>
                            GetDbValOrNull<decimal>(ExecuteScalarWithParameters(
                                CreateInstallmentsRenQuote.GetMaintenanceFeeByRatingID,
                                new[] { CreateParamInt("@RatingID", i2) }, conn)) ?? 0M;

                       
                        DateTime? GetMinEffDatePolicyBinders() =>
                            GetDbValOrNull<DateTime>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetMinEffDatePolicyBinders, policyNo, conn));

                        DateTime? GetMinEffDatePolicyRenQuote() =>
                            GetDbValOrNull<DateTime>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetMinEffDatePolicyRenQuote, policyNo, conn));

                        decimal GetMVRFeeByRatingID(int? ratingId3) =>
                           (decimal?)GetDbValOrNull<int>(ExecuteScalarWithParameters(
                               CreateInstallmentsRenQuote.GetMVRFeeByRatingID,
                               new[] { CreateParamInt("@RatingID", ratingId3) }, conn)) ?? 0M;


                        decimal GetPIPPDOnlyFeeByRatingID(int? ratingId2) =>
                            GetDbValOrNull<decimal>(ExecuteScalarWithParameters(
                                CreateInstallmentsRenQuote.GetPIPPDOnlyFeeByRatingID,
                                new[] { CreateParamInt("@RatingID", ratingId2) }, conn)) ?? 0M;

                        decimal GetPolicyFeeByRatingID(int? ratingId4) =>
                            GetDbValOrNull<decimal>(ExecuteScalarWithParameters(
                                CreateInstallmentsRenQuote.GetPolicyFeeByRatingID,
                                new[] { CreateParamInt("@RatingID", ratingId4) }, conn)) ?? 0M;

                        decimal GetRewriteFeeByRatingFee(int? ratingId1) =>
                            GetDbValOrNull<decimal>(ExecuteScalarWithParameters(
                                CreateInstallmentsRenQuote.GetRewriteFeeByRatingFee,
                                new[] { CreateParamInt("@RatingID", ratingId1) }, conn)) ?? 0M;

                        decimal GetSR22FeeByRatingID(int? i3) =>
                            GetDbValOrNull<decimal>(ExecuteScalarWithParameters(
                                CreateInstallmentsRenQuote.GetSR22FeeByRatingID,
                                new[] { CreateParamInt("@RatingID", i3) }, conn)) ?? 0M;

                        decimal GetTotalAmt() =>
                            GetDbValOrNull<decimal>(
                                ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.GetTotalAmt,
                                    policyNo, conn)) ?? 0M;

                        int InsertIntoCreateVarsNameAndValue(string name, double? val) =>
                            ExecuteNonQueryWithParameters(CreateInstallmentsRenQuote.InsertIntoCreateVarsNameAndValue, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 50),
                                CreateParamVarchar("@UserName", userName, 50),
                                CreateParamVarchar("@VariableName", name, 50),
                                CreateParamFloat("@VariableValue", val),
                            }, conn);

                        void SetIgnores() => ExecuteNonQueryWithPolicyNo(CreateInstallmentsRenQuote.SetIgnore, policyNo, conn);
                       

                        void SetPremiumBalanceZeroIfNeg() =>
                            ExecuteNonQueryWithPolicyNo(CreateInstallmentsRenQuote.SetPremiumBalanceZeroIfNeg, policyNo,
                                conn);

                        void SetInstallmentPymtToBalanceInstallment() =>
                            ExecuteNonQueryWithPolicyNo(CreateInstallmentsRenQuote.SetInstallmentPymtToBalanceInstallment,
                                policyNo, conn);

                        decimal SumDownPymtAmount() =>
                            GetDbValOrNull<decimal>(ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.SumDownPymtAmount,
                                policyNo, conn)) ?? 0M;

                        double SumFeesPayments() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.SumFeesPayments,
                                policyNo, conn)) ?? 0D;

                        double SumTotalAmt() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(CreateInstallmentsRenQuote.SumTotalAmt,
                                policyNo, conn)) ?? 0D;

                        void UpdateBalanceInstallmentForAllInstallmentsForPolicy(string rateCycle1) =>
                            ExecuteNonQueryWithParameters(
                                CreateInstallmentsRenQuote.UpdateBalanceInstallmentForAllInstallmentsForPolicy, new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamVarchar("@RateCycle", rateCycle1, 99)
                                },
                                conn);

                        void UpdateBalanceInstallmentsUsingDiff(decimal? diff2) =>
                            ExecuteNonQueryWithParameters(CreateInstallmentsRenQuote.UpdateBalanceInstallmentsUsingDiff,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamFloat("@Diff", (double?)diff2)
                                }, conn);

                        void ZeroOutDepositFieldsForNonDepositRecord() =>
                            ExecuteNonQueryWithPolicyNo(
                                CreateInstallmentsRenQuote.ZeroOutDepositFieldsForNonDepositRecord, policyNo, conn);

                        void ZeroOutInstallmentPymtAndFeesForDepositRecord() =>
                            ExecuteNonQueryWithPolicyNo(
                                CreateInstallmentsRenQuote.ZeroOutInstallmentPymtAndFeesForDepositRecord, policyNo, conn);

                        // ---------------- End Local Methods ----------------

                        conn.Open();

                        installmentPymt = null;
                        installCount = null;

                        // ------------------------------------------------------------------
                        errorSection = "1: Delete remaining valid RenQuoteInstallments for policy. ";

                        // -- Delete remaining valid RenQuoteInstallments for policy.  
                        // --This is needed if this stored procedure is called more than once.
                     
                        SetIgnores();

                        // ------------------------------------------------------------------
                        errorSection = "2: Update Policy.PolicyWritten if parameter passed.. ";

                        var createDownPayment = nullableCreateDownPayment ?? true;

                        // ------------------------------------------------------------------
                        errorSection = "3: Get parameter @InstallNo";

                        var installmentNo = 0;

                        if (installNo is null)
                        {
                            installNo = createDownPayment ? 0 : 1;
                        }

                        // ------------------------------------------------------------------
                        errorSection = "4: Get parameter @InstallCount";

                        if (installCount is null)
                        {
                            installCount = (int)GetInstallCountUsingRateCycle();
                            if (installCount == 0)
                            {
                                installCount =  GetInstallCount();
                            }
                        }

                        // ------------------------------------------------------------------
                        errorSection = "5:  Get parameter @DueDate";

                        if (dueDate is null)
                        {
                            // Get DueDate from next valid installment.
                            //- If not found, get DueDate from Policy Effective date.
                            //-- If not found, get DueDate from PolicyBinders Effective date.
                            //--- otherwise 1 month from today.

                            dueDate = ((GetDueDate()
                                        ?? GetMinEffDatePolicyRenQuote())
                                       ?? GetMinEffDatePolicyBinders())
                                      ??  (DateTime?)DateTime.Now.AddMonths(1);
                        
                        }

                        // ------------------------------------------------------------------
                        errorSection = "6: Declare variables";

                        DateTime? dateIgnored = null;
                        //DateTime? effDate;
                        double? lastPayAmt = null;
                        DateTime? lastPayDate = null;

                        // Clean up debug information for policy      

                        DeleteDebugInfo();

                        // ------------------------------------------------------------------
                        errorSection = "7: Create first installment";

                        // --Create Deposit Record if @CreateDownPayment = 1
                        // -- Otherwise, create first Installment record.

                        var result = ExecuteQueryWithParameters(CreateInstallmentsRenQuote.CreateFirstInstallment, new []
                            {
                                CreateParamVarchar("@PolicyNo",policyNo,50),
                                CreateParamDecimal("@InstallNo",installNo)
                            },
                            conn);
                        var values = (object[])result.First();
                        var dayOfMonthStr = GetDbRefValOrNull<string>(values[0]);
                        policyWritten = (decimal?)GetDbValOrNull<double>(values[1]);
                        var descr = GetDbRefValOrNull<string>(values[2]);
                        var payPlan = GetDbValOrNull<int>(values[3]);
                        var installmentFee = (decimal?)GetDbValOrNull<int>(values[4]);
                        var policyFee = (decimal?)GetDbValOrNull<double>(values[5]);
                        var sr22Fee = (decimal?)GetDbValOrNull<double>(values[6]);
                        var fhcfFee = (decimal?)GetDbValOrNull<int>(values[7]);
                        var dbSetupFee = (decimal?)GetDbValOrNull<int>(values[8]);
                        var mvrFee = (decimal?)GetDbValOrNull<double>(values[9]);
                        var maintenanceFee = (decimal?)GetDbValOrNull<double>(values[10]);
                        var pippdOnlyFee = (decimal?)GetDbValOrNull<double>(values[11]);
                        var rewriteFee = (decimal?)GetDbValOrNull<double>(values[12]);
                        var downPymtPercent = (decimal?)GetDbValOrNull<double>(values[13]);
                        var lateFee = (decimal?)GetDbValOrNull<int>(values[14]);
                        var pymtApplied = (decimal?)GetDbValOrNull<int>(values[15]);
                        var balanceInstallment = (decimal?)GetDbValOrNull<int>(values[16]);
                        var lockedInt = GetDbValOrNull<int>(values[17]);
                        var ignoreRecInt = GetDbValOrNull<int>(values[18]);
                        var rateCycle = GetDbRefValOrNull<string>(values[20]);
                        var sixMonth = GetDbValOrNull<bool>(values[21]);
                        var ratingId = GetDbValOrNull<int>(values[22]);

                        if (!int.TryParse(dayOfMonthStr, out var dayOfMonth))
                            throw new StoredProcedureException($"Unable to parse day of month \"{dayOfMonthStr}\"");
                        var locked = lockedInt != null ?  lockedInt != 0 : null as bool?;
                        var ignoreRec = ignoreRecInt != null ? ignoreRecInt != 0 : null as bool?;


                        if (policyFee == 0M) policyFee = GetPolicyFeeByRatingID(ratingId);
                    
                        if (sr22Fee == 0M) sr22Fee = GetSR22FeeByRatingID(ratingId);
                    
                        if (mvrFee == 0M) mvrFee = GetMVRFeeByRatingID(ratingId);
                    
                        if (maintenanceFee == 0M) maintenanceFee = GetMaintenanceFeeByRatingID(ratingId);
                    
                        if (pippdOnlyFee == 0M) pippdOnlyFee = GetPIPPDOnlyFeeByRatingID(ratingId);
                    
                        if (rewriteFee == 0M) rewriteFee = GetRewriteFeeByRatingFee(ratingId);
                    

                        // Reduce written by any payments marked premium adjustments

                        var adjustmentTotal = SumTotalAmt();
                        InsertIntoCreateVarsNameAndValue("Adjustment Total", adjustmentTotal);

                        policyWritten -= (decimal?)adjustmentTotal;
                        var written = (double?)policyWritten;
                        InsertIntoCreateVarsNameAndValue("Adjustment Written", written);

                        // ------------------------------------------------------------------
                        errorSection = "8: Set @FeeTotal";

                        decimal? feeTotal = (policyFee ?? 0M) + (sr22Fee ?? 0M) + (fhcfFee ?? 0M) + (mvrFee ?? 0M) +
                                            (maintenanceFee ?? 0M) + (pippdOnlyFee ?? 0M) + (rewriteFee ?? 0M);

                        // ------------------------------------------------------------------
                        errorSection = "9: Set @DownPymtAmount ";

                        decimal? downPymtAmount = SumDownPymtAmount();

                        if (createDownPayment || downPymtAmount == 0M)
                        {
                            downPymtAmount = DecimalRoundNull(((policyWritten ?? 0M) - (feeTotal ?? 0M)) * (downPymtPercent ?? 0M) +
                                                              (feeTotal ?? 0M),2);
                        }

                        if (forceDepositToAmountPaid != null && (bool)forceDepositToAmountPaid)
                        {
                            downPymtAmount = GetTotalAmt();

                            if (amountForForceDeposit > 50D)
                            {
                                downPymtAmount = (decimal?)amountForForceDeposit;
                            }
                        
                        }

                        InsertIntoCreateVarsNameAndValue("Down Payment Amount",(double?)downPymtAmount);

                        // ------------------------------------------------------------------
                        errorSection = "10:  set @InstallmentPymt ";

                        var countAlreadyBilled = GetCountRenquoteinstallments();

                        double? amountAlreadyBilled = SumFeesPayments();

                        InsertIntoCreateVarsNameAndValue("Count Billed",(double?)countAlreadyBilled);

                        InsertIntoCreateVarsNameAndValue("Amount ALready Billed",amountAlreadyBilled);

                        installNo = countAlreadyBilled;

                        // ------------------------------------------------------------------
                        errorSection = "11: set @PremiumBalance";

                        var maxInstallFees = GetMaxBillingFee(policyWritten, downPymtAmount, installCount,
                            sixMonth ?? false ? 6 : 12);

                        decimal? tempDecimal;
                        if ((tempDecimal = (installmentFee ?? 0M) * (installCount ?? 0)) < maxInstallFees)
                        {
                            maxInstallFees = tempDecimal;
                        }

                        InsertIntoCreateVarsNameAndValue("Max Install Fees",(double?)maxInstallFees);

                        // ------------------------------------------------------------------
                        errorSection = "11a: set @PremiumBalance";

                        // Handle issue where there are no RenQuoteInstallments left to bill

                        decimal? premiumBalance = (policyWritten ?? 0M) - (decimal)(amountAlreadyBilled ?? 0D);

                        var origInstallCount = installCount;

                        if (installCount <= countAlreadyBilled && Math.Abs((decimal)premiumBalance) > 5)
                        {
                            installCount = installCount + (countAlreadyBilled - installCount) + 1;
                        } // No remaining installments to bill

                        InsertIntoCreateVarsNameAndValue("Original Install Count",(double?)origInstallCount);
                        InsertIntoCreateVarsNameAndValue("Actual Install Count", (double?)installCount);

                        // Calculate Installment amount based on balance and installments remaining

                        InsertIntoCreateVarsNameAndValue("CreateD Down Payment",createDownPayment ? 1D : 0D);
                        InsertIntoCreateVarsNameAndValue("Force Deposit to Paid",forceDepositToAmountPaid != null ? ((bool)forceDepositToAmountPaid ? 1D : 0D) : (double?)null);


                        // ------------------------------------------------------------------
                        errorSection = "11b: set @PremiumBalance";

                        // Calculate Installment Pymt if not passed in as parameter
                        if (installmentPymt is null)
                        {
                            var policyLessDown = (policyWritten ?? 0M) - (downPymtAmount ?? 0M);

                            if (installCount == 0) // If there are no installments - Policy is paid in full on deposit/down payment record
                            {
                                installmentPymt = DecimalRoundNull(policyLessDown, 2);
                            }
                            else
                            {
                                //  Policy has installments

                                if (createDownPayment)
                                {
                                    installmentPymt = DecimalRoundNull(policyLessDown / installCount, 2);

                                    if (installmentPymt * installCount < policyLessDown)
                                    {
                                        installmentPymt += 0.01M;
                                    }
                                    premiumBalance = policyLessDown;
                                }
                                else
                                {
                                    installmentPymt = installCount - countAlreadyBilled > 0 ? DecimalRoundNull(policyLessDown / (installCount - countAlreadyBilled), 2) : DecimalRoundNull(premiumBalance, 2);

                                    if (installmentPymt * (installCount - countAlreadyBilled) <
                                        (policyWritten ?? 0) - (decimal)(amountAlreadyBilled ?? 0))
                                    {
                                        installmentPymt += 0.01M;
                                    }
                                    premiumBalance = (policyWritten ?? 0) - (decimal)(amountAlreadyBilled ?? 0);
                                }
                            }
                        }

                        InsertIntoCreateVarsNameAndValue("Installment Payment",(double?)installmentPymt);
                        InsertIntoCreateVarsNameAndValue("Premium Balance", (double?)premiumBalance);
                        InsertIntoCreateVarsNameAndValue("Installs Remaining", (double?)(installCount - countAlreadyBilled));


                        // ------------------------------------------------------------------
                        errorSection = "12: INSERT INTO RenQuoteInstallments";

                        if (installNo == 0M && amountAlreadyBilled == 0D)
                        {
                            ExecuteNonQueryWithParameters(CreateInstallmentsRenQuote.InsertIntoRenQuoteInstallments, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 99),
                                CreateParamDecimal("@InstallNo", installNo),
                                CreateParamInt("@InstallCount", installCount ?? 0),
                                CreateParamVarchar("@Descr", descr ?? string.Empty, 99),
                                CreateParamInt("@PayPlan", payPlan),
                                CreateParamDateTime("@DueDate", dueDate),
                                CreateParamDecimal("@PolicyWritten", decimal.Round(policyWritten ?? 0M,2)),
                                CreateParamDecimal("@InstallmentFee", decimal.Round(installmentFee ?? 0M,2)),
                                CreateParamDecimal("@PolicyFee", decimal.Round(policyFee ?? 0M,2)),
                                CreateParamDecimal("@SR22Fee", decimal.Round(sr22Fee ?? 0M,2)),
                                CreateParamDecimal("@FHCF_Fee", decimal.Round(fhcfFee ?? 0M,2)),
                                CreateParamDecimal("@DBSetupFee", decimal.Round(dbSetupFee ?? 0M,2)),
                                CreateParamDecimal("@MVRFee", decimal.Round(mvrFee ?? 0M,2)),
                                CreateParamDecimal("@MaintenanceFee", decimal.Round(maintenanceFee ?? 0M,2)),
                                CreateParamDecimal("@PIPPDOnlyFee", decimal.Round(pippdOnlyFee ?? 0M,2)),
                                CreateParamDecimal("@RewriteFee", decimal.Round(rewriteFee ?? 0M,2)),
                                CreateParamDecimal("@LateFee", decimal.Round(lateFee ?? 0M,2)),
                                CreateParamDecimal("@FeeTotal", decimal.Round(feeTotal ?? 0M,2)),
                                CreateParamDecimal("@DownPymtPercent",downPymtPercent ?? 0M),
                                CreateParamDecimal("@DownPymtAmount", decimal.Round(downPymtAmount ?? 0M,2)),
                                CreateParamDecimal("@InstallmentPymt", decimal.Round(installmentPymt ?? 0M,2)),
                                CreateParamDecimal("@PremiumBalance",decimal.Round( premiumBalance ?? 0M,2)),
                                CreateParamDecimal("@PymtApplied", decimal.Round(pymtApplied ?? 0M,2)),
                                CreateParamDecimal("@BalanceInstallment", decimal.Round(balanceInstallment ?? 0M,2)),
                                CreateParamBit("@Locked", locked ?? false),
                                CreateParamBit("@IgnoreRec", ignoreRec ?? false),
                                CreateParamVarchar("@UserName", userName, 99),
                                CreateParamInt("@HistoryIndexID", historyIndexId)
                            }, conn);
                        }

                        // ------------------------------------------------------------------
                        errorSection = "13: While @InstallNo <  @InstallCount";

                        // Create remaining Installment records
                        decimal? runningInstallFees = 0M;
                        decimal? currInstallFee = 0M;

                        // Loop through policy's installments
                        while (installmentNo < installCount)
                        {
                            // Set current installment fee based on maximum installment fees allowed
                            currInstallFee = 0M;

                            if (maxInstallFees - runningInstallFees > installmentFee)
                            {
                                currInstallFee = installmentFee;
                            }
                            else if (runningInstallFees < maxInstallFees)
                            {
                                currInstallFee = maxInstallFees = runningInstallFees;
                            }

                            runningInstallFees += currInstallFee;
                            installmentNo += 1;

                            InsertIntoCreateVarsNameAndValue("Install No", (double?)installmentNo);
                            InsertIntoCreateVarsNameAndValue("Premium Balance", (double?)premiumBalance);
                            InsertIntoCreateVarsNameAndValue("Pre Premium Balance", (double?)premiumBalance);

                            if (installmentNo > installNo)
                            {
                                premiumBalance = DecimalRoundNull(premiumBalance - installmentPymt, 2);

                                if (premiumBalance < 0M && installmentPymt > 0M)
                                {
                                    installmentPymt += premiumBalance;
                                }
                            }

                        }

                        // -- Set payment date of installment to Policy.EffDate + 1 month. 
                        // -- Use same day of month of Effective date, if it is a valid date.
                        // -- Otherwise, just add 1 month.

                        InsertIntoCreateVarsNameAndValue("Post Premium Balance", (double?)premiumBalance);

                        var noDays = GetFirstDue(payPlan);

                        if (installmentNo == 1)
                        {
                            dueDate = ((DateTime)dueDate).AddDays(noDays);
                            // ReSharper disable once RedundantAssignment
                            dayOfMonth = ((DateTime)dueDate).Day;
                        }
                        else
                        {
                            if (dueDate != null)
                            {
                                if (installNo == 1)
                                {
                                    dueDate = ((DateTime)dueDate).AddDays(noDays);
                                    // ReSharper disable once RedundantAssignment
                                    dayOfMonth = ((DateTime)dueDate).Day;
                                }
                                else
                                {
                                    var nextMonth = ((DateTime)dueDate).AddMonths(1);
                                    dueDate = dayOfMonth <=
                                              DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month)
                                        ? new DateTime(nextMonth.Year, nextMonth.Month, dayOfMonth)
                                        : nextMonth;
                                }
                            }
                        }

                        var installDesc = $"Installment {installmentNo}";

                        if (installNo > origInstallCount)
                        {
                            installDesc = (installmentPymt + currInstallFee > 0) ? "Add Premium" : "Return Premium";
                        }

                        if (installmentNo > installNo)
                        {
                            ExecuteNonQueryWithParameters(CreateInstallmentsRenQuote.InsertIntoRenQuoteInstallmentsXX, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 99),
                                CreateParamDecimal("@InstallmentNo", installmentNo),
                                CreateParamInt("@InstallCount", installCount),
                                CreateParamVarchar("@InstallDesc", installDesc, 99),
                                CreateParamInt("@PayPlan", payPlan),
                                CreateParamDateTime("@DueDate", dueDate),
                                CreateParamDecimal("@PolicyWritten", policyWritten),
                                CreateParamDecimal("@currInstallFee", currInstallFee),
                                CreateParamDecimal("@PolicyFee", policyFee),
                                CreateParamDecimal("@SR22Fee", sr22Fee),
                                CreateParamDecimal("@FHCF_Fee", fhcfFee),
                                CreateParamDecimal("@DBSetupFee", dbSetupFee),
                                CreateParamDecimal("@MVRFee", mvrFee),
                                CreateParamDecimal("@MaintenanceFee", maintenanceFee),
                                CreateParamDecimal("@PIPPDOnlyFee", pippdOnlyFee),
                                CreateParamDecimal("@RewriteFee", rewriteFee),
                                CreateParamDecimal("@LateFee", lateFee),
                                CreateParamDecimal("@FeeTotal", feeTotal),
                                CreateParamDecimal("@DownPymtPercent",downPymtPercent),
                                CreateParamDecimal("@DownPymtAmountCalc", string.Equals(installDesc,"Add Premium") || string.Equals(installDesc,"Return Premium") ? 0M : downPymtAmount),
                                CreateParamDecimal("@InstallmentPymtCalc", installmentPymt + currInstallFee),
                                CreateParamDecimal("@PremiumBalanceCalc",premiumBalance < 0M ? 0M : premiumBalance),
                                CreateParamDecimal("@PymtApplied", pymtApplied),
                                CreateParamDecimal("@BalanceInstallmentCalc", string.Equals(installDesc,"Add Premium") || string.Equals(installDesc,"Return Premium") ? installmentPymt: balanceInstallment),
                                CreateParamBit("@Locked", locked),
                                CreateParamBit("@IgnoreRec", ignoreRec),
                                CreateParamDateTime("@DateIgnored", dateIgnored),
                                CreateParamDateTime("@LastPayDate", lastPayDate),
                                CreateParamFloat("@LastPayAmt", lastPayAmt),
                                CreateParamVarchar("@UserName", userName, 99),
                                CreateParamInt("@HistoryIndexID", historyIndexId)
                            }, conn);
                        } // end of individual policy loop     

                        // ------------------------------------------------------------------
                        errorSection = "14: Zero out InstallmentPymt, Installment Fees for Deposit record.";

                        ZeroOutInstallmentPymtAndFeesForDepositRecord();

                        // ------------------------------------------------------------------
                        errorSection = "15: Zero out deposit fields for non-Deposit Installment record.";

                        ZeroOutDepositFieldsForNonDepositRecord();

                        // ------------------------------------------------------------------
                        errorSection = "16: Update BalanceInstallment for all installments for policy.";

                        UpdateBalanceInstallmentForAllInstallmentsForPolicy(rateCycle);

                        var diff = GetDiff();
                        InsertIntoCreateVarsNameAndValue("Diff",(double?)diff);

                        if (diff != 0M)
                        {
                            UpdateBalanceInstallmentsUsingDiff(diff);
                            SetInstallmentPymtToBalanceInstallment();
                        }

                        if (premiumBalance < 0M)
                        {
                            SetPremiumBalanceZeroIfNeg();
                        }

                        // ------------------------------------------------------------------
                        errorSection = "12: EXEC spUW_InstallmentPaymentApply.";

                        // -- Now re-apply existing payments to the Installments for the policy.
                        // --Re - write nightly job to update RenQuoteInstallments.LastPayDate with Payments.PostmarkDate

                        DateTime? postmarkDate = null;

                        (new StoredProcedures(this)).InstallmentPaymentApply(policyNo,postmarkDate,userName,null);

                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "CreateInstallmentsRenQuote",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Gets the list of projected installments including deposit
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// GetListProjectedInstallments.
        /// 
        /// Migration of stored procedure spUW_GetListProjectedInstallments
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="ratingId"></param>
        /// <param name="payPlan"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        [SuppressMessage("ReSharper", "ReturnValueOfPureMethodIsNotUsed")]
        public List<object> TxGetListProjectedInstallments(string policyNo, int ratingId, int payPlan,
            Guid? guid = null)
        {
            var errorSection = "0: Startup";

            var returnSelects = new List<object>();
            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        var createDownPayment =
                            true; // use true if Deposit/Down payment installment record is to be created. Used in PremiumChange() proc.
                        //bool stop = false;
                        decimal? installmentFee = 0M;
                        decimal? lateFee = null;
                        decimal? pymtApplied = null;
                        int? payPlanId;


                        var sqlParams = new[]
                        {

                            CreateParamVarchar("@PolicyNo", policyNo, 100),
                            CreateParamInt("@RatingID", ratingId)
                        };
                        var result =
                            ExecuteQueryWithParameters(GetListProjectedInstallments.GetRatingMainInfo, sqlParams, conn);

                        var values = (object[])result.First();
                        //var renCount = GetDbValOrNull<int>(values[0]);
                        var policyTerm = (int)GetDbValOrNull<int>(values[1]);
                        var effDate = GetDbValOrNull<DateTime>(values[2]);
                        var rateCycle = GetDbRefValOrNull<string>(values[3]);
                        var oPolicyWritten = (decimal?)GetDbValOrNull<double>(values[4]);
                        decimal? policyFee = (int)values[5];
                        decimal? sr22Fee = (int)values[6];
                        decimal? oMaintenanceFee = (decimal)(double)values[7];
                        decimal? pippdOnlyFee = (decimal)(double)values[8];
                        decimal? rewriteFee = (decimal)(double)values[9];
                        decimal? mvrFee = (int)values[10];

                        var oFeeTotal = policyFee + sr22Fee + oMaintenanceFee + pippdOnlyFee + mvrFee + rewriteFee;
                        var policyPremium = oPolicyWritten - oFeeTotal;

                        // Temporary Dev Patch ?
                        if (policyTerm == 12)
                        {
                            policyTerm = 6;
                        }

                        // get play plan info by policyNo and rateCycle

                        sqlParams = new[]
                        {
                            CreateParamFloat("@PolicyTerm", policyTerm),
                            CreateParamVarchar("@RateCycle", rateCycle, 20)
                        };
                        var results =
                            ExecuteQueryWithParameters(GetListProjectedInstallments.GetPayPlanInfo, sqlParams, conn);
                        var tmpPayPlans = new List<TmpPayPlans>();
                        var indexCounter = 0;
                        foreach (var rec in results)
                        {

                            var recValues = (object[])rec;
                            tmpPayPlans.Add(new TmpPayPlans()
                            {
                                IndexID = ++indexCounter,
                                PayPlanID = GetDbValOrNull<int>(recValues[0]),
                                State = GetDbRefValOrNull<string>(recValues[1]),
                                Term = GetDbValOrNull<double>(recValues[2]),
                                Code = GetDbRefValOrNull<string>(recValues[3]),
                                EFT = GetDbValOrNull<double>(recValues[4]),
                                BI = GetDbValOrNull<double>(recValues[5]),
                                NewOrRen = GetDbRefValOrNull<string>(recValues[6]),
                                DP_AsPerc = GetDbValOrNull<double>(recValues[7]),
                                Installments = GetDbValOrNull<double>(recValues[8]),
                                FirstDue = GetDbValOrNull<double>(recValues[9]),
                                BillCycle = GetDbValOrNull<double>(recValues[10]),
                                RenQuote = GetDbRefValOrNull<string>(recValues[11]),
                                RateCycle = GetDbRefValOrNull<string>(recValues[12])
                            });

                        }

                        var tempFinal = new TempFinal();

                        // ------------------------------------------------------------------
                        errorSection = "5: Create installments";

                        var currCount = 1;
                        int? maxCount = tmpPayPlans.Max(p => p.IndexID);
                        var iFeeWith = new List<IFee>();
                        while (currCount <= maxCount)
                        {
                            var plist = (from p in tmpPayPlans
                                where p.IndexID == currCount
                                select new
                                {
                                    p.IndexID,
                                    p.EFT,
                                    p.DP_AsPerc,
                                    p.Installments,
                                    p.FirstDue,
                                    p.BillCycle
                                }).ToList();
                            if (plist.Count == 0)
                                throw new StoredProcedureException("Empty tmp pay plans list");
                            var prec = plist.First();
                            payPlanId = prec.IndexID;
                            var isEFT = (int?)prec.EFT;
                            var downPymtPercent = (decimal?)prec.DP_AsPerc;
                            var installCount = (int?)prec.Installments;
                            var firstDueNoDays = (int?)prec.FirstDue;
                            //var secondDays = (int?)prec.BillCycle;


                            if (!createDownPayment)
                            {
                                createDownPayment = true;
                            }

                            int? installNo = createDownPayment ? 0 : 1;

                            int? paymentType= 2;
                            var dueDate= effDate;
                            var descr= installNo == 0 ? "Deposit" : $"Installment {installNo}";
                            // override installment payment.  Parameter is needed for PremiumChange procedure.
                            decimal? installmentPymt = 0M;

                            if (downPymtPercent == 1M)
                            {
                                installmentFee = 0M;
                                paymentType = 3;
                            }

                            if (isEFT == 1)
                            {
                                paymentType = 4;
                            }

                            var policyWritten = oPolicyWritten;
                            var feeTotal = oFeeTotal;
                            var maintenanceFee = oMaintenanceFee;


                            var downPymtAmount = DecimalRoundNull(policyPremium * downPymtPercent + feeTotal, 2);

                            var balanceInstallment = downPymtAmount;

                            if (installmentPymt == 0M)
                            {
                                installmentPymt = installCount == 0
                                    ? DecimalRoundNull(policyWritten - downPymtAmount + installmentFee,
                                        2)
                                    : DecimalRoundNull((policyWritten - downPymtAmount) / installCount,
                                        2);
                            }

                            decimal? premiumBalance = decimal.Round((policyWritten ?? 0M) - (downPymtAmount ?? 0M), 2);

                            tempFinal.AddRecord(new TempFinalRecord()
                            {
                                InstallNo = installNo,
                                PaymentType = paymentType,
                                NumberOfPayments = 1 + (installCount ?? 0),
                                InstallmentDesc = descr ?? string.Empty,
                                PayPlan = payPlanId,
                                DueDate = dueDate,
                                PolicyWritten = DecimalRoundNull(policyWritten ?? 0M, 2),
                                InstallmentFee = DecimalRoundNull(installmentFee ?? 0M, 2),
                                PolicyFee = DecimalRoundNull(policyFee ?? 0M, 2),
                                SR22Fee = DecimalRoundNull(sr22Fee ?? 0M, 2),
                                MVRFee = DecimalRoundNull(mvrFee ?? 0M, 2),
                                LateFee = DecimalRoundNull(lateFee ?? 0M, 2),
                                MaintenanceFee = DecimalRoundNull(maintenanceFee ?? 0M, 2),
                                PIPPDOnlyFee = DecimalRoundNull(pippdOnlyFee ?? 0M, 2),
                                RewriteFee = DecimalRoundNull(rewriteFee ?? 0M, 2),
                                FeeTotal = DecimalRoundNull(feeTotal ?? 0M, 2),
                                DownPaymentPct = downPymtPercent ?? 0M,
                                DownPaymentAmt = DecimalRoundNull(downPymtAmount ?? 0M, 2),
                                InstallmentAmt = DecimalRoundNull(installmentPymt ?? 0M, 2),
                                PremiumBalance = DecimalRoundNull(premiumBalance ?? 0M, 2),
                                PymtApplied = DecimalRoundNull(pymtApplied ?? 0M, 2),
                                BalanceInstallment = DecimalRoundNull(balanceInstallment ?? 0M, 2)
                            });

                            var dayOfMonth = 0;
                            while (installNo < installCount)
                            {
                                installNo++;

                                premiumBalance = DecimalRoundNull(premiumBalance - installmentPymt, 2);


                                if (dueDate != null)
                                {
                                    if (installNo == 1)
                                    {
                                        dueDate = dueDate != null && firstDueNoDays != null
                                            ? ((DateTime)dueDate).AddDays((int)firstDueNoDays)
                                            : (DateTime?)null;
                                        // ReSharper disable once RedundantAssignment
                                        dayOfMonth = ((DateTime)dueDate).Day;
                                    }
                                    else
                                    {
                                        var nextMonth = ((DateTime)dueDate).AddMonths(1);
                                        dueDate = dayOfMonth <=
                                                  DateTime.DaysInMonth(nextMonth.Year, nextMonth.Month)
                                            ? new DateTime(nextMonth.Year, nextMonth.Month, dayOfMonth)
                                            : nextMonth;
                                    }
                                }

                                tempFinal.AddRecord(new TempFinalRecord
                                {
                                    InstallNo = installNo,
                                    PaymentType = paymentType,
                                    NumberOfPayments = 1 + installCount,
                                    InstallmentDesc = $"Installment {installNo}",
                                    PayPlan = payPlanId,
                                    DueDate = dueDate,
                                    PolicyWritten = policyWritten,
                                    InstallmentFee = installmentFee ?? 0M,
                                    PolicyFee = policyFee ?? 0M,
                                    SR22Fee = sr22Fee ?? 0M,
                                    MVRFee = mvrFee ?? 0M,
                                    LateFee = lateFee ?? 0M,
                                    MaintenanceFee = maintenanceFee ?? 0M,
                                    PIPPDOnlyFee = pippdOnlyFee ?? 0M,
                                    RewriteFee = rewriteFee ?? 0M,
                                    FeeTotal = feeTotal ?? 0M,
                                    DownPaymentPct = downPymtPercent ?? 0M,
                                    DownPaymentAmt = downPymtAmount ?? 0M,
                                    InstallmentAmt = installmentPymt ?? 0M,
                                    PremiumBalance = premiumBalance ?? 0M,
                                    PymtApplied = pymtApplied ?? 0M,
                                    BalanceInstallment = balanceInstallment ?? 0M
                                });


                            } // while InstallNo

                            // ---------------------------------
                            //    UPDATE #TempFinal SET
                            //    	InstallmentAmt = 0
                            //    	,InstallmentFee = 0
                            //    	,DueDate = @EffDate
                            //    WHERE InstallmentDesc = 'Deposit';
                            //
                            // Following code performs the above SQL

                            tempFinal.Table.Where(c => string.Equals(c.InstallmentDesc, "Deposit")).ToList().Select(c =>
                            {
                                c.InstallmentAmt = 0M;
                                c.InstallmentFee = 0M;
                                c.DueDate = effDate;
                                return c;
                            }).ToList();

                            // ---------------------------------
                            //    UPDATE #TempFinal SET
                            //    	DownPaymentAmt = 0
                            //    	,PolicyFee = 0
                            //    	,SR22Fee = 0
                            //    	,MVRFee = 0
                            //    	,MaintenanceFee = 0
                            //    	,PIPPDOnlyFee = 0
                            //    	,RewriteFee = 0
                            //    	,FeeTotal = 0
                            //    WHERE LEFT(InstallmentDesc, 11) LIKE 'Install%';
                            //
                            // Following code performs the above SQL

                            tempFinal.Table.Where(c =>
                                    c.InstallmentDesc.Length >= 11 &&
                                    c.InstallmentDesc.Substring(0, 11).StartsWith("Install"))
                                .ToList().Select(c =>
                                {
                                    c.DownPaymentAmt = 0;
                                    c.PolicyFee = 0;
                                    c.SR22Fee = 0;
                                    c.MVRFee = 0;
                                    c.MaintenanceFee = 0;
                                    c.PIPPDOnlyFee = 0;
                                    c.RewriteFee = 0;
                                    c.FeeTotal = 0;
                                    return c;
                                }).ToList();

                            // ---------------------------------
                            //    WITH IFee AS (
                            //    		SELECT InstallNo
                            //    			,(SELECT amount FROM AARateFees WHERE FeeDesc = 'Installment_Fee' AND RateCycle = @RateCycle) * (ISNULL(LAG(PremiumBalance,1) OVER (ORDER BY InstallNo),0)) AS InstallmentFee
                            //    		FROM #TempFinal
                            //    		WHERE PayPlan = @PayPlanID
                            //    	)
                            //    	UPDATE F SET
                            //    		InstallmentFee = ROUND(IFee.InstallmentFee,2)
                            //    		,InstallmentAmt = ROUND(IFee.InstallmentFee,2) + F.InstallmentAmt
                            //    	FROM #TempFinal AS F
                            //    		INNER JOIN IFee ON IFee.InstallNo = F.InstallNo
                            //    	WHERE PayPlan = @PayPlanID;
                            //
                            // Following code performs the above SQL

                            // Get embedded query's value
                            double? embedAmount;
                            using (var cmd = new SqlCommand(GetListProjectedInstallments.GetInstallmentFeeAmount, conn))
                            {
                                cmd.Parameters.Add(CreateParamVarchar("@RateCycle", rateCycle, 30));
                                embedAmount = GetDbValOrNull<double>(cmd.ExecuteScalar());
                            }

                            // Make temp version of TempFinal ordered by InstallNo
                            var sortedTempFinal = tempFinal.Table.Where(c => c.PayPlan == payPlanId)
                                .OrderBy(c => c.InstallNo).ToList();

                            // Create an IFee table to use as the result of the WITH clause
                            //var iFeeWith = new List<IFee>();
                            iFeeWith.Clear();

                            // Run through sorted table with for loop so can emulate LAG function
                            for (var i = 0; i < sortedTempFinal.Count; i++)
                            {
                                var recSorted = sortedTempFinal[i];
                                if (recSorted.PayPlan == payPlanId)
                                {
                                    iFeeWith.Add(new IFee
                                    {
                                        InstallNo = recSorted.InstallNo,
                                        InstallmentFee = (decimal?)embedAmount *
                                                         (i > 0 ? (sortedTempFinal[i - 1].PremiumBalance ?? 0M) : 0)
                                    });
                                }
                            }

                            //TempFinalRecord LagInstallNo(double? installNo)
                            //{
                            //    var sortedList = tempFinal.Table.OrderBy(c => c.InstallNo).ToList();
                            //    for (int i = 0; i < sortedList.Count; i++)
                            //    {
                            //        if (sorted)
                            //    }
                            //}

                            // Perform UPDATE joining IFee table with TempFinal table
                            //var temp2 = from tt in tempFinal
                            //    join ff in iFeeWith on ff.InstallNo == tt.InstallNo
                            //    select new { InstallmentFee = ff.InstallmentFee, InstallmentAmt = tt.InstallmentAmt };

                            var temp2 = tempFinal.Table.Where(c => c.PayPlan == payPlanId)
                                .Join(iFeeWith, ff => ff.InstallNo, ifee => ifee.InstallNo,
                                    (ff, ifee) => new
                                        { tempFinalRec = ff, iFeeWithRec = ifee }).ToList();
                            //{ InstallmentFee = ifee.InstallmentFee, InstallmentAmt = ff.InstallmentAmt }).ToList();

                            foreach (var rec in temp2)
                            {
                                rec.tempFinalRec.InstallmentFee = DecimalRoundNull(rec.iFeeWithRec.InstallmentFee, 2);
                                rec.tempFinalRec.InstallmentAmt = DecimalRoundNull(rec.iFeeWithRec.InstallmentFee, 2) +
                                                                  rec.tempFinalRec.InstallmentAmt;
                            }


                            // ---------------------------------
                            //   UPDATE #TempFinal SET
                            //       BalanceInstallment = Round(IsNull(DownPaymentAmt, 0) + IsNull(InstallmentAmt, 0), 2)
                            //   WHERE PayPlan = @PayPlanID;
                            //
                            // Following code performs the above SQL

                            tempFinal.Table.Where(c => c.PayPlan == payPlanId).ToList().Select(c =>
                            {
                                c.BalanceInstallment =
                                    decimal.Round((c.DownPaymentAmt ?? 0M) + (c.InstallmentAmt ?? 0M), 2);
                                return c;
                            }).ToList();

                            // ---------------------------------
                            // DECLARE @Diff FLOAT = ((SELECT SUM(BalanceInstallment) - SUM(InstallmentFee) FROM #TempFinal WHERE PayPlan = @PayPlanID) - @PolicyWritten) * -1;
                            //
                            // Following code performs the above SQL

                            var biSum = tempFinal.Table.Where(c => c.PayPlan == payPlanId).Sum(c => c.BalanceInstallment);
                            var ifSum = tempFinal.Table.Where(c => c.PayPlan == payPlanId).Sum(c => c.InstallmentFee);
                            var diff = (biSum - ifSum - policyWritten) * -1;

                            // ---------------------------------
                            //    IF (SELECT SUM(BalanceInstallment) - SUM(InstallmentFee) FROM #TempFinal) <> @PolicyWritten
                            //    BEGIN
                            //    	UPDATE #TempFinal SET
                            //    		BalanceInstallment = BalanceInstallment + @Diff
                            //    	WHERE InstallNo = (SELECT MAX(InstallNo) FROM #TempFinal WHERE PayPlan = @PayPlanID)
                            //    		AND PayPlan = @PayPlanID;
                            //    
                            //    	UPDATE #TempFinal SET
                            //    		InstallmentAmt = BalanceInstallment
                            //    	WHERE InstallmentAmt > 0
                            //    		AND PayPlan = @PayPlanID;
                            //    END;
                            //
                            // Following code performs the above SQL

                            if (tempFinal.Table.Sum(c => c.BalanceInstallment) -
                                tempFinal.Table.Sum(c => c.InstallmentFee) != policyWritten)
                            {
                                var maxInstallNo = tempFinal.Table.Where(e => e.PayPlan == payPlanId).Max(e => e.InstallNo);

                                tempFinal.Table.Where(c => DoublesWithinEpsilon(c.InstallNo, maxInstallNo) && c.PayPlan == payPlanId).Select(
                                    d =>
                                    {
                                        d.BalanceInstallment += diff;
                                        return d;
                                    }).ToList();


                                tempFinal.Table.Where(c =>
                                    c.PayPlan != null && c.InstallmentAmt != null && c.InstallmentAmt > 0M &&
                                    c.PayPlan == payPlanId).Select(c =>
                                {
                                    c.InstallmentAmt = c.BalanceInstallment;
                                    return c;
                                }).ToList();
                            }

                            currCount++;

                        } // while currCount <= maxCount

                        if (guid is null)
                        {
                            // set up for a set of selects returned
                            returnSelects.Add(tempFinal.Table
                                .Where(c => 1 == (payPlan == 0 || c.PayPlan == payPlan ? 1 : 0))
                                .OrderBy(c => c.PayPlan).ThenBy(c => c.InstallNo).Select(c => c).ToList());

                        }
                        else
                        {
                            int ratingCount;
                            using (var cmd = new SqlCommand(Queries.RatingInstallments_CountByMainIndexId, conn))
                            {
                                cmd.Parameters.Add(CreateParamInt("@RatingID", ratingId));
                                ratingCount = (int)cmd.ExecuteScalar();
                            }

                            if (ratingCount > 0)
                            {
                                using (var cmd = new SqlCommand(Queries.RatingInstallments_DeleteByMainIndexId, conn))
                                {
                                    cmd.Parameters.Add(CreateParamInt("@RatingID", ratingId));
                                    cmd.ExecuteNonQuery();
                                }
                            }

                            var tempFinalList = tempFinal.Table
                                .Where(c => 1 == (payPlan == 0 || c.PayPlan == payPlan ? 1 : 0))
                                .OrderBy(c => c.PayPlan).ThenBy(c => c.InstallNo).Select(c => c).ToList();

                            foreach (var rec in tempFinalList)
                            {
                                using (var cmd = new SqlCommand(GetListProjectedInstallments.InsertIntoRatingInstallments,
                                           conn))
                                {
                                    cmd.Parameters.Add(CreateParamUniqueIdentifier("@GUID", guid));
                                    cmd.Parameters.Add(CreateParamInt("@RatingID", ratingId));
                                    cmd.Parameters.Add(CreateParamDecimal("@InstallNo", (decimal?)rec.InstallNo, 5, 1));
                                    cmd.Parameters.Add(CreateParamVarchar("@InstallmentDesc", rec.InstallmentDesc, 40));
                                    cmd.Parameters.Add(CreateParamInt("@PayPlan", rec.PayPlan));
                                    cmd.Parameters.Add(CreateParamDateTime("@DueDate", rec.DueDate));
                                    cmd.Parameters.Add(CreateParamMoney("@PolicyWritten", rec.PolicyWritten));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@InstallmentFee", rec.InstallmentFee));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@PolicyFee", rec.PolicyFee));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@SR22Fee", rec.SR22Fee));
                                    cmd.Parameters.Add(CreateParamInt("@MVRFee", (int?)rec.MVRFee));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@LateFee", rec.LateFee));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@MaintenanceFee", rec.MaintenanceFee));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@PIPPDOnlyFee", rec.PIPPDOnlyFee));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@RewriteFee", rec.RewriteFee));
                                    cmd.Parameters.Add(CreateParamMoney("@FeeTotal", rec.FeeTotal));
                                    cmd.Parameters.Add(CreateParamDecimal("@DownPaymentPct", rec.DownPaymentPct));
                                    cmd.Parameters.Add(CreateParamMoney("@DownPaymentAmt", rec.DownPaymentAmt));
                                    cmd.Parameters.Add(CreateParamSmallMoney("@InstallmentAmt", rec.InstallmentAmt));
                                    cmd.Parameters.Add(CreateParamMoney("@PremiumBalance", rec.PremiumBalance));
                                    cmd.Parameters.Add(CreateParamMoney("@PymtApplied", rec.PymtApplied));
                                    cmd.Parameters.Add(CreateParamMoney("@BalanceInstallment", rec.BalanceInstallment));
                                    cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid?.ToString() ?? string.Empty, policyNo, string.Empty, DateTime.Now, "GetListProjectedInstallments",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }

            return returnSelects;
        }

        /// <summary>
        /// Get the total amount to draft during the renewal process cycle
        /// including any past due amounts
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// GetRenewalDraftAmount.
        /// 
        /// Migration of stored procedure spUW_GetRenewalDraftAmount
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public decimal TxGetRenewalDraftAmount(string policyNo, string userName, string guid)
        {
            var errorSection = "";
            if (string.IsNullOrEmpty(userName))
            {
                userName = Environment.UserName;
            }

            List<object> renewalDraftAmountCalc;

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        renewalDraftAmountCalc = ExecuteQueryWithPolicyNo(GetRenewalDraftAmount.Long, policyNo, conn);

                        var obj = ExecuteScalarWithPolicyNo(Queries.AuditRenewalDraftAmountCalc_CountByPolicyNo,
                            policyNo, conn);
                        var nrecs = (int)obj;
                        if (nrecs > 0)
                        {
                            ExecuteNonQueryWithPolicyNo(Queries.AuditRenewalDraftAmountCalc_DeleteByPolicyNo, policyNo,
                                conn);
                        }

                        foreach (var rec in renewalDraftAmountCalc)
                        {
                            using (var cmd = new SqlCommand(GetRenewalDraftAmount.Insert,conn))
                            {
                                var values = (object[])rec;
                                string recPolicyNo = (string)values[0];
                                decimal priorTotalPaid = (decimal)values[1],
                                    pendingAchAmount = (decimal)values[2],
                                    priorBalanceOwed = (decimal)values[3],
                                    priorBalance = (decimal)values[4],
                                    renewalDepositAmount = (decimal)values[5],
                                    amountToDraft = (decimal)values[6];
                            
                                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", recPolicyNo, 25));
                                cmd.Parameters.Add(CreateParamMoney("@PriorTotalPaid", priorTotalPaid));
                                cmd.Parameters.Add(CreateParamMoney("@PendingACHAmount", pendingAchAmount));
                                cmd.Parameters.Add(CreateParamMoney("@PriorBalanceOwed", priorBalanceOwed));
                                cmd.Parameters.Add(CreateParamMoney("@PriorBalance", priorBalance));
                                cmd.Parameters.Add(CreateParamMoney("@RenewalDepositAmount", renewalDepositAmount));
                                cmd.Parameters.Add(CreateParamMoney("@AmountToDraft", amountToDraft));
                            }
                        }

                    }

                    txScope.Complete();
                }

                return decimal.Round((decimal)((object [])renewalDraftAmountCalc.First())[6],2);  //  gets AmountToDraft from the first record
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "GetRenewalDraftAmount",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        public double? TxGetTotalDueOnPolicy(string policyNo, string userName, string guidErrorLog)
        {
            var errorSection = "-1: Declare and init.";
            if (string.IsNullOrEmpty(userName))
            {
                userName = Environment.UserName;
            }
            var guid = guidErrorLog ?? string.Empty;

            try
            {
                using (var conn = GetConnection())
                {
                    conn.Open();

                    var amtDue = GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(StoredProcedureQueries.GetTotalDueOnPolicy.GetTotalDue, policyNo, conn));
                    return (double?)DecimalRoundNull((decimal?)amtDue, 2);
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "GetTotalDueOnPolicy", ex.HResult.ToString(),
                    $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Applies open payments to Installments
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// InstallmentPaymentApply.
        /// 
        /// Migration of stored procedure  spUW_InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="postmarkDate"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxInstallmentPaymentApply(string policyNo, DateTime? postmarkDate, string userName, string guid)
        {
            if (string.IsNullOrEmpty(policyNo)) throw new ArgumentException("Policy # is null or empty.");

            var errorSection = "1: Startup";
            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Insert new Credit Card fees. ";

                        var numPaymentRecs = ConvertFromDbVal(ExecuteScalarWithPolicyNo(InstallmentPaymentApply.CountOfINSPaymentRecordsByPolicyNo, policyNo, conn), 0);
                        if (numPaymentRecs == 0)
                            throw new StoredProcedureException($"No Policy records for policy # {policyNo}");

                        var numInstallmentRecs =
                            ConvertFromDbVal(ExecuteScalarWithPolicyNo(Queries.Installments_CountByPolicyNo, policyNo, conn), 0);
                        if (numInstallmentRecs == 0)
                            throw new StoredProcedureException($"No Installment records for policy # {policyNo}");

                        if (numPaymentRecs > numInstallmentRecs)
                        {
                            IpaInsertNewCreditCardFees(policyNo, userName, conn);
                        }

                        // ------------------------------------------------------------------
                        errorSection = "2:Find open payments from Payments table.";

                        var pymtAmtRemaining =
                            (decimal)ConvertFromDbVal(ExecuteScalarWithPolicyNo(InstallmentPaymentApply.FindOpenPayments1, policyNo, conn), 0D);
                        var negInstallAmt =
                            ConvertFromDbVal(ExecuteScalarWithPolicyNo(InstallmentPaymentApply.GetNegativeInstallmentAmounts, policyNo, conn),
                                0M);

                        pymtAmtRemaining -= negInstallAmt;

                        // ------------------------------------------------------------------
                        errorSection = "5:Clear out Pymt Applied. Recalculate BalanceInstallment. Apply total.";

                        ExecuteNonQueryWithPolicyNo(InstallmentPaymentApply.UpdateInstallmentPymtApplied,
                            policyNo, conn);

                        ExecuteNonQueryWithPolicyNo(InstallmentPaymentApply.SubtractInstallmentFee, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "6: Open InstallCursor";

                        var recList = ExecuteQueryWithPolicyNo(InstallmentPaymentApply.Cursor, policyNo, conn);

                        if (recList.Count > 0)
                        {
                            foreach (var rec in recList)
                            {
                                var values = (object[])rec;
                                var indexId = (int)values[0];
                                var balanceInstallment = (decimal)values[7];

                                if (pymtAmtRemaining != 0M || balanceInstallment < 0)
                                {
                                    // ------------------------------------------------------------------
                                    errorSection = "7:Apply the payment.";

                                    if (balanceInstallment < 0M)
                                    {
                                        var amtToApply = balanceInstallment;
                                        negInstallAmt = (balanceInstallment > negInstallAmt
                                            ? amtToApply
                                            : (balanceInstallment < negInstallAmt
                                                ? negInstallAmt - amtToApply
                                                : 0M));

                                        IpaUpdateBalanceInstallment(policyNo, conn, indexId, amtToApply);

                                        pymtAmtRemaining += amtToApply;
                                    }
                                    else
                                    {
                                        if (pymtAmtRemaining - balanceInstallment >= 0M)
                                        {
                                            pymtAmtRemaining -= balanceInstallment;

                                            IpaUpdateLastPayAmt(policyNo, postmarkDate, userName, conn, indexId,
                                                balanceInstallment);
                                        }
                                        else
                                        {   // Total payments are less than line item balance; partial payment of installment

                                            // -----------------------------------------------------
                                            errorSection = "8:partial payment of installment";

                                            IpaUpdatePartialPymtApplied(policyNo, postmarkDate, userName, conn, indexId,
                                                pymtAmtRemaining);

                                            pymtAmtRemaining = 0M;
                                        }
                                    }

                                    // ------------------------------------------------------------------
                                    errorSection = "9: update BalanceInstallment.";

                                    IpaUpdateBalanceInstallmentFinal(policyNo, conn, indexId);
                                }
                            }
                        }

                        // ------------------------------------------------------------------
                        errorSection = "14: update BalanceInstallment.";

                        ExecuteNonQueryWithPolicyNo(
                            InstallmentPaymentApply.UpdateInstallmentBalanceInstallment2, policyNo, conn);

                        ExecuteNonQueryWithPolicyNo(InstallmentPaymentApply.UpdateInstallmentLocked,
                            policyNo, conn);
                    }

                    txScope.Complete();

                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "InstallmentPaymentApply",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }

        }

        public int? TxInstallNoCurrent(string policyNo, DateTime mailDate)
        {
            using (var conn = GetConnection())
            {
                // 1) Determine all due installments using formula: where DueDate <= MailDate + 20                                 
                // 2) Determine last Due installment. 

                // get total mail days
                var totalMailDays = GetDbValOrNull<int>(ExecuteScalarNoArgs(InstallNoCurrent.GetMailDays, conn)) ?? 0;

                // get install no
                var installNo = GetDbValOrNull<int>(InTxInstallNoCurrentGetInstallNoByMailDate(policyNo, mailDate, totalMailDays, conn)) ?? -1;

                // IF  @InstallNo = 0, just get last unpaid installment.This would apply when @MailDate is well after DueDate of last installment. XXXX
                // IF @InstallNo = 0, set install number to highest locked record or leave as 0

                if (installNo == 0)
                {
                    installNo = GetDbValOrNull<int>(ExecuteScalarWithPolicyNo(InstallNoCurrent.GetMaxLockedInstallNo, policyNo, conn)) ?? 0;
                }

                return installNo;
            }
            
        }

        public bool TxIsHoliday(DateTime dayToValidate)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                var cnt = (int)ExecuteScalarWithParameters(Queries.Holidays_CountByDate, new[]
                {
                    CreateParamDateTime("@DayToValidate", dayToValidate)
                }, conn);

                return cnt > 0;
            }
        }

        [SuppressMessage("ReSharper", "UnusedLocalFunctionReturnValue")]
        [SuppressMessage("ReSharper", "ReturnValueOfPureMethodIsNotUsed")]
        public void TxIssueCancellation(string policyNo, DateTime? cancellationDate, bool flatCancel, bool shortRate,
            DateTime? nullableAcctDate = null, string userName = "", string guidErrorLog = "",
            string guidHistoryIndexId = "")
        {
            var acctDate = nullableAcctDate ?? DateTime.Now;
            var cancellationId = 0;
            int ignoreId;
            int historyId;
            TempTable<TempAuditCoverageData> tempAuditCoverageData;
            DateTime? effDate;

            var policyValid = true;
            var errorSection = "0: Startup";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {

                        // --------- local methods ---------------------------------

                        void LocalLogError(string errorDescr) => LogErrorWithConn(guidErrorLog, policyNo, userName, DateTime.Now,
                            "IssueCancellation", string.Empty, errorDescr,conn);

                        TempTable<TempPreSetWritten> MakePreSetWrittenTable()
                        {
                            var preSetWritten = new TempTable<TempPreSetWritten>();
                            var preSetWrittenList = ExecuteQueryWithParameters(IssueCancellation.PreSetWritten,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamInt("@IgnoreID", ignoreId)
                                }, conn);
                            foreach (var rec in preSetWrittenList)
                            {
                                var values = (object[])rec;
                                preSetWritten.AddRecord(new TempPreSetWritten
                                {
                                    IndexID = GetDbValOrNull<int>(values[0]),
                                    Coverage = GetDbRefValOrNull<string>(values[1]),
                                    VehIndex = GetDbValOrNull<int>(values[2]),
                                    PolicyNo = GetDbRefValOrNull<string>(values[3]),
                                    Written = GetDbValOrNull<double>(values[4])
                                });
                            }

                            return preSetWritten;
                        }

                        TempTable<TempPreNegateUpdate> MakePreNegateUpdateTable()
                        {
                            var preNegateUpdate = new TempTable<TempPreNegateUpdate>();
                            var preNegateUpdateList = ExecuteQueryWithParameters(IssueCancellation.PreNegateUpdate, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 99),
                                CreateParamInt("@IgnoreID", ignoreId)
                            }, conn);
                            foreach (var rec in preNegateUpdateList)
                            {
                                var values = (object[])rec;
                                preNegateUpdate.AddRecord(new TempPreNegateUpdate
                                {
                                    PolicyNo = GetDbRefValOrNull<string>(values[0]),
                                    IsFee = GetDbValOrNull<bool>(values[1]),
                                    EffDate = GetDbValOrNull<DateTime>(values[2]),
                                    EarnStop = GetDbValOrNull<DateTime>(values[3]),
                                    Negate = GetDbValOrNull<bool>(values[4]),
                                    IgnoreRecord = GetDbValOrNull<bool>(values[5]),
                                    Coverage = GetDbRefValOrNull<string>(values[6]),
                                    VehIndex = GetDbValOrNull<int>(values[7]),
                                    TermIndexID = GetDbValOrNull<int>(values[8]),
                                    IndexID = GetDbValOrNull<int>(values[9]),
                                });
                            }

                            return preNegateUpdate;
                        }

                        TempTable<TempJoin> MakePreJoinTable()
                        {
                            var prejoin = new TempTable<TempJoin>();
                            var prejoinList = GetPrejoinList();
                            foreach (var rec in prejoinList)
                            {
                                var values = (object[])rec;
                                prejoin.AddRecord(new TempJoin
                                {
                                    TermIndexID = (int)values[0],
                                    Coverage = GetDbRefValOrNull<string>(values[1]),
                                    VehIndex = GetDbValOrNull<int>(values[2]),
                                    Amount = GetDbValOrNull<double>(values[3]),
                                    Negate = values[4] is null ? (bool?)null : GetDbValOrNull<int>(values[4]) != 0,
                                    TermDesc = GetDbRefValOrNull<string>(values[5]),
                                    Annualized = GetDbValOrNull<int>(values[6]),
                                    EffDate = GetDbValOrNull<DateTime>(values[7]),
                                    EarnStop = GetDbValOrNull<DateTime>(values[8])
                                });
                            }

                            return prejoin;
                        }

                        List<object> GetPrejoinList()
                        {
                            return ExecuteQueryWithParameters(IssueCancellation.InsertIntoDaysPreJoin, new []
                            {
                                CreateParamVarchar("@PolicyNo",policyNo,99),
                                CreateParamInt("@IgnoreID",ignoreId)
                            } , conn);
                        }

                        int InsertIntoPolicyPrintSuspense()
                        {
                            return ExecuteNonQueryWithParameters(IssueCancellation.InsertIntoPolicyPrintSuspense, new []
                            {
                                CreateParamVarchar("@PolicyNo",policyNo,99),
                                CreateParamInt("@HistoryID",historyId),
                                CreateParamVarchar("@UserName",userName,99)
                            }, conn);
                        }

                        object UpdateAuditPolicyHistoryReasons()
                        {
                            return ExecuteScalarWithPolicyNo(IssueCancellation.UpdateAuditPolicyHistoryReasons, policyNo,
                                conn);
                        }

                        int GetCountHistoryCancels()
                        {
                            return (int)ExecuteScalarWithPolicyNo(IssueCancellation.GetCountHistoryCancels, policyNo, conn);
                        }

                        int GetInstallNo() =>
                            ConvertFromDbVal(ExecuteScalarWithParameters(IssueCancellation.GetInstallNo,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamDateTime("@AcctDate", acctDate)
                                }, conn), 0) + 1;

                        decimal GetBilledAmount() =>
                            ConvertFromDbVal(ExecuteScalarWithParameters(IssueCancellation.GetBilledAmount,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamDateTime("@AcctDate", acctDate)
                                }, conn), 0M);

                        int UpdateInstallments() =>
                            ExecuteNonQueryWithParameters(IssueCancellation.UpdateInstallments, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 99),
                                CreateParamInt("@HistoryID", historyId),
                                CreateParamDateTime("CancellationDate", cancellationDate)
                            }, conn);

                        int UpdatePolicyEndorseQuoteAdndCost() =>
                            ExecuteNonQueryWithPolicyNoPlusInt(IssueCancellation.UpdatePolicyEndorseQuote_ADNDCost, policyNo,
                                "@IgnoreID", ignoreId,
                                conn);

                        int UpdatePolicyEndorseQuoteLouCost() =>
                            ExecuteNonQueryWithPolicyNoPlusInt(IssueCancellation.UpdatePolicyEndorseQuote_LOUCost, policyNo,
                                "@IgnoreID", ignoreId,
                                conn);

                        int UpdatePolicyEndorseQuoteCarsCoverageWritten(string coverage, int currCar) =>
                            ExecuteNonQueryWithParameters(IssueCancellation.Update_PolicyEndorseQuoteCars_Written,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99), CreateParamInt("@IgnoreID", ignoreId),
                                    CreateParamVarchar("@Coverage", coverage, 99), CreateParamInt("@currCar", currCar)
                                }, conn);

                        int GetCountPolicyQuoteCars() =>
                            (int)ExecuteScalarWithPolicyNo(IssueCancellation.GetCountPolicyEndorseQuoteCars,
                                policyNo, conn);

                        int UpdatePolicyFutureActionsCancel() =>
                            ExecuteNonQueryWithPolicyNo(IssueCancellation.UpdatePolicyFutureActionsCancel, policyNo, conn);

                        int UpdateAuditPolicyHistorySetNoticeInactive() =>
                            ExecuteNonQueryWithPolicyNo(IssueCancellation.UpdateAuditPolicyHistoryNoticeInactive, policyNo,
                                conn);

                        int GetPolicyCancelCount() =>
                            (int)ExecuteScalarWithPolicyNo(IssueCancellation.GetPolicyCancelCount, policyNo, conn);

                        void GenAddlRecordsReverseFeeChange(string feeName, double fee, double currFee)
                        {
                            tempAuditCoverageData.AddRecords(tempAuditCoverageData.Table.Select(c =>
                                new TempAuditCoverageData
                                {
                                    AcctDate = acctDate,
                                    Action = "CANCEL",
                                    ActionID = cancellationId,
                                    Annualized = (int?)(fee - currFee),
                                    ChangeDateEnd = false,
                                    Company = tempAuditCoverageData.Table.Max(d => d.Company),
                                    Coverage = feeName,
                                    EarnNoDays = 1,
                                    EarnStart = cancellationDate,
                                    EarnStop = cancellationDate,
                                    EarnedSum = fee - currFee,
                                    EffDate = effDate,
                                    ExpDate = cancellationDate,
                                    IgnoreIndexID = 0,
                                    IgnoreRecord = false,
                                    TempTableID = 0,
                                    IsFee = true,
                                    LastDayEarn = 0D,
                                    Negate = false,
                                    LOB = tempAuditCoverageData.Table.Max(d => d.LOB),
                                    OneDayEarn = fee - currFee,
                                    OrigAction = "NEW",
                                    PolicyNo = c.PolicyNo,
                                    ProRata = 1D,
                                    State = tempAuditCoverageData.Table.Max(d => d.State),
                                    TermIndexID = cancellationId,
                                    VehIndex = 0,
                                    Written = fee - currFee
                                }).ToList());
                        }

                        double GetSumWrittenByFeeName(string feeName) =>
                            ConvertFromDbVal(ExecuteScalarWithParameters(IssueCancellation.GetSumWritten_Fee, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 30),
                                CreateParamInt("@IgnoreID", ignoreId),
                                CreateParamVarchar("@FeeName", feeName, 20)
                            }, conn), 0D);

                        double GetFHCFFeeTimesSum(double? writtenSum, string rc) => DoubleRound(
                            writtenSum * GetDbValOrNull<double>(ExecuteScalarWithParameters(IssueCancellation.GetAmountFHCFFee,
                                new[] { CreateParamVarchar("@RateCycle", rc, 99) }, conn)), 2) ?? 0;

                        double? GetSumWrittenFromDb() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNoPlusInt(IssueCancellation.GetSumWritten, policyNo,
                                "@IgnoreID", ignoreId, conn));

                        DateTime? GetMaxEarnStop() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithParameters(IssueCancellation.GetMaxEarnStop,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 20),
                                    CreateParamInt("@IgnoreID", ignoreId)
                                }, conn));

                        DateTime? GetEffDateForPolicy() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(IssueCancellation.GetEffDateForPolicy, policyNo,
                                conn));

                        DateTime? GetMinEffDate() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithParameters(IssueCancellation.GetMinEffDate,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 20),
                                    CreateParamInt("@IgnoreID", ignoreId)
                                }, conn));

                        string GetRateCycleForPolicy() =>
                            GetDbRefValOrNull<string>(ExecuteScalarWithPolicyNo(IssueCancellation.GetRateCycleForPolicy,
                                policyNo, conn)) ?? "AA20190101";

                        double GetNewBaseCommission() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(IssueCancellation.GetNewBaseCommission, policyNo,
                                conn)) ?? 0D;

                        double GetNewTotalFees() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(IssueCancellation.GetNewTotalFees, policyNo,
                                conn)) ?? 0D;

                        double GetNewWritten() =>
                            GetDbValOrNull<double>(ExecuteScalarWithPolicyNo(IssueCancellation.GetNewWritten, policyNo,
                                conn)) ?? 0D;

                        int GetPolicyCountOid() =>
                            (int)ExecuteScalarWithPolicyNo(Queries.Policy_CountOIDByPolicyNo, policyNo, conn);

                        int GetPolicyBinderCancelStatus() =>
                            (int)ExecuteScalarWithPolicyNo(IssueCancellation.GetPolicyBinderCancelStatus, policyNo, conn);

                        DateTime? GetTop1EffDate() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(Queries.Policy_Top1EffDateByPolicyNo, policyNo,
                                conn));

                        DateTime? GetTop1ExpDate() =>
                            GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(Queries.Policy_Top1ExpDateByPolicyNo, policyNo,
                                conn));

                        int GetMaxId() =>
                            GetDbValOrNull<int>(ExecuteScalarWithPolicyNo(IssueCancellation.GetMaxIndexID, policyNo,
                                conn)) ?? 0;

                        // ---- end local methods -------------------------------

                        conn.Open();
                        effDate = GetEffDateForPolicy();
                        //var isAnnual = GetIsAnnualForPolicy();
                        var rateCycle = GetRateCycleForPolicy();
                        var origBaseCommission = GetNewBaseCommission();
                        //var origOtherComm = GetNewOtherComm();
                        var origTotalFees = GetNewTotalFees();
                        var origWritten = GetNewWritten();

                        userName = userName ?? Environment.UserName;

                        if (cancellationDate != null && (((DateTime)cancellationDate).Month > acctDate.Month &&
                                                         ((DateTime)cancellationDate).Year >= acctDate.Year))
                        {
                            acctDate = (DateTime)cancellationDate;
                        }

                        // ---- add error if policy if policy passed does not exists ----------------------------------------- 

                        if (GetPolicyCountOid() == 0)
                        {
                            policyValid = false;
                            LocalLogError($"Policy \"{policyNo}\" does not exist.");
                        }

                        // ---- Add error if policy is not an allowed PolicyStatus. -----------------------------------------
                        // --Allowed statuses: ACTIVE, NOTICE.
                        // -- Disallowed statuses:  BINDER, CANCEL.

                        if (GetPolicyBinderCancelStatus() > 0)
                        {
                            policyValid = false;
                            LocalLogError($"Policy Status {policyNo} must be Active.");
                        }

                        // ---- Add error if @EffDate_History is before Policy EffDate, or after Policy ExpDate. -------------

                        var policyEffDate = GetTop1EffDate();
                        var policyExpDate = GetTop1ExpDate();

                        if (cancellationDate < policyEffDate || cancellationDate > policyExpDate)
                        {
                            policyValid = false;
                            LocalLogError(
                                "Amend Effective Date cannot be before Policy Effective Date, or after Policy Expiration Date.");
                        }

                        if (policyValid)
                        {
                            // (comment from original stored proc)
                            // *********************************************************************************************
                            // Begin new logic - Set up days to determine what negate records need to be created. Allows
                            // for OOS cancellations. Sets up the negate records then sets the Written to pro-rata calc
                            // so that return can be justified using standard business practices. Once new logic is complete
                            // call old logic to put all of the data into the endorsement quote tables and call all standard
                            // calls to generate audit records.
                            // *********************************************************************************************

                            if (flatCancel)
                            {
                                shortRate = false;
                            }

                            ignoreId = GetMaxId();

                            // make temp tables corresponding to #Days and #AllDays
                            var days = new TempTable<TempDays>();
                            var allDays = new TempTable<TempAllDays>();

                            var startDate = GetMinEffDate();
                            var stopDate = GetMaxEarnStop();

                            if (startDate != null)
                            {
                                // ReSharper disable once PossibleInvalidOperationException
                                DateTime runningDate = (DateTime)startDate;

                                while (runningDate <= stopDate)
                                {
                                    allDays.AddRecord(new TempAllDays{TheDay = runningDate});
                                    runningDate = runningDate.AddDays(1);
                                }
                            }

                            // ----------------------------------------------------
                            // Insert Non-Negate Records

                            // Get data to join with allDays

                            var prejoin = MakePreJoinTable();

                            days.AddRecords(
                                (from pj in prejoin.Table
                                    from ad in allDays.Table
                                    where (ad.TheDay >= pj.EffDate && ad.TheDay <= pj.EarnStop)
                                    select new TempDays
                                    {
                                        TermIndexID = pj.TermIndexID,
                                        Coverage = pj.Coverage,
                                        VehIndex = pj.VehIndex,
                                        TheDay = ad.TheDay,
                                        Amount = pj.Amount,
                                        Negated = pj.Negate,
                                        TermDesc = pj.TermDesc,
                                        Annualized = pj.Annualized
                                    }).ToList());

                            // ----------------------------------------------------
                            // --Update Negated Records

                            var preNegateUpdate = MakePreNegateUpdateTable();

                            // Update using table

                            days.Table.Where(d => preNegateUpdate.Table
                                    .Any(pn => string.Equals(pn.Coverage, d.Coverage) && pn.VehIndex == d.VehIndex &&
                                               pn.TermIndexID == d.TermIndexID && d.TheDay >= pn.EffDate && d.TheDay <= pn.EarnStop))
                                .Select(
                                    c =>
                                    {
                                        c.Negated = true;
                                        return c;
                                    }).ToList();

                            // ----------------------------------------------------
                            // Create negate records

                            tempAuditCoverageData = new TempTable<TempAuditCoverageData>(days.Table
                                .Where(c => c.TheDay >= cancellationDate && (c.Negated != null && !(bool)c.Negated))
                                .GroupBy(d => new
                                {
                                    d.TermIndexID,
                                    d.Coverage,
                                    d.VehIndex
                                }).Select(e => new TempAuditCoverageData()
                                {
                                    PolicyNo = policyNo,
                                    VehIndex = e.Key.VehIndex,
                                    Company = 1,
                                    State = "FL",
                                    LOB = 1,
                                    ActionID = cancellationId,
                                    TermIndexID = e.Key.TermIndexID,
                                    Coverage = e.Key.Coverage,
                                    IsFee = false,
                                    Negate = true,
                                    IgnoreRecord = false,
                                    IgnoreIndexID = 0,
                                    Action = "CANCEL",
                                    OrigAction = e.Max(r => r.TermDesc),
                                    AcctDate = acctDate,
                                    EffDate = cancellationDate,
                                    ExpDate = cancellationDate,
                                    ProRata = 0D,
                                    EarnStart = e.Min(r => r.TheDay),
                                    EarnStop = e.Max(r => r.TheDay),
                                    EarnNoDays = 0,
                                    Annualized = (int?)e.Max(r => r.Annualized),
                                    Written = DoubleRound(e.Sum(r => r.Amount), 0) * (-1D),
                                    OneDayEarn = e.Max(r => r.Amount) * (-1D),
                                    LastDayEarn = 0D,
                                    EarnedSum = DoubleRound(e.Sum(r => r.Amount), 0) * (-1D),
                                    ChangeDateEnd = false
                                }).ToList());


                            tempAuditCoverageData.Table.Select(c =>
                            {
                                c.EarnNoDays = DateDiffDaysNull(c.EarnStart, c.EarnStop) ?? 0;
                                c.LastDayEarn = DoubleRound(c.Written - c.OneDayEarn * (c.EarnNoDays - 1), 3);
                                c.ProRata = Math.Round((DateDiffDaysNull(c.EarnStart, c.EarnStop) ?? 0) / 365D, 3,
                                    MidpointRounding.AwayFromZero);
                                return c;
                            }).ToList();


                            // Set Written as ProRata of Annualized

                            if (tempAuditCoverageData.Table.Max(c => c.EarnNoDays) > 1)
                            {
                                tempAuditCoverageData.Table.Select(c =>
                                {
                                    c.Written = DoubleRound(c.ProRata * c.Annualized * (-1D), 0);
                                    return c;
                                }).ToList();


                                if (flatCancel)
                                {
                                    var preSetWritten = MakePreSetWrittenTable();

                                    tempAuditCoverageData.Table
                                        .Where(c => c.TempTableID ==
                                                    tempAuditCoverageData.Table
                                                        .Where(a => string.Equals(a.Coverage, c.Coverage) &&
                                                                    a.VehIndex == c.VehIndex)
                                                        .Max(e => e.TempTableID))
                                        .Select(f =>
                                        {
                                            var part1 = (
                                                from psw in preSetWritten.Table
                                                from k in tempAuditCoverageData.Table
                                                where string.Equals(psw.Coverage, f.Coverage) &&
                                                      psw.VehIndex == f.VehIndex && string.Equals(psw.PolicyNo, policyNo) &&
                                                      psw.IndexID == ignoreId
                                                select psw.Written
                                            ).Sum();
                                            var part2 = tempAuditCoverageData.Table
                                                .Where(g =>
                                                    string.Equals(g.Coverage, f.Coverage) && g.VehIndex == f.VehIndex &&
                                                    string.Equals(g.PolicyNo, policyNo))
                                                .Sum(h => h.Written);
                                            f.Written -= (part1 ?? 0D) + (part2 ?? 0D);
                                            return f;
                                        }).ToList();

                                } // Set Written to the reverse of all prior written premium

                                tempAuditCoverageData.Table.Select(c =>
                                {
                                    c.LastDayEarn = c.Written - (c.EarnNoDays - 1) * c.OneDayEarn;
                                    c.EarnedSum = (c.EarnNoDays - 1) * c.OneDayEarn + c.LastDayEarn;
                                    return c;
                                }).ToList();
                            }

                            // Here would be a section populating a temp table #tmpWritten,
                            // but the table is then not used anywhere so the section is omitted

                            var writtenSum = tempAuditCoverageData.Table.Sum(c => c.Written) + GetSumWrittenFromDb();

                            // section setting isOos omitted since the variable is not used anywhere

                            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                            if (shortRate && writtenSum > 10D && !flatCancel)
                            {
                                // Generate additional records to capture 10% of return by Vehicle, coverage with 2 days of earned

                                tempAuditCoverageData.AddRecords(tempAuditCoverageData.Table.Select(c =>
                                    new TempAuditCoverageData
                                    {
                                        AcctDate = acctDate,
                                        Action = c.Action,
                                        ActionID = cancellationId,
                                        Annualized = c.Annualized,
                                        ChangeDateEnd = false,
                                        Company = c.Company,
                                        Coverage = c.Coverage,
                                        EarnNoDays = 1,
                                        EarnStart = cancellationDate,
                                        EarnStop = cancellationDate,
                                        EarnedSum = DoubleRound(c.Written * (-1D) * 0.1D, 0),
                                        EffDate = c.EffDate,
                                        ExpDate = c.ExpDate,
                                        IgnoreIndexID = 0,
                                        IgnoreRecord = false,
                                        IsFee = c.IsFee,
                                        LastDayEarn = 0D,
                                        Negate = false,
                                        LOB = c.LOB ?? 1,
                                        OneDayEarn = DoubleRound(c.Written * (-1D) * 0.1D, 0),
                                        OrigAction = c.OrigAction,
                                        PolicyNo = c.PolicyNo,
                                        ProRata = 1D,
                                        State = c.State ?? "FL",
                                        TermIndexID = c.TermIndexID,
                                        VehIndex = c.VehIndex,
                                        Written = DoubleRound(c.Written * (-1D) * 0.1D, 0)
                                    }).ToList());
                            }

                            writtenSum = tempAuditCoverageData.Table.Sum(c => c.Written) + GetSumWrittenFromDb();

                            var fhcfFee = GetFHCFFeeTimesSum(writtenSum, rateCycle);
                            var sr22Fee = GetSumWrittenByFeeName("SR22Fee");
                            var dbSetupFee = GetSumWrittenByFeeName("DBSetupFee");
                            var mvrFee = GetSumWrittenByFeeName("MVRFee");
                            var policyFee = GetSumWrittenByFeeName("PolicyFee");
                            var maintenanceFee = GetSumWrittenByFeeName("MaintenanceFee");
                            var pippdOnlyFee = GetSumWrittenByFeeName("PIPPDOnlyFee");
                            var rewriteFee = GetSumWrittenByFeeName("RewriteFee");

                            var currFhcfFee = GetSumWrittenByFeeName("FHCFFee");
                            var currSr22Fee = sr22Fee;
                            var currDbSetupFee = dbSetupFee;
                            var currMvrFee = mvrFee;
                            var currPolicyFee = policyFee;
                            var currMaintenanceFee = maintenanceFee;
                            var currPippdOnlyFee = pippdOnlyFee;
                            var currRewriteFee = rewriteFee;

                            if (flatCancel)
                            {
                                fhcfFee = 0D;
                                sr22Fee = 0D;
                                dbSetupFee = 0D;
                                mvrFee = 0D;
                                policyFee = 0D;
                                maintenanceFee = 0D;
                                pippdOnlyFee = 0D;
                                rewriteFee = 0D;
                            }

                            // Insert records to account for any changes in fees

                            // Generate additional records reverse change in fee amount

                            if (!DoublesWithinEpsilon(fhcfFee, currFhcfFee))
                                GenAddlRecordsReverseFeeChange("FHCFFee", fhcfFee, currFhcfFee);

                            if (!DoublesWithinEpsilon(sr22Fee, currSr22Fee))
                                GenAddlRecordsReverseFeeChange("SR22Fee", sr22Fee, currSr22Fee);

                            if (!DoublesWithinEpsilon(dbSetupFee, currDbSetupFee))
                                GenAddlRecordsReverseFeeChange("DBSetupFee", dbSetupFee, currDbSetupFee);

                            if (!DoublesWithinEpsilon(mvrFee, currMvrFee))
                                GenAddlRecordsReverseFeeChange("MVRFee", mvrFee, currMvrFee);

                            if (!DoublesWithinEpsilon(policyFee, currPolicyFee))
                                GenAddlRecordsReverseFeeChange("PolicyFee", policyFee, currPolicyFee);

                            if (!DoublesWithinEpsilon(maintenanceFee, currMaintenanceFee))
                                GenAddlRecordsReverseFeeChange("MaintenanceFee", maintenanceFee, currMaintenanceFee);

                            if (!DoublesWithinEpsilon(pippdOnlyFee, currPippdOnlyFee))
                                GenAddlRecordsReverseFeeChange("PIPPDOnlyFee", pippdOnlyFee, currPippdOnlyFee);

                            if (!DoublesWithinEpsilon(rewriteFee, currRewriteFee))
                                GenAddlRecordsReverseFeeChange("RewriteFee", rewriteFee, currRewriteFee);


                            // Get Amounts for Policy records
                            var commissionPrem = writtenSum;
                            writtenSum += fhcfFee + sr22Fee + dbSetupFee + mvrFee + policyFee + maintenanceFee + pippdOnlyFee +
                                          rewriteFee;
                            // *********************************************************************************************
                            // End new logic. No use math to insert all audit records and to set the policy information.
                            // *********************************************************************************************

                            // ----------------------------------------------------------------------------------
                            errorSection = "1: Copy from Policy to PolicyEndorseQuote.";

                            (new StoredProcedures(this)).CopyPolicyEndorseQuoteToPolicyTables(policyNo, userName, guidErrorLog);

                            ExecuteNonQueryWithParameters(IssueCancellation.UpdatePolicyEndorseQuoteCancel, new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 99),
                                CreateParamDateTime("@CancellationDate", cancellationDate)
                            }, conn);

                            // ----------------------------------------------------------------------------------
                            errorSection = "3: AuditPolicyHistory insert.";

                            historyId = ConvertScopeIdentity(ExecuteScalarWithParameters(IssueCancellation.InsertAuditPolicyHistory,
                                new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamDateTime("@AcctDate", acctDate),
                                    CreateParamDateTime("@CancellationDate", cancellationDate),
                                    CreateParamVarchar("@UserName", userName, 99),
                                    CreateParamFloat("@OrigBaseCommission", origBaseCommission),
                                    CreateParamFloat("@CommissionPrem", commissionPrem),
                                    CreateParamFloat("@OrigTotalFees", origTotalFees),
                                    CreateParamFloat("@WrittenSum", writtenSum),
                                    CreateParamFloat("@OrigWritten", origWritten),
                                    CreateParamVarchar("@GUID_HistoryIndexID", guidHistoryIndexId, 99),
                                }, conn)) ?? 0;

                            if (GetPolicyCancelCount() > 0)
                            {
                                UpdateAuditPolicyHistorySetNoticeInactive();
                                UpdatePolicyFutureActionsCancel();
                            }

                            // Update the History key in the temp audit records

                            tempAuditCoverageData.Table.Select(c =>
                            {
                                c.ActionID = historyId;
                                return c;
                            }).ToList();

                            tempAuditCoverageData.Table.Where(c => c.TermIndexID == cancellationId).Select(c =>
                            {
                                c.TermIndexID = historyId;
                                return c;
                            }).ToList();

                            // Insert generated records into AuditCoverageData

                            foreach (var rec in tempAuditCoverageData.Table)
                            {
                                var sqlParams = new[]
                                {
                                    CreateParamDateTime("@AcctDate", rec.AcctDate),
                                    CreateParamVarchar("@Action", rec.Action, 99),
                                    CreateParamInt("@ActionID", rec.ActionID),
                                    CreateParamInt("@Annualized", rec.Annualized),
                                    CreateParamBit("@ChangeDateEnd", rec.ChangeDateEnd),
                                    CreateParamInt("@Company", rec.Company),
                                    CreateParamVarchar("@Coverage", rec.Coverage, 99),
                                    CreateParamInt("@EarnNoDays", rec.EarnNoDays),
                                    CreateParamDateTime("@EarnStart", rec.EarnStart),
                                    CreateParamDateTime("@EarnStop", rec.EarnStop),
                                    CreateParamFloat("@EarnedSum", rec.EarnedSum),
                                    CreateParamDateTime("@EffDate", rec.EffDate),
                                    CreateParamDateTime("@ExpDate", rec.ExpDate),
                                    CreateParamInt("@IgnoreIndexID", rec.IgnoreIndexID),
                                    CreateParamBit("@IgnoreRecord", rec.IgnoreRecord),
                                    CreateParamInt("@IndexID", rec.TempTableID),
                                    CreateParamBit("@IsFee", rec.IsFee),
                                    CreateParamFloat("@LastDayEarn", rec.LastDayEarn),
                                    CreateParamBit("@Negate", rec.Negate),
                                    CreateParamInt("@LOB", rec.LOB),
                                    CreateParamFloat("@OneDayEarn", rec.OneDayEarn),
                                    CreateParamVarchar("@OrigAction", rec.OrigAction, 99),
                                    CreateParamVarchar("@PolicyNo", rec.PolicyNo, 99),
                                    CreateParamFloat("@ProRata", rec.ProRata),
                                    CreateParamVarchar("@State", rec.State, 99),
                                    CreateParamInt("@TermIndexID", rec.TermIndexID),
                                    CreateParamInt("@VehIndex", rec.VehIndex),
                                    CreateParamFloat("@Written", rec.Written),
                                };
                                ExecuteNonQueryWithParameters(IssueCancellation.InsertIntoAuditCoverageDateFromTemp, sqlParams,
                                    conn);

                                // ----------------------------------------------------------------------------------
                                errorSection = "12: update PolicyEndorseQuoteCars.";

                                var currCar = 1;
                                while (currCar <= GetCountPolicyQuoteCars())
                                {
                                    // ----------------------------------------------------------------------------------
                                    errorSection = "13: update PolicyEndorseQuoteCars BIWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("BI", currCar);

                                    // ----------------------------------------------------------------------------------
                                    errorSection = "14: update PolicyEndorseQuoteCars PDWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("PD", currCar);

                                    // ----------------------------------------------------------------------------------
                                    errorSection = "15: update PolicyEndorseQuoteCars  MPWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("MP", currCar);

                                    // ----------------------------------------------------------------------------------
                                    errorSection = "16: update PolicyEndorseQuoteCars UMWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("UM", currCar);

                                    // ----------------------------------------------------------------------------------
                                    errorSection = "17: update PolicyEndorseQuoteCars CompWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("COMP", currCar);

                                    // ----------------------------------------------------------------------------------
                                    errorSection = "18: update PolicyEndorseQuoteCars CollWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("COLL", currCar);

                                    // ----------------------------------------------------------------------------------
                                    errorSection = "19: update PolicyEndorseQuoteCars CollWritten.";

                                    UpdatePolicyEndorseQuoteCarsCoverageWritten("PIP", currCar);

                                    currCar++;
                                }

                                // ----------------------------------------------------------------------------------
                                errorSection = "20: update PolicyEndorseQuote Policy Fields.";

                                ExecuteNonQueryWithParameters(IssueCancellation.UpdatePolicyEndorseQuoteFees, new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamFloat("@WrittenSum", writtenSum),
                                    CreateParamFloat("@CommissionPrem", commissionPrem),
                                    CreateParamFloat("@FHCFFee", fhcfFee),
                                    CreateParamFloat("@DBSetupFee", dbSetupFee),
                                    CreateParamFloat("@SR22Fee", sr22Fee),
                                    CreateParamFloat("@MVRFee", mvrFee),
                                    CreateParamFloat("@PolicyFee", policyFee),
                                    CreateParamFloat("@MaintenanceFee", maintenanceFee),
                                    CreateParamFloat("@PIPPDOnlyFee", pippdOnlyFee),
                                    CreateParamFloat("@RewriteFee", rewriteFee)
                                }, conn);

                                UpdatePolicyEndorseQuoteAdndCost();
                                UpdatePolicyEndorseQuoteLouCost();

                                // ----------------------------------------------------------------------------------
                                errorSection = "4: EXEC spUW_AuditSaveDifference Policy tables. ";

                                var storedProcs = new StoredProcedures(this);

                                storedProcs.AuditSaveDifference(policyNo, "PolicyEndorseQuote", "Policy", historyId, userName, false,
                                    guidErrorLog);

                                // ----------------------------------------------------------------------------------
                                errorSection = "5 :EXEC spUW_AuditSaveDifference PolicyCars tables. ";

                                storedProcs.AuditSaveDifference(policyNo, "PolicyEndorseQuoteCars", "PolicyCars", historyId, userName,
                                    false, guidErrorLog);

                                // ----------------------------------------------------------------------------------
                                errorSection = "6 :EXEC spUW_AuditSaveDifference PolicyDrivers tables. ";

                                storedProcs.AuditSaveDifference(policyNo, "PolicyEndorseQuoteDrivers", "PolicyDrivers",
                                    historyId, userName, false, guidErrorLog);

                                // ----------------------------------------------------------------------------------
                                errorSection = "7 :EXEC spUW_AuditSaveDifference PolicyDriversViolations tables. ";

                                storedProcs.AuditSaveDifference(policyNo, "PolicyEndorseQuoteDriversViolations",
                                    "PolicyDriversViolations", historyId, userName, false, guidErrorLog);

                                // ----------------------------------------------------------------------------------
                                errorSection = "30: Insert Record into Policy from PolicyEndorseQuote. ";

                                storedProcs.CopyPolicyEndorseQuoteToPolicyTables(policyNo, userName, guidErrorLog);

                                // ----------------------------------------------------------------------------------
                                errorSection = "29: Copy PolicyNo from Policy tables to AuditPolicy tables. ";

                                storedProcs.CopyPolicyToAuditTables(policyNo, historyId, userName, null);

                                // ----------------------------------------------------------------------------------
                                errorSection = "48: Update installments";

                                // 1) set all installments that have a balance or are locked with a fee to ignore.. but flagged with the hist id of the cancellation
                                // 2) determine what premium has already been billed
                                // 3) add a new installment that balances the premiums flagged with the hist id of the cancellation

                                // Note: on reinstatements.. set all installments set to ignore with canc ID to ignore=0 and all 
                                //     installments created by cancellation script to ignore=1

                                UpdateInstallments();


                                // ----------------------------------------------------------------------------------
                                errorSection = "48: Get totals";

                                var billedAmount = (double)GetBilledAmount();

                                // var feeTotal = GetFeeTotal();

                                var installNo = GetInstallNo();

                                // ----------------------------------------------------------------------------------
                                errorSection = "48a: Insert Installment";

                                ExecuteNonQueryWithParameters(IssueCancellation.InsertIntoInstallments, new[]
                                {
                                    CreateParamVarchar("@PolicyNo", policyNo, 99),
                                    CreateParamInt("@InstallNo", installNo),
                                    CreateParamDateTime("@AcctDate", acctDate),
                                    CreateParamFloat("@WrittenSum", writtenSum),
                                    CreateParamFloat("@BilledAmount", billedAmount),
                                    CreateParamInt("@HistoryID", historyId),
                                    CreateParamVarchar("@UserName", userName, 99),
                                }, conn);

                                // ----------------------------------------------------------------------------------
                                errorSection = "9: exec  spUW_InstallmentPaymentApply. ";

                                storedProcs.InstallmentPaymentApply(policyNo, null, userName, guidErrorLog);

                                // ----------------------------------------------------------------------------------
                                errorSection = "49: AuditPolicyHistoryReasons Update History Index";

                                if (GetCountHistoryCancels() > 0)
                                    UpdateAuditPolicyHistoryReasons();

                                // ----------------------------------------------------------------------------------
                                errorSection = "28: Insert into Print Suspense. ";

                                InsertIntoPolicyPrintSuspense();
                            }

                        }  // policy valid

                    }  // connection

                    txScope.Complete();

                } // transaction
            }
            catch (Exception ex)
            {
                TxLogError(guidErrorLog, policyNo, userName, DateTime.Now, "IssueCancellation",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }

        }  // TxIssueCancellation()

        // ReSharper disable once RedundantAssignment
        public void TxIssueNotice(string policyNo, DateTime? noticeDate, string cancelType, string reason1,
            string reason2,
            string reason3, string reason4, string userName, DateTime? nullableMailDate, bool isNSFFeeCharged, string guid)
        {
            var mailDate = nullableMailDate ?? DateTime.Now;
            var reasons = new[] { reason1, reason2, reason3, reason4 };
            DateTime? existingPolicyCancelDate;
            int? totalActiveNoticeCount;
            bool isNoticeDateSmallerThanPolicyCancelDate = false;
            DateTime? policyEffDate;
            var errorSection = string.Empty;

            bool policyValid = true;

            try
            {
                using (var conn = GetConnection())
                {
                    void LocalLogError(string errorDescr) => LogErrorWithConn(guid, policyNo, userName, DateTime.Now, "IssueNotice", string.Empty, errorDescr,conn);

                    conn.Open();

                    switch (cancelType)
                    {
                        case "Non-Renewal":
                            var obj = ExecuteScalarWithPolicyNo(Queries.Policy_GetExpDateByPolicyNo, policyNo, conn);
                            noticeDate = GetDbValOrNull<DateTime>(obj);
                            break;
                        case "Underwriting Request":
                            noticeDate = (mailDate).AddDays(45D);
                            break;
                        default:
                            noticeDate = (mailDate).AddDays(12D); // Default to Non-Pay
                            break;
                    }

                    // ------------------------------------------------------------------
                    errorSection = "0: Startup";

                    userName = userName ?? Environment.UserName;

                    existingPolicyCancelDate = GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(Queries.Policy_GetCancelDateByPolicyNo, policyNo, conn));
                    totalActiveNoticeCount = GetDbValOrNull<int>(ExecuteScalarWithPolicyNo(IssueNotice.GetTotalActiveNoticeCount,policyNo,conn));
                
                    //  ----add error if policy if policy passed does not exists -----------------------------------------
                    if ((int)ExecuteScalarWithPolicyNo(Queries.Policy_CountOIDByPolicyNo, policyNo, conn) == 0)
                    {
                        policyValid = false;
                        LocalLogError($"Policy {policyNo} does not exist.");
                    }

                    // Add error if policy is not an allowed PolicyStatus. ---------------------------------------- -
                    // Allowed statuses: ACTIVE, CANCEL, or NOTICE
                    // Disallowed statuses: BINDER.

                    if ((int)ExecuteScalarWithPolicyNo(IssueNotice.DisallowedStatus, policyNo, conn) > 0)
                    {
                        policyValid = false;
                        LocalLogError($"Policy Status for Policy {policyNo} must be ACTIVE, CANCEL, or NOTICED.");
                    }

                    // ----add error if @NoticeDate is before Policy EffDate, or after Policy ExpDate. -----------------------------------------

                    policyEffDate =
                        GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(Queries.Policy_Top1EffDateByPolicyNo, policyNo,
                            conn));
                    var policyExpDate = GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(Queries.Policy_Top1ExpDateByPolicyNo, policyNo,
                        conn));

                    if (noticeDate < policyEffDate || noticeDate > policyExpDate)
                    {
                        policyValid = false;
                        LocalLogError("Notice Date cannot be before Policy Effective Date, or after Policy Expiration Date.");
                    }

                    // ---------------------------------------------------------------------------------
                    // if Policy passes validation above, issue the policy.

                    var list = ExecuteQueryWithPolicyNo(IssueNotice.GetPreSection1Info, policyNo, conn);
                    var tempNotices = new TempTable<TempNotices>();
                    foreach (var rec in list)
                    {
                        var values = (object[])rec;
                        tempNotices.AddRecord(new TempNotices
                        {
                            APHIndexId = GetDbValOrNull<int>(values[0]),
                            PolicyNo = GetDbRefValOrNull<string>(values[1]),
                            APHNoticeMailDate = GetDbValOrNull<DateTime>(values[2]),
                            APHNoticeCancelType = GetDbRefValOrNull<string>(values[3]),
                            APHCancelDate = GetDbValOrNull<DateTime>(values[4]),
                            APHAction = GetDbRefValOrNull<string>(values[5]),
                        });
                    }

                    if (tempNotices.Table.Count(c => c.APHNoticeCancelType == cancelType) > 0)
                    {
                        policyValid = false;
                        LocalLogError($"{cancelType}: Duplicate active notices of same type for a policy not allowed.");
                    }
                }

                if (policyValid)
                {
                    using (var txScope = new TransactionScope())
                    {
                        using (var conn = GetConnection())
                        {
                            conn.Open();
                            // ------------------------------------------------------------------
                            errorSection = "1:Get Policy values.";

                            var resultSet = ExecuteQueryWithPolicyNo(IssueNotice.GetAuditPolicyHistory, policyNo, conn);
                            if (resultSet.Count == 0)
                                throw new StoredProcedureException(
                                    $"No AuditPolicyHistory records for Policy # {policyNo}.");
                            var values = (object[])resultSet[0];
                            var origBaseCommission = ConvertFromDbVal(values[0], 0D);
                            // NOT USED: var origOtherComm = ConvertFromDbVal(values[1], 0D);
                            var origTotalFees = ConvertFromDbVal(values[2], 0D);
                            var origWritten = ConvertFromDbVal(values[3], 0D);

                            // ------------------------------------------------------------------
                            errorSection = "2: get @TotalMailDays.";

                            // NOT USED: var runDate = DateValueNoTimestamp(DateTime.Now);
                            // NOT USED: var totalMailDays = GetDbValOrNull<int>(ExecuteScalarNoArgs(IssueNotice.GetTotalMailDays, conn));

                            // ------------------------------------------------------------------
                            errorSection = "3:  Get current Installment number.";

                            var actionType = "NOTICE";
                            var acctDate = DateTime.Now;
                            var installNo = TxInstallNoCurrent(policyNo, mailDate);


                            // ------------------------------------------------------------------
                            errorSection = "4: AuditPolicyHistory insert.";

                            var noticeAmount = ConvertFromDbVal(
                                ExecuteScalarWithPolicyNo(IssueNotice.GetSumBalanceInstallments, policyNo, conn), 0M);
                            var noticeFeeAmount = 10D;


                            // ----- Find out if late fee was already charged for current installment. -----------
                            // -----If True, then subtract $10  from noticeAmount.  ----------------------------

                            var sqlParams = new[]
                            {
                                CreateParamVarchar("@PolicyNo", policyNo, 40),
                                CreateParamInt("@InstallNo", installNo)
                            };
                            var lateFeeCount =
                                (int)ExecuteScalarWithParameters(IssueNotice.GetLateFeeCount, sqlParams, conn);
                            if (lateFeeCount > 0 && noticeAmount >= 10M)
                            {
                                noticeAmount -= 10M;
                            }

                            //-------------------------------------------------------------------------------------
                            // --Add NSF Fee if @Is_NSF_Fee_Charged = 1 

                            var rateCycle =
                                ConvertFromDbVal(ExecuteScalarWithPolicyNo(IssueNotice.GetTop1RateCycleFromPolicy,
                                    policyNo,
                                    conn), string.Empty);

                            if (rateCycle.TrimEnd().Length == 0) // use latest ratecycle if not set on policy
                            {
                                // NOT USED: rateCycle = GetDbRefValOrNull<string>(ExecuteScalarNoArgs(IssueNotice.GetTop1RateCycleFromAARateCycles, conn));
                            }

                            // errorSection = "2: Get NSF_Fee";

                            var nsfFee = ConvertFromDbVal(ExecuteScalarWithParameters(IssueNotice.GetNSFFee, new []{CreateParamVarchar("@RateCycle",rateCycle,20)}, conn), 0D);

                            //-------------------------------------------------------------------------------------
                            // --Insert Main History Table

                            sqlParams = new[]
                            {
                                CreateParamVarchar("@PolicyNo",policyNo,25),
                                CreateParamVarchar("@ActionType",actionType,25),
                                CreateParamDateTime("@AcctDate",acctDate),
                                CreateParamDateTime("@EffDate",noticeDate),
                                CreateParamDateTime("@ProcessDate",acctDate),
                                CreateParamDateTime("@MailDate",mailDate),
                                CreateParamVarchar("@UserName",userName,25),
                                CreateParamFloat("@NoticeAmount",(double)noticeAmount),
                                CreateParamFloat("@NoticeFeeAmount", noticeFeeAmount),
                                CreateParamFloat("@NSFFeeAmount", isNSFFeeCharged ? nsfFee : 0D),
                                CreateParamVarchar("@CancelType",cancelType,30),
                                CreateParamFloat("@OrigBaseCommission",origBaseCommission),
                                CreateParamFloat("@OrigTotalFees",origTotalFees),
                                CreateParamFloat("@OrigWritten",origWritten),
                                CreateParamDateTime("@NoticeDate",noticeDate),
                                CreateParamVarchar("@GUID",guid,40)
                            };
                            var historyId =
                                ConvertScopeIdentity(ExecuteScalarWithParameters(IssueNotice.InsertIntoAuditPolicyHistory,
                                    sqlParams, conn)) ??
                                throw new StoredProcedureException("Insert into AuditPolicyHistory failed");
                        
                            // ------------------------------------------------------------------
                            errorSection = "5: INSERT INTO AuditPolicyHistoryReasons.";

                            var reasonNo = 1;
                            for (int i = 0; i < 4; i++)
                            {
                                var reason = reasons[i];
                                if ((reason?.Trim().Length ?? 0) == 0) continue;

                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,40),
                                    CreateParamInt("@HistoryID",historyId),
                                    CreateParamVarchar("@ActionType",actionType,40),
                                    CreateParamInt("@ReasonNo",reasonNo),
                                    CreateParamVarchar("@Reason",reason,40), // untrimmed?
                                    CreateParamVarchar("@UserName",userName,40),
                                    CreateParamDateTime("@AcctDate",acctDate),
                                    CreateParamVarchar("@GUID",guid,36),
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.InsertReasons, sqlParams, conn);
                            }

                            // ------------------------------------------------------------------
                            errorSection = "7:   Update Policy status to NOTICE.";

                            var policyCount = (int)ExecuteScalarWithPolicyNo(IssueNotice.GetNoticePoliciesCount, policyNo, conn);
                            var maxId = ConvertFromDbVal(ExecuteScalarWithPolicyNo(IssueNotice.GetMaxID, policyNo, conn),
                                0);
                            if ((policyCount > 0 || maxId > 0) && existingPolicyCancelDate > noticeDate)
                            {
                                isNoticeDateSmallerThanPolicyCancelDate = true;
                            }

                            if (string.Equals(cancelType, "Non-Renewal"))
                            {
                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,25),
                                    CreateParamBit("@IsNoticeDateSmallerThanPolicyCancelDate",isNoticeDateSmallerThanPolicyCancelDate),
                                    CreateParamDateTime("@NoticeDate",noticeDate),
                                    CreateParamDateTime("@ExistingPolicyCancelDate",existingPolicyCancelDate),
                                    CreateParamInt("@TotalActiveNoticeCount",totalActiveNoticeCount)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdatePolicyRenewalStatusNonRenew, sqlParams, conn);

                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,25),
                                    CreateParamInt("@HistoryID",historyId)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdateHistoryReasons, sqlParams, conn);
                            }
                            else
                            {
                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,25),
                                    CreateParamBit("@IsNoticeDateSmallerThanPolicyCancelDate",isNoticeDateSmallerThanPolicyCancelDate),
                                    CreateParamDateTime("@NoticeDate",noticeDate),
                                    CreateParamDateTime("@ExistingPolicyCancelDate",existingPolicyCancelDate),
                                    CreateParamInt("@TotalActiveNoticeCount",totalActiveNoticeCount)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdatePolicyStatusNotice, sqlParams, conn);
                            }


                            // ------------------------------------------------------------------
                            errorSection = "12:  Update prior records in PolicyPrintSuspense.";

                            ExecuteNonQueryWithPolicyNo(IssueNotice.UpdateSuspense, policyNo, conn);

                            // ------------------------------------------------------------------
                            errorSection = "13: Insert into Print Suspense.";

                            sqlParams = new[]
                            {
                                CreateParamVarchar("@PolicyNo",policyNo,20),
                                CreateParamInt("@HistoryID",historyId),
                                CreateParamVarchar("@UserName",userName,50)
                            };
                            ExecuteNonQueryWithParameters(IssueNotice.InsertIntoSuspense, sqlParams, conn);

                            // ------------------------------------------------------------------
                            errorSection = "14: exec spUW_PolicyUpdateFieldFormats.";

                            (new StoredProcedures(this)).PolicyUpdateFieldFormats(policyNo);

                            // ------------------------------------------------------------------
                            errorSection = "15: Insert into PolicyFutureActions.";

                            // var cancelDate = noticeDate?.AddDays(30);
                            var dueDate = noticeDate?.AddDays(30);

                            nullableMailDate = GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(IssueNotice.GetTop1MailDate, policyNo, conn));
                            var processDate = GetDbValOrNull<DateTime>(ExecuteScalarWithPolicyNo(IssueNotice.GetTop1EffDate, policyNo, conn));
                            var action = "CANCEL";

                            (new StoredProcedures(this)).PolicyFutureActionsAddRecord(policyNo, action,dueDate,nullableMailDate,processDate,userName,guid,historyId);

                            policyCount =
                                (int)ExecuteScalarWithPolicyNo(IssueNotice.GetNoticePoliciesCount, policyNo, conn);
                            if (policyCount > 0)
                            {
                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,20),
                                    CreateParamInt("@HistoryID",historyId),
                                    CreateParamDateTime("@NoticeDate",noticeDate)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdateFutureActions, sqlParams, conn);
                            }

                            // ----------------------------------------------------------------------------------

                            if (string.Equals(cancelType, "Non-Renewal"))
                            {
                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,20),
                                    CreateParamInt("@HistoryID",historyId)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdateSuspenseNonRenew, sqlParams, conn);
                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,20),
                                    CreateParamInt("@HistoryID",historyId)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdateHistoryNonRenew, sqlParams, conn);
                            }

                            if (string.Equals(cancelType, "Deposit Agent Sweep Bounced"))
                            {
                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,20),
                                    CreateParamInt("@HistoryID",historyId),
                                    CreateParamDateTime("@MailDate",nullableMailDate),
                                    CreateParamDateTime("@Policy_EffDate",policyEffDate)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdateHistoryEffVoidDates, sqlParams, conn);

                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,20),
                                    CreateParamDateTime("@Policy_EffDate",policyEffDate)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdatePolicyVoidpend, sqlParams, conn);

                                sqlParams = new[]
                                {
                                    CreateParamVarchar("@PolicyNo",policyNo,20),
                                    CreateParamInt("@HistoryID",historyId)
                                };
                                ExecuteNonQueryWithParameters(IssueNotice.UpdateSuspenseBounced, sqlParams, conn);
                            }

                        }

                        txScope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "IssueNotice",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Apply Late Fee for past due current installments for a Policy
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// LateFeePolicyApply.
        /// 
        /// Migration of stored procedure spUW_LateFeePolicyApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="mailDate"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxLateFeePolicyApply(string policyNo, DateTime mailDate, string userName, string guid)
        {
            const string defaultRateCycle = "AA20190101";
            const double defaultLateFee = 10D;

            var errorSection = "0: Startup";
            if (userName == null)
            {
                userName = Environment.UserName;
            }

            var policyCount = GetCountOfPolicyRecordsByPolicyNo(policyNo);

            if (policyCount == 0)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "LateFeePolicyApply", "",
                    $"Policy # {policyNo} does not exist.");
            }

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        var rateCycle =
                            ConvertFromDbVal<string>(
                                ExecuteScalarWithPolicyNo(Queries.Policy_TopRateCycleByPolicyNo, policyNo, conn), null) ??
                            ConvertFromDbVal(ExecuteScalarWithPolicyNo(LateFeePolicyApply.GetRateCycle, policyNo, conn),
                                defaultRateCycle);

                        // ------------------------------------------------------------------
                        errorSection = "1: Get standard late fee.";

                        decimal lateFee;
                        using (var cmd = new SqlCommand(LateFeePolicyApply.GetAmount, conn))
                        {
                            cmd.Parameters.Add(CreateParamVarchar("@RateCycle", rateCycle, 20));
                            lateFee = (decimal)ConvertFromDbVal(cmd.ExecuteScalar(), defaultLateFee);
                        }

                        // ------------------------------------------------------------------
                        errorSection = "2: Get Days Past Due For Late Fee.";

                        var daysPastDueForLateFee =
                            ConvertFromDbVal(ExecuteScalarNoArgs(LateFeePolicyApply.GetDaysPastDue, conn), 0);

                        // ------------------------------------------------------------------
                        errorSection = "3: Get current Installment number.";

                        var installNo = TxInstallNoCurrent(policyNo, mailDate);

                        // ------------------------------------------------------------------
                        errorSection = "4: Insert into Installments.";

                        using (var cmd = new SqlCommand(LateFeePolicyApply.InsertInstallments, conn))
                        {
                            cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                            cmd.Parameters.Add(CreateParamDateTime("@MailDate", mailDate));
                            cmd.Parameters.Add(CreateParamSmallMoney("@LateFee", lateFee));
                            cmd.Parameters.Add(CreateParamVarchar("@UserName", userName, 25));
                            cmd.Parameters.Add(CreateParamDecimal("@InstallNo", installNo, 5, 1));
                            cmd.Parameters.Add(CreateParamInt("@DaysPastDueForLateFee", daysPastDueForLateFee));
                            cmd.ExecuteNonQuery();
                        }

                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "LateFeePolicyApply",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Add entry to ErrorLog table
        ///
        /// Before setting the string properties, the values are truncated to fit
        /// into their corresponding fields using the value in the Size attribute
        /// of each field
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="dateTimeError">Timestamp of the error</param>
        /// <param name="errorProcedure">Where the error occurred</param>
        /// <param name="errorCode">Error # or code</param>
        /// <param name="errorDescr">Error description</param>
        public void TxLogError(string guid, string policyNo, string userName, DateTime? dateTimeError,
            string errorProcedure,
            string errorCode, string errorDescr)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                LogErrorWithConn(guid, policyNo, userName, dateTimeError, errorProcedure, errorCode, errorDescr, conn);
            }

        }

        /// <summary>
        /// Determine minimum due for endorsement payments
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// MinimumEndorsementPaymentDue.
        /// 
        /// Migrated from stored procedure spUW_MinimumEndorsementPaymentDue
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="endorsementDate"></param>
        /// <param name="fullAmountOfEndorsement"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public decimal TxMinimumEndorsementPaymentDue(string policyNo, DateTime endorsementDate,
            decimal fullAmountOfEndorsement, string userName)
        {
            decimal minimumAmountDue;
            int? daysPolicyRemaining = null;
            double? endorsementRunRate = null;
            DateTime? dueDateNextInstallment = null;
            int? daysUntilNextPaymentDue = null;

            var guid = String.Empty;
            var errorSection = "1: Startup";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // Get dates and plan for specified policy number
                        // PayPlan is a foreign key

                        var results = ExecuteQueryWithPolicyNo(MinimumEndorsementPaymentDue.GetDatesAndPayPlan, policyNo, conn);
                        if (results.Count == 0)
                            throw new StoredProcedureException($"No dates and pay plan retrieved for policy # {policyNo}");
                        var values = (object[])results.First();
                        var effDate = ConvertFromDbVal<DateTime>(values[0]);
                        var expDate = ConvertFromDbVal<DateTime>(values[1]);
                        if (effDate == default || expDate == default)
                            throw new StoredProcedureException($"One or more dates null for policy # {policyNo}");
                        if (values[2] == null || values[2] == DBNull.Value)
                            throw new StoredProcedureException($"Pay plan null for policy # {policyNo}");
                        var payPlan = (int)values[2];

                        // Get Downpayment as Percentage

                        var obj = InTxGetDownpaymentAsPercentageFromRatePayPlans(payPlan, conn);
                        var dpAsPct = ConvertFromDbVal(obj, 0D); // TODO: or throw exp on null?

                        // original test was comparing double against 1

                        if (Math.Abs(dpAsPct - 1) < FloatTolerance)
                        {
                            minimumAmountDue = fullAmountOfEndorsement;
                        }
                        else
                        {
                            daysPolicyRemaining = DateDiffDays(endorsementDate, expDate);
                            endorsementRunRate = (double)fullAmountOfEndorsement / daysPolicyRemaining;

                            obj = InTxGetDueDateNextInstallment(policyNo, endorsementDate, conn);
                            if (obj == null || obj == DBNull.Value)
                            {
                                minimumAmountDue = fullAmountOfEndorsement;
                            }
                            else
                            {
                                dueDateNextInstallment = (DateTime)obj;
                                daysUntilNextPaymentDue = DateDiffDays(endorsementDate, (DateTime)dueDateNextInstallment);
                                daysUntilNextPaymentDue += 12;
                                minimumAmountDue =
                                    decimal.Round((decimal)(daysUntilNextPaymentDue * endorsementRunRate), 2);
                            }
                        }

                        InTxInsertAuditMinimumDueEndorsement(policyNo, endorsementDate, fullAmountOfEndorsement, userName,
                            effDate, expDate, payPlan, dpAsPct, daysPolicyRemaining, endorsementRunRate,
                            dueDateNextInstallment, daysUntilNextPaymentDue, minimumAmountDue, conn);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "EndorsementPaymentApply", // shouldn't this be MinimumAmountDue?
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }

            return minimumAmountDue;
        }

        /// <summary>
        /// Move money between policies
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// MoveMoney.
        /// 
        /// Migration of stored procedure spUW_MoveMoney
        /// </summary>
        /// <param name="moveFromPolicyNo"></param>
        /// <param name="moveToPolicyNo"></param>
        /// <param name="amount"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="displayDate"></param>
        /// <param name="ignoreToPolicyExists"></param>
        public void TxMoveMoney(string moveFromPolicyNo, string moveToPolicyNo, decimal amount, string userName,
            string guid,
            DateTime? displayDate = null, bool ignoreToPolicyExists = false)
        {
            var errorSection = "0: Startup";
            if (userName == null)
            {
                userName = Environment.UserName;
            }

            try
            {
                // ---- Add error if Policy or Binder not found for PolicyNo. -----------------------------------------

                ValidateMoveMoneyParameters(moveFromPolicyNo, userName, guid);

                if (!ignoreToPolicyExists)
                {
                    ValidateMoveMoneyParameters(moveToPolicyNo, userName, guid);
                }

                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // -----------------------------------------------------------------------------------
                        errorSection = "1: INSERT INTO Payments.";

                        InTxMoveMoneyInsertFrom(moveFromPolicyNo, moveToPolicyNo, amount, userName, displayDate, conn);

                        InTxMoveMoneyInsertTo(moveFromPolicyNo, moveToPolicyNo, amount, userName, displayDate, conn);

                        var postMarkDate = DateTime.Now;

                        var sp = new StoredProcedures(this);
                        sp.InstallmentPaymentApply(moveFromPolicyNo,postMarkDate,userName,guid);
                        sp.InstallmentPaymentApply(moveToPolicyNo,postMarkDate,userName,guid);

                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, moveFromPolicyNo, userName, DateTime.Now, "MoveMoney",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        public DateTime TxNextBusinessDay(DateTime day)
        {
            const double oneDay = 1D;
            const int maxDaysToTry = 100;

            var nextDay = day.AddDays(oneDay);
            var cnt = 0;

            while (!TxValidBusinessDay(nextDay))
            {
                nextDay = nextDay.AddDays(oneDay);

                if (cnt++ > maxDaysToTry)
                    return DateTime.MinValue;
            }

            return nextDay;
        }

        /// <summary>
        /// Get Policy Balance
        ///
        /// Migrated from user-defined db function fnPolicyBalance
        /// </summary>
        /// <param name="policyNo"></param>
        /// <returns></returns>
        public decimal TxPolicyBalance(string policyNo)
        {
            using (var conn = GetConnection())
            {
                conn.Open();

                var balance = ConvertFromDbVal(ExecuteScalarWithPolicyNo(StoredProcedureQueries.PolicyBalance.GetPolicyBalance, policyNo, conn), 0M);
                return decimal.Round(balance, 2);
            }
        }

        /// <summary>
        /// Insert a record into PolicyFutureActions table
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// PolicyFutureActionsAddRecord.
        /// 
        /// Migration of stored procedure spUW_PolicyFutureActions_AddRecord
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="action"></param>
        /// <param name="dueDate"></param>
        /// <param name="mailDate"></param>
        /// <param name="processDate"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="historyIndexId"></param>
        public void TxPolicyFutureActionsAddRecord(string policyNo, string action, DateTime? dueDate,
            DateTime? mailDate,
            DateTime? processDate, string userName, string guid, int? historyIndexId = null)
        {
            var errorSection = "0: Startup";
            var policyValid = true;

            if (userName == null)
            {
                userName = Environment.UserName;
            }

            if (GetCountOfPolicyOidRecordsByPolicyNo(policyNo) == 0)
            {
                policyValid = false;
                TxLogError(guid, policyNo, userName, DateTime.Now, "PolicyFutureActionsAddRecord", string.Empty,
                    $"{errorSection}; Policy {policyNo} does not exist.");
            }

            if (policyValid)
            {
                try
                {
                    using (var txScope = new TransactionScope())
                    {
                        using (var conn = GetConnection())
                        {
                            conn.Open();

                            using (var cmd = new SqlCommand(PolicyFutureActionsAddRecord.InsertPolicyFutureActions, conn))
                            {
                                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 20));
                                cmd.Parameters.Add(CreateParamVarchar("@Action", action, 25));
                                cmd.Parameters.Add(CreateParamDateTime("@DueDate", dueDate));
                                cmd.Parameters.Add(CreateParamDateTime("@MailDate", mailDate));
                                cmd.Parameters.Add(CreateParamDateTime("@ProcessDate", processDate));
                                cmd.Parameters.Add(CreateParamInt("@HistoryIndexID", historyIndexId));
                                cmd.ExecuteNonQuery();
                            }
                        }

                        txScope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    TxLogError(guid, policyNo, userName, DateTime.Now, "PolicyFutureActionsAddRecord",
                        ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                    throw;
                }
            }
        }

        /// <summary>
        /// Policy fields formatting and cleanup
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// PolicyUpdateFieldFormats.
        /// 
        /// Migration of stored procedure spUW_PolicyUpdateFieldFormats
        /// </summary>
        /// <param name="policyNo"></param>
        public void TxPolicyUpdateFieldFormats(string policyNo)
        {
            var errorSection = "1: Update Policy Insured Full Name.";
            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        ExecuteNonQueryWithPolicyNo(PolicyUpdateFieldFormats.SetIns1FullName, policyNo, conn);
                        ExecuteNonQueryWithPolicyNo(PolicyUpdateFieldFormats.SetIns2FullName, policyNo, conn);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(string.Empty, policyNo, Environment.UserName, DateTime.Now, "PolicyUpdateFieldFormats",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// RatePolicyAlert.
        /// 
        /// Migration of stored procedure spUW_RatePolicyAlert
        /// </summary>
        /// <param name="currIdStr"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void TxRatePolicyAlert(string currIdStr, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            string rateCycle = null;
            var currIdIsNumeric = int.TryParse(currIdStr, out int currId);
            var errorSection = "";
            if (userName is null)
            {
                userName = "ApplicationUser";
            }

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        if (isRenewal != null && (bool)isRenewal && currIdIsNumeric)
                        {
                            using (var cmd = new SqlCommand(RatePolicyAlert.GetRateCycleIsRenewal, conn))
                            {
                                cmd.Parameters.Add(CreateParamInt("@currID", currId));
                                rateCycle = GetDbRefValOrNull<string>(cmd.ExecuteScalar());
                            }

                        }
                        else if (quoteId != null)
                        {
                            DateTime? effDate;
                            using (var cmd = new SqlCommand(RatePolicyAlert.GetEffDate, conn))
                            {
                                cmd.Parameters.Add(CreateParamVarchar("@QuoteID", quoteId, 20));
                                effDate = GetDbValOrNull<DateTime>(cmd.ExecuteScalar());
                            }

                            using (var cmd = new SqlCommand(RatePolicyAlert.GetRateCycleQuoteId, conn))
                            {
                                cmd.Parameters.Add(CreateParamDateTime("@EffDate", effDate));
                                rateCycle = GetDbRefValOrNull<string>(cmd.ExecuteScalar());
                            }
                        }
                        else if (currIdIsNumeric)
                        {
                            using (var cmd = new SqlCommand(RatePolicyAlert.GetRateCycleNumeric, conn))
                            {
                                cmd.Parameters.Add(CreateParamInt("@currID", currId));
                                rateCycle = GetDbRefValOrNull<string>(cmd.ExecuteScalar());
                            }
                        }

                        if (rateCycle is null)
                        {
                            throw new StoredProcedureException(
                                $"Null Rate Cycle for currId \"{currIdStr}\" and quoteId \"{quoteId ?? "(null)"}\"");
                        }

                        new StoredProcedures(this).GetRateSpartanMethod(rateCycle)(currId, guid, userName, quoteId,
                            rtrProcessing, isRenewal);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, currIdStr, userName, DateTime.Now, "RatePolicyAlert",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// 
        ///
        /// 
        /// 
        /// Migration of stored procedure spUW_RateSpartan_CVData
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        [SuppressMessage("ReSharper", "ReturnValueOfPureMethodIsNotUsed")]
        public void TxRateSpartan_CVData(int currId, string guid, string userName, string quoteId = null, bool rtrProcessing = false,
            bool? isRenewal = null)
        {
            var errorSection = "-1: Startup";
            userName = userName ?? "ApplicationUser";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        errorSection = "0: Pull CV JSON from Quote";

                        if (quoteId != null)
                        {
                            int iCount;
                            string policyNo;

                            var tmpLoopTable = new TmpLoopTable();

                            var historyList =
                                ExecuteQueryWithParameters(RateSpartan_CVData.GetVeriskHistory, new []{ CreateParamVarchar("@QuoteID", quoteId, 20) }, conn);
                            foreach (var rec in historyList)
                            {
                                var values = (object[])rec;
                                tmpLoopTable.AddRecord(new TmpLoopTableRecord
                                {
                                    CVIndexID = GetDbValOrNull<int>(values[0]),
                                    PolicyNo = GetDbRefValOrNull<string>(values[1]),
                                    ResponseJson = GetDbRefValOrNull<string>(values[2]),
                                });
                            }

                            var maxCount = tmpLoopTable.Table.Select(c => c.TmpIndexID).DefaultIfEmpty(0).Max();
                            iCount = 1;

                            while (iCount <= maxCount)
                            {
                                var result = tmpLoopTable.Table.Where(c => c.TmpIndexID == iCount)
                                    .Select(c => new
                                    {
                                        c.PolicyNo,
                                        c.ResponseJson
                                    }).FirstOrDefault();
                                if (result == null) continue;
                                policyNo = result.PolicyNo;
                                var responseJson = result.ResponseJson;

                                errorSection = "1: Parse CV response";

                                VeriskHistoryRoot deserializedObject = JsonConvert.DeserializeObject<VeriskHistoryRoot>(responseJson);

                                if (deserializedObject?.body is null) continue;
                                var body = deserializedObject.body;
                                tmpLoopTable.Table.Where(c => c.PolicyNo.Equals(policyNo) && c.TmpIndexID == iCount)
                                    .Select(
                                        d =>
                                        {
                                            d.PreviousCompany = Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.carrier.name;
                                            d.EffDate = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.startDate);
                                            d.ExpDate = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.endDate);
                                            d.PreviousCompany2 = Enull(Enull(body.coverageLapseInformation)?.coverageIntervals,1)?.carrier.name;
                                            d.EffDate2 = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals,1)?.startDate);
                                            d.ExpDate2 = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals,1)?.endDate);
                                            d.NumberOfLapseDays = ParseDouble(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.numberOfLapseDays);
                                            d.HasPossibleLapse = ParseChar(Enull(body.coverageLapseInformation)?.hasPossibleLapse);
                                            d.IsCurrentInforceCoverage = ParseChar(Enull(body.coverageLapseInformation)?.isCurrentInforceCoverage);
                                            return d;
                                        }).ToList();

                                iCount++;
                            }

                            errorSection = "1: Store CV response";

                            foreach (var tmpRec in tmpLoopTable.Table)
                            {
                                //  ISNULL((
                                //      ISNULL((DateDiff(d,
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) >= 0 THEN EffDate ELSE EffDate2 END),
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) >= 0 THEN ExpDate ELSE ExpDate2 END))),0)
                                //      +
                                //      ISNULL((DateDiff(d,
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) > 0 THEN EffDate2 ELSE EffDate END),
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) > 0 THEN ExpDate2 ELSE ExpDate END))),0)
                                //      ) / 30
                                //  ),0)
                                //
                                // calcs for CVTermMonths -- implements the above SQL expression
                                var daydiff = DateDiffDaysNull(tmpRec.ExpDate, tmpRec.EffDate2) >= 0;
                                var diffA = DateDiffDaysNull(daydiff ? tmpRec.EffDate : tmpRec.EffDate2, daydiff ? tmpRec.ExpDate : tmpRec.ExpDate2);
                                var diffB = DateDiffDaysNull(daydiff ? tmpRec.EffDate2 : tmpRec.EffDate, daydiff ? tmpRec.ExpDate2 : tmpRec.ExpDate);

                                var sqlParams = new[]
                                {
                                    CreateParamInt("@currID",currId),
                                    CreateParamInt("@CVIndexID",tmpRec.CVIndexID),
                                    CreateParamVarchar("@PolicyNo",tmpRec.PolicyNo,25),
                                    CreateParamVarchar("@ResponseJSON",tmpRec.ResponseJson,-1),
                                    CreateParamVarchar("@PreviousCompany",tmpRec.PreviousCompany,30),
                                    CreateParamDateTime("@EffDate",tmpRec.EffDate),
                                    CreateParamDateTime("@ExpDate",tmpRec.ExpDate),
                                    CreateParamFloat("@NumberOfLapseDays",tmpRec.NumberOfLapseDays),
                                    CreateParamVarchar("@HasPossibleLapse",tmpRec.HasPossibleLapse,1),
                                    CreateParamVarchar("@IsCurrentInforceCoverage",tmpRec.IsCurrentInforceCoverage,1),
                                    CreateParamVarchar("@PreviousCompany2",tmpRec.PreviousCompany2,30),
                                    CreateParamDateTime("@EffDate2",tmpRec.EffDate2),
                                    CreateParamDateTime("@ExpDate2",tmpRec.ExpDate2),
                                    CreateParamInt("@CVTermMonths", (diffA ?? 0) + (diffB ?? 0) / 30)
                                };
                                ExecuteNonQueryWithParameters(RateSpartan_CVData.InsertIntoRatingCV, sqlParams, conn);
                            }

                        } // quoted != null
                    } // conn

                    txScope.Complete();
                }

            }
            catch (Exception ex)
            {
                TxLogError(guid, currId.ToString(), userName, DateTime.Now, "RateSpartan_CVData",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// 
        ///
        /// 
        /// Migration of stored procedure spUW_RateSpartan_CVData
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void TxRateSpartan_CVData_AA20210412(int currId, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            var errorSection = "-1: Startup";
            userName = userName ?? "ApplicationUser";
            int ratingCVIndexID = 0;

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        //---------------------------------------------
                        errorSection = "0: Pull CV JSON from Quote";

                        if (quoteId != null)
                        {
                            int iCount;
                            string policyNo;

                            var tmpLoopTable = new TmpLoopTable();

                            var historyList =
                                ExecuteQueryWithParameters(RateSpartan_CVData.GetVeriskHistory, new[] { CreateParamVarchar("@QuoteID", quoteId, 20) }, conn);
                            foreach (var rec in historyList)
                            {
                                var values = (object[])rec;
                                tmpLoopTable.AddRecord(new TmpLoopTableRecord
                                {
                                    CVIndexID = GetDbValOrNull<int>(values[0]),
                                    PolicyNo = GetDbRefValOrNull<string>(values[1]),
                                    ResponseJson = GetDbRefValOrNull<string>(values[2]),
                                });
                            }

                            var maxCount = tmpLoopTable.Table.Select(c => c.TmpIndexID).DefaultIfEmpty(0).Max();
                            iCount = 1;

                            while (iCount <= maxCount)
                            {
                                var result = tmpLoopTable.Table.Where(c => c.TmpIndexID == iCount)
                                    .Select(c => new
                                    {
                                        c.PolicyNo,
                                        c.ResponseJson
                                    }).FirstOrDefault();
                                if (result == null) continue;
                                policyNo = result.PolicyNo;
                                var responseJson = result.ResponseJson;

                                errorSection = "1: Parse CV response";

                                VeriskHistoryRoot deserializedObject = JsonConvert.DeserializeObject<VeriskHistoryRoot>(responseJson);

                                if (deserializedObject?.body is null) continue;
                                var body = deserializedObject.body;
                                tmpLoopTable.Table.Where(c => c.PolicyNo.Equals(policyNo) && c.TmpIndexID == iCount)
                                    .Select(
                                        d =>
                                        {
                                            d.PreviousCompany = Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.carrier.name;
                                            d.EffDate = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.startDate);
                                            d.ExpDate = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.endDate);
                                            d.PreviousCompany2 = Enull(Enull(body.coverageLapseInformation)?.coverageIntervals, 1)?.carrier.name;
                                            d.EffDate2 = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals, 1)?.startDate);
                                            d.ExpDate2 = ParseDate(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals, 1)?.endDate);
                                            d.NumberOfLapseDays = ParseDouble(Enull(Enull(body.coverageLapseInformation)?.coverageIntervals)?.numberOfLapseDays);
                                            d.HasPossibleLapse = ParseChar(Enull(body.coverageLapseInformation)?.hasPossibleLapse);
                                            d.IsCurrentInforceCoverage = ParseChar(Enull(body.coverageLapseInformation)?.isCurrentInforceCoverage);
                                            return d;
                                            // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
                                        }).ToList();

                                iCount++;
                            }

                            //---------------------------------------------
                            errorSection = "1: Store CV response";

                            foreach (var tmpRec in tmpLoopTable.Table)
                            {
                                //  ISNULL((
                                //      ISNULL((DateDiff(d,
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) >= 0 THEN EffDate ELSE EffDate2 END),
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) >= 0 THEN ExpDate ELSE ExpDate2 END))),0)
                                //      +
                                //      ISNULL((DateDiff(d,
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) > 0 THEN EffDate2 ELSE EffDate END),
                                //          (CASE WHEN DateDiff(d,ExpDate,EffDate2) > 0 THEN ExpDate2 ELSE ExpDate END))),0)
                                //      ) / 30
                                //  ),0)
                                //
                                // calcs for CVTermMonths -- implements the above SQL expression
                                var daydiff = DateDiffDaysNull(tmpRec.ExpDate, tmpRec.EffDate2) >= 0;
                                var diffA = DateDiffDaysNull(daydiff ? tmpRec.EffDate : tmpRec.EffDate2, daydiff ? tmpRec.ExpDate : tmpRec.ExpDate2);
                                var diffB = DateDiffDaysNull(daydiff ? tmpRec.EffDate2 : tmpRec.EffDate, daydiff ? tmpRec.ExpDate2 : tmpRec.ExpDate);

                                var sqlParams = new[]
                                {
                                    CreateParamInt("@currID",currId),
                                    CreateParamInt("@CVIndexID",tmpRec.CVIndexID),
                                    CreateParamVarchar("@PolicyNo",tmpRec.PolicyNo,25),
                                    CreateParamVarchar("@ResponseJSON",tmpRec.ResponseJson,-1),
                                    CreateParamVarchar("@PreviousCompany",tmpRec.PreviousCompany,30),
                                    CreateParamDateTime("@EffDate",tmpRec.EffDate),
                                    CreateParamDateTime("@ExpDate",tmpRec.ExpDate),
                                    CreateParamFloat("@NumberOfLapseDays",tmpRec.NumberOfLapseDays),
                                    CreateParamVarchar("@HasPossibleLapse",tmpRec.HasPossibleLapse,1),
                                    CreateParamVarchar("@IsCurrentInforceCoverage",tmpRec.IsCurrentInforceCoverage,1),
                                    CreateParamVarchar("@PreviousCompany2",tmpRec.PreviousCompany2,30),
                                    CreateParamDateTime("@EffDate2",tmpRec.EffDate2),
                                    CreateParamDateTime("@ExpDate2",tmpRec.ExpDate2),
                                    CreateParamInt("@CVTermMonths", (diffA ?? 0) + (diffB ?? 0) / 30)
                                };
                                // run query which also returns key of record just inserted into RatingCV
                                ratingCVIndexID =
                                    ConvertScopeIdentity(ExecuteScalarWithParameters(
                                        RateSpartan_CVData_AA20210412.InsertIntoRatingCVKeyBack, sqlParams, conn)) ??
                                    throw new StoredProcedureException("Failed insert into RatingCV");
                            }

                        
                            //---------------------------------------------
                            errorSection = "2: Update Quote Values";

                            ExecuteNonQueryWithParameters(RateSpartan_CVData_AA20210412.UpdatePolicyQuote,
                                new[] { CreateParamVarchar("@QuoteID", quoteId, 50) }, conn);

                            var sqlParams2 = new[]
                            {
                                CreateParamVarchar("@QuoteID", quoteId, 50),
                                CreateParamInt("@RatingCVIndexID",ratingCVIndexID)
                            };
                            ExecuteNonQueryWithParameters(RateSpartan_CVData_AA20210412.UpdatePolicyQuoteJoinRatingCV,
                                sqlParams2, conn);

                            ExecuteNonQueryWithParameters(RateSpartan_CVData_AA20210412.UpdateRatingMain,
                                new[] { CreateParamInt("@currID", currId) }, conn);


                        } // quoted != null
                    } // conn

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, currId.ToString(), userName, DateTime.Now, "RateSpartan_CVData_AA20210412",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Inserts Relativity into AA Table
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// TotalRelativities_AA20210412.
        /// 
        /// Migration of stored procedure spUW_TotalRelativities_AA20210412
        /// </summary>
        /// <param name="suffix"></param>
        /// <param name="mainIndexId"></param>
        /// <param name="vehIndexId"></param>
        /// <param name="ratedOprIndexId"></param>
        /// <param name="relativity"></param>
        public void TxTotalRelativitiesBase(string suffix, int mainIndexId, int vehIndexId = 0,
            int ratedOprIndexId = 0,
            string relativity = "")
        {
            var rateOrderSets = new List<int[]>()
            {
                new[] { 27, 29 },
                new[] { 25, 27 }
            };

            var rateOrders = string.IsNullOrEmpty(suffix)
                ? rateOrderSets[1]
                : string.Equals(suffix,
                    "AA20210412")
                    ? rateOrderSets[0]
                    : throw new StoredProcedureException($"Invalid suffix to TotalRelativities \"{suffix}\"");


            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        switch (relativity)
                        {
                            case "Total Discount Factor":
                            case "Total Surcharge Factor":
                                HandleRelativityCasesGroup1(relativity, mainIndexId, vehIndexId, ratedOprIndexId, conn);
                                break;

                            case "Without Fee":
                            case "Annualized Vehicle Premium":
                            case "Vehicle Premium":
                                HandleRelativityCasesGroup2(relativity, mainIndexId, vehIndexId, ratedOprIndexId, rateOrders,
                                    conn);
                                break;

                            case "Annualized Policy":
                                HandleRelativityCasesGroup3(relativity, mainIndexId, vehIndexId, ratedOprIndexId, conn);
                                break;

                            case "Policy Premium":
                                HandleRelativityCasesGroup4(relativity, mainIndexId, conn);
                                break;

                            default:
                                throw new StoredProcedureException($"Invalid relativity case \"{relativity}\"");
                        }
                    }
                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError("000", string.Empty, "RATING", DateTime.Now, $"TotalRelativities{suffix}",
                    ex.HResult.ToString(), $"Update RatingRelativities {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Copy Policy to PolicyRenQuote for Renewal Quote Issuance
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// TransferPolicyToPolicyRenQuote.
        /// 
        /// Migration of stored procedure spUW_TranferPolicyToPolicyRenQuote[sic]
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxTransferPolicyToPolicyRenQuote(string policyNo, string userName, string guid)
        {
            var errorSection = "0: Start";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Policy Record";

                        ExecuteNonQueryWithPolicyNo(TransferPolicyToPolicyRenQuote.PolicyRecord, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: Policy Cars Record";

                        ExecuteNonQueryWithPolicyNo(TransferPolicyToPolicyRenQuote.PolicyCarsRecord, policyNo, conn);

                        var results =
                            ExecuteQueryWithPolicyNo(TransferPolicyToPolicyRenQuote.GetRatePlanInfo, policyNo, conn);
                        var values = (object[])results.First();
                        //var code =
                        //    ConvertFromDbValOrException<string>(values[0], $"Code null for policy # {policyNo}");
                        var eft =
                            ConvertFromDbValOrException<double>(values[1], $"EFT null for policy # {policyNo}");
                        var dpAsPerc =
                            ConvertFromDbValOrException<double>(values[2], $"DP as Perc null for policy # {policyNo}");

                        using (var cmd = new SqlCommand(TransferPolicyToPolicyRenQuote.UpdatePayPlan,conn))
                        {
                            cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 50));
                            cmd.Parameters.Add(CreateParamFloat("@EFT", eft));
                            cmd.Parameters.Add(CreateParamFloat("@DP_AsPerc", dpAsPerc));
                        }

                        // ------------------------------------------------------------------
                        errorSection = "3: Policy Drivers Records";

                        ExecuteNonQueryWithPolicyNo(TransferPolicyToPolicyRenQuote.PolicyDriversRecords, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "4: Policy Violations Records";

                        ExecuteNonQueryWithPolicyNo(TransferPolicyToPolicyRenQuote.PolicyViolationsRecords, policyNo, conn);

                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "TransferPolicyToPolicyRenQuote",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Copy PolicyRenQuote to Binders for Renewal Policy Issuance
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// TransferRenQuoteToPolicyBinders.
        /// 
        /// Migration of stored procedure spUW_TranferRenQuoteToPolicyBinders[sic]
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxTransferRenQuoteToPolicyBinders(string policyNo, string userName, string guid)
        {
            var errorSection = "0: Start";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Policy Record";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToPolicyBinders.TransferReqQuoteToPolicyBinders_Insert, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: Policy Cars Record";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToPolicyBinders.TransferReqQuoteToPolicyBinders_InsertCars, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "3: Policy Drivers Records";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToPolicyBinders.TransferReqQuoteToPolicyBinders_InsertDrivers, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "4: Policy Violations Records";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToPolicyBinders.TransferReqQuoteToPolicyBinders_InsertDriversViolations, policyNo, conn);

                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "TransferRenQuoteToPolicyBinders",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Copies records for given Policy # from PolicyRenQuote{type} to PolicyRenQuoteTemp{type}
        /// 
        /// Where {type} is regular(blank), Cars, Drivers, and DriversViolations
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// TransferRenQuoteToRenQuoteTempByPolicyNo.
        /// 
        /// Migrated from spUW_TransferRenQuoteToRenQuoteTemp
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TxTransferRenQuoteToRenQuoteTempByPolicyNo(string policyNo, string userName, string guid)
        {
            var errorSection = "0: Start";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();

                        // ------------------------------------------------------------------
                        errorSection = "1: Policy Record";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToRenQuoteTemp.Copy, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "2: Policy Cars Record";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToRenQuoteTemp.CopyCars, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "3: Policy Drivers Records";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToRenQuoteTemp.CopyDrivers, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "4: Policy Violations Records";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToRenQuoteTemp.CopyDriversViolations, policyNo, conn);

                        // ------------------------------------------------------------------
                        errorSection = "5: RenQuoteInstallments";

                        ExecuteNonQueryWithPolicyNo(TransferRenQuoteToRenQuoteTemp.CopyInstallments, policyNo, conn);

                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "TransferRenQuoteToRenQuoteTemp",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        /// <summary>
        /// Loads the Audit Table for Installment Fees.
        /// 
        /// Performs the code found in the transaction wrapping most of the method
        /// UpdateInstallmentAudit.
        /// 
        /// Migration of stored procedure spMAINT_UpdateInstallmentAudit
        /// </summary>
        /// <param name="userName"></param>
        public void TxUpdateInstallmentAudit(string userName = "SYSTEM")
        {
            string errorSection = "0: Startup.";

            try
            {
                using (var txScope = new TransactionScope())
                {
                    using (var conn = GetConnection())
                    {
                        conn.Open();
                        // Insert new Installment Fees where Fee Index ID does not exists

                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertInstallmentFee,conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertCreditCardFee, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertNSFFee, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertLateFee, conn);


                        // Insert Installment Fee where Fee set to ignore and Amt not $0 in Audit

                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertInstallmentFeeIgnore, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertCreditCardFeeIgnore, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertNSFFeeIgnore, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertLateFeeIgnore, conn);

                        // Insert Installment Fee where Fee = 0 and no longer set to Ignore

                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertInstallmentFeeIgnoreUndone, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertCreditCardFeeIgnoreUndone, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertNSFFeeIgnoreUndone, conn);
                        ExecuteNonQueryNoArgs(UpdateInstallmentAudit.InsertLateFeeIgnoreUndone, conn);
                    }

                    txScope.Complete();
                }
            }
            catch (Exception ex)
            {
                TxLogError("000", "REPORT", userName, DateTime.Now, "UpdateInstallmentAudit",
                    ex.HResult.ToString(), $"{errorSection};{ex.Message ?? string.Empty}");
                throw;
            }
        }

        public bool TxValidBusinessDay(DateTime dayToValidate)
        {
            var day = dayToValidate.DayOfWeek;
            if (day == DayOfWeek.Saturday || day == DayOfWeek.Sunday)
                return false;

            return !TxIsHoliday(dayToValidate);
        }


        #region Extracted Transaction Methods

        private decimal InTxCopyPolicyToPolicyEndorseQuoteByPolicyNo(string policyNo, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(CopyPolicyToPolicyEndorseQuoteTables.InsertPlain, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 50));

                var result = cmd.ExecuteScalar();

                if (result == null || result == DBNull.Value)
                    throw new StoredProcedureException(
                        "Unable to retrieve key to inserted PolicyEndorseQuote record");
                return (decimal)result;
            }
        }

        private object InTxGetCountOfPolicyBinderRecordsByPolicyNo(string policyNo, SqlConnection conn)
        {
            return ExecuteScalarWithPolicyNo(Queries.PolicyBinders_CountByPolicyNo, policyNo, conn);
        }

        private object InTxGetCountOfPolicyRenQuoteRecordsByPolicyNo(string policyNo, SqlConnection conn)
        {
            return ExecuteScalarWithPolicyNo(Queries.PolicyRenQuote_CountByPolicyNo, policyNo, conn);
        }

        private object InTxGetDownpaymentAsPercentageFromRatePayPlans(int payPlan, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(MinimumEndorsementPaymentDue.DownpaymentAsPercentage, conn))
            {
                cmd.Parameters.Add(CreateParamInt("@PayPlan", payPlan));

                return cmd.ExecuteScalar();
            }
        }

        private object InTxGetDueDateNextInstallment(string policyNo, DateTime endorsementDate, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(MinimumEndorsementPaymentDue.GetDueDateNextInstallment, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd.Parameters.Add(CreateParamDateTime("@EndorsementDate", endorsementDate));

                return cmd.ExecuteScalar();
            }
        }

        private int InTxInsertAuditMinimumDueEndorsement(string policyNo, DateTime endorsementDate,
            decimal fullAmountOfEndorsement,
            string userName, DateTime effDate, DateTime expDate, int payPlan, double dpAsPcnt, int? daysPolicyRemaining,
            double? endorsementRunRate, DateTime? dueDateNextInstallment, int? daysUntilNextPaymentDue, decimal minimumAmountDue,
            SqlConnection conn)
        {
            using (var cmd = new SqlCommand(MinimumEndorsementPaymentDue.InsertAuditMinimumDueEndorsement, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 50));
                cmd.Parameters.Add(CreateParamDateTime("@EndorsementDate", endorsementDate));
                cmd.Parameters.Add(CreateParamMoney("@FullAmountOfEndorsement", fullAmountOfEndorsement));
                cmd.Parameters.Add(CreateParamVarchar("@UserName", userName, 50));
                cmd.Parameters.Add(CreateParamDateTime("@EffDate", effDate));
                cmd.Parameters.Add(CreateParamDateTime("@ExpDate", expDate));
                cmd.Parameters.Add(CreateParamInt("@PayPlan", payPlan));
                cmd.Parameters.Add(CreateParamFloat("@DP_AsPerc", dpAsPcnt));
                cmd.Parameters.Add(CreateParamInt("@DaysPolicyRemaining", daysPolicyRemaining));
                cmd.Parameters.Add(CreateParamFloat("@EndorsementRunRate", endorsementRunRate));
                cmd.Parameters.Add(CreateParamDateTime("@DueDateNextInstallment", dueDateNextInstallment));
                cmd.Parameters.Add(CreateParamInt("@DaysUntilNextPaymentDue", daysUntilNextPaymentDue));
                cmd.Parameters.Add(CreateParamMoney("@MinimumAmountDue", minimumAmountDue));

                return cmd.ExecuteNonQuery();
            }
        }

        private object InTxInstallNoCurrentGetInstallNoByMailDate(string policyNo, DateTime mailDate, int totalMailDays, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(InstallNoCurrent.GetInstallNo, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd.Parameters.Add(CreateParamDateTime("@MailDate", mailDate));
                cmd.Parameters.Add(CreateParamInt("@TotalMailDays", totalMailDays));

                return cmd.ExecuteScalar();
            }
        }

        private object InTxInstallPaymentApplyGetInstallNo(string policyNo, DateTime effDate, DateTime ccFeeDate,
            SqlConnection conn)
        {
            using (var cmd = new SqlCommand(InstallmentPaymentApply.GetInstallNo, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd.Parameters.Add(CreateParamDateTime("@EffDate", effDate));
                cmd.Parameters.Add(CreateParamDateTime("@CCFeeDate", ccFeeDate));

                return cmd.ExecuteScalar();
            }
        }

        private int InTxInstallPaymentApplyInsertCreditCardFees(string policyNo, decimal ccInstallNo,
            double installCount,
            int payPlan, DateTime ccFeeDate, decimal policyWritten, decimal ccFeeAmt, string userName,
            SqlConnection conn)
        {
            using (var cmd = new SqlCommand(InstallmentPaymentApply.InsertCreditCardFees, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd.Parameters.Add(CreateParamDecimal("@CCInstallNo", ccInstallNo, 5, 1));
                cmd.Parameters.Add(CreateParamSmallInt("@InstallCount", (int)installCount));
                cmd.Parameters.Add(CreateParamInt("@PayPlan", payPlan));
                cmd.Parameters.Add(CreateParamDateTime("@CCFeeDate", ccFeeDate));
                cmd.Parameters.Add(CreateParamMoney("@PolicyWritten", policyWritten));
                cmd.Parameters.Add(CreateParamDecimal("@CCFeeAmt", ccFeeAmt));
                cmd.Parameters.Add(CreateParamVarchar("@UserName", userName, 25));

                return cmd.ExecuteNonQuery();
            }
        }

        private bool InTxIsHoliday(DateTime dayToValidate, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(Queries.Holidays_CountByDate, conn))
            {
                cmd.Parameters.Add(CreateParamDateTime("@DayToValidate", dayToValidate));

                var cnt = (int)cmd.ExecuteScalar();
                return cnt > 0;
            }
        }

        private int InTxMoveMoneyInsertFrom(string moveFromPolicyNo, string moveToPolicyNo, decimal amount, string userName, DateTime? displayDate, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(MoveMoney.MoveMoney_InsertFrom, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@MoveFromPolicyNo", moveFromPolicyNo, 25));
                cmd.Parameters.Add(CreateParamVarchar("@MoveToPolicyNo", moveToPolicyNo, 25));
                cmd.Parameters.Add(CreateParamFloat("@Amount", (double)amount));
                cmd.Parameters.Add(CreateParamVarchar("@UserName", userName, 50));
                cmd.Parameters.Add(CreateParamDateTime("@DisplayDate", displayDate));

                return cmd.ExecuteNonQuery();
            }
        }

        private int InTxMoveMoneyInsertTo(string moveFromPolicyNo, string moveToPolicyNo, decimal amount, string userName, DateTime? displayDate, SqlConnection conn)
        {
            using (var cmd = new SqlCommand(MoveMoney.MoveMoney_InsertTo, conn))
            {
                cmd.Parameters.Add(CreateParamVarchar("@MoveFromPolicyNo", moveFromPolicyNo, 25));
                cmd.Parameters.Add(CreateParamVarchar("@MoveToPolicyNo", moveToPolicyNo, 25));
                cmd.Parameters.Add(CreateParamFloat("@Amount", (double)amount));
                cmd.Parameters.Add(CreateParamVarchar("@UserName", userName, 50));
                cmd.Parameters.Add(CreateParamDateTime("@DisplayDate", displayDate));

                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Clears PolicyEndorseQuote tables by PolicyNo
        ///
        /// reg, cars, drivers, and drivers violations
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="conn"></param>
        private static void ClearPolicyEndorseQuote(string policyNo, SqlConnection conn)
        {

            // ------------------------------------------------------------------
            var errorSection = "1: Policy Record";

            ExecuteNonQueryWithPolicyNo(
                StoredProcedureQueries.ClearPolicyEndorseQuote.DeletePolicyEndorseQuoteByPolicyNo, policyNo, conn);

            // ------------------------------------------------------------------
            errorSection = "2: Policy Cars Record";

            ExecuteNonQueryWithPolicyNo(
                StoredProcedureQueries.ClearPolicyEndorseQuote.DeletePolicyEndorseQuoteCarsByPolicyNo, policyNo, conn);

            // ------------------------------------------------------------------
            errorSection = "3: Policy Drivers Records";

            ExecuteNonQueryWithPolicyNo(
                StoredProcedureQueries.ClearPolicyEndorseQuote.DeletePolicyEndorseQuoteDriversByPolicyNo, policyNo,
                conn);

            // ------------------------------------------------------------------
            errorSection = "4: Policy Violations Records";

            ExecuteNonQueryWithPolicyNo(
                StoredProcedureQueries.ClearPolicyEndorseQuote.DeletePolicyEndorseQuoteDriversViolationsByPolicyNo,
                policyNo,
                conn);
        }

        /// <summary>
        /// Runs first query -- if not-zero result, runs second query
        ///
        /// In practice, it is used as: if any records exists (by PolicyNo), delete them
        ///
        /// </summary>
        /// <param name="countQuery"></param>
        /// <param name="deleteQuery"></param>
        /// <param name="policyNo"></param>
        /// <param name="conn"></param>
        private static void DeleteIfAny(string countQuery, string deleteQuery, string policyNo, SqlConnection conn)
        {
            var cnt = (int)ExecuteScalarWithPolicyNo(countQuery, policyNo, conn);
            if (cnt > 0)
            {
                ExecuteNonQueryWithPolicyNo(deleteQuery, policyNo, conn);
            }
        }

        private int GetCountOfPolicyRecordsByPolicyNo(string policyNo)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                return (int)ExecuteScalarWithPolicyNo(Queries.Policy_CountByPolicyNo, policyNo, conn);
            }
        }

        private int GetCountOfPolicyOidRecordsByPolicyNo(string policyNo)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                return (int)ExecuteScalarWithPolicyNo(Queries.Policy_CountOIDByPolicyNo, policyNo, conn);
            }
        }

        private int GetCountOfPolicyBinderRecordsByPolicyNo(string policyNo)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                return (int)InTxGetCountOfPolicyBinderRecordsByPolicyNo(policyNo, conn);
            }
        }

        private int GetCountOfPolicyRenQuoteRecordsByPolicyNo(string policyNo)
        {
            using (var conn = GetConnection())
            {
                conn.Open();
                return (int)InTxGetCountOfPolicyRenQuoteRecordsByPolicyNo(policyNo, conn);
            }
        }

        /// <summary>
        /// In Total_Relativities_AA20210412, handles first group
        /// of relativity cases.
        ///
        /// The first group contains two cases at this time -- they
        /// are "Total Discount Factor" and "Total Surcharge Factor"
        /// </summary>
        /// <param name="relativity"></param>
        /// <param name="mainIndexId"></param>
        /// <param name="vehIndexId"></param>
        /// <param name="ratedOprIndexId"></param>
        /// <param name="conn"></param>
        private static void HandleRelativityCasesGroup1(string relativity, int mainIndexId, int vehIndexId,
          int ratedOprIndexId, SqlConnection conn)
        {
            double? BI = 1;
            double? PIPNI = 1;
            double? PIPNR = 1;
            double? PD = 1;
            double? UMSC = 1;
            double? UMMC = 1;
            double? COMP = 1;
            double? COLL = 1;
            const int precision = 8;

            var sqlParams = new[]
            {
                CreateParamInt("@MainIndexID", mainIndexId),
                CreateParamInt("@VehIndexID", vehIndexId),
                CreateParamInt("@RatedOprIndexID", ratedOprIndexId),
                CreateParamVarchar("@Type", RelativitiesCaseSelects[relativity], 40)
            };
            var resultList = ExecuteQueryWithParameters(RelativitiesCaseSelectQueries[relativity],
                sqlParams,
                conn);

            var tmpRelativities = new TmpRelativities();

            SaveRelativitiesToTempTable(resultList, tmpRelativities);

            var currIndex = 1;
            var maxIndex = tmpRelativities.Table.Max(c => c.IndexID);

            while (currIndex <= maxIndex)
            {
                var rec = tmpRelativities.Table.Find(c => c.IndexID == currIndex);
                BI *= rec.BI;
                PIPNI *= rec.PIPNI;
                PIPNR *= rec.PIPNR;
                PD *= rec.PD;
                UMSC *= rec.UMSC;
                UMMC *= rec.UMMC;
                COMP *= rec.COMP;
                COLL *= rec.COLL;

                currIndex++;
            }

            sqlParams = new[]
            {
                CreateParamInt("@MainIndexID", mainIndexId),
                CreateParamInt("@VehIndexID", vehIndexId),
                CreateParamInt("@RatedOprIndexID", ratedOprIndexId),
                CreateParamFloat("@BI", BI),
                CreateParamFloat("@PIPNI", PIPNI),
                CreateParamFloat("@PIPNR", PIPNR),
                CreateParamFloat("@PD", PD),
                CreateParamFloat("@UMSC", UMSC),
                CreateParamFloat("@UMMC", UMMC),
                CreateParamFloat("@COMP", COMP),
                CreateParamFloat("@COLL", COLL),
                CreateParamInt("@Precision",precision),
                CreateParamVarchar("@Relativity", RelativitiesCaseUpdates[relativity], 40),
            };
            ExecuteNonQueryWithParameters(RelativitiesCaseUpdatetQueries[relativity], sqlParams,
                conn);
        }

        /// <summary>
        /// In Total_Relativities_AA20210412, handles second group
        /// of relativity cases.
        /// 
        /// The second group contains three cases at this time -- they
        /// are "Without Fee", "Annualized Vehicle Premium", and "Vehicle Premium"
        /// </summary>
        /// <param name="relativity"></param>
        /// <param name="mainIndexId"></param>
        /// <param name="vehIndexId"></param>
        /// <param name="ratedOprIndexId"></param>
        /// <param name="rateOrders"></param>
        /// <param name="conn"></param>
        private static void HandleRelativityCasesGroup2(string relativity,
         int mainIndexId, int vehIndexId, int ratedOprIndexId, IReadOnlyList<int> rateOrders, SqlConnection conn)
        {
            double? BI = 0D;
            double? PIPNI = 0D;
            double? PIPNR = 0D;
            double? PD = 0D;
            double? UMSC = 0D;
            double? UMMC = 0D;
            double? COMP = 0D;
            double? COLL = 0D;

            var sqlParams = new List<SqlParameter>
            {
                CreateParamInt("@MainIndexID", mainIndexId),
                CreateParamInt("@VehIndexID", vehIndexId),
                CreateParamInt("@RatedOprIndexID", ratedOprIndexId)
            };
            if (string.Equals(relativity, "Without Fee") || string.Equals(relativity, "Annualized Vehicle Premium"))
            {
                sqlParams.Add(CreateParamInt("@RateOrderNo",rateOrders[0]));
            }
            if (string.Equals(relativity, "Annualized Vehicle Premium"))
            {
                sqlParams.Add(CreateParamInt("@RateOrderNoMax", rateOrders[1]));
            }
            var resultList = ExecuteQueryWithParameters(RelativitiesCaseSelectQueries[relativity], sqlParams.ToArray(), conn);

            var tmpRelativities = new TmpRelativities();

            SaveRelativitiesToTempTable(resultList,tmpRelativities);

            var currIndex = 1;
            var maxIndex = tmpRelativities.Table.Max(c => c.IndexID);

            while (currIndex <= maxIndex)
            {
                var temp = tmpRelativities.Table.Where(c => c.IndexID == currIndex).Select(c => new
                {
                    c.MathAction,
                    c.RoundStep
                }).First();
                var mathAction = temp.MathAction;
                var precision = temp.RoundStep ?? 0;

                switch (mathAction)
                {
                    case "+":
                        var add = tmpRelativities.Table.Where(c => c.IndexID == currIndex).Select(c => new
                        {
                            BI = DoubleRound(c.BI + BI, precision),
                            PIPNI = DoubleRound(c.PIPNI + PIPNI, precision),
                            PIPNR = DoubleRound(c.PIPNR + PIPNR, precision),
                            PD = DoubleRound(c.PD + PD, precision),
                            UMSC = DoubleRound(c.UMSC + UMSC, precision),
                            UMMC = DoubleRound(c.UMMC + UMMC, precision),
                            COMP = DoubleRound(c.COMP + COMP, precision),
                            COLL = DoubleRound(c.COLL + COLL, precision)
                        }).First();
                        BI = add.BI;
                        PIPNI = add.PIPNI;
                        PIPNR = add.PIPNR;
                        PD = add.PD;
                        UMSC = add.UMSC;
                        UMMC = add.UMMC;
                        COMP = add.COMP;
                        COLL = add.COLL;
                        break;

                    case "*":
                        var mult = tmpRelativities.Table.Where(c => c.IndexID == currIndex).Select(c => new
                        {
                            BI = DoubleRound(c.BI * BI, precision),
                            PIPNI = DoubleRound(c.PIPNI * PIPNI, precision),
                            PIPNR = DoubleRound(c.PIPNR * PIPNR, precision),
                            PD = DoubleRound(c.PD * PD, precision),
                            UMSC = DoubleRound(c.UMSC * UMSC, precision),
                            UMMC = DoubleRound(c.UMMC * UMMC, precision),
                            COMP = DoubleRound(c.COMP * COMP, precision),
                            COLL = DoubleRound(c.COLL * COLL, precision)
                        }).First();
                        BI = mult.BI;
                        PIPNI = mult.PIPNI;
                        PIPNR = mult.PIPNR;
                        PD = mult.PD;
                        UMSC = mult.UMSC;
                        UMMC = mult.UMMC;
                        COMP = mult.COMP;
                        COLL = mult.COLL;
                        break;

                    case "=":
                        sqlParams = new List<SqlParameter>
                        {
                            CreateParamInt("@MainIndexID", mainIndexId),
                            CreateParamInt("@VehIndexID", vehIndexId),
                            CreateParamInt("@RatedOprIndexID", ratedOprIndexId),
                            CreateParamFloat("@BI", BI),
                            CreateParamFloat("@PIPNI", PIPNI),
                            CreateParamFloat("@PIPNR", PIPNR),
                            CreateParamFloat("@PD", PD),
                            CreateParamFloat("@UMSC", UMSC),
                            CreateParamFloat("@UMMC", UMMC),
                            CreateParamFloat("@COMP", COMP),
                            CreateParamFloat("@COLL", COLL),
                            CreateParamInt("@Precision",precision),
                            CreateParamVarchar("@UpdateType", RelativitiesCaseUpdates[relativity], 40),
                        };
                        ExecuteNonQueryWithParameters(RelativitiesCaseUpdatetQueries[relativity], sqlParams.ToArray(),
                            conn);
                        break;

                    default:
                        throw new StoredProcedureException($"Invalid math action \"{mathAction}\"");
                }

                currIndex++;
            }
        }

        /// <summary>
        /// In Total_Relativities_AA20210412, handles third group
        /// of relativity cases.
        ///
        /// The third group contains only one case at this time -- it
        /// is "Annualized Policy"
        /// </summary>
        /// <param name="relativity"></param>
        /// <param name="mainIndexId"></param>
        /// <param name="vehIndexId"></param>
        /// <param name="ratedOprIndexId"></param>
        /// <param name="conn"></param>
        private static void HandleRelativityCasesGroup3(string relativity,
        int mainIndexId, int vehIndexId, int ratedOprIndexId, SqlConnection conn)
        {
            double? BI = 0D;
            double? PIPNI = 0D;
            double? PIPNR = 0D;
            double? PD = 0D;
            double? UMSC = 0D;
            double? UMMC = 0D;
            double? COMP = 0D;
            double? COLL = 0D;
            var precision = 0;

            var sqlParams = new[]
            {
                CreateParamInt("@MainIndexID", mainIndexId),
                CreateParamInt("@VehIndexID", vehIndexId),
                CreateParamInt("@RatedOprIndexID", ratedOprIndexId)
            };
            var resultList = ExecuteQueryWithParameters(RelativitiesCaseSelectQueries[relativity], sqlParams, conn);

            var tmpRelativities = new TmpRelativities();

            SaveRelativitiesToTempTable(resultList, tmpRelativities);

            var currIndex = 1;
            var maxIndex = tmpRelativities.Table.Max(c => c.IndexID);

            while (currIndex <= maxIndex)
            {
                var add = tmpRelativities.Table.Where(c => c.IndexID == currIndex).Select(c => new
                {
                    BI = DoubleRound(c.BI + BI, precision),
                    PIPNI = DoubleRound(c.PIPNI + PIPNI, precision),
                    PIPNR = DoubleRound(c.PIPNR + PIPNR, precision),
                    PD = DoubleRound(c.PD + PD, precision),
                    UMSC = DoubleRound(c.UMSC + UMSC, precision),
                    UMMC = DoubleRound(c.UMMC + UMMC, precision),
                    COMP = DoubleRound(c.COMP + COMP, precision),
                    COLL = DoubleRound(c.COLL + COLL, precision)
                }).First();
                BI = add.BI;
                PIPNI = add.PIPNI;
                PIPNR = add.PIPNR;
                PD = add.PD;
                UMSC = add.UMSC;
                UMMC = add.UMMC;
                COMP = add.COMP;
                COLL = add.COLL;

                currIndex++;
            }

            sqlParams = new[]
            {
                CreateParamInt("@MainIndexID", mainIndexId),
                CreateParamFloat("@BI", BI),
                CreateParamFloat("@PIPNI", PIPNI),
                CreateParamFloat("@PIPNR", PIPNR),
                CreateParamFloat("@PD", PD),
                CreateParamFloat("@UMSC", UMSC),
                CreateParamFloat("@UMMC", UMMC),
                CreateParamFloat("@COMP", COMP),
                CreateParamFloat("@COLL", COLL)
            };
            ExecuteNonQueryWithParameters(RelativitiesCaseUpdatetQueries[relativity], sqlParams,
                conn);
        }

        /// <summary>
        /// In Total_Relativities_AA20210412, handles fourth group
        /// of relativity cases.
        ///
        /// The fourth group contains only one case at this time -- it
        /// is "Policy Premium"
        /// </summary>
        /// <param name="relativity"></param>
        /// <param name="mainIndexId"></param>
        /// <param name="conn"></param>
        private static void HandleRelativityCasesGroup4(string relativity,
       int mainIndexId, SqlConnection conn)
        {
            double? BI = 0D;
            double? PIPNI = 0D;
            double? PIPNR = 0D;
            double? PD = 0D;
            double? UMSC = 0D;
            double? UMMC = 0D;
            double? COMP = 0D;
            double? COLL = 0D;
            var precision = 0;

            var sqlParams = new[]
            {
                CreateParamInt("@MainIndexID", mainIndexId)
            };
            var resultList = ExecuteQueryWithParameters(RelativitiesCaseSelectQueries[relativity], sqlParams, conn);

            var tmpRelativities = new TmpRelativities();

            SaveRelativitiesToTempTable(resultList, tmpRelativities);

            var currIndex = 1;
            var maxIndex = tmpRelativities.Table.Max(c => c.IndexID);

            while (currIndex <= maxIndex)
            {
                var add = tmpRelativities.Table.Where(c => c.IndexID == currIndex).Select(c => new
                {
                    BI = DoubleRound(c.BI + BI, precision),
                    PIPNI = DoubleRound(c.PIPNI + PIPNI, precision),
                    PIPNR = DoubleRound(c.PIPNR + PIPNR, precision),
                    PD = DoubleRound(c.PD + PD, precision),
                    UMSC = DoubleRound(c.UMSC + UMSC, precision),
                    UMMC = DoubleRound(c.UMMC + UMMC, precision),
                    COMP = DoubleRound(c.COMP + COMP, precision),
                    COLL = DoubleRound(c.COLL + COLL, precision)
                }).First();
                BI = add.BI;
                PIPNI = add.PIPNI;
                PIPNR = add.PIPNR;
                PD = add.PD;
                UMSC = add.UMSC;
                UMMC = add.UMMC;
                COMP = add.COMP;
                COLL = add.COLL;

                currIndex++;
            }

            sqlParams = new[]
            {
                CreateParamInt("@MainIndexID", mainIndexId),
                CreateParamFloat("@BI", BI),
                CreateParamFloat("@PIPNI", PIPNI),
                CreateParamFloat("@PIPNR", PIPNR),
                CreateParamFloat("@PD", PD),
                CreateParamFloat("@UMSC", UMSC),
                CreateParamFloat("@UMMC", UMMC),
                CreateParamFloat("@COMP", COMP),
                CreateParamFloat("@COLL", COLL)
            };
            ExecuteNonQueryWithParameters(RelativitiesCaseUpdatetQueries[relativity], sqlParams,
                conn);
        }

        /// <summary>
        /// Extraction from InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="conn"></param>
        /// <param name="indexId"></param>
        private static void IpaUpdateBalanceInstallmentFinal(string policyNo, SqlConnection conn, int indexId)
        {
            using (var cmd2 =
                new SqlCommand(InstallmentPaymentApply.FinalUpdateBalanceInstallment,
                    conn))
            {
                cmd2.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd2.Parameters.Add(CreateParamInt("@IndexID", indexId));

                cmd2.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Extraction from InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="conn"></param>
        /// <param name="indexId"></param>
        /// <param name="amtToApply"></param>
        private static void IpaUpdateBalanceInstallment(string policyNo, SqlConnection conn, int indexId, decimal amtToApply)
        {
            using (var cmd2 = new SqlCommand(InstallmentPaymentApply.UpdatePymtAppliedBalanceInstallment, conn))
            {
                cmd2.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd2.Parameters.Add(CreateParamInt("@IndexID", indexId));
                cmd2.Parameters.Add(CreateParamMoney("@AmtToApply", amtToApply));

                cmd2.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Extraction from InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="postmarkDate"></param>
        /// <param name="userName"></param>
        /// <param name="conn"></param>
        /// <param name="indexId"></param>
        /// <param name="balanceInstallment"></param>
        private static void IpaUpdateLastPayAmt(string policyNo, DateTime? postmarkDate, string userName, SqlConnection conn,
            int indexId, decimal balanceInstallment)
        {
            using (var cmd2 = new SqlCommand(InstallmentPaymentApply.UpdatePymtAppliedLastPayAmt1, conn))
            {
                cmd2.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd2.Parameters.Add(CreateParamInt("@IndexID", indexId));
                cmd2.Parameters.Add(CreateParamVarchar("@UserName", userName, 25));
                cmd2.Parameters.Add(CreateParamDateTime("@PostmarkDate", postmarkDate));
                cmd2.Parameters.Add(CreateParamMoney("@BalanceInstallment", balanceInstallment));

                cmd2.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Extraction from InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="postmarkDate"></param>
        /// <param name="userName"></param>
        /// <param name="conn"></param>
        /// <param name="indexId"></param>
        /// <param name="pymtAmtRemaining"></param>
        private static void IpaUpdatePartialPymtApplied(string policyNo, DateTime? postmarkDate, string userName,
            SqlConnection conn, int indexId, decimal pymtAmtRemaining)
        {
            using (var cmd2 = new SqlCommand(InstallmentPaymentApply.UpdatePymtAppliedPartial, conn))
            {
                cmd2.Parameters.Add(CreateParamVarchar("@PolicyNo", policyNo, 25));
                cmd2.Parameters.Add(CreateParamInt("@IndexID", indexId));
                cmd2.Parameters.Add(CreateParamVarchar("@UserName", userName, 25));
                cmd2.Parameters.Add(CreateParamDateTime("@PostmarkDate", postmarkDate));
                cmd2.Parameters.Add(CreateParamDecimal("@PymtAmtRemaining", pymtAmtRemaining));

                cmd2.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Extraction from InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="conn"></param>
        private void IpaInsertNewCreditCardFees(string policyNo, string userName, SqlConnection conn)
        {
            // --- Get policy and installment info for policy #

            var results = ExecuteQueryWithPolicyNo(InstallmentPaymentApply.Join1, policyNo, conn);
            if (results.Count == 0)
                throw new StoredProcedureException($"No dates and pay plan retrieved for policy # {policyNo}");
            var values = (object[])results.First();
            var effDate =
                ConvertFromDbValOrException<DateTime>(values[0], $"Effective date null for policy # {policyNo}");
            var installCount =
                ConvertFromDbValOrException<double>(values[1], $"Install count null for policy # {policyNo}");
            var payPlan =
                ConvertFromDbValOrException<int>(values[2], $"Pay plan null for policy # {policyNo}");
            var policyWritten =
                ConvertFromDbValOrException<decimal>(values[3], $"Policy Written null for policy # {policyNo}");



            // ---- Get CCFee amount and date
            var objList = ExecuteQueryWithPolicyNo(InstallmentPaymentApply.GetCCFeeAmtAndDate, policyNo, conn);
            if (objList.Count == 0)
                throw new StoredProcedureException($"No CC Fee and date retrieved for policy {policyNo}");
            var objValues = (object[])objList.First();
            var ccFeeAmt =
                ConvertFromDbValOrException<decimal>(objValues[0], $"CC Fee amount null for policy {policyNo}");
            var ccFeeDate = ConvertFromDbValOrException<DateTime>(objValues[1], $"CC date null for policy {policyNo}");


            // ---- Get Install #
            var ccInstallNo = ConvertFromDbVal(InTxInstallPaymentApplyGetInstallNo(policyNo, effDate, ccFeeDate, conn),
                0M);


            // COMMENT FROM STORED PROC
            //    --@amit If CC Or Effective Date and Deposit Date is same day, then Install No will be null.
            //    -- Installments due date which is less than CC FeeDate, Install number will be of that Installment.
            //    --As That CC is considered as payment for Installments Due date which are less than CC Fee date.
            InTxInstallPaymentApplyInsertCreditCardFees(policyNo, ccInstallNo, installCount, payPlan, ccFeeDate,
                policyWritten, ccFeeAmt, userName, conn);

        }

        /// <summary>
        /// Add entry to ErrorLog table
        /// 
        /// Before setting the string properties, the values are truncated to fit
        /// into their corresponding fields using the value in the Size attribute
        /// of each field
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="dateTimeError">Timestamp of the error</param>
        /// <param name="errorProcedure">Where the error occurred</param>
        /// <param name="errorCode">Error # or code</param>
        /// <param name="errorDescr">Error description</param>
        /// <param name="conn"></param>
        private int LogErrorOnce(string guid, string policyNo, string userName, DateTime? dateTimeError,
            string errorProcedure,
            string errorCode, string errorDescr, SqlConnection conn)
        {
            return ExecuteNonQueryWithParameters(LogError.LogErrorEntry, new[]
            {
                CreateParamVarchar("@GUID_", Truncate(guid, 36), 36),
                CreateParamVarchar("@PolicyNo", Truncate(policyNo, 20), 20),
                CreateParamVarchar("@UserName", Truncate(userName, 20), 20),
                CreateParamDateTime("@DateTimeError", dateTimeError),
                CreateParamVarchar("@ErrorProcedure", Truncate(errorProcedure, 100), 100),
                CreateParamVarchar("@ErrorCode", Truncate(errorCode, 10), 10),
                CreateParamVarchar("@ErrorDescr", Truncate(errorDescr, 2000), 2000)
            }, conn);
        }

        /// <summary>
        /// Add entry to ErrorLog table
        /// 
        /// An attempt is made to add and entry to the error log using the
        /// parameters passed in.  If that is not successful, another attempt
        /// is made using a set of generic values.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="dateTimeError">Timestamp of the error</param>
        /// <param name="errorProcedure">Where the error occurred</param>
        /// <param name="errorCode">Error # or code</param>
        /// <param name="errorDescr">Error description</param>
        /// <param name="conn"></param>
        private void LogErrorWithConn(string guid, string policyNo, string userName, DateTime? dateTimeError,
            string errorProcedure, string errorCode, string errorDescr, SqlConnection conn)
        {
            try
            {
                LogErrorOnce(guid, policyNo, userName, dateTimeError, errorProcedure, errorCode, errorDescr, conn);
            }
            catch (Exception ex)
            {
                LogErrorOnce(null, string.Empty, Environment.UserName, DateTime.Now,
                    (new StackTrace()).GetFrame(2).GetMethod().Name, string.Empty, ex.Message ?? string.Empty, conn);
            }
        }

        /// <summary>
        /// Performs validation of parameters for MoveMoney method
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        private void ValidateMoveMoneyParameters(string policyNo, string userName, string guid)
        {
            var policyCount = GetCountOfPolicyRecordsByPolicyNo(policyNo);
            var policyBinderCount = GetCountOfPolicyBinderRecordsByPolicyNo(policyNo);
            var policyRenQuoteCount = GetCountOfPolicyRenQuoteRecordsByPolicyNo(policyNo);

            if (policyCount == 0 && policyBinderCount == 0 && policyRenQuoteCount == 0)
            {
                TxLogError(guid, policyNo, userName, DateTime.Now, "MoveMoney", "",
                    $"Policy # {policyNo} does not exist.");
            }
        }

        #endregion

        #region Temporary Tables

        /// <summary>
        /// Class used in place of SQL temporary tables
        ///
        /// 
        /// </summary>
        /// <typeparam name="T">record definition which must obey interface ITempTableRecord</typeparam>
        public class TempTable<T> where T : ITempTableRecord
        {
            public TempTable(IEnumerable<T> recs):this()
            {
                AddRecords(recs);
            }

            public TempTable()
            {
                Table = new List<T>();
                LastIndex = 0;
            }

            public List<T> Table { get; set; }

            public int LastIndex { get; set; }

            public T AddRecord(T record)
            {
                record.TempTableID = ++LastIndex;
                Table.Add(record);
                return record;
            }

            public void AddRecords(IEnumerable<T> recs)
            {
                foreach (var rec in recs)
                {
                    AddRecord(rec);
                }
            }

        }

        public interface ITempTableRecord
        {
            int TempTableID { get; set; }
        }

        public class TempAllDays : ITempTableRecord
        {
            public int TempTableID { get; set; }
            public DateTime TheDay { get; set; }
        }

        public class TempAuditCoverageData : ITempTableRecord
        {
            public int TempTableID { get; set; }
            public DateTime? AcctDate { get; set; }
            public string Action { get; set; }
            public int? ActionID { get; set; }
            public int? Annualized { get; set; }  //
            public bool? ChangeDateEnd { get; set; }
            public int? Company { get; set; }
            public string Coverage { get; set; }
            public int? EarnNoDays { get; set; }
            public DateTime? EarnStart { get; set; }
            public DateTime? EarnStop { get; set; }
            public double? EarnedSum { get; set; }
            public DateTime? EffDate { get; set; }
            public DateTime? ExpDate { get; set; }
            public int? IgnoreIndexID { get; set; }
            public bool? IgnoreRecord { get; set; }
            public bool? IsFee { get; set; }
            public double? LastDayEarn { get; set; }
            public bool? Negate { get; set; }
            public int? LOB { get; set; }
            public double? OneDayEarn { get; set; }
            public string OrigAction { get; set; }
            public string PolicyNo { get; set; }
            public double? ProRata { get; set; }
            public string State { get; set; }
            public int? TermIndexID { get; set; }
            public int? VehIndex { get; set; }
            public double? Written { get; set; }

        }

        public class TempDays : ITempTableRecord
        {
            public int TempTableID { get; set; }

            public int IndexID { get; set; }
            public int? TermIndexID { get; set; }
            public string Coverage { get; set; }
            public int? VehIndex { get; set; }
            public DateTime? TheDay { get; set; }
            public double? Amount { get; set; }
            public bool? Negated { get; set; }
            public string TermDesc { get; set; }
            public double? Annualized { get; set; }

        }

        public class TempJoin : ITempTableRecord
        {
            public int TempTableID { get; set; }
            public int? TermIndexID { get; set; }
            public string Coverage { get; set; }
            public int? VehIndex { get; set; }
            public double? Amount { get; set; }
            public bool? Negate { get; set; }
            public string TermDesc { get; set; }
            public double? Annualized { get; set; }
            public DateTime? EffDate { get; set; }
            public DateTime? EarnStop { get; set; }
        }

        public class TempNotices : ITempTableRecord
        {
            public int TempTableID { get; set; }
            public int? APHIndexId { get; set; }
            public string PolicyNo { get; set; }
            public DateTime? APHNoticeMailDate { get; set; }
            public string APHNoticeCancelType { get; set; }
            public DateTime? APHCancelDate { get; set; }
            public string APHAction { get; set; }
        }

        public class TempPreNegateUpdate : ITempTableRecord
        {
            public int TempTableID { get; set; }
            public string PolicyNo { get; set; }
            public bool? IsFee { get; set; }
            public DateTime? EffDate { get; set; }
            public DateTime? EarnStop { get; set; }
            public bool? Negate { get; set; }
            public bool? IgnoreRecord { get; set; }
            public string Coverage { get; set; }
            public int? VehIndex { get; set; }
            public int? TermIndexID { get; set; }
            public int? IndexID { get; set; }

        }

        public class TempPreSetWritten : ITempTableRecord
        {
            public int TempTableID { get; set; }
            public string Coverage { get; set; }
            public int? VehIndex { get; set; }
            public int? IndexID { get; set; }
            public string PolicyNo { get; set; }
            public double? Written { get; set; }

        }

        #endregion

        #region Temp Tables to be converted to generics

        /// <summary>
        /// Used by AuditSaveDifference
        /// </summary>
        public class TempColumns
        {
            public TempColumns()
            {
                Table = new List<TempColumnsRecord>();
                LastIndex = 0;
            }

            public List<TempColumnsRecord> Table { get; set; }

            public int LastIndex { get; set; }

            public TempColumnsRecord AddRecord(TempColumnsRecord record)
            {
                record.RowNbr = ++LastIndex;
                Table.Add(record);
                return record;
            }
        }

        /// <summary>
        /// Used by AuditSaveDifference
        /// </summary>
        public class TempColumnsRecord
        {
            public int RowNbr { get; set; }
            public string ColumnName { get; set; }
        }

        /// <summary>
        /// Class used where a temp table was used in the original
        /// stored proc for GetListProjectedInstallments
        /// </summary>
        public class TempFinal
        {
            public TempFinal()
            {
                Table = new List<TempFinalRecord>();
                LastIndex = 0;
            }

            public List<TempFinalRecord> Table { get; set; }

            public int LastIndex { get; set; }

            public TempFinalRecord AddRecord(TempFinalRecord record)
            {
                record.IndexID = ++LastIndex;
                Table.Add(record);
                return record;
            }
        }

        /// <summary>
        /// Record definition for TempFinal table used by
        /// GetListProjectedInstallments
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class TempFinalRecord
        {
            public int IndexID { get; set; }
            public double? InstallNo { get; set; }
            public int? PaymentType { get; set; }
            public int? NumberOfPayments { get; set; }
            public string InstallmentDesc { get; set; }
            public int? PayPlan { get; set; }
            public DateTime? DueDate { get; set; }
            public decimal? PolicyWritten { get; set; }
            public decimal? InstallmentFee { get; set; }
            public decimal? PolicyFee { get; set; }
            public decimal? SR22Fee { get; set; }
            public decimal? MVRFee { get; set; }
            public decimal? LateFee { get; set; }
            public decimal? MaintenanceFee { get; set; }
            public decimal? PIPPDOnlyFee { get; set; }
            public decimal? RewriteFee { get; set; }
            public decimal? FeeTotal { get; set; }
            public decimal? DownPaymentPct { get; set; }
            public decimal? DownPaymentAmt { get; set; }
            public decimal? InstallmentAmt { get; set; }
            public decimal? PremiumBalance { get; set; }
            public decimal? PymtApplied { get; set; }
            public decimal? BalanceInstallment { get; set; }
        }

        /// <summary>
        /// Used by RateSpartan_CVData
        /// and RateSpartan_CVData_AA20210412
        /// </summary>
        public class TmpLoopTable
        {
            public TmpLoopTable()
            {
                Table = new List<TmpLoopTableRecord>();
                LastIndex = 0;
            }

            public List<TmpLoopTableRecord> Table { get; set; }

            public int LastIndex { get; set; }

            public TmpLoopTableRecord AddRecord(TmpLoopTableRecord record)
            {
                record.TmpIndexID = ++LastIndex;
                Table.Add(record);
                return record;
            }
        }

        /// <summary>
        /// Used by RateSpartan_CVData
        /// and RateSpartan_CVData_AA20210412
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class TmpLoopTableRecord
        {
            public int TmpIndexID { get; set; }
            public int? CVIndexID { get; set; }
            public string PolicyNo { get; set; }
            public string ResponseJson { get; set; }
            public string PreviousCompany { get; set; }
            public DateTime? EffDate { get; set; }
            public DateTime? ExpDate { get; set; }
            public string PreviousCompany2 { get; set; }
            public DateTime? EffDate2 { get; set; }
            public DateTime? ExpDate2 { get; set; }
            public double? NumberOfLapseDays { get; set; }
            public string HasPossibleLapse { get; set; }
            public string IsCurrentInforceCoverage { get; set; }

        }

        /// <summary>
        /// Class used where a temp table was used in the original
        /// stored proc for GetListProjectedInstallments
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class TmpPayPlans
        {
            public int IndexID { get; set; }
            public int? PayPlanID { get; set; }
            public string State { get; set; }
            public double? Term { get; set; }
            public string Code { get; set; }
            public double? EFT { get; set; }
            public double? BI { get; set; }
            public string NewOrRen { get; set; }
            public double? DP_AsPerc { get; set; }
            public double? Installments { get; set; }
            public double? FirstDue { get; set; }
            public double? BillCycle { get; set; }
            public string RenQuote { get; set; }
            public string RateCycle { get; set; }
        }

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        public class TmpRelativities
        {
            public TmpRelativities()
            {
                Table = new List<TmpRelativitiesRecord>();
                LastIndex = 0;
            }

            public List<TmpRelativitiesRecord> Table { get; set; }

            public int LastIndex { get; set; }

            public TmpRelativitiesRecord AddRecord(TmpRelativitiesRecord record)
            {
                record.IndexID = ++LastIndex;
                Table.Add(record);
                return record;
            }
        }

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        public class TmpRelativitiesRecord
        {
            public int IndexID { get; set; }
            public int RRIndexID { get; set; }
            public int? RatingOrderIndexID { get; set; }
            public int MainIndexID { get; set; }
            public int? VehIndexID { get; set; }
            public int? RatedOprIndexID { get; set; }
            public int? RateOrderNo { get; set; }
            public string MathAction { get; set; }
            public int? RoundStep { get; set; }
            public double? BI { get; set; }
            public double? PIPNI { get; set; }
            public double? PIPNR { get; set; }
            public double? PD { get; set; }
            public double? UMSC { get; set; }
            public double? UMMC { get; set; }
            public double? COMP { get; set; }
            public double? COLL { get; set; }
        }

        #endregion

        #region Inner Classes and Dictionaries

        /// <summary>
        /// Class used to mimic WITH clause in
        /// GetListProjectedInstallments
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class IFee
        {
            public double? InstallNo { get; set; }
            public decimal? InstallmentFee { get; set; }
        }

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        private static readonly Dictionary<string, string> RelativitiesCaseSelects = new Dictionary<string, string>()
        {
            {"Total Discount Factor","Discount"},
            {"Total Surcharge Factor","Surcharge"},
            {"Without Fee",""},
            {"Annualized Vehicle Premium",""},
            {"Vehicle Premium",""},
            {"Annualized Policy","Annualized"},
            {"Policy Premium","Subtotal"}
        };

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        private static readonly Dictionary<string, string> RelativitiesCaseUpdates = new Dictionary<string, string>()
        {
            {"Total Discount Factor","Total Discount Factor"},
            {"Total Surcharge Factor","Total Surcharge Factor"},
            {"Without Fee","Without Fee"},
            {"Annualized Vehicle Premium","Annualized"},
            {"Vehicle Premium","Subtotal"},
            {"Annualized Policy","Annualized"},
            {"Policy Premium","Total"}
        };

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        private static readonly Dictionary<string, string> RelativitiesCaseSelectQueries = new Dictionary<string, string>()
        {
            {"Total Discount Factor",TotalRelativities_AA20210412.SelectFromRatingRelativities_Group1},
            {"Total Surcharge Factor",TotalRelativities_AA20210412.SelectFromRatingRelativities_Group1},
            {"Without Fee",TotalRelativities_AA20210412.SelectFromRatingRelativities_WIthoutFee},
            {"Annualized Vehicle Premium", TotalRelativities_AA20210412.SelectFromRatingRelativities_AnnualizedVehiclePremium},
            {"Vehicle Premium",TotalRelativities_AA20210412.SelectFromRatingRelativities_VehiclePremium},
            {"Annualized Policy",TotalRelativities_AA20210412.SelectFromRatingRelativities_AnnualizedPolicy},
            {"Policy Premium",TotalRelativities_AA20210412.SelectFromRatingRelativities_PolicyPremium}
        };

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        private static readonly Dictionary<string, string> RelativitiesCaseUpdatetQueries = new Dictionary<string, string>()
        {
            {"Total Discount Factor",TotalRelativities_AA20210412.UpdateRatingRelativities_Relativity},
            {"Total Surcharge Factor",TotalRelativities_AA20210412.UpdateRatingRelativities_Relativity},
            {"Without Fee",TotalRelativities_AA20210412.UpdateRatingRelativities_Type},
            {"Annualized Vehicle Premium", TotalRelativities_AA20210412.UpdateRatingRelativities_Type},
            {"Vehicle Premium",TotalRelativities_AA20210412.UpdateRatingRelativities_Type},
            {"Annualized Policy",TotalRelativities_AA20210412.UpdateRatingRelativities_AnnualizedPolicy},
            {"Policy Premium",TotalRelativities_AA20210412.UpdateRatingRelativities_PolicyPremium}
        };

        /// <summary>
        /// Used by TotalRelativities_AA20210412
        /// </summary>
        /// <param name="resultList"></param>
        /// <param name="tmpRelativities"></param>
        private static void SaveRelativitiesToTempTable(List<object> resultList, TmpRelativities tmpRelativities)
        {
            foreach (var rec in resultList)
            {
                var values = (object[])rec;
                tmpRelativities.AddRecord(new TmpRelativitiesRecord
                {
                    RRIndexID = (int)values[0],
                    RatingOrderIndexID = GetDbValOrNull<int>(values[1]),
                    MainIndexID = (int)values[2],
                    VehIndexID = GetDbValOrNull<int>(values[3]),
                    RatedOprIndexID = GetDbValOrNull<int>(values[4]),
                    RateOrderNo = GetDbValOrNull<int>(values[5]),
                    MathAction = GetDbRefValOrNull<string>(values[6]),
                    RoundStep = GetDbValOrNull<byte>(values[7]),
                    BI = GetDbValOrNull<double>(values[8]),
                    PIPNI = GetDbValOrNull<double>(values[9]),
                    PIPNR = GetDbValOrNull<double>(values[10]),
                    PD = GetDbValOrNull<double>(values[11]),
                    UMSC = GetDbValOrNull<double>(values[12]),
                    UMMC = GetDbValOrNull<double>(values[13]),
                    COMP = GetDbValOrNull<double>(values[14]),
                    COLL = GetDbValOrNull<double>(values[15])
                });
            }
        }

        #endregion

        #region Support

        /// <summary>
        /// Rounds a double or returns null if value is null
        /// </summary>
        /// <param name="val"></param>
        /// <param name="precision"></param>
        /// <returns>value rounded to precision places or null</returns>
        private static double? DoubleRound(double? val, int precision)
        {
            return val != null ? Math.Round((double)val, precision, MidpointRounding.AwayFromZero) : (double?)null;
        }

        /// <summary>
        /// Tests two doubles for equality using an epsilon
        ///
        /// Doubles should not be tested for equality using the
        /// equals operator, so this tests if the two values
        /// are within epsilon of each other
        /// </summary>
        /// <param name="val1"></param>
        /// <param name="val2"></param>
        /// <param name="epsilon"></param>
        /// <returns>true of the values are within epsilon of each other</returns>
        private static bool DoublesWithinEpsilon(double? val1, double? val2, double epsilon = 0.0000001D)
        {
            return (!(val1 is null) && !(val2 is null)) && Math.Abs((double)val1 - (double)val2) < epsilon;
        }


        /// <summary>
        /// Used to allow null conditional propagation when chaining method
        /// calls and properties referencing array elements
        ///
        /// e.g.
        /// with a reference like
        ///    body.coverageLapseInformation[0].coverageIntervals[1].startDate
        ///
        /// where any expression down the line could be null -- not just the last ref
        /// 
        ///    (Enull(Enull(body?.coverageLapseInformation)?.coverageIntervals,1)?.startDate);
        ///
        /// allows a test for null over the entire reference
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arr"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T Enull<T>(T[] arr, int index = 0) where T : class
        {
            return arr?.Length > index ? arr[index] : null;
        }

        /// <summary>
        /// Returns a new SqlConnection using the connection string
        /// saved in the constructor.
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetConnection()
        {
            return new SqlConnection(_connectionString);
        }

        private static string ParseChar(string charString)
        {
            if (string.IsNullOrEmpty(charString)) return null;
            return charString.Substring(0, 1);
        }

        private static DateTime? ParseDate(string dateString)
        {
            if (string.IsNullOrEmpty(dateString)) return null;
            return DateTime.ParseExact(dateString, "yyyyMMdd", CultureInfo.InvariantCulture);
        }

        private static double? ParseDouble(string doubleString)
        {
            if (string.IsNullOrEmpty(doubleString)) return null;
            return double.Parse(doubleString, CultureInfo.InvariantCulture);
        }

        #endregion
    }

}