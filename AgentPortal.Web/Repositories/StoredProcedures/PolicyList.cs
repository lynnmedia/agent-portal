using System;

namespace AgentPortal.Repositories.StoredProcedures
{
    /// <summary>
    /// Returned by AutomaticActionStep1
    /// </summary>
    public class PolicyList
    {
        public int? Id_num { get; set; }
        public string PolicyNo { get; set; }
        public string AgentCode { get; set; }
        public string AgentStatus { get; set; }
        public DateTime? EffDate { get; set; }
        public DateTime? ExpDate { get; set; }
        public DateTime? CancDate { get; set; }
        public DateTime? EquityDate { get; set; }
        public double? Annualized { get; set; }
        public double? PerDiem { get; set; }
        public double? Written { get; set; }
        public double? TotalPaid { get; set; }
        public double? TotalNSFFees { get; set; }
        public string PolicyStatus { get; set; }
        public string RenewalStatus { get; set; }
        public double? CurrMinDue { get; set; }
        public DateTime? CurrDueDate { get; set; }
        public DateTime? MinDueDateNotPrinted { get; set; }
        public double? CurrTotalDue { get; set; }
        public double? NoticeAmt { get; set; }
        public DateTime? NoticeProcessDate { get; set; }
        public double? PaidSinceNotice { get; set; }
        public int? DaysInForce { get; set; }
        public int? DaysFromExpiration { get; set; }
        public bool? IsNSF { get; set; }
        public bool? IsFNR { get; set; }
        public bool? IsFNRClaims { get; set; }
        public bool? OpenSuspense { get; set; }
        public bool? OpenSuspenseSetToCancel { get; set; }
        public DateTime? FirstSuspDate { get; set; }
        public DateTime? SecondSuspDate { get; set; }
        public bool? IsDirectBill { get; set; }
        public bool? IsPIF { get; set; }
        public bool? IsEFT { get; set; }
        public bool? IsRenewal { get; set; }
        public bool? _Send2ndSusp { get; set; }
        public bool? _Reinstate { get; set; }
        public bool? _NoticeNonPay { get; set; }
        public bool? _NoticeUnd { get; set; }
        public bool? _FinalCancel { get; set; }
        public bool? _PrintInvoice { get; set; }
        public bool? _SetZeroDueInvoice { get; set; }
        public bool? _IssueNonRenewal { get; set; }
        public bool? _IssueNonRenewalAgent { get; set; }
        public bool? _IssueRenQuote { get; set; }
        public bool? _IssueRenewal { get; set; }
        public bool? _AlertPossibleUndReinstatement { get; set; }
        public bool? _IssueNSFDepositNotice { get; set; }
        public bool? _IssueVoidPolicy { get; set; }
        public bool? _IssueNonRenewalClaimsMsg { get; set; }
        public bool? _AlertPossibleUndNonRenewalReinstatement { get; set; }
        public bool? _AutomaticRenewalInvoice { get; set; }
        public bool? _DraftRenewalDownPayment { get; set; }
        public bool? DoneProcessing { get; set; }
        public DateTime? ActionDate { get; set; }
        public int? RefID { get; set; }
        public DateTime? CreateDate { get; set; }
    }

}