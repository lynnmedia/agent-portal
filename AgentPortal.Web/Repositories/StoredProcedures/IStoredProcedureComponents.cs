﻿using System;
using System.Collections.Generic;
using System.Configuration;

// ReSharper disable InconsistentNaming

namespace AgentPortal.Repositories.StoredProcedures
{
    public interface IStoredProcedureComponents
    {
        decimal TxPolicyBalance(string policyNo);

        int TxAllowAutoDraft(string policyNo);

        void TxAuditSaveDifference(string policyNo, string table1, string table2, int? historyId, string userName,
            bool? ignoreSpecialFields = false, string guid = "");

        List<PolicyList> TxAutomaticActionStep1(DateTime? requestedReportDate);

        void TxAutomaticActionsStep3(DateTime? requestedReportDate = null);

        void TxClearPolicyEndorseQuote(string policyNo, string userName, string guid);

        void TxClearPolicyRenQuoteTemp(string policyNo, string guid, string userName);

        void TxCopyPolicyToAuditTables(string policyNo, int historyId, string userName, string guid);

        void TxCopyPolicyEndorseQuoteToPolicyTables(string policyNo, string userName, string guid, bool fromWeb = false);

        void TxCopyPolicyToPolicyEndorseQuoteTables(string policyNo, string userName, string guid);

        void TxCreateInstallmentsPolicy(string policyNo, decimal? policyWritten, int? installCount, decimal? installNo,
            DateTime? dueDate, decimal? installmentPymt, bool? nullableCreateDownPayment, bool? forceDepositToAmountPaid,
            string userName, string guid, int? historyIndexId, double? amountForForceDeposit);

        void TxCreateInstallmentsRenQuote(string policyNo, decimal? policyWritten, int? installCount, decimal? installNo,
            DateTime? dueDate, decimal? installmentPymt, bool? nullableCreateDownPayment, bool? forceDepositToAmountPaid,
            string userName, string guid, int? historyIndexId, double? amountForForceDeposit);

        List<object> TxGetListProjectedInstallments(string policyNo, int ratingId, int payPlan, Guid? guid = null);

        decimal TxGetRenewalDraftAmount(string policyNo, string userName, string guid);

        double? TxGetTotalDueOnPolicy(string policyNo, string userName, string guidErrorLog);

        void TxInstallmentPaymentApply(string policyNo, DateTime? postmarkDate, string userName, string guid);

        int? TxInstallNoCurrent(string policyNo, DateTime mailDate);

        bool TxIsHoliday(DateTime dayToValidate);

        void TxIssueCancellation(string policyNo, DateTime? cancellationDate, bool flatCancel, bool shortRate,
            DateTime? acctDate = null, string userName = "", string guidErrorLog = "", string guidHistoryIndexId = "");

        void TxIssueNotice(string policyNo, DateTime? noticeDate, string cancelType, string reason1, string reason2,
            string reason3, string reason4, string userName, DateTime? mailDate, bool isNSFFeeCharged, string guid);

        void TxLateFeePolicyApply(string policyNo, DateTime mailDate, string userName, string guid = "");

        void TxLogError(string guid, string policyNo, string userName, DateTime? dateTimeError,
            string errorProcedure, string errorCode, string errorDescr);

        decimal TxMinimumEndorsementPaymentDue(string policyNo, DateTime endorsementDate,
            decimal fullAmountOfEndorsement, string userName);

        void TxMoveMoney(string moveFromPolicyNo, string moveToPolicyNo, decimal amount, string userName,
            string guid,
            DateTime? displayDate = null,
            bool ignoreToPolicyExists = false);

        DateTime TxNextBusinessDay(DateTime day);

        void TxPolicyUpdateFieldFormats(string policyNo);

        void TxPolicyFutureActionsAddRecord(string policyNo, string action, DateTime? dueDate, DateTime? mailDate,
            DateTime? processDate, string userName, string guid, int? historyIndexId = null);

        void TxRatePolicyAlert(string currIdStr, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null);

        void TxRateSpartan_CVData(int currId, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null);

        void TxRateSpartan_CVData_AA20210412(int currId, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null);

        void TxTotalRelativitiesBase(string suffix, int mainIndexId, int vehIndexId = 0, int ratedOprIndexId = 0,
            string relativity = "");

        void TxTransferPolicyToPolicyRenQuote(string policyNo, string userName, string guid);

        void TxTransferRenQuoteToPolicyBinders(string policyNo, string userName, string guid);

        void TxTransferRenQuoteToRenQuoteTempByPolicyNo(string policyNo, string userName, string guid);

        void TxUpdateInstallmentAudit(string userName = "SYSTEM");

        bool TxValidBusinessDay(DateTime dayToValidate);

    }
}