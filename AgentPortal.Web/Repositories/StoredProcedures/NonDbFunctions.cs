﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.WebPages;
using static AgentPortal.Repositories.Sql.SqlHelper;

namespace AgentPortal.Repositories.StoredProcedures
{
    /// <summary>
    /// These methods are the migrated versions of user-defined functions
    /// in the database which do not require database access
    /// </summary>
    public static class NonDbFunctions
    {
        /// <summary>
        /// Returns only the date part of a DateTime value without the time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime DateValueNoTimestamp(DateTime dateTime)
        {
            return dateTime.Date;
        }

        /// <summary>
        /// Returns a rating indicating how alike two strings (names) are
        /// 
        /// Exact matches return a zero (0).  The less alike the two strings
        /// are, the higher the rating returned.
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns>Similarity rating -- higher means less alike</returns>
        public static int EditDistance(string str1, string str2)
        {
            string str1Trimmed = str1.Trim();
            string str2Trimmed = str2.Trim();
            int str1Length = str1Trimmed.Length;
            int str2Length = str2Trimmed.Length;
            int c = 0;
            List<int> cv1 = Enumerable.Range(0, str2Length + 1).ToList();
            List<int> cv0 = new List<int>() { 0 };

            char[] str1Chars = str1Trimmed.ToCharArray();
            char[] str2Chars = str2Trimmed.ToCharArray();

            for (int i = 1; i <= str1Length; i++)
            {
                char str1Char = str1Chars[i - 1];
                c = i;
                cv0.Clear();
                cv0.Add(i);

                for (int j = 1; j <= str2Length; j++)
                {
                    c++;
                    int cTemp = cv1[j - 1] + ((str1Char == str2Chars[j - 1]) ? 0 : 1);
                    c = Math.Min(c, cTemp);
                    cTemp = cv1[j + 1 - 1] + 1;
                    c = Math.Min(c, cTemp);
                    cv0.Add(c);
                }

                cv1 = cv0.ToList();
            }

            return c;
        }

        /// <summary>
        /// Returns the Max direct bill fee that can be billed
        /// 
        /// </summary>
        /// <param name="policyTotal"></param>
        /// <param name="downPayment"></param>
        /// <param name="numInstallments"></param>
        /// <param name="policyTerm"></param>
        /// <returns></returns>
        public static decimal? GetMaxBillingFee(decimal? policyTotal, decimal? downPayment, int? numInstallments,
            int? policyTerm)
        {
            //if ((policyTotal ?? downPayment ?? policyTerm) == null)
             //   return null;

            var workingNumInstallments = numInstallments ?? 0;

            var uncollectedAmount = policyTotal - downPayment;
            var downPaymentPercent = (policyTerm == 12) ? 0.18M : 0.09M;
            var installNo = 0;

            // set @InstallmentAmount = round((@PolicyTotal - @DownPayment) / (Case IsNull(@NoInstallments, 0) when 0 then 1 else @NoInstallments end) ,2)
            var installmentAmount = policyTotal - downPayment;
            if (workingNumInstallments != 0)
            {
                installmentAmount /= workingNumInstallments;
            }

            installmentAmount = DecimalRoundNull(installmentAmount, 2);


            // while @InstallNo < @NoInstallments
            // begin
            //      set @UncollectedAmount = @UncollectedAmount + (@InstallmentAmount * (@NoInstallments - @InstallNo))
            //       set @InstallNo = @InstallNo + 1
            // end
            while (installNo < workingNumInstallments)
            {
                uncollectedAmount += installmentAmount * (workingNumInstallments - installNo++);
            }


            //  set @NoInstallments = @NoInstallments + 1
            workingNumInstallments++;

            //  return round((@UncollectedAmount / (Case IsNull(@NoInstallments, 0) when 0 then 1 else @NoInstallments end)) *@DownPaymentPercent,2)
            var temp = uncollectedAmount * downPaymentPercent;
            if (workingNumInstallments != 0)
            {
                temp /= workingNumInstallments;
            }

            return DecimalRoundNull(temp, 2);
        }

        public static string PhonePart(string phonePart, string phoneNumber)
        {
            var extIndicators = new List<string>() {"EXT","X" }; //  order important
            var country = string.Empty;
            var area = string.Empty;
            // ReSharper disable once RedundantAssignment
            var local = string.Empty;
            string ext = string.Empty;
            string result = string.Empty;

            phonePart = phonePart?.Trim().ToLower() ?? string.Empty;
            if (phonePart.IsEmpty())
                return string.Empty;
            phoneNumber = phoneNumber?.Trim().ToLower() ?? string.Empty;
            if (phoneNumber.IsEmpty())
                return string.Empty;

            var numLessExt = phoneNumber;
            // get separate extension from rest
            foreach (var indicator in extIndicators)
            {
                var pos = phoneNumber.IndexOf(indicator, StringComparison.InvariantCultureIgnoreCase);
                if (pos >= 0)
                {
                    if (pos < phoneNumber.Length - indicator.Length)
                    {
                        ext = phoneNumber.Substring(pos + indicator.Length).Trim();
                        if (pos > 0)
                            numLessExt = phoneNumber.Substring(0, pos).Trim();
                        break;
                    }
                }
            }

            // Strip out non-numeric characters
            var numLessExtStripped = new string(numLessExt.Where(char.IsDigit).ToArray());
            var len = numLessExtStripped.Length;

            if (len == 10) // format like: 1 - 999 - 999 - 9999
            {
                area = numLessExtStripped.Substring(0, 3);
                local = numLessExtStripped.Substring(3, 7);
            }
            else if (len >= 11 && len <= 13) // format like: ??9-999-999-9999
            {
                area = numLessExtStripped.Substring(len -  10, 10).Substring(0, 3);
                local = numLessExtStripped.Substring(len-7);
            }
            else if (len == 14) // format like: 999-999-9999-9999
            {
                country = numLessExtStripped.Substring(0, 3);
                area = numLessExtStripped.Substring(len - 11, 11).Substring(0, 3);
                local = numLessExtStripped.Substring(len - 8);
            }
            else if (len >= 15) // format like: ???999-9999-9999-9999
            {
                country = numLessExtStripped.Substring(0, len  -  12);
                area = numLessExtStripped.Substring(len - 12, 12).Substring(0, 4);
                local = numLessExtStripped.Substring(len - 8);
            }
            else if (len > 7) // 
            {
                area = numLessExtStripped.Substring(0, len - 7);
                local = numLessExtStripped.Substring(len - 7);
            }
            else
            {
                local = numLessExtStripped;
            }

            switch (phonePart )
            {
                case "area":
                    result = area;
                    break;
                case "local":
                    result = local;
                    break;
                case "ext":
                    result = ext;
                    break;
                case "formatted":
                    if (local.Length > 4)
                    {
                        local = $"{local.Substring(0, local.Length - 4)}-{local.Substring(local.Length-4)}";
                    }
                    result = $"{(area.IsEmpty() ? area : area + "-")}{local}{(ext.IsEmpty() ? null : $" ext: {ext}")}";
                    break;
                case "numbersonly":
                    result = $"{country}{area}{local}";
                    break;
            }
            
            return result;
        }


    }
}