﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AgentPortal.Repositories.StoredProcedures.StoredProcedureQueries {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class CopyPolicyToAuditTables {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal CopyPolicyToAuditTables() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("AgentPortal.Repositories.StoredProcedures.StoredProcedureQueries.CopyPolicyToAudi" +
                            "tTables", typeof(CopyPolicyToAuditTables).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO AuditPolicy (
        ///			[HistoryIndexID]
        ///			,[PolicyNo]
        ///			,[PriorPolicyNo]
        ///			,[NextPolicyNo]
        ///			,[RenCount]
        ///			,[State]
        ///			,[LOB]
        ///			,[EffDate]
        ///			,[ExpDate]
        ///			,[PolicyStatus]
        ///			,[RenewalStatus]
        ///			,[DateBound]
        ///			,[DateIssued]
        ///			,[CancelDate]
        ///			,[CancelType]
        ///			,[Ins1First]
        ///			,[Ins1Last]
        ///			,[Ins1MI]
        ///			,[Ins1Suffix]
        ///			,[Ins1FullName]
        ///			,[Ins2First]
        ///			,[Ins2Last]
        ///			,[Ins2MI]
        ///			,[Ins2Suffix]
        ///			,[Ins2FullName]
        ///			,[Territory]
        ///			,[PolicyWritten]
        ///			,[PolicyAnnual [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CopyPolicyToAudit {
            get {
                return ResourceManager.GetString("CopyPolicyToAudit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO AuditPolicyCars (
        ///			[HistoryIndexID]
        ///			,[PolicyNo]
        ///			,[CarIndex]
        ///			,[EffDate]
        ///			,[ExpDate]
        ///			,[RateCycle]
        ///			,[Deleted]
        ///			,[VIN]
        ///			,[VehYear]
        ///			,[VehMake]
        ///			,[VehModel1]
        ///			,[VehModel2]
        ///			,[VehDoors]
        ///			,[VehCylinders]
        ///			,[VehDriveType]
        ///			,[VINIndex]
        ///			,[PrincOpr]
        ///			,[RatedOpr]
        ///			,[RatingClass]
        ///			,[TotalPoints]
        ///			,[ISOSymbol]
        ///			,[ISOPhyDamSymbol]
        ///			,[BISymbol]
        ///			,[PDSymbol]
        ///			,[PIPSymbol]
        ///			,[UMSymbol]
        ///			,[MPSymbol]
        ///			,[COMPSymbol]
        ///			,[COLLS [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CopyPolicyToAudit_Cars {
            get {
                return ResourceManager.GetString("CopyPolicyToAudit_Cars", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO AuditPolicyDrivers (
        ///			[HistoryIndexID]
        ///			,[PolicyNo]
        ///			,[DriverIndex]
        ///			,[EffDate]
        ///			,[ExpDate]
        ///			,[Deleted]
        ///			,[FirstName]
        ///			,[LastName]
        ///			,[MI]
        ///			,[Suffix]
        ///			,[Class]
        ///			,[Gender]
        ///			,[Married]
        ///			,[DOB]
        ///			,[DriverAge]
        ///			,[Youthful]
        ///			,[Points]
        ///			,[PrimaryCar]
        ///			,[SafeDriver]
        ///			,[SeniorDefDrvDisc]
        ///			,[SeniorDefDrvDate]
        ///			,[LicenseNo]
        ///			,[LicenseSt]
        ///			,[DateLicensed]
        ///			,[InternationalLic]
        ///			,[Unverifiable]
        ///			,[Inexp]
        ///			,[SR22]
        ///			,[SuspLic] [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CopyPolicyToAudit_Drivers {
            get {
                return ResourceManager.GetString("CopyPolicyToAudit_Drivers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT INTO AuditPolicyDriversViolations (
        ///			[HistoryIndexID]
        ///			,[PolicyNo]
        ///			,[DriverIndex]
        ///			,[ViolationNo]
        ///			,[ViolationIndex]
        ///			,[ViolationCode]
        ///			,[ViolationGroup]
        ///			,[ViolationDesc]
        ///			,[ViolationDate]
        ///			,[ViolationMonths]
        ///			,[ViolationPoints]
        ///			,[Chargeable]
        ///			,[OptimisticLockField]
        ///			,[GCRecord]
        ///			,[PTSDriverIndex]
        ///			,[FromMVR]
        ///			)
        ///		SELECT @HistoryID
        ///			,PolicyNo
        ///			,DriverIndex
        ///			,ViolationNo
        ///			,ViolationIndex
        ///			,ViolationCode
        ///			,ViolationGroup
        ///			,Viol [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CopyPolicyToAudit_DriversViolations {
            get {
                return ResourceManager.GetString("CopyPolicyToAudit_DriversViolations", resourceCulture);
            }
        }
    }
}
