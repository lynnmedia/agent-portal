using System;
using System.Collections.Generic;

namespace AgentPortal.Repositories.StoredProcedures
{
    /// <summary>
    /// SQL Stored Procedures from the database migrated here in C#
    /// 
    /// </summary>
    public class StoredProcedures
    {
        private readonly IStoredProcedureComponents _storedProcedureComponents;
        public delegate void RateSpartanMethod(int currId, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null);

        private Dictionary<string, RateSpartanMethod> _rateSpartanMethods;

        public StoredProcedures(IStoredProcedureComponents storedProcedureComponents)
        {
            _storedProcedureComponents = storedProcedureComponents;
        }

        #region Functions Accessing DB

        /// <summary>
        /// Migration of user-defined scalar-valued function fn_AllowAutoDraft
        /// </summary>
        /// <param name="policyNo"></param>
        /// <returns></returns>
        public int AllowAutoDraft(string policyNo)
        {
            return _storedProcedureComponents.TxAllowAutoDraft(policyNo);
        }

        /// <summary>
        /// Returns current Installment InstallNo (whole number) for PolicyNo
        ///
        /// Late Fees are InstallNo + 0.1
        /// Default to -1 if Null
        ///
        /// Migrated from user-defined scalar-valued function fnInstallNoCurrent
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="mailDate"></param>
        /// <returns></returns>
        public int? InstallNoCurrent(string policyNo, DateTime mailDate)
        {
            return _storedProcedureComponents.TxInstallNoCurrent(policyNo, mailDate);
        }

        /// <summary>
        /// Return next business day for a given date
        ///
        /// Stored proc didn't protect against infinite loop
        /// -- chose to limit tries here
        /// otherwise, matches stored proc logic
        ///
        /// Migrated from user-defined scalar-valued function fnNextBusinessDay
        /// </summary>
        /// <param name="day"></param>
        /// <returns>next business day</returns>
        public DateTime NextBusinessDay(DateTime day)
        {
            return _storedProcedureComponents.TxNextBusinessDay(day);
        }

        /// <summary>
        /// Get Policy Balance
        /// 
        /// Migrated from user-defined scalar-valued function fnPolicyBalance
        /// </summary>
        /// <param name="policyNo"></param>
        /// <returns></returns>
        public decimal PolicyBalance(string policyNo)
        {
            return _storedProcedureComponents.TxPolicyBalance(policyNo);
        }

        /// <summary>
        /// Check if a date is a valid business day
        ///
        /// Migrated from user-defined scalar-valued function fnPolicyBalanceValidBusinessDay
        /// </summary>
        /// <param name="dayToValidate"></param>
        /// <returns></returns>
        public bool ValidBusinessDay(DateTime dayToValidate)
        {
            return _storedProcedureComponents.TxValidBusinessDay(dayToValidate);
        }

        #endregion

        /// <summary>
        /// Copy from Policy tables into AuditPolicy tables 
        ///       for a given PolicyNo and History ID.
        ///
        /// Migration of stored Procedure spLOG_CopyPolicyToAuditTables
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="historyId"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void CopyPolicyToAuditTables(string policyNo, int historyId, string userName, string guid)
        {
            _storedProcedureComponents.TxCopyPolicyToAuditTables(policyNo, historyId, userName, guid);
        }

        /// <summary>
        /// Add entry to ErrorLog table
        /// 
        /// Following the logic in the original stored procedure, an attempt
        /// is made to insert a record into the ErrorLog table using the values
        /// passed in to the function with a system provided value if the passed-in
        /// value is null, and, in the case of strings, an empty string if the
        /// system-provided value is null.
        /// 
        /// If the insertion fails, another attempt is made this time using
        /// system-provided values with the empty string backup if null as before.
        ///
        /// Migration of spLOG_ErrorLog
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="dateTimeError">Timestamp of the error</param>
        /// <param name="errorProcedure">Where the error occurred</param>
        /// <param name="errorCode">Error # or code</param>
        /// <param name="errorDescr">Error description</param>
        /// <returns>key to entry saved to data store</returns>
        public void LogError(string guid, string policyNo, string userName, DateTime? dateTimeError,
            string errorProcedure, string errorCode, string errorDescr)
        {
            _storedProcedureComponents.TxLogError(guid, policyNo, userName, dateTimeError, errorProcedure, errorCode, errorDescr);
        }

        /// <summary>
        /// Compare two tables and save difference.
        /// 
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="table1"></param>
        /// <param name="table2"></param>
        /// <param name="historyId"></param>
        /// <param name="userName"></param>
        /// <param name="ignoreSpecialFields">Ignore changes for these special fields. @IgnoreSpecialFields = 1 for Issue Endorsement procedure, otherwise 0.</param>
        /// <param name="guid"></param>
        public void AuditSaveDifference(string policyNo, string table1, string table2, int? historyId, string userName,
            bool? ignoreSpecialFields = false, string guid = "")
        {
            _storedProcedureComponents.TxAuditSaveDifference(policyNo, table1, table2, historyId, userName, ignoreSpecialFields, guid);
        }

        /// <summary>
        /// Night Automatic Actions Process
        ///
        /// Migration of stored procedure spUW_AutomaticActionsStep1
        /// </summary>
        /// <param name="requestedReportDate"></param>
        /// <returns></returns>
        public List<PolicyList> AutomaticActionsStep1(DateTime? requestedReportDate = null)
        {
            return _storedProcedureComponents.TxAutomaticActionStep1(requestedReportDate);
        }

        /// <summary>
        /// Post automatic night actions 3
        ///
        /// Migration of stored procedure spUW_AutomaticActionsStep3
        /// </summary>
        /// <param name="requestedReportDate"></param>
        public void AutomaticActionsStep3(DateTime? requestedReportDate = null)
        {
            _storedProcedureComponents.TxAutomaticActionsStep3(requestedReportDate);
        }

        /// <summary>
        /// Delete records from Policy Endorse Quote tables for given policy #
        ///
        /// Migration of stored Procedure spUW_ClearPolicyEndorseQuote
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void ClearPolicyEndorseQuote(string policyNo, string userName, string guid)
        {
            _storedProcedureComponents.TxClearPolicyEndorseQuote(policyNo, userName, guid);
        }

        /// <summary>
        /// Delete records from Policy Ren Quote Temp tables for given policy #
        ///
        /// Migration of stored Procedure spUW_ClearPolicyRenQuoteTemp
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void ClearPolicyRenQuoteTemp(string policyNo, string userName, string guid)
        { 
            _storedProcedureComponents.TxClearPolicyRenQuoteTemp(policyNo, guid, userName);
        }

        /// <summary>
        /// Copy from PolicyEndorseQuote tables into Policy tables for a given PolicyNo
        /// 
        /// AuditPolicy tables should be updated first, before calling this procedure.
        /// 
        /// Migration of stored Procedure spUW_CopyPolicyEndorseQuoteToPolicyTables
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="fromWeb"></param>
        public void CopyPolicyEndorseQuoteToPolicyTables(string policyNo, string userName, string guid = "", bool fromWeb = false)
        {
            _storedProcedureComponents.TxCopyPolicyEndorseQuoteToPolicyTables(policyNo, userName, guid, fromWeb);
        }

        /// <summary>
        /// Copy from Policy tables into PolicyEndorseQuote tables for a given PolicyNo
        /// 
        /// Migration of stored Procedure spUW_CopyPolicyToPolicyEndorseQuoteTables
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void CopyPolicyToPolicyEndorseQuoteTables(string policyNo, string userName, string guid)
        {
            _storedProcedureComponents.TxCopyPolicyToPolicyEndorseQuoteTables(policyNo, userName, guid);
        }

        public void CreateInstallmentsPolicy(string policyNo, decimal? policyWritten, int? installCount, decimal? installNo,
            DateTime? dueDate, decimal? installmentPymt, bool? createDownPayment, bool? forceDepositToAmountPaid,
            string userName, string guid, int? historyIndexId, double? amountForForceDeposit)
        {
            _storedProcedureComponents.TxCreateInstallmentsPolicy(policyNo, policyWritten, installCount,
                installNo, dueDate, installmentPymt, createDownPayment, forceDepositToAmountPaid, userName, guid,
                historyIndexId, amountForForceDeposit);
        }

        public void CreateInstallmentsRenQuote(string policyNo, decimal? policyWritten, int? installCount, decimal? installNo,
            DateTime? dueDate, decimal? installmentPymt, bool? createDownPayment, bool? forceDepositToAmountPaid,
            string userName, string guid, int? historyIndexId, double? amountForForceDeposit)
        {
            _storedProcedureComponents.TxCreateInstallmentsRenQuote(policyNo, policyWritten, installCount,
                installNo, dueDate, installmentPymt, createDownPayment, forceDepositToAmountPaid, userName, guid,
                historyIndexId, amountForForceDeposit);
        }

        /// <summary>
        /// Gets the list of projected installments including deposit
        ///
        /// Migration of stored procedure spUW_GetListProjectedInstallments
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="ratingId"></param>
        /// <param name="payPlan"></param>
        /// <param name="guid"></param>
        public void GetListProjectedInstallments(string policyNo, int ratingId, int payPlan, Guid? guid = null)
        {
            _storedProcedureComponents.TxGetListProjectedInstallments(policyNo, ratingId, payPlan, guid);
        }

        /// <summary>
        /// Get the total amount to draft during the renewal process cycle
        /// including any past due amounts
        /// 
        /// Migration of stored procedure spUW_GetRenewalDraftAmount
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <returns>amount to draft</returns>
        public decimal GetRenewalDraftAmount(string policyNo, string userName, string guid)
        {
            return _storedProcedureComponents.TxGetRenewalDraftAmount(policyNo, userName, guid);
        }

        /// <summary>
        /// Get the total due on the policy
        ///
        /// Migrated from spUW_GetTotalDueOnPolicy
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guidErrorLog"></param>
        /// <returns></returns>
        public double? GetTotalDueOnPolicy(string policyNo, string userName, string guidErrorLog)
        {
            return _storedProcedureComponents.TxGetTotalDueOnPolicy(policyNo, userName, guidErrorLog);
        }

        /// <summary>
        /// Applies open payments to Installments
        /// 
        /// Migrated from spUW_InstallmentPaymentApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="postmarkDate"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void InstallmentPaymentApply(string policyNo, DateTime? postmarkDate, string userName, string guid)
        {
            _storedProcedureComponents.TxInstallmentPaymentApply(policyNo, postmarkDate, userName, guid);
        }

        public bool IsHoliday(DateTime dayToValidate)
        {
            return _storedProcedureComponents.TxIsHoliday(dayToValidate);
        }

        /// <summary>
        /// Issue a cancellation
        ///
        /// Migrated from spUW_IssueCancellation
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="cancellationDate"></param>
        /// <param name="flatCancel"></param>
        /// <param name="shortRate"></param>
        /// <param name="acctDate"></param>
        /// <param name="userName"></param>
        /// <param name="guidErrorLog"></param>
        /// <param name="guidHistoryIndexId"></param>
        public void IssueCancellation(string policyNo, DateTime? cancellationDate, bool flatCancel, bool shortRate, DateTime? acctDate = null,
            string userName = "", string guidErrorLog = "", string guidHistoryIndexId = "")
        {
            _storedProcedureComponents.TxIssueCancellation(policyNo, cancellationDate, flatCancel, shortRate,
                acctDate, userName, guidErrorLog, guidHistoryIndexId);
        }

        /// <summary>
        /// Issue Notice for policy
        ///
        /// Migrated from spUW_IssueNotice
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="noticeDate"></param>
        /// <param name="cancelType"></param>
        /// <param name="reason1"></param>
        /// <param name="reason2"></param>
        /// <param name="reason3"></param>
        /// <param name="reason4"></param>
        /// <param name="isNsfFeeCharged"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="mailDate"></param>
        public void IssueNotice(string policyNo, DateTime? noticeDate, string cancelType, string reason1, string reason2,
            string reason3, string reason4, string userName, DateTime? mailDate, bool isNsfFeeCharged = false,
            string guid = "")
        {
            _storedProcedureComponents.TxIssueNotice(policyNo, noticeDate, cancelType, reason1, reason2, reason3,
                reason4, userName, mailDate, isNsfFeeCharged, guid);
        }

        /// <summary>
        /// Apply Late Fee for past due current installments for a Policy
        /// 
        /// Migration of stored procedure spUW_LateFeePolicyApply
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="mailDate"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void LateFeePolicyApply(string policyNo, DateTime mailDate, string userName, string guid = "")
        {
            _storedProcedureComponents.TxLateFeePolicyApply(policyNo, mailDate, userName, guid);
        }

        /// <summary>
        /// Determine minimum due for endorsement payments
        /// 
        /// Migrated from stored procedure spUW_MinimumEndorsementPaymentDue
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="endorsementDate"></param>
        /// <param name="fullAmountOfEndorsement"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public decimal MinimumEndorsementPaymentDue(string policyNo, DateTime endorsementDate,
            decimal fullAmountOfEndorsement, string userName)
        {
            return _storedProcedureComponents.TxMinimumEndorsementPaymentDue(policyNo, endorsementDate, fullAmountOfEndorsement,
                userName);
        }

        /// <summary>
        /// Move money between policies
        /// 
        /// Migration of stored procedure spUW_MoveMoney
        /// </summary>
        /// <param name="moveFromPolicyNo"></param>
        /// <param name="moveToPolicyNo"></param>
        /// <param name="amount"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="displayDate"></param>
        /// <param name="ignoreToPolicyExists"></param>
        public void MoveMoney(string moveFromPolicyNo, string moveToPolicyNo, decimal amount, string userName, string guid = "",
            DateTime? displayDate = null, bool ignoreToPolicyExists = false)
        {
            _storedProcedureComponents.TxMoveMoney(moveFromPolicyNo, moveToPolicyNo, amount, userName, guid,
                displayDate, ignoreToPolicyExists);
        }

        /// <summary>
        /// Insert a record into PolicyFutureActions table
        /// 
        /// Migration of stored procedure spUW_PolicyFutureActions_AddRecord
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="action"></param>
        /// <param name="dueDate"></param>
        /// <param name="mailDate"></param>
        /// <param name="processDate"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        /// <param name="historyIndexId"></param>
        public void PolicyFutureActionsAddRecord(string policyNo, string action, DateTime? dueDate, DateTime? mailDate,
            DateTime? processDate, string userName, string guid = "", int? historyIndexId = null)
        {
            _storedProcedureComponents.TxPolicyFutureActionsAddRecord(policyNo, action, dueDate, mailDate,
                processDate, userName, guid, historyIndexId);
        }

        /// <summary>
        /// Policy fields formatting and cleanup
        /// 
        /// Migration of stored procedure spUW_PolicyUpdateFieldFormats
        /// </summary>
        /// <param name="policyNo"></param>
        public void PolicyUpdateFieldFormats(string policyNo)
        {
            _storedProcedureComponents.TxPolicyUpdateFieldFormats(policyNo);
        }

        /// <summary>
        ///
        /// Migration of stored procedure spUW_RatePolicyAlert
        /// </summary>
        /// <param name="currIdStr"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RatePolicyAlert(string currIdStr, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            _storedProcedureComponents.TxRatePolicyAlert(currIdStr, guid, userName, quoteId, rtrProcessing, isRenewal);
        }

        /// <summary>
        ///
        /// Not yet started migration of stored procedure spUW_RateSpartan_AA20190101
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RateSpartan_AA20190101(int currId, string guid, string userName = "ApplicationUser",
            string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            throw new NotImplementedException("RateSpartan_AA20190101 has not been implemented yet.");
        }

        /// <summary>
        ///
        /// Not yet started migration of stored procedure spUW_RateSpartan_AA20200115
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RateSpartan_AA20200115(int currId, string guid, string userName = "ApplicationUser",
            string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            throw new NotImplementedException("RateSpartan_AA20200115 has not been implemented yet.");
        }

        /// <summary>
        ///
        /// Not yet started migration of stored procedure spUW_RateSpartan_AA20201007
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RateSpartan_AA20201007(int currId, string guid, string userName = "ApplicationUser",
            string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            throw new NotImplementedException("RateSpartan_AA20201007 has not been implemented yet.");
        }

        /// <summary>
        ///
        /// Not yet started migration of stored procedure spUW_RateSpartan_AA20210412
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RateSpartan_AA20210412(int currId, string guid, string userName = "ApplicationUser",
            string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            throw new NotImplementedException("RateSpartan_AA20210412 has not been implemented yet.");
        }

        /// <summary>
        /// 
        ///  Migration of stored procedure spUW_RateSpartan_CVData
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RateSpartan_CVData(int currId, string guid, string userName,string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            _storedProcedureComponents.TxRateSpartan_CVData(currId, guid, userName, quoteId, rtrProcessing,
                isRenewal);
        }

        /// <summary>
        /// 
        ///  Migration of stored procedure spUW_RateSpartan_CVData_AA20210412
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="guid"></param>
        /// <param name="userName"></param>
        /// <param name="quoteId"></param>
        /// <param name="rtrProcessing"></param>
        /// <param name="isRenewal"></param>
        public void RateSpartan_CVData_AA20210412(int currId, string guid, string userName, string quoteId = null,
            bool rtrProcessing = false, bool? isRenewal = null)
        {
            _storedProcedureComponents.TxRateSpartan_CVData_AA20210412(currId, guid, userName, quoteId, rtrProcessing,
                isRenewal);
        }

        /// <summary>
        ///  Inserts Relativity into AA Table
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// TotalRelativities.
        /// 
        /// Migration of stored procedure spUW_TotalRelativities
        /// </summary>
        /// <param name="mainIndexId"></param>
        /// <param name="vehIndexId"></param>
        /// <param name="ratedOprIndexId"></param>
        /// <param name="relativity"></param>
        public void TotalRelativities(int mainIndexId, int vehIndexId = 0, int ratedOprIndexId = 0,
            string relativity = "")
        {
            _storedProcedureComponents.TxTotalRelativitiesBase(string.Empty, mainIndexId, vehIndexId, ratedOprIndexId, relativity);
        }

        /// <summary>
        ///  Inserts Relativity into AA Table
        ///
        /// Performs the code found in the transaction wrapping most of the method
        /// TotalRelativities_AA20210412.
        /// 
        /// Migration of stored procedure spUW_TotalRelativities_AA20210412
        /// </summary>
        /// <param name="mainIndexId"></param>
        /// <param name="vehIndexId"></param>
        /// <param name="ratedOprIndexId"></param>
        /// <param name="relativity"></param>
        public void TotalRelativities_AA20210412(int mainIndexId, int vehIndexId = 0, int ratedOprIndexId = 0,
            string relativity = "")
        {
            _storedProcedureComponents.TxTotalRelativitiesBase("AA20210412", mainIndexId, vehIndexId, ratedOprIndexId, relativity);
        }

        /// <summary>
        /// Copy Policy to PolicyRenQuote for Renewal Quote Issuance
        /// 
        /// Migration of stored procedure spUW_TranferPolicyToPolicyRenQuote[sic]
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TransferPolicyToPolicyRenQuote(string policyNo, string userName, string guid)
        {
            _storedProcedureComponents.TxTransferPolicyToPolicyRenQuote(policyNo, userName, guid);
        }

        /// <summary>
        /// Copy PolicyRenQuote to Binders for Renewal Policy Issuance
        /// 
        /// Migration of stored procedure spUW_TranferRenQuoteToPolicyBinders[sic]
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TransferRenQuoteToPolicyBinders(string policyNo, string userName, string guid)
        {
            _storedProcedureComponents.TxTransferRenQuoteToPolicyBinders(policyNo, userName, guid);

        }

        /// <summary>
        /// Copies records for given Policy # from PolicyRenQuote{type} to PolicyRenQuoteTemp{type}
        ///
        /// Where {type} is regular(blank), Cars, Drivers, and DriversViolations
        ///
        /// Migration of stored procedure spUW_TransferRenQuoteToRenQuoteTemp
        /// </summary>
        /// <param name="policyNo"></param>
        /// <param name="userName"></param>
        /// <param name="guid"></param>
        public void TransferRenQuoteToRenQuoteTemp(string policyNo, string userName, string guid)
        {
            _storedProcedureComponents.TxTransferRenQuoteToRenQuoteTempByPolicyNo(policyNo, userName, guid);
        }

        /// <summary>
        /// Loads the Audit Table for Installment Fees.
        ///
        /// Migration of stored procedure spMAINT_UpdateInstallmentAudit
        /// </summary>
        /// <param name="userName"></param>
        public void UpdateInstallmentAudit(string userName = "SYSTEM")
        {
            _storedProcedureComponents.TxUpdateInstallmentAudit(userName);
        }

        #region Support

        /// <summary>
        /// Returns RateSpartan method corresponding to the given rate cycle
        /// </summary>
        /// <param name="rateCycle"></param>
        /// <returns></returns>
        public RateSpartanMethod GetRateSpartanMethod(string rateCycle)
        {
            if (_rateSpartanMethods is null)
            {
                _rateSpartanMethods = new Dictionary<string, RateSpartanMethod>()
                {
                    { "AA20190101", RateSpartan_AA20190101 },
                    { "AA20200115", RateSpartan_AA20200115 },
                    { "AA20201007", RateSpartan_AA20201007 },
                    { "AA20210412", RateSpartan_AA20210412 }
                };
            }

            return _rateSpartanMethods[rateCycle];
        }

        #endregion

    }
}