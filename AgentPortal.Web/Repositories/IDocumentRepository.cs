﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgentPortal.Support.DataObjects;

namespace AgentPortal.Repositories
{
    public interface IDocumentRepository
    {
        PortalDocumentRequest GetDocumentRequest(string policyNo);
        void SaveDocumentRequest(PortalDocumentRequest documentRequest);
    }
}
