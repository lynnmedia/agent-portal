﻿using AgentPortal.Support.DataObjects;

namespace AgentPortal.Repositories
{
    public interface IAgentRepository
    {
        IAgents GetAgentByEmail(string emailAddress);
        IAgents GetAgentByAlternateEmail(string emailAddress);
    }
}