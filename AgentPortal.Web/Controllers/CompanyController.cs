﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;
using AgentPortal.Models;
//using AgentPortal.PalmsADServiceReference;
using AgentPortal.Support;
// using AgentPortal.Service_References.PalmsADServiceReference;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Xpo;

namespace AgentPortal.Controllers
{
    public class CompanyController : BaseController
	{
        private List<IISLogDataModel> listIISLogs = new List<IISLogDataModel>();

        public CompanyController() : base()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.CompanyLogoName) || string.IsNullOrEmpty(Sessions.Instance.FooterMiddle))
            {
                int companyID = Convert.ToInt32(ConfigSettings.ReadSetting("CompanyID"));
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    PortalThemes theme = uow.FindObject<PortalThemes>(CriteriaOperator.Parse("IndexID = ?", companyID));
                    if (theme != null)
                    {
                        Sessions.Instance.CompanyName = theme.CompanyName;
                        Sessions.Instance.CompanyLogoName = theme.LogoName;
                        Sessions.Instance.CompanyPrimaryColor = theme.PrimaryColor;
                        Sessions.Instance.CompanySecondaryColor = ""; // to do
                        Sessions.Instance.WhoWeAre = theme.WhoWeAre;
                        Sessions.Instance.FullServiceCo = theme.FullServiceCo;
                        Sessions.Instance.WhoWeRepresent = theme.WhoWeRepresent;
                        Sessions.Instance.WhyThisCompany = theme.WhyThisCompany;
                        Sessions.Instance.AboutUs = theme.AboutUs;
                        Sessions.Instance.Welcome = theme.Welcome;
                        Sessions.Instance.OurStaff = theme.OurStaff;
                        Sessions.Instance.UnderwritingAndAccounting = theme.UnderwritingAndAccounting;
                        Sessions.Instance.ClaimsManagement = theme.ClaimsManagement;
                        Sessions.Instance.SpecialUnit = theme.SpecialUnit;
                        Sessions.Instance.InformationTechnology = theme.InformationTechnology;
                        Sessions.Instance.CustomerService = theme.CustomerService;
                        Sessions.Instance.AboutImageName = theme.AboutImageName;
                        Sessions.Instance.SliderImageName1 = theme.SliderImageName1;
                        Sessions.Instance.SliderImageName2 = theme.SliderImageName2;
                        Sessions.Instance.SliderImageName3 = theme.SliderImageName3;
                        Sessions.Instance.ContactPhone = theme.ContactPhone;
                        Sessions.Instance.ContactEmail = theme.ContactEmail;
                        Sessions.Instance.FooterMiddle = theme.FooterMiddle;
                        Sessions.Instance.FooterAboutUs = theme.FooterAboutUs;
                    }
                }
            }
        }
		
        //
		// GET: /Company/
		public ActionResult Index()
		{
			if (Sessions.Instance.IsCompany)
			{
				return View();
			}
			else
			{
				return RedirectToAction("Login", "Company");
			}
		}//END Index
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public ActionResult Login()
		{
			if (Sessions.Instance.IsAuth)
			{
				return RedirectToAction("Index", "Company");
			}
			else
			{
				return View();
			}
		}//END Login
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		[HttpPost]
		public ActionResult Login(CompanyLoginModel model)
		{
   //          ADServiceClient ADClient = new ADServiceClient("WSHttpBinding_IADService");   //("WSHttpBinding_IADService");
			// ADUser User              = new ADUser();
			// User.password            = model.Password;
			// User.username            = model.UserName;
   //          //////////////////////////////
			// if (ADClient.AuthenticateAD(User))
			// {
			// 	Sessions.Instance.IsCompany = true;
			// 	Sessions.Instance.IsAuth    = true;
			// 	Sessions.Instance.Username  = model.UserName;
			// 	return RedirectToAction("Index", "Company");
			// }
			// else
			// {
			// 	ModelState.AddModelError("", "The user name or password provided is incorrect.");
			// }
            //////////////////////////////
			return View();
		}//END Login
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public ActionResult LogOut()
		{
            Session.Abandon();
			return RedirectToAction("Login", "Company");
		}//END Logout
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult Dashboard()
        {            
            return View();
        }
		public ActionResult Maps()
		{
		    using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
		    {
		        if (Sessions.Instance.IsCompany)
		        {

		            MarketingMapModel model = new MarketingMapModel();           
                  //  model.SeminoleAgents = new XPCollection<AgentGoogleSource>(model.uow, CriteriaOperator.Parse("Company = 'Seminole'"));
                    model.IbGreenAgents = new XPCollection<AgentGoogleSource>(model.uow, CriteriaOperator.Parse("Company = 'IBGreen'"));
                    model.FraudClinics = new XPCollection<AgentGoogleSource>(model.uow, CriteriaOperator.Parse("Company = 'FRAUD CLINIC'"));
                    model.AccuAutoAgents = new XPCollection<AgentGoogleSource>(model.uow, CriteriaOperator.Parse("Company = 'ACCUAUTO'"));
                    model.Agents = new XPCollection<XpoAgents>(model.uow, CriteriaOperator.Parse("AgentCode LIKE 'A0%' AND Latitude IS NOT NULL AND Longitude IS NOT NULL"));

		            return View(model);
		        }
		        else
		        {
		            return RedirectToAction("Login", "Company");
		        }
		    }
		}//END Maps
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		public ActionResult IISLogs()
		{
            if (!Sessions.Instance.IsCompany)
			{
				return RedirectToAction("Index", "Company");
			}
			else
			{
               
				return View();
			}
		}//END IISLogs
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		[HttpPost]
        public ActionResult IISLogs(IISLogSearchModel logdata)
		{
            listIISLogs.RemoveRange(0, listIISLogs.Count);
            string query = string.Format(@" SELECT
	                                            COUNT(*) AS Hits,
	                                            [c-ip],
	                                            date,
	                                            [cs-uri-stem]
                                            FROM IIS_HOTH_LOGS 
                                            WHERE 
                                                [cs-uri-stem] = '/Content/css/reset.css'  
                                            AND 
                                                CONVERT(varchar, date, 101) BETWEEN CONVERT(varchar, '{0}', 101) AND CONVERT(varchar, '{1}', 101) 
                                            AND [c-ip] NOT IN ('::1', '::1%0', '192.168.0.254 ')
                                    GROUP BY [c-ip], date, [cs-uri-stem]
                                    ORDER BY COUNT(*) DESC", logdata.StartDate.ToString("MM/dd/yyyy"), logdata.EndDate.ToString("MM/dd/yyyy"));

            SqlDataReader getLogData = Database.ExecuteQuery(query, CommandType.Text);

            while (getLogData.Read())
            {
                IISLogDataModel iisLogData = new IISLogDataModel();
                iisLogData.Date = Convert.ToDateTime(getLogData[2]);
                iisLogData.IPAddress = getLogData[1].ToString();
                iisLogData.NumOfHits = Convert.ToInt32(getLogData[0]);
                listIISLogs.Add(iisLogData);
            }

            return View(listIISLogs);
        }//END IISLogs
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult VinLookup()
        {
            if (Sessions.Instance.IsCompany)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Company");
            }
        }//END VinLookup
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	}//END Class Dec
}//END Namepsace Dec
