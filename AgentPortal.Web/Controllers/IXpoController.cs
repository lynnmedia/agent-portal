﻿using DevExpress.Xpo;

namespace AgentPortal.Controllers
{
    public interface IXpoController
    {
        Session XpoSession { get; }
    }
}