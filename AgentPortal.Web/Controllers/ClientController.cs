﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web.Mvc;
using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure;
using PalmInsure.Palms.Utilities;

namespace AgentPortal.Controllers
{
    public class ClientController : BaseController
    {
        public ClientController()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.CompanyLogoName) || string.IsNullOrEmpty(Sessions.Instance.FooterMiddle))
            {
                int companyID = Convert.ToInt32(ConfigSettings.ReadSetting("CompanyID"));
                using (var uow = new UnitOfWork())
                {
                    PortalThemes theme = uow.FindObject<PortalThemes>(CriteriaOperator.Parse("IndexID = ?", companyID));
                    if (theme != null)
                    {
                        Sessions.Instance.CompanyName = theme.CompanyName;
                        Sessions.Instance.CompanyLogoName = theme.LogoName;
                        Sessions.Instance.CompanyPrimaryColor = theme.PrimaryColor;
                        Sessions.Instance.CompanySecondaryColor = ""; // to do
                        Sessions.Instance.WhoWeAre = theme.WhoWeAre;
                        Sessions.Instance.FullServiceCo = theme.FullServiceCo;
                        Sessions.Instance.WhoWeRepresent = theme.WhoWeRepresent;
                        Sessions.Instance.WhyThisCompany = theme.WhyThisCompany;
                        Sessions.Instance.AboutUs = theme.AboutUs;
                        Sessions.Instance.Welcome = theme.Welcome;
                        Sessions.Instance.OurStaff = theme.OurStaff;
                        Sessions.Instance.UnderwritingAndAccounting = theme.UnderwritingAndAccounting;
                        Sessions.Instance.ClaimsManagement = theme.ClaimsManagement;
                        Sessions.Instance.SpecialUnit = theme.SpecialUnit;
                        Sessions.Instance.InformationTechnology = theme.InformationTechnology;
                        Sessions.Instance.CustomerService = theme.CustomerService;
                        Sessions.Instance.AboutImageName = theme.AboutImageName;
                        Sessions.Instance.SliderImageName1 = theme.SliderImageName1;
                        Sessions.Instance.SliderImageName2 = theme.SliderImageName2;
                        Sessions.Instance.SliderImageName3 = theme.SliderImageName3;
                        Sessions.Instance.ContactPhone = theme.ContactPhone;
                        Sessions.Instance.ContactEmail = theme.ContactEmail;
                        Sessions.Instance.FooterMiddle = theme.FooterMiddle;
                        Sessions.Instance.FooterAboutUs = theme.FooterAboutUs;
                    }
                }
            }
        }

        //
        // GET: /Client/
        public ActionResult Index()
        {
            return RedirectToAction("Login", "Client");
        }//END Indexa
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region Login/Logout and Registration

        public ActionResult LogOut()
        {
            Session.Abandon();

            return RedirectToAction("Login", "Client");
        }//END Login
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///

        public ActionResult Policy(string emailAddress)
        {
            if (!Sessions.Instance.IsClient)
            {
                return RedirectToAction("Login");
            }

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                if (string.IsNullOrEmpty(emailAddress) && !string.IsNullOrEmpty(Sessions.Instance.EmailAddress))
                {
                    emailAddress = Sessions.Instance.EmailAddress;
                }

                //Get policy newest policy for client
                XpoPolicy newestPolicyForClient =
                    new XPCollection<XpoPolicy>(uow,
                            CriteriaOperator.Parse("MainEmail = ? AND PolicyStatus != 'CANCEL'", emailAddress))
                        .OrderByDescending(p => p.PolicyNo).FirstOrDefault();
                if (newestPolicyForClient == null)
                {
                    return View("Login");
                }

                Sessions.Instance.PolicyNo = newestPolicyForClient.PolicyNo;
                ViewPolicyModel model = LoadPolicy(newestPolicyForClient.PolicyNo, false, uow, true);
                model.IsClient = true;
                return View("Policy", model);
            }
        }

        public ActionResult AgentInfo(string agentCode = null, string policyNo = null)
        {
            if (string.IsNullOrEmpty(agentCode) && !string.IsNullOrEmpty(policyNo))
            {
                agentCode = AgentUtils.GetAgentCode(policyNo);
            }
            if (!string.IsNullOrEmpty(agentCode))
            {
               var agentVM = AgentUtils.GetAgentModel(agentCode);
                return View(agentVM);
            }
            return View();
        }

        public ActionResult ShowPolicyById(string policyNo)
        {
            if (!Sessions.Instance.IsClient)
            {
                return RedirectToAction("Login");
            }

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                //Get policy newest policy for client
                XpoPolicy xpoPolicy =
                    uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                if (xpoPolicy == null)
                {
                    return View("Login");
                }

                Sessions.Instance.PolicyNo = xpoPolicy.PolicyNo;
                ViewPolicyModel model = LoadPolicy(xpoPolicy.PolicyNo, false, uow, true);
                model.IsClient = true;
                return View("Policy", model);
            }
        }


        public ActionResult Login(AgentLoginModel model)
        {
            if (Sessions.Instance.IsClient)
            {
                if (string.IsNullOrEmpty(model.UserName) && !string.IsNullOrEmpty(Sessions.Instance.EmailAddress))
                {
                    model.UserName = Sessions.Instance.EmailAddress;
                }
                return RedirectToAction("Policy", new { emailAddress = model.UserName });
            }

            ClientLoginModel clientModel = new ClientLoginModel();
            clientModel.EmailAddress = model.UserName;
            return View(clientModel);
        }//END Login
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        protected ViewPolicyModel LoadPolicy(string policyNo, bool isAgent, UnitOfWork uow, bool isClient = false)
        {
            ViewPolicyModel model = new ViewPolicyModel();

            SelectedPolicyDO selPolicy = new SelectedPolicyDO();
            selPolicy.agentcode = Sessions.Instance.AgentCode ?? String.Empty;
            selPolicy.policyno = policyNo;

            XpoPolicy getPolicyInfo;
            if (Sessions.Instance.AgentCode == "A00000003")
            {
                getPolicyInfo = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .FirstOrDefault();
            }
            else if (isClient)
            {
                XpoClientLogin getClientInfo = new XPCollection<XpoClientLogin>(uow,
                    CriteriaOperator.Parse("EmailAddress = ?",
                        Sessions.Instance.EmailAddress)).FirstOrDefault();

                if (getClientInfo == null)
                {
                    ViewPolicyModel nullModel = null;
                    return nullModel;
                }

                getPolicyInfo = new XPCollection<XpoPolicy>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .FirstOrDefault();
            }
            else
            {
                getPolicyInfo = new XPCollection<XpoPolicy>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", policyNo, Sessions.Instance.AgentCode))
                    .FirstOrDefault();
            }

            model.PolicyNo = getPolicyInfo.PolicyNo;
            model.PriorPolicyNo = getPolicyInfo.PriorPolicyNo;
            model.NextPolicyNo = getPolicyInfo.NextPolicyNo;
            model.RenCount = getPolicyInfo.RenCount;
            model.State = getPolicyInfo.State;
            model.LOB = getPolicyInfo.LOB;
            model.EffDate = getPolicyInfo.EffDate;
            model.ExpDate = getPolicyInfo.ExpDate;
            model.DisplayStatus = PortalUtils.GetDisplayStatus(getPolicyInfo.PolicyNo);
            model.PolicyStatus = getPolicyInfo.PolicyStatus;
            model.RenewalStatus = getPolicyInfo.RenewalStatus;
            if (!string.IsNullOrEmpty(model.RenewalStatus) && model.RenewalStatus.Equals("QTE"))
            {
                PolicyRenQuote policyRenewalQuote =
                    new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", policyNo))
                        .OrderByDescending(prq => prq.OID).FirstOrDefault();
                if (policyRenewalQuote != null)
                {
                    model.RenewalQuoteNumber = policyRenewalQuote.PolicyNo;
                    model.RenewalEffDate = policyRenewalQuote.EffDate;
                }
            }
            model.DateBound = getPolicyInfo.DateBound;
            model.DateIssued = getPolicyInfo.DateIssued;
            model.CancelDate = getPolicyInfo.CancelDate;
            model.CancelType = getPolicyInfo.CancelType;
            model.Ins1First = getPolicyInfo.Ins1First;
            PolicyPreferences languagePreference = new XPCollection<PolicyPreferences>(uow,
                CriteriaOperator.Parse("PolicyNo = ? AND Type = 'Preferred_Language'", policyNo)).FirstOrDefault();
            string preferredLanguage = languagePreference?.Value;
            if (string.IsNullOrEmpty(preferredLanguage))
            {
                preferredLanguage = "ENGLISH";
            }

            model.InsuredPreferredLanguage = preferredLanguage;
            model.Ins1Last = getPolicyInfo.Ins1Last;
            model.Ins1MI = getPolicyInfo.Ins1MI;
            model.Ins1Suffix = getPolicyInfo.Ins1Suffix;
            model.Ins1FullName = getPolicyInfo.Ins1FullName;
            model.Ins2First = getPolicyInfo.Ins2First;
            model.Ins2Last = getPolicyInfo.Ins2Last;
            model.Ins2MI = getPolicyInfo.Ins2MI;
            model.Ins2Suffix = getPolicyInfo.Ins2Suffix;
            model.Ins2FullName = getPolicyInfo.Ins2FullName;
            model.Territory = getPolicyInfo.Territory;
            model.PolicyWritten = getPolicyInfo.PolicyWritten;
            model.PolicyAnnualized = getPolicyInfo.PolicyAnnualized;
            model.CommPrem = getPolicyInfo.CommPrem;
            model.DBSetupFee = getPolicyInfo.DBSetupFee;
            model.MaintenanceFee = getPolicyInfo.MaintenanceFee;
            model.PIPPDOnlyFee = getPolicyInfo.PIPPDOnlyFee;
            model.PolicyFee = getPolicyInfo.PolicyFee;
            model.SR22Fee = getPolicyInfo.SR22Fee;
            model.FHCFFee = getPolicyInfo.FHCFFee;
            model.RateCycle = getPolicyInfo.RateCycle;
            model.SixMonth = getPolicyInfo.SixMonth;
            model.PayPlan = getPolicyInfo.PayPlan;
            model.PaidInFullDisc = getPolicyInfo.PaidInFullDisc;
            model.EFT = getPolicyInfo.EFT;
            model.MailStreet = getPolicyInfo.MailStreet;
            model.MailCity = getPolicyInfo.MailCity;
            model.MailState = getPolicyInfo.MailState;
            model.MailZip = getPolicyInfo.MailZip;
            model.MailGarageSame = getPolicyInfo.MailGarageSame;
            model.GarageStreet = getPolicyInfo.GarageStreet;
            model.GarageCity = getPolicyInfo.GarageCity;
            model.GarageState = getPolicyInfo.GarageState;
            model.GarageZip = getPolicyInfo.GarageZip;
            model.GarageTerritory = getPolicyInfo.GarageTerritory;
            model.GarageCounty = getPolicyInfo.GarageCounty;
            model.HomePhone = getPolicyInfo.HomePhone;
            model.WorkPhone = getPolicyInfo.WorkPhone;
            model.MainEmail = getPolicyInfo.MainEmail;
            model.AltEmail = getPolicyInfo.AltEmail;
            model.Paperless = getPolicyInfo.Paperless;
            model.AgentCode = getPolicyInfo.AgentCode;
            model.CommAtIssue = getPolicyInfo.CommAtIssue;
            model.LOUCommAtIssue = getPolicyInfo.LOUCommAtIssue;
            model.ADNDCommAtIssue = getPolicyInfo.ADNDCommAtIssue;
            model.AgentGross = getPolicyInfo.AgentGross;
            model.PIPDed = getPolicyInfo.PIPDed;
            model.NIO = getPolicyInfo.NIO;
            model.NIRR = getPolicyInfo.NIRR;
            model.UMStacked = getPolicyInfo.UMStacked;
            model.HasBI = getPolicyInfo.HasBI;
            model.HasMP = getPolicyInfo.HasMP;
            model.HasUM = getPolicyInfo.HasUM;
            model.HasLOU = getPolicyInfo.HasLOU;
            model.BILimit = getPolicyInfo.BILimit;
            model.PDLimit = getPolicyInfo.PDLimit;
            model.UMLimit = getPolicyInfo.UMLimit;
            model.MedPayLimit = getPolicyInfo.MedPayLimit;
            model.Homeowner = getPolicyInfo.Homeowner;
            model.RenDisc = getPolicyInfo.RenDisc;
            model.TransDisc = (TransferDiscountOptions)getPolicyInfo.TransDisc;
            model.PreviousCompany = getPolicyInfo.PreviousCompany;
            model.PreviousExpDate = getPolicyInfo.PreviousExpDate;
            model.PreviousBI = getPolicyInfo.PreviousBI;
            model.PreviousBalance = getPolicyInfo.PreviousBalance;
            model.DirectRepairDisc = getPolicyInfo.DirectRepairDisc;
            model.UndTier = getPolicyInfo.UndTier;
            model.OOSEnd = getPolicyInfo.OOSEnd;
            model.LOUCost = getPolicyInfo.LOUCost;
            model.LOUAnnlPrem = getPolicyInfo.LOUAnnlPrem;
            model.ADNDLimit = getPolicyInfo.ADNDLimit;
            model.ADNDCost = getPolicyInfo.ADNDCost;
            model.ADNDAnnlPrem = getPolicyInfo.ADNDAnnlPrem;
            model.HoldRtn = getPolicyInfo.HoldRtn;
            model.NonOwners = getPolicyInfo.NonOwners;
            model.ChangeGUID = getPolicyInfo.ChangeGUID;
            model.policyCars = getPolicyInfo.policyCars;
            model.policyDrivers = getPolicyInfo.policyDrivers;
            model.Unacceptable = getPolicyInfo.Unacceptable;
            model.HousholdIndex = getPolicyInfo.HousholdIndex;
            model.RatingID = getPolicyInfo.RatingID;
            model.OID = getPolicyInfo.OID;
            model.Cars = getPolicyInfo.Cars;
            model.Drivers = getPolicyInfo.Drivers;
            model.IsReturnedMail = getPolicyInfo.IsReturnedMail;
            model.IsOnHold = getPolicyInfo.IsOnHold;
            model.IsClaimMisRep = getPolicyInfo.IsClaimMisRep;
            model.IsNoREI = getPolicyInfo.IsNoREI;
            model.IsSuspense = getPolicyInfo.IsSuspense;
            model.isAnnual = getPolicyInfo.isAnnual;
            model.Carrier = getPolicyInfo.Carrier;
            model.HasPriorCoverage = getPolicyInfo.HasPriorCoverage;
            model.HasPriorBalance = getPolicyInfo.HasPriorBalance;
            model.HasLapseNone = getPolicyInfo.HasLapseNone;
            model.HasPD = getPolicyInfo.HasPD;
            model.HasTOW = getPolicyInfo.HasTOW;
            model.PIPLimit = getPolicyInfo.PIPLimit;
            model.HasADND = getPolicyInfo.HasADND;
            model.MVRFee = getPolicyInfo.MVRFee;
            model.WorkLoss = getPolicyInfo.WorkLoss;
            model.RentalAnnlPrem = getPolicyInfo.RentalAnnlPrem;
            model.RentalCost = getPolicyInfo.RentalCost;
            model.HasLapse110 = getPolicyInfo.HasLapse110;
            model.HasLapse1131 = getPolicyInfo.HasLapse1131;
            model.AppQuestionAnswers = getPolicyInfo.AppQuestionAnswers;
            model.AppQuestionExplainOne = getPolicyInfo.AppQuestionExplainOne;
            model.AppQuestionExplainTwo = getPolicyInfo.AppQuestionExplainTwo;
            model.FromPortal = getPolicyInfo.FromPortal;
            model.FromRater = getPolicyInfo.FromRater;
            model.QuoteNo = getPolicyInfo.QuoteNo;
            model.AgencyRep = getPolicyInfo.AgencyRep;
            model.QuotedDate = getPolicyInfo.QuotedDate;
            model.Bridged = getPolicyInfo.Bridged;
            model.IsAllowRenPastExp = getPolicyInfo.IsAllowRenPastExp;
            model.AllowPymntWithin15Days = getPolicyInfo.AllowPymntWithin15Days;

            if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
            {
                var clientUploads = new XPCollection<AuditImageIndex>(uow,
                                    CriteriaOperator.Parse("FileNo = ? AND FormDesc = 'CLIENT UPLOAD'", policyNo)).ToList();
                model.AgentUploads = clientUploads;
            }
            else
            {
                var agentUploads = new XPCollection<AuditImageIndex>(uow,
                            CriteriaOperator.Parse("FileNo = ? AND FormDesc = 'AGENT UPLOAD'", policyNo)).ToList();
                model.AgentUploads = agentUploads;
            }
            //------------------------------------------
            //Get Renewal Info
            XPCollection<PolicyRenQuote> getRenQuote =
                new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", model.PolicyNo));
            model.HasRenewal = getRenQuote.Count > 0 ? true : false;

            if (model.HasRenewal)
            {
                model.RenEffDate = getRenQuote.FirstOrDefault().EffDate;
                model.RenPymntPlan = getRenQuote.FirstOrDefault().PayPlan;
                model.RenPremium = getRenQuote.FirstOrDefault().PolicyWritten.ToMoney<double>();
                model.RenQuoteAnnual = getRenQuote.FirstOrDefault().isAnnual;
                model.RenQuoteSixMonth = getRenQuote.FirstOrDefault().SixMonth;
                AuditPolicyHistory renewalDepsoitRecord = new XPCollection<AuditPolicyHistory>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND Action = 'RDP'", model.PolicyNo)).OrderByDescending(aph => aph.IndexID).FirstOrDefault();
                if (renewalDepsoitRecord != null)
                {

                    model.RenDepositAmt = renewalDepsoitRecord.NoticeAmount.ToString("C");
                    model.RenMinDueAmt = (decimal)renewalDepsoitRecord.NoticeAmount;
                }

                XPCollection<RenQuoteInstallments> getIntalls = new XPCollection<RenQuoteInstallments>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND IgnoreRec = '0'", getRenQuote.FirstOrDefault().PolicyNo));
                foreach (RenQuoteInstallments install in getIntalls.Where(m => m.Descr == "Installment 1"))
                {
                    model.MonthlyRenewalPymnt = install.InstallmentPymt.SafeString().Parse<double>();
                }

                foreach (var install in getIntalls)
                {
                    model.RenInstallFees += install.InstallmentFee.SafeString().Parse<double>();
                }

                model.RenInstallFees = model.RenInstallFees + getRenQuote.FirstOrDefault().PolicyWritten;

                XPCollection<XpoAARatePayPlans> getPayPlans = new XPCollection<XpoAARatePayPlans>(uow);

                bool isAllowLowDp = PortalRightsUtils.HasRights("LOW DOWN PAYMENT", Sessions.Instance.AgentCode, true);

                if (isAllowLowDp)
                {
                    getPayPlans.Criteria = CriteriaOperator.Parse(
                        "RateCycle = ? AND Term = ? AND NewOrRen <> 'N'",
                        getRenQuote.FirstOrDefault().RateCycle,
                        getRenQuote.FirstOrDefault().SixMonth ? "6" : "12");
                }
                else
                {
                    getPayPlans.Criteria = CriteriaOperator.Parse(
                        "RateCycle = ? AND Term = ? AND NewOrRen <> 'N'", // AND Active = '1'",
                        getRenQuote.FirstOrDefault().RateCycle,
                        getRenQuote.FirstOrDefault().SixMonth ? "6" : "12");
                }

                model.RenPayPlans = new System.Collections.Generic.List<SelectListItem>();
                foreach (XpoAARatePayPlans plan in getPayPlans)
                {
                    SelectListItem item = new SelectListItem();
                    item.Text = PortalUtils.GetEnglishPayPlan(model.RateCycle, plan.IndexID.SafeString());
                    item.Value = plan.IndexID.SafeString();
                    item.Selected = plan.IndexID == model.RenPymntPlan ? true : false;

                    model.RenPayPlans.Add(item);
                }
            }

            //------------------------------------------
            //Get Coverage Info
            //ViewData["ViewPolCoverage"] = PalmsClient.GetCoverageInfo(selPolicy);
            model.GetCoverageInfo = PolicyWebUtils.GetCoverageInfo(model.PolicyNo);
            //------------------------------------------
            model.BillingGuid = Guid.NewGuid();
            UtilitiesStoredProcedure.ExecspUW_PrepBillingTab(new Session(), model.PolicyNo, Sessions.Instance.Username, model.BillingGuid, true);

            foreach (XpoAuditBillingInfo bill in model.GetBillingInfo.OrderByDescending(m => m.IndexID)
                .Where(m => m.RowDesc.Contains("Installment") || m.RowDesc.Contains("Deposit")).Take(1))
            {
                model.DateDue = bill.RowDate;
            }

            model.TotalDue =
                PalmsClient.ExecspUW_GetTotalDueOnPolicy(model.PolicyNo, "web-user", Guid.NewGuid().ToString());

            if (model.GetBillingInfo.Count > 0)
                model.MinDue = model.GetBillingInfo[model.GetBillingInfo.Count - 1].MinDue;
            model.TotalPremium = model.GetBillingInfo.Sum(m => m.AmtBilled);
            model.TotalPaid = model.GetBillingInfo.Sum(m => m.Payment);
            model.TotalFees = model.GetBillingInfo.Sum(m => m.InstallFee) + model.GetBillingInfo.Sum(m => m.NSFFee) +
                              model.GetBillingInfo.Sum(m => m.LateFee);

            model.AmtToPay = Math.Round(model.MinDue, 2, MidpointRounding.AwayFromZero) < 0.0
                ? 0.0
                : Math.Round(model.MinDue, 2, MidpointRounding.AwayFromZero);
            if (model.AmtToPay == 0)
            {
                model.AmtToPay = -model.MinDue;
            }

            //------------------------------------------
            //Get History
            model.GetHistory = PolicyWebUtils.GetHistory(model.PolicyNo);
            model.HistoryReasons = PolicyWebUtils.GetHistoryReasons(model.PolicyNo);
            //------------------------------------------
            decimal priorAmt = model.TotalDue.SafeString().Parse<decimal>();
            //------------------------------------------
            model.RenMinDueAmt = priorAmt + model.RenMinDueAmt;
            model.RenMinDueAmtStr = model.RenMinDueAmt.ToMoney<decimal>();
            //------------------------------------------
            //string decpage = String.Format(@"C:\inetpub\wwwroot\Content\files\policydocs\{0}_DecPage.pdf", selPolicy.policyno);
            string decpage =
                String.Format(
                    Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_DecPage.pdf"),
                    selPolicy.policyno);
            // using (Stream stream = new FileStream(decpage, FileMode.Create))
            // {
            //     try
            //     {
            //         byte[] decBytes = PalmsClient.GetPolicyDocs(selPolicy.policyno, "DEC");
            //
            //         stream.Write(decBytes, 0, decBytes.Length);
            //     }
            //     catch (Exception ex)
            //     {
            //     }
            // }

            string idcards =
                String.Format(
                    Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_IDCard.pdf"),
                    selPolicy.policyno);

            // using (Stream stream = new FileStream(idcards, FileMode.Create))
            // {
            //     try
            //     {
            //         byte[] decBytes = PalmsClient.GetPolicyDocs(selPolicy.policyno, "IDCARD");
            //
            //         stream.Write(decBytes, 0, decBytes.Length);
            //     }
            //     catch (Exception ex)
            //     {
            //     }
            // }

            string invoice =
                String.Format(
                    Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_Invoice.pdf"),
                    selPolicy.policyno);
            // using (Stream stream = new FileStream(invoice, FileMode.Create))
            // {
            //     try
            //     {
            //         byte[] decBytes = PalmsClient.GetPolicyDocs(selPolicy.policyno, "INVOICE");
            //
            //         stream.Write(decBytes, 0, decBytes.Length);
            //     }
            //     catch (Exception ex)
            //     {
            //     }
            // }

            string pymntsched =
                String.Format(
                    Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") +
                                   @"policydocs/{0}_PaymentSchedule.pdf"), selPolicy.policyno);
            // using (Stream stream = new FileStream(pymntsched, FileMode.Create))
            // {
            //     try
            //     {
            //         byte[] decBytes = PalmsClient.GetPolicyDocs(selPolicy.policyno, "PYMNTSCHEDULE");
            //
            //         stream.Write(decBytes, 0, decBytes.Length);
            //     }
            //     catch (Exception ex)
            //     {
            //     }
            // }

            string renewalDec =
                String.Format(
                    Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") + @"policydocs/{0}_RenewalDec.pdf"),
                    selPolicy.policyno);
            // using (Stream stream = new FileStream(renewalDec, FileMode.Create))
            // {
            //     try
            //     {
            //         byte[] decBytes = GetPolicyDocs(selPolicy.policyno, "RENDEC");
            //         if (decBytes.Length > 100)
            //         {
            //             stream.Write(decBytes, 0, decBytes.Length);
            //         }
            //         else
            //         {
            //             model.RenDecFilename = "NONE";
            //         }
            //     }
            //     catch (Exception ex)
            //     {
            //     }
            // }

            string renewalPaySchd =
                String.Format(
                    Server.MapPath(ConfigSettings.ReadSetting("ContentFilesPath") +
                                   @"policydocs/{0}_RenewalPaySchedule.pdf"), selPolicy.policyno);
            // using (Stream stream = new FileStream(renewalPaySchd, FileMode.Create))
            // {
            //     try
            //     {
            //         byte[] decBytes = PalmsClient.GetPolicyDocs(selPolicy.policyno, "RENPYMNTSCHEDULE");
            //
            //         if (decBytes.Length > 100)
            //         {
            //             stream.Write(decBytes, 0, decBytes.Length);
            //         }
            //         else
            //         {
            //             model.RenPaySchdFilename = "NONE";
            //         }
            //     }
            //     catch (Exception ex)
            //     {
            //     }
            // }

            if (System.IO.File.Exists(decpage))
            {
                model.decpagefilename = Url.Content(decpage);
                model.decpagedatetime = System.IO.File.GetCreationTime(decpage).ToShortDateString();
            }

            //------------------------------------------
            if (System.IO.File.Exists(Url.Content(idcards)))
            {
                model.idcarddatetime = System.IO.File.GetCreationTime(idcards).ToShortDateString();
                model.idcardfilename = Url.Content(idcards);
            }

            //------------------------------------------
            if (System.IO.File.Exists(Url.Content(invoice)))
            {
                model.invoicedatetime = System.IO.File.GetCreationTime(invoice).ToShortDateString();
                model.invoicefilename = Url.Content(invoice);
            }

            //------------------------------------------
            if (System.IO.File.Exists(Url.Content(pymntsched)))
            {
                model.pymtnscheduledatetime = System.IO.File.GetCreationTime(pymntsched).ToShortDateString();
                model.pymntschedule = Url.Content(pymntsched);
            }

            //------------------------------------------
            if (System.IO.File.Exists(Url.Content(renewalDec)) && model.RenDecFilename != "NONE")
            {
                model.RenDecDateTime = System.IO.File.GetCreationTime(renewalDec).ToShortDateString();
                model.RenDecFilename = Url.Content(renewalDec);
            }

            //------------------------------------------
            if (System.IO.File.Exists(Url.Content(renewalPaySchd)) && model.RenPaySchdFilename != "NONE")
            {
                model.RenPaySchdDateTime = System.IO.File.GetCreationTime(renewalPaySchd).ToShortDateString();
                model.RenPaySchdFilename = Url.Content(renewalPaySchd);
            }

            //------------------------------------------
            var getReasons = new XPCollection<XpoAuditPolicyHistoryReasons>(uow,
                CriteriaOperator.Parse("PolicyNo = ? AND HistoryAction = 'HOLD'", policyNo)).FirstOrDefault();
            if (getReasons != null)
            {
                model.isOnHold = true;
            }

            //------------------------------------------
            //Check If Policy Is or Has Been Renewed
            XPCollection<XpoPolicy> getIfRenewal =
                new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.PriorPolicyNo));
            model.IsRenewalPolicy = getIfRenewal.Count > 0 ? true : false;

            string prevPolicyNo = model.PolicyNo.GenPriorPolicyNo();
            string nextPolicyNo = model.PolicyNo.GenNextPolicyNo();


            XPCollection<XpoPolicy> getOldPolicy =
                new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", prevPolicyNo));
            int oldPolicyCount = getOldPolicy.Count;
            prevPolicyNo = oldPolicyCount == 0 ? prevPolicyNo.Split(new char[] { '-' })[0] : prevPolicyNo;

            int priorPolicyCount = model.PolicyNo == prevPolicyNo
                ? 0
                : new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", prevPolicyNo)).Count();

            int nextPolicyCount = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", nextPolicyNo))
                .Count();

            model.GenNextPolicyNo = nextPolicyCount == 0 ? "" : nextPolicyNo;
            model.GenPrevPolicyNo = priorPolicyCount == 0 ? "" : prevPolicyNo;

            PalmsClient.CloseRef();
            return model;
        }

        public ActionResult ForgotPassword()
        {
            return View("ResetPassword",new PasswordResetModel() {  IsForgotPassword = true });

        }

        [HttpPost]
        public ActionResult Login(ClientLoginModel input)
        {
            if (Sessions.Instance.IsClient)
            {
                return RedirectToAction("Policy", new { emailAddress = input.EmailAddress });
            }

            bool success = CheckClientLogin(input.EmailAddress, input.Password);

            if (!success) 
            {
                ModelState.AddModelError("EmailAddress", "Invalid Email Address or Password");
                ModelState.AddModelError("Password", "Invalid Email Address or Password");
                return View(input);
            }
            Sessions.Instance.IsAuth = true;
            Sessions.Instance.IsClient = true;
            Sessions.Instance.EmailAddress = input.EmailAddress;
            PalmsClient.CloseRef();
            return RedirectToAction("Policy", new { emailAddress = input.EmailAddress });

        }//END Login
         ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private bool CheckClientLogin(string userName, string password)
         {
             if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
             {
                 return false;
             }

             bool isValidLogin = false;
             XpoClientLogin loginForUser = null;
             using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
             {
                 loginForUser =
                     new XPCollection<XpoClientLogin>(uow,
                             CriteriaOperator.Parse("EmailAddress = ? AND IsActive = 1 AND IsValidated = 1", userName))
                         .FirstOrDefault();


                 if (loginForUser != null)
                 {
                     string hashedpass = loginForUser.HashedPassword;
                     hashedpass = Blowfish.Decrypt_CBC(hashedpass);
                     string encPass = Blowfish.Encrypt_CBC(password);
                     string userPassHash = password.Trim();
                     if (userPassHash.Equals(hashedpass))
                     {
                         isValidLogin = true;
                     }
                     else
                     {
                         isValidLogin = false;
                     }
                 }
             }

             return isValidLogin;
         }

        private string GetClientPolicyNumber(string emailAddress)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicy currentPolicyForClient = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("MainEmail = ? AND Status = 'ACTIVE'", emailAddress)).OrderByDescending(p => p.PolicyNo).FirstOrDefault();
                return currentPolicyForClient?.PolicyNo;
            }
        }

        public ActionResult Register()
        {
            return View();
        }//END Register
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // private ClientRegistrationError RegisterClient(ClientRegisterModel input)
        // {
        //     using (UnitOfWork uow = new UnitOfWork())
        //     {
        //         int emailCount = 0;
        //         XPCollection<ClientLogin> getClientData = new XPCollection<ClientLogin>(uow, CriteriaOperator.Parse("EmailAddress = ?", input.EmailAddress));
        //         emailCount = getClientData.Count;
        //         ////////////////////////////////////////////////////////
        //         //Validate registration fields against policy record
        //         if (emailCount != 0)
        //         {
        //             return ClientRegistrationError.EmailInUse;
        //         }
        //         
        //         if (!String.IsNullOrEmpty(input.PolicyNumber))
        //         {
        //             Policy getPolicyData = new XPCollection<Policy>(uow, CriteriaOperator.Parse("PolicyNo = ?", input.PolicyNumber)).FirstOrDefault();
        //             if (getPolicyData == null)
        //             {
        //                 return ClientRegistrationError.InvalidPolicy;
        //             }
        //         }
        //         
        //         if(!string.IsNullOrEmpty(input.Firstname) && !string.IsNullOrEmpty(input.Lastname))
        //         {
        //             Policy getPolicy = new XPCollection<Policy>(uow, CriteriaOperator.Parse("Ins1First = ? AND Ins1Last = ?", input.Firstname, input.Lastname)).FirstOrDefault();
        //             if (getPolicy == null)
        //             {
        //                 return ClientRegistrationError.InvalidName;
        //             }
        //         }
        //         
        //         if (!string.IsNullOrEmpty(input.DateOfBirth.ToShortDateString()))
        //         {
        //             PolicyDrivers getDrivers = new XPCollection<PolicyDrivers>(uow, CriteriaOperator.Parse("DOB = ?", input.DateOfBirth)).FirstOrDefault();
        //             if (getDrivers == null)
        //             {
        //                 return ClientRegistrationError.InvalidDob;
        //             }
        //         }
        //
        //         ////////////////////////////////////////////////////////
        //         // We're good to go, generate a random password.
        //         string validationCode = Guid.NewGuid().ToString();
        //         string randomPassword = validationCode.Split("-".ToCharArray(), 1).First();
        //         ////////////////////////////////////////////////////////
        //         ClientLogin client = new ClientLogin()
        //         {
        //             EmailAddress     = input.EmailAddress,
        //             SecurityQuestion = input.SecurityQuestion,
        //             SecurityAnswer   = input.SecurityAnswer,
        //             PolicyNumber     = input.PolicyNumber,
        //             ValidationCode   = validationCode,
        //             HashedPassword   = Security.SHA512(randomPassword, null)
        //         };
        //         client.Save();
        //         PalmsClient.CloseRef();
        //         return ClientRegistrationError.None;
        //     }
        // }//END RegisterClient
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // [HttpPost]
        // public ActionResult Register(ClientRegisterModel input)
        // {
        //     ClientRegistrationError error = RegisterClient(input);
        //
        //     if (error == ClientRegistrationError.None) 
        //     {
        //         string clientName = new XPCollection<Policy>(CriteriaOperator.Parse("PolicyNo = ?", input.PolicyNumber)).FirstOrDefault().Ins1First;
        //         string validationCode = new XPCollection<ClientLogin>(CriteriaOperator.Parse("EmailAddress = ?", input.EmailAddress)).FirstOrDefault().ValidationCode;
        //         try
        //         {
        //             
        //             // EmailNoReply.SendMail(
        //             //     input.EmailAddress,
        //             //     "noreply@mydemoinsco.com",
        //             //     "Verify your DIC registration",
        //             //     EmailRenderer.RegistrationValidation(clientName, validationCode, System.Web.HttpContext.Current.Request.Url.Authority),
        //             //     true);
        //         }
        //         catch(Exception ex)
        //         {
        //             LogUtils.Log(Sessions.Instance.Username, "SENDING OF EMAIL FAILED: " + ex.Message, "FATAL");
        //         }
        //
        //         return View("RegisterSuccess", input);
        //     }
        //
        //     switch (error)
        //     {
        //         case ClientRegistrationError.PolicyInUse:
        //             ModelState.AddModelError("PolicyNumber", "There is already an account tied to this policy");
        //             break;
        //         case ClientRegistrationError.InvalidDob:
        //             ModelState.AddModelError("DateOfBirth", "The date of birth provided does not match the date of birth on record for this policy");
        //             break;
        //         case ClientRegistrationError.InvalidName:
        //             ModelState.AddModelError("Firstname", "The name provided does not match the name on record for this policy");
        //             ModelState.AddModelError("Lastname", "The name provided does not match the name on record for this policy");
        //             break;
        //         case ClientRegistrationError.InvalidPolicy:
        //             ModelState.AddModelError("PolicyNumber", "We could not find the specified policy in our records");
        //             break;
        //         case ClientRegistrationError.InvalidLicense:
        //             ModelState.AddModelError("DriversLicense", "We could not find the specified drivers license in our records");
        //             break;
        //         case ClientRegistrationError.EmailInUse:
        //             ModelState.AddModelError("EmailAddress", "There is already an account registered with this Email Address");
        //             break;
        //     }
        //
        //     return View(input);
        // }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult SetPassword(string key,string reset=null)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                var model = new ClientSetPasswordModel()
                {
                    ValidationCode = key
                };

                //Ensure validation code is valid
                if (string.IsNullOrEmpty(key))
                {
                    PalmsClient.CloseRef();
                    ModelState.AddModelError("ValidationCode", "The provided validation code is invalid");
                    return View(model);
                }

                string decryptedCode = Blowfish.Decrypt_CBC(key);
                XpoClientLogin existingValidationCode =
                    uow.FindObject<XpoClientLogin>(CriteriaOperator.Parse("ValidationCode = ?", decryptedCode));
                if (existingValidationCode == null)
                {
                    ModelState.AddModelError("ValidationCode", "The provided validation code is invalid");
                }
                else if (reset == "1")
                {
                    model.IsResetPassword = true;
                    model.EmailAddress = existingValidationCode.EmailAddress;
                }
                else if (existingValidationCode.IsActive && existingValidationCode.IsValidated)
                {
                    AgentLoginModel loginModel = new AgentLoginModel();
                    model.EmailAddress = existingValidationCode.EmailAddress;
                    return RedirectToAction("Login", model);
                }
                else
                {
                    model.EmailAddress = existingValidationCode.EmailAddress;
                }
                PalmsClient.CloseRef();
                return View(model);
            }
        } //END SetPassword
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [HttpPost]
        public ActionResult SetPassword(string emailAddress, ClientSetPasswordModel input)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                if (SetClientPassword(input.EmailAddress, input.NewPassword, input.IsResetPassword))
                {
                    Sessions.Instance.IsAuth = true;
                    Sessions.Instance.IsClient = true;
                    Sessions.Instance.EmailAddress = input.EmailAddress;
                    return RedirectToAction("Policy", "Client", new { emailAddress = input.EmailAddress });
                }

                PalmsClient.CloseRef();
                ModelState.AddModelError("ValidationCode", "Validation code not found or has expired.");
                return View();
            }

        }//END SetPassword
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///
        private bool SetClientPassword(string emailAddress, string newPassword, bool isResetPassword=false)
        {
            if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(newPassword))
            {
                return false;
            }

            using (UnitOfWork uow = new UnitOfWork())
            {
                var criteria = CriteriaOperator.Parse("EmailAddress = ? AND IsActive = 0 AND IsValidated = 0", emailAddress);
                if (isResetPassword)
                    criteria = CriteriaOperator.Parse("EmailAddress = ?", emailAddress);
                XpoClientLogin existingLogin = uow.FindObject<XpoClientLogin>(criteria);
                if (existingLogin == null)
                {
                    return false;
                }
                existingLogin.HashedPassword = Blowfish.Encrypt_CBC(newPassword);
                existingLogin.IsValidated = true;
                existingLogin.IsActive = true;
                existingLogin.Save();
                uow.CommitChanges();
                return true;
            }
        }

        public ActionResult ResetPassword()
        {
            if (!Sessions.Instance.IsClient)
            {
                return RedirectToAction("Login");
            }
            var model = new PasswordResetModel()    
            {
                EmailAddress = Sessions.Instance.EmailAddress,
                IsResetPassword=true
            };
            return View(model);
        }//END ResetPassword
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [HttpPost]
        public ActionResult ResetPassword(PasswordResetModel input, string PasswordResetType)
        {

            if (string.IsNullOrEmpty(PasswordResetType))
            {
                ModelState.AddModelError("EmailAddress", "Server Error Occured, please try agian!");
                return View(input);
            }
            if (PasswordResetType.ToLower() == "send email")
            {
                // send email
                if (string.IsNullOrEmpty(input.EmailAddress))
                {
                    ModelState.AddModelError("EmailAddress", "Please Enter Email Address!");
                    return View(input);
                }
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    var existingClientAccount = uow.FindObject<XpoClientLogin>(CriteriaOperator.Parse("EmailAddress = ?", input.EmailAddress));
                    if (existingClientAccount != null)
                    {
                        using (SHA256 hashTool = SHA256.Create())
                        {
                            existingClientAccount.ValidationCode = Guid.NewGuid().ToString();
                            string hashedGuid = Blowfish.Encrypt_CBC(existingClientAccount.ValidationCode);
                            existingClientAccount.Save();
                            uow.CommitChanges();
                            string link =
                                $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Client/SetPassword?key={hashedGuid}&reset=1";
                            EmailService.SendEmailNotification(new List<string>() { existingClientAccount.EmailAddress },
                                "Reset your Password to access auto insurance policy with Palm Insure!",
                                $@"<div><p>The Palm Insure client portal is where you can access your new auto insurance policy documents and make payments. You have requested to reset the password for your account.  Please click the link below to reset your password to access our client portal. </p></div><br/><div> <a href='{link}'>Click here to Reset Password for your account.</a></div><br/>",
                                true);
                            //ViewBag.ForgotPasswordMsg = $"Email sent on {existingClientAccount.EmailAddress} to reset your password.";
                            return View(new PasswordResetModel() { IsForgotPassword = true, IsPasswordEmailSent=true, EmailAddress = existingClientAccount.EmailAddress });
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("EmailAddress", "There is no account exists with this Email address.");
                        return View(input);
                    }
                }
            }
            else if (PasswordResetType.ToLower() == "reset password")
            {
                if (!ModelState.IsValid)
                {
                    return View(input);
                }
                var isOldPasswordValid = CheckClientLogin(input.EmailAddress, input.OldPassword);
                if (!isOldPasswordValid)
                {
                    ModelState.AddModelError("OldPassword", "The Entered current password is not correct.");
                    return View(input);
                }
                else if(input.OldPassword == input.NewPassword)
                {
                    ModelState.AddModelError("NewPassword", "The Entered new password and current password are same.");
                    return View(input);
                }

                if (SetClientPassword(input.EmailAddress, input.NewPassword, input.IsResetPassword))
                {
                    Session.Abandon();
                    return RedirectToAction("Login");
                }
                ModelState.AddModelError("NewPassword", "Unable to set the new password. Please try again!");
                return View(input);
            }
            return View(input);
        }//END ResetPassword
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #endregion

        public ActionResult Payment(MakePaymentModel model)
        {
            if (!Sessions.Instance.IsClient)
            {
                return RedirectToAction("Login");
            }
            //////////////////
            model.PolicyNo = Sessions.Instance.PolicyNumber;
            if (model.PaymentAmt == 0)
            {
                return RedirectToAction("Policy");
            }

            PaymentService.HandlePaymentModel(model);

            return View(model);
        }//END Payment
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }//END Class Dec
}//END Namespace Dec
