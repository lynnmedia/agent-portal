﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Repositories.StoredProcedures;
using AgentPortal.Services;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using log4net;
using PalmInsure.Palms.Utilities;


// ReSharper disable InconsistentNaming
namespace AgentPortal.Controllers
{
    public class AgentController : BaseController
    {
        private static readonly SSRSHelpers ssrsHelpers = new SSRSHelpers();

        public AgentController()
        {
            AgentControllerLog = LogManager.GetLogger("AgentControllerLogger");
            //PalmsServiceClient PalmsClient = new PalmsServiceClient("BasicHttpBinding_IPalmsService");
            if (string.IsNullOrEmpty(Sessions.Instance.CompanyLogoName) ||
                string.IsNullOrEmpty(Sessions.Instance.FooterMiddle))
            {
                var companyID = Convert.ToInt32(ConfigSettings.ReadSetting("CompanyID"));
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var theme = uow.FindObject<PortalThemes>(CriteriaOperator.Parse("IndexID = ?", companyID));
                    if (theme != null)
                    {
                        Sessions.Instance.CompanyName = theme.CompanyName;
                        Sessions.Instance.CompanyLogoName = theme.LogoName;
                        Sessions.Instance.CompanyPrimaryColor = theme.PrimaryColor;
                        Sessions.Instance.CompanySecondaryColor = ""; // to do
                        Sessions.Instance.WhoWeAre = theme.WhoWeAre;
                        Sessions.Instance.FullServiceCo = theme.FullServiceCo;
                        Sessions.Instance.WhoWeRepresent = theme.WhoWeRepresent;
                        Sessions.Instance.WhyThisCompany = theme.WhyThisCompany;
                        Sessions.Instance.AboutUs = theme.AboutUs;
                        Sessions.Instance.Welcome = theme.Welcome;
                        Sessions.Instance.OurStaff = theme.OurStaff;
                        Sessions.Instance.UnderwritingAndAccounting = theme.UnderwritingAndAccounting;
                        Sessions.Instance.ClaimsManagement = theme.ClaimsManagement;
                        Sessions.Instance.SpecialUnit = theme.SpecialUnit;
                        Sessions.Instance.InformationTechnology = theme.InformationTechnology;
                        Sessions.Instance.CustomerService = theme.CustomerService;
                        Sessions.Instance.AboutImageName = theme.AboutImageName;
                        Sessions.Instance.SliderImageName1 = theme.SliderImageName1;
                        Sessions.Instance.SliderImageName2 = theme.SliderImageName2;
                        Sessions.Instance.SliderImageName3 = theme.SliderImageName3;
                        Sessions.Instance.ContactPhone = theme.ContactPhone;
                        Sessions.Instance.ContactEmail = theme.ContactEmail;
                        Sessions.Instance.FooterMiddle = theme.FooterMiddle;
                        Sessions.Instance.FooterAboutUs = theme.FooterAboutUs;
                    }
                }
            }
        }

        public ILog AgentControllerLog { get; }

        public JsonResult ValidateIfDriverHasMvrForEndorsement(string driverIndex)
        {
            var hasMvr = AgentService.HasMvr(driverIndex);
            return Json(new {returnMsg = "Success", hasMvr}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return RedirectToAction("Login", "Agent");
        } //END Index
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult InfoSheet()
        {
            if (Request.IsAjaxRequest())
                return PartialView();
            return View();
        } //END InfoSheet
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult Order()
        {
            if (Sessions.Instance.IsAuth
            ) //LF *** 9/6/18 commented out to see if works - TODO does this have to be here, for security?  && Request.IsAjaxRequest())
                return View();
            return RedirectToAction("Login", "Agent");
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //
        // /Agent/Login
        public ActionResult Login(string id, string redirectUrl = null)
        {
            if (Sessions.Instance.IsAuth) return RedirectToAction("Search", "Agent");

            var model = new AgentLoginModel();
            model.RedirectUrl = redirectUrl;
            model.ReturnCode = id;

            return View(model);
        } //END Login
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //
        // /Agent/Search
        public ActionResult Search()
        {
            if (Sessions.Instance.IsClient)
            {
                var clientModel = new AgentLoginModel();
                clientModel.UserName = Sessions.Instance.EmailAddress;
                return RedirectToAction("Policy", "Client", new {model = clientModel});
            }

            if (Sessions.Instance.IsAuth && Sessions.Instance.IsAgent)
            {
                if (AgentUtils.GetScalar<bool>("IsRequireCall", Sessions.Instance.AgentCode))
                    return RedirectToAction("PleaseCall", "Agent");

                var model = AgentService.AgentPolicySearch();
                return View(model);
            }

            return RedirectToAction("Login", "Agent", new {id = "S404"});
        }

        //END Search
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // [HttpGet]
        // public void TestPalmsService1()
        // {
        //    
        // }
        //
        [HttpGet]
        public void TestPalmsService2()
        {
            // SSRSHelpers ssrsHelpers = new SSRSHelpers();
            //
            // string testPolicyNo = "DPFL0217862";
            // ssrsHelpers.GeneratePaymentReceipt(testPolicyNo, XpoHelper.GetNewUnitOfWork());
            //
            // using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            // {
            //
            //     uow.ExecuteSproc("spUW_InstallmentPaymentApply", new OperandValue("DPFL0217866"),
            //         new OperandValue(DateTime.Now.ToString()), new OperandValue("portal"),
            //         new OperandValue(Guid.NewGuid().ToString()));
            // }
        }

        [HttpGet]
        [BasicAgentAuthentication]
        public ActionResult BasicRaterLogin(string quoteNumber)
        {
            if (!string.IsNullOrEmpty(quoteNumber)) Sessions.Instance.PolicyNo = quoteNumber;

            var quote = AgentService.GetQuote(quoteNumber);
            if (quote.AgentCode == null) AgentService.UpdateQuoteAgent(quoteNumber, Sessions.Instance.AgentCode);
            if (!string.IsNullOrEmpty(quote?.AgentCode) && !Sessions.Instance.AgentCode.Equals(quote?.AgentCode))
            {
                Sessions.Instance.RatingErrMsg =
                    $"<div class='error'><p>Agent {Sessions.Instance.AgencyName} does not have access to this quote. You have been redirected to the agency's home page.</p></div>";
                return RedirectToAction("Search");
            }

            if (!string.IsNullOrEmpty(Sessions.Instance.PolicyNo))
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0} bridged over from a rater, going directly to Quote Edit",
                        Sessions.Instance.Username), "INFO");
                return RedirectToAction("Edit", "Quote", new
                {
                    id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)
                });
            }

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult TokenRaterLogin(string quoteNumber, Guid? token = null)
        {
            if (string.IsNullOrEmpty(quoteNumber) || token == null || token.Equals(Guid.Empty))
                return RedirectToAction("Login");

            var agentToken = AgentService.GetAgentAuthToken(quoteNumber, token);
            if (agentToken == null) return RedirectToAction("Login");

            var quote = AgentService.GetQuote(quoteNumber);
            if (quote == null) return RedirectToAction("Login");

            if (agentToken.PolicyNo.Equals(quote.PolicyNo) && agentToken.Token.Equals(token) &&
                DateTime.Now.Date < agentToken.ExpirationDate.Date)
            {
                Sessions.Instance.AgentCode = quote.AgentCode;
                Sessions.Instance.AgencyName = AgentService.GetAgencyName(quote.AgentCode);
                Sessions.Instance.AgentName = AgentService.GetAgentName(quote.AgentCode);
                Sessions.Instance.Username = AgentUtils.GetEmailAddr(quote.AgentCode);
                Sessions.Instance.IsAuth = true;
                Sessions.Instance.IsAgent = true;
                Sessions.Instance.PolicyNo = quoteNumber;
                AgentControllerLog.Info($"Username {Sessions.Instance.Username} has logged in");
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0} bridged over from a rater, going directly to Quote Edit",
                        Sessions.Instance.Username), "INFO");
                return RedirectToAction("Edit", "Quote", new
                {
                    id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)
                });
            }

            return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult RaterLogin(string userName, string password, string quoteNumber)
        {
            var login = new LoginDO();
            login.username = userName;
            login.password = password;
            if (!string.IsNullOrEmpty(quoteNumber) && AgentService.CheckLogin(login.username, login.password))
            {
                AgentService.LogIP(userName, password, Request);
                var agentCode = AgentService.GetAgentCode(userName);
                Sessions.Instance.AgentCode = agentCode;
                Sessions.Instance.AgencyName = AgentService.GetAgencyName(agentCode);
                Sessions.Instance.AgentName = AgentService.GetAgentName(agentCode);
                Sessions.Instance.Username = AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode);
                Sessions.Instance.IsAuth = true;
                Sessions.Instance.IsAgent = true;
                Sessions.Instance.PolicyNo = quoteNumber;
                AgentControllerLog.Info($"Username {Sessions.Instance.Username} has logged in");

                var quote = AgentService.GetQuote(quoteNumber);
                if (quote != null && string.IsNullOrEmpty(quote.AgentCode))
                {
                    AgentService.UpdateQuoteAgent(quoteNumber, Sessions.Instance.AgentCode);
                }
                else if (!string.IsNullOrEmpty(quote?.AgentCode) && !agentCode.Equals(quote?.AgentCode))
                {
                    Sessions.Instance.RatingErrMsg =
                        $"<div class='error'><p>Agent {Sessions.Instance.AgencyName} does not have access to this quote. You have been redirected to the agency's home page.</p></div>";
                    return RedirectToAction("Search");
                }

                if (!string.IsNullOrEmpty(Sessions.Instance.PolicyNo))
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0} bridged over from a rater, going directly to Quote Edit",
                            Sessions.Instance.Username), "INFO");
                    return RedirectToAction("Edit", "Quote", new
                    {
                        id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)
                    });
                }
            }
            return RedirectToAction("Login");
        }


        [HttpPost]
        public ActionResult Login(AgentLoginModel model)
        {
            if (Sessions.Instance.IsClient)
            {
                var clientModel = new AgentLoginModel();
                clientModel.UserName = Sessions.Instance.EmailAddress;
                return RedirectToAction("Policy", "Client", new {model = clientModel});
            }

            if (ModelState.IsValid)
            {
                if (AgentService.CheckLogin(model.UserName, model.Password))
                {
                    AgentService.LogIP(model.UserName, model.Password, Request);
                    var agentCode = AgentService.GetAgentCode(model.UserName);
                    Sessions.Instance.AgentCode = agentCode;
                    Sessions.Instance.AgencyName = AgentService.GetAgencyName(agentCode);
                    Sessions.Instance.AgentName = AgentService.GetAgentName(agentCode);
                    Sessions.Instance.Username = AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode);
                    Sessions.Instance.IsAuth = true;
                    Sessions.Instance.IsAgent = true;
                    LogUtils.Log(Sessions.Instance.Username, $"Username {Sessions.Instance.Username} has logged in",
                        "INFO");
                    if (AgentUtils.GetScalar<bool>("IsRequireCall", Sessions.Instance.AgentCode))
                        return RedirectToAction("PleaseCall", "Agent");

                    if (!string.IsNullOrEmpty(model.RedirectUrl)) return Redirect(model.RedirectUrl);

                    ////////////////////////////////////////////////
                    if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Search", "Agent");

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0} bridged over from a rater, going directly to Quote Edit",
                            Sessions.Instance.Username), "INFO");
                    return RedirectToAction("Edit", "Quote", new
                    {
                        id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)
                    });
                } //END Success Login

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Failed Logon Attempt by {0}", model.UserName), "WARN");
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }

            return View(model);
        } //END Login Action POST

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        [HttpPost]
        public ActionResult Search(SearchPolicyModel msearch)
        {
            if (!Sessions.Instance.IsAuth || !Sessions.Instance.IsAgent) return RedirectToAction("Login", "Agent");

            var model = AgentService.PolicySearch(msearch);
            return View(model);
        }

        //END Search

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //LF 11/7/18
        //CalcDriverPoints

        [HttpPost]
        public ActionResult DriverAdd(EndorsementDriverModel model)
        {
            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

                if (TempData["FromRating"] != null) TempData["FromRating"] = TempData["FromRating"];

                TempData["LanguagePreference"] = TempData["LanguagePreference"];

                //Sessions.Instance.PolicyNo = model.QuoteId;
                var endorsePolicy = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;

                AgentService.AddDriverEndorsement();


                if (model.IsEdit)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: EDITING DRIVER {1}", Sessions.Instance.PolicyNo, model.DriverIndex),
                        "INFO");
                    AgentService.EndorseDriverUpdate(model, endorsePolicy);
                    TempData["EndorsePolicyModel"] = endorsePolicy;
                    TempData["EndorseDriverModel"] = model;
                    TempData["LanguagePreference"] = TempData["LanguagePreference"];
                    TempData["ClearEndorseQuoteTables"] =
                        false; // true; // false;  //LF 12/6/18 - to not get a previously unissued endorsement
                    TempData["EndorsementDate"] = TempData["EndorsementDate"];
                    TempData["DriverID"] = ""; //LF 11/7/18
                    TempData["FromRating"] = false;
                    return RedirectToAction("Endorsement", "Agent", new {id = Sessions.Instance.PolicyNo});
                }

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: NEW QUOTE", Sessions.Instance.PolicyNo), "INFO");

                AgentService.EndorseDriverAdd(model, endorsePolicy);
                TempData["EndorsePolicyModel"] = endorsePolicy;
                TempData["EndorseDriverModel"] = model;
                TempData["LanguagePreference"] = TempData["LanguagePreference"];
                TempData["ClearEndorseQuoteTables"] =
                    false; // true; // false;  //LF 12/6/18 - to not get a previously unissued endorsement
                TempData["EndorsementDate"] = TempData["EndorsementDate"];
                TempData["DriverID"] = ""; //LF 11/7/18
                TempData["DriverCount"] = endorsePolicy.Drivers.Count; //LF 11/7/18

                return RedirectToAction("Endorsement", "Agent", new {id = Sessions.Instance.PolicyNo});
            }

            var errors = model.Validate(null).ToList();
            errors.Where(vr => vr.MemberNames == null)
                .Distinct()
                .ToList()
                .ForEach(vr =>
                {
                    if (!ModelState.ContainsKey(vr.ErrorMessage))
                        ModelState.AddModelError(vr.ErrorMessage, vr.ErrorMessage);
                });
            model.ValidationErrors = ModelState;
            return View("AddDriver", model);
        }

        //END DriverAdd

        public ActionResult DriverLoad(string id, EndorsementDriverModel model)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var endorseModel = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: LOADING DRIVER DATA", Sessions.Instance.PolicyNo), "INFO");

            var driverModel = AgentService.LoadDriverData(id, endorseModel);
            TempData["EndorsePolicyModel"] = endorseModel;
            if (TempData["EndorsementDate"] != null) TempData["EndorsementDate"] = TempData["EndorsementDate"];

            if (TempData["FromRating"] != null) TempData["FromRating"] = TempData["FromRating"];

            TempData["LanguagePreference"] = TempData["LanguagePreference"];
            TempData["DriverID"] = id;
            TempData["DriverIndex"] = driverModel.DriverIndex;
            return RedirectToAction("AddDriver", driverModel);
        }

        //END DriverLoad
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        ///     add new driver
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddDriver(string id, string tmpfName = "", string tmplName = "", string tmpMI = "",
            string tmpfName2 = "", string tmplName2 = "", string tmpMI2 = "")
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            if (TempData["FromRating"] != null) TempData["FromRating"] = TempData["FromRating"];

            TempData["LanguagePreference"] = TempData["LanguagePreference"];

            var endorseModel = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;

            var driverID = TempData["DriverID"] as string;

            var driverModel = AgentService.GetDriver(tmpfName, tmplName, tmpMI, tmpfName2, tmplName2, tmpMI2, driverID,
                endorseModel, out var newDriverIndex);


            TempData["DriverIndex"] = newDriverIndex;
            TempData["EndorsePolicyModel"] = endorseModel;
            TempData["DriverModel"] = driverModel;
            TempData["DriverID"] = driverID;
            TempData["DriverCount"] = endorseModel.Drivers.Count; //LF 11/7/18
            TempData["LanguagePreference"] = TempData["LanguagePreference"];
            TempData["ClearEndorseQuoteTables"] =
                false; // true; // false;  //LF 12/6/18 - to not get a previously unissued endorsement
            TempData["EndorsementDate"] = TempData["EndorsementDate"];

            var result = AgentService.GetMissingEndorseReasons(Sessions.Instance.PolicyNo, "0");
            TempData["AgentEndorsementAllReasons"] = result.AllReasons;
            ViewBag.UnSelectedEndorseReasons = result.UnSelectedEndorseReasons;
            ViewBag.SelectedEndorseReasons = result.SelectedEndorseReasons;

            return View("AddDriver", driverModel);
        }


        [HttpGet]
        public ActionResult OrderMvrForEndorsement(string firstName, string lastName, string dob, string licenseNo,
            string licenseState, int driverIndex)
        {
            try
            {
                var driver = new EndorsementDriverModel
                {
                    FirstName = firstName,
                    LastName = lastName,
                    DateOfBirth = dob,
                    DriversLicense = licenseNo,
                    LicenseState = licenseState,
                    DriverIndex = driverIndex
                };

                var mvrReportResult = IntegrationsUtility.MvrSubmitRequest(
                    Sessions.Instance.PolicyNo,
                    driver.FirstName, driver.LastName, driver.DateOfBirth,
                    driver.DriversLicense, driver.LicenseState);
                var results =
                    AgentService.HandleMvrOrderForEndorsement(Sessions.Instance.PolicyNo, driver, mvrReportResult);
                return Json(new {Violations = results, mvrsMsg = "Successful"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new {errorMsg = ex.Message, mvrsMsg = "UnSuccessful"}, JsonRequestBehavior.AllowGet);
            }
        } //END OrderMvr
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        public ActionResult DriverDelete(string id)
        {
            var getDrivers = AgentService.DeleteDriver(id);
            TempData["EndorsePolicyModel"] = TempData["EndorsePolicyModel"];
            TempData["DriverCount"] = getDrivers.Count; //LF 11/7/18
            ViewBag.EndorsementSelectedReasons =
                AgentService.GetSelectedAgentEndorsementReasons(Sessions.Instance.PolicyNo);
            return PartialView("~/Views/Agent/DisplayTemplates/Drivers.cshtml",
                getDrivers.Select(drivers => UtilitiesRating.MapXpoToModel(drivers)).ToList());
        }

        //END DriverDelete

        public ActionResult Endorsement(string id, string endorsementDate, string errorMessage = "",
            string endorsementReasons = "")
        {
            if (string.IsNullOrEmpty(id)) return RedirectToAction("Search", "Agent");

            if (TempData["EndorsementDate"] != null && endorsementDate == null)
            {
                TempData["EndorsementDate"] = TempData["EndorsementDate"];
                endorsementDate = TempData["EndorsementDate"].ToString();
            }
            else
            {
                TempData["EndorsementDate"] = endorsementDate;
            }

            TempData["LanguagePreference"] = TempData["LanguagePreference"];
            Sessions.Instance.PolicyNo = id;
            Sessions.Instance.IsNewQuote = true;
            bool clearTables = true;
            if (TempData["ClearEndorseQuoteTables"] != null)
            {
                clearTables =  (bool) TempData["ClearEndorseQuoteTables"];
            }

            if (TempData["PolicyEndorseDiffAmt"] != null)
                TempData["PolicyEndorseDiffAmt"] = TempData["PolicyEndorseDiffAmt"];
            if (TempData["PolicyEndorseDiffAmt"] == null || clearTables)
                TempData["PolicyEndorseDiffAmt"] = 0.00;


            if (clearTables)
            {
                AgentService.ClearEndorseQuoteTables(id, endorsementReasons);

                TempData["ClearEndorseQuoteTables"] = false;
            }

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var compDed = "NONE";
                var collDed = "NONE";
                id = string.IsNullOrEmpty(id) ? Sessions.Instance.PolicyNo : id;
                var endorsement = AgentService.FindPolicyEndorseQuote(id);
                var policy = AgentService.FindPolicy(id);

                if (endorsement == null || policy == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("Endorsement Policy Not Found: {0}".ToUpper(), id),
                        "FATAL");
                    return RedirectToAction("Search", "Agent", new {id = "Q404"});
                }

                var cars = AgentService.PolicyEndorseQuoteCarsList(endorsement);
                var drivers = AgentService.PolicyEndorseQuoteDriversList(endorsement);
                var model = UtilitiesRating.MapXpoToModel(endorsement);
                var languagePreference = AgentService.LanguagePreference();
                var language = languagePreference?.Value;
                if (TempData["LanguagePreference"] != null) language = TempData["LanguagePreference"].ToString();

                if (string.IsNullOrEmpty(language)) language = "ENGLISH";

                TempData["LanguagePreference"] = language;
                model.InsuredPreferredLanguage = language;
                model.Vehicles = new List<EndorsementVehicleModel>();
                model.Drivers = new List<EndorsementDriverModel>();
                model.ErrorMessage = errorMessage;
                model.SameAsGarage = endorsement.MailGarageSame;
                model.GarageStreetAddress = endorsement.GarageStreet;
                model.GarageCity = endorsement.GarageCity;
                model.GarageState = endorsement.GarageState;
                model.GarageZIPCode = endorsement.GarageZip;
                model.GarageCounty = endorsement.GarageCounty;
                model.MailingStreetAddress = endorsement.MailStreet;
                model.MailingCity = endorsement.MailCity;
                model.MailingState = endorsement.MailState;
                model.MailingZIPCode = endorsement.MailZip;
                model.OldPolicyWritten = policy.PolicyWritten -
                                         (policy.DBSetupFee + policy.FHCFFee + policy.MaintenanceFee +
                                          policy.MVRFee + policy.PolicyFee + policy.SR22Fee + policy.RewriteFee +
                                          policy.PIPPDOnlyFee);
                //----------------------
                foreach (var car in cars)
                {
                    if (car.HasPhyDam)
                    {
                        compDed = car.CompDed;
                        collDed = car.CollDed;

                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("Quote {0} has PhyDam: Comp Ded: {1}, Coll Ded: {2}".ToUpper(), id, compDed,
                                collDed), "FATAL");
                    }

                    model.Vehicles.Add(UtilitiesRating.MapXpoToModel(car));
                }

                model.CompDed = compDed;
                model.CollDed = collDed;
                model.Workloss = Convert.ToBoolean(endorsement.WorkLoss);
                model.NIO = endorsement.NIO;
                model.NIRR = endorsement.NIRR;
                foreach (var driver in drivers) model.Drivers.Add(UtilitiesRating.MapXpoToModel(driver));

                /* //LF 11/25/18 commented out, need to make change in endorsement, if Insured Name changes, change name to same on Driver 1, if Driver 1 name changes, change Insured name to same 
                if (drivers.Any())
                {
                    model.FirstName1 = drivers.First().FirstName;
                    model.LastName1 = drivers.First().LastName;
                    model.MiddleInitial1 = drivers.First().MI;
                } */
                //----------------------
                model.CompDed = compDed;
                model.CollDed = collDed;
                //----------------------
                foreach (var carModel in model.Vehicles)
                {
                    var xpo = new PolicyEndorseQuoteCars(uow);
                    xpo.PolicyNo = carModel.PolicyID;
                    model.AgentCode = Sessions.Instance.AgentCode;
                    UtilitiesRating.MapModelToXpo(carModel, xpo);
                }

                foreach (var driverModel in model.Drivers)
                {
                    var xpo = new PolicyEndorseQuoteDrivers(uow);
                    xpo.PolicyNo = driverModel.PolicyID;
                    model.AgentCode = Sessions.Instance.AgentCode;
                    var violations = AgentService.PolicyEndorseQuoteDriversViolationsList(driverModel);
                    if (violations.Any())
                    {
                        driverModel.HasMVR = true;
                        driverModel.Violations = new List<EndorsementViolationModel>();
                        foreach (var violation in violations)
                            driverModel.Violations.Add(UtilitiesRating.MapXpoToModel(violation));
                    }

                    UtilitiesRating.MapModelToXpo(driverModel, xpo);
                }

                /*LF added 11/8/18
                 if (model.WorkLossGroup == 1 || model.WorkLossGroup == 2)
                     model.Workloss = true;
                 else
                     model.Workloss = false;
                 if (model.Workloss == false)
                     model.WorkLossGroup = 3;
                 //-- to here */
                model.GetCoverageInfo = PolicyWebUtils.GetEndorsementCoverageInfo(model.PolicyNo);
                //----------------------
                Sessions.Instance.PrevAction = "ENDORSEMENT";
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Edit Endorsement {0}".ToUpper(), Sessions.Instance.PolicyNo), "INFO");
                var policyDiff = 0.0;
                if (TempData["PolicyEndorseDiffAmt"] != null)
                    policyDiff = Convert.ToDouble(TempData["PolicyEndorseDiffAmt"]);
                if (policyDiff > 0.0 || policyDiff < 0.0)
                    model.PolicyDiffAmt = policyDiff;
                model.EndorsementReasons = new List<string>();
                model.RequiredDocuments = new List<string>();
                TempData["EndorsePolicyModel"] = model;

                if (endorsementDate == null)
                {
                    if (TempData["EndorsementDate"] != null)
                    {
                        endorsementDate = (string) TempData["EndorsementDate"];
                        TempData["EndorsementDate"] = endorsementDate;
                    }
                }
                else
                {
                    TempData["EndorsementDate"] = endorsementDate;
                }

                ViewBag.EndorsementDate = endorsementDate;
                ViewBag.EndorsementMinDue = "0";
                TempData["DriverId"] = ""; //LF 12/9/18  added these 2 lines
                TempData["VehicleId"] = "";
                if (TempData["FromRating"] != null && (bool) TempData["FromRating"])
                {
                    TempData["FromRating"] = TempData["FromRating"];
                    model.PolicyRated = true;

                    var date = DateTime.Parse(endorsementDate);
                    var result = AgentService.GetMinimumEndorsementPaymentDue(date, policyDiff, Sessions.Instance.PolicyNo, Sessions.Instance.Username);

                    if (result.Success) ViewBag.EndorsementMinDue = result.Value.ToString("n2");
                }

                model.EndorsementInstallments =
                    PortalPaymentUtils.GetEndorsementInstallments(Sessions.Instance.PolicyNo, model.PolicyRated);
                TempData["LanguagePreference"] = TempData["LanguagePreference"];
                var policyNoticeCancelType = PortalUtils.GetCancelTypeOfPolicyWithNotice(id);
                if (!string.IsNullOrEmpty(policyNoticeCancelType) && string.Equals(policyNoticeCancelType, "NON-PAY",
                    StringComparison.OrdinalIgnoreCase))
                {
                    model.ErrorMessage = $"Unable to endorse policy#:{id} as it is currently on NOTICE";
                    return View(model);
                }

                var validateErrorMessage = AgentService.ValidateEndorsement(endorsement.PolicyNo, true);

                if (!string.IsNullOrEmpty(validateErrorMessage))
                    model.ErrorMessage = validateErrorMessage;
                else
                    model.ErrorMessage = string.Empty;
                ViewBag.SkipEndorseReasons = false;
                if (TempData["EndorsementReasonsError"] != null &&
                    !string.IsNullOrEmpty(TempData["EndorsementReasonsError"].ToString()))
                {
                    model.ErrorMessage = TempData["EndorsementReasonsError"].ToString();
                    TempData["EndorsementReasonsError"] = null;

                    var agentEndorsementAllReasons = TempData["AgentEndorsementAllReasons"] as AllAgentEndorseReasons;
                    if (agentEndorsementAllReasons != null)
                        ViewBag.AgentEndorsementAllReasons = agentEndorsementAllReasons;
                    TempData["AgentEndorsementAllReasons"] = null;
                    //ViewBag.SkipEndorseReasons = true;
                }


                PolicyWebUtils.GetEndorsementChangeTypes(out var premBearingChanges, out var nonPremBearingChanges, id);
                ViewBag.PremBearingChanges = premBearingChanges;
                ViewBag.NonPremBearingChanges = nonPremBearingChanges;
                model.SelectedReasons = AgentService.GetSelectedAgentEndorsementReasons(endorsement.PolicyNo);

                return View(model);
            } //end UOW
        }

        [HttpPost]
        public ActionResult IssueEndorsementDetails(string policyNo, DateTime? effDate, string diffAmount,
            bool isPost = true)
        {
            var endorsement = AgentService.FindPolicyEndorseQuote(policyNo);

            var errorMessage = AgentService.ValidateEndorsement(policyNo, true);
            if (string.IsNullOrEmpty(errorMessage))
            {
                var result = AgentService.GetMissingEndorseReasons(policyNo, endorsement.OID.ToString());
                var missingReasons = result.MissingReasonsText;
                TempData["AgentEndorsementAllReasons"] = result.AllReasons;
                ViewBag.UnSelectedEndorseReasons = result.UnSelectedEndorseReasons;
                ViewBag.SelectedEndorseReasons = result.SelectedEndorseReasons;
                if (!string.IsNullOrEmpty(missingReasons))
                {
                    errorMessage =
                        $"The required changes for this endorsement have not been completed. Please make sure that you complete all of the reasons you selected; {missingReasons}.";
                    TempData["EndorsementReasonsError"] = errorMessage;
                    TempData["FromRating"] = TempData["FromRating"];
                }
                else if (!string.IsNullOrEmpty(ViewBag.UnSelectedEndorseReasons))
                {
                    TempData["EndorsementReasonsError"] =
                        $"Endorsement changes have been identified for the following unselected reasons; {ViewBag.UnSelectedEndorseReasons}." +
                        $"<br/>Please make sure that you only perform changes for the selected Reasons; {ViewBag.SelectedEndorseReasons}.";
                    //errorMessage = ViewBag.UnSelectedEndorseReasons;
                    errorMessage = "EndorsementReasonsError";
                    TempData["FromRating"] = TempData["FromRating"];
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                TempData["ClearEndorseQuoteTables"] = false;
                return RedirectToAction("Endorsement",
                    new
                    {
                        id = policyNo,
                        endorsementDate = effDate.GetValueOrDefault(),
                        errorMessage
                    });
            }

            //var isValidForIssuance = endorsement.IsValidateForIssuance();

            //if (isValidForIssuance)
            //{
            PolicyWebUtils.GetEndorsementChangeTypes(out var premChanges, out var nonPremChanges, policyNo);

            if (nonPremChanges && !premChanges)
                return RedirectToAction("IssueEndorsementDetails", new
                {
                    policyNo,
                    diffAmount = 0,
                    effDate,
                    isNonPremBearingsOnly = "1"
                });
            //}

            //string issuanceErrorMessage =
            //       "Only specific endorsements are allowed to be issued in the portal; Name Change, Address Change, Phone Numbers, Add Driver, and Add Vehicle without comp/coll. Please email underwriting@palminsure.com, or please call us at (866) 436-7256 and press 2 to reach an Underwriter.";

            if (string.IsNullOrEmpty(diffAmount) && TempData["PolicyEndorseDiffAmt"] != null)
                diffAmount = TempData["PolicyEndorseDiffAmt"].ToString();
            double.TryParse(diffAmount, out var amountDiff);

            AgentService.SaveEnodrsementChanges(policyNo, amountDiff, effDate);
            //return RedirectToAction("ShowEndorsementDiffernces", new
            //{
            //    policyNumber = policyNo,
            //    errorMessage = string.Empty // isValidForIssuance ? null : issuanceErrorMessage
            //});
            return RedirectToAction("IssueEndorsementDetailsWithPayment", new
            {
                policyNo,
                diffAmount = amountDiff,
                effDate
                //previewChanges = false
            });
        }

        public ActionResult IssueEndorsementDetailsWithPayment(string policyNo, DateTime? effDate, string diffAmount)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var errorMessage = AgentService.ValidateEndorsement(policyNo, false);
                if (!string.IsNullOrEmpty(errorMessage))
                {
                    TempData["ClearEndorseQuoteTables"] = false;
                    return RedirectToAction("Endorsement",
                        new
                        {
                            id = policyNo,
                            endorsementDate = effDate.GetValueOrDefault(),
                            errorMessage
                        });
                }

                //if (!endorsement.IsValidateForIssuance())
                //{
                //    string issuanceErrorMessage =
                //          "Only specific endorsements are allowed to be issued in the portal; Name Change, Address Change, Phone Numbers, Add Driver, and Add Vehicle without comp/coll. Please email underwriting@palminsure.com, or please call us at (866) 436-7256 and press 2 to reach an Underwriter.";
                //    return RedirectToAction("ShowEndorsementDiffernces", new
                //    {
                //        policyNumber = policyNo,
                //        errorMessage = issuanceErrorMessage
                //    });
                //}

                double diffAmountDbl = double.Parse(diffAmount);
                var result =  AgentService.GetMinimumEndorsementPaymentDue(effDate, diffAmountDbl, policyNo, Sessions.Instance.Username);
                var amount = result.Value;

                AgentService.AddLogMessage(
                    $"Endorsement-{policyNo}- diffAmount-{diffAmount} | spUW_MinimumEndorsementPaymentDue sp returned value-{amount}.");


                if (amount <= 0)
                    return RedirectToAction("IssueEndorsementDetails", "Agent", new
                    {
                        policyNo,
                        effDate,
                        diffAmount = 0
                    });

                return RedirectToAction("CardConnectPayment", "Quote", new
                {
                    quoteNo = policyNo,
                    minimumPayAmt = amount,
                    isInitialDepostPayment = false,
                    isEndorsementPayment = true,
                    endorsementEffectiveDate = effDate
                });
            }
        }

        public ActionResult IssueEndorsementDetails(string policyNo, DateTime? effDate, string diffAmount,
            string isNonPremBearingsOnly = "0")
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo))
            {
                AgentService.AddLogMessage("Endorsement- Policy number is Empty. Redirecting to Login page.");
                return RedirectToAction("Login", "Agent");
            }

            var errorMessage = AgentService.ValidateEndorsement(policyNo, false);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                TempData["ClearEndorseQuoteTables"] = false;
                return RedirectToAction("Endorsement",
                    new
                    {
                        id = policyNo,
                        endorsementDate = effDate.GetValueOrDefault(),
                        errorMessage
                    });
            }

            var model = AgentService.IssueEndorsementDetails(policyNo, effDate, diffAmount, isNonPremBearingsOnly, Sessions.Instance.Username, TempData["LanguagePreference"].ToString());
            if (isNonPremBearingsOnly == "1")
            {
                ViewBag.isNonPremBearingsOnly = true;
            }
            TempData["PayAmt"] = 0;
            TempData["FromRating"] = null;
            TempData["IsEndorsementPayment"] = true;
            TempData["LanguagePreference"] = TempData["LanguagePreference"];
            return View("IssueEndorsementDetails", model);
        }

        public ActionResult Policy(string id)
        {
            ViewPolicyModel model = null;
            if (Sessions.Instance.IsAgent)
            {
                Sessions.Instance.PolicyNo = id;
                if (AgentUtils.GetScalar<bool>("IsRequireCall", Sessions.Instance.AgentCode))
                    return RedirectToAction("PleaseCall", "Agent");

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Viewing Policy {0} as {1}", id, Sessions.Instance.IsAgent ? "AGENT" : "USER"),
                    "INFO");
                model = AgentService.GetPolicy(id, Server, Url);
                ViewBag.ReasonList = AgentService.GetEndorseReasonList(model.CarsList, model.DriversList,
                    is2ndInsExists: !string.IsNullOrEmpty(model.Ins2First) || !string.IsNullOrEmpty(model.Ins2Last));
                return View(model);
            }

            return RedirectToAction("Login", "Agent");
        }

        [HttpGet]
        public ActionResult UpdateEftForPolicy(string policyNo)
        {
            var model = new EftAccountUpdateModel();
            model.PolicyNo = policyNo;

            var policy = AgentService.FindPolicy(policyNo);
            if (policy == null) return RedirectToAction("Search");

            model.Name = $"{policy.Ins1First} {policy.Ins1Last}";
            model.Address = policy.GarageStreet;
            model.City = policy.GarageCity;
            model.State = policy.GarageState;
            model.Postal = policy.GarageZip;

            model.IFramePort = ConfigSettings.ReadSetting("CardConnect_IFrame_Port");
            return View("EftAccountUpdate", model);
        }

        [HttpPost]
        public ActionResult UpdateEftForPolicy(EftAccountUpdateModel model)
        {
            AgentService.UpdateEftForPolicy(model);
            return RedirectToAction("Policy", new { id = model.PolicyNo });
        }

        [HttpGet]
        public ActionResult Payment(string policyNo, double endorsementPaymentAmount)
        {
            var payModel = new MakePaymentModel
            {
                PolicyNo = policyNo,
                MinimumAmount = endorsementPaymentAmount,
                PaymentAmt = endorsementPaymentAmount,
                PolicyType = "ENDORSEMENT"
            };
            if (TempData["EndorsementEffDate"] != null) TempData["EndorsementEffDate"] = TempData["EndorsementEffDate"];

            return View(payModel);
        } //END Payment

        [HttpPost]
        public ActionResult Payment(MakePaymentModel model)
        {
            if (model.PaymentAmt == 0) return RedirectToAction("Policy", new { id = model.PolicyNo });

            return RedirectToAction("CardConnectPayment", "Quote",
                new { quoteNo = model.PolicyNo, minimumPayAmt = model.PaymentAmt, isInitialDepostPayment = false });
        } //END Payment
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //TODO: Remove this sweep controller once we know its no longer needed, moved to AjaxHelpers
        [HttpPost]
        public ActionResult Sweep(AgentSweepModel model)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                if (Sessions.Instance.SweepBtnHitCount == 1)
                    switch (model.PolicyType)
                    {
                        case "QUOTE":
                            return RedirectToAction("GeneratePolicyDocs", "Quote", new
                            {
                                id = model.PolicyNumber
                            });
                            break;
                        case "RENEWAL":
                            return RedirectToAction("Policy", new
                            {
                                id = model.PolicyNumber
                            });
                            break;
                        case "POLICY":
                            return RedirectToAction("Policy", new
                            {
                                id = model.PolicyNumber
                            });
                            break;
                        default:
                            return RedirectToAction("Policy", new
                            {
                                id = model.PolicyNumber
                            });
                            break;
                    }

                Sessions.Instance.SweepBtnHitCount = 1;

                if (!Sessions.Instance.IsAgent || string.IsNullOrEmpty(Sessions.Instance.AgentCode))
                    return RedirectToAction("Login");

                //////////////////////////////////
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: SWEEPING AGENT ACCOUNT FOR {1}", Sessions.Instance.PolicyNo,
                        Sessions.Instance.AgentCode), "DEBUG");

                double amtPrior = 0;
                double renAmt = 0;
                var isRenewal = false;

                switch (model.PolicyType)
                {
                    case "RENEWAL":
                        var getRenQuote =
                            new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?",
                                model.PolicyNumber)).FirstOrDefault();

                        if (getRenQuote == null) return RedirectToAction("Login");

                        var getRenInstalls =
                            new XPCollection<RenQuoteInstallments>(uow,
                                CriteriaOperator.Parse("PolicyNo = ? AND Descr = 'Deposit' AND IgnoreRec = '0'",
                                    getRenQuote.PolicyNo)).FirstOrDefault();
                        if (getRenInstalls == null) return RedirectToAction("Login");

                        var renPymnt = getRenInstalls.DownPymtAmount;
                        amtPrior = renPymnt.ToString().Parse<double>() - model.Amount;
                        renAmt = model.Amount + amtPrior;
                        var totalboth = renPymnt.SafeString().Parse<double>() + model.Amount;

                        var now = DateTime.Now;
                        var pymntBatchGuid = Guid.NewGuid().ToString();

                        //Full renewal payment amt from cybersource
                        if (model.Amount > 0.0)
                        {
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: SAVING RENEWAL PAYMENT INFORMATION TO PAYMENTS TABLE",
                                    model.PolicyNumber), "INFO");

                            AgentUtils.SweepAcct(Sessions.Instance.AgentCode, getRenInstalls.PolicyNo, renAmt,
                                Sessions.Instance.Username);
                        }

                        PortalUtils.AuditPayments(Sessions.Instance.Username,
                            PymntLocation.AgentSweep,
                            PymntTypes.Renewal,
                            totalboth,
                            amtPrior,
                            renPymnt.SafeString().Parse<double>(),
                            model.Amount,
                            pymntBatchGuid,
                            true,
                            false,
                            true);

                        if (Math.Abs(amtPrior) > 1.0)
                        {
                            var SaveToRen = new XpoPayments(uow);
                            SaveToRen.PolicyNo = model.PolicyNumber;
                            SaveToRen.PaymentType = "MOV";
                            SaveToRen.PostmarkDate = now;
                            SaveToRen.AcctDate = now;
                            SaveToRen.CheckAmt = amtPrior;
                            SaveToRen.TotalAmt = amtPrior;
                            SaveToRen.UserName = "system";
                            SaveToRen.BatchGUID = pymntBatchGuid;
                            SaveToRen.CreateDate = now;
                            SaveToRen.CheckNotes = "MOVED FROM " + model.PolicyNumber;
                            SaveToRen.Save();
                            uow.CommitChanges();

                            var SaveToPrior = new XpoPayments(uow);
                            SaveToPrior.PolicyNo = getRenQuote.PolicyNo;
                            SaveToPrior.PaymentType = "MOV";
                            SaveToPrior.PostmarkDate = now;
                            SaveToPrior.AcctDate = now;
                            SaveToPrior.CheckAmt = amtPrior * -1;
                            SaveToPrior.TotalAmt = amtPrior * -1;
                            SaveToPrior.UserName = "system";
                            SaveToPrior.BatchGUID = pymntBatchGuid;
                            SaveToPrior.CreateDate = now;
                            SaveToPrior.CheckNotes = "MONEY MOVED TO " + getRenQuote.PolicyNo;
                            SaveToPrior.Save();
                            uow.CommitChanges();
                        }

                        isRenewal = true;
                        break;
                    default:
                        AgentUtils.SweepAcct(Sessions.Instance.AgentCode, model.PolicyNumber, model.Amount,
                            Sessions.Instance.Username);
                        break;
                }

                //////////////////////////////////
                var subject = string.Format("[PALMINSURE.COM] Agent Sweep received for {0} on {1}.",
                    DateTime.Now.ToShortDateString(), model.PolicyNumber);
                string name = null;
                if (!model.PolicyNumber.StartsWith("Q-") && !model.PolicyNumber.StartsWith("D-"))
                {
                    var policy =
                        new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.PolicyNumber))
                            .FirstOrDefault();
                    if (policy != null) name = string.Format("{0} {1}", policy.Ins1First, policy.Ins1Last);
                }
                else
                {
                    var quote =
                        new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.PolicyNumber))
                            .FirstOrDefault();
                    if (quote != null) name = string.Format("{0} {1}", quote.Ins1First, quote.Ins1Last);
                }
                //////////////////////////////////

                var pymntMailNotes = isRenewal
                    ? string.Format(
                        @"Sweep by agent code {0}. <br/> Renewal Payment: {1}. <br/> Prior Policy Payment: {2}",
                        Sessions.Instance.AgentCode, renAmt, amtPrior)
                    : string.Format("Sweep by agent code {0}", Sessions.Instance.AgentCode);

                var body = EmailRenderer.PaymentSuccess(name,
                    model.PolicyNumber,
                    Sessions.Instance.AgentCode,
                    "Sweep",
                    DateTime.Now,
                    model.Amount,
                    pymntMailNotes
                );

                try
                {
                    if (WebHostService.IsProduction())
                    {
                        var sendTo = "underwriting@palminsure.com";
                        EmailService.SendEmailNotification(new List<string> {sendTo}, subject, body, true);
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: SEND SUCCESS SWEEP EMAIL TO UNDERWRITING FOR AGENT {1}",
                                Sessions.Instance.PolicyNo, Sessions.Instance.AgentCode), "INFO");
                    }
                }
                catch
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format(
                            "{0}: ERROR OCCURED SENDING SUCCESS SWEEP EMAIL TO UNDERWRITING FOR AGENT {1}",
                            Sessions.Instance.PolicyNo, Sessions.Instance.AgentCode), "FATAL");
                }

                //////////////////////////////////
                var agentEmail = AgentUtils.GetEmailAddr(Sessions.Instance.AgentCode);
                if (!string.IsNullOrEmpty(agentEmail))
                    try
                    {
                        if (WebHostService.IsProduction())
                        {
                            var sendTo = "underwriting@palminsure.com";
                            EmailService.SendEmailNotification(new List<string> {sendTo}, subject, body, true);
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: SEND SUCCESS SWEEP EMAIL TO AGENT AT {1}",
                                    Sessions.Instance.PolicyNo, agentEmail), "INFO");
                        }
                    }
                    catch (NullReferenceException)
                    {
                        "Agent email returned null, email now craps out".Log();
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format(
                                "{0}: SENDING EMAIL TO AGENT CRAPPED OUT, EMAIL SENT TO AGENT AT {1}",
                                Sessions.Instance.PolicyNo, agentEmail), "INFO");
                    }
                    catch (SmtpException)
                    {
                        "Office 365 is being stupid".Log();
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format(
                                "{0}: SENDING EMAIL TO AGENT CRAPPED OUT, EMAIL SENT TO AGENT AT {1}",
                                Sessions.Instance.PolicyNo, agentEmail), "INFO");
                    }

                //////////////////////////////////
                Sessions.Instance.PaymntResponseLink =
                    string.Format(
                        @"<div class='success'>Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype=ACH'>here</a> for your receipt</div>",
                        model.PolicyNumber);
                switch (model.PolicyType)
                {
                    case "QUOTE":
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: SWEEP MADE FROM QUOTE, REDIRECTING TO GENERATE POLICY DOCS",
                                Sessions.Instance.PolicyNo), "INFO");
                        return RedirectToAction("GeneratePolicyDocs", "Quote", new
                        {
                            id = model.PolicyNumber
                        });
                        break;
                    case "RENEWAL":
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format(
                                "{0}: SWEEP MADE FROM RENEWAL POLICY, REDIRECT TO GENERATE RENEWAL DOCS",
                                Sessions.Instance.PolicyNo), "INFO");
                        return RedirectToAction("GeneratePolicyDocs", "Renewal", new
                        {
                            id = model.PolicyNumber
                        });
                        break;
                    case "POLICY":
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: SWEEP MADE FROM POLICY, REDIRECT TO POLICY VIEW",
                                Sessions.Instance.PolicyNo), "INFO");
                        return RedirectToAction("Policy", new
                        {
                            id = model.PolicyNumber
                        });
                        break;
                    default:
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: SWEEP MADE FROM POLICY, REDIRECT TO POLICY VIEW",
                                Sessions.Instance.PolicyNo), "INFO");
                        return RedirectToAction("Policy", new
                        {
                            id = model.PolicyNumber
                        });
                        break;
                }
            }
        } //END Sweep
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Agent");
        } //END LogOut
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [HttpPost]
        public ActionResult Success()
        {
            string agentcode = null;
            string agentEmailAddr = null;
            string insuredEmailAddr = null;
            var sendToBoth = false;
            var isTestEnv = false;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var sysVars = new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = 'SYSTEMISDEV'"))
                    .FirstOrDefault();
                if (sysVars != null) isTestEnv = sysVars.Value == "true" ? true : false;


                var model = new PaymentResponseModel();
                model.csbillTo_city = Request.Form.Get("billTo_city");
                model.csbillTo_country = Request.Form.Get("billTo_country");
                model.csbillTo_firstName = Request.Form.Get("billTo_firstName");
                model.csbillTo_lastName = Request.Form.Get("billTo_lastName");
                model.csbillTo_phoneNumber = Request.Form.Get("billTo_phoneNumber");
                model.csbillTo_postalCode = Request.Form.Get("billTo_postalCode");
                model.csbillTo_state = Request.Form.Get("billTo_state");
                model.csbillTo_street1 = Request.Form.Get("billTo_street1");
                model.cscard_accountNumber = Request.Form.Get("card_accountNumber");
                model.cscard_cardType = Request.Form.Get("card_cardType");
                model.cscard_expirationMonth = Request.Form.Get("card_expirationMonth");
                model.cscard_expirationYear = Request.Form.Get("card_expirationYear");
                model.csccAuthReply_amount = string.IsNullOrEmpty(Request.Form.Get("ccAuthReply_amount"))
                    ? Request.Form.Get("orderAmount")
                    : Request.Form.Get("ccAuthReply_amount");
                model.csccAuthReply_authorizationCode = Request.Form.Get("ccAuthReply_authorizationCode");
                model.csccAuthReply_authorizedDateTime = Request.Form.Get("ccAuthReply_authorizedDateTime");
                model.csccAuthReply_avsCode = Request.Form.Get("ccAuthReply_avsCode");
                model.csccAuthReply_avsCodeRaw = Request.Form.Get("ccAuthReply_avsCodeRaw");
                model.csccAuthReply_cvCode = Request.Form.Get("ccAuthReply_cvCode");
                model.csccAuthReply_cvCodeRaw = Request.Form.Get("ccAuthReply_cvCodeRaw");
                model.csccAuthReply_processorResponse = Request.Form.Get("ccAuthReply_processorResponse");
                model.csccAuthReply_reasonCode = Request.Form.Get("ccAuthReply_reasonCode");
                model.csdecision = Request.Form.Get("decision");
                model.csdecision_publicSignature = Request.Form.Get("decision_publicSignature");
                model.csmerchantID = Request.Form.Get("merchantID");
                model.csorderAmount = Request.Form.Get("orderAmount");
                model.csorderAmount_publicSignature = Request.Form.Get("orderAmount_publicSignature");
                model.csorderCurrency = Request.Form.Get("orderCurrency");
                model.csorderCurrency_publicSignature = Request.Form.Get("orderCurrency_publicSignature");
                model.csorderNumber = Request.Form.Get("orderNumber");
                model.csorderNumber_publicSignature = Request.Form.Get("orderNumber_publicSignature");
                model.csorderPage_requestToken = Request.Form.Get("orderPage_requestToken");
                model.csorderPage_serialNumber = Request.Form.Get("orderPage_serialNumber");
                model.csorderPage_transactionType = Request.Form.Get("orderPage_transactionType");
                model.cspaymentOption = Request.Form.Get("paymentOption");
                model.csreasonCode = Request.Form.Get("reasonCode");
                model.csreconciliationID = Request.Form.Get("reconciliationID");
                model.csrequestID = Request.Form.Get("requestID");
                model.cssignedDataPublicSignature = Request.Form.Get("signedDataPublicSignature");
                model.cssignedFields = Request.Form.Get("signedFields");
                model.cstransactionSignature = Request.Form.Get("transactionSignature");
                model.pymntPolicyNumber = Request.Form.Get("pymntPolicyNumber");
                model.decision = Request.Form.Get("decision");
                model.now = DateTime.Now.ToString();
                model.portalController = Request.Form.Get("portalController");
                model.policyType = Request.Form.Get("policyType");
                PortalUtils.AuditCybersourcePymnt(model);
                model.RenPolicyNo = PortalUtils.GetRenPolicyNo(model.pymntPolicyNumber);
                //---------------------------
                var checkamt = model.csccAuthReply_amount.Parse<double>();
                var totalamt = model.csccAuthReply_amount.Parse<double>();
                //---------------------------
                if (model.pymntPolicyNumber.Upper().Contains("PAFL"))
                {
                    sendToBoth = true;
                    agentcode = AgentUtils.GetCorrectAgentCode(model.pymntPolicyNumber, false);
                    agentEmailAddr = AgentUtils.GetEmailAddr(agentcode);
                    insuredEmailAddr = InsuredUtils.GetInsuredEmail(model.pymntPolicyNumber);
                }
                else
                {
                    agentcode = AgentUtils.GetCorrectAgentCode(model.pymntPolicyNumber);
                    agentEmailAddr = AgentUtils.GetEmailAddr(agentcode);
                    LogUtils.Log(agentEmailAddr,
                        string.Format("{0}: PAYMENT USING CYBER SOURCE WAS A SUCCESS", model.pymntPolicyNumber),
                        "INFO");
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                var pymntBatchGUID = Guid.NewGuid().ToString();
                var now = DateTime.Now;
                var paymentType = Sessions.Instance.IsClient ? "INS" : "BUS";

                var getCode =
                    new XPCollection<SystemCyberSourceCodes>(uow,
                        CriteriaOperator.Parse("ResponseCode = ?", model.csreasonCode)).FirstOrDefault();
                string actionType = null;
                string responseDesc = null;
                if (getCode != null)
                {
                    actionType = getCode.ActionType;
                    responseDesc = getCode.ResponseDesc;
                }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Build Email Responses to users and Palm Insure
                string name = null;
                var userType = Sessions.Instance.IsClient ? "Client" : "Agent";
                string body = null;
                string emailAddr = null;
                var subject = string.Format("[PALMINSURE.COM] {2} payment received for {0} on {1}.",
                    DateTime.Now.ToShortDateString(), model.pymntPolicyNumber, userType);
                //-----------
                //Get Insured name based on policy/quote no.
                if (model.pymntPolicyNumber.Contains("PAFL"))
                {
                    var policy =
                        new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", model.pymntPolicyNumber))
                            .FirstOrDefault();
                    if (policy != null) name = string.Format("{0} {1}", policy.Ins1First, policy.Ins1Last);
                }
                else
                {
                    var quote =
                        new XPCollection<XpoPolicyQuote>(uow,
                            CriteriaOperator.Parse("PolicyNo = ?", model.pymntPolicyNumber)).FirstOrDefault();
                    if (quote != null) name = string.Format("{0} {1}", quote.Ins1First, quote.Ins1Last);
                }

                //-----------
                switch (actionType)
                {
                    case "PASS":
                        LogUtils.Log(agentEmailAddr,
                            string.Format("{0}: PAYMENT REASON CODE RESULT WAS PASS - {1}", model.pymntPolicyNumber,
                                responseDesc), "INFO");
                        var getPayments = new XPCollection<Payments>(uow,
                            CriteriaOperator.Parse("TokenID = ?", model.csrequestID));
                        emailAddr = agentEmailAddr;
                        var paymentCount = getPayments.Count;
                        if (paymentCount == 0)
                        {
                            switch (model.policyType)
                            {
                                case "RENEWAL":

                                    var getRenQuote = new XPCollection<PolicyRenQuote>(uow,
                                            CriteriaOperator.Parse("PriorPolicyNo = ?", model.pymntPolicyNumber))
                                        .FirstOrDefault();

                                    if (getRenQuote == null) return RedirectToAction("Login");

                                    var getRenInstalls = new XPCollection<RenQuoteInstallments>(uow,
                                        CriteriaOperator.Parse("PolicyNo = ? AND Descr = 'Deposit' AND IgnoreRec = '0'",
                                            getRenQuote.PolicyNo)).FirstOrDefault();
                                    if (getRenInstalls == null) return RedirectToAction("Login");

                                    var renPymnt = getRenInstalls.DownPymtAmount;
                                    var amtPrior = renPymnt.ToString().Parse<double>() -
                                                   model.csccAuthReply_amount.Parse<double>();
                                    var renAmt = model.csccAuthReply_amount.Parse<double>() - amtPrior;

                                    //Full renewal payment amt from cybersource
                                    if (model.csccAuthReply_amount.Parse<double>() > 0.0)
                                    {
                                        LogUtils.Log(agentEmailAddr,
                                            string.Format("{0}: SAVING RENEWAL PAYMENT INFORMATION TO PAYMENTS TABLE",
                                                model.pymntPolicyNumber), "INFO");
                                        var SaveRenPymnt = new XpoPayments(uow);
                                        SaveRenPymnt.PolicyNo = getRenQuote.PolicyNo;
                                        SaveRenPymnt.PaymentType = paymentType;
                                        SaveRenPymnt.PostmarkDate = now;
                                        SaveRenPymnt.AcctDate = now;
                                        SaveRenPymnt.CheckAmt = model.csccAuthReply_amount.Parse<double>();
                                        SaveRenPymnt.TotalAmt = model.csccAuthReply_amount.Parse<double>();
                                        SaveRenPymnt.UserName = "system";
                                        SaveRenPymnt.BatchGUID = pymntBatchGUID;
                                        SaveRenPymnt.CreateDate = now;
                                        SaveRenPymnt.TokenID = model.csrequestID;
                                        SaveRenPymnt.PortalRespCode = model.csreasonCode;
                                        SaveRenPymnt.PortalReplyMsg = model.decision;
                                        SaveRenPymnt.Save();
                                        uow.CommitChanges();
                                    }


                                    if (Math.Abs(amtPrior) > 1.0)
                                    {
                                        var SaveToRen = new XpoPayments(uow);
                                        SaveToRen.PolicyNo = getRenQuote.PolicyNo;
                                        SaveToRen.PaymentType = "MOV";
                                        SaveToRen.PostmarkDate = now;
                                        SaveToRen.AcctDate = now;
                                        SaveToRen.CheckAmt = amtPrior;
                                        SaveToRen.TotalAmt = amtPrior;
                                        SaveToRen.UserName = "system";
                                        SaveToRen.BatchGUID = pymntBatchGUID;
                                        SaveToRen.CreateDate = now;
                                        SaveToRen.TokenID = model.csrequestID;
                                        SaveToRen.PortalRespCode = model.csreasonCode;
                                        SaveToRen.PortalReplyMsg = model.decision;
                                        SaveToRen.CheckNotes = "MOVED FROM " + model.pymntPolicyNumber;
                                        SaveToRen.Save();
                                        uow.CommitChanges();

                                        var SaveToPrior = new XpoPayments(uow);
                                        SaveToPrior.PolicyNo = getRenQuote.PolicyNo;
                                        SaveToPrior.PaymentType = "MOV";
                                        SaveToPrior.PostmarkDate = now;
                                        SaveToPrior.AcctDate = now;
                                        SaveToPrior.CheckAmt = amtPrior * -1;
                                        SaveToPrior.TotalAmt = amtPrior * -1;
                                        SaveToPrior.UserName = "system";
                                        SaveToPrior.BatchGUID = pymntBatchGUID;
                                        SaveToPrior.CreateDate = now;
                                        SaveToPrior.TokenID = model.csrequestID;
                                        SaveToPrior.PortalRespCode = model.csreasonCode;
                                        SaveToPrior.PortalReplyMsg = model.decision;
                                        SaveToPrior.CheckNotes = "MONEY MOVED TO " + getRenQuote.PolicyNo;
                                        SaveToPrior.Save();
                                        uow.CommitChanges();
                                    }

                                    break;
                                default:
                                    LogUtils.Log(agentEmailAddr,
                                        string.Format("{0}: SAVING PAYMENT INFORMATION TO PAYMENTS TABLE",
                                            model.pymntPolicyNumber), "INFO");
                                    var SavePayment = new XpoPayments(uow);
                                    SavePayment.PolicyNo = model.pymntPolicyNumber;
                                    SavePayment.PaymentType = paymentType;
                                    SavePayment.PostmarkDate = now;
                                    SavePayment.AcctDate = now;
                                    SavePayment.CheckAmt = checkamt;
                                    SavePayment.TotalAmt = totalamt;
                                    SavePayment.UserName = "system";
                                    SavePayment.BatchGUID = pymntBatchGUID;
                                    SavePayment.CreateDate = now;
                                    SavePayment.TokenID = model.csrequestID;
                                    SavePayment.PortalRespCode = model.csreasonCode;
                                    SavePayment.PortalReplyMsg = model.decision;
                                    SavePayment.Save();
                                    uow.CommitChanges();
                                    break;
                            }
                            var _storedProcedures = new StoredProcedures(new StoredProcedureComponentsSql());
                            _storedProcedures.InstallmentPaymentApply(model.pymntPolicyNumber, now, "portal",Guid.NewGuid().ToString());
                        }

                        model.DisplayResponseMsg = responseDesc;
                        model.BuiltResponseMsg = "Thank you for your payment on " + model.pymntPolicyNumber + ".";
                        model.IsSuccessful = true;
                        model.DisplayCssClass = "success";
                        body = EmailRenderer.PaymentSuccess(name,
                            model.pymntPolicyNumber,
                            agentcode,
                            "Cybersource",
                            now,
                            totalamt,
                            "Payment made by agent " + Sessions.Instance.AgentCode + ". " + responseDesc);
                        if (paymentCount == 0)
                        {
                            try
                            {
                                if (WebHostService.IsProduction())
                                {
                                    var sendTo = "underwriting@palminsure.com";
                                    EmailService.SendEmailNotification(new List<string> {sendTo}, subject,
                                        body, true);
                                    LogUtils.Log(agentEmailAddr,
                                        string.Format("{0}: SENT EMAIL TO UNDERWRITING AT PALMINSURE.COM",
                                            model.pymntPolicyNumber), "INFO");
                                }
                            }
                            catch
                            {
                                LogUtils.Log(agentEmailAddr,
                                    string.Format("{0}: ERROR OCCURED SENDING MAIL TO UND AT PALMINSURE.COM",
                                        model.pymntPolicyNumber), "FATAL");
                            }

                            try
                            {
                                try
                                {
                                    if (WebHostService.IsProduction())
                                    {
                                        var sendTo = "underwriting@palminsure.com";
                                        EmailService.SendEmailNotification(new List<string> {sendTo}, subject,
                                            body, true);
                                        LogUtils.Log(agentEmailAddr,
                                            string.Format("{0}: SENT EMAIL TO AGENT AT {1}", model.pymntPolicyNumber,
                                                emailAddr), "INFO");
                                    }
                                }
                                catch (Exception)
                                {
                                    LogUtils.Log(agentEmailAddr,
                                        string.Format("{0}: FAILED SENDING EMAIL TO AGENT AT {1}",
                                            model.pymntPolicyNumber, emailAddr), "INFO");
                                }

                                if (sendToBoth)
                                    try
                                    {
                                        if (WebHostService.IsProduction())
                                        {
                                            var sendTo = "underwriting@palminsure.com";
                                            EmailService.SendEmailNotification(new List<string> {sendTo},
                                                subject, body, true);
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        LogUtils.Log(insuredEmailAddr,
                                            string.Format("{0}: FAILED SENDING PAYMENT EMAIL TO {2}",
                                                model.pymntPolicyNumber, emailAddr, insuredEmailAddr), "WARN");
                                    }
                            }
                            catch (NullReferenceException)
                            {
                                "Sending mail to agent/user crapped out, could not send".Log();
                                LogUtils.Log(agentEmailAddr,
                                    string.Format("{0}: SEND EMAIL TO AGENT FAILED, EMAIL ADDR USED {1}",
                                        model.pymntPolicyNumber, emailAddr), "WARN");
                            }
                        }

                        break;
                    case "FAIL":
                        LogUtils.Log(agentEmailAddr,
                            string.Format("{0}: PAYMENT REASON CODE RESULT WAS FAIL - {1}", model.pymntPolicyNumber,
                                responseDesc), "FATAL");
                        emailAddr = AgentUtils.GetEmailAddr(AgentUtils.GetCorrectAgentCode(model.pymntPolicyNumber));
                        model.DisplayResponseMsg = responseDesc;
                        model.IsSuccessful = false;
                        model.DisplayCssClass = "error";
                        model.BuiltResponseMsg =
                            "There was a problem with your payment, please view the above message for further instruction.";
                        var notes = string.Format(@"{0} payment {1}. {2}", userType,
                            model.IsSuccessful ? "successful" : "failed", responseDesc);
                        body = EmailRenderer.PaymentSuccess(name,
                            model.pymntPolicyNumber,
                            agentcode,
                            "Cybersource",
                            now,
                            totalamt,
                            notes);
                        if (WebHostService.IsProduction())
                            EmailService.SendEmailNotification(
                                new List<string> {"underwriting@palminsure.com"}, subject, body, true);

                        LogUtils.Log(agentEmailAddr,
                            string.Format("{0}: SENT FAILED PAYMENT EMAIL TO UNDERWRITING AT PALMINSURE.COM",
                                model.pymntPolicyNumber, emailAddr), "WARN");
                        try
                        {
                            if (WebHostService.IsProduction())
                                EmailService.SendEmailNotification(new List<string> {emailAddr}, subject, body,
                                    true);
                        }
                        catch (NullReferenceException)
                        {
                            "Sending mail to agent/user crapped out, could not send".Log();
                            LogUtils.Log(agentEmailAddr,
                                string.Format("{0}: SENT FAILED PAYMENT EMAIL TO AGENT AT {1}", model.pymntPolicyNumber,
                                    emailAddr), "WARN");
                        }

                        if (sendToBoth)
                            try
                            {
                                if (WebHostService.IsProduction())
                                    EmailService.SendEmailNotification(new List<string> {insuredEmailAddr},
                                        subject, body, true);
                            }
                            catch (Exception)
                            {
                                LogUtils.Log(insuredEmailAddr,
                                    string.Format("{0}: FAILED SENDING FAILED PAYMENT EMAIL TO {2}",
                                        model.pymntPolicyNumber, emailAddr, insuredEmailAddr), "WARN");
                            }

                        break;
                }

                PalmsClient.CloseRef();
                return View(model);
            }
        } //END Success
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        [HttpPost]
        public ActionResult UploadImage(string AgentCode, string PolicyNo, IEnumerable<HttpPostedFileWrapper> files)
        {
            DocumentService.UploadImage(AgentCode, PolicyNo, files);

            return RedirectToAction("Policy", "Agent", new { id = PolicyNo });
        }

        [HttpPost]
        public ActionResult UploadFiles(List<HttpPostedFileBase> files)
        {
            DocumentService.UploadFiles(files);
            return RedirectToAction("Policy", "Agent", new { id = Sessions.Instance.PolicyNo });
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult PaymentReceipt(string id, string pymnttype)
        {
            var path = AgentService.GetPaymentReceiptPath(id, pymnttype, Server,Url);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //        public void GenerateUmRejection(string policyNo)
        //        {
        //            PalmsClient.GenerateUmRejection(policyNo, "Policy");
        //        }

        public ActionResult DownloadAgentSuspenseReport()
        {
            var path = DocumentService.DownloadAgentSuspenseReport(Sessions.Instance.AgentCode,Sessions.Instance.Username);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }


        public ActionResult DownloadRenewalPolicyDec(string policyNo)
        {
            var path = DocumentService.DownloadRenewalPolicyDec(policyNo,PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadRenewalPaymentSchedule(string renewalPolicyNo)
        {
            var path = DocumentService.DownloadRenewalPaymentSchedule(renewalPolicyNo);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadRenewalInvoice(string renewalPolicyNo)
        {
            var path = DocumentService.DownloadRenewalInvoice(renewalPolicyNo);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadAgentInforceReport()
        {
            var path = DocumentService.DownloadAgentInforceReport(Sessions.Instance.AgentCode);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }


        public ActionResult DownloadAgentStatusReport()
        {
            var path = DocumentService.DownloadAgentStatusReport(Sessions.Instance.AgentCode,PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadAgentLossRatioReport()
        {
            var path = DocumentService.DownloadAgentLossRatioReport(Sessions.Instance.AgentCode,PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }


        public ActionResult DownloadPolicyIDCards()
        {
            var path = DocumentService.DownloadPolicyIdCards(Sessions.Instance.PolicyNo, PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadPolicyDocument(string id)
        {
            var path = DocumentService.DownloadPolicyDocument(id, out var fileName, PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
                Response.ContentType = "application/force-download";
                Response.AppendHeader("Content-Disposition", path);
                return File(path, "application/pdf");
            }
            var contentType = MimeMapping.GetMimeMapping(path);
            var cd = new ContentDisposition
            {
                FileName = fileName,
                Inline = true
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.ContentType = contentType;
            return File(path, contentType);
        }

        public ActionResult PrintAllDocuments(string policyno, bool policyDec = false, bool idCards = false,
            bool paySchedule = false, bool paymentReceiptLink = false)
        {
            var path = DocumentService.GetCompiledPdf(policyno, policyDec, idCards, paySchedule, paymentReceiptLink, PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadPolicyDec()
        {
            var path = DocumentService.DownloadPolicyDec(Sessions.Instance.PolicyNo, PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadApplicationForInsurance()
        {
            var policyno = Sessions.Instance.PolicyNo;
            var path = DocumentService.DownloadApplicationForInsurance(policyno, PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }

        public ActionResult DownloadPolicyPaymentSchedule(string type = "NEW")
        {
            var policyno = Sessions.Instance.PolicyNo;
            var path = DocumentService.DownloadPolicyPaymentSchedule(policyno, PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");

        }

        public ActionResult DownloadPolicyInvoice(string type = "NEW")
        {
            var policyno = Sessions.Instance.PolicyNo;
            var path = DocumentService.DownloadPolicyInvoice(policyno,PalmsClient);
            if (path.IsNullOrEmpty())
            {
                path = Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf");
            }
            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition", path);
            return File(path, "application/pdf");
        }


        public ActionResult PleaseCall()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SendESignRequestForReinstatementNoLossRequest(UnitOfWork uow, string policyNo,
            string dateOfReinstatement, string reinsateStage = "")
        {
            LogUtils.Log(Sessions.Instance.Username, string.Format(
                "{0}: SendESignRequestForReinstatementNoLossRequest called, REIDate:{1},reinsateStage {2}",
                Sessions.Instance.PolicyNo, dateOfReinstatement, reinsateStage), "INFO");

            var isActionComplete = false;
            var isReinstateCompleted = false;
            if (string.IsNullOrEmpty(reinsateStage))
                isActionComplete = false;

            try
            {
                if (reinsateStage.Equals("Reinstate Policy To Active"))
                {
                    var totalAmtDue = AgentService.GetBalanceToPayForReinstatement(policyNo);

                    //Call function which will actually Reinstate the 
                    // Payment call to get the payment done.
                    if (AgentService.ReinstateCancelPolicyToActive(policyNo) && totalAmtDue <= 0)
                    {
                        isActionComplete = true;
                        isReinstateCompleted = true;
                    }
                }

                if (reinsateStage.Equals("Request Re-Instatement"))
                {
                    var ssrsHelpers = new SSRSHelpers();
                    var reinstatementNoLossFile = ssrsHelpers.GenerateReinstatementNoLossRequest(policyNo, uow);
                    AgentControllerLog.Info(
                        $"GenerateReinstatementNoLossRequest: PolicyNo={policyNo}, request PDF File Created= {reinstatementNoLossFile}");

                    if (!string.IsNullOrEmpty(reinstatementNoLossFile))
                        using (var uowrk = XpoHelper.GetNewUnitOfWork())
                        {
                            var existingPolicy =
                                uowrk.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                            var userEmailMap = new Dictionary<string, string>();
                            var insuredFirst = existingPolicy.Ins1First;
                            var insuredLast = existingPolicy.Ins1Last;

                            if (existingPolicy != null && !string.IsNullOrEmpty(insuredFirst) &&
                                !string.IsNullOrEmpty(insuredLast) &&
                                !string.IsNullOrEmpty(existingPolicy.MainEmail))
                            {
                                var insuredName = $"{insuredFirst} {insuredLast}";
                                userEmailMap.Add(insuredName, existingPolicy.MainEmail);
                            }
                            else
                            {
                                AgentControllerLog.Info(
                                    $"SendESignRequestForReinstatementNoLossRequest:{policyNo} doesnot have Isnured name or email. FirstName={existingPolicy.Ins1First} LastName={existingPolicy.MainEmail} MainEmail={existingPolicy.MainEmail}");
                            }

                            var agent = uowrk.FindObject<XpoAgents>(CriteriaOperator.Parse("AgentCode = ?",
                                existingPolicy.AgentCode));

                            //if (agent != null && !string.IsNullOrEmpty(agent.AgencyName)
                            //                    && existingPolicy != null
                            //                    && !string.IsNullOrEmpty(existingPolicy.AgentESignEmailAddress))
                            //{
                            //    userEmailMap.Add(agent.AgencyName, existingPolicy.AgentESignEmailAddress); //existingPolicy.AgentESignEmailAddress);
                            //}
                            if (agent != null && !string.IsNullOrEmpty(agent.AgencyName) &&
                                !string.IsNullOrEmpty(agent.EmailAddress))
                                userEmailMap.Add(agent.AgencyName, agent.EmailAddress);
                            else
                                AgentControllerLog.Info(
                                    $"SendESignRequestForReinstatementNoLossRequest:{policyNo} doesnot have Agency email id for Agent code {agent.AgentCode}");


                            if (userEmailMap.Any())
                            {
                                var ccEmails = new List<string>();
                                if (ConfigSettings.ReadSetting("IIS_SERVER")?.ToUpper() == "IIS-AAINS-DEV")
                                    ccEmails.Add("alok81@gmail.com");
                                else
                                    ccEmails.Add("underwriting@palminsure.com");

                                IntegrationsUtility.SendESignatureRequest(policyNo, userEmailMap,
                                    new List<string> {reinstatementNoLossFile}, ccEmails, "ReinstatementRequest");

                                isActionComplete = true;
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                LogUtils.Log(Sessions.Instance.Username, "Error in processing and sending ESign email .", "INFO");
                return Json(new {returnMsg = "FAILURE", exceptionMsg = ex.Message});
                ;
            }

            if (isActionComplete)
                return Json(new {returnMsg = "SUCCESS", REINSTATECOMPLETED = isReinstateCompleted});
            return Json(new
            {
                returnMsg = "FAILURE",
                exceptionMsg =
                    "An error occurred while processing your Reinstatement request. Please contact support if you continue to receive this message."
            });
        }


        public ActionResult GetPaymentForReinstatementOfCancelledPolicy(string policyNo)
        {
            var totalDueAmt = AgentService.GetBalanceToPayForReinstatement(policyNo);

            return RedirectToAction("CardConnectPayment", "Quote", new
            {
                quoteNo = policyNo,
                minimumPayAmt = totalDueAmt,
                isInitialDepostPayment = false,
                isEndorsementPayment = false,
                endorsementEffectiveDate = DateTime.Now,
                isPayForReinstatement = true
            });
        }


        public ActionResult IssueReinstatementDetails(string policyNo, double paidAmt, string payrecieptLink,
            bool isAgentSweep = false)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: IssueReinstatementDetails PolicyNO:{1}", policyNo, payrecieptLink), "INFO");
            var reinstatementfileLink = string.Empty;
            var listRecieptPDFs = new List<string>();
            var concatPdfLink = string.Empty;
            var concatPdfForPrintFilePath = string.Format(@"\\{0}\pdfs\{1}_REIDocs.pdf",
                ConfigSettings.ReadSetting("IIS_Server"), policyNo);
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var policydata =
                        uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));


                    var reiguid = Guid.NewGuid().ToString();
                    PortalUtils.AuditDataForReinstatementACH("REINSTATEMENT", paidAmt, policyNo, reiguid,
                        Sessions.Instance.Username);
                    var selData = uow.ExecuteSproc("spUW_IssueReinstatement", policyNo, Sessions.Instance.Username,
                        reiguid);

                    reinstatementfileLink =
                        ssrsHelpers.GenerateNoticeOfReinstatementDoc(policyNo, policydata.CancelDate, uow);
                    var policyafterrei = uow.FindObject<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo));
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: IssueReinstatementDetails Policy status after REI:{1}",
                            policyafterrei.PolicyStatus, payrecieptLink), "INFO");

                    if (!string.IsNullOrEmpty(payrecieptLink))
                        listRecieptPDFs.Add(payrecieptLink);
                    if (!string.IsNullOrEmpty(reinstatementfileLink))
                        listRecieptPDFs.Add(reinstatementfileLink);

                    SSRSHelpers.ConcatPDFs(listRecieptPDFs, concatPdfForPrintFilePath);

                    var reifileName = Path.GetFileName(reinstatementfileLink);
                    var payreceiptfileName = Path.GetFileName(payrecieptLink);
                    var concatPdffileName = Path.GetFileName(concatPdfForPrintFilePath);
                    reinstatementfileLink =
                        string.Format(ConfigSettings.ReadSetting("PolicyDocsBaseUrl") + @"/Files/{0}", reifileName);
                    payrecieptLink = string.Format(ConfigSettings.ReadSetting("PolicyDocsBaseUrl") + @"/Files/{0}",
                        payreceiptfileName);
                    concatPdfLink = string.Format(ConfigSettings.ReadSetting("PolicyDocsBaseUrl") + @"/Files/{0}",
                        concatPdffileName);
                    return RedirectToAction("ReinstatementPayment", "Quote",
                        new
                        {
                            policyNo, paymentReceiptUrl = payrecieptLink, payREINoticeUrl = reinstatementfileLink,
                            pdfConcatFileLink = concatPdfLink,
                            isAgentSweep
                        });
                }
            }
            catch (Exception ex)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Exception in IssueReinstatementDetails PolicyNO:{0}", policyNo), "INFO");
            }
            finally
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format(
                        "{0}: Finally exec in IssueReinstatementDetails PolicyNO: REIPdfile{1} ConcatPDfFile{2} PayReciept {3}",
                        policyNo, reinstatementfileLink, concatPdfLink, payrecieptLink), "INFO");
            }

            return RedirectToAction("Policy", new {id = policyNo});
        }

        #region Violations

        [HttpPost]
        public ActionResult ViolationAdd(EndorsementViolationAddModel model)
        {
            LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: ADD VIOLATIONS", Sessions.Instance.PolicyNo),
                "INFO");
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                ////////////////////
                // Get Violation Data
                var cycle = XpoAARateCycles.GetCurrentCycle(uow).RateCycle;
                var lookupViolation = new XPCollection<XpoAARateViolations>(uow,
                        CriteriaOperator.Parse("RateCycle = ? AND LongDesc = ?", cycle, model.Description))
                    .FirstOrDefault();
                ////////////////////
                if (lookupViolation == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VIOLATION DOES NOT EXIST", Sessions.Instance.PolicyNo), "FATAL");
                    throw new HttpException(400,
                        string.Format("Could not find violation in cycle {0} by description: {1}", cycle,
                            model.Description));
                }

                var driverIndex = Convert.ToInt32(TempData["DriverIndex"]);

                var getViolations =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", Sessions.Instance.PolicyNo,
                            driverIndex));

                var violation = new PolicyEndorseQuoteDriversViolations(uow);
                violation.DriverIndex = driverIndex;
                violation.PolicyNo = Sessions.Instance.PolicyNo;
                violation.ViolationNo = getViolations.Count + 1;
                violation.ViolationIndex = getViolations.Count + 1;
                violation.ViolationDate = model.Date;
                violation.ViolationDesc = model.Description;
                violation.ViolationPoints = lookupViolation.PointsFirst;
                violation.ViolationGroup = lookupViolation.ViolationGroup;
                violation.FromMVR = model.FromMvr;
                violation.ViolationCode = lookupViolation.Code;
                violation.Save();
                uow.CommitChanges();
                getViolations.Add(violation);
                var violations =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?",
                            Sessions.Instance.PolicyNo,
                            driverIndex)); //model.DriverIndex));   LF changed 11/7/18  - model.DriverIndex=0
                var listViolations = new List<EndorsementViolationModel>();
                foreach (var driverViolation in violations)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: LOAD VIOLATIONS FROM DRIVER TO GRID FOR DISPLAY",
                            Sessions.Instance.PolicyNo), "INFO");
                    var vModel = new EndorsementViolationModel();
                    vModel.ViolationId = driverViolation.OID;
                    vModel.Description = driverViolation.ViolationDesc;
                    vModel.Date = driverViolation.ViolationDate;
                    vModel.Points = driverViolation.ViolationPoints;
                    vModel.FromMvr = driverViolation.FromMVR;
                    listViolations.Add(vModel);
                }

                TempData["EndorsePolicyModel"] = TempData["EndorsePolicyModel"];
                TempData["DriverModel"] = TempData["DriverModel"];
                TempData["DriverID"] = TempData["DriverID"];
                TempData["DriverIndex"] = driverIndex; //LF 12/9/18
                return PartialView("~/Views/Agent/DisplayTemplates/Violations.cshtml", listViolations);
            }

            //throw new NotImplementedException();
        }

        // id = Violation.OID
        public ActionResult ViolationDelete(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var violation =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow, CriteriaOperator.Parse("OID = ?", id))
                        .FirstOrDefault();
                var driverIndex = violation.DriverIndex;
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: DELETE VIOLATION FOR DRIVER {1}", Sessions.Instance.PolicyNo, driverIndex),
                    "WARN");
                if (violation == null)
                {
                    //throw new HttpException(404, "Vehicle not found");
                }

                violation.Delete();
                uow.CommitChanges();
                DbHelperUtils.DeleteRecord("PolicyEndorseQuoteDriversViolations", "OID", id);
                var violations =
                    new XPCollection<PolicyEndorseQuoteDriversViolations>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?",
                            Sessions.Instance.PolicyNo,
                            driverIndex));
                var listViolations = new List<EndorsementViolationModel>();
                foreach (var driverViolation in violations)
                {
                    var vModel = new EndorsementViolationModel();
                    vModel.ViolationId = driverViolation.OID;
                    vModel.Description = driverViolation.ViolationDesc;
                    vModel.Date = driverViolation.ViolationDate;
                    vModel.Points = driverViolation.ViolationPoints;
                    vModel.FromMvr = driverViolation.FromMVR;
                    vModel.FromAplus = driverViolation.FromAPlus;
                    vModel.FromCV = driverViolation.FromCV;
                    listViolations.Add(vModel);
                }

                return PartialView("~/Views/Agent/DisplayTemplates/Violations.cshtml", listViolations);
            }
        }

        #endregion

        #region Vehicles

        [HttpGet]
        public ActionResult AddVehicle()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            if (TempData["FromRating"] != null) TempData["FromRating"] = false;

            TempData["LanguagePreference"] = TempData["LanguagePreference"];

            var endorseModel = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;
            var vehicleID = TempData["VehicleID"] as string;
            var carIndex = TempData["CarIndex"] as string;
            var vehicleModel = AgentService.GetVehicle(vehicleID, endorseModel);

            ViewBag.CompDed = endorseModel.CompDed;
            ViewBag.CollDed = endorseModel.CollDed;

            TempData["EndorsePolicyModel"] = endorseModel;
            TempData["VehicleModel"] = vehicleModel;
            TempData["ClearEndorseQuoteTables"] =
                false; // true; // false;  //LF 12/6/18 - to not get a previously unissued endorsement
            TempData["EndorsementDate"] = TempData["EndorsementDate"];
            TempData["LanguagePreference"] = TempData["LanguagePreference"];
            TempData["VehicleID"] = vehicleID;
            TempData["CarIndex"] = vehicleModel.CarIndex;

            var result = AgentService.GetMissingEndorseReasons(Sessions.Instance.PolicyNo, "0");
            TempData["AgentEndorsementAllReasons"] = result.AllReasons;
            ViewBag.UnSelectedEndorseReasons = result.UnSelectedEndorseReasons;
            ViewBag.SelectedEndorseReasons = result.SelectedEndorseReasons;
            return View("AddVehicle", vehicleModel);
        }


        [HttpGet]
        public ActionResult ResetVehicle()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var endorsementModel = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;
            var carIndex = Convert.ToInt32(TempData["CarIndex"]); //LF added 10/9/18

            var vehicleModel =
                endorsementModel.Vehicles.Find(v =>
                    v.CarIndex ==
                    carIndex); //quoteModel.Vehicles[carIndex - 1]; // quoteModel.Vehicles.Find(v => v.VehicleId == vehicleID);
            if (vehicleModel == null)
            {
                if (carIndex == 0)
                {
                    vehicleModel = new EndorsementVehicleModel();

                    vehicleModel.Year = 0;
                    vehicleModel.Make = "";
                    vehicleModel.VehModel = "";
                    vehicleModel.Style = "";
                    vehicleModel.VIN = "";
                    vehicleModel.Artisan = false;
                    vehicleModel.HasAirbag = false;
                    vehicleModel.HasAntiLockBrakes = false;
                    vehicleModel.HasAntiTheftDevice = false;
                    vehicleModel.HasConvOrTT = false;
                    vehicleModel.HasPhyDam = false;
                    vehicleModel.IsBusinessUse = false;
                    vehicleModel.IsCustomized = false;
                    vehicleModel.ComprehensiveDeductible = "NONE";
                    vehicleModel.CollisionDeductible = "NONE";
                }
                else
                {
                    throw new Exception($"Car {carIndex} not found");
                }
            }

            ViewBag.CompDed = endorsementModel.CompDed;
            ViewBag.CollDed = endorsementModel.CollDed;

            TempData["EndorsePolicyModel"] = endorsementModel;
            TempData["VehicleModel"] = vehicleModel;
            TempData["VehicleID"] = TempData["VehicleID"];
            TempData["CarIndex"] = vehicleModel.CarIndex;
            TempData["EndorsementDate"] = TempData["EndorsementDate"];
            TempData["ClearEndorseQuoteTables"] = false;
            TempData["LanguagePreference"] = TempData["LanguagePreference"];

            return View("AddVehicle", vehicleModel);
        }

        [HttpPost]
        public ActionResult VehicleAdd(EndorsementVehicleModel model, FormCollection formData)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            if (TempData["FromRating"] != null) TempData["FromRating"] = false;

            TempData["LanguagePreference"] = TempData["LanguagePreference"];

            var isModelValid = true;
            if (model.HasPhyDam)
            {
                if (model.ComprehensiveDeductible == "NONE" || model.ComprehensiveDeductible == "0" ||
                    model.ComprehensiveDeductible == "")
                {
                    ModelState.AddModelError("ComprehensiveDeductible",
                        "Comp/Coll deductibles are required with physical damage.");
                    return View("AddVehicle", model);
                }

                var policy = AgentService.FindPolicy(Sessions.Instance.PolicyNo);

                //check if the agent is restricted for adding vehicles with Phy Damage coverage or not.
                if (policy != null)
                {
                    var agentRestrictions = AgentService.AgentsRestrictionByZipAndCoverage(policy);
                    if (agentRestrictions?.RestrictedCoverageName != null &&
                        agentRestrictions.RestrictedCoverageName.Equals("PhyDam"))
                        //If A plan is being used that is not 26% or 100% PIF, vehicle cannot be added as it would violate the restriction on pay plans for this agent and zip code.
                        if (PortalUtils.IsPolicyPayPlan33PercentDownOrPaidInFull(policy))
                        {
                            ModelState.AddModelError("PayPlans",
                                $"Due to your underwriting results, your agency cannot add a vehicle with physical damage in the area {policy.GarageZip}, as the current pay plan on the policy is not approved for this transaction. Please contact your Marketing Representative for additional information.");
                            return View("AddVehicle", model);
                        }
                }
            }

            if (isModelValid && !string.IsNullOrEmpty(model.VIN))
            {
                var vehicleInfo = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, model.VIN)?.Vehicle;
                if (vehicleInfo == null)
                {
                    ModelState.AddModelError("VIN",
                        "This vehicle's VIN is not in our records, cannot add this vehicle.");
                    isModelValid = false;
                }
            }

            if (!isModelValid)
                return View("AddVehicle", model);

            TempData["LanguagePreference"] = TempData["LanguagePreference"];

            if (TempData["EndorsePolicyModel"] != null) TempData["EndorsePolicyModel"] = TempData["EndorsePolicyModel"];

            if (TempData["VehicleID"] != null) TempData["VehicleID"] = TempData["VehicleID"];

            //Check for symbols
            if (!model.HasAllSymbols() && !string.IsNullOrEmpty(model.VIN))
                try
                {
                    var vehicleInfo = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, model.VIN);
                    if (vehicleInfo != null)
                    {
                        AgentService.GetVehicleSymbols(model, vehicleInfo);
                    }
                    else
                    {
                        ModelState.AddModelError("Symbols",
                            "Unable to order symbols for VIN. Please contact underwriting for assistance.");
                        return View("AddVehicle", model);
                    }
                }
                catch (Exception e)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("Error occurred ordering symbols for Policy: {0}, VIN: {1} - Error: {2}",
                            Sessions.Instance.PolicyNo, model.VIN, e.Message),
                        "INFO");
                    ModelState.AddModelError("Symbols",
                        "Unable to order symbols for VIN. Please contact underwriting for assistance.");
                    return View("AddVehicle", model);
                }

            var endorseModel = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;

            AgentService.AddVehicleEndorsement(model, endorseModel);

            if (model.IsEdit)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: EDITING VEHICLE INFO FOR CAR {1}", Sessions.Instance.PolicyNo, model.CarIndex),
                    "INFO");
                AgentService.UpdateVehicle(model, formData, endorseModel);
                TempData["EndorsePolicyModel"] = endorseModel; //LF 5/1/19  was TempData["EndorsementPolicyModel"]
                TempData["VehicleModel"] = model;
                TempData["VehicleID"] = ""; //LF 11/2/18
                TempData["ClearEndorseQuoteTables"] =
                    false; // true; // false;  //LF 12/6/18 - to not get a previously unissued endorsement
                TempData["EndorsementDate"] = TempData["EndorsementDate"];
                TempData["LanguagePreference"] = TempData["LanguagePreference"];
                return RedirectToAction("Endorsement", "Agent", new { id = Sessions.Instance.PolicyNo });
            }

            AgentService.AddVehicle(model, formData, endorseModel);
            TempData["EndorsePolicyModel"] = endorseModel; //LF 5/1/19  was TempData["EndorsementPolicyModel"]
            TempData["VehicleModel"] = model;
            TempData["CarIndex"] = "";
            TempData["VehicleID"] = ""; //LF 11/7/18
            TempData["ClearEndorseQuoteTables"] =
                false; // true; // false;  //LF 12/6/18 - to not get a previously unissued endorsement
            TempData["LanguagePreference"] = TempData["LanguagePreference"];
            TempData["EndorsementDate"] = TempData["EndorsementDate"];
            return RedirectToAction("Endorsement", "Agent", new { id = Sessions.Instance.PolicyNo });
        }

        //END VehicleAdd
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult VehicleLoad(string id)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            if (TempData["FromRating"] != null) TempData["FromRating"] = TempData["FromRating"];

            TempData["LanguagePreference"] = TempData["LanguagePreference"];

            var endorseModel = TempData["EndorsePolicyModel"] as EndorsementPolicyModel;
            var model = AgentService.VehicleLoad(id, endorseModel);
            TempData["EndorsePolicyModel"] = endorseModel;
            TempData["VehicleID"] = id;
            TempData["CarIndex"] = model.CarIndex; //LF  added 11/7/18
            return RedirectToAction("AddVehicle", model);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult VehicleDelete(string id)
        {
            var cars = AgentService.VehicleDelete(id);
            TempData["EndorsePolicyModel"] = TempData["EndorsePolicyModel"];
            TempData["VehicleID"] = TempData["VehicleID"];
            ViewBag.EndorsementSelectedReasons =
                AgentService.GetSelectedAgentEndorsementReasons(Sessions.Instance.PolicyNo);
            return PartialView("~/Views/Agent/DisplayTemplates/Vehicles.cshtml",
                cars.Select(c => UtilitiesRating.MapXpoToModel(c)).ToList());
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #endregion

        #region Lienholders

        [HttpPost]
        public ActionResult LienHolderAdd(EndorsementLienHolderAddModel model)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: ADD LIENHOLDER DATA TO CAR {1}", Sessions.Instance.PolicyNo, model.CarIndex),
                "INFO");
            var carIndex = Convert.ToInt32(TempData["CarIndex"]); //LF added 11/7/18
            var listLienHolderModel = AgentService.AddLienHolder(model, carIndex);
            TempData["EndorsePolicyModel"] = TempData["EndorsePolicyModel"];
            TempData["VehicleID"] = TempData["VehicleID"];
            TempData["CarIndex"] = carIndex; //LF 12/9/18 - added this line
            return PartialView("~/Views/Agent/DisplayTemplates/LienHolders.cshtml", listLienHolderModel);
        }

        public ActionResult LienHolderDelete(string id)
        {
            var listLiens = AgentService.DeleteLienHolder(id);
            TempData["EndorsePolicyModel"] = TempData["EndorsePolicyModel"];
            TempData["VehicleID"] = TempData["VehicleID"];
            return PartialView("~/Views/Agent/DisplayTemplates/LienHolders.cshtml", listLiens);
        }

        #endregion

        #region DisallowedEndorsement

        [HttpPost]
        public ActionResult DisallowedEndorsement(DisallowedEndorsementModel model, FormCollection fc)
        {
            if (model.DisallowedEndorsementQuoteId > 0)
            {
                var disallowedQuote = AgentService.GetDisallowedAgentEndorsementQuote(model, fc, out var nonPremChanges, out var premChanges);
                if (nonPremChanges && !premChanges)
                    return RedirectToAction("IssueEndorsementDetails", new
                    {
                        policyNo = disallowedQuote.PolicyNo,
                        diffAmount = 0,
                        effDate = disallowedQuote.EndorsementEffDate,
                        isNonPremBearingsOnly = "1"
                    });
                return RedirectToAction("IssueEndorsementDetailsWithPayment", new
                {
                    policyNo = disallowedQuote.PolicyNo,
                    diffAmount = disallowedQuote.AdditionalPremiumCollected,
                    effDate = disallowedQuote.EndorsementEffDate
                    //previewChanges = false
                });
            }
            return View("DisallowedEndorsements", model);
        }

        public ActionResult ShowEndorsementDiffernces(string policyNumber, string errorMessage = null)
        {

            ModelState.AddModelError("PolicyNumber", errorMessage);
            var model = AgentService.GetDisallowedEndorsements(policyNumber,errorMessage);

            return View("DisallowedEndorsements", model);
        }

        public ActionResult SendEndorsementRequest(string policyNumber = null, DateTime? endorsementEffDate = null,
            string diffAmount = null, int? disAllowedEndorsementQuoteId = 0)
        {
            var model = new DisallowedEndorsementModel();
            model.PolicyNumber = policyNumber;
            if (string.IsNullOrEmpty(diffAmount) && TempData["PolicyEndorseDiffAmt"] != null)
                diffAmount = TempData["PolicyEndorseDiffAmt"].ToString();
            double amount;
            double.TryParse(diffAmount, out amount);
            if (endorsementEffDate == null && TempData["EndorsementDate"] != null)
                endorsementEffDate = Convert.ToDateTime(TempData["EndorsementDate"].ToString());

            var errorMessage = AgentService.ValidateEndorsement(policyNumber, true);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                TempData["ClearEndorseQuoteTables"] = false;
                return RedirectToAction("Endorsement",
                    new
                    {
                        id = policyNumber,
                        endorsementDate = endorsementEffDate,
                        errorMessage
                    });
            }


            AgentControllerLog.Info(
                $"Start DisallowedEndorsement: policyno- {policyNumber},endorsementEffDate={endorsementEffDate},diffAmount={diffAmount}");

            //TODO: BM - why is this looped 7 times?
            for (var i = 0; i < 7; i++)
            {
                model = new DisallowedEndorsementModel();
                model.PolicyNumber = policyNumber;
                model.DisallowedEndorsementQuoteId =
                    AgentService.GetDisallowedEndorsementChanges(policyNumber, model, amount, endorsementEffDate);

                if (model.DisallowedEndorsementQuoteId > 0)
                    break;
            }

            return View("DisallowedEndorsements", model);
        }

        public ActionResult BackToEndorsment(string policyNo, string EndorsementEffDate)
        {
            TempData["FromRating"] = null;
            TempData["ClearEndorseQuoteTables"] = false;
            return RedirectToAction("Endorsement", "Agent", new {id = policyNo, endorsementDate = EndorsementEffDate});
        }

        public ActionResult PaymentSchedule(string policyNo)
        {
            var model = PortalPaymentUtils.GetPaymentSchedule(policyNo);
            return View(model);
        }


        public ActionResult PaymentsDataDetails(string policyNo)
        {
            var paymentsmodel = PortalPaymentUtils.GetPolicyPaymentsDataForPaySchedule(policyNo);
            return View(paymentsmodel);
        }

        #endregion
    } //END Class
} //END Namespace