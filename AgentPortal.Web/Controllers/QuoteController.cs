﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using AgentPortal.Models;
using AgentPortal.Repositories;
using AgentPortal.Repositories.StoredProcedures;
using AgentPortal.Services;
using AgentPortal.Support;
using AgentPortal.Support.Attributes;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;

//using DevExpress.Data.Filtering;
//using DevExpress.Xpo;
using log4net;
using PalmInsure.Palms.Utilities;

namespace AgentPortal.Controllers
{
    public class QuoteController : BaseController
    {
        private readonly QuoteService QuoteService;

        public ReportService ReportService;

        #region Creation

        private List<string> listPDFs = new List<string>();
        private List<string> listParamNames = new List<string>();
        private List<string> listParamValues = new List<string>();
        private string reportName = null;
        private readonly ILog quoteControllerLog = LogManager.GetLogger("QuoteControllerLogger");

        public QuoteController(QuoteService quoteService, ReportService reportService)
        {
            QuoteService = quoteService;
            ReportService = reportService;
        }
        #region Helper Methods

        private string GetUserIP()
        {
            var ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList)) return ipList.Split(',')[0];

            return Request.ServerVariables["REMOTE_ADDR"];
        }

        public ActionResult Index()
        {
            return RedirectToAction("Create");
        }

        #endregion

        public ActionResult Create()
        {
            if (!PortalRightsUtils.HasRights("QUOTE EDIT", Sessions.Instance.AgentCode, true))
                return RedirectToAction("Search", "Agent");

            if (AgentUtils.GetScalar<bool>("IsRequireCall", Sessions.Instance.AgentCode))
                return RedirectToAction("PleaseCall", "Agent");

            var model = QuoteService.CreateQuote(); // IQuoteService later on

            Sessions.Instance.PrevAction = "CREATE";
            TempData["DriverId"] = "";
            TempData["VehicleId"] = "";
            LogUtils.Log(Sessions.Instance.Username, string.Format("Created Quote {0}".ToUpper(), model.QuoteId),
                "DEBUG");
            return RedirectToAction("Edit", new {id = Blowfish.Encrypt_CBC(model.QuoteId)});
        }

        public ActionResult Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
                return RedirectToAction("Search", "Agent");
            if (string.IsNullOrEmpty(Sessions.Instance.AgentCode)) return RedirectToAction("Login", "Agent");
            /*
				[NOTE] Quote Edit View
				---------------------------- 
				Top portaion of the edit controller is used for briding from Quickaja Quote and Accu Auto.
				It will do any relevant checks against PolicyQuote and Agents tables to decide if the agent
				has proper credentials to auto login and go straight to the edit quote. 
			  
				If something does not match, agent is dumped to the login screen where they can login and search for what they need.
			*/

            try
            {
                id = Blowfish.Decrypt_CBC(id);
            }
            catch
            {
                //return RedirectToAction("Search", "Agent");  //LF 5/1/19
            }

            Sessions.Instance.PolicyNo = id;
            if (string.IsNullOrEmpty(Sessions.Instance.AgentCode)) //LF ???
            {
                //init vars
                string agentPass;
                IAgents getAgentData;
                using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                {
                    //----------------
                    var getQuoteData = uow.QuoteRepository.GetQuote(id);
                    if (getQuoteData == null) return RedirectToAction("Search", "Agent");

                    var raterType = getQuoteData.FromRater;
                    var agentEmailAddr = getQuoteData.AgentAccount.Trim();
                    Sessions.Instance.EmailAddress = agentEmailAddr;
                    var quotePass = string.IsNullOrEmpty(getQuoteData.AgentPassword)
                        ? ""
                        : Blowfish.Decrypt_CBC(getQuoteData.AgentPassword);
                    //----------------
                    //Getr agents password/info based on incoming rater type
                    if (raterType == "AA")
                    {
                        getAgentData = uow.AgentRepository.GetAgentByAlternateEmail(agentEmailAddr);

                        if (getAgentData != null)
                        {
                            agentPass = string.IsNullOrEmpty(getAgentData.AltPassword)
                                ? ""
                                : Blowfish.Decrypt_CBC(getAgentData.AltPassword);
                            Sessions.Instance.AgentCode = getAgentData.AgentCode;
                            Sessions.Instance.IsAuth = true;
                            Sessions.Instance.IsAgent = true;
                        }
                        else
                        {
                            return RedirectToAction("Login", "Agent");
                        }
                    }
                    else
                    {
                        getAgentData = uow.AgentRepository.GetAgentByEmail(agentEmailAddr);

                        if (getAgentData != null)
                        {
                            agentPass = string.IsNullOrEmpty(getAgentData.Password)
                                ? ""
                                : Blowfish.Decrypt_CBC(getAgentData.Password);
                            Sessions.Instance.AgentCode = getAgentData.AgentCode;
                            Sessions.Instance.IsAuth = true;
                            Sessions.Instance.IsAgent = true;
                        }
                        else
                        {
                            return RedirectToAction("Login", "Agent");
                        }
                    }

                    //----------------
                    if (agentPass != quotePass) return RedirectToAction("Login", "Agent");
                    var updateQuote = uow.QuoteRepository.GetQuote(Sessions.Instance.PolicyNo);
                    updateQuote.Bridged = !string.IsNullOrEmpty(raterType);
                    updateQuote.AgentCode = Sessions.Instance.AgentCode;
                    uow.QuoteRepository.SaveQuote(updateQuote);
                    uow.PolicyRepository.SavePolicyPreference(Sessions.Instance.PolicyNo, "Preferred_Language", "ENGLISH");
                    uow.Save();

                    //----------------
                    LogUtils.Log(agentEmailAddr, GetUserIP());

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("Bridged from {0} for Quote {1}".ToUpper(), raterType,
                            Sessions.Instance.PolicyNo),
                        "DEBUG");
                }
            } //END Pre-Quote Edit briding to see if agent is able to auto login

            //------------------------------------------------------------------------------
            if (!PortalRightsUtils.HasRights("QUOTE EDIT", Sessions.Instance.AgentCode, true))
                return RedirectToAction("Search", "Agent");

            //------------------------------------------------------------------------------
            Sessions.Instance.IsNewQuote = Sessions.Instance.PrevAction == "CREATE" ? true : false;
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {

                var preferenceForQuote = uow.PolicyRepository.GetPolicyPreference(Sessions.Instance.PolicyNo, "Preferred_Language");
                string languagePreference;
                if (preferenceForQuote == null)
                {
                    languagePreference = "ENGLISH";
                    uow.PolicyRepository.SavePolicyPreference(Sessions.Instance.PolicyNo,"Preferred_Language",languagePreference);
                    uow.Save();
                }
                else
                {
                    languagePreference = preferenceForQuote.Value;
                }

                var compDed = "NONE";
                var collDed = "NONE";
                id = string.IsNullOrEmpty(id) ? Sessions.Instance.PolicyNo : id;
                var quote = uow.QuoteRepository.GetQuote(id);
                //----------------------
                if (quote == null)
                {
                    LogUtils.Log(Sessions.Instance.Username, string.Format("Quote Not Found: {0}".ToUpper(), id),
                        "FATAL");
                    return RedirectToAction("Search", "Agent", new {id = "Q404"});
                }

                var model = QuoteService.CreateQuoteCars(quote as XpoPolicyQuote, languagePreference, compDed, collDed, id);

                /*     else
                     {
                         if (model.WorkLossGroup == 1 || model.WorkLossGroup == 2)
                             model.Workloss = true;
                         else
                             model.Workloss = false;
                     }  */
                //LF 4/1/19 shouldn't be here            if (model.Workloss == false)
                //                model.WorkLossGroup = 2; // 3;
                //----------------------
                Sessions.Instance.PrevAction = "EDIT";
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("Edit Quote {0}".ToUpper(), Sessions.Instance.PolicyNo), "INFO");
                TempData["QuoteModel"] = model;
                TempData["DriverId"] = "";
                TempData["VehicleId"] = "";
                return View(model); // --> post to method below titled "Edit"
            }
        }

        [HttpPost]
        public ActionResult Edit(QuoteModel model)
        {
            if (!PortalRightsUtils.HasRights("QUOTE EDIT", Sessions.Instance.AgentCode, true))
                return RedirectToAction("Search", "Agent");

            var quoteNumber = model.QuoteId; //Issue: goes nowhere
            //model.Validate(null).ToList();
            Sessions.Instance.IsNewQuote = false;
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var getDrivers = uow.QuoteRepository.GetQuoteDrivers(model.QuoteId);
                var getCars = uow.QuoteRepository.GetQuoteCars(model.QuoteId);
                model.Drivers = new List<DriverModel>(getDrivers.Select(d => UtilitiesRating.MapXpoToModel(d)));
                model.Vehicles = new List<VehicleModel>(getCars.Select(c => UtilitiesRating.MapXpoToModel(c)));
                //------------------------------
                ModelState.Clear();
                TryValidateModel(model);
                //------------------------------
                var cycle = uow.RateCycleRepository.GetCurrentCycle().RateCycle;
                //------------------------------
                if (!ModelState.IsValid) return View(model);

                QuoteService.CreateQuotePolicy(model, cycle);

                Sessions.Instance.PrevAction = "EDIT";
                return RedirectToAction("Questionnaire", new
                {
                    id = Blowfish.Encrypt_CBC(model.QuoteId)
                }); 
            }
        }

        [HttpGet] // proabably use IPaymentService later on here
        public ActionResult CardConnectPayment(string quoteNo, decimal minimumPayAmt,
            bool isInitialDepostPayment = true, bool isEndorsementPayment = false,
            bool isTitanEndorsementPayment = false,
            DateTime? endorsementEffectiveDate = null, bool isRenewalPayment = false, bool isClientPayment = false,
            decimal AmtToPay = 0, string agentESignEmailAddress = "", bool isPayForReinstatement = false)
        {
            if (!Sessions.Instance.IsAuth)
            {
                if (isTitanEndorsementPayment)
                    return RedirectToAction("Login", "Agent",
                        new
                        {
                            redirectUrl =
                                $@"/Quote/CardConnectPayment?quoteNo={quoteNo}&minimumPayAmt={minimumPayAmt}&isInitialDepostPayment={isInitialDepostPayment}&isEndorsementPayment={isEndorsementPayment}&isTitanEndorsementPayment={isTitanEndorsementPayment}&endorsementEffectiveDate={endorsementEffectiveDate}"
                        });
                return RedirectToAction("Login", "Agent");
            }

            if (isClientPayment && minimumPayAmt <= 0 && AmtToPay <= 0)
                return RedirectToAction("ShowPolicyById", "Client", new {policyNo = quoteNo});

            if (string.IsNullOrEmpty(quoteNo)) throw new ArgumentException("Quote Number must be supplied.");

            if (minimumPayAmt == 0 && isEndorsementPayment)
                return RedirectToAction("IssueEndorsementDetails", "Agent", new
                {
                    policyNo = quoteNo,
                    effDate = endorsementEffectiveDate,
                    diffAmount = 0
                });
            if (!isClientPayment && minimumPayAmt <= AmtToPay)
            {
                if (AmtToPay > 0)
                    minimumPayAmt = AmtToPay;
                else
                    throw new ArgumentException("Invalid Payment amount.");
            }

            if (isClientPayment) minimumPayAmt = Convert.ToDecimal(AmtToPay);
            // IPaymentService later on
            var model = PaymentService.CreateCardConnectPaymentModel(quoteNo, minimumPayAmt, isInitialDepostPayment,
                agentESignEmailAddress, isEndorsementPayment, endorsementEffectiveDate, isTitanEndorsementPayment,
                isRenewalPayment, isClientPayment, isPayForReinstatement);

            return View("CardConnectPayment", model);
        }

        public ActionResult PostSweep(string policyType,
            string policyno,
            string agentcode,
            string amount,
            string ckAcctType,
            string chkAcctNo,
            string chkRoutingNo,
            string payorName,
            bool saveBankInfo,
            bool isInitialDepositPayment = true,
            bool isEndorsementPayment = false,
            bool isTitanEndorsementPayment = false,
            DateTime? endorsementEffectiveDate = null,
            bool isRenewalPayment = false)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    $"PolicyPerformanceMeasurementASMethod. Policy/Quote: {policyno} - StartTime: {DateTime.Now}",
                    "INFO");
                string msg = null;
                string errMsg = null;
                string successMsg = null;
                string continueMsg = null;
                var quoteNumber = policyno;
                var modelAmt = amount.Parse<double>();
                var agentEmail = AgentUtils.GetEmailAddr(agentcode);
                var getAch = uow.PaymentRepository.GetAchTransactions(policyno);
                var getRenAch = uow.PaymentRepository.GetAchTransactions(policyno.GenNextPolicyNo());
                var getRenPayment = uow.PaymentRepository.GetPayments(policyno.GenNextPolicyNo());

                #region Check Make Sure They Are Agent

                if (!Sessions.Instance.IsAgent || string.IsNullOrEmpty(agentcode))
                {
                    msg = "FAILED";
                    errMsg = string.Format(
                        "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                        ErrorUtils.ErrCodeToId(DErrors.LostSession));

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{1}: AGENT LOST SESSION MAKING PAYMENT FOR {0}", policyno, agentcode), "WARN");

                    return JsonErrorResponse(msg, errMsg, "WARN");
                }

                #endregion

                #region Check For Unswept Sweeps and Ren payments

                if (policyType != "RENEWAL" && getAch.Count > 0 && isInitialDepositPayment)
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg,
                        ref continueMsg, ref msg);
                    if (msg.Equals("SUCCESS"))
                        return RedirectToAction("GeneratePolicyDocs", new
                        {
                            id = policyno
                        });
                }

                if (policyType == "RENEWAL" && getRenAch.Count > 0)
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg,
                        ref continueMsg, ref msg);
                    return Json(new {returnMsg = msg, succesMsg = successMsg, continueMsg},
                        JsonRequestBehavior.AllowGet);
                }

                if (policyType == "RENEWAL" && getRenPayment.Count > 0)
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg,
                        ref continueMsg, ref msg);
                    return Json(new {returnMsg = msg, succesMsg = successMsg, continueMsg},
                        JsonRequestBehavior.AllowGet);
                }

                if (policyType == "ENDORSEMENT")
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg,
                        ref continueMsg, ref msg);
                    return Json(new {returnMsg = msg, succesMsg = successMsg, continueMsg},
                        JsonRequestBehavior.AllowGet);
                }

                #endregion

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: SWEEPING AGENT ACCOUNT FOR {1}", policyno, agentcode), "DEBUG");

                #region saveBankInfo

                // if (saveBankInfo)
                // {
                //     var save = new PolicyAltBankInfo(uow);
                //     save.ABA = Blowfish.Encrypt_CBC(chkRoutingNo);
                //     save.Account = "INDIVIDUAL";
                //     save.AccountNumber = Blowfish.Encrypt_CBC(chkAcctNo);
                //     save.AccountType = Blowfish.Encrypt_CBC(ckAcctType);
                //     save.AcctHolderName = payorName;
                //     save.PolicyNo = policyType == "RENEWAL" ? policyno.GenNextPolicyNo() : policyno;
                //     save.Save();
                //     uow.CommitChanges();
                // }

                #endregion

                double amtPrior = 0;
                double renAmt = 0;
                var isRenewal = false; // ISSUE: goes nowhere, probabaly "isRenewalPayment"

                switch (policyType)
                {
                    case "RENEWAL":
                        var getRenQuote = uow.QuoteRepository.GetRenewalQuote(policyno);

                        #region Null RenQuote Error Checking

                        if (getRenQuote == null)
                        {
                            msg = "FAILED";
                            errMsg = string.Format(
                                "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                                ErrorUtils.ErrCodeToId(DErrors.NoRenQuote));

                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: CANNOT FIND RENEWAL QUOTE {0}", policyno, agentcode), "FATAL");

                            return JsonErrorResponse(msg, errMsg, "FATAL");
                        }

                        #endregion

                        //-------------------------------------
                        //Renewal Installments Error Checking
                        var getRenInstalls = uow.PaymentRepository.GetRenewalInstallments(getRenQuote.PolicyNo);

                        #region Null RenQuote Installments Error Checking

                        if (getRenInstalls == null)
                        {
                            msg = "FAILED";
                            errMsg = string.Format(
                                "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                                ErrorUtils.ErrCodeToId(DErrors.NoRenInstalls));

                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: CANNOT FIND RENEWAL QUOTE INSTALLMENTS {0}", policyno, agentcode),
                                "WARN");

                            return JsonErrorResponse(msg, errMsg, "FATAL");
                        }

                        #endregion

                        PaymentService.ProcessRenewalPayment(getRenInstalls, getRenQuote, amtPrior, renAmt, modelAmt,
                            policyno, agentcode, saveBankInfo, amount, policyType);

                        //isRenewal = true; // ISSUE: goes nowhere, probabaly "isRenewalPayment"
                        break;
                    default:
                        AgentUtils.SweepAcct(agentcode, policyno, amount.Parse<double>(), Sessions.Instance.Username,
                            saveBankInfo);
                        var paytype = policyType == "REINSTATEMENT" ? PymntTypes.Reinstatement : PymntTypes.Renewal;
                        var guid = Guid.NewGuid().ToString();

                        PortalUtils.AuditPayments(Sessions.Instance.Username,
                            PymntLocation.AgentSweep, // ISSUE: PymntLocation misspell
                            paytype,
                            modelAmt,
                            0,
                            0,
                            modelAmt,
                            guid,
                            false,
                            true,
                            false);

                        break;
                }

                PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg,
                    ref continueMsg, ref msg);

				var _storedProcedures = new StoredProcedures(new StoredProcedureComponentsSql());
				_storedProcedures.InstallmentPaymentApply(policyno, DateTime.Now, "SYSTEM",Guid.NewGuid().ToString());

                LogUtils.Log(Sessions.Instance.Username,
                    $"PolicyPerformanceMeasurementASMethod. Policy/Quote: {policyno} - EndTime: {DateTime.Now}",
                    "INFO");
                if (isTitanEndorsementPayment || isRenewalPayment ||
                    !isInitialDepositPayment && !isEndorsementPayment && !(policyType == "REINSTATEMENT"))
                {
                    if (isTitanEndorsementPayment)
                    {
                        var endorsementQuoteForPolicy = uow.QuoteRepository.GetEndorsementQuote(policyno);
                        if (endorsementQuoteForPolicy != null)
                        {
                            endorsementQuoteForPolicy.IsPremiumPaymentPaid = true;
                            endorsementQuoteForPolicy.Save();
                            uow.Save();
                        }

                        //Notify Endorsements team in UW
                        var endorsementsEmail = "endorsements@palminsure.com";
                        EmailService.SendEmailNotification(new List<string> {endorsementsEmail},
                            $"Endorsement Additional Premium Payment Completed. Policy: {policyno}, Agent: {Sessions.Instance.AgencyName}",
                            $"<div>An additional premium payment was completed</div><br /><div>PolicyNo: {policyno}</div><div>Agent: {Sessions.Instance.AgencyName}</div><div>Amount: {modelAmt.ToString("C")}</div>",
                            true);
                    }

                    var paymentReceiptFilePath =
                        PalmsClient.GeneratePaymentReceipt(policyno, true);
                    var fileName = Path.GetFileName(paymentReceiptFilePath);
                    var fileLink = string.Format(ConfigSettings.ReadSetting("PolicyDocsBaseUrl") + @"/Files/{0}",
                        fileName);
                    return RedirectToAction("PaymentReceipt", "Quote",
                        new {policyNo = policyno, paymentReceiptUrl = fileLink});
                }

                if (isEndorsementPayment)
                    return RedirectToAction("IssueEndorsementDetails", "Agent",
                        new {policyNo = policyno, effDate = endorsementEffectiveDate, diffAmount = amount});

                if (policyType == "REINSTATEMENT")
                {
                    var paymentREIReceiptFilePath = PalmsClient.GeneratePaymentReceipt(policyno, true);
                    return RedirectToAction("IssueReinstatementDetails", "Agent",
                        new
                        {
                            policyNo = policyno,
                            paidAmt = modelAmt,
                            payrecieptLink = paymentREIReceiptFilePath,
                            isAgentSweep = true
                        });
                }

                LogUtils.Log(Sessions.Instance.Username,
                    $"PolicyGenerationMeasurementAS, Policy#: {Sessions.Instance.PolicyNo}. EndTime: {DateTime.Now}",
                    "INFO");
                return RedirectToAction("GeneratePolicyDocs", new
                {
                    id = policyno
                });
            }
        }

        private JsonResult JsonErrorResponse(string msg, string errMsg, string errorMessageLevel)
        {
            return Json(new {returnMsg = msg, errorMsg = errMsg, errMsgLevel = errorMessageLevel},
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CardConnectPayment(CardConnectPaymentModel model)
        {
            if (model == null) throw new InvalidOperationException("CardConnectPaymentModel cannot be null.");
            bool isEft;
            bool isQuoteDepositPayment = true;
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {

                var quote = uow.QuoteRepository.GetQuote(model.PolicyNo);
                if (quote != null)
                {
                    isEft = quote.EFT;
                }
                else
                {
                    var policy = uow.PolicyRepository.GetPolicy(model.PolicyNo);
                    if (policy == null)
                    {
                        throw new InvalidOperationException($"No matching policy or quote was found for {model.PolicyNo}");
                    }
                    isEft = policy.EFT;
                    isQuoteDepositPayment = false;
                }
            }

            if (model.IsClientPayment)
            {
                if (!Sessions.Instance.IsClient)
                {
                    return RedirectToAction("Login", "Client");
                }
                if (model.Amount <= 0)
                {
                    return RedirectToAction("ShowPolicyById", "Client", new { model.PolicyNo });
                }
            }
            else
            {
                if (string.IsNullOrEmpty(Sessions.Instance.AgentCode))
                {
                    return RedirectToAction("Login", "Agent");
                }
            }

            if (!PaymentService.AgentHasAccess(model))
            {
                Sessions.Instance.RatingErrMsg = $@"<div class='error'>{Sessions.Instance.AgentName} does not have access to this policy.</div>";
                return RedirectToAction("Search", "Agent");
            }

            if (PaymentService.HasExistingPayment(model)) return View(model);

            if (model.IsInitialDepostPayment && PaymentService.HasPolicyViolations(model.PolicyNo))
            {
                return RedirectToAction("Search", "Agent");
            }

            if (model.IsCreditCard && !PaymentService.ValidateCreditCardInfo(model))
            {
                return View(model);
            }

            if (model.IsAch && !PaymentService.ValidateACHInformation(model))
            {
                return View(model);
            }

            if (model.IsAgentAccountSweep)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    $"PolicyGenerationMeasurementAS, Policy#: {model.PolicyNo}. StartTime: {DateTime.Now}", "INFO");
                if (model.IsEft && model.IsInitialDepostPayment)
                {
                    if (!PaymentService.ValidateAgentSweepPaymentInfo(model)) return View(model);

                    var existingProfile = PaymentService.GetPolicyPaymentProfiles(model.PolicyNo);
                    if (existingProfile == null) PaymentService.CreatePaymentProfile(model, model.PolicyNo);

                    return RedirectToAction("PostSweep", new
                    {
                        policyType = "QUOTE",
                        policyno = model.PolicyNo,
                        amount = Math.Round(model.Amount, 2),
                        saveBankInfo = false,
                        agentCode = Sessions.Instance.AgentCode
                    });
                }

                var type = "QUOTE";
                if (model.IsRenewalPayment) type = "RENEWAL";

                if (model.IsPayForReinstatement) type = "REINSTATEMENT";

                return RedirectToAction("PostSweep", new
                {
                    policyType = type,
                    policyno = model.PolicyNo,
                    amount = Math.Round(model.Amount, 2),
                    saveBankInfo = false,
                    agentCode = Sessions.Instance.AgentCode,
                    isInitialDepositPayment = model.IsInitialDepostPayment,
                    isEndorsementPayment = model.IsEndorsementPayment,
                    isTitanEndorsementPayment = model.IsTitanEndorsementPayment,
                    endorsementEffectiveDate = model.EndorsementEffectiveDate,
                    isRenewalPayment = model.IsRenewalPayment
                });
            }
            
            LogUtils.Log(Sessions.Instance.Username, $"PolicyGenerationMeasurementCC, Policy#: {Sessions.Instance.PolicyNo}. StartTime: {DateTime.Now}", "INFO");
            


            var result = PaymentService.ProcessPayment(model, model.IsInitialDepostPayment && isEft);
            if (result.Success)
            {
                if (isQuoteDepositPayment)
                {

                    LogUtils.Log(Sessions.Instance.Username, $"PolicyGenerationMeasurementCC, Policy#: {Sessions.Instance.PolicyNo}. EndTime: {DateTime.Now}", "INFO");
                    return RedirectToAction("GeneratePolicyDocs", new
                    {
                        id = model.PolicyNo
                    });
                }

                if (model.PaymentType == PaymentTypes.EndorsementPayment)
                {
                    return RedirectToAction("IssueEndorsementDetails", "Agent",
                        new
                        {
                            model.PolicyNo,
                            effDate = model.EndorsementEffectiveDate,
                            diffAmount = model.Amount.ToString()
                        });
                }

                if (model.PaymentType == PaymentTypes.TitanEndorsementPayment)
                {
                    using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                    {
                        uow.EndorsementRepository.MarkEndorsementAsPaid(model.PolicyNo);
                        uow.Save();
                    }
                    

                    //Notify Endorsements team in UW
                    var endorsementsEmail = "endorsements@palminsure.com";
                    if (!WebHostService.IsProduction()) endorsementsEmail = "yogeshk@agsft.com";

                    EmailService.SendEmailNotification(new List<string> {endorsementsEmail},
                        $"Endorsement Additional Premium Payment Completed. Policy: {model.PolicyNo}, Agent: {Sessions.Instance.AgencyName}",
                        $"<div>An additional premium payment was completed</div><br /><div>PolicyNo: {model.PolicyNo}</div><div>Agent: {Sessions.Instance.AgencyName}</div><div>Amount: {model.Amount.ToString("C")}</div>",
                        true);
                }

                if (model.PaymentType == PaymentTypes.ReinstatementPayment)
                {
                    return RedirectToAction("IssueReinstatementDetails", "Agent",
                        new
                        {
                            model.PolicyNo,
                            paidAmt = Math.Round(Convert.ToDouble(result.Amount), 2),
                            payrecieptLink = result.ReceiptLink
                        });
                }

                //process all these types as installment
                if (model.PaymentType == PaymentTypes.InstallmentPayment 
                    || model.PaymentType == PaymentTypes.TitanEndorsementPayment 
                    || model.PaymentType == PaymentTypes.RenewalPayment
                    || model.PaymentType == PaymentTypes.InitialDepositPayment)
                {
                    using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                    {
                        uow.PolicyRepository.ApplyPaymentsToInstallments(model.PolicyNo);
                        uow.Save();
                    }
                    return RedirectToAction("PaymentReceipt", "Quote",
                        new { model.PolicyNo, paymentReceiptUrl = result.ReceiptLink, isClientReceipt = model.IsClientPayment });
                }
            }
            //payment failed
            ModelState.AddModelError("DeclinedPayment", result.FailureReason);
            return View("CardConnectPayment", model);
        }

        public ActionResult PaymentReceipt(string policyNo, string paymentReceiptUrl, bool isClientReceipt = false)
        {
            var model = new PaymentReceiptModel
            {
                PolicyNo = policyNo,
                Link = paymentReceiptUrl,
                IsClientReceipt = isClientReceipt
            };
            return View(model);
        }

        public ActionResult ReinstatementPayment(string policyNo, string paymentReceiptUrl, string payREINoticeUrl,
            string pdfConcatFileLink, bool isAgentSweep = false)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("ReinstatementPayment :{0} : {1} : {2} : {3}", policyNo, paymentReceiptUrl,
                    payREINoticeUrl, pdfConcatFileLink), "INFO");
            var model = new ReinstatementModel
            {
                PolicyNo = policyNo,
                PayLink = paymentReceiptUrl,
                ReinstatementLink = payREINoticeUrl,
                ConcatPdfFileLink = pdfConcatFileLink,
                IsAgentSweep = isAgentSweep
            };
            return View(model);
        }

        public ActionResult Questionnaire(string id)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.AgentCode))
                return RedirectToAction("Login", "Agent", new {id = "S404"});

            if (AgentUtils.GetScalar<bool>("IsRequireCall", Sessions.Instance.AgentCode))
                return RedirectToAction("PleaseCall", "Agent");

            id = Blowfish.Decrypt_CBC(id);

            var model = QuoteService.CreateQuestionnaireModel(id, Blowfish);

            return View(model);
        }

        [HttpPost]
        public ActionResult Questionnaire(QuestionnaireNewModel model)
        {
            if (string.IsNullOrEmpty(model.QuoteId)) return RedirectToAction("Login", "Agent", new {id = "Q404"});

            model.QuoteId = Blowfish.Decrypt_CBC(model.QuoteId);
            if (!ModelState.IsValid) return View(model);

            //-------------------
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("POSTING ANSWERS FOR QUESTIONNAIRE FOR QUOTE {0}", Sessions.Instance.PolicyNo), "INFO");

            QuoteService.CreatePolicyQuoteQuestionnaire(model, Sessions.Instance.PolicyNo); // in Policy Service later

            var vm = new QuoteIntegrationsViewModel();
            vm.QuoteNo = model.QuoteId;
            TempData["RiskCheckDriversViolations"] = null;
            TempData["MvrDriversViolations"] = null;
            TempData["AplusViolations"] = null;
            TempData["CVModels"] = null;
            return RedirectToAction("QuoteIntegrations", vm);
        }

        public ActionResult RateAndShow(string id)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            string policyno = null;
            try
            {
                policyno = Blowfish.Decrypt_CBC(id);
            }
            catch
            {
                policyno = id;
            }

            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: VIEWING RATING PAGE FOR QUOTE", Sessions.Instance.PolicyNo), "INFO");
            if (TempData["QuoteModel"] != null)
                TempData["QuoteModel"] = TempData["QuoteModel"] as QuoteModel;

            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var quoteChk = uow.QuoteRepository.GetQuote(Sessions.Instance.PolicyNo);
                if (quoteChk == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("Quote Not Found: {0}".ToUpper(), Sessions.Instance.PolicyNo), "FATAL");
                    throw new HttpException(404, "Quote not found");
                }
                var quotePrevCompany = quoteChk.PreviousCompany;
                var quotePrevExpDate = quoteChk.PreviousExpDate;
                if (QuoteService.QuestionnaireYesAnswers(quoteChk.PolicyNo, quoteChk.AppQuestionAnswers))
                    return RedirectToAction("Search", "Agent");

                #region Has2PlusPIPClaims

                /*     if (Has2PlusPIPClaims(quoteChk))
                     {
                         Sessions.Instance.RatingErrMsg = @"<div class='error'>Quote " + quoteChk.PolicyNo + " is invalid due to 2 or more PIP claims in past 3 years.</div>";
                         return RedirectToAction("Search", "Agent");
                     }

                     if (HasAnyDriverResideLessThanTenMonths(quoteChk))
                     {
                         Sessions.Instance.RatingErrMsg = @"<div class='error'>Quote " + quoteChk.PolicyNo + " is invalid due to driver residing in Florida less than 10 months.</div>";
                         return RedirectToAction("Search", "Agent");
                     }
                     */

                #endregion

                var isNewBusinessAllowed = false; //Not currently used, but could be based on business ask.
                uow.PolicyRepository.GetPolicyScore(policyno, Sessions.Instance.Username, isNewBusinessAllowed);

                var listPayPlans = new List<XpoAARatePayPlans>();
                var listInstallments = new List<RatingInstallmentsDO>();
                var isAtRiskTerritory = false;
                var newPayPlans = PaymentService.CreatePayPlan(quoteChk, listPayPlans, out isAtRiskTerritory);

                Sessions.Instance.RatingGuid = Guid.NewGuid().ToString();
                uow.PolicyRepository.RatePolicy(Sessions.Instance.Username,Sessions.Instance.RatingGuid,Sessions.Instance.PolicyNo);


                if (!string.IsNullOrEmpty(quotePrevCompany))
                    UtilitiesRating.SavePrevCompanyAndExpDateInQuote(Sessions.Instance.PolicyNo, quotePrevCompany,
                        quotePrevExpDate);
                DataSet data;
                var polQuote = PaymentService.RateDriver(listInstallments, out data);

                var quoteModel = UtilitiesRating.MapXpoToModel(polQuote);
                var drivers = uow.QuoteRepository.GetQuoteDrivers(polQuote.PolicyNo);
                var violations = uow.QuoteRepository.GetViolations(polQuote.PolicyNo);

                quoteModel.Drivers = new List<DriverModel>();
                foreach (var driver in drivers) quoteModel.Drivers.Add(UtilitiesRating.MapXpoToModel(driver));

                var ratingReturnCode = polQuote.RatingReturnCode;
                if (ratingReturnCode == "INVALID AGENT")
                {
                    Sessions.Instance.RatingErrMsg =
                        @"<div class='error'>You are unable to write business at this time, if you have any questions please contact underwriting@palminsure.com</div>";
                    return RedirectToAction("Search", "Agent");
                }

                if (violations.Any())
                {
                    if (!violations.Any(v => v.FromMVR) || !violations.Any(v => v.FromAPlus))
                    {
                        Sessions.Instance.RatingErrMsg =
                            @"<div class='error'>Not all underwriting reports were found and this policy cannot be bound. Please contact underwriting@palminsure.com with any questions.</div>";
                        return RedirectToAction("Search", "Agent");
                    }
                }
                else
                {
                    Sessions.Instance.RatingErrMsg =
                        @"<div class='error'>Not all underwriting reports were found and this policy cannot be bound. Please contact underwriting@palminsure.com with any questions.</div>";
                    return RedirectToAction("Search", "Agent");
                }

                if (isAtRiskTerritory)
                {
                    var pipClaimsForQuote = uow.QuoteRepository.GetPipClaims(polQuote.PolicyNo);
                    if (pipClaimsForQuote.Any())
                    {
                        Sessions.Instance.RatingErrMsg =
                            $@"<div class='error'>Quote #: {polQuote.PolicyNo} - No prior PIP claims are allowed in this zip code. If you have any questions please contact underwriting.</div>";
                        return RedirectToAction("Search", "Agent");
                    }
                }

                PalmsClient.CloseRef();


                var phyDamImages = uow.PolicyRepository.GetDamageImages(policyno);
                quoteModel.TotalVehiclesWithPhysicalDamage = uow.PolicyRepository.GetDamagedVehicleCount(policyno);
                quoteModel.PhysicalDamageImages = phyDamImages.ToList();
                quoteModel.TotalPhysicalDamageImageUploads = phyDamImages.Count;
                var mvrsViewModel = new OrderMvrsViewModel();
                mvrsViewModel.Drivers = quoteModel.Drivers;
                ViewBag.UploadImagesDeepLinkSent =
                    !string.IsNullOrEmpty(IntegrationsUtility.GetDeepLink(policyno, true));
                return View("ShowRatedQuote", Tuple.Create(quoteModel, data, newPayPlans, listInstallments));
            }
        }

        #endregion

        #region Drivers

        [HttpPost]
        public ActionResult DriverAdd(DriverModel model)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            if (ModelState.IsValid)
            {
                //Sessions.Instance.PolicyNo = model.QuoteId;
                var quoteModel = TempData["QuoteModel"] as QuoteModel;
                if (model.IsEdit)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: EDITING DRIVER {1}", Sessions.Instance.PolicyNo, model.DriverIndex),
                        "INFO");
                    //PBI 881: to enable the integration call if the changes done while editing a Driver.
                    if (model.IsExcluded != quoteModel.Drivers[model.DriverIndex - 1].IsExcluded ||
                        model.DriversLicense != quoteModel.Drivers[model.DriverIndex - 1].DriversLicense
                        || model.FirstName != quoteModel.Drivers[model.DriverIndex - 1].FirstName ||
                        model.LastName != quoteModel.Drivers[model.DriverIndex - 1].LastName)
                        IntegrationsUtility.UpdateFlagForAllIntegrationCall(Sessions.Instance.PolicyNo, false, true);

                    using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                    {
                        var getDrivers =
                            uow.QuoteRepository.GetQuoteDriversByIndex(Sessions.Instance.PolicyNo, model.DriverIndex);

                        var driver = getDrivers.FirstOrDefault();
                        getDrivers.Remove(driver);
                        driver.PolicyNo = Sessions.Instance.PolicyNo;

                        UtilitiesRating.MapModelToXpo(model, driver);
                        driver.Points =
                            QuoteService.CalcDriverPoints(Sessions.Instance.PolicyNo, model.DriverIndex); //LF 11/5/18
                        uow.QuoteRepository.SaveDriver(driver);
                        uow.Save();

                        if (model.OldDlNum != model.DriversLicense)
                        {
                            var updateDriver = uow.QuoteRepository.GetQuoteDrivers(Sessions.Instance.PolicyNo);

                            foreach (var driver1 in updateDriver)
                            {
                                var Update = uow.QuoteRepository.GetQuoteDriverByOID(driver1.OID.ToString());
                                Update.DriverMsg = null;
                                uow.QuoteRepository.SaveDriver(Update);
                                uow.Save();
                            }
                        }

                        getDrivers.Add(driver);
                        quoteModel.Drivers.Add(model);
                        TempData["QuoteModel"] = quoteModel;
                        TempData["DriverModel"] = model;
                        TempData["DriverID"] = ""; //LF 11/2/18

                        return RedirectToAction("Edit", "Quote",
                            new {id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)});
                    }
                }

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: NEW QUOTE", Sessions.Instance.PolicyNo), "INFO");

                using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                {
                    var getDrivers = uow.QuoteRepository.GetQuoteDrivers(Sessions.Instance.PolicyNo);

                    if (getDrivers.Count > 10)
                    {
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: DRIVERS COUNT > 10", Sessions.Instance.PolicyNo), "INFO");
                        throw new HttpException(400, "One policy may only have 10 driver maximum");
                    }

                    //--- LF  10/1/18   11/5 moved above driver.Save to get points and save with driver
                    var newDriverIndex = getDrivers.Count + 1;
                    var dpoints = 0;
                    //TODO
                    //getViolations for Sessions.Instance.PolicyNo and driver.DriverIndex==0
                    //set Violation.DriverIndex = driver.DriverIndex
                    //Save
                    //commit
                    //change quoteModel.Drivers.Violations[all] to driver.DriverIndex - don't need to do this, code gets them from table again
                    //----------

                    var violationIndex = 0;
                    foreach (var violationModel in model.NewDriverViolations)
                        QuoteService.UpdateDriverViolations(violationModel.Date, violationModel.Description, newDriverIndex, ++violationIndex);

                    var driver = new PolicyQuoteDrivers();
                    driver.DriverIndex = newDriverIndex; // getDrivers.Count + 1;
                    driver.PolicyNo = Sessions.Instance.PolicyNo;
                    UtilitiesRating.MapModelToXpo(model, driver);
                    driver.Points = dpoints;
                    uow.QuoteRepository.SaveDriver(driver);
                    uow.Save();
                    getDrivers.Add(driver);
                    quoteModel.Drivers.Add(model);


                    //881:
                    IntegrationsUtility.UpdateFlagForAllIntegrationCall(Sessions.Instance.PolicyNo, false, true);
                    //===== 881

                    TempData["QuoteModel"] = quoteModel;
                    TempData["DriverModel"] = model;
                    TempData["DriverID"] = ""; //LF 11/2/18
                    TempData["DriverCount"] = quoteModel.Drivers.Count; //LF 11/2/18
                    //return PartialView("~/Views/Quote/DisplayTemplates/Drivers.cshtml", getDrivers.Select(d => UtilitiesRating.MapXpoToModel(d)).ToList());
                    return RedirectToAction("Edit", "Quote",
                        new {id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)});
                }
            }

            ModelState.AddModelError("InvalidModel", "Invalid model state.");
            return View("AddDriver", model);
        }

        /// <summary>
        ///     DriverLoad
        /// </summary>
        /// <param name="id">Id is Driver index id</param>
        /// <returns>DriverModel</returns>
        public ActionResult DriverLoad(string id)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var quoteModel = TempData["QuoteModel"] as QuoteModel;
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: LOADING DRIVER DATA", Sessions.Instance.PolicyNo), "INFO");
                var driver = uow.QuoteRepository.GetQuoteDriverByOID(id);

                var model = QuoteService.AddDriverViolations(driver);

                var tmpModel = quoteModel.Drivers.Find(v => v.DriverId == id);
                quoteModel.Drivers.Remove(tmpModel);
                quoteModel.Drivers.Add(model);
                TempData["QuoteModel"] = quoteModel;
                TempData["DriverID"] = id;
                TempData["DriverIndex"] = model.DriverIndex;
                return RedirectToAction("AddDriver", model);
            }
        }

        /// <summary>
        ///     add new driver
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddDriver(string id, string tmpfName = "", string tmplName = "", string tmpMI = "",
            string tmpfName2 = "", string tmplName2 = "", string tmpMI2 = "")
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var quoteModel = TempData["QuoteModel"] as QuoteModel;
            var driverID = TempData["DriverID"] as string;

            DriverModel driverModel;
            if (string.IsNullOrEmpty(driverID))
            {
                var newDriverIndex = 0;
                using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                {
                    newDriverIndex = uow.QuoteRepository.GetQuoteDrivers(Sessions.Instance.PolicyNo).Count + 1;
                }

                driverModel = new DriverModel();
                //quoteModel.Drivers.Add(driverModel);
                if (newDriverIndex == 1)
                {
                    driverModel.FirstName = (tmpfName ?? string.Empty).ToUpper();
                    driverModel.LastName = (tmplName ?? string.Empty).ToUpper();
                    driverModel.MiddleInitial = (tmpMI ?? string.Empty).ToUpper();
                }
                else if (newDriverIndex == 2)
                {
                    driverModel.FirstName = (tmpfName2 ?? string.Empty).ToUpper();
                    driverModel.LastName = (tmplName2 ?? string.Empty).ToUpper();
                    driverModel.MiddleInitial = (tmpMI2 ?? string.Empty).ToUpper();
                }

                driverModel.DriverIndex = quoteModel.Drivers.Count;
                //LF 12/6/18  Delete any/all violations with Sessions.Instance.PolicyNo and driverModel.DriverIndex (since just created)
                DbHelperUtils.DeleteRecord("PolicyQuoteDriversViolations", "PolicyNo", "DriverIndex",
                    Sessions.Instance.PolicyNo, driverModel.DriverIndex.ToString());

                TempData.Remove("DriverIndex");
            }
            else
            {
                driverModel = quoteModel.Drivers.Find(v => v.DriverId == driverID);
            }

            TempData["QuoteModel"] = quoteModel;
            TempData["DriverModel"] = driverModel;
            TempData["DriverID"] = driverID;
            TempData["DriverCount"] = quoteModel.Drivers.Count; //LF
            return View("AddDriver", driverModel);
        }

        public ActionResult DriverDelete(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getDrivers = QuoteService.DeleteDriverRecords(uow, id);

                TempData["QuoteModel"] = TempData["QuoteModel"];
                TempData["DriverCount"] = getDrivers.Count; //LF
                return PartialView("~/Views/Quote/DisplayTemplates/Drivers.cshtml",
                    getDrivers.Select(drivers => UtilitiesRating.MapXpoToModel(drivers)).ToList());
            }
        }

        #endregion

        #region Violations

        [HttpPost]
        public ActionResult ViolationAdd(ViolationAddModel model)
        {
            if (Request.IsAjaxRequest())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: ADD VIOLATIONS", Sessions.Instance.PolicyNo), "INFO");
                using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                {
                    var driverIndex = Convert.ToInt32(TempData["DriverIndex"]);
                    var violationCount = uow.QuoteRepository.GetDriverViolationsCount(Sessions.Instance.PolicyNo, driverIndex);

                    QuoteService.UpdateDriverViolations(model.Date, model.Description, driverIndex, violationCount + 1);

                    uow.Save();
                    var driverViolations = uow.QuoteRepository.GetViolationsByDriver(Sessions.Instance.PolicyNo, driverIndex);
                    ////////////////////
                    var listViolations = new List<ViolationModel>();
                    var quoteModel = TempData["QuoteModel"] as QuoteModel;
                    foreach (var driverViolation in driverViolations)
                    {
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("{0}: LOAD VIOLATIONS FROM DRIVER TO GRID FOR DISPLAY",
                                Sessions.Instance.PolicyNo), "INFO");
                        var vModel = new ViolationModel();
                        vModel.ViolationId = driverViolation.OID;
                        vModel.Description = driverViolation.ViolationDesc;
                        vModel.Date = driverViolation.ViolationDate;
                        vModel.Points = driverViolation.ViolationPoints;
                        vModel.DriverIndex = driverViolation.DriverIndex;
                        listViolations.Add(vModel);
                    }

                    if (quoteModel?.Drivers?.Any(x => x.DriverIndex == driverIndex) == true)
                    {
                        var driverModel = quoteModel.Drivers.Find(x => x.DriverIndex == driverIndex);
                        driverModel.Violations = listViolations;
                    }

                    TempData["QuoteModel"] = quoteModel;
                    TempData["DriverModel"] = TempData["DriverModel"];
                    TempData["DriverID"] = TempData["DriverID"];
                    TempData["DriverIndex"] = driverIndex; //LF 12/5/18
                    return PartialView("~/Views/Quote/DisplayTemplates/Violations.cshtml", listViolations);
                }
            }

            throw new NotImplementedException();
        }

        // id = Violation.OID
        public ActionResult ViolationDelete(string id)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var violations = uow.QuoteRepository.GetViolations(Sessions.Instance.PolicyNo);
                var violation = violations.FirstOrDefault(v => v.OID.ToString().Equals(id));
                if (violation == null) throw new InvalidOperationException($"Violation not found with OID: {id}");

                var driverIndex = violation.DriverIndex;
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: DELETE VIOLATION FOR DRIVER {1}", Sessions.Instance.PolicyNo, driverIndex),
                    "WARN");

                var deletedIndex = violation.ViolationIndex;
                IList<PolicyQuoteDriversViolations> violationsToReIndex =
                    violations.Where(v => v.ViolationIndex > deletedIndex).ToList();
                foreach (var violationToReIndex in violationsToReIndex)
                {
                    violationToReIndex.ViolationIndex -= 1;
                    violationToReIndex.ViolationNo -= 1;
                    violationToReIndex.Save();
                }

                violation.Delete();
                uow.Save();
                DbHelperUtils.DeleteRecord("PolicyQuoteDriversViolations", "OID", id);

                var listViolations = new List<ViolationModel>();
                foreach (var driverViolation in violations)
                {
                    var vModel = new ViolationModel();
                    vModel.ViolationId = driverViolation.OID;
                    vModel.Description = driverViolation.ViolationDesc;
                    vModel.Date = driverViolation.ViolationDate;
                    vModel.Points = driverViolation.ViolationPoints;
                    vModel.FromMvr = driverViolation.FromMVR;
                    vModel.FromAPlus = driverViolation.FromAPlus;
                    vModel.FromCV = driverViolation.FromCV;
                    listViolations.Add(vModel);
                }

                var quoteModel = TempData["QuoteModel"] as QuoteModel;
                if (quoteModel?.Drivers?.Any(x => x.DriverIndex == driverIndex) == true)
                    quoteModel.Drivers.Find(x => x.DriverIndex == driverIndex).Violations = listViolations;

                TempData["QuoteModel"] = quoteModel;
                return PartialView("~/Views/Quote/DisplayTemplates/Violations.cshtml", listViolations);
            }
        }

        #endregion

        #region Vehicles

        [HttpGet]
        public ActionResult AddVehicle()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var quoteModel = TempData["QuoteModel"] as QuoteModel;
            if (quoteModel == null) return RedirectToAction("Search", "Agent");

            var vehicleID = TempData["VehicleID"] as string;
            VehicleModel vehicleModel;
            if (string.IsNullOrEmpty(vehicleID))
            {
                vehicleModel = new VehicleModel();
                quoteModel.Vehicles.RemoveAll(x => x.VehicleId == "0" || string.IsNullOrEmpty(x.VehicleId));
                vehicleModel.CarIndex = quoteModel.Vehicles.Count + 1;
                quoteModel.Vehicles.Add(vehicleModel);
            }
            else
            {
                vehicleModel = quoteModel?.Vehicles?.Find(v => v.VehicleId == vehicleID);
                if (vehicleModel == null) return RedirectToAction("Search", "Agent");
                vehicleModel?.LienHolders?.Clear();
                using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                {
                    if (!string.IsNullOrEmpty(quoteModel.QuoteId) && vehicleModel.CarIndex > 0)
                    {
                        var lienHolders =
                            uow.PolicyRepository.GetLienHoldersForCar(quoteModel.QuoteId, vehicleModel.CarIndex);
                        foreach (var lienHolder in lienHolders)
                            vehicleModel.LienHolders.Add(UtilitiesRating.MapXpoToModel(lienHolder));
                    }
                }
            }

            ViewBag.CompDed = quoteModel.CompDed;
            ViewBag.CollDed = quoteModel.CollDed;

            TempData["QuoteModel"] = quoteModel;
            TempData["VehicleModel"] = vehicleModel;
            TempData["VehicleID"] = vehicleID;
            TempData["CarIndex"] = vehicleModel.CarIndex;

            return View("AddVehicle", vehicleModel);
        }

        [HttpGet]
        public ActionResult ResetVehicle()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var quoteModel = TempData["QuoteModel"] as QuoteModel;
            var carindex = Convert.ToInt32(TempData["CarIndex"]);

            var vehicleModel = QuoteService.FindVehicle(quoteModel, carindex);

            ViewBag.CompDed = quoteModel.CompDed;
            ViewBag.CollDed = quoteModel.CollDed;

            TempData["QuoteModel"] = quoteModel;
            TempData["VehicleModel"] = vehicleModel;
            TempData["VehicleID"] = TempData["VehicleID"];
            TempData["CarIndex"] = vehicleModel.CarIndex;
            ;

            return View("AddVehicle", vehicleModel);
        }

        [HttpPost]
        public ActionResult VehicleAdd(VehicleModel model, FormCollection formCollection)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");
            var isModelValid = true;
            if (model.HasPhyDam)
                if (model.ComprehensiveDeductible == "NONE" || model.ComprehensiveDeductible == "0" ||
                    model.ComprehensiveDeductible == "")
                {
                    ModelState.AddModelError("Deductible", "Comp/Coll deductibles are required with physical damage.");
                    isModelValid = false;
                }

            if (isModelValid && !string.IsNullOrEmpty(model.VIN))
            {
                var vehicleByVIN = true;
                var vehicleInfo = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, model.VIN)?.Vehicle;
                if (vehicleInfo == null && model.VIN.Length < 17)
                {
                    vehicleInfo = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, null, model.Year.ToString(),
                        model.Make, model.VehModel, model.Style)?.Vehicle;
                    vehicleByVIN = false;
                }

                if (vehicleInfo == null)
                {
                    ModelState.AddModelError("VIN",
                        "This vehicle's VIN is not in our records, can not add this vehicle.");
                    isModelValid = false;
                }
                else
                {
                    if (!vehicleByVIN)
                        if (model.VIN != vehicleInfo.VIN)
                        {
                            ModelState.AddModelError("VIN",
                                "Invalid VIN or VIN's vehicle info not matching with Year, Make, Model and Style. Can not add this vehicle.");
                            isModelValid = false;
                        }
                }
            }

            if (!isModelValid)
                return View("AddVehicle", model);

            if (TempData["QuoteModel"] != null) TempData["QuoteModel"] = TempData["QuoteModel"];
            if (TempData["VehicleID"] != null) TempData["VehicleID"] = TempData["VehicleID"];
            //Check for symbols
            if (!model.HasAllSymbols() && !string.IsNullOrEmpty(model.VIN))
                try
                {
                    var vehicleInfo = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, model.VIN);
                    if (vehicleInfo != null)
                    {
                        model.PDSymbol = vehicleInfo.Liability?.RiskAnalyzerPropertyDamageRatingSymbol;
                        model.PIPSymbol = vehicleInfo.Liability?.RiskAnalyzerPersonalInjuryProtectionRatingSymbol;
                        model.BISymbol = vehicleInfo.Liability?.RiskAnalyzerBodilyInjuryRatingSymbol;
                        model.MPSymbol = vehicleInfo.Liability?.RiskAnalyzerMedicalPaymentsRatingSymbol;
                        if (!string.IsNullOrEmpty(
                            vehicleInfo.PhysicalDamage?.RiskAnalyzerCollisionRatingSymbol2010Prior))
                            model.CollSymbol = vehicleInfo.PhysicalDamage?.RiskAnalyzerCollisionRatingSymbol2010Prior;
                        else
                            model.CollSymbol = vehicleInfo.PhysicalDamage
                                ?.RiskAnalyzerCollisionRatingSymbol2011Subsequent;
                        if (!string.IsNullOrEmpty(vehicleInfo.PhysicalDamage
                            ?.RiskAnalyzerComprehensiveRatingSymbol2010Prior))
                            model.CompSymbol = vehicleInfo.PhysicalDamage
                                ?.RiskAnalyzerComprehensiveRatingSymbol2010Prior;
                        else
                            model.CompSymbol = vehicleInfo.PhysicalDamage
                                ?.RiskAnalyzerComprehensiveRatingSymbol2011Subsequent;
                    }
                    else
                    {
                        ModelState.AddModelError("Symbols",
                            "Unable to order symbols for VIN. Please contact underwriting for assistance.");
                        return View("AddVehicle", model);
                    }
                }
                catch (Exception e)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("Error occurred ordering symbols for Policy: {0}, VIN: {1} - Error: {2}",
                            Sessions.Instance.PolicyNo, model.VIN, e.Message),
                        "INFO");
                    ModelState.AddModelError("Symbols",
                        "Unable to order symbols for VIN. Please contact underwriting for assistance.");
                    return View("AddVehicle", model);
                }

            var quoteModel = TempData["QuoteModel"] as QuoteModel;

            #region TEST if (model.HasPhyDam)

            //if (model.HasPhyDam)
            //{
            //    using (var uow = XpoHelper.GetNewUnitOfWork())
            //    {
            //        string vcompded = quoteModel.CompDed;
            //        if (model.ComprehensiveDeductible != vcompded)
            //        {
            //            XPCollection<PolicyQuoteCars> currCars = new XPCollection<PolicyQuoteCars>(uow,
            //                CriteriaOperator.Parse("PolicyNo = ? AND HasPhyDam = 1", Sessions.Instance.PolicyNo));
            //            if (currCars.Count > 0)
            //            {
            //                foreach (PolicyQuoteCars car in currCars)
            //                {
            //                    if (car.HasPhyDam && car.CompDed != model.ComprehensiveDeductible &&
            //                        car.CarIndex != model.CarIndex)
            //                    {
            //                        car.CompDed = model.ComprehensiveDeductible;
            //                        car.CollDed = model.CollisionDeductible;
            //                        car.Save();
            //                        uow.CommitChanges();
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}

            #endregion

            if (model.IsEdit)
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: EDITING VEHICLE INFO FOR CAR {1}", Sessions.Instance.PolicyNo, model.CarIndex),
                    "INFO");
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    QuoteService.EditQuote(uow, model, quoteModel, formCollection, TempData);

                    return RedirectToAction("Edit", "Quote",
                        new {id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)});
                }
            }

            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                QuoteService.DenyQuote(uow, model, quoteModel, formCollection, TempData);

                return RedirectToAction("Edit", "Quote",
                    new {id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)});
            }
        }

        public ActionResult VehicleLoad(string id)
        {
            if (string.IsNullOrEmpty(Sessions.Instance.PolicyNo)) return RedirectToAction("Login", "Agent");

            var quoteModel = TempData["QuoteModel"] as QuoteModel;
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: LOAD VEHICLE DATA FOR QUOTE", Sessions.Instance.PolicyNo), "INFO");
                var car = uow.QuoteRepository.GetQuoteCarByOID(id);
                if (car == null)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VEHICLE DATA NOT FOUND FOR CAR {1}", Sessions.Instance.PolicyNo, id),
                        "FATAL");
                    throw new HttpException(404, "Car not found");
                }

                var model = UtilitiesRating.MapXpoToModel(car);
                //	var lienHolders = new XPCollection<PolicyCarLienHolders>(uow, CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo));  LF 10/1/18
                /***            var lienHolders = new XPCollection<PolicyCarLienHolders>(uow, CriteriaOperator.Parse("PolicyNo = ? AND CarIndex = ?", Sessions.Instance.PolicyNo, model.CarIndex));
                            foreach (PolicyCarLienHolders lienHolder in lienHolders)
                            {
                                LienHolderModel lienHolderModel = UtilitiesRating.MapXpoToModel(lienHolder);
                                model.LienHolders.Add(lienHolderModel);
                            }  LF 10/2/2018 Commented out - this is done in MapXpoToModel - with this uncommented, all are doubled  ***/
                model.IsEdit = true;
                var tmpModel = quoteModel.Vehicles.Find(v => v.VehicleId == id);
                quoteModel.Vehicles.Remove(tmpModel);
                quoteModel.Vehicles.Add(model);
                TempData["QuoteModel"] = quoteModel;
                TempData["VehicleID"] = id;
                TempData["CarIndex"] = model.CarIndex; //LF  added 10/9/18
                //return PartialView("~/Views/Quote/EditorTemplates/Vehicle.cshtml", UtilitiesRating.MapXpoToModel(car));
                return RedirectToAction("AddVehicle", model);
            }
        }

        public ActionResult VehicleDelete(string id)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var car = QuoteService.DeleteVehicleRecords(id);

                var cars = uow.QuoteRepository.GetQuoteCars(car.PolicyNo);

                return PartialView("~/Views/Quote/DisplayTemplates/Vehicles.cshtml",
                    cars.Select(c => UtilitiesRating.MapXpoToModel(c)).ToList());
            }
        }

        #endregion

        #region Lienholders

        [HttpPost]
        public ActionResult LienHolderAdd(LienHolderAddModel model)
        {
            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: ADD LIENHOLDER DATA TO CAR {1}", Sessions.Instance.PolicyNo, model.CarIndex),
                "INFO");

            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var vehicleModel = TempData["VehicleModel"] as VehicleModel;
                var carIndex = Convert.ToInt32(TempData["CarIndex"]);
                if (!string.IsNullOrEmpty(model.Name) && !string.IsNullOrEmpty(model.StreetAddress) &&
                    !string.IsNullOrEmpty(model.ZIP) && !string.IsNullOrEmpty(model.City) &&
                    !string.IsNullOrEmpty(model.State))
                {
                    var totalLienHolders = uow.PolicyRepository.GetLienHolderCount(model.PolicyNo);
                    var addLien1 = new PolicyCarLienHolders();
                    addLien1.CarIndex = carIndex;
                    addLien1.LienHolderIndex = totalLienHolders + 1;
                    addLien1.PolicyNo = Sessions.Instance.PolicyNo;
                    addLien1.Name = model.Name.Upper();
                    addLien1.Address = model.StreetAddress.Upper();
                    addLien1.City = model.City.Upper();
                    addLien1.State = model.State.Upper();
                    addLien1.Zip = model.ZIP;
                    addLien1.IsNonInstitutional = model.IsNonInstitutional;
                    addLien1.Phone = model.Phone;
                    addLien1.isAddInterest = model.isAddInterest;
                    addLien1.VIN = vehicleModel.VIN;
                    uow.PolicyRepository.SaveLienHolder(addLien1);
                    uow.Save();
                    var lhModel = UtilitiesRating.MapXpoToModel(addLien1);
                    vehicleModel.LienHolders.Add(lhModel);
                }

                TempData["VehicleID"] = TempData["VehicleID"];
                TempData["VehicleModel"] = vehicleModel;
                TempData["CarIndex"] = carIndex;
                return RedirectToAction("AddVehicle");
            }
        }

        public ActionResult AddLienHolder(string quoteNo)
        {
            var addModel = new LienHolderAddModel();
            addModel.PolicyNo = quoteNo;
            TempData["VehicleModel"] = TempData["VehicleModel"];
            return View("AddLienHolder", addModel);
        }

        public ActionResult LienHolderDelete(string id)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var getLien = uow.PolicyRepository.GetLienHolderById(id);
                if (getLien != null) DbHelperUtils.DeleteRecord("PolicyCarLienHolders", "ID", id);

                var vehicleID = TempData["VehicleID"] as string;
                TempData["VehicleID"] = vehicleID;
                TempData["VehicleModel"] = TempData["VehicleModel"];

                return RedirectToAction("AddVehicle");
            }
        }

        #endregion

        #region Quote Documents

        [HttpPost]
        public ActionResult UploadPhysicalDamageImages(string PolicyNo, List<HttpPostedFileBase> files)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                DocumentService.UploadImages(PolicyNo, files, quoteControllerLog);

                return RedirectToAction("RateAndShow", new {id = PolicyNo});
            }
        }

        public ActionResult GeneratePolicyDocs(string id)
        {
            var policyNo = DocumentService.CreatePolicyDocs(id, PalmsClient);

            return RedirectToAction("Documents", new
            {
                id = Blowfish.Encrypt_CBC(policyNo)
            });
        }

        public ActionResult DownloadInfinityRatingReport()
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var policyno = Sessions.Instance.PolicyNo;
                var ratingID = uow.QuoteRepository.GetQuote(Sessions.Instance.PolicyNo).RatingID;

                if (ratingID > 0)
                {
                    var ratingFile = PalmsClient.GenerateInfinityRatingReport(ratingID.ToString());
                    var ratingFilename = Path.GetFileName(ratingFile);
                    var ratingFileLocation = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + ratingFilename;

                    if (System.IO.File.Exists(ratingFileLocation))
                    {
                        var file = new ContentDisposition
                        {
                            FileName = ratingFileLocation,
                            Inline = false
                        };

                        Response.ContentType = "application/force-download";
                        Response.AppendHeader("Content-Disposition", ratingFileLocation);
                        return File(ratingFileLocation, "application/pdf");
                    }

                    Response.ContentType = "application/force-download";
                    Response.AppendHeader("Content-Disposition",
                        Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf"));
                    PalmsClient.CloseRef();
                    return File(Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf"),
                        "application/pdf");
                }

                Response.ContentType = "application/force-download";
                Response.AppendHeader("Content-Disposition",
                    Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf"));
                PalmsClient.CloseRef();
                return File(Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf"),
                    "application/pdf");
            }
        }

        public ActionResult Documents(string id, bool testMode = false)
        {
            try
            {
                id = Blowfish.Decrypt_CBC(id);
            }
            catch
            {
            }

            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: VIEWING DOCUMENT PAGE", Sessions.Instance.PolicyNo), "INFO");
            var model = new DocumentsModel
            {
                PolicyNo = id,
                IsTest = testMode,
                ShowEftProfileMissingMsg = PolicyWebUtils.ShowEftPaymentProfileMissingMsg(id)
            };
            return View(model);
        }

        [NoCache]
        public ActionResult Payment(MakePaymentModel model, string minimumPayAmt = "")
        {
            Sessions.Instance.SweepBtnHitCount = 0;

            LogUtils.Log(Sessions.Instance.Username,
                string.Format("{0}: VIEWING MAKE PAYMENT PAGE", Sessions.Instance.PolicyNo), "INFO");

            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var getPayments = uow.PaymentRepository.GetPayments(Sessions.Instance.PolicyNo);

                if (getPayments.Count > 0)
                {
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: PAYMENT ALREADY EXISTS, SEND TO GENERATE POLICY DOCS",
                            Sessions.Instance.PolicyNo), "WARN");
                    return RedirectToAction("GeneratePolicyDocs", new
                    {
                        id = Sessions.Instance.PolicyNo
                    });
                }

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: CALL HANDLE PAYMENT MODEL METHOD", Sessions.Instance.PolicyNo), "INFO");

                PaymentService.HandlePaymentModel(model);
                return RedirectToAction("GeneratePolicyDocs", new
                {
                    id = Sessions.Instance.PolicyNo
                });
            }
        } //END Payment

        [HttpGet]
        public ActionResult QuoteIntegrations(QuoteIntegrationsViewModel vm)
        {
            ReportService.BuildExistingReports(vm);
            var isRCPOSOrdered = false;
            var allIntegrationsOrdered =
                IntegrationsUtility.GetFlagForAllIntegrationCall(vm.QuoteNo, ref isRCPOSOrdered);
            vm.RiskCheckOrdered = isRCPOSOrdered;

            if (!string.IsNullOrEmpty(vm?.QuoteNo) && vm.AllIntegrationsOrdered())
            {
                IntegrationsUtility.UpdateFlagForAllIntegrationCall(vm.QuoteNo, true, true, true);
                return View("QuoteIntegrations", vm);
            }

            var violationsModels = (IList<MvrViolationsModel>) TempData["MvrDriversViolations"];
            var aplusViolationsModels = (IList<APlusClaimModel>) TempData["AplusViolations"];
            var cvModels = (IList<CoverageVerifierReportModel>) TempData["CVModels"];
            Sessions.Instance.PolicyNo = vm.QuoteNo;
            if (vm != null && !string.IsNullOrEmpty(vm.QuoteNo))
            {
                if (!vm.IsRiskCheckProcessed()) return View("QuoteIntegrations", vm);

                if (violationsModels?.Any() == true || aplusViolationsModels?.Any() == true || cvModels?.Any() == true)
                {
                    if (violationsModels?.Any() == true) vm.MvrDriverViolationsModels = violationsModels;

                    if (aplusViolationsModels?.Any() == true) vm.APlusClaimModels = aplusViolationsModels;

                    if (cvModels != null && cvModels.Any()) vm.CoverageVerifierReportModels = cvModels;
                }

                using (var uow = UnitOfWorkFactory.GetUnitOfWork())
                {
                    // call policy score sproc if risk check and coverage verifier have been ordered
                    if (vm.RiskCheckOrdered && vm.CoverageVerifierOrdered())
                    {
                        var isNewBusinessAllowed = false;
                        var result = uow.PolicyRepository.GetPolicyScore(vm.QuoteNo, Sessions.Instance.Username, isNewBusinessAllowed);
                        if (result?.ResultSet?[1] != null && result.ResultSet[1].Rows[0] != null &&
                            result.ResultSet[1].Rows[0].Values[1] != null)
                        {
                            isNewBusinessAllowed = (bool) result.ResultSet[1].Rows[0].Values[1];
                            vm.PolicyScoreValidForBinding = isNewBusinessAllowed;
                        }
                        else
                        {
                            vm.PolicyScoreValidForBinding = false;
                        }
                    }
                }

                return View("QuoteIntegrations", vm);
            }

            var newViewModel = new QuoteIntegrationsViewModel();
            newViewModel.QuoteNo = vm.QuoteNo;
            isRCPOSOrdered = false;
            newViewModel.RiskCheckOrdered = isRCPOSOrdered;
            return View("QuoteIntegrations", newViewModel);
        }

        [HttpPost]
        public ActionResult RiskCheckIntegration(string id, FormCollection fc)
        {
            var vm = new QuoteIntegrationsViewModel {QuoteNo = id};
            var notAssociatedDriverIndexes = fc["RcPosDNotAssociatedDriverIds"].Split(',')
                .Where(x => !string.IsNullOrEmpty(x)).ToList();

            var NotAssociatedDrivIndexesToExclude = new List<string>();
            var confirmNotAssociatedDrivIndexes = new Dictionary<string, string>();
            foreach (var driverIndex in notAssociatedDriverIndexes)
            {
                var ddlNotAssociatedDriv = "ddlNotAssociatedDriver-" + driverIndex;

                if (fc.AllKeys.Contains(ddlNotAssociatedDriv))
                {
                    var ddlNotAssDrivSelected = fc[ddlNotAssociatedDriv];
                    if (ddlNotAssDrivSelected?.ToLower() == "rate or exclude")
                        NotAssociatedDrivIndexesToExclude.Add(driverIndex);
                    else if (!string.IsNullOrEmpty(ddlNotAssDrivSelected) &&
                             !confirmNotAssociatedDrivIndexes.ContainsKey(driverIndex))
                        confirmNotAssociatedDrivIndexes.Add(driverIndex, ddlNotAssDrivSelected);
                }
                //var chboxNADKey = "chkbox-exclude-NA-DI-" + driverIndex;
                //if (fc.AllKeys.Contains(chboxNADKey) && fc[chboxNADKey] == "on")
                //    NotAssociatedDrivIndexesToExclude.Add(driverIndex);

                //chboxNADKey = "chkbox-notAssociated-NA-DI-" + driverIndex;
                //if (fc.AllKeys.Contains(chboxNADKey) && fc[chboxNADKey] == "on")
                //    confirmNotAssociatedDrivIndexes.Add(driverIndex);
            }

            QuoteService.UpdateDriversExcludedAndNotAssociated(NotAssociatedDrivIndexesToExclude,
                confirmNotAssociatedDrivIndexes, id);
            if (NotAssociatedDrivIndexesToExclude.Count() > 0)
            {
                IntegrationsUtility.UpdateFlagForAllIntegrationCall(Sessions.Instance.PolicyNo, false, true);
                return RedirectToAction("Edit", new {id = Blowfish.Encrypt_CBC(id)});
            }

            return RedirectToAction("QuoteIntegrations", vm);
        }

        private void SetReportParams(string formdesc, string policyno, int historyid)
        {
            ReportService.MakeReportParams(Blowfish, formdesc, policyno, historyid);

            //Logger.Log(listParamNames, "listParamNames");
            //Logger.Log(listParamValues, "listParamValues");
        }

        public ActionResult OrderMvr()
        {
            try
            {
                //string[] mvrs = PalmsClient.OrderMVR(Sessions.Instance.PolicyNo);
                //PalmsClient.CloseRef();
                return RedirectToAction("RateAndShow", "Quote",
                    new {id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)});
            }
            catch
            {
                return RedirectToAction("RateAndShow", "Quote",
                    new {id = Blowfish.Encrypt_CBC(Sessions.Instance.PolicyNo)});
            }
        }

        public ActionResult DeleteQuote(string PolicyNo)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var Quote = uow.QuoteRepository.GetQuote(PolicyNo);
                Quote.IsShow = false;
                uow.QuoteRepository.SaveQuote(Quote);
                uow.Save();

                LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: THIS QUOTE HAS BEEN DELETED", PolicyNo),
                    "FATAL");
            }

            return RedirectToAction("Search", "Agent");
        }

        public ActionResult Download()
        {
            var quoteno = Sessions.Instance.PolicyNo;
            var insAppFile = PalmsClient.GenerateInsuranceApplication(quoteno, "Quote");
            LogUtils.Log(Sessions.Instance.Username, Sessions.Instance.PolicyNo + ": PRINTING INS QUOTE APPLICATION");
            var insAppFileFilename = Path.GetFileName(insAppFile);
            var location = $@"\\{ConfigSettings.ReadSetting("IIS_Server")}\pdfs\" + insAppFileFilename;

            if (System.IO.File.Exists(location))
            {
                var file = new ContentDisposition
                {
                    FileName = insAppFile,
                    Inline = false
                };

                Response.ContentType = "application/force-download";
                Response.AppendHeader("Content-Disposition", insAppFile);
                return File(location, "application/pdf");
            }

            Response.ContentType = "application/force-download";
            Response.AppendHeader("Content-Disposition",
                Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf"));
            PalmsClient.CloseRef();
            return File(Url.Content(ConfigSettings.ReadSetting("ContentFilesPath") + @"Error.pdf"), "application/pdf");
        }

        public ActionResult RequestDocuments(string PolicyNo, string EmailAddr)
        {
            using (var uow = UnitOfWorkFactory.GetUnitOfWork())
            {
                var docs = uow.DocumentRepository.GetDocumentRequest(PolicyNo);
                    
                docs.Username = EmailAddr;
                uow.DocumentRepository.SaveDocumentRequest(docs);
                uow.Save();

                return RedirectToAction("Search", "Agent");
            }
        }

        #endregion

        #region TESTS

        //////////////////////////////////////////////////////////////////////////////////////////////////
        ///
        //        public void Test()
        //        {
        //            XPCollection<Policy> allPolicies = new XPCollection<Policy>();
        //            foreach (var policy in allPolicies)
        //            {
        //                VendorVeriskHistory historyForRiskCheck =
        //                    new XPCollection<VendorVeriskHistory>(uow, CriteriaOperator.Parse("PolicyNo = ?", policy.PolicyNo))
        //                        .FirstOrDefault();
        //                if (historyForRiskCheck == null)
        //                {
        //                    RiskCheckRquest requestData = GetRiskCheckRequestData(policy.PolicyNo);
        //                    IntegrationsUtility.RiskCheckReport(policy.PolicyNo, requestData).GetAwaiter().GetResult();
        //                }
        //            }
        //        }

        #endregion
    }
}