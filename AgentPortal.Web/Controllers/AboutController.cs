﻿using System;
using System.Web.Mvc;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Controllers
{
    public class AboutController : BaseController
    {
        public AboutController() : base()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.CompanyLogoName) || string.IsNullOrEmpty(Sessions.Instance.FooterMiddle))
            {
                int companyID = Convert.ToInt32(ConfigSettings.ReadSetting("CompanyID"));
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    PortalThemes theme = uow.FindObject<PortalThemes>(CriteriaOperator.Parse("IndexID = ?", companyID));
                    if (theme != null)
                    {
                        Sessions.Instance.CompanyName = theme.CompanyName;
                        Sessions.Instance.CompanyLogoName = theme.LogoName;
                        Sessions.Instance.CompanyPrimaryColor = theme.PrimaryColor;
                        Sessions.Instance.CompanySecondaryColor = ""; // to do
                        Sessions.Instance.WhoWeAre = theme.WhoWeAre;
                        Sessions.Instance.FullServiceCo = theme.FullServiceCo;
                        Sessions.Instance.WhoWeRepresent = theme.WhoWeRepresent;
                        Sessions.Instance.WhyThisCompany = theme.WhyThisCompany;
                        Sessions.Instance.AboutUs = theme.AboutUs;
                        Sessions.Instance.Welcome = theme.Welcome;
                        Sessions.Instance.OurStaff = theme.OurStaff;
                        Sessions.Instance.UnderwritingAndAccounting = theme.UnderwritingAndAccounting;
                        Sessions.Instance.ClaimsManagement = theme.ClaimsManagement;
                        Sessions.Instance.SpecialUnit = theme.SpecialUnit;
                        Sessions.Instance.InformationTechnology = theme.InformationTechnology;
                        Sessions.Instance.CustomerService = theme.CustomerService;
                        Sessions.Instance.AboutImageName = theme.AboutImageName;
                        Sessions.Instance.SliderImageName1 = theme.SliderImageName1;
                        Sessions.Instance.SliderImageName2 = theme.SliderImageName2;
                        Sessions.Instance.SliderImageName3 = theme.SliderImageName3;
                        Sessions.Instance.ContactPhone = theme.ContactPhone;
                        Sessions.Instance.ContactEmail = theme.ContactEmail;
                        Sessions.Instance.FooterMiddle = theme.FooterMiddle;
                        Sessions.Instance.FooterAboutUs = theme.FooterAboutUs;
                    }
                }
            }
        }
        //
        // GET: /About/
        public ActionResult Index()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.CompanyLogoName) || string.IsNullOrEmpty(Sessions.Instance.FooterMiddle))
            {
                int companyID = Convert.ToInt32(ConfigSettings.ReadSetting("CompanyID"));
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    PortalThemes theme = uow.FindObject<PortalThemes>(CriteriaOperator.Parse("IndexID = ?", companyID));
                    if (theme != null)
                    {
                        Sessions.Instance.CompanyName = theme.CompanyName;
                        Sessions.Instance.CompanyLogoName = theme.LogoName;
                        Sessions.Instance.CompanyPrimaryColor = theme.PrimaryColor;
                        Sessions.Instance.CompanySecondaryColor = ""; // to do
                        Sessions.Instance.WhoWeAre = theme.WhoWeAre;
                        Sessions.Instance.FullServiceCo = theme.FullServiceCo;
                        Sessions.Instance.WhoWeRepresent = theme.WhoWeRepresent;
                        Sessions.Instance.WhyThisCompany = theme.WhyThisCompany;
                        Sessions.Instance.AboutUs = theme.AboutUs;
                        Sessions.Instance.Welcome = theme.Welcome;
                        Sessions.Instance.OurStaff = theme.OurStaff;
                        Sessions.Instance.UnderwritingAndAccounting = theme.UnderwritingAndAccounting;
                        Sessions.Instance.ClaimsManagement = theme.ClaimsManagement;
                        Sessions.Instance.SpecialUnit = theme.SpecialUnit;
                        Sessions.Instance.InformationTechnology = theme.InformationTechnology;
                        Sessions.Instance.CustomerService = theme.CustomerService;
                        Sessions.Instance.AboutImageName = theme.AboutImageName;
                        Sessions.Instance.SliderImageName1 = theme.SliderImageName1;
                        Sessions.Instance.SliderImageName2 = theme.SliderImageName2;
                        Sessions.Instance.SliderImageName3 = theme.SliderImageName3;
                        Sessions.Instance.ContactPhone = theme.ContactPhone;
                        Sessions.Instance.ContactEmail = theme.ContactEmail;
                        Sessions.Instance.FooterMiddle = theme.FooterMiddle;
                        Sessions.Instance.FooterAboutUs = theme.FooterAboutUs;
                    }
                }
            }
            return View();
        }

    }//END Class
}//END Namespace
