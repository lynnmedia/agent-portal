﻿using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using AgentPortal.Support.Utilities.LexisNexis;
using AlertAuto.Integrations.Contracts.MVR;
using AlertAuto.Integrations.Contracts.RAPA;
using AlertAuto.Integrations.Contracts.Verisk.APlus;
using AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using AlertAuto.Integrations.Contracts.HelloSign;
using Address = AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Address;
using Driver = AlertAuto.Integrations.Contracts.Verisk.CoverageVerifier.Driver;
using PolicySignature = AgentPortal.Support.DataObjects.PolicySignature;
using RatingRelativitiesDo = AgentPortal.Support.DataObjects.RatingRelativitiesDo;
using Subject = AlertAuto.Integrations.Contracts.Verisk.APlus.Subject;
using VinLookupDO = AgentPortal.Support.DataObjects.VinLookupDO;
using WorkFlowForms = AgentPortal.Support.DataObjects.WorkFlowForms;
using RiskCheck = AlertAuto.Integrations.Contracts.Verisk.RiskCheck;


namespace AgentPortal.Controllers
{
    //[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class AjaxHelpersController : BaseController
    {
        private PalmsServiceClient PalmsClientInstance;

        /// <summary>
        /// City/State lookup by zip code.
        /// </summary>
        /// <param name="id">Zip Code</param>
        /// <returns>State - string</returns>
        /// <returns>Cities - Array</returns>
        public ActionResult ZipLookup(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var results =
                    new XPCollection<ZipLookup>(uow, new BinaryOperator("ZipCode", id, BinaryOperatorType.Equal));
                results.Load();

                if (results.Count == 0)
                {
                    return null;
                }


                return Json(new
                {
                    state = results.First().State,
                    cities = results.Select(z => z.CityName).ToArray()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Zip code lookup for garageing data. Includes County.
        /// </summary>
        /// <param name="id">Zipcode</param>
        /// <returns></returns>
        public ActionResult GarageZipLookup(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var results =
                    new XPCollection<ZipLookup>(uow, new BinaryOperator("ZipCode", id, BinaryOperatorType.Equal));
                results.Load();

                if (results.Count == 0)
                {
                    return null;
                }

                return Json(new
                {
                    state = results.First().State,
                    cities = results.Select(z => z.CityName).ToArray(),
                    counties = results.Select(c => c.County).ToArray()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ViolationAutocomplete()
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string rateCycle = XpoAARateCycles.GetCurrentCycle(uow).RateCycle;

                if (String.IsNullOrWhiteSpace(rateCycle))
                {
                    throw new Exception("Could not find most recent rate cycle");
                }


                var getViolations =
                    new XPCollection<Support.DataObjects.XpoAARateViolations>(uow, CriteriaOperator.Parse("RateCycle = ?", rateCycle));
                var sortViolations = new SortingCollection { new SortProperty("LongDesc", SortingDirection.Ascending) };
                getViolations.Sorting = sortViolations;

                return Json(getViolations.Select(v => v.LongDesc).ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

        // TODO: Replace Linq with XPCollections.

        #region Vin lookups

        /// <summary>
        /// Get All Makes Available By Year
        /// </summary>
        /// <param name="year">Year</param>
        /// <returns></returns>
        public JsonResult VinMakes(string year)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var output = new List<dynamic>();

                var vins = new XPCollection<VehicleMakes>(uow, CriteriaOperator.Parse("IsActive = ?", "1"));

                foreach (var vin in vins)
                {
                    dynamic itemToAdd = new
                    {
                        vin.ISOCode,
                        vin.Make
                    };
                    output.Add(itemToAdd);
                }
                ////////////////////

                return Json(output, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult VinModels(string year, string make)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                return Json(new XPQuery<VinLookupDO>(uow)
                    .Where(v => v.xxYear == year && v.FullName == make)
                    .Select(v => v.FullModelName)
                    .Distinct()
                    .ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult VinBodyStyles(string year, string make, string model)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                return Json(new XPQuery<VinLookupDO>(uow)
                    .Where(v => v.xxYear == year && v.FullName == make && v.FullModelName == model)
                    .Select(v => v.BodyStyle)
                    .Distinct()
                    .ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult VinPartials(string year, string make, string model, string bodyStyle)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var vins = new XPQuery<VinLookupDO>(uow)
                    .Where(v => v.xxYear == year && v.FullName == make && v.FullModelName == model &&
                                v.BodyStyle == bodyStyle)
                    .Select(v => v.VinNo)
                    .Distinct()
                    .ToList();

                return Json(vins, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetVehicleInfoByVin(string vin = "", string year = "", string make = "", string model = "",
            string bodyStyle = "")
        {
            if (string.IsNullOrEmpty(vin) && (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(make) ||
                                              string.IsNullOrEmpty(model) || string.IsNullOrEmpty(bodyStyle)))
            {
                return new JsonResult();
            }

            if (!string.IsNullOrEmpty(vin) && vin.Length < 10)
            {
                return new JsonResult();
            }

            VeriskVinSearchResultBody result = IntegrationsUtility.SearchVin(Sessions.Instance.PolicyNo, vin, year, make, model, bodyStyle);

            if (TempData["QuoteModel"] != null)
            {
                TempData["QuoteModel"] = TempData["QuoteModel"];
            }

            if (TempData["VehicleID"] != null)
            {
                TempData["VehicleID"] = TempData["VehicleID"];
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchVehicles(string year, string make, string model = "")
        {
            if (string.IsNullOrEmpty(year) || string.IsNullOrEmpty(make))
            {
                return new JsonResult();
            }

            if (TempData["QuoteModel"] != null)
            {
                TempData["QuoteModel"] = TempData["QuoteModel"];
            }
            if (TempData["VehicleID"] != null)
            {
                TempData["VehicleID"] = TempData["VehicleID"];
            }

            VeriskVehicleSearchResultBody result = IntegrationsUtility.SearchVehicle(Sessions.Instance.PolicyNo, year, make, model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Drivers Helpers

        public JsonResult GenerateDriversLicense(string fname, string lname, string mInitial, string gender, string dob)
        {
            var utilities = new UtilitiesObj();
            var dateOfBirth = dob.Parse<DateTime>();
            var month = dateOfBirth.ToString("MM").Parse<int>();
            var day = dateOfBirth.ToString("dd").Parse<int>();

            string driverLicense = null;
            try
            {
                driverLicense = utilities.GenerateFloridaDl(lname, fname, mInitial, gender,
                    dateOfBirth.ToString("yyyy"), day, month);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return Json(new { dlnumber = driverLicense }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RatePolicyEndorsementQuote(string policyNo, string garageStreetAddress, string garageCity,
            string garageState, string garageZIPCode, string mailingStreetAddress, string mailingCity,
            string mailingState, string mailingZIPCode, string pipDeductible, string biLimit, string compDed,
            bool? workLoss, string pdLimit, string collDed,
            bool? NIO, string medPayLimit, bool? NIRR, string umLimit, bool? umStacked, string adndLimit,
            string transDisc, bool? hasHomeownersDisc, bool? hasDirectRepairDisc, bool? hasLou, string insFirstName,
            string insuredLastName, string ins2FirstName, string ins2LastName, bool? hasPPO, bool? hasPaperlessDiscount, bool? hasAdvancedQuotesDiscount,
            string homePhone = null, string workPhone = null, string email = null, bool fromRating = false, string preferredLanguage = "ENGLISH")
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = "";
                string diff = "";
                double oldPrem = 0.0;
                double newPrem = 0.0;

                try
                {
                    if (fromRating)
                    {
                        TempData["FromRating"] = true;
                    }
                    // clear endorsement tables before a new rating.
                    //  uow.ExecuteSproc("spUW_ClearPolicyEndorseQuote", policyNo, Sessions.Instance.Username, new Guid().ToString());
                    // uow.ExecuteSproc("spUW_CopyPolicyToPolicyEndorseQuoteTables", policyNo, Sessions.Instance.Username, new Guid().ToString());

                    string endorsementDate = "";
                    if (TempData["EndorsementDate"] != null)
                        endorsementDate = TempData["EndorsementDate"].ToString();
                    if (string.IsNullOrEmpty(endorsementDate))
                        endorsementDate = DateTime.Now.ToString("MM-dd-yyyy");

                    if (workLoss == null)
                        workLoss = false;
                    if (hasDirectRepairDisc == null)
                        hasDirectRepairDisc = false;

                    XPCollection<PolicyEndorseQuoteDrivers> drivers;
                    PolicyEndorseQuote endorsement;
                    var model = AjaxService.CreateEndorsement(uow, out drivers, out endorsement, oldPrem, newPrem, policyNo,  garageStreetAddress,  garageCity,
                         garageState,  garageZIPCode,  mailingStreetAddress,  mailingCity, mailingState,  mailingZIPCode,  pipDeductible,  biLimit, workLoss,  
                         pdLimit, NIO,  medPayLimit, NIRR,  umLimit,  adndLimit, hasLou,  insFirstName, insuredLastName,  ins2FirstName,  ins2LastName, hasPPO, 
                         homePhone = null,  workPhone = null,  email = null);

                    model.CompDed = compDed;
                    model.CollDed = collDed;
                    TempData["LanguagePreference"] = preferredLanguage;
                    model.InsuredPreferredLanguage = preferredLanguage;
                    foreach (PolicyEndorseQuoteDrivers driver in drivers)
                    {
                        model.Drivers.Add(UtilitiesRating.MapXpoToModel(driver));
                    }

                    endorsement.Save();
                    uow.CommitChanges();

                    foreach (EndorsementDriverModel driverModel in model.Drivers)
                    {
                        PolicyEndorseQuoteDrivers xpo = uow.FindObject<PolicyEndorseQuoteDrivers>(
                            CriteriaOperator.Parse("PolicyNo = ? AND DriverIndex = ?", policyNo,
                                driverModel.DriverIndex));
                        xpo.PolicyNo = driverModel.PolicyID;
                        UtilitiesRating.MapModelToXpo(driverModel, xpo);
                        xpo.Save();
                        uow.CommitChanges();
                    }

                    Guid guid2 = Guid.NewGuid();
                    uow.ExecuteSproc("spUW_IssueEndorsement", policyNo, Sessions.Instance.Username, endorsementDate,
                        guid2.ToString(), guid2.ToString(), 0, 0, DateTime.Now, 0, DateTime.Now, 0, 1);
                    using (UnitOfWork uow2 = XpoHelper.GetNewUnitOfWork())
                    {
                        PolicyEndorseQuote endorsement2 = new XPCollection<PolicyEndorseQuote>(uow2, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
                        if (endorsement2 != null)
                        {
                            newPrem = endorsement2.PolicyWritten;
                        }
                    }

                    diff = (newPrem - oldPrem).ToString("n2");
                    TempData["ClearEndorseQuoteTables"] = false;
                    TempData["PolicyEndorseDiffAmt"] = diff;
                    msg = "SUCCESS";
                }
                catch (Exception ex)
                {
                    msg = "FAILED : " + ex.Message;
                }
                finally
                {
                    TempData["EndorsementDate"] = TempData["EndorsementDate"];
                }

                return Json(new
                {
                    returnMsg = msg,
                    diff,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetEndorseQuoteDriversCount(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getDrivers =
                    new XPCollection<PolicyEndorseQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", id));
                var count = getDrivers.Count;

                return Json(new { driverCount = count }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetQuoteDriversCount(string id)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getDrivers = new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", id));
                var count = getDrivers.Count;

                return Json(new { driverCount = count }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        [HttpPost]
        public JsonResult UpdatePolicyEndorsementField(string policyNo, string fieldName, string fieldValue)
        {
            if (string.IsNullOrEmpty(policyNo) || string.IsNullOrEmpty(fieldName) || 
               (string.IsNullOrEmpty(fieldValue) && fieldName?.ToLower() != "firstname2" && fieldName?.ToLower() != "lastname2" &&
                 fieldName?.ToLower() != "middleinitial2" && fieldName?.ToLower() != "middleinitial1" && fieldName?.ToLower() != "suffix1"
                 && fieldName?.ToLower() != "suffix2"))
            {
                return Json(new { isSucess=false, errorMessage="Invalid Data"}, JsonRequestBehavior.DenyGet);
            }
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                AjaxService.CreatePolicyEndorsementQuote(uow, policyNo, fieldName, fieldValue);
            }

            PolicyWebUtils.GetEndorsementChangeTypes(out bool premBearingchanges, out bool nonPremBearingChanges, policyNo, false);

            return Json(new
            {
                isSucess = true,
                onlyNonPremBearingChanges = (!premBearingchanges && nonPremBearingChanges),
                premBearingChanges = premBearingchanges
            }, JsonRequestBehavior.DenyGet);
        }

        [HttpGet]
        public JsonResult GetEndorsementChangeTypes(string policyNo)
        {
            PolicyWebUtils.GetEndorsementChangeTypes(out bool premBearingchanges, out bool nonPremBearingChanges, policyNo, false);

            return Json(new
            {
                isSucess = true,
                onlyNonPremBearingChanges = (!premBearingchanges && nonPremBearingChanges),
                premBearingChanges = premBearingchanges
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdatePayPlan(string payplan, string quoteNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var updateQuote = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                try
                {
                    updateQuote.PayPlan = payplan.Parse<int>();
                    XpoAARatePayPlans payPlan =
                        new XPCollection<XpoAARatePayPlans>(uow, CriteriaOperator.Parse("IndexID = ?", updateQuote.PayPlan))
                            .FirstOrDefault();
                    if (payPlan != null && payPlan.Code != null && payPlan.Code.ToUpper().Contains("EFT"))
                    {
                        updateQuote.EFT = true;
                        updateQuote.PaidInFullDisc = false;
                    }
                    else if (payPlan != null && payPlan.Code != null && payPlan.Code.ToUpper().Equals("FULL"))
                    {
                        updateQuote.PaidInFullDisc = true;
                        updateQuote.EFT = false;
                    }
                    else
                    {
                        updateQuote.PaidInFullDisc = false;
                        updateQuote.EFT = false;
                    }
                    updateQuote.Save();
                    uow.CommitChanges();
                    return Json(new { returnMsg = "Update Successful" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { returnMsg = "Update Failed" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult RequestPolicyCancellation(string policyNo, string dateOfCancel)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    bool success = PalmsClient.RequestCancellationNotice(policyNo, dateOfCancel);
                    if (success)
                    {
                        XPCollection<AuditImageIndex> auditImages =
                            new XPCollection<AuditImageIndex>(uow, CriteriaOperator.Parse("FileNo = ?", policyNo));
                        int index = auditImages.Max(i => i.IndexID);
                        AuditImageIndex imgToUpdate = auditImages.Where(i => i.IndexID == index).FirstOrDefault();
                        int? formId = null;
                        WorkFlowForms form = uow.FindObject<WorkFlowForms>(
                            CriteriaOperator.Parse(
                                "FormName = 'Cancellation Request' AND Department = 'UNDERWRITING'"));
                        if (form != null)
                        {
                            formId = form.IndexID;
                            if (formId != null && formId != 0)
                            {
                                string reclassifiedUser = null;
                                //  imgToUpdate.AssignedTo =
                                //       WorkFlowUtilities.ProcessWorkFlow(formId, policyNo, out reclassifiedUser);
                                imgToUpdate.WFClassification = form.FormName;
                                imgToUpdate.Dept = "UNDERWRITING";
                                imgToUpdate.UserName = Sessions.Instance.Username;
                                imgToUpdate.Category = imgToUpdate.FormDesc;
                                XpoAuditWorkFlowDO auditWFItem = new XpoAuditWorkFlowDO(uow);
                                auditWFItem.ImageIndexID = imgToUpdate.IndexID;
                                auditWFItem.FileNo = policyNo;
                                auditWFItem.Dept = imgToUpdate.Dept;
                                auditWFItem.FormType = imgToUpdate.FormType;
                                auditWFItem.FormDesc = imgToUpdate.FormDesc;
                                auditWFItem.DateCreated = DateTime.Now;
                                auditWFItem.CreatedBy = UserInfo.Instance.Username;
                                //  auditWFItem.AssignedTo = imgToUpdate.AssignedTo;
                                auditWFItem.WFClassification = imgToUpdate.WFClassification;
                                imgToUpdate.Save();
                                auditWFItem.Save();
                                uow.CommitChanges();
                            }
                        }

                        return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(
                            new
                            {
                                returnMsg = "FAILURE",
                                exceptionMsg =
                                    "An error occurred while processing your request. Please contact support if you continue to receive this message."
                            }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json(new { returnMsg = "FAILURE", exceptionMsg = msg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult RequestPolicyReInstatement(string policyNo, string dateOfReinstatement)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    EmailService.SendEmailNotification(new List<string>() { "reinstatements@palminsure.com" },
                        $"Agent {Sessions.Instance.AgencyName} has requested re-instatment for policy {policyNo}.",
                        $"Agent {Sessions.Instance.AgencyName} has requested re-instatment for policy {policyNo}.",
                        false);
                    bool success = PalmsClient.RequestReInStatementNotice(policyNo, dateOfReinstatement);
                    if (success)
                    {
                        XPCollection<AuditImageIndex> auditImages =
                            new XPCollection<AuditImageIndex>(uow, CriteriaOperator.Parse("FileNo = ?", policyNo));
                        int index = auditImages.Max(i => i.IndexID);
                        AuditImageIndex imgToUpdate = auditImages.Where(i => i.IndexID == index).FirstOrDefault();
                        int? formId = null;
                        WorkFlowForms form = uow.FindObject<WorkFlowForms>(
                            CriteriaOperator.Parse(
                                "FormName = 'Reinstatement Request' AND Department = 'UNDERWRITING'"));
                        if (form != null)
                        {
                            formId = form.IndexID;
                            if (formId != null && formId != 0)
                            {
                                string reclassifiedUser = null;
                                imgToUpdate.AssignedTo =
                                    WorkFlowUtilities.ProcessWorkFlow(formId, policyNo, out reclassifiedUser);
                                imgToUpdate.WFClassification = form.FormName;
                                imgToUpdate.Dept = "UNDERWRITING";
                                imgToUpdate.UserName = Sessions.Instance.Username;
                                imgToUpdate.Category = imgToUpdate.FormDesc;
                                XpoAuditWorkFlowDO auditWFItem = new XpoAuditWorkFlowDO(uow);
                                auditWFItem.ImageIndexID = imgToUpdate.IndexID;
                                auditWFItem.FileNo = policyNo;
                                auditWFItem.Dept = imgToUpdate.Dept;
                                auditWFItem.FormType = imgToUpdate.FormType;
                                auditWFItem.FormDesc = imgToUpdate.FormDesc;
                                auditWFItem.DateCreated = DateTime.Now;
                                auditWFItem.CreatedBy = UserInfo.Instance.Username;
                                auditWFItem.AssignedTo = imgToUpdate.AssignedTo;
                                auditWFItem.WFClassification = imgToUpdate.WFClassification;
                                imgToUpdate.Save();
                                auditWFItem.Save();
                                uow.CommitChanges();
                            }
                        }

                        return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(
                            new
                            {
                                returnMsg = "FAILURE",
                                exceptionMsg =
                                    "An error occurred while processing your request. Please contact support if you continue to receive this message."
                            }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                    return Json(new { returnMsg = "FAILURE", exceptionMsg = msg }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult UpdateCreditMsg(string msg, string quoteNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicyQuote update =
                    uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo =?", Sessions.Instance.PolicyNo));
                if (update != null)
                {
                    update.CreditMsg = msg;
                    uow.CommitChanges();
                }

                return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateDriverMsg(string msg, string quoteNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                PolicyQuoteDrivers update =
                    uow.FindObject<PolicyQuoteDrivers>(
                        CriteriaOperator.Parse("PolicyNo =?", Sessions.Instance.PolicyNo));
                if (update != null)
                {
                    update.DriverMsg = msg;
                    uow.CommitChanges();
                }

                return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReRequestESignaturesForQuote(string policyNo)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                if (string.IsNullOrEmpty(policyNo))
                {
                    throw new ArgumentException("PolicyNo cannot be null or empty.");
                }

                XpoPolicy xpoPolicy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo))
                    .FirstOrDefault();
                XpoAgents xpoAgent =
                    new XPCollection<XpoAgents>(uow,
                        CriteriaOperator.Parse("AgentCode = ?", Sessions.Instance.AgentCode)).FirstOrDefault();
                IEnumerable<PolicySignature> existingSignatures =
                    new XPCollection<PolicySignature>(uow,
                        CriteriaOperator.Parse("PolicyNo = ? AND Status = 1", policyNo));
                if (existingSignatures.Any())
                {
                    Dictionary<string, string> userEmailMap = new Dictionary<string, string>();
                    foreach (var sig in existingSignatures)
                    {
                        string name = "";
                        if (sig.EmailAddress.ToLower().Equals(xpoPolicy.MainEmail.ToLower()))
                        {
                            name = $"{xpoPolicy.Ins1First} {xpoPolicy.Ins1Last}";
                        }
                        else if (sig.EmailAddress.ToLower().Equals(xpoAgent.EmailAddress.ToLower()))
                        {
                            name = xpoAgent.AgencyName;
                        }

                        if (!userEmailMap.ContainsKey(name))
                        {
                            userEmailMap.Add(name, sig.EmailAddress);
                        }
                    }

                    EmailService.ReSendESignatureRequest(policyNo,
                        existingSignatures.FirstOrDefault().SignatureRequestId, userEmailMap);
                }

                return RedirectToAction("Policy", "Agent", new { id = policyNo });
            }
        }

        public ActionResult OrderIntegrations(string quoteNo, bool isRiskCheck = false, bool orderCoverageVerifierOnly = false)
        {
            if (string.IsNullOrEmpty(quoteNo))
            {
                throw new ArgumentException("QuoteNo cannot be null or empty.");
            }

            QuoteIntegrationsViewModel vm;
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                vm = AjaxService.CreateQuoteIntegrations(uow, quoteNo, isRiskCheck, orderCoverageVerifierOnly);
                uow.CommitChanges();  
            }

            if (isRiskCheck)
                IntegrationsUtility.UpdateFlagForAllIntegrationCall(vm.QuoteNo, !isRiskCheck, true, isRiskCheck);
            else if (!isRiskCheck)
                IntegrationsUtility.UpdateFlagForAllIntegrationCall(vm.QuoteNo, !isRiskCheck, false, !isRiskCheck);
            TempData["RiskCheckDriversViolations"] = vm.RiskCheckDriverViolationsModels;
            TempData["MvrDriversViolations"] = vm.MvrDriverViolationsModels;
            TempData["AplusViolations"] = vm.APlusClaimModels;
            TempData["CVModels"] = vm.CoverageVerifierReportModels;
            return RedirectToAction("QuoteIntegrations", "Quote", vm);
        }

        public JsonResult SaveInsuredEftInfo(string bankName, string acctType, string bankAcctType, string aba,
            string acctNum, string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getBankInfo =
                    new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber = ?", policyNo))
                        .FirstOrDefault();

                if (getBankInfo == null)
                {
                    try
                    {
                        var bankInfo = new PolicyBankInfo(uow);
                        bankInfo.Account = acctType;
                        bankInfo.ABA = Blowfish.Encrypt_CBC(aba);
                        bankInfo.AccountNumber = Blowfish.Encrypt_CBC(acctNum);
                        bankInfo.AccountType = Blowfish.Encrypt_CBC(bankAcctType);
                        bankInfo.BankAccountName = bankName;
                        bankInfo.PolicyNumber = policyNo;
                        bankInfo.Save();
                        uow.CommitChanges();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                    return Json(new { returnMsg = "Saved Insured Eft" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var updateBankInfo =
                        uow.FindObject<PolicyBankInfo>(CriteriaOperator.Parse("PolicyNumber = ?", policyNo));

                    try
                    {
                        updateBankInfo.Account = acctType;
                        updateBankInfo.ABA = Blowfish.Encrypt_CBC(aba);
                        updateBankInfo.AccountNumber = Blowfish.Encrypt_CBC(acctNum);
                        updateBankInfo.AccountType = Blowfish.Encrypt_CBC(bankAcctType);
                        updateBankInfo.BankAccountName = bankName;
                        updateBankInfo.PolicyNumber = policyNo;

                        updateBankInfo.Save();
                        uow.CommitChanges();
                        return Json(new { returnMsg = "Update Successful" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        return Json(new { returnMsg = "Update Failed" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
        }

        public JsonResult CalcPolicyTermDate(string effDate, int? termMonths)
        {
            try
            {
                var months = termMonths == null ? 6 : (int)termMonths;

                var effectiveDate = effDate.Parse<DateTime>();
                var expDate = AjaxService.CalcExpDate(effectiveDate, months);
                var expDateStr = expDate.ToString("MM/dd/yyyy");
                return Json(new { expirationDate = expDateStr }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { expirationDate = "UnSuccessful" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateQuoteEdit(string quoteNo, string fieldName, string fieldValue)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                if (string.IsNullOrEmpty(quoteNo))
                {
                    return Json(new { returnMsg = "Update Failed" }, JsonRequestBehavior.AllowGet);
                }

                if (TempData["QuoteModel"] != null)
                {
                    TempData["QuoteModel"] = TempData["QuoteModel"];
                }
                if (TempData["VehicleID"] != null)
                {
                    TempData["VehicleID"] = TempData["VehicleID"];
                }
                if (fieldName == "CompDed" || fieldName == "CollDed")
                {
                    try
                    {
                        var getCars =
                           new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                        foreach (var car in getCars) //.Where(car => car.HasPhyDam))
                        {
                            if (string.IsNullOrEmpty(fieldValue) || fieldValue == "0")
                                fieldValue = "NONE";
                            car.CompDed = fieldValue;
                            car.CollDed = fieldValue;
                            if (!string.IsNullOrEmpty(fieldValue) && fieldValue.ToUpper() != "NONE")
                            {
                                if (car.HasPhyDam)
                                {
                                    car.HasComp = true;
                                    car.HasColl = true;
                                }
                                else
                                {
                                    car.HasComp = false;
                                    car.HasColl = false;
                                }
                            }
                            else
                            {
                                car.HasComp = false;
                                car.HasColl = false;
                            }
                            car.Save();

                        }
                        /*
                                                PolicyQuoteCars carsUpdate =
                                                    uow.FindObject<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                                                switch (fieldName)
                                                {
                                                    case "CompDed":
                                                    case "CollDed":
                                                        carsUpdate.CompDed = fieldValue;
                                                        carsUpdate.CollDed = fieldValue;
                                                        if (!string.IsNullOrEmpty(fieldValue) && fieldValue != "NONE")
                                                        {
                                                            carsUpdate.HasColl = true;
                                                            carsUpdate.HasComp = true;
                                                        }
                                                        else
                                                        {
                                                            carsUpdate.HasColl = false;
                                                            carsUpdate.HasComp = false;
                                                        }

                                                        break;
                                                }

                                                carsUpdate.Save();
                                                uow.CommitChanges(); */
                        return Json(new { returnMsg = "Update Successful" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        return Json(new { returnMsg = "Update Failed" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(fieldValue))
                        fieldValue = fieldValue.Upper();
                    XpoPolicyQuote updateQuote =
                        uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", quoteNo));
                    DateTime polEffDate = updateQuote.EffDate;
                    try
                    {
                        switch (fieldName)
                        {
                            //Save General Policy Info
                            case "RatingState":
                                updateQuote.State = fieldValue;
                                break;
                            case "IsCurrentCustomer":
                                updateQuote.IsCurrentCustomer = Convert.ToBoolean(fieldValue);
                                break;
                            case "PriorTerm":
                                updateQuote.PriorPolicyTerm = fieldValue;
                                break;
                            case "EffectiveDate":
                                updateQuote.EffDate = fieldValue.Parse<DateTime>();
                                break;
                            case "PolicyTermMonths":
                                updateQuote.isAnnual = (fieldValue.Parse<int>() == 12);
                                updateQuote.SixMonth = (fieldValue.Parse<int>() == 6);
                                updateQuote.ExpDate = AjaxService.CalcExpDate(polEffDate, fieldValue.Parse<int>());
                                break;
                            case "PriorCoverage":
                                updateQuote.HasPriorCoverage = fieldValue.Parse<bool>();
                                break;
                            case "PreviousBI":
                                updateQuote.PreviousBI = fieldValue.Parse<bool>();
                                break;
                            case "PriorCompany":
                                updateQuote.PreviousCompany = fieldValue;
                                break;
                            case "PriorPolicyNo":
                                updateQuote.PriorPolicyNo = fieldValue;
                                break;
                            case "PreviousExpDate":
                                updateQuote.PreviousExpDate = fieldValue.Parse<DateTime>();
                                break;
                            //Save Insured Info
                            case "FirstName1":
                                updateQuote.Ins1First = fieldValue;
                                break;
                            case "MiddleInitial1":
                                updateQuote.Ins1MI = fieldValue;
                                break;
                            case "LastName1":
                                updateQuote.Ins1Last = fieldValue;
                                updateQuote.Ins1FullName = string.Format("{0} {1}", updateQuote.Ins1First,
                                    updateQuote.Ins1Last);
                                break;
                            case "Suffix1":
                                updateQuote.Ins1Suffix = fieldValue;
                                break;
                            case "FirstName2":
                                updateQuote.Ins2First = fieldValue;
                                break;
                            case "MiddleInitial2":
                                updateQuote.Ins2MI = fieldValue;
                                break;
                            case "LastName2":
                                updateQuote.Ins2Last = fieldValue;
                                updateQuote.Ins2FullName = string.Format("{0} {1}", updateQuote.Ins2First,
                                    updateQuote.Ins2Last);
                                break;
                            case "Suffix2":
                                updateQuote.Ins2Suffix = fieldValue;
                                break;
                            case "HomePhone":
                                updateQuote.HomePhone = fieldValue;
                                break;
                            case "WorkPhone":
                                updateQuote.WorkPhone = fieldValue;
                                break;
                            case "EmailAddress":
                                updateQuote.MainEmail = fieldValue;
                                break;
                            //Save Garage Address
                            case "GarageStreetAddress":
                                updateQuote.GarageStreet = fieldValue;
                                break;
                            case "GarageCity":
                                if (!fieldValue.ToUpper().Equals("FIND BY ZIP"))
                                {
                                    updateQuote.GarageCity = fieldValue;
                                }
                                break;
                            case "GarageZIPCode":
                                updateQuote.GarageZip = fieldValue;
                                break;
                            case "GarageCounty":
                                updateQuote.GarageCounty = fieldValue;
                                break;
                            case "GarageState":
                                updateQuote.GarageState = fieldValue;
                                break;
                            //Save Mailing Info
                            case "SameAsGarage":
                                updateQuote.MailGarageSame = fieldValue.Parse<bool>();
                                break;
                            case "MailingCity":
                                if (!fieldValue.ToUpper().Equals("FIND BY ZIP"))
                                {
                                    updateQuote.MailCity = fieldValue;
                                }
                                break;
                            case "MailingStreetAddress":
                                updateQuote.MailStreet = fieldValue;
                                break;
                            case "MailingZIPCode":
                                updateQuote.MailZip = fieldValue;
                                break;
                            case "MailingState":
                                updateQuote.MailState = fieldValue;
                                break;
                            //Save Coverages And Discounts
                            case "PipDeductible":
                                updateQuote.PIPDed = fieldValue;
                                break;
                            case "Workloss":
                                bool value = fieldValue.Parse<bool>();
                                updateQuote.WorkLoss = value == true ? 1 : 0;
                                break;
                            case "WorkLossGroup":
                                updateQuote.NIO = fieldValue == "1";
                                updateQuote.NIRR = fieldValue == "2";
                                break;
                            case "PdLimit":
                                updateQuote.PDLimit = fieldValue;
                                break;
                            case "BiLimit":
                                updateQuote.BILimit = fieldValue;
                                break;
                            case "MedPayLimit":
                                updateQuote.MedPayLimit = fieldValue;
                                break;
                            case "UmLimit":
                                updateQuote.UMLimit = fieldValue;
                                break;
                            case "AdndLimit":
                                updateQuote.ADNDLimit = fieldValue.Parse<int>();
                                break;
                            case "HasTransDisc":
                                TransferDiscountOptions option = (TransferDiscountOptions)Enum.Parse(typeof(TransferDiscountOptions),
                                    fieldValue.Parse<string>());
                                updateQuote.TransDisc = (int)option;
                                break;
                            case "HasHomeownersDisc":
                                updateQuote.Homeowner = fieldValue.Parse<bool>();
                                break;
                            case "DirectRepairDiscount":
                                updateQuote.DirectRepairDisc = false; // fieldValue.Parse<bool>();
                                break;
                            case "PriorBiLimit":
                                updateQuote.PriorBILimit = fieldValue;
                                break;
                        }

                        //////////////////////
                        try
                        {
                            updateQuote.Save();
                            uow.CommitChanges();
                            return Json(new { returnMsg = "Update Successful" }, JsonRequestBehavior.AllowGet);
                        }
                        catch
                        {
                        }

                        //////////////////////
                        // Save Comp/Coll to car
                        var getCars =
                            new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteNo));

                        switch (fieldName)
                        {
                            case "CompDed":

                                foreach (var car in getCars.Where(car => car.HasPhyDam))
                                {
                                    car.CompDed = fieldValue;
                                    car.CollDed = fieldValue;
                                    car.Save();
                                }

                                break;
                            case "CollDed":
                                foreach (var car in getCars.Where(car => car.HasPhyDam))
                                {
                                    car.CompDed = fieldValue;
                                    car.CollDed = fieldValue;
                                    car.Save();
                                }

                                break;
                        }

                        //////////////////////
                        return Json(new { returnMsg = "Update Successful" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        return Json(new { returnMsg = "Update Failed" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
        }

        public JsonResult PriorCompaniesAutoComplete()
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var getPriorCompanies =
                    new XPCollection<PortalPriorCompanies>(uow, CriteriaOperator.Parse("Active = 1"));

                return Json(getPriorCompanies.Select(comp => comp.CompanyName).ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetQuoteCarsCount(string id)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                var getCars = new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", id));
                var count = getCars.Count;

                return Json(new { carCount = count }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckForExistingQuote(string agentcode, string firstname, string lastname)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                var returnmsg =
                    @"<div id='existingQuotes' class='info'><span>It appears like you've done this quote before, please edit one of the existing quotes if you used AccuAuto or QuickQuote to bridge.</span><br/><br/>";
                var isTrue = false;
                var getPolicies = new XPCollection<XpoPolicyQuote>(uow,
                    CriteriaOperator.Parse("AgentCode = ? AND Ins1First = ? AND Ins1Last = ?", agentcode, firstname,
                        lastname));

                foreach (var quote in getPolicies)
                {
                    isTrue = true;
                    returnmsg += string.Format(@"<a href='/Quote/Edit/{0}'>{1} - ({2})</a><br/>",
                        Blowfish.Encrypt_CBC(quote.PolicyNo), quote.PolicyNo,
                        string.IsNullOrEmpty(quote.FromRater) ? "Direct Quote" : quote.FromRater);
                }

                returnmsg += @"</div>";

                return Json(new { returnQuotes = isTrue ? returnmsg : "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [Obsolete("This code for routing does not work right, do not use")]
        public JsonResult ValidateRoutingNumber(string aba)
        {
            var routingNumber = aba.Parse<int>();
            string returnMsg;
            //The check equation for a number a1a2a3a4a5a6a7a8a9 is 3a1 + 7a2 + a3 + 3a4 + 7a5 + a6 + 3a7 + 7 a8 + a9 mod 10 = 0
            //must be 9 digits

            int[] routingNumberAsArray =
            {
                routingNumber / 100000000,
                (routingNumber % 100000000) / 10000000,
                (routingNumber % 10000000) / 1000000,
                (routingNumber % 1000000) / 100000,
                (routingNumber % 100000) / 10000,
                (routingNumber % 10000) / 1000,
                (routingNumber % 1000) / 100,
                (routingNumber % 100) / 10,
                (routingNumber % 10) / 1
            };

            //calculate the route sum using the formula above. 3 7 1.
            int routeSum =
                3 * routingNumberAsArray[0] +
                7 * routingNumberAsArray[1] +
                routingNumberAsArray[2] +
                3 * routingNumberAsArray[3] +
                7 * +routingNumberAsArray[4] +
                routingNumberAsArray[5] +
                3 * routingNumberAsArray[6] +
                7 * routingNumberAsArray[7] +
                routingNumberAsArray[8];

            returnMsg = routeSum % 10 == 0 ? "Valid ABA" : "Invalid ABA";

            return Json(new { returnABA = returnMsg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ShowFakePayDocs(string value)
        {
            string isTrue;

            isTrue = "I LOVE DAVID WIMBLEY" == value.ToUpper() ? "true" : "false;";

            return Json(new { returnMsg = isTrue }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateDriversLicense(string value)
        {
            string msg = null;

            if (value.Length == 15)
            {
                msg = "AUTO-GENERATED";
            }

            if (value.Length > 3 && value.Length < 15)
            {
                msg = "UNLICENSED";
            }

            if (value.Length < 4)
            {
                msg = "NOT VALID";
            }

            if (value.Length == 17)
            {
                msg = "TRUE";
            }

            return Json(new { returnMsg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckIfDriverHasMvr(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                bool hasMvr = false;
                //TODO: determine if MVR date should come into play to determine if we need to ordero a new one.
                var getDrivers = new XPCollection<PolicyQuoteDrivers>(uow,
                    CriteriaOperator.Parse("PolicyNo = ? AND HasMvr = 1", policyno));

                if (getDrivers.Any())
                {
                    hasMvr = true;
                }

                return Json(new { returnMsg = hasMvr ? "SUCCESS" : "FALSE" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckNewPolicyDocDownloads(string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                NewPolicyDocumentsStatusModel documentStatusModel = DocumentService.AuditPolicyDocDownloads(uow, policyNo);

                return Json(documentStatusModel, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetEftInfo(string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg;
                string accountType = null;
                string aba = null;
                string accountNumber = null;
                string bankName = null;
                string bankAcctType = null;

                try
                {
                    var getBankInfo =
                        new XPCollection<PolicyBankInfo>(uow, CriteriaOperator.Parse("PolicyNumber = ?", policyNo))
                            .FirstOrDefault();

                    if (getBankInfo == null)
                    {
                        return Json(
                            new
                            {
                                returnMsg = "BAD",
                                acctType = "",
                                acctAba = "",
                                acctNumber = "",
                                acctBankName = "",
                                acctBankType = ""
                            }, JsonRequestBehavior.AllowGet);
                    }

                    getBankInfo.Reload();
                    accountType = getBankInfo.Account;
                    bankAcctType = Blowfish.Decrypt_CBC(getBankInfo.AccountType);
                    aba = Blowfish.Decrypt_CBC(getBankInfo.ABA);
                    accountNumber = Blowfish.Decrypt_CBC(getBankInfo.AccountNumber);
                    bankName = getBankInfo.BankAccountName;
                    msg = "GOOD";
                }
                catch (Exception)
                {
                    msg = "BAD";
                }

                return Json(
                    new
                    {
                        returnMsg = msg,
                        acctType = accountType,
                        acctAba = aba,
                        acctNumber = accountNumber,
                        acctBankName = bankName,
                        acctBankType = bankAcctType
                    }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ValidateDate(string date)
        {
            string msg;

            var validateDate = date.Parse<DateTime>();
            var day = validateDate.Day;
            var year = validateDate.Year;

            switch (validateDate.Year.ToString().Length)
            {
                case 4:
                    year = validateDate.Year;
                    break;
                case 2:
                    var futureYear2 = ("20" + validateDate.Year).Parse<int>();
                    var pastYear2 = ("19" + validateDate.Year).Parse<int>();

                    year = futureYear2 > DateTime.Now.Year ? pastYear2 : futureYear2;
                    break;
                case 1:
                    var futureYear1 = ("200" + validateDate.Year).Parse<int>();
                    var pastYear1 = ("190" + validateDate.Year).Parse<int>();

                    year = futureYear1 > DateTime.Now.Year ? pastYear1 : futureYear1;
                    break;
            }


            int month = validateDate.Month;

            msg = new DateTime(year, month, day).ToString("MM/dd/yyyy");


            return Json(new { returnMsg = "SUCCESS", returnDate = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddNewVin(string vin,
            string vehYear,
            string vehMake,
            string vehModel,
            string abs,
            string ars,
            string convertable,
            string fourwheeldrive,
            string ISOSymbol,
            string VSRSymbol,
            string BISymbol,
            string PDSymbol,
            string COMPSymbol,
            string COLLSymbol,
            string UMSymbol,
            string MPSymbol
        )
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = null;
                string rtnMsg = null;

                try
                {
                    vin = vin.Insert(8, "&");
                    vin = vin.Remove(9, 1);

                    var save = new VinLookupDO(uow);
                    save.VinNo = vin.Upper();
                    save.xxYear = vehYear.Upper();
                    save.FullName = vehMake.Upper();
                    save.AbbrModelName = vehModel.Length > 10 ? vehModel.Substring(0, 9).Upper() : vehModel.Upper();
                    save.FullModelName = vehModel.Upper();
                    save.RestraintInfo = ars;
                    save.AntiLockBrake = abs;
                    save.FourWheelDrive = fourwheeldrive;
                    save.ISOSymbol = ISOSymbol;
                    save.VSRSymbol = VSRSymbol.Parse<int>();
                    save.BISymbol = BISymbol;
                    save.PDSymbol = PDSymbol;
                    save.COMPSymbol = COMPSymbol;
                    save.COLLSymbol = COLLSymbol;
                    save.UMSymbol = UMSymbol;
                    save.MPSymbol = MPSymbol;
                    save.VinID = PalmsClient.GetMaxVinId() + 1;
                    save.Increment = DateTime.Now.ToString("yymmddhhmmssff");
                    save.Save();
                    uow.CommitChanges();

                    rtnMsg = "SUCCESS";
                }
                catch (Exception ex)
                {
                    rtnMsg = "An Error Occurred: " + ex.Message;
                }

                PalmsClient.CloseRef();
                return Json(new { returnMsg = rtnMsg, message = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetSymbols(string year, string make, string model, string bodystyle)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = null;
                string isoSymb = null;
                string vsrSymb = null;
                string biSymb = null;
                string pdSymb = null;
                string compSymb = null;
                string collSymb = null;
                string umSymb = null;
                string mpSymb = null;

                var getVinData = new XPCollection<VinLookupDO>(uow,
                    CriteriaOperator.Parse("xxYear = ? AND FullName = ? AND FullModelName = ? AND BodyStyle = ?", year,
                        make, model, bodystyle));

                foreach (var vin in getVinData)
                {
                    isoSymb = vin.ISOSymbol;
                    vsrSymb = vin.VSRSymbol.ToString();
                    biSymb = vin.BISymbol;
                    pdSymb = vin.PDSymbol;
                    compSymb = vin.COMPSymbol;
                    collSymb = vin.COLLSymbol;
                    umSymb = vin.UMSymbol;
                    mpSymb = vin.MPSymbol;

                    msg = "SUCCESS";
                }

                return Json(new
                {
                    returnMsg = msg,
                    ISOSymbol = isoSymb,
                    VSRSymbol = vsrSymb,
                    BISymbol = biSymb,
                    PDSymbol = pdSymb,
                    COMPSymbol = compSymb,
                    COLLSymbol = collSymb,
                    UMSymbol = umSymb,
                    MPSymbol = mpSymb
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateRenPayPlan(string payplan, string quoteNo, string reRate, string payplanname)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msgreturn = null;
                string payPlan = "";

                if (reRate == "true")
                {
                    var getRen =
                        new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", quoteNo))
                            .FirstOrDefault();

                    if (getRen == null)
                    {
                        return Json(new { returnMsg = "Error" }, JsonRequestBehavior.AllowGet);
                    }

                    try
                    {
                        string nextPolicyNo = quoteNo.GenNextPolicyNo();
                        uow.ExecuteSproc("spUW_ClearPolicyRenQuoteTemp", nextPolicyNo,
                            Sessions.Instance.Username, new Guid().ToString());
                        uow.ExecuteSproc("spUW_TransferRenQuoteToRenQuoteTemp", nextPolicyNo,
                            Sessions.Instance.Username, new Guid().ToString());
                        var update =
                            uow.FindObject<PolicyRenQuoteTemp>(CriteriaOperator.Parse("PolicyNo = ?",
                                nextPolicyNo));
                        update.EFT = payplanname.SafeString().Contains("(EFT Discount)");
                        update.PayPlan = payplan.Parse<int>();
                        payPlan = update.PayPlan.ToString();
                        update.Save();
                        uow.CommitChanges();


                        msgreturn = "Successful";
                    }
                    catch (Exception ex)
                    {
                        msgreturn = "It appears an error occured: " + ex.Message;
                    }
                }

                //PalmsClient.CloseRef();
                return Json(new { returnMsg = msgreturn, returnPayPlan = payPlan }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetInfinityRatingRelativities(string quoteno)
        {
            string rtnHTML = "";
            string rtnMsg = "";
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                int ratingID = uow
                    .FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", Sessions.Instance.PolicyNo))
                    .RatingID;
                if (ratingID > 0)
                {
                    rtnMsg = "Success";
                    rtnHTML += @"
                    <table>
				        <thead>                 
				        	<tr>
						        <td>IndexID</td>
						        <td>Main IndexID</td>
						        <td>Vehicle IndexID</td>
                                <td>Operator IndexID</td>
						        <td>Policy Number</td>
						        <td>Vehicle No</td>
						        <td>Operator No</td>
						        <td>Table Name</td>
						        <td>Table IndexID</td>
						        <td>Rate Order No</td>
						        <td>Math Action</td>
						        <td>Round Step</td>
						        <td>Include Household</td>
						        <td>Relativity</td>
						        <td>BI</td>
						        <td>PIPNI</td>
						        <td>PIPNR</td>
						        <td>PD</td>
						        <td>UM</td>
						        <td>UMS</td>
						        <td>MP</td>
						        <td>COMP</td>
						        <td>COLL</td>
						        <td>SE</td>
						        <td>Date Created</td>
						        <td>Page No</td>
					        </tr>
				        </thead>";
                    var query = uow.ExecuteQuery(string.Format(
                        @"SELECT * FROM RatingRelativities where MainIndexID={0} order by VehicleNo,RatedOperatorNo,RateOrderNo",
                        ratingID));
                    foreach (SelectStatementResultRow row in query.ResultSet[0].Rows)
                    {
                        rtnHTML += " <tr >";
                        rtnHTML += "<td >" + row.Values[0] + "</td>";
                        rtnHTML += "<td >" + row.Values[1] + "</td>";
                        rtnHTML += "<td >" + row.Values[2] + "</td>";
                        rtnHTML += "<td >" + row.Values[3] + "</td>";
                        rtnHTML += "<td >" + row.Values[4] + "</td>";
                        rtnHTML += "<td >" + row.Values[5] + "</td>";
                        rtnHTML += "<td >" + row.Values[6] + "</td>";
                        rtnHTML += "<td >" + row.Values[7] + "</td>";
                        rtnHTML += "<td >" + row.Values[8] + "</td>";
                        rtnHTML += "<td >" + row.Values[9] + "</td>";
                        rtnHTML += "<td >" + row.Values[10] + "</td>";
                        rtnHTML += "<td >" + row.Values[11] + "</td>";
                        rtnHTML += "<td >" + row.Values[12] + "</td>";
                        rtnHTML += "<td >" + row.Values[13] + "</td>";
                        rtnHTML += "<td >" + row.Values[14] + "</td>";
                        rtnHTML += "<td >" + row.Values[15] + "</td>";
                        rtnHTML += "<td >" + row.Values[16] + "</td>";
                        rtnHTML += "<td >" + row.Values[17] + "</td>";
                        rtnHTML += "<td >" + row.Values[18] + "</td>";
                        rtnHTML += "<td >" + row.Values[19] + "</td>";
                        rtnHTML += "<td >" + row.Values[20] + "</td>";
                        rtnHTML += "<td >" + row.Values[21] + "</td>";
                        rtnHTML += "<td >" + row.Values[22] + "</td>";
                        rtnHTML += "<td >" + row.Values[23] + "</td>";
                        rtnHTML += "<td >" + row.Values[24] + "</td>";
                        rtnHTML += "<td >" + row.Values[25] + "</td>";
                        rtnHTML += "</tr>";
                    }

                    rtnHTML += "</table>";
                }
                else
                    rtnMsg = "Failure";
            }

            rtnHTML = rtnHTML.Replace("\r", "");
            rtnHTML = rtnHTML.Replace("\n", "");
            rtnHTML = rtnHTML.Replace("\t", "");
            return Json(new { returnMsg = rtnMsg, returnHtml = rtnHTML }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RateQuote(string quoteno, string pipDeductible, string transferDiscountValue,
            bool hasWorkLossYesSelected, bool hasNamedInsuredOnlyChecked,
            bool hasHomeOwnersDiscount, bool hasPPO, bool? hasPaperlessDiscount = false, bool? hasadvancedQuoteDiscount = false,
            bool emailQuote = false, string city = null, string state = null, string county = null, bool? iscurrentCustomerChecked = false,
            string priorCoverageTerm = null, bool mailSameAsGarage = true)
        {
            string returnHtmlFinal = null;
            string msgreturn = null;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("[FAST QUOTE] {0} rated quote", Sessions.Instance.AgentCode), "INFO");
             
                string rtnHtml = null;
                bool hasPhyDam = false;
                try
                {
                    
                    //if any car has physical damage, check for Comp/Coll deductible not None
                    XPCollection<PolicyQuoteCars> phyDamCars = new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ? AND HasPhyDam = '1'", quoteno)); //.FirstOrDefault();
                    hasPhyDam = phyDamCars.Any(c =>
                        c.HasPhyDam && (!string.IsNullOrEmpty(c.CompDed) && c.CompDed != "NONE"));
                    if (phyDamCars.Count > 0)
                    {
                        PolicyQuoteCars car = phyDamCars.FirstOrDefault();
                        if (car?.CompDed == null || car.CompDed == "NONE")
                        {
                            string errMsg = "Comp/Coll deductibles must be selected.";
                            string msg = string.Format(@"<div class='warning'>{0}</div>", errMsg);
                            return Json(new { returnMsg = msgreturn, returnHtml = msg }, JsonRequestBehavior.AllowGet);
                        }
                    } //at least one vehicle has physical damage

                    PolicyQuoteDrivers getSr22 =
                        new XPCollection<PolicyQuoteDrivers>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND SR22 = '1'", quoteno)).FirstOrDefault();
                    bool hasSr22 = getSr22 == null ? false : true;
                    string termMonths = "6";

                    List<RatingInstallmentsDO> listInstallments = new List<RatingInstallmentsDO>();
                    listInstallments.Clear();


                    XpoPolicyQuote quoteUpdate = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo = ?", quoteno));

                    Dictionary<int, string> payPlansDict = AjaxService.CreateDamagedCarQuote(uow, termMonths, quoteUpdate, hasPhyDam, quoteno, pipDeductible, transferDiscountValue,
                        hasWorkLossYesSelected, hasNamedInsuredOnlyChecked, hasHomeOwnersDiscount, hasPPO, hasPaperlessDiscount,
                        hasadvancedQuoteDiscount, emailQuote, city, state, county, iscurrentCustomerChecked,
                        priorCoverageTerm, mailSameAsGarage);

                    Sessions.Instance.RatingGuid = Guid.NewGuid().ToString();

                    uow.ExecuteSproc("spUW_RatePolicyAlert",
                                new OperandValue(0),
                                new OperandValue(Sessions.Instance.Username),
                                new OperandValue(Sessions.Instance.RatingGuid),
                                new OperandValue(quoteno),
                                new OperandValue(0));

                    if (TempData["QuoteModel"] != null)
                        TempData["QuoteModel"] = TempData["QuoteModel"] as QuoteModel;
                    XpoPolicyQuote quote =
                        new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteno))
                            .FirstOrDefault();
                    quote.Reload();
                    if (quote == null)
                    {
                        LogUtils.Log(Sessions.Instance.Username,
                            string.Format("Quote Not Found: {0}".ToUpper(), quoteno), "FATAL");
                        throw new HttpException(404, "Quote not found");
                    }
                    int carsCount =
                        new XPCollection<PolicyQuoteCars>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteno)).Count;
                    int driversCount =
                        new XPCollection<PolicyQuoteDrivers>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteno))
                            .Count;

                    if (carsCount == 0 || driversCount == 0)
                    {
                        string errMsg = carsCount == 0 && driversCount == 0
                            ?
                            "Must have at least one car and one driver added to quote"
                            : driversCount == 0
                                ? "Must have at least one driver added to quote."
                                : carsCount == 0
                                    ? "Must have at least one car added to quote"
                                    : "";
                        string msg = string.Format(@"<div class='warning'>{0}</div>", errMsg);
                        return Json(new { returnMsg = msgreturn, returnHtml = msg }, JsonRequestBehavior.AllowGet);
                    }
                    Support.DataObjects.XpoAARateTerritory rateTerritory =
                        new XPCollection<Support.DataObjects.XpoAARateTerritory>(uow,
                            CriteriaOperator.Parse("ZIP = ?", quoteUpdate.GarageZip)).FirstOrDefault();
                    if (rateTerritory == null)
                    {
                        string errorMessage = "Invalid Zip code. Contact UW for assistance.";
                        string msg = string.Format(@"<div class='warning'>{0}</div>", errorMessage);

                        return Json(new { returnMsg = msgreturn, returnHtml = msg }, JsonRequestBehavior.AllowGet);
                    }

                    if (quoteUpdate.HasPriorCoverage && (quoteUpdate.PreviousExpDate == null ||
                                                         quoteUpdate.PreviousExpDate.Equals(DateTime.MinValue)))
                    {
                        string errorMessage = "Previous policy expiration date is required when prior coverage selected.";
                        string msg = string.Format(@"<div class='warning'>{0}</div>", errorMessage);

                        return Json(new { returnMsg = msgreturn, returnHtml = msg }, JsonRequestBehavior.AllowGet);
                    }

                    if (string.IsNullOrEmpty(quoteUpdate.GarageZip))
                    {
                        string errorMessage = "Zip code is required to rate.";
                        string msg = string.Format(@"<div class='warning'>{0}</div>", errorMessage);

                        return Json(new { returnMsg = msgreturn, returnHtml = msg }, JsonRequestBehavior.AllowGet);
                    }
                    Sessions.Instance.RatingErrMsg = quote.RatingID == Sessions.Instance.PrevRatingId ? "Error Code 989: There appears to have been an error. Please click retry b" : string.Empty;
                    //DataSet coverages = PalmsClient.GetQuoteCoverageInfo(quoteno);

                    DataSet coverages = AjaxService.GetQuoteCoverageInfo(quoteno);

                    LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: QUOTE TERM MONTHS {1}", quoteno, termMonths), "DEBUG");

                    XPCollection<XpoPolicyQuote> getQuote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", quoteno));
                    getQuote.Reload();
                    XpoPolicyQuote polQuote = getQuote.FirstOrDefault();

                    Guid installGuid = Guid.NewGuid();
                    uow.ExecuteSproc("spUW_GetListProjectedInstallments",
                        new OperandValue(polQuote.PolicyNo),
                        new OperandValue(polQuote.RatingID),
                        new OperandValue(polQuote.PayPlan.ToString()),
                        new OperandValue(installGuid));
                    QuoteModel quoteModel = UtilitiesRating.MapXpoToModel(polQuote);

                    string policyWritten = null;
                    
                    returnHtmlFinal = AjaxService.WritePolicyInstallments(uow, quoteno, coverages, policyWritten, payPlansDict, quoteModel, installGuid,
                        rtnHtml, msgreturn, hasPhyDam, emailQuote, returnHtmlFinal);
                }
                catch (Exception ex)
                {
                    msgreturn = "FAILED";

                    rtnHtml = string.Format(
                        @"<div class='error'>An error occured rating this quote, please try again. {0}</div>",
                        ex.Message);

                    LogUtils.Log("Quote-Edit: " + Sessions.Instance.Username,
                                    string.Format("Error occured while rating quote {0}: {1}", quoteno, ex.Message));
                }
            }
            return Json(new { returnMsg = msgreturn, returnHtml = returnHtmlFinal }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckIfUploadDocumentExists(string fileName)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string path =
                    Path.Combine(ConfigSettings.ReadSetting("ContentUploadPath"), fileName);

                return Json(new { fileExists = System.IO.File.Exists(path) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult OrderSupplies(string agentCode, string jacketsNo)
        {
            string msgreturn = null;
            string rtnHtml = null;

            try
            {
                string agentName = AgentUtils.GetAgencyName(agentCode);
                string subject = string.Format("[AGENT ORDER] Agent {0} has ordered supplies", agentCode);

                string order = string.Format(@"
				<ul>
					<li><b>Policy Jackets:<b> {0}</li>
				</ul>
				", jacketsNo);

                string body = EmailRenderer.OrderSupplies(agentName, order);

                if (WebHostService.IsProduction())
                {
                    string sendTo = "underwriting@palminsure.com";
                    string sendTo2 = "marketing@palminsure.com";
                    EmailService.SendEmailNotification(new List<string>() { sendTo, sendTo2 }, subject, body, true);
                }
                else
                {
                    string sendTo = "technical.support@palminsure.com";
                    EmailService.SendEmailNotification(new List<string>() { sendTo }, subject, body, true);
                }

                msgreturn = "Successs";
                rtnHtml = "Order Successful.";
            }
            catch (Exception)
            {
                msgreturn = "Fail";
                rtnHtml = "There was an error submitting your request, please try again";
            }


            return Json(new { returnMsg = msgreturn, returnHtml = rtnHtml }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogMsg(string username, string dbgmsg, string dbglvl)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg;

                try
                {
                    var save = new XpoActivityLog(uow)
                    {
                        Username = username,
                        ActivityType = dbglvl,
                        ActivityDesc = dbgmsg,
                        ActivityDate = DateTime.Now,
                        Application = "PORTAL"
                    };
                    save.Save();
                    uow.CommitChanges();

                    msg = "SUCCESS";
                }
                catch (Exception)
                {
                    msg = "FAILED";
                }

                return Json(new { returnMsg = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPolicyRenewalInfo(string policyno)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = "";
                string renPremium = "";
                string priorBal = "";
                string depositAmt = "";
                string monthlyPayment = "0 (Includes $10 Install Fee)";
                string totalWithFees = "";
                string renEffDate = "";
                string renMinDueAmt = "";
                double tmpInstallFees = 0.0;
                try
                {
                    string nextPolicyNo = policyno.GenNextPolicyNo();
                    var getRenQuote = uow.GetObjects(uow.GetClassInfo<PolicyRenQuoteTemp>(),
                        CriteriaOperator.Parse("PolicyNo = ?", nextPolicyNo), null, 1, false, true);
                    if (getRenQuote.Count == 1)
                    {
                        var renQuote = getRenQuote.OfType<PolicyRenQuoteTemp>().FirstOrDefault();
                        renPremium = renQuote.PolicyWritten.ToMoney<double>().ToString();
                        double tmpBal =
                            PalmsClient.ExecspUW_GetTotalDueOnPolicy(nextPolicyNo, "web-user",
                                Guid.NewGuid().ToString());
                        priorBal = tmpBal.ToString();
                        renEffDate = renQuote.EffDate.ToShortDateString();

                        var getInstalls = new XPCollection<RenQuoteInstallmentsTemp>(uow,
                            CriteriaOperator.Parse("PolicyNo = ? AND IgnoreRec = '0'", nextPolicyNo));
                        if (getInstalls.Count > 0)
                        {
                            foreach (RenQuoteInstallmentsTemp install in getInstalls.Where(
                                m => m.Descr == "Deposit"))
                            {
                                depositAmt = install.DownPymtAmount.ToMoney<decimal>().ToString();
                                renMinDueAmt = install.DownPymtAmount.ToString("n2");
                            }

                            foreach (RenQuoteInstallmentsTemp install in getInstalls.Where(m =>
                                m.Descr == "Installment 1"))
                            {
                                monthlyPayment =
                                    install.InstallmentPymt.SafeString().Parse<double>().ToString("C") +
                                    " (Includes $10 Install Fee)";
                            }

                            foreach (var install in getInstalls)
                            {
                                tmpInstallFees += install.InstallmentFee.SafeString().Parse<double>();
                            }

                            totalWithFees = (tmpInstallFees + renQuote.PolicyWritten).ToString("C");
                        }
                    }

                    msg = "SUCCESS";
                }
                catch (Exception ex)
                {
                    msg = "FAILED : " + ex.Message;
                }
                finally
                {
                    PalmsClient.Close();
                }

                return Json(new
                {
                    returnMsg = msg,
                    returnRenPremium = renPremium,
                    returnPriorBal = priorBal,
                    returnDepositAmt = depositAmt,
                    returnMonthlyPayment = monthlyPayment,
                    returnTotalWithFees = totalWithFees,
                    returnRenEffDate = renEffDate,
                    returnRenMinDueAmt = renMinDueAmt
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetUploadsForPolicy(string policyNo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    List<dynamic> uploadsForPolicy = new List<dynamic>();

                    if (Sessions.Instance.IsClient && !Sessions.Instance.IsAgent)
                    {
                        XPCollection<AuditImageIndexDO> clientImages = new XPCollection<AuditImageIndexDO>(uow, CriteriaOperator.Parse("FileNo = ? AND FormDesc = 'CLIENT UPLOAD'", policyNo));

                        foreach (AuditImageIndexDO upload in clientImages)
                        {
                            dynamic uploadInfo = new
                            {
                                Name = upload.FileName,
                                UploadDate = upload.DateCreated.ToShortDateString(),
                            };
                            uploadsForPolicy.Add(uploadInfo);
                        }
                        return Json(new { Message = "Success", Uploads = uploadsForPolicy }, JsonRequestBehavior.AllowGet);
                    }

                    XPCollection<AuditImageIndexDO> imagesForPolicy =
                                 new XPCollection<AuditImageIndexDO>(uow, CriteriaOperator.Parse("FileNo = ? AND FormDesc = 'AGENT UPLOAD'", policyNo));

                    foreach (AuditImageIndexDO upload in imagesForPolicy)
                    {
                        dynamic uploadInfo = new
                        {
                            Name = upload.FileName,
                            UploadDate = upload.DateCreated.ToShortDateString(),
                        };
                        uploadsForPolicy.Add(uploadInfo);
                    }
                    return Json(new { Message = "Success", Uploads = uploadsForPolicy }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { Message = "Error", ErrorMessage = e.Message }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public JsonResult UpdateRenPolicyTerm(string policyno, string isSixMonth, string isAnnual, string payPlan)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg;
                try
                {
                    string nextPolicyNo = policyno.GenNextPolicyNo();
                    var getRen =
                        new XPCollection<PolicyRenQuoteTemp>(uow, CriteriaOperator.Parse("PolicyNo = ?", nextPolicyNo))
                            .FirstOrDefault();
                    if (getRen == null)
                    {
                        return Json(new { returnMsg = "FAILED" }, JsonRequestBehavior.AllowGet);
                    }

                    try
                    {
                        var update =
                            uow.FindObject<PolicyRenQuoteTemp>(CriteriaOperator.Parse("PolicyNo = ?", nextPolicyNo));
                        update.SixMonth = isSixMonth.Parse<bool>();
                        update.isAnnual = isAnnual.Parse<bool>();
                        update.ExpDate = isSixMonth.Parse<bool>()
                            ? update.EffDate.AddMonths(6)
                            : update.EffDate.AddMonths(12);
                        update.Save();
                        uow.CommitChanges();
                        uow.ExecuteSproc("spUW_RateRenQuoteTemp", nextPolicyNo, Sessions.Instance.Username, new Guid().ToString());

                        msg = "SUCCESS";
                    }
                    catch (Exception ex)
                    {
                        msg = "FAILED : " + ex.Message;
                    }
                }
                catch (Exception ex)
                {
                    msg = "FAILED: " + ex.Message;
                }

                string logo = Path.Combine(Server.MapPath("~/Content/images"), "PalmInsureLogoNew.png");
                string filename = string.Format("{0}_Receipt_{1}.pdf", policyno,
                    DateTime.Now.ToString("MMddyyyymmhhss"));
                string outputPath = Path.Combine(Server.MapPath("~/Content/files/PaymentReceipts"), filename);
                PortalUtils.GeneratePaymentReceipt(policyno, "ACH", logo, outputPath);
                return Json(new { returnMsg = msg, returnPayPlan = payPlan }, JsonRequestBehavior.AllowGet);
            }
        }

        private JsonResult JsonErrorResponse(string msg, string errMsg, string errorMessageLevel)
        {
            return Json(new { returnMsg = msg, errorMsg = errMsg, errMsgLevel = errorMessageLevel },
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult PostSweep(string policyType,
            string policyno,
            string agentcode,
            string amount,
            string ckAcctType,
            string chkAcctNo,
            string chkRoutingNo,
            string payorName,
            bool saveBankInfo)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = null;
                string errMsg = null;
                string successMsg = null;
                string continueMsg = null;
                var modelAmt = amount.Parse<double>();
                var agentEmail = AgentUtils.GetEmailAddr(agentcode);
                var getAch = new XPCollection<AuditACHTransactions>(uow,
                    CriteriaOperator.Parse("RecordKey = ? AND ConfirmationNo IS NULL", policyno));
                var getRenAch = new XPCollection<AuditACHTransactions>(uow,
                    CriteriaOperator.Parse(
                        "RecordKey = ? AND ConfirmationNo IS NULL",
                        policyno.GenNextPolicyNo()));
                var getRenPayment = new XPCollection<Payments>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?", policyno.GenNextPolicyNo()));

                #region Check Make Sure They Are Agent

                if (!Sessions.Instance.IsAgent || string.IsNullOrEmpty(agentcode))
                {
                    msg = "FAILED";
                    errMsg = string.Format(
                        "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                        ErrorUtils.ErrCodeToId(DErrors.LostSession));

                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{1}: AGENT LOST SESSION MAKING PAYMENT FOR {0}", policyno, agentcode), "WARN");

                    return JsonErrorResponse(msg, errMsg, "WARN");
                }

                #endregion

                //////////////////////////////////

                #region Check For Unswept Sweeps and Ren payments

                if (policyType != "RENEWAL" && getAch.Count > 0)
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg, ref continueMsg, ref msg);
                    return Json(new { returnMsg = msg, succesMsg = successMsg, continueMsg = continueMsg },
                        JsonRequestBehavior.AllowGet);
                }

                if (policyType == "RENEWAL" && getRenAch.Count > 0)
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg, ref continueMsg, ref msg);
                    return Json(new { returnMsg = msg, succesMsg = successMsg, continueMsg = continueMsg },
                        JsonRequestBehavior.AllowGet);
                }

                if (policyType == "RENEWAL" && getRenPayment.Count > 0)
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg, ref continueMsg, ref msg);
                    return Json(new { returnMsg = msg, succesMsg = successMsg, continueMsg = continueMsg },
                        JsonRequestBehavior.AllowGet);
                }

                if (policyType == "ENDORSEMENT")
                {
                    PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg, ref continueMsg, ref msg);
                    return Json(new { returnMsg = msg, succesMsg = successMsg, continueMsg = continueMsg },
                        JsonRequestBehavior.AllowGet);
                }

                #endregion

                LogUtils.Log(Sessions.Instance.Username,
                    string.Format("{0}: SWEEPING AGENT ACCOUNT FOR {1}", policyno, agentcode), "DEBUG");

                // if (saveBankInfo)
                // {
                //     var save = new PolicyAltBankInfo(uow);
                //     save.ABA = Blowfish.Encrypt_CBC(chkRoutingNo);
                //     save.Account = "INDIVIDUAL";
                //     save.AccountNumber = Blowfish.Encrypt_CBC(chkAcctNo);
                //     save.AccountType = Blowfish.Encrypt_CBC(ckAcctType);
                //     save.AcctHolderName = payorName;
                //     save.PolicyNo = policyType == "RENEWAL" ? policyno.GenNextPolicyNo() : policyno;
                //     save.Save();
                //     uow.CommitChanges();
                // }

                double amtPrior = 0;
                double renAmt = 0;
                bool isRenewal = false;

                switch (policyType)
                {
                    case "RENEWAL":
                        PolicyRenQuote getRenQuote =
                            new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?",
                                policyno)).FirstOrDefault();

                        #region Null RenQuote Error Checking

                        if (getRenQuote == null)
                        {
                            msg = "FAILED";
                            errMsg = string.Format(
                                "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                                ErrorUtils.ErrCodeToId(DErrors.NoRenQuote));

                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: CANNOT FIND RENEWAL QUOTE {0}", policyno, agentcode), "FATAL");

                            return JsonErrorResponse(msg, errMsg, "FATAL");
                        }

                        #endregion

                        //-------------------------------------
                        //Renewal Installments Error Checking
                        var getRenInstalls =
                            new XPCollection<RenQuoteInstallments>(uow,
                                CriteriaOperator.Parse("PolicyNo = ? AND Descr = 'Deposit' AND IgnoreRec = '0'",
                                    getRenQuote.PolicyNo)).FirstOrDefault();

                        #region Null RenQuote Installments Error Checking

                        if (getRenInstalls == null)
                        {
                            msg = "FAILED";
                            errMsg = string.Format(
                                "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                                ErrorUtils.ErrCodeToId(DErrors.NoRenInstalls));

                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: CANNOT FIND RENEWAL QUOTE INSTALLMENTS {0}", policyno, agentcode),
                                "WARN");

                            return JsonErrorResponse(msg, errMsg, "FATAL");
                        }

                        #endregion

                        //-------------------------------------
                        //Process The Renewal Payment
                        decimal renPymnt = getRenInstalls.DownPymtAmount;
                        amtPrior = renPymnt.SafeString().Parse<double>() - modelAmt;
                        renAmt = modelAmt + amtPrior;
                        double totalboth = renPymnt.SafeString().Parse<double>() + modelAmt;

                        DateTime now = DateTime.Now;
                        string pymntBatchGuid = Guid.NewGuid().ToString();


                        if (modelAmt > 0.0)
                        {
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: SAVING RENEWAL PAYMENT INFORMATION TO PAYMENTS TABLE", policyno),
                                "INFO");

                            AgentUtils.SweepAcct(agentcode, getRenInstalls.PolicyNo, renAmt, Sessions.Instance.Username,
                                saveBankInfo);
                        }

                        PortalUtils.AuditPayments(username: Sessions.Instance.Username,
                            location: PymntLocation.AgentSweep,
                            pymntType: PymntTypes.Renewal,
                            totalAmt: totalboth,
                            priorAmt: amtPrior,
                            renAmt: renPymnt.SafeString().Parse<double>(),
                            modelAmt: modelAmt,
                            pymntGuid: pymntBatchGuid,
                            isSweep: true,
                            isFirstData: false,
                            isRen: true);

                        if (Math.Abs(amtPrior) > 1.0)
                        {
                            PortalPaymentUtils.PostToPayments(policyno: policyno,
                                paymentType: "MOV",
                                acctDate: now,
                                postmarkDate: now,
                                checkAmt: amtPrior * -1,
                                totalAmt: amtPrior * -1,
                                username: "system",
                                guid: pymntBatchGuid,
                                checkNotes: "MOVED FROM " + policyno,
                                transId: null,
                                responseCode: null);

                            PortalPaymentUtils.PostToPayments(policyno: getRenQuote.PolicyNo,
                                paymentType: "MOV",
                                acctDate: now,
                                postmarkDate: now,
                                checkAmt: amtPrior,
                                totalAmt: amtPrior,
                                username: "system",
                                guid: pymntBatchGuid,
                                checkNotes: "MONEY MOVED TO " + getRenQuote.PolicyNo,
                                transId: null,
                                responseCode: null);
                        }

                        //-------------------------------------
                        isRenewal = true;
                        break;
                    default:
                        AgentUtils.SweepAcct(agentcode, policyno, amount.Parse<double>(), Sessions.Instance.Username,
                            saveBankInfo);

                        PortalUtils.AuditPayments(username: Sessions.Instance.Username,
                            location: PymntLocation.AgentSweep,
                            pymntType: PymntTypes.Renewal,
                            totalAmt: modelAmt,
                            priorAmt: 0,
                            renAmt: 0,
                            modelAmt: modelAmt,
                            pymntGuid: Guid.NewGuid().ToString(),
                            isSweep: false,
                            isFirstData: true,
                            isRen: false);
                        break;
                }

                //////////////////////////////////
                //                var policy = new XPCollection<Policy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno))
                //                    .FirstOrDefault();
                //                var quote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno))
                //                    .FirstOrDefault();
                //                var name = policy == null
                //                    ? string.Format("{0} {1}", quote.Ins1First, quote.Ins1Last)
                //                    : string.Format("{0} {1}", policy.Ins1First, policy.Ins1Last);

                //TODO: update email noitification
                //                PortalPaymentUtils.SendSweepSuccessMail(policyno: policyno,
                //                    name: name,
                //                    agentEmail: agentEmail,
                //                    agentcode: agentcode,
                //                    renAmt: renAmt,
                //                    amtPrior: amtPrior,
                //                    modelAmt: modelAmt,
                //                    isRenewal: isRenewal,
                //                    isDev: IsDev(),
                //                    isCheck: isCheck);
                //--------------------
                PaymentService.AgentSweepRedirectMsg(Blowfish, TempData, policyno, policyType, ref successMsg, ref continueMsg, ref msg);
                return Json(new { returnMsg = msg, succesMsg = successMsg, continueMsg = continueMsg },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult PostToPaymentVendor(string policyno,
            string policyType,
            string modelAmount,
            string paymentMethod,
            string phoneNo,
            string firstname,
            string lastname,
            string address,
            string city,
            string state,
            string zipcode,
            string chkAcctType,
            string chkAcctNo,
            string chkNo,
            string chkRoutingNo,
            string dlState,
            string dlNo,
            string cardType,
            string cardNo,
            string cardCvvNo,
            string cardExpMonth,
            string cardExpYear)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                #region Initialize Vars For Payments

                //Payment Related Vars
                string paymentType = Sessions.Instance.IsClient ? "INS" : "BUS";
                var modelAmt = modelAmount.Parse<double>();
                string renPolicyNo = PortalUtils.GetRenPolicyNo(policyno);
                double checkamt = modelAmt;
                double totalamt = modelAmt;
                bool sendToBoth = policyType == "POLICY";
                string pymntBatchGuid = Guid.NewGuid().ToString();
                DateTime now = DateTime.Now;
                //Email Related Vars
                string agentcode =
                    AgentUtils.GetCorrectAgentCode(policyno, policyType == "QUOTE", policyType == "RENEWAL");
                string agentEmailAddr = AgentUtils.GetEmailAddr(agentcode);
                string insuredEmailAddr = InsuredUtils.GetInsuredEmail(policyno);
                string userType = Sessions.Instance.IsClient ? "Client" : "Agent";
                string body = null;
                string subject = String.Format("[PALMINSURE.COM] {2} payment received for {0} on {1}.",
                    DateTime.Now.ToShortDateString(), policyno, userType);
                //-----------
                string msg = null;
                string continueMsg = null;
                string successMsg = null;
                string errMsg = null;

                #endregion

                //-----------
                //Get Insured name based on policy/quote no.
                var policy = new XPCollection<XpoPolicy>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno))
                    .FirstOrDefault();
                var quote = new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno))
                    .FirstOrDefault();
                var getAch = new XPCollection<AuditACHTransactions>(uow,
                    CriteriaOperator.Parse("RecordKey = ? AND ConfirmationNo IS NULL", policyno));
                var getRenAch = new XPCollection<AuditACHTransactions>(uow,
                    CriteriaOperator.Parse("RecordKey = ? AND ConfirmationNo IS NULL", policyno.GenNextPolicyNo()));
                var getQuotePayments =
                    new XPCollection<Payments>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno));
                var getPriorRenPayments = new XPCollection<Payments>(uow,
                    CriteriaOperator.Parse("PolicyNo = ?",
                        policyno.GenNextPolicyNo()));
                // var name          = policy == null ? string.Format("{0} {1}", quote.Ins1First, quote.Ins1Last) : string.Format("{0} {1}", policy.Ins1First, policy.Ins1Last);
                var name = firstname.ToUpper() + " " + lastname.ToUpper();
                //-----------------------------------------------------------------------------
                //Make Payment To First Data

                switch (paymentMethod)
                {
                    case "CHECK":
                        if (policyType != "RENEWAL" && getAch.Count > 0)
                        {
                            VendorPymntRedirectMsg(policyType, policyno, paymentMethod, ref msg, ref successMsg,
                                ref continueMsg);
                            break;
                        }

                        if (policyType == "RENEWAL" && getRenAch.Count > 0)
                        {
                            VendorPymntRedirectMsg(policyType, policyno, paymentMethod, ref msg, ref successMsg,
                                ref continueMsg);
                            break;
                        }

                        return PostSweep(policyType: policyType,
                            policyno: policyno,
                            agentcode: agentcode,
                            amount: modelAmount,
                            ckAcctType: chkAcctType,
                            chkAcctNo: chkAcctNo,
                            chkRoutingNo: chkRoutingNo,
                            payorName: name,
                            saveBankInfo: true);
                        break;
                    default:
                        if (policyType == "QUOTE" && (getQuotePayments.Count > 0 || getAch.Count > 0))
                        {
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: VENDOR PAYMENT ALREADY MADE FOR QUOTE", policyno), "INFO");
                            VendorPymntRedirectMsg(policyType, policyno, paymentMethod, ref msg, ref successMsg,
                                ref continueMsg);
                            break;
                        }

                        if (policyType == "RENEWAL" && (getRenAch.Count > 0 || getPriorRenPayments.Count > 0))
                        {
                            LogUtils.Log(Sessions.Instance.Username,
                                string.Format("{0}: VENDOR PAYMENT ALREADY MADE FOR RENEWAL", policyno), "INFO");
                            VendorPymntRedirectMsg(policyType, policyno, paymentMethod, ref msg, ref successMsg,
                                ref continueMsg);
                            break;
                        }

                        PortalPaymentUtils.PostCreditCard(cardNo, cardExpMonth.PadLeft(2, '0'), cardExpYear, cardCvvNo,
                            modelAmt, name, policyno);

                        var getPaymentsByOrder = new XPCollection<Payments>(uow,
                            CriteriaOperator.Parse("TokenID = ?", PortalPaymentUtils.OrderId));

                        #region Process Errors From Vendor

                        if (PortalPaymentUtils.HasErrors)
                        {
                            LogUtils.Log(agentEmailAddr,
                                string.Format("{0}: PAYMENT REASON CODE RESULT WAS FAIL - {1}", policyno,
                                    PortalPaymentUtils.ErrMsg), "FATAL");

                            string notes = string.Format(@"{0} payment {1}. {2}", userType, "failed",
                                PortalPaymentUtils.ErrMsg);
                            body = EmailRenderer.PaymentSuccess(insuredName: name,
                                policyNo: policyno,
                                agentcode: agentcode,
                                paymentType: "First Data",
                                date: now,
                                amount: totalamt,
                                notes: notes);

                            if (WebHostService.IsProduction())
                            {
                                string sendTo = "underwriting@palminsure.com";
                                EmailService.SendEmailNotification(new List<string>() { sendTo }, subject, body, true);
                            }
                            LogUtils.Log(agentEmailAddr,
                                string.Format("{0}: SENT FAILED PAYMENT EMAIL TO UNDERWRITING AT PALMINSURE.COM",
                                    policyno, agentEmailAddr), "WARN");

                            PortalPaymentUtils.SendVendorPymntFailed(policyno, subject, body, agentEmailAddr,
                                insuredEmailAddr, WebHostService.IsDev(), sendToBoth);

                            msg = "FAILED";
                            errMsg = string.Format(
                                "There was an issue making your payment. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0} <br/><br/> Error Message: {1}",
                                ErrorUtils.ErrCodeToId(DErrors.VendorPymntErrCode),
                                PortalPaymentUtils.ErrMsg);

                            return JsonErrorResponse(msg, errMsg, "error");
                        }

                        #endregion

                        //-----------------------------------------------------------------------------
                        //Process Successful Payment
                        LogUtils.Log(agentEmailAddr,
                            string.Format("{0}: PAYMENT REASON CODE RESULT WAS PASS - {1}", policyno, "responsedesc"),
                            "INFO");

                        if (getPaymentsByOrder.Count == 0)
                        {
                            switch (policyType)
                            {
                                case "RENEWAL":
                                    var getRenQuote =
                                        new XPCollection<PolicyRenQuote>(uow,
                                            CriteriaOperator.Parse("PriorPolicyNo = ?",
                                                policyno)).FirstOrDefault();

                                    #region Null RenQuote Error Checking

                                    if (getRenQuote == null)
                                    {
                                        msg = "FAILED";
                                        errMsg = string.Format(
                                            "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                                            ErrorUtils.ErrCodeToId(DErrors.NoRenQuote));

                                        LogUtils.Log(Sessions.Instance.Username,
                                            string.Format("{0}: CANNOT FIND RENEWAL QUOTE {0}", policyno, agentcode),
                                            "FATAL");

                                        return JsonErrorResponse(msg, errMsg, "FATAL");
                                    }

                                    #endregion

                                    var getRenInstalls =
                                        new XPCollection<RenQuoteInstallments>(uow,
                                            CriteriaOperator.Parse(
                                                "PolicyNo = ? AND Descr = 'Deposit' AND IgnoreRec = '0'",
                                                getRenQuote.PolicyNo)).FirstOrDefault();

                                    #region Null RenQuote Installments Error Checking

                                    if (getRenInstalls == null)
                                    {
                                        msg = "FAILED";
                                        errMsg = string.Format(
                                            "There was an issue making your payment through Agent Sweep. Please try refreshing the page and making your payment again. <br/><br/> Error Code: {0}",
                                            ErrorUtils.ErrCodeToId(DErrors.NoRenInstalls));

                                        LogUtils.Log(Sessions.Instance.Username,
                                            string.Format("{0}: CANNOT FIND RENEWAL QUOTE INSTALLMENTS {0}", policyno,
                                                agentcode), "WARN");

                                        return JsonErrorResponse(msg, errMsg, "FATAL");
                                    }

                                    #endregion

                                    decimal renPymnt = getRenInstalls.DownPymtAmount;
                                    double amtPrior = renPymnt.SafeString().Parse<double>() - modelAmt;

                                    PortalUtils.AuditPayments(username: Sessions.Instance.Username,
                                        location: PymntLocation.AgentSweep,
                                        pymntType: PymntTypes.Renewal,
                                        totalAmt: totalamt,
                                        priorAmt: amtPrior,
                                        renAmt: renPymnt.SafeString().Parse<double>(),
                                        modelAmt: modelAmt,
                                        pymntGuid: pymntBatchGuid,
                                        isSweep: false,
                                        isFirstData: true,
                                        isRen: true);

                                    //Full renewal payment amt from cybersource
                                    if (modelAmt > 0.0)
                                    {
                                        LogUtils.Log(agentEmailAddr,
                                            string.Format("{0}: SAVING RENEWAL PAYMENT INFORMATION TO PAYMENTS TABLE",
                                                policyno), "INFO");

                                        PortalPaymentUtils.PostToPayments(policyno: getRenQuote.PolicyNo,
                                            paymentType: paymentType,
                                            acctDate: now,
                                            postmarkDate: now,
                                            checkAmt: totalamt,
                                            totalAmt: totalamt,
                                            username: "system",
                                            guid: pymntBatchGuid,
                                            checkNotes: null,
                                            transId: PortalPaymentUtils.OrderId,
                                            responseCode: PortalPaymentUtils.ReturnMsg);
                                    }

                                    if (Math.Abs(amtPrior) > 1.0)
                                    {
                                        PortalPaymentUtils.PostToPayments(policyno: policyno,
                                            paymentType: "MOV",
                                            acctDate: now,
                                            postmarkDate: now,
                                            checkAmt: amtPrior * -1,
                                            totalAmt: amtPrior * -1,
                                            username: "system",
                                            guid: pymntBatchGuid,
                                            checkNotes: "MOVED FROM " + policyno,
                                            transId: PortalPaymentUtils.OrderId,
                                            responseCode: PortalPaymentUtils.ReturnMsg);

                                        PortalPaymentUtils.PostToPayments(policyno: getRenQuote.PolicyNo,
                                            paymentType: "MOV",
                                            acctDate: now,
                                            postmarkDate: now,
                                            checkAmt: amtPrior,
                                            totalAmt: amtPrior,
                                            username: "system",
                                            guid: pymntBatchGuid,
                                            checkNotes: "MONEY MOVED TO " + getRenQuote.PolicyNo,
                                            transId: PortalPaymentUtils.OrderId,
                                            responseCode: PortalPaymentUtils.ReturnMsg);
                                    }

                                    break;
                                default:
                                    LogUtils.Log(agentEmailAddr,
                                        string.Format("{0}: SAVING PAYMENT INFORMATION TO PAYMENTS TABLE", policyno),
                                        "INFO");

                                    PortalUtils.AuditPayments(username: Sessions.Instance.Username,
                                        location: PymntLocation.AgentSweep,
                                        pymntType: PymntTypes.Renewal,
                                        totalAmt: totalamt,
                                        priorAmt: 0,
                                        renAmt: 0,
                                        modelAmt: modelAmt,
                                        pymntGuid: pymntBatchGuid,
                                        isSweep: false,
                                        isFirstData: true,
                                        isRen: false);

                                    PortalPaymentUtils.PostToPayments(policyno: policyno,
                                        paymentType: paymentType,
                                        acctDate: now,
                                        postmarkDate: now,
                                        checkAmt: checkamt,
                                        totalAmt: checkamt,
                                        username: "system",
                                        guid: pymntBatchGuid,
                                        checkNotes: null,
                                        transId: PortalPaymentUtils.OrderId,
                                        responseCode: PortalPaymentUtils.ReturnMsg);
                                    break;
                            }


                            PalmsClient.RunInstallmentsPaymentApply(policyno, DateTime.Now, "portal",
                                Guid.NewGuid().ToString());


                            body = EmailRenderer.PaymentSuccess(name,
                                policyno,
                                agentcode,
                                "First Data",
                                now,
                                totalamt,
                                "Payment made by agent " + Sessions.Instance.AgentCode + ". " + "");

                            string logo = Path.Combine(Server.MapPath("~/Content/images"), "PalmInsureLogoNew.png");
                            string filename = string.Format("{0}_Receipt_{1}.pdf", policyno,
                                DateTime.Now.ToString("MMddyyyymmhhss"));
                            string outputPath = Path.Combine(Server.MapPath("~/Content/files/PaymentReceipts"),
                                filename);

                            PortalUtils.GeneratePaymentReceipt(policyno, paymentType, logo, outputPath);
                        }

                        #region Decide what links to show based on policy type

                        VendorPymntRedirectMsg(policyType, policyno, paymentMethod, ref msg, ref successMsg,
                            ref continueMsg);

                        #endregion

                        break;
                }

                PalmsClient.CloseRef();
                return Json(new { returnMsg = msg, succesMsg = successMsg, continueMsg = continueMsg },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateCs(string policyno, string creditscore)
        {
            //using (var uow = XpoHelper.GetNewUnitOfWork())
            //{
            //    XpoPolicyQuote update = uow.FindObject<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo =?", policyno));
            //    AARatesAACreditScore getScore =
            //        uow.FindObject<AARatesAACreditScore>(uow, CriteriaOperator.Parse("MinScore <= ? AND MaxScore >= ?", creditscore, creditscore));

            //    if (update != null && getScore != null)
            //    {
            //        update.EstimatedCreditScore = getScore.MinScore;
            //        update.CreditScore = creditscore.Parse<int>();
            //        update.Save();
            //        uow.CommitChanges();
            //    }
            //    return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);

            //}
            return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LogIP()
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                try
                {
                    string ipAddress = Request.ServerVariables["REMOTE_ADDR"].ToString();
                    DateTime dateofLog = DateTime.Now;
                    Support.DataObjects.XpoDemoPortalLogs log = new Support.DataObjects.XpoDemoPortalLogs();
                    log.IPAddress = ipAddress;
                    log.DateOfLog = dateofLog;
                    log.Save();
                    uow.CommitChanges();
                    return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception)
                {
                    return Json(new { returnMsg = "ERROR" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult GetLexisCs(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                XpoPolicyQuote update = uow.FindObject<XpoPolicyQuote>(CriteriaOperator.Parse("PolicyNo =?", policyno));
                if (update == null)
                {
                    return Json(new { returnMsg = "ERROR" }, JsonRequestBehavior.AllowGet);
                }

                DocumentService.CreateLexisDocument(uow, update);

                return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetRelativities(string policyno)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = "ERROR";
                string rtData = "";

                if (PortalUtils.EnableCreditScore())
                {
                    XPCollection<XpoPolicyQuote> getQuote =
                        new XPCollection<XpoPolicyQuote>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyno));

                    foreach (var quote in getQuote)
                    {
                        XPCollection<RatingRelativitiesDo> getRelativities =
                            new XPCollection<RatingRelativitiesDo>(uow,
                                CriteriaOperator.Parse("MainIndexID = ?", quote.RatingID));

                        rtData = @"<table id='RatingRelativities'>
									<thead>
										<th>PolicyNo</th>
										<th>MainIndexID</th>
										<th>VehicleNo</th>
										<th>OperatorNo</th>
										<th>RateOrderNo</th>
										<th>MathAction</th>
										<th>RoundStep</th>
										<th>Relativity</th>
										<th>BI</th>
										<th>PIPNI</th>
										<th>PIPNR</th>
										<th>PD</th>
										<th>UM</th>
										<th>UMS</th>
										<th>MP</th>
										<th>COMP</th>
										<th>COLL</th>
										<th>SE</th>
										<th>PageNo</th>
									</thead>
								<tbody>";
                        foreach (var rel in getRelativities.OrderBy(m => m.RatedOperatorNo).ThenBy(m => m.VehicleNo)
                            .ThenBy(m => m.RateOrderNo))
                        {
                            msg = "SUCCESS";

                            rtData += string.Format(@"
								<tr>
									<td>{0}</td>
									<td>{1}</td>
									<td>{2}</td>
									<td>{3}</td>
									<td>{4}</td>
									<td>{5}</td>
									<td>{6}</td>
									<td>{7}</td>
									<td>{8}</td>
									<td>{9}</td>
									<td>{10}</td>
									<td>{11}</td>
									<td>{12}</td>
									<td>{13}</td>
									<td>{14}</td>
									<td>{15}</td>
									<td>{16}</td>
									<td>{17}</td>
									<td>{18}</td>
								</tr>
							", rel.PolicyNo,
                                rel.MainIndexID,
                                rel.VehicleNo,
                                rel.RatedOperatorNo,
                                rel.RateOrderNo,
                                rel.MathAction,
                                rel.RoundStep,
                                rel.Relativity,
                                rel.BI,
                                rel.PIPNI,
                                rel.PIPNR,
                                rel.PD,
                                rel.UM,
                                rel.UMS,
                                rel.MP,
                                rel.COMP,
                                rel.COLL,
                                rel.SE,
                                rel.PageNo);
                        }

                        rtData += "</tbody></table>";
                    }
                }

                return Json(new { returnMsg = msg, returnData = rtData }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAgentPaperless(string agentcode)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = null;
                string data = null;

                XpoAgents xpoAgent = new XPCollection<XpoAgents>(uow, CriteriaOperator.Parse("AgentCode = ?", agentcode))
                    .FirstOrDefault();

                if (xpoAgent == null)
                {
                    return Json(
                        new { returnMsg = "ERROR", returnData = "Agent Code Not Found, Please Refresh Your Page" },
                        JsonRequestBehavior.AllowGet);
                }

                msg = "SUCCESS";
                data = xpoAgent.IsPaperless.SafeString();

                return Json(new { returnMsg = msg, returnData = data }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateAgentPaperless(string agentcode, string ispaperless)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                string msg = null;
                string data = null;

                try
                {
                    XpoAgents update = uow.FindObject<XpoAgents>(CriteriaOperator.Parse("AgentCode = ?", agentcode));
                    update.IsPaperless = ispaperless.Upper() == "TRUE";
                    update.Save();
                    uow.CommitChanges();
                    msg = "SUCCESS";
                }
                catch
                {
                    return Json(
                        new
                        {
                            returnMsg = "ERROR",
                            returnData =
                                "There was an issue saving your choice to go paperless, Please refresh the page and try again"
                        }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { returnMsg = msg, returnData = data }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetIsDateOOSAndIsSavedEndorsement(string policyNumber, string policyEffDateStr, string endorseDateStr, string policyExpDateStr)
        {
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                DateTime policyEffDate = Convert.ToDateTime(policyEffDateStr);
                DateTime endorseDate = Convert.ToDateTime(endorseDateStr);
                DateTime policyExpDate = Convert.ToDateTime(policyExpDateStr);
                DateTime earliestDate;
                string msg = "";


                XPCollection<AuditPolicyHistory> getEndorsementInfo =
                    new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ? AND Action = 'ENDORSE'",
                        policyNumber));
                int endorseCount = getEndorsementInfo.Count();
                if (endorseCount == 0)
                    earliestDate = policyEffDate;
                else
                {
                    earliestDate = getEndorsementInfo.Max(m => m.EffDate);
                }
                var policyEndorsequote = uow.FindObject<PolicyEndorseQuote>(CriteriaOperator.Parse("PolicyNo = ?", policyNumber));
                if (endorseDate >= earliestDate && endorseDate <= policyExpDate && (policyEndorsequote?.IsSaved != true))
                {
                    TempData["ClearEndorseQuoteTables"] = null;
                    TempData["FromRating"] = false;
                }
                return Json(new
                {
                    IsDateOOS = endorseDate < earliestDate,
                    IsEndorseDateLaterThanExpDate = endorseDate > policyExpDate,
                    IsSavedEndorsement = policyEndorsequote?.IsSaved ?? false
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult SendDeepLinkEmail(string policyNumber)
        {
            try
            {
                var deepLinkSent = IntegrationsUtility.SendUploadImagesDeepLink(policyNumber, null);
                return Json(new { isSuccess = deepLinkSent, errorMessage = string.Empty }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { isSuccess = false, errorMessage = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetRewritePolicyReasons(string policyNo)
        {
            try
            {
                var reasonsList = new List<string>();
                var reasons = UtilitiesStoredProcedure.ExecspUW_GetRewritePolicyReasons(new Session(), policyNo);
                if (reasons?.ResultSet?.FirstOrDefault()?.Rows?.Count() > 0)
                {
                    foreach (var row in reasons.ResultSet[0].Rows)
                    {
                        if (row.Values.Count() > 0)
                        {
                            reasonsList.Add(row.Values[0].ToString());
                        }
                    }
                }
                return Json(new { returnMsg = "SUCCESS", reasonsList = reasonsList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return Json(new { returnMsg = "FAILURE", exceptionMsg = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveRewritePolicyNotes(string policyNo, string reasonNote)
        {
            try
            {
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    var note = new AuditRewritePolicyReasons(uow);
                    note.PolicyNo = policyNo;
                    note.Reason = reasonNote;
                    note.IsNote = true;
                    note.Save();
                    uow.CommitChanges();
                }
                return Json(new { returnMsg = "SUCCESS" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return Json(new { returnMsg = "FAILURE", exceptionMsg = msg }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SendEndorsementPaymentSchedule(string policyNo)
        {
            try
            {
                bool isPolicyRated = false;
                if (TempData["FromRating"] != null && (bool)TempData["FromRating"])
                {
                    TempData["FromRating"] = TempData["FromRating"];
                    isPolicyRated = true;
                }

                var emailBody = RenderPartialViewToString("~/Views/Shared/EndorsementpaymentSchedule.cshtml", PortalPaymentUtils.GetEndorsementInstallments(policyNo, isPolicyRated));

                EmailService.SendEmailNotification(new List<string>() { PortalUtils.GetInsuredEmail(policyNo) },
                    $"Endorsement Payment Schedule - {policyNo}", emailBody, true);


                return Json(new { isSuccess = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { dlnumber = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public virtual string RenderPartialViewToString(string viewName, object viewmodel)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = viewmodel;
            ViewData.Add("HideEmailPaymentScheduleButton", true);

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
                var viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        private void VendorPymntRedirectMsg(string policyType, string policyno, string paymentMethod, ref string msg,
            ref string successMsg, ref string continueMsg)
        {
            switch (policyType)
            {
                case "QUOTE":
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VENDOR PAYMENT MADE FROM QUOTE", policyno), "INFO");
                    //successMsg = string.Format("Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype={1}'>here</a> for your receipt", policyno, paymentMethod == "CHECK" ? "CHECKING" : "PAYMENTVENDOR");
                    continueMsg = string.Format(
                        "<div class='success'>Payment Successful. Please click the button below to issue this policy.<br/><br/><a class='btnSubmit' href='/Quote/GeneratePolicyDocs/{0}'>Get Policy Documents</a></div>",
                        Blowfish.Encrypt_CBC(policyno));
                    msg = "SUCCESS";
                    break;
                case "RENEWAL":
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VENDOR PAYMENT MADE FROM RENEWAL POLICY", policyno), "INFO");
                    msg = "SUCCESS";
                    //successMsg = string.Format("Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype={1}'>here</a> for your receipt", policyno, paymentMethod == "CHECK" ? "CHECKING" : "PAYMENTVENDOR");
                    continueMsg = string.Format(
                        "<div class='success'>Payment Successful. Please click the button below to issue this renewal.<br/><br/><a class='btnSubmit' href='/Renewal/GeneratePolicyDocs/{0}'>Get Renewal Documents</a></div>",
                        Blowfish.Encrypt_CBC(policyno));
                    break;
                default:
                    LogUtils.Log(Sessions.Instance.Username,
                        string.Format("{0}: VENDOR PAYMENT MADE FROM POLICY", policyno), "INFO");
                    msg = "SUCCESS";
                    successMsg = string.Format(
                        "Thank you for your payment, please click <a style='text-decoration: underline;' href='/Agent/PaymentReceipt/{0}?pymnttype={1}'>here</a> for your receipt",
                        policyno, paymentMethod == "CHECK" ? "CHECKING" : "PAYMENTVENDOR");
                    continueMsg =
                        string.Format(
                            "<div class='success'>Payment Successful<br/><br/><a class='btnSubmit' href='/Agent/Policy/{0}'>Go Back To Policy</a></div>",
                            policyno);
                    break;
            }
        }

        [HttpPost]
        public void UpdatePolicyQuoteField(string quoteNo, string fieldName, string fieldValue)
        {
            if (string.IsNullOrEmpty(quoteNo) || string.IsNullOrEmpty(fieldName) || string.IsNullOrEmpty(fieldValue))
            {
                return;
            }
            using (var uow = XpoHelper.GetNewUnitOfWork())
            {
                if (TempData["QuoteModel"] != null)
                {
                    TempData["QuoteModel"] = TempData["QuoteModel"];
                }
                if (TempData["VehicleID"] != null)
                {
                    TempData["VehicleID"] = TempData["VehicleID"];
                }
                XpoPolicyQuote quote =
                    new XPCollection<XpoPolicyQuote>(uow,
                        CriteriaOperator.Parse("PolicyNo = ?", quoteNo)).FirstOrDefault();
                if (quote != null)
                {
                    switch (fieldName)
                    {
                        case "FirstName1":
                            quote.Ins1First = fieldValue;
                            break;
                        case "MiddleInitial1":
                            quote.Ins1MI = fieldValue;
                            break;
                        case "LastName1":
                            quote.Ins1Last = fieldValue;
                            break;
                        case "Suffix1":
                            quote.Ins1Suffix = fieldValue;
                            break;
                        case "FirstName2":
                            quote.Ins2First = fieldValue;
                            break;
                        case "MiddleInitial2":
                            quote.Ins2MI = fieldValue;
                            break;
                        case "LastName2":
                            quote.Ins2Last = fieldValue;
                            break;
                        case "Suffix2":
                            quote.Ins2Suffix = fieldValue;
                            break;
                        case "HomePhone":
                            quote.HomePhone = fieldValue;
                            break;
                        case "WorkPhone":
                            quote.WorkPhone = fieldValue;
                            break;
                        case "GarageStreetAddress":
                            quote.GarageStreet = fieldValue;
                            break;
                        case "GarageCity":
                            if (!fieldValue.ToUpper().Equals("FIND BY ZIP"))
                            {
                                quote.GarageCity = fieldValue;
                            }
                            break;
                        case "GarageState":
                            quote.GarageState = fieldValue;
                            break;
                        case "GarageZIPCode":
                            quote.GarageZip = fieldValue;
                            break;
                        case "MailingStreetAddress":
                            quote.MailStreet = fieldValue;
                            break;
                        case "MailingCity":
                            if (!fieldValue.ToUpper().Equals("FIND BY ZIP"))
                            {
                                quote.MailCity = fieldValue;
                            }
                            break;
                        case "MailingState":
                            quote.MailState = fieldValue;
                            break;
                        case "MailingZIPCode":
                            quote.MailZip = fieldValue;
                            break;
                        case "PipDeductible":
                            quote.PIPDed = fieldValue;
                            break;
                        case "EmailAddress":
                            quote.MainEmail = fieldValue;
                            break;
                        case "WorkLoss":
                            bool hasWorkLoss = Convert.ToBoolean(fieldValue);
                            quote.WorkLoss = Convert.ToInt32(hasWorkLoss);
                            break;
                        case "WorkLossGroup":
                            bool isNio = Convert.ToBoolean(fieldValue);
                            if (isNio)
                            {
                                quote.NIO = true;
                                quote.NIRR = false;
                            }
                            else
                            {
                                quote.NIO = false;
                                quote.NIRR = true;
                            }
                            break;
                        case "Transfer_Discount":
                            quote.TransDisc = Convert.ToInt32(fieldValue);
                            break;
                        case "Homeowners_Discount":
                            quote.Homeowner = Convert.ToBoolean(fieldValue);
                            break;
                        case "Paperless_Discount":
                            quote.Paperless = Convert.ToBoolean(fieldValue);
                            break;
                        case "Advanced_Quote_Discount":
                            quote.AdvancedQuote = Convert.ToBoolean(fieldValue);
                            break;
                        case "Payment_Plan":
                            quote.PayPlan = Convert.ToInt32(fieldValue);
                            break;
                        case "Language_Preference":
                            PolicyPreferences preferenceForQuote =
                                new XPCollection<PolicyPreferences>(uow,
                                    CriteriaOperator.Parse("PolicyNo = ? AND Type = ? ", Sessions.Instance.PolicyNo, "Preferred_Language")).FirstOrDefault();
                            if (preferenceForQuote == null)
                            {
                                preferenceForQuote = new PolicyPreferences(uow)
                                {
                                    PolicyNo = Sessions.Instance.PolicyNo,
                                    Type = "Preferred_Language",
                                    Value = "ENGLISH"
                                };
                            }
                            else
                            {
                                preferenceForQuote.Value = fieldValue.ToUpper();
                            }
                            preferenceForQuote.Save();
                            break;
                        case "ESignature":
                            bool requestESignature;
                            Boolean.TryParse(fieldValue, out requestESignature);
                            if (requestESignature)
                            {
                                // add
                                PolicyESignatureRequests newRequest = new PolicyESignatureRequests(uow);
                                newRequest.PolicyNo = quoteNo;
                                newRequest.Save();
                            }
                            else
                            {
                                PolicyESignatureRequests requestToDelete =
                                    new XPCollection<PolicyESignatureRequests>(uow,
                                        CriteriaOperator.Parse("PolicyNo = ?", quoteNo)).FirstOrDefault();
                                if (requestToDelete != null)
                                {
                                    requestToDelete.Delete();
                                }
                            }
                            break;
                        case "AgentESignEmailAddress":
                            quote.AgentESignEmailAddress = fieldValue;
                            break;
                        default:
                            break;
                    }
                    quote.Save();
                    uow.CommitChanges();
                }
            }
        }
    }
}