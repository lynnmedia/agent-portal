﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using AgentPortal.Models;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Controllers
{
    public class DiagnosticsController : BaseController
    {
        //
        // GET: /Diagnostics/
        private List<DiagnosticsModel> listDiagnostics = new List<DiagnosticsModel>();

        private string GetUserIP()
        {
            string ipList = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return Request.ServerVariables["REMOTE_ADDR"];
        }

        public ActionResult Index()
        {
            if (Sessions.Instance.IsCompany)
            {
                bool isDev = false;
                using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
                {
                    SystemVars getVersion = new XPCollection<SystemVars>(CriteriaOperator.Parse("Name == ?", "SYSTEMISDEV")).FirstOrDefault();
                    isDev = getVersion.Value == "true" ? true : false;

                }

                DiagnosticsModel model = new DiagnosticsModel();
                model.IsDevEnvironment = isDev ? "Yes" : "No";
                model.XpoWcfServerLink = ConfigSettings.ReadSetting("XPO_WCF");
                model.Servername = Environment.MachineName;
                model.Username = Environment.UserName;
                model.Version = Environment.Version.ToString();
                model.UserIP = GetUserIP();


                //PortalUtils.PostDocUpload("TESTPOLICYNO", "AGENTUPLOAD", @"c:\inetpub\wwwroot\content\files\uploads\PostDoc86081ce4-945a-425d-9ae9-c593e11a90e1.pdf");

                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Company");
            }
        }//END Index
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public ActionResult DbCheck()
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                SystemVars getVersion = new XPCollection<SystemVars>(CriteriaOperator.Parse("Name == ?", "SYSTEMISDEV")).FirstOrDefault();
                bool isDev = getVersion.Value == "true" ? true : false;

                return Content(isDev.ToString());
            }//END UOW
        }//END IsDevCheck
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        protected TimeSpan TimeCall()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Version WCFVersion = PalmsClient.Version();
            sw.Stop();

            PalmsClient.CloseRef();
            return sw.Elapsed;
        }//END TimeCall
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        
    }//END Controller
}//END Namespace
