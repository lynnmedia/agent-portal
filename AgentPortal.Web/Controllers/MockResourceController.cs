﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AgentPortal.Support;

namespace AgentPortal.Controllers
{
    public abstract class MockResourceController : BaseController
    {
        protected ActionResult ShowResources<TResource>(List<TResource> resourceProvider, Func<TResource, bool> selector, string template)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(template, resourceProvider.Where(selector).ToList());
            }
            throw new NotImplementedException();
        }//END ShowResources
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        protected ActionResult AddResource<TResource>(List<TResource> resourceProvider, TResource resource, Func<TResource, bool> selector, string template)
        {
            if (!ModelState.IsValid)
            {
                if (Request.IsAjaxRequest())
                {
                    return new JsonModelErrorsResult(ModelState);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            resourceProvider.Add(resource);
            return ShowResources(resourceProvider, selector, template);
        }//END AddResource
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }//END Class
}//END Namespace
