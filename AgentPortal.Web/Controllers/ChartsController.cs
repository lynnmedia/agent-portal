﻿namespace AgentPortal.Controllers
{
    public class ChartsController : BaseController
    {
        //
        // GET: /Charts/

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /*
        //Demo 2 series line chart example, save for rainy day. 
        public JsonResult GetLineData()
        {
            Dictionary<string, double[]> retVal = new Dictionary<string, double[]>();
            double[] Array1 = { 7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6 };
            retVal.Add("Tokyo", Array1);

            double[] Array2 = { -0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5 };
            retVal.Add("New York", Array2);

            return Json(retVal.ToArray(), JsonRequestBehavior.AllowGet);
        }
        */
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        //
        // Get: /Charts/_WebAnalytics
        //public PartialViewResult _WebAnalytics()
        //{
        //    Dictionary<object, object> webData = new Dictionary<object, object>();
        //    SqlDataReader WebDataReader = Database.ExecuteQuery("SELECT COUNT([c-ip]) AS VisitorCount, [c-ip] AS VisitorIP FROM IIS_HOTH_LOGS GROUP BY [c-ip] ORDER BY VisitorCount DESC", CommandType.Text);
        //    while (WebDataReader.Read())
        //    {
        //        if (WebDataReader["VisitorIP"].ToString() != "192.168.0.254" || WebDataReader["VisitorIP"].ToString() != "::1")
        //        {
        //            webData.Add(WebDataReader["VisitorIP"], WebDataReader["VisitorCount"]);
        //        }
        //    }

        //    Highcharts chart1 = new Highcharts("chart1")
        //            .SetXAxis(new XAxis { Categories = new[] { "Ödmjukhet", "Engagemang", "Kompetens", "Lönsamhet" } })
        //            .SetYAxis(new YAxis { Title = new YAxisTitle { Text = "Betygskalan" } })
        //            .SetSeries(new Series { Data = new Data(new object[] { 1, 8, 9, 6 }), Name = "Snittbetyg" })
        //            .SetTitle(new Title { Text = "Örebro Statistik" })
        //            .InitChart(new Chart { DefaultSeriesType = ChartTypes.Bar });

        //    return PartialView(chart1);
        //}
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }//END Class Dec
}//END Namespace Dec
