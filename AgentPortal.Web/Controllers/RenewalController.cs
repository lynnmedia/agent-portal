﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;

namespace AgentPortal.Controllers
{
    public class RenewalController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GeneratePolicyDocs(string id)
        {
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                string quoteNo = null;
                bool isTestEnv = false;

                try
                {
                    quoteNo = Blowfish.Decrypt_CBC(id);
                }
                catch
                {
                    quoteNo = id;
                }
                //----------------------------------------------
                //[Step 1] Issue Renewal Quote
                PolicyRenQuote RenQuote = new XPCollection<PolicyRenQuote>(uow, CriteriaOperator.Parse("PriorPolicyNo = ?", quoteNo)).FirstOrDefault();

                //PolicyWebUtils.SaveRenewalSnapshot(RenQuote.PolicyNo, RenQuote.SixMonth ? 6 : 12, RenQuote.isAnnual, RenQuote.SixMonth, RenQuote.PayPlan, RenQuote.);

                if (RenQuote == null)
                {
                    return RedirectToAction("Agent", "Policy", new { id = quoteNo });
                }

                PalmsClient.IssueRenewal(RenQuote.PolicyNo, Sessions.Instance.Username, Guid.NewGuid().ToString());
                LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: ISSUED RENEWAL QUOTE", RenQuote.PolicyNo), "INFO");

                PortalUtils.SaveApprovalRec(RenQuote.PolicyNo, RenQuote.PriorPolicyNo, RenQuote.AgentCode);
                //----------------------------------------------
                //[Step 2] Get Ren History Index
                int historyIndex = 0;
                XPCollection<AuditPolicyHistory> getHistory = new XPCollection<AuditPolicyHistory>(uow, CriteriaOperator.Parse("PolicyNo = ?", RenQuote.PolicyNo));
                historyIndex = getHistory.Count > 0 ? getHistory.Min(m => m.IndexID) : 0;

                if (historyIndex == 0)
                {
                    LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: ERROR OCCURED GETTING HISTORY INDEX FROM AUDIT POLICY HISTORY FOR RENEWAL", Sessions.Instance.PolicyNo), "FATAL");
                }
                //----------------------------------------------
                //[Step 3] Send Email letting everyone know we issued a Renewal
                SystemVars sysVars = new XPCollection<SystemVars>(uow, CriteriaOperator.Parse("Name = 'SYSTEMISDEV'")).FirstOrDefault();
                if (sysVars != null)
                {
                    isTestEnv = sysVars.Value == "true" ? true : false;
                }

                string agentname = AgentUtils.GetAgencyName(RenQuote.AgentCode);
                string subject = isTestEnv ? "[TEST] Renewal Policy Issued" : "Renewal Policy Issued";
                string bodytitle = string.Format("[Renewal] Renewal Policy {0} was issued", RenQuote.PolicyNo);
                string body = EmailRenderer.IssueRenewal(title: bodytitle, 
                                                         policyno: RenQuote.PolicyNo, 
                                                         priorpolicyno: RenQuote.PriorPolicyNo, 
                                                         insuredname: RenQuote.Ins1FullName, 
                                                         agentcode: RenQuote.AgentCode, 
                                                         agentname: agentname, 
                                                         policytotal: RenQuote.PolicyWritten.ToString());
                if (WebHostService.IsProduction())
                {
                    string sendTo = "underwriting@palminsure.com";
                    EmailService.SendEmailNotification(new List<string>() { sendTo }, subject, body, true);
                }
                //----------------------------------------------
                //[Step 4] Generate Documents
                try
                {
                    PalmsClient.GenerateForms(RenQuote.PolicyNo, historyIndex, Sessions.Instance.Username);
                    LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: GENERATED RENEWAL FORMS USING HISTORY INDEX {1}", RenQuote.PolicyNo, historyIndex), "INFO");
                }
                catch (Exception ex)
                {
                    LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: ERROR OCCURED GENERATING RENEWAL DOCUMENTS. INDEX {1}. Msg: {2}", RenQuote.PolicyNo, historyIndex, ex.Message), "FATAL");
                }
                //----------------------------------------------
                //[Step ] Redirect this
                Sessions.Instance.PolicyNo = RenQuote.PolicyNo;
                PalmsClient.CloseRef();
                return RedirectToAction("Documents", "Renewal", new
                {
                    id = Blowfish.Encrypt_CBC(id)
                });
            }
        }

        public ActionResult Documents(string id)
        {
            id = Blowfish.Decrypt_CBC(id);
            LogUtils.Log(Sessions.Instance.Username, string.Format("{0}: VIEWING RENEWAL DOCUMENT PAGE", Sessions.Instance.PolicyNo), "INFO");
            return View();
        }
    }
}
