﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AgentPortal.Models;
using AgentPortal.PalmsServiceReference;
using AgentPortal.Repositories;
using AgentPortal.Repositories.Xpo;
using AgentPortal.Services;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Enums;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Utilities;
using PalmInsure.Palms.Xpo;
using Blowfish = AgentPortal.Support.Utilities.Blowfish;

namespace AgentPortal.Controllers
{
#if !DEBUG
    //[RequireHTTPS]
#endif
    public abstract class BaseController : Controller
    {
        #region Init Properties/Fields/Vars

        private PalmsServiceClient PalmsClientInstance;

        protected String getMerchantID = "palminsure";
        protected String getSharedSecret = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRyqlFSIG7/+clLfa9cbhOnohkDGo0919g1FvJBZsiufe9hpx6OpMrJXHdiNWx083OnPrW6sjPski2kPwtIgcEu6Ow5y8W8nb//T5BaVG0TXU9MsoeO5xKqAVMDiQcaTlZUw6OQs9t5J1hP4W5UsogRlbPbOREG5fHkmJDmLcPPQIDAQAB";
        protected String getSerialNumber = "3393796841070176056165";

        #endregion

        public PalmsServiceClient PalmsClient
        {
            get
            {
                if (PalmsClientInstance == null)
                {
                    PalmsClientInstance = new PalmsServiceClient("BasicHttpBinding_IPalmsService");
                }

                return PalmsClientInstance;
            }
        }

        public Blowfish Blowfish = new Blowfish(Constants.blowfishKey);
        public AjaxService AjaxService { get; }
        public DocumentService DocumentService { get; }
        public IUnitOfWorkFactory UnitOfWorkFactory { get; }
        public AgentService AgentService { get; }

        public WebHostService WebHostService { get; }

        public PaymentService PaymentService { get; }

        public EmailService EmailService { get; }

        public BaseController()
        {
            WebHostService = DependencyResolver.Current.GetService<WebHostService>();
            EmailService = DependencyResolver.Current.GetService<EmailService>();
            PaymentService = DependencyResolver.Current.GetService<PaymentService>();
            AgentService = DependencyResolver.Current.GetService<AgentService>();
            AjaxService = DependencyResolver.Current.GetService<AjaxService>();
            DocumentService = DependencyResolver.Current.GetService<DocumentService>();
            UnitOfWorkFactory = DependencyResolver.Current.GetService<IUnitOfWorkFactory>();
        }

        protected override void OnException(ExceptionContext ex)
        {
            string messageBody = String.Format(@"
				<table border='0' style='border-left: 1px solid #cccccc;border-top: 1px solid #cccccc; border-right: 1px solid #cccccc; border-bottom: 1px solid #cccccc;'>
					<thead style='border-bottom: 1px solid #cccccc;'>  
						<tr style='background: #004418; color: #ffffff;'>        
							<th style='color: #ffffff; text-align: center;' colspan='2'>{0}</th>
						</tr>
						<tr style='border-bottom: 1px solid #cccccc; background: #004418; color: #ffffff;'>        
							<th style='border-bottom: 1px solid #cccccc; text-align: center; color: #ffffff;'>Property</th>
							<th style='border-bottom: 1px solid #cccccc;text-align: center; color: #ffffff;'>Exception</th>
						</tr>
					</thead>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Computer:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{1}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Domain:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{2}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>User:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{3}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Exception Type:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{4}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Message:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{5}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Target Site:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{6}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Source:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{7}
						</td>
					</tr>
					<tr>
						<td style='width: 150px; border-bottom: 1px solid #cccccc;'>
							<b>Stack Trace:</b>
						</td>
						<td style='border-bottom: 1px solid #cccccc;'>
							{8}
						</td>
					</tr>
				</table>
			",
             "PALMINSURE.COM - PORTAL",
             Environment.MachineName,
             Environment.UserDomainName,
             Environment.UserName,
             ex.Exception.GetType(),
             ex.Exception.Message,
             ex.Exception.TargetSite,
             ex.Exception.Source,
             ex.Exception.StackTrace
             );
            //--------------------------------------------------------

            EmailService.SendEmailNotification(new List<string>() {"technical.support@palminsure.com"},
                "Unhandeled EXCEPTION - " + ex.Exception.GetType().ToString(), messageBody, true);
        }

        /// <summary>
        /// Load the policy into the returned model, AND into the ViewData
        /// </summary>
        /// <param name="policyNo"></param>
        /// <returns></returns>
        /// 
        protected PolicyEndorseQuote LoadNewEndorsementFromPolicy(string policyNo)
        {
            PolicyEndorseQuote endorsement = new PolicyEndorseQuote();
            XpoPolicy policyInfo = new XPCollection<XpoPolicy>(CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
            endorsement.PolicyNo = policyInfo.PolicyNo;
            endorsement.PriorPolicyNo = policyInfo.PriorPolicyNo;
            endorsement.NextPolicyNo = policyInfo.NextPolicyNo;
            endorsement.RenCount = policyInfo.RenCount;
            endorsement.State = policyInfo.State;
            endorsement.LOB = policyInfo.LOB;
            // need to talk to scott about this. which date is eff date, exp date, etc.
            endorsement.EffDate = DateTime.Now;
            endorsement.ExpDate = policyInfo.ExpDate;
            endorsement.PolicyStatus = policyInfo.PolicyStatus;
            endorsement.RenewalStatus = policyInfo.RenewalStatus;
            endorsement.DateBound = policyInfo.DateBound;
            endorsement.DateIssued = DateTime.Now;
            endorsement.CancelDate = policyInfo.CancelDate;
            endorsement.CancelType = policyInfo.CancelType;
            endorsement.Ins1First = policyInfo.Ins1First;
            endorsement.Ins1Last = policyInfo.Ins1Last;
            endorsement.Ins1MI = policyInfo.Ins1MI;
            endorsement.Ins1FullName = policyInfo.Ins1FullName;
            endorsement.Ins2First = policyInfo.Ins2First;
            endorsement.Ins2Last = policyInfo.Ins2Last;
            endorsement.Ins2MI = policyInfo.Ins2MI;
            endorsement.Ins2FullName = policyInfo.Ins2FullName;
            endorsement.Territory = policyInfo.Territory;
            endorsement.PolicyWritten = policyInfo.PolicyWritten;
            endorsement.PolicyAnnualized = policyInfo.PolicyAnnualized;
            endorsement.CommPrem = policyInfo.CommPrem;
            endorsement.DBSetupFee = policyInfo.DBSetupFee;
            endorsement.PolicyFee = policyInfo.PolicyFee;
            endorsement.SR22Fee = policyInfo.SR22Fee;
            endorsement.FHCFFee = policyInfo.FHCFFee;
            endorsement.MaintenanceFee = policyInfo.MaintenanceFee;
            endorsement.PIPPDOnlyFee = policyInfo.PIPPDOnlyFee;
            endorsement.RateCycle = policyInfo.RateCycle;
            endorsement.SixMonth = policyInfo.SixMonth;
            endorsement.PayPlan = policyInfo.PayPlan;
            endorsement.PaidInFullDisc = policyInfo.PaidInFullDisc;
            endorsement.EFT = policyInfo.EFT;
            endorsement.MailStreet = policyInfo.MailStreet;
            endorsement.MailCity = policyInfo.MailCity;
            endorsement.MailState = policyInfo.MailState;
            endorsement.MailZip = policyInfo.MailZip;
            endorsement.MailGarageSame = policyInfo.MailGarageSame;
            endorsement.GarageStreet = policyInfo.GarageStreet;
            endorsement.GarageCity = policyInfo.GarageCity;
            endorsement.GarageState = policyInfo.GarageState;
            endorsement.GarageZip = policyInfo.GarageZip;
            endorsement.GarageTerritory = policyInfo.GarageTerritory;
            endorsement.GarageCounty = policyInfo.GarageCounty;
            endorsement.HomePhone = policyInfo.HomePhone;
            endorsement.WorkPhone = policyInfo.WorkPhone;
            endorsement.MainEmail = policyInfo.MainEmail;
            endorsement.AltEmail = policyInfo.AltEmail;
            endorsement.Paperless = policyInfo.Paperless;
            endorsement.AgentCode = policyInfo.AgentCode;
            endorsement.CommAtIssue = policyInfo.CommAtIssue;
            endorsement.LOUCommAtIssue = policyInfo.LOUCommAtIssue;
            endorsement.ADNDCommAtIssue = policyInfo.ADNDCommAtIssue;
            endorsement.AgentGross = policyInfo.AgentGross;
            endorsement.PIPDed = policyInfo.PIPDed;
            endorsement.NIO = policyInfo.NIO;
            endorsement.NIRR = policyInfo.NIRR;
            endorsement.UMStacked = policyInfo.UMStacked;
            endorsement.HasBI = policyInfo.HasBI;
            endorsement.HasMP = policyInfo.HasMP;
            endorsement.HasUM = policyInfo.HasUM;
            endorsement.HasLOU = policyInfo.HasLOU;
            endorsement.BILimit = policyInfo.BILimit;
            endorsement.PDLimit = policyInfo.PDLimit;
            endorsement.UMLimit = policyInfo.UMLimit;
            endorsement.MedPayLimit = policyInfo.MedPayLimit;
            endorsement.Homeowner = policyInfo.Homeowner;
            endorsement.RenDisc = policyInfo.RenDisc;
            endorsement.TransDisc = policyInfo.TransDisc;
            endorsement.PreviousCompany = policyInfo.PreviousCompany;
            endorsement.PreviousBI = policyInfo.PreviousBI;
            endorsement.PreviousBalance = policyInfo.PreviousBalance;
            endorsement.DirectRepairDisc = policyInfo.DirectRepairDisc;
            endorsement.UndTier = policyInfo.UndTier;
            endorsement.OOSEnd = policyInfo.OOSEnd;
            endorsement.LOUCost = policyInfo.LOUCost;
            endorsement.LOUAnnlPrem = policyInfo.LOUAnnlPrem;
            endorsement.ADNDLimit = policyInfo.ADNDLimit;
            endorsement.ADNDCost = policyInfo.ADNDCost;
            endorsement.ADNDAnnlPrem = policyInfo.ADNDAnnlPrem;
            endorsement.HoldRtn = policyInfo.HoldRtn;
            endorsement.NonOwners = policyInfo.NonOwners;
            endorsement.ChangeGUID = policyInfo.ChangeGUID;
            endorsement.PolicyCars = policyInfo.policyCars;
            endorsement.PolicyDrivers = policyInfo.policyDrivers;
            endorsement.Unacceptable = policyInfo.Unacceptable;
            endorsement.HousholdIndex = policyInfo.HousholdIndex;
            endorsement.RatingID = policyInfo.RatingID;
            endorsement.OID = policyInfo.OID;
            endorsement.Cars = policyInfo.policyCars;
            endorsement.Drivers = policyInfo.policyDrivers;
            endorsement.IsReturnedMail = policyInfo.IsReturnedMail;
            endorsement.IsOnHold = policyInfo.IsOnHold;
            endorsement.IsClaimMisRep = policyInfo.IsClaimMisRep;
            endorsement.IsNoREI = policyInfo.IsNoREI;
            endorsement.IsSuspense = policyInfo.IsSuspense;
            endorsement.isAnnual = policyInfo.isAnnual;
            endorsement.Carrier = policyInfo.Carrier;
            endorsement.HasPriorCoverage = policyInfo.HasPriorCoverage;
            endorsement.HasPriorBalance = policyInfo.HasPriorBalance;
            endorsement.HasLapseNone = policyInfo.HasLapseNone;
            endorsement.HasPD = policyInfo.HasPD;
            endorsement.HasTOW = policyInfo.HasTOW;
            endorsement.PIPLimit = policyInfo.PIPLimit;
            endorsement.HasADND = policyInfo.HasADND;
            endorsement.MVRFee = policyInfo.MVRFee;
            endorsement.WorkLoss = policyInfo.WorkLoss;
            endorsement.RentalAnnlPrem = policyInfo.RentalAnnlPrem;
            endorsement.RentalCost = policyInfo.RentalCost;
            endorsement.HasLapse1131 = policyInfo.HasLapse110;
            endorsement.QuotedAmount = policyInfo.QuotedAmount;
            endorsement.FromRater = policyInfo.FromRater;
            return endorsement;
        }

        //END HandlePaymentModel
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    }
}//END Namespace Dec.