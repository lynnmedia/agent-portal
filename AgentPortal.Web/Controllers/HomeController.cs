﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using PalmInsure.Palms.Xpo;

namespace AgentPortal.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController()
        {
            if (string.IsNullOrEmpty(Sessions.Instance.CompanyLogoName) || string.IsNullOrEmpty(Sessions.Instance.FooterMiddle))
            {
                int companyID = Convert.ToInt32(ConfigSettings.ReadSetting("CompanyID"));
                using (var uow = XpoHelper.GetNewUnitOfWork())
                {
                    PortalThemes theme = uow.FindObject<PortalThemes>(CriteriaOperator.Parse("IndexID = ?", companyID));
                    if (theme != null)
                    {
                        Sessions.Instance.CompanyName = theme.CompanyName;
                        Sessions.Instance.CompanyLogoName = theme.LogoName;
                        Sessions.Instance.CompanyPrimaryColor = theme.PrimaryColor;
                        Sessions.Instance.CompanySecondaryColor = ""; // to do
                        Sessions.Instance.WhoWeAre = theme.WhoWeAre;
                        Sessions.Instance.FullServiceCo = theme.FullServiceCo;
                        Sessions.Instance.WhoWeRepresent = theme.WhoWeRepresent;
                        Sessions.Instance.WhyThisCompany = theme.WhyThisCompany;
                        Sessions.Instance.AboutUs = theme.AboutUs;
                        Sessions.Instance.Welcome = theme.Welcome;
                        Sessions.Instance.OurStaff = theme.OurStaff;
                        Sessions.Instance.UnderwritingAndAccounting = theme.UnderwritingAndAccounting;
                        Sessions.Instance.ClaimsManagement = theme.ClaimsManagement;
                        Sessions.Instance.SpecialUnit = theme.SpecialUnit;
                        Sessions.Instance.InformationTechnology = theme.InformationTechnology;
                        Sessions.Instance.CustomerService = theme.CustomerService;
                        Sessions.Instance.AboutImageName = theme.AboutImageName;
                        Sessions.Instance.SliderImageName1 = theme.SliderImageName1;
                        Sessions.Instance.SliderImageName2 = theme.SliderImageName2;
                        Sessions.Instance.SliderImageName3 = theme.SliderImageName3;
                        Sessions.Instance.ContactPhone = theme.ContactPhone;
                        Sessions.Instance.ContactEmail = theme.ContactEmail;
                        Sessions.Instance.FooterMiddle = theme.FooterMiddle;
                        Sessions.Instance.FooterAboutUs = theme.FooterAboutUs;
                    }
                }
            }
        }
        public ActionResult Index()
        {
            if (!(string.IsNullOrEmpty(Sessions.Instance.Username)) && !(string.IsNullOrEmpty(Sessions.Instance.AgentCode)))
            {
                return RedirectToAction("Search", "Agent");
            }
            else
                return View();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        public ActionResult Error()
        {
            Exception exception = Server.GetLastError();
            if (exception != null)
            {
                HandleErrorInfo info = new HandleErrorInfo(exception, "Home", "Error");
                return View("Error", info);
            }
            
            return RedirectToAction("Login", "Agent");
        }

        public ActionResult TestPdf()
        {
            string logo = Path.Combine(Server.MapPath("~/Content/images"), "PalmInsureLogoNew.png");
            string outputPath = Path.Combine(Server.MapPath("~/Content/Uploads"), "PaymentReceipt.Pdf");

            // Get PolicyNo and Amt from user (SELECT * FROM AuditACHTransactions WHERE RecordKey = 'pafl0100372' Or SELECT * FROM Payments WHERE PolicyNo = 'pafl0100372-01' )
            // Run and traverse to (for example) http://localhost:64428/home/testpdf
            ///////////////////////////////////
            string policyNo = "PAFL0100799";
            string amt = "178.92";
            ///////////////////////////////////

            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                PolicyDO Policy = new XPCollection<PolicyDO>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();

                if (Policy != null)
                {
                    ReportRenderer.PaymentReceipt(outputPdfPath: outputPath,
                                                  logoPath: logo,
                                                  address: "Representing Palm Insure, Inc. P.O. Box 25277 Sarasota, FL 34277-2277",
                                                  policyNo: Policy.PolicyNo,
                                                  insuredName: Policy.Ins1FullName,
                                                  policyEffDate: Policy.EffDate.ToShortDateString(),
                                                  policyExpDate: Policy.ExpDate.ToShortDateString(),
                                                  payType: "BUS",
                                                  payNotes: "FIRSTDATA",
                                                  payDate: DateTime.Now.ToShortDateString(),
                                                  payAmt: amt);
                }
                else
                {
                    PolicyRenQuoteDO PolicyRenQuote = new XPCollection<PolicyRenQuoteDO>(uow, CriteriaOperator.Parse("PolicyNo = ?", policyNo)).FirstOrDefault();
                    if (PolicyRenQuote != null)
                    {
                        ReportRenderer.PaymentReceipt(outputPdfPath: outputPath,
                                                      logoPath: logo,
                                                      address: "Representing Palm Insure, Inc. P.O. Box 25277 Sarasota, FL 34277-2277",
                                                      policyNo: PolicyRenQuote.PolicyNo,
                                                      insuredName: PolicyRenQuote.Ins1FullName,
                                                      policyEffDate: PolicyRenQuote.EffDate.ToShortDateString(),
                                                      policyExpDate: PolicyRenQuote.ExpDate.ToShortDateString(),
                                                      payType: "BUS",
                                                      payNotes: "AGENT SWEEP",
                                                      payDate: DateTime.Now.ToShortDateString(),
                                                      payAmt: amt);
                    }
                }

                //Encase the above are null
                
                //ReportRenderer.PaymentReceipt(outputPdfPath: outputPath,
                //                                      logoPath: logo,
                //                                      address: "Representing AGIC, Inc. P.O. Box 25277 Sarasota, FL 34277-2277",
                //                                      policyNo: policyNo,
                //                                      insuredName: "ADAMS, GRENIQUE M",
                //                                      policyEffDate: "02-08-2013",
                //                                      policyExpDate: "08-08-2013",
                //                                      payType: "BUS",
                //                                      payNotes: "AGENT SWEEP",
                //                                      payDate: DateTime.Now.ToShortDateString(),
                //                                      payAmt: amt);
                 
            }

            return View();
        }
    }//END Class Dec
}//END Namespace Dec
