﻿using AgentPortal.Support;
using AgentPortal.Support.DataObjects;
using AgentPortal.Support.Integrations;
using AgentPortal.Support.Utilities;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;

namespace AgentPortal
{
    public class ClientLoginManager
    {

        public static Policy GetLatestRenewedPolicy(Policy policy, List<Policy> policies)
        {

            var policyNextPolicy = policies.FirstOrDefault(x => x.PriorPolicyNo == policy.PolicyNo);
            if (policyNextPolicy != null)
                return GetLatestRenewedPolicy(policyNextPolicy, policies);
            else
                return policy;
        }

        public static void CreateClientLogins()
        {
            bool isSaved = false;
            using (UnitOfWork uow = XpoHelper.GetNewUnitOfWork())
            {
                var allPolicies = (
                           from p in uow.Query<Policy>().Where(x => x.PolicyStatus != null && x.PolicyStatus != "CANCEL")
                           from c in uow.Query<ClientLogin>()
                           .Where(x => !string.IsNullOrEmpty(p.MainEmail) && !string.IsNullOrEmpty(x.EmailAddress) &&
                                    p.MainEmail.Trim().ToLower() == x.EmailAddress.Trim().ToLower()).DefaultIfEmpty()
                           select new { policy = p, clientLogin = c }
                           ).Where(x => x.clientLogin == null)
                           .Select(x => x.policy)
                           .ToList();
                if (allPolicies.Count() <= 0)
                    return;
                var latestPolicyNos = new Dictionary<string, string>();
                foreach (var policy in allPolicies)
                {
                    var childPolicy = GetLatestRenewedPolicy(policy, allPolicies);
                    if (!latestPolicyNos.ContainsKey(childPolicy.PolicyNo))
                    {
                        latestPolicyNos.Add(childPolicy.PolicyNo, childPolicy.MainEmail);
                    }
                }
                var latestPolicies = allPolicies.Where(x => latestPolicyNos.Keys.Contains(x.PolicyNo)).ToList();

                var duplicateNames = new List<Policy>();
                var policyIds = new List<Policy>();
                var validClientsCount = 0;
                string invalidEmailAddress = null;
                if (latestPolicies.Count() > 0)
                {
                    foreach (var policy in latestPolicies)
                    {
                        var duplicatesFound = latestPolicies.Where(x => x.MainEmail?.Trim()?.ToLower() == policy.MainEmail?.Trim()?.ToLower() &&
                                                                        x.Ins1First?.Trim()?.ToLower() != policy.Ins1First?.Trim()?.ToLower() && x.OID != policy.OID);
                        if (!duplicatesFound.Any())
                        {
                            if (!policyIds.Any(x => x.PolicyNo == policy.PolicyNo))
                            {
                                policyIds.Add(policy);
                            }
                        }
                        else
                        {
                            if (!duplicateNames.Any(x => x.OID == policy.OID))
                                duplicateNames.Add(policy);
                            foreach (var duplicateFound in duplicatesFound)
                            {
                                if (!duplicateNames.Any(x => x.OID == duplicateFound.OID))

                                    duplicateNames.Add(duplicateFound);
                            }
                        }
                    }
                }
                if (policyIds.Count() > 0)
                {

                    Blowfish Blowfish = new Blowfish(Constants.blowfishKey);
                    var validPolicies = policyIds.Where(x => !string.IsNullOrEmpty(x.Ins1First))
                                        .Select(x => new
                                        {
                                            MainEmail = x.MainEmail.Trim().ToLower(),
                                            Ins1First = char.ToUpper(x.Ins1First.Trim()[0]) + x.Ins1First.Trim().Substring(1).ToLower(),
                                            isValidEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(x.MainEmail.Trim().ToLower()).Success
                                        }).Distinct();

                    var invalidEmails = new List<Policy>();
                    foreach (var policy in validPolicies.Where(x => x.isValidEmail))
                    {
                        string guidString = Guid.NewGuid().ToString();
                        string hashedGuid = Blowfish.Encrypt_CBC(guidString);
                        ClientLogin newClientLogin = new ClientLogin(uow)
                        {
                            EmailAddress = policy.MainEmail,
                            ValidationCode = guidString,
                            HashedPassword = ""
                        };
                        newClientLogin.Save();
                        uow.CommitChanges();
                        validClientsCount++;
                        string link =
                            $@"{ConfigSettings.ReadSetting("PolicyDocsBaseUrl")}/Client/SetPassword?key={hashedGuid}";
                        IntegrationsUtility.SendEmailNotification(new List<string>() { policy.MainEmail },
                            "Access your new auto insurance policy with Palm Insure!",
                            $@"<div><p>Please click the link below to finish setting up your account to access our client portal and maintain your paperless discount.  The client portal is where you can access your new auto insurance policy documents and make payments.</p></div><br/><div></div> <div> <a href='{link}'>Click here to access your account.</a> </div><br/>",
                            true);
                        //string guidString = Guid.NewGuid().ToString();
                        //ClientLogin newClientLogin = new ClientLogin(uow)
                        //{
                        //    EmailAddress = policy.MainEmail,
                        //    ValidationCode = guidString,
                        //    HashedPassword = Blowfish.Encrypt_CBC(policy.Ins1First + "2020#"),
                        //    IsValidated = true,
                        //    IsActive =true
                        //};
                        //newClientLogin.Save();
                    }

                    if (validPolicies.Any(x => !x.isValidEmail))
                        invalidEmailAddress = string.Join(",", validPolicies.Where(x=>!x.isValidEmail).Select(x => x.MainEmail).Distinct());
                }

                isSaved = validClientsCount > 0;
                string duplicates = string.Empty;
                if (isSaved && duplicateNames.Any())
                {
                    foreach (var dupName in duplicateNames.Select(x => x.MainEmail?.Trim()?.ToLower()).Distinct())
                    {
                        var matchedPolicies = duplicateNames.Where(x => x.MainEmail?.Trim()?.ToLower() == dupName);
                        if (matchedPolicies.Any())
                        {
                            duplicates += "Email:" + matchedPolicies.First().MainEmail?.Trim()?.ToLower() + Environment.NewLine +
                                "FirstNames:" + string.Join(",", matchedPolicies.Select(x => x.Ins1First).Distinct()) + Environment.NewLine +
                                "Policies:" + string.Join(",", matchedPolicies.Select(x => x.PolicyNo).Distinct()) + Environment.NewLine + Environment.NewLine;
                        }
                    }
                }
                string message = "Create Logins for exisitng Clients ";
                if (isSaved)
                {
                    message += " is successfull";
                    message += Environment.NewLine + "Total logins Created are " + validClientsCount+ Environment.NewLine;
                    if (!string.IsNullOrEmpty(invalidEmailAddress))
                    {
                        message += "Invalid Email Address Found are:" + invalidEmailAddress + Environment.NewLine;
                    }

                    if (!string.IsNullOrEmpty(duplicates))
                    {
                        message += "Duplicates Found below:"+ Environment.NewLine+ duplicates + Environment.NewLine; ;
                    }

                }
                else
                {
                    message += "failed";
                    message += Environment.NewLine + "Total Clients found are " + policyIds.Count() + Environment.NewLine;
                }

                IntegrationsUtility.SendEmailNotification(new List<string>() { "technical.support@palminsure.com" },
                    "Create Logins For Existing Clients",
                    message
                    , false);


            }
        }

    }
}